<html>

<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name=Generator content="Microsoft Word 12 (filtered)">
<style>
<!--
 /* Font Definitions */
 @font-face
	{font-family:Wingdings;
	panose-1:5 0 0 0 0 0 0 0 0 0;}
@font-face
	{font-family:SimSun;
	panose-1:2 1 6 0 3 1 1 1 1 1;}
@font-face
	{font-family:Mangal;
	panose-1:2 4 5 3 5 2 3 3 2 2;}
@font-face
	{font-family:"Cambria Math";
	panose-1:2 4 5 3 5 4 6 3 2 4;}
@font-face
	{font-family:Tahoma;
	panose-1:2 11 6 4 3 5 4 4 2 4;}
@font-face
	{font-family:"Microsoft YaHei";
	panose-1:2 11 5 3 2 2 4 2 2 4;}
@font-face
	{font-family:"Albertus Medium";
	panose-1:0 0 0 0 0 0 0 0 0 0;}
@font-face
	{font-family:"\@SimSun";
	panose-1:2 1 6 0 3 1 1 1 1 1;}
@font-face
	{font-family:"\@Microsoft YaHei";
	panose-1:2 11 5 3 2 2 4 2 2 4;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin:0cm;
	margin-bottom:.0001pt;
	font-size:10.0pt;
	font-family:"Times New Roman","serif";}
h1
	{mso-style-link:"Heading 1 Char";
	margin-top:0cm;
	margin-right:0cm;
	margin-bottom:0cm;
	margin-left:21.6pt;
	margin-bottom:.0001pt;
	text-align:center;
	text-indent:-21.6pt;
	page-break-after:avoid;
	font-size:10.0pt;
	font-family:"Arial","sans-serif";
	font-weight:bold;
	text-decoration:underline;}
h2
	{mso-style-link:"Heading 2 Char";
	margin-top:12.0pt;
	margin-right:0cm;
	margin-bottom:3.0pt;
	margin-left:28.8pt;
	text-indent:-28.8pt;
	page-break-after:avoid;
	font-size:14.0pt;
	font-family:"Arial","sans-serif";
	font-weight:bold;
	font-style:italic;}
h4
	{mso-style-link:"Heading 4 Char";
	margin-top:0cm;
	margin-right:0cm;
	margin-bottom:0cm;
	margin-left:43.2pt;
	margin-bottom:.0001pt;
	text-indent:-43.2pt;
	page-break-after:avoid;
	font-size:12.0pt;
	font-family:"Times New Roman","serif";
	font-weight:normal;
	text-decoration:underline;}
h5
	{mso-style-link:"Heading 5 Char";
	margin-right:0cm;
	margin-left:0cm;
	font-size:10.0pt;
	font-family:"Times New Roman","serif";
	font-weight:bold;}
p.MsoCommentText, li.MsoCommentText, div.MsoCommentText
	{mso-style-link:"Comment Text Char";
	margin-top:0cm;
	margin-right:0cm;
	margin-bottom:10.0pt;
	margin-left:0cm;
	font-size:10.0pt;
	font-family:"Albertus Medium","sans-serif";}
p.MsoCaption, li.MsoCaption, div.MsoCaption
	{margin-top:6.0pt;
	margin-right:0cm;
	margin-bottom:6.0pt;
	margin-left:0cm;
	font-size:12.0pt;
	font-family:"Times New Roman","serif";
	font-style:italic;}
p.MsoTitle, li.MsoTitle, div.MsoTitle
	{mso-style-link:"Title Char";
	margin:0cm;
	margin-bottom:.0001pt;
	text-align:center;
	font-size:20.0pt;
	font-family:"Times New Roman","serif";
	font-weight:bold;}
p.MsoBodyText, li.MsoBodyText, div.MsoBodyText
	{mso-style-link:"Body Text Char";
	margin-top:0cm;
	margin-right:0cm;
	margin-bottom:6.0pt;
	margin-left:0cm;
	font-size:10.0pt;
	font-family:"Times New Roman","serif";}
p.MsoSubtitle, li.MsoSubtitle, div.MsoSubtitle
	{mso-style-link:"Subtitle Char";
	margin-top:12.0pt;
	margin-right:0cm;
	margin-bottom:6.0pt;
	margin-left:0cm;
	text-align:center;
	page-break-after:avoid;
	font-size:14.0pt;
	font-family:"Arial","sans-serif";
	font-style:italic;}
a:link, span.MsoHyperlink
	{color:blue;
	text-decoration:underline;}
a:visited, span.MsoHyperlinkFollowed
	{color:purple;
	text-decoration:underline;}
p
	{margin-right:0cm;
	margin-left:0cm;
	font-size:12.0pt;
	font-family:"Times New Roman","serif";}
p.MsoCommentSubject, li.MsoCommentSubject, div.MsoCommentSubject
	{mso-style-link:"Comment Subject Char";
	margin:0cm;
	margin-bottom:.0001pt;
	font-size:10.0pt;
	font-family:"Times New Roman","serif";
	font-weight:bold;}
p.MsoAcetate, li.MsoAcetate, div.MsoAcetate
	{mso-style-link:"Balloon Text Char";
	margin:0cm;
	margin-bottom:.0001pt;
	font-size:8.0pt;
	font-family:"Tahoma","sans-serif";}
p.MsoListParagraph, li.MsoListParagraph, div.MsoListParagraph
	{margin-top:0cm;
	margin-right:0cm;
	margin-bottom:0cm;
	margin-left:36.0pt;
	margin-bottom:.0001pt;
	font-size:10.0pt;
	font-family:"Times New Roman","serif";}
p.MsoListParagraphCxSpFirst, li.MsoListParagraphCxSpFirst, div.MsoListParagraphCxSpFirst
	{margin-top:0cm;
	margin-right:0cm;
	margin-bottom:0cm;
	margin-left:36.0pt;
	margin-bottom:.0001pt;
	font-size:10.0pt;
	font-family:"Times New Roman","serif";}
p.MsoListParagraphCxSpMiddle, li.MsoListParagraphCxSpMiddle, div.MsoListParagraphCxSpMiddle
	{margin-top:0cm;
	margin-right:0cm;
	margin-bottom:0cm;
	margin-left:36.0pt;
	margin-bottom:.0001pt;
	font-size:10.0pt;
	font-family:"Times New Roman","serif";}
p.MsoListParagraphCxSpLast, li.MsoListParagraphCxSpLast, div.MsoListParagraphCxSpLast
	{margin-top:0cm;
	margin-right:0cm;
	margin-bottom:0cm;
	margin-left:36.0pt;
	margin-bottom:.0001pt;
	font-size:10.0pt;
	font-family:"Times New Roman","serif";}
span.Heading1Char
	{mso-style-name:"Heading 1 Char";
	mso-style-link:"Heading 1";
	font-family:"Arial","sans-serif";
	font-weight:bold;
	text-decoration:underline;}
span.Heading2Char
	{mso-style-name:"Heading 2 Char";
	mso-style-link:"Heading 2";
	font-family:"Arial","sans-serif";
	font-weight:bold;
	font-style:italic;}
span.Heading4Char
	{mso-style-name:"Heading 4 Char";
	mso-style-link:"Heading 4";
	text-decoration:underline;}
span.TitleChar
	{mso-style-name:"Title Char";
	mso-style-link:Title;
	font-weight:bold;}
span.SubtitleChar
	{mso-style-name:"Subtitle Char";
	mso-style-link:Subtitle;
	font-family:"Arial","sans-serif";
	font-style:italic;}
span.BodyTextChar
	{mso-style-name:"Body Text Char";
	mso-style-link:"Body Text";}
span.Heading5Char
	{mso-style-name:"Heading 5 Char";
	mso-style-link:"Heading 5";
	font-family:"Times New Roman","serif";
	font-weight:bold;}
span.item-no
	{mso-style-name:item-no;}
span.apple-converted-space
	{mso-style-name:apple-converted-space;}
span.CommentTextChar
	{mso-style-name:"Comment Text Char";
	mso-style-link:"Comment Text";
	font-family:"Albertus Medium","sans-serif";}
span.BalloonTextChar
	{mso-style-name:"Balloon Text Char";
	mso-style-link:"Balloon Text";
	font-family:"Tahoma","sans-serif";}
span.CommentSubjectChar
	{mso-style-name:"Comment Subject Char";
	mso-style-link:"Comment Subject";
	font-family:"Albertus Medium","sans-serif";
	font-weight:bold;}
.MsoChpDefault
	{font-size:10.0pt;}
@page WordSection1
	{size:612.0pt 792.0pt;
	margin:72.0pt 72.0pt 72.0pt 72.0pt;}
div.WordSection1
	{page:WordSection1;}
 /* List Definitions */
 ol
	{margin-bottom:0cm;}
ul
	{margin-bottom:0cm;}
-->
</style>

</head>

<body lang=EN-MY link=blue vlink=purple>

<div class=WordSection1>

<p class=MsoNormal style='margin-left:0cm;text-indent:0cm;line-height:115%'><b><span
lang=EN-US style='font-size:18.0pt;line-height:115%;color:#333333'>Privacy
Policy</span></b></p>

<p class=MsoNormal style='margin-left:0cm;text-indent:0cm;line-height:115%'><b><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>&nbsp;</span></b></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%'><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>GLOCO MALAYSIA SDN. BHD. together with its parent and <a
name="_GoBack"></a>sister companies (“us”, “we”, or “<b>GLOCO</b>”, which also
includes its affiliates) is the author and publisher of the internet resource
www.ghealth.com, the mobile application ‘<b>gHealth</b>’ and other GLOCO’s
services (together “<b>Website</b>”) on the world wide web as well as the
software and applications provided by GLOCO, including but not limited to the
mobile application ‘gHealth’, the software and applications of the brand names
‘gHealth’ and GLOCO’s software and applications (together with the Website,
referred to as the “<b>Services</b>”).</span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%'><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>&nbsp;</span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%'><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>This privacy policy (&quot;<b>Privacy Policy</b>&quot;) explains
how we collect, use, share, disclose and protect Personal information about the
Users of the Services, including the Practitioners (as defined in the Terms of
Use, which may be accessed via the following weblink https://ghealth.com/privacy
(the “Terms of Use”), the End-Users (as defined in the Terms of Use), and the
visitors of Website (jointly and severally referred to as “you” or “Users” in
this Privacy Policy). We created this Privacy Policy to demonstrate our
commitment to the protection of your privacy and your personal information.
Your use of and access to the Services is subject to this Privacy Policy and
our Terms of Use. Any capitalized term used but not defined in this Privacy
Policy shall have the meaning attributed to it in our Terms of Use.</span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%'><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>&nbsp;</span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%'><span lang=EN-GB style='font-size:12.0pt;line-height:115%;
color:#333333'>Please read the following Privacy Policy for this Website, the
mobile application ‘gHealth’ and GLOCO’s software and applications before
proceeding to access and use the same. By accessing and using the same, you
have agreed that this Privacy Policy shall supersede and cancel all previous
agreements either in written form or orally between the parties in respect of
this Website, the mobile application ‘gHealth’ and GLOCO’s software and
applications.</span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%'><span lang=EN-GB style='font-size:12.0pt;line-height:115%;
color:#333333'>&nbsp;</span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%'><span lang=EN-GB style='font-size:12.0pt;line-height:115%;
color:#333333'>While all information herein has been prepared in good faith, no
representation or warranty, expressed or implied, is made and no responsibility
or liability will be accepted by </span><span lang=EN-US style='font-size:12.0pt;
line-height:115%;color:#333333'>GLOCO MALAYSIA SDN. BHD. together with its
sister companies (<b>“GLOCO”</b>) </span><span lang=EN-GB style='font-size:
12.0pt;line-height:115%;color:#333333'>as to the accuracy or completeness of
information contained herein, which is subject to change from time to time
without prior notice.</span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%'><span lang=EN-GB style='font-size:12.0pt;line-height:115%;
color:#333333'>&nbsp;</span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%'><span lang=EN-GB style='font-size:12.0pt;line-height:115%;
color:#333333'>GLOCO owns the copyright of this website, and content of this
website can be viewed online, or downloaded in soft or hard copies for an
individual’s own reference only. Any reproduction in whole or in part of the
content of this website or link to any other website without the express prior
written consent of GLOCO is strictly prohibited.</span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%'><span lang=EN-GB style='font-size:12.0pt;line-height:115%;
color:#333333'>&nbsp;</span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%'><span lang=EN-GB style='font-size:12.0pt;line-height:115%;
color:#333333'>&nbsp;</span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%'><span lang=EN-GB style='font-size:12.0pt;line-height:115%;
color:#333333'>&nbsp;</span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%'><span lang=EN-GB style='font-size:12.0pt;line-height:115%;
color:#333333'>By using our website, you are explicitly consenting that any and
all your personal data and other information provided, keyed-in by you and
further collected, stored and transmitted by GLOCO from this website may be
used, processed, shared and disclosed for such purposes and to such persons in
accordance with GLOCO’s website privacy statement.</span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%'><span lang=EN-GB style='font-size:12.0pt;line-height:115%;
color:#333333'>&nbsp;</span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%'><span lang=EN-GB style='font-size:12.0pt;line-height:115%;
color:#333333'>GLOCO will not be liable for any loss or damage including
without limitation, indirect or consequential loss or damage, or any loss or
damage whatsoever arising from loss of data or profits arising out of, or in
connection with, the use of this website.</span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%'><span lang=EN-GB style='font-size:12.0pt;line-height:115%;
color:#333333'>&nbsp;</span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%'><span lang=EN-GB style='font-size:12.0pt;line-height:115%;
color:#333333'>Links to other websites through this website is included for
additional information and for your convenience only. GLOCO has no control over
the nature, content and availability of those websites. In addition, the
inclusion of these links does not necessarily imply a recommendation to that
link nor do GLOCO endorse the views expressed on those external websites.</span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%'><span lang=EN-GB style='font-size:12.0pt;line-height:115%;
color:#333333'>&nbsp;</span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%'><span lang=EN-GB style='font-size:12.0pt;line-height:115%;
color:#333333'>Please also note that GLOCO endeavours to ensure that this
website is available 24 hours a day, 7 days a week, throughout a calendar year.
However, should this website be temporarily suspended or is made unavailable
for any reason or for any length of time, GLOCO does not accept any liability
which may arise from this website’s downtime.</span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%'><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>&nbsp;</span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%'><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>BY USING THE SERVICES OR BY OTHERWISE GIVING US YOUR
INFORMATION, YOU WILL BE DEEMED TO HAVE READ, UNDERSTOOD AND AGREED TO THE
PRACTICES AND POLICIES OUTLINED IN THIS PRIVACY POLICY AND AGREE TO BE BOUND BY
THE PRIVACY POLICY. YOU HEREBY CONSENT TO OUR COLLECTION, USE AND SHARING,
DISCLOSURE OF YOUR INFORMATION AS DESCRIBED IN THIS PRIVACY POLICY. WE RESERVE
THE RIGHT TO CHANGE, MODIFY, ADD OR DELETE PORTIONS OF THE TERMS OF THIS
PRIVACY POLICY, AT OUR SOLE DISCRETION, AT ANY TIME. IF YOU DO NOT AGREE WITH
THIS PRIVACY POLICY AT ANY TIME, DO NOT USE ANY OF THE SERVICES OR GIVE US ANY
OF YOUR INFORMATION. IF YOU USE THE SERVICES ON BEHALF OF SOMEONE ELSE (SUCH AS
YOUR CHILD) OR AN ENTITY (SUCH AS YOUR EMPLOYER), YOU REPRESENT THAT YOU ARE
AUTHORISED BY SUCH INDIVIDUAL OR ENTITY TO (I) ACCEPT THIS PRIVACY POLICY ON
SUCH INDIVIDUAL’S OR ENTITY’S BEHALF, AND (II) CONSENT ON BEHALF OF SUCH
INDIVIDUAL OR ENTITY TO OUR COLLECTION, USE AND DISCLOSURE OF SUCH INDIVIDUAL’S
OR ENTITY’S INFORMATION AS DESCRIBED IN THIS PRIVACY POLICY.</span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%'><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>&nbsp;</span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%'><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>&nbsp;</span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%'><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>&nbsp;</span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%'><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpFirst style='text-align:justify;text-justify:inter-ideograph;
text-indent:-18.0pt;line-height:115%'><b><span lang=EN-US style='font-size:
12.0pt;line-height:115%;color:#333333'>1.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></b><b><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>COLLECTION OF PERSONAL INFORMATION</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph;line-height:115%'><b><span lang=EN-US style='font-size:12.0pt;
line-height:115%;color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph;line-height:115%'><span lang=EN-US style='font-size:12.0pt;
line-height:115%;color:#333333'>Generally some of the Services require us to
know who you are so that we can best meet your needs. When you access the
Services, or through any interaction with us via emails, telephone calls or
other correspondence, we may ask you to voluntarily provide us with certain
information that personally identifies you or could be used to personally
identify you. You hereby consent to the collection of such information by GLOCO.
Without prejudice to the generality of the above, information collected by us
from you may include (but is not limited to) the following:</span></p>

<p class=MsoListParagraphCxSpLast style='text-align:justify;text-justify:inter-ideograph;
line-height:115%'><b><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>&nbsp;</span></b></p>

<p class=MsoNormal style='margin-left:60.0pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-60.0pt;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>i.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>contact data (such as your email address and phone number);</span></p>

<p class=MsoNormal style='margin-left:60.0pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-60.0pt;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>ii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>demographic data (such as your gender, your date of birth and
your pin code);</span></p>

<p class=MsoNormal style='margin-left:60.0pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-60.0pt;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>iii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>data regarding your usage of the services and history of the
appointments made by or with you through the use of Services;</span></p>

<p class=MsoNormal style='margin-left:60.0pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-60.0pt;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>iv.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>insurance data (such as your insurance carrier and insurance
plan);</span></p>

<p class=MsoNormal style='margin-left:60.0pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-60.0pt;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>v.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>other information that you voluntarily choose to provide to us
(such as information shared by you with us through emails or letters.</span></p>

<p class=MsoNormal style='margin-left:60.0pt;text-align:justify;text-justify:
inter-ideograph;line-height:115%'><span lang=EN-US style='font-size:12.0pt;
line-height:115%;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpFirst style='text-align:justify;text-justify:inter-ideograph;
line-height:115%'><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>The information collected from you by GLOCO may constitute
‘personal information’ or ‘sensitive personal data or information’ under the Personal
Data Protection Act 2010.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph;line-height:115%'><span lang=EN-US style='font-size:12.0pt;
line-height:115%;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph;line-height:115%'><b><span lang=EN-US style='font-size:12.0pt;
line-height:115%;color:#333333'>“Personal Information”</span></b><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>&nbsp;is
defined under the Personal Data Protection Act 2010 to mean any information
that relates to a natural person, which, either directly or indirectly, in
combination with other information available or likely to be available to a
body corporate, is capable of identifying such person.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph;line-height:115%'><span lang=EN-US style='font-size:12.0pt;
line-height:115%;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph;line-height:115%'><span lang=EN-US style='font-size:12.0pt;
line-height:115%;color:#333333'>The Personal Data Protection Act 2010 further
defines “Sensitive Personal Data or Information” of a person to mean personal
information about that person relating to:</span></p>

<p class=MsoListParagraphCxSpLast style='text-align:justify;text-justify:inter-ideograph;
line-height:115%'><b><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>&nbsp;</span></b></p>

<p class=MsoNormal style='margin-left:60.0pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-60.0pt;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>vi.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>passwords;</span></p>

<p class=MsoNormal style='margin-left:60.0pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-60.0pt;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>vii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>financial information such as bank accounts, credit and debit
card details or other payment instrument details;</span></p>

<p class=MsoNormal style='margin-left:60.0pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-60.0pt;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>viii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>physical, physiological and mental health condition;</span></p>

<p class=MsoNormal style='margin-left:60.0pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-60.0pt;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>ix.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>sexual orientation;</span></p>

<p class=MsoNormal style='margin-left:60.0pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-60.0pt;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>x.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>medical records and history;</span></p>

<p class=MsoNormal style='margin-left:60.0pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-60.0pt;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>xi.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>biometric information;</span></p>

<p class=MsoNormal style='margin-left:60.0pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-60.0pt;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>xii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>information received by body corporate under lawful contract or
otherwise;</span></p>

<p class=MsoNormal style='margin-left:60.0pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-60.0pt;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>xiii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>visitor details as provided at the time of registration or
thereafter; and</span></p>

<p class=MsoNormal style='margin-left:60.0pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-60.0pt;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>xiv.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>call data records.</span></p>

<p class=MsoNormal style='margin-left:60.0pt;text-align:justify;text-justify:
inter-ideograph;line-height:115%'><span lang=EN-US style='font-size:12.0pt;
line-height:115%;color:#333333'>&nbsp;</span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%'><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>            GLOCO will be free to use, collect and disclose
information that is freely available in the             public domain without
your consent.</span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%'><b><span style='font-size:12.0pt;line-height:115%;color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpFirst style='text-align:justify;text-justify:inter-ideograph;
text-indent:-18.0pt;line-height:115%'><b><span style='font-size:12.0pt;
line-height:115%;color:#333333'>2.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></b><b><span style='font-size:12.0pt;line-height:115%;color:#333333'>PERSONAL
DATA PROTECTION POLICY</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph;line-height:115%'><b><span style='font-size:12.0pt;line-height:
115%;color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph;line-height:115%'><span style='font-size:12.0pt;line-height:
115%;color:#333333'>We, </span><b><span lang=EN-US style='font-size:12.0pt;
line-height:115%;color:#333333'>GLOCO MALAYSIA SDN. BHD.</span></b><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'> together
with our sister companies (“<b>GLOCO</b>”)</span><span style='font-size:12.0pt;
line-height:115%;color:#333333'>, are committed to comply with the Malaysian
Personal Data Protection Act 2010 (the “<b>PDPA</b>”) in ensuring that your
personal data are proficiently protected. This Personal Data Protection Policy
(the “<b>Policy</b>”) is to inform you about our practices in dealing with
privacy matters and the policy that we have implemented to ensure that your
privacy remains intact. </span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph;line-height:115%'><span style='font-size:12.0pt;line-height:
115%;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph;line-height:115%'><span style='font-size:12.0pt;line-height:
115%;color:#333333'>From here on, the word “you” or “your” or any variation
thereof shall refer to the owner of the personal data, which include both the
patients and medical practitioners.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph;line-height:115%'><span style='font-size:12.0pt;line-height:
115%;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph;line-height:115%'><span style='font-size:12.0pt;line-height:
115%;color:#333333'>The word “we” or “us” or any variation thereof, including
“GLOCO” shall refer to </span><b><span lang=EN-US style='font-size:12.0pt;
line-height:115%;color:#333333'>GLOCO MALAYSIA SDN. BHD.</span></b><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'> and its
sister companies</span><span style='font-size:12.0pt;line-height:115%;
color:#333333'>.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph;line-height:115%'><span style='font-size:12.0pt;line-height:
115%;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
style='font-size:12.0pt;line-height:115%;color:#333333'>2.1<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span
style='font-size:12.0pt;line-height:115%;color:#333333'>General</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span style='font-size:12.0pt;
line-height:115%;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span style='font-size:12.0pt;
line-height:115%;color:#333333'>For the purpose of this Policy, in line with the
provisions under the PDPA, “Personal Data” shall mean any data or information
that relates directly or indirectly to you and makes you identifiable or makes
it possible for another person to identify you. It also includes “Sensitive
Personal Data” which covers information regarding your physical or mental
health, religion, beliefs, or any such other data under the PDPA.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span style='font-size:12.0pt;
line-height:115%;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span style='font-size:12.0pt;
line-height:115%;color:#333333'>This Policy relates to all Personal Data and
Sensitive Personal Data which is in our possession or which will be collected
by us in the future throughout your time with us. This Policy supplements but
does not supersede nor replace any other consent you may have previously
provided to us in respect of your Personal Data and Sensitive Personal Data,
and your consent herein are in addition to any rights which we may have at law
to collect, use, process or disclose.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span style='font-size:12.0pt;
line-height:115%;color:#333333'> </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
style='font-size:12.0pt;line-height:115%;color:#333333'>2.2<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span
style='font-size:12.0pt;line-height:115%;color:#333333'>How We Collect</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span style='font-size:12.0pt;
line-height:115%;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span style='font-size:12.0pt;
line-height:115%;color:#333333'>We collect your Personal Data and Sensitive
Personal Data mostly directly from you through:</span></p>

<p class=MsoListParagraphCxSpLast style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span style='font-size:12.0pt;
line-height:115%;color:#333333'>&nbsp;</span></p>

<p class=MsoNormal style='margin-left:72.0pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt;line-height:115%'><span style='font-size:
12.0pt;line-height:115%;font-family:Symbol;color:#333333'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:115%;color:#333333'>Medical
software, database and apps;</span></p>

<p class=MsoNormal style='margin-left:72.0pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt;line-height:115%'><span style='font-size:
12.0pt;line-height:115%;font-family:Symbol;color:#333333'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:115%;color:#333333'>Medical
visits at clinics, hospitals and other forms of medical institution;</span></p>

<p class=MsoNormal style='margin-left:72.0pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt;line-height:115%'><span style='font-size:
12.0pt;line-height:115%;font-family:Symbol;color:#333333'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:115%;color:#333333'>Various
Application and/or Registration Forms;</span></p>

<p class=MsoNormal style='margin-left:72.0pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt;line-height:115%'><span style='font-size:
12.0pt;line-height:115%;font-family:Symbol;color:#333333'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:115%;color:#333333'>Medical
records;</span></p>

<p class=MsoNormal style='margin-left:72.0pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt;line-height:115%'><span style='font-size:
12.0pt;line-height:115%;font-family:Symbol;color:#333333'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:115%;color:#333333'>Documents
and records given as part of the application process;</span></p>

<p class=MsoNormal style='margin-left:72.0pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt;line-height:115%'><span style='font-size:
12.0pt;line-height:115%;font-family:Symbol;color:#333333'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:115%;color:#333333'>Enquiry
Forms;</span></p>

<p class=MsoNormal style='margin-left:72.0pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt;line-height:115%'><span style='font-size:
12.0pt;line-height:115%;font-family:Symbol;color:#333333'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:115%;color:#333333'>Request
Forms ; and</span></p>

<p class=MsoNormal style='margin-left:72.0pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt;line-height:115%'><span style='font-size:
12.0pt;line-height:115%;font-family:Symbol;color:#333333'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:115%;color:#333333'>Previous
qualification related documents and/or certificates.</span></p>

<p class=MsoNormal style='margin-left:54.0pt;text-align:justify;text-justify:
inter-ideograph;line-height:115%'><span style='font-size:12.0pt;line-height:
115%;color:#333333'>&nbsp;</span></p>

<p class=MsoNormal style='margin-left:54.0pt;text-align:justify;text-justify:
inter-ideograph;line-height:115%'><span style='font-size:12.0pt;line-height:
115%;color:#333333'>There may be instances where your Personal Data and
Sensitive Personal Data are collected through third parties or external sources
which are not restricted to the following:</span></p>

<p class=MsoNormal style='margin-left:54.0pt;text-align:justify;text-justify:
inter-ideograph;line-height:115%'><span style='font-size:12.0pt;line-height:
115%;color:#333333'>&nbsp;</span></p>

<p class=MsoNormal style='margin-left:72.0pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt;line-height:115%'><span style='font-size:
12.0pt;line-height:115%;font-family:Symbol;color:#333333'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:115%;color:#333333'>Third
parties such as previous medical clinics, hospitals and institutions, law
enforcement agencies and other governmental entities;</span></p>

<p class=MsoNormal style='margin-left:72.0pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt;line-height:115%'><span style='font-size:
12.0pt;line-height:115%;font-family:Symbol;color:#333333'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:115%;color:#333333'>Third
party service providers incidental or in relation to the above; or</span></p>

<p class=MsoNormal style='margin-left:72.0pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt;line-height:115%'><span style='font-size:
12.0pt;line-height:115%;font-family:Symbol;color:#333333'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:115%;color:#333333'>Public
domains for publicly available data.</span></p>

<p class=MsoListParagraphCxSpFirst style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span style='font-size:12.0pt;
line-height:115%;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpLast style='margin-left:54.0pt;text-indent:-18.0pt;
line-height:115%'><span style='font-size:12.0pt;line-height:115%;color:#333333'>2.3<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span
style='font-size:12.0pt;line-height:115%;color:#333333'>What We Collect</span></p>

<p class=MsoNormal style='margin-left:36.0pt;text-align:justify;text-justify:
inter-ideograph;line-height:115%'><span style='font-size:12.0pt;line-height:
115%;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpFirst style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span style='font-size:12.0pt;
line-height:115%;color:#333333'>For optimal medical, healthcare and administrative
services, we need to collect various Personal Data and sometimes, Sensitive
Personal Data from you. Generally, the Personal Data that we collect and
process include, but are not limited to:-</span></p>

<p class=MsoListParagraphCxSpLast style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span style='font-size:12.0pt;
line-height:115%;color:#333333'>&nbsp;</span></p>

<p class=MsoNormal style='margin-left:72.0pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt;line-height:115%'><span style='font-size:
12.0pt;line-height:115%;font-family:Symbol;color:#333333'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:115%;color:#333333'>Name;</span></p>

<p class=MsoNormal style='margin-left:72.0pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt;line-height:115%'><span style='font-size:
12.0pt;line-height:115%;font-family:Symbol;color:#333333'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:115%;color:#333333'>Identification
Number (NRIC for Malaysian citizens and Passport Number non-citizen);</span></p>

<p class=MsoNormal style='margin-left:72.0pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt;line-height:115%'><span style='font-size:
12.0pt;line-height:115%;font-family:Symbol;color:#333333'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:115%;color:#333333'>Contact
details (telephone number, address, email address, etc)</span></p>

<p class=MsoNormal style='margin-left:72.0pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt;line-height:115%'><span style='font-size:
12.0pt;line-height:115%;font-family:Symbol;color:#333333'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:115%;color:#333333'>Photo;</span></p>

<p class=MsoNormal style='margin-left:72.0pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt;line-height:115%'><span style='font-size:
12.0pt;line-height:115%;font-family:Symbol;color:#333333'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:115%;color:#333333'>Previous
qualifications;</span></p>

<p class=MsoNormal style='margin-left:72.0pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt;line-height:115%'><span style='font-size:
12.0pt;line-height:115%;font-family:Symbol;color:#333333'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:115%;color:#333333'>Parents/Guardians’
marital status;</span></p>

<p class=MsoNormal style='margin-left:72.0pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt;line-height:115%'><span style='font-size:
12.0pt;line-height:115%;font-family:Symbol;color:#333333'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:115%;color:#333333'>Medical
record and details;</span></p>

<p class=MsoNormal style='margin-left:72.0pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt;line-height:115%'><span style='font-size:
12.0pt;line-height:115%;font-family:Symbol;color:#333333'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:115%;color:#333333'>Emergency
contact person(s) details;</span></p>

<p class=MsoNormal style='margin-left:72.0pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt;line-height:115%'><span style='font-size:
12.0pt;line-height:115%;font-family:Symbol;color:#333333'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:115%;color:#333333'>Billing
related information;</span></p>

<p class=MsoNormal style='margin-left:72.0pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt;line-height:115%'><span style='font-size:
12.0pt;line-height:115%;font-family:Symbol;color:#333333'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:115%;color:#333333'>Employment/Occupation;
and</span></p>

<p class=MsoNormal style='margin-left:72.0pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt;line-height:115%'><span style='font-size:
12.0pt;line-height:115%;font-family:Symbol;color:#333333'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:115%;color:#333333'>Any
other details as per required in all application and/or registration forms and
other forms issued by us.</span></p>

<p class=MsoNormal style='margin-left:54.0pt;text-align:justify;text-justify:
inter-ideograph;line-height:115%'><span style='font-size:12.0pt;line-height:
115%;color:#333333'>&nbsp;</span></p>

<p class=MsoNormal style='margin-left:54.0pt;text-align:justify;text-justify:
inter-ideograph;line-height:115%'><span style='font-size:12.0pt;line-height:
115%;color:#333333'>The provision of all information that we requested in the
relevant forms is mandatory unless otherwise specified to ensure that your
application or request will be processed efficiently. Your failure in providing
us with the relevant information may prevent us from processing your
application or request. </span></p>

<p class=MsoNormal style='margin-left:54.0pt;text-align:justify;text-justify:
inter-ideograph;line-height:115%'><span style='font-size:12.0pt;line-height:
115%;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraph style='margin-left:54.0pt;text-indent:-18.0pt;
line-height:115%'><span style='font-size:12.0pt;line-height:115%;color:#333333'>2.4<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span
style='font-size:12.0pt;line-height:115%;color:#333333'>Purpose of the Personal
Data</span></p>

<p class=MsoNormal style='margin-left:36.0pt;text-align:justify;text-justify:
inter-ideograph;line-height:115%'><span style='font-size:12.0pt;line-height:
115%;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpFirst style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span style='font-size:12.0pt;
line-height:115%;color:#333333'>Your Personal Data and Sensitive Personal Data
are collected and processed for, but not limited to, the following purposes:-</span></p>

<p class=MsoListParagraphCxSpLast style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span style='font-size:12.0pt;
line-height:115%;color:#333333'>&nbsp;</span></p>

<p class=MsoNormal style='margin-left:72.0pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt;line-height:115%'><span style='font-size:
12.0pt;line-height:115%;font-family:Symbol;color:#333333'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:115%;color:#333333'>To
process application for admission;</span></p>

<p class=MsoNormal style='margin-left:72.0pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt;line-height:115%'><span style='font-size:
12.0pt;line-height:115%;font-family:Symbol;color:#333333'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:115%;color:#333333'>To
maintain your personal details and medical records;</span></p>

<p class=MsoNormal style='margin-left:72.0pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt;line-height:115%'><span style='font-size:
12.0pt;line-height:115%;font-family:Symbol;color:#333333'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:115%;color:#333333'>To
provide you an update on the products/software offered;</span></p>

<p class=MsoNormal style='margin-left:72.0pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt;line-height:115%'><span style='font-size:
12.0pt;line-height:115%;font-family:Symbol;color:#333333'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:115%;color:#333333'>To
manage usage or admission into gHealth;</span></p>

<p class=MsoNormal style='margin-left:72.0pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt;line-height:115%'><span style='font-size:
12.0pt;line-height:115%;font-family:Symbol;color:#333333'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:115%;color:#333333'>To
communicate to patients and medical practitioners on any important
announcements/updates/offers;</span></p>

<p class=MsoNormal style='margin-left:72.0pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt;line-height:115%'><span style='font-size:
12.0pt;line-height:115%;font-family:Symbol;color:#333333'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:115%;color:#333333'>To
update on products/packages/updates from business partners or business
associates;</span></p>

<p class=MsoNormal style='margin-left:72.0pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt;line-height:115%'><span style='font-size:
12.0pt;line-height:115%;font-family:Symbol;color:#333333'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:115%;color:#333333'>To
collect information for relevant local or international statutory authorities
or third party service providers related to healthcare products;</span></p>

<p class=MsoNormal style='margin-left:72.0pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt;line-height:115%'><span style='font-size:
12.0pt;line-height:115%;font-family:Symbol;color:#333333'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:115%;color:#333333'>To
care for the medical needs of patients;</span></p>

<p class=MsoNormal style='margin-left:72.0pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt;line-height:115%'><span style='font-size:
12.0pt;line-height:115%;font-family:Symbol;color:#333333'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:115%;color:#333333'>To
manage and respond to enquiries by the patients and medical practitioners;</span></p>

<p class=MsoNormal style='margin-left:72.0pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt;line-height:115%'><span style='font-size:
12.0pt;line-height:115%;font-family:Symbol;color:#333333'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:115%;color:#333333'>To
provide the relevant administrative support, counselling and pastoral services;</span></p>

<p class=MsoNormal style='margin-left:72.0pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt;line-height:115%'><span style='font-size:
12.0pt;line-height:115%;font-family:Symbol;color:#333333'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:115%;color:#333333'>To
administer the fee and other payment; and</span></p>

<p class=MsoNormal style='margin-left:72.0pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt;line-height:115%'><span style='font-size:
12.0pt;line-height:115%;font-family:Symbol;color:#333333'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:115%;color:#333333'>Any
other purposes related to the services provided by GLOCO and gHealth.</span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%'><span style='font-size:12.0pt;line-height:115%;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraph style='margin-left:54.0pt;text-indent:-18.0pt;
line-height:115%'><span style='font-size:12.0pt;line-height:115%;color:#333333'>2.5<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span
style='font-size:12.0pt;line-height:115%;color:#333333'>Disclosure of Your
Personal Data</span></p>

<p class=MsoNormal style='margin-left:36.0pt;text-align:justify;text-justify:
inter-ideograph;line-height:115%'><span style='font-size:12.0pt;line-height:
115%;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpFirst style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span style='font-size:12.0pt;
line-height:115%;color:#333333'>We will keep your Personal Data and Sensitive
Personal Data as strictly confidential unless written consent was given to us
to disclose them or if the disclosure is within the ambit of permitted
disclosure under the prevailing PDPA or guidelines.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span style='font-size:12.0pt;
line-height:115%;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpLast style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span style='font-size:12.0pt;
line-height:115%;color:#333333'>We may however, disclose your information to
the non-exhaustive list of third parties below:-</span></p>

<p class=MsoNormal style='margin-left:72.0pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt;line-height:115%'><span style='font-size:
12.0pt;line-height:115%;font-family:Symbol;color:#333333'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:115%;color:#333333'>GLOCO’s
business partners or business associates;</span></p>

<p class=MsoNormal style='margin-left:72.0pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt;line-height:115%'><span style='font-size:
12.0pt;line-height:115%;font-family:Symbol;color:#333333'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:115%;color:#333333'>GLOCO’s
group of companies or their appointed agencies;</span></p>

<p class=MsoNormal style='margin-left:72.0pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt;line-height:115%'><span style='font-size:
12.0pt;line-height:115%;font-family:Symbol;color:#333333'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:115%;color:#333333'>Medical
institutions, clinics and hospitals;</span></p>

<p class=MsoNormal style='margin-left:72.0pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt;line-height:115%'><span style='font-size:
12.0pt;line-height:115%;font-family:Symbol;color:#333333'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:115%;color:#333333'>Professional
bodies;</span></p>

<p class=MsoNormal style='margin-left:72.0pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt;line-height:115%'><span style='font-size:
12.0pt;line-height:115%;font-family:Symbol;color:#333333'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:115%;color:#333333'>Third
parties appointed by GLOCO to provide healthcare services and/or products such
as health and lifestyle products, insurance &amp; takaful, etc; and</span></p>

<p class=MsoNormal style='margin-left:72.0pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt;line-height:115%'><span style='font-size:
12.0pt;line-height:115%;font-family:Symbol;color:#333333'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:115%;color:#333333'>Other
relevant institutions or companies.</span></p>

<p class=MsoNormal style='margin-left:54.0pt;text-align:justify;text-justify:
inter-ideograph;line-height:115%'><span style='font-size:12.0pt;line-height:
115%;color:#333333'>&nbsp;</span></p>

<p class=MsoNormal style='margin-left:54.0pt;text-align:justify;text-justify:
inter-ideograph;line-height:115%'><span style='font-size:12.0pt;line-height:
115%;color:#333333'>We will not transfer your Personal Data and Sensitive
Personal Data to a country outside of Malaysia unless:-</span></p>

<p class=MsoNormal style='margin-left:72.0pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt;line-height:115%'><span style='font-size:
12.0pt;line-height:115%;font-family:Symbol;color:#333333'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:115%;color:#333333'>We
have your consent (express or implied through your subscription of our website
and/or mobile application); and</span></p>

<p class=MsoNormal style='margin-left:72.0pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt;line-height:115%'><span style='font-size:
12.0pt;line-height:115%;font-family:Symbol;color:#333333'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:115%;color:#333333'>The
receiving country’s data protection laws provide an adequate level of data
protection.</span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%'><span style='font-size:12.0pt;line-height:115%;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraph style='margin-left:54.0pt;text-indent:-18.0pt;
line-height:115%'><span style='font-size:12.0pt;line-height:115%;color:#333333'>2.6<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span
style='font-size:12.0pt;line-height:115%;color:#333333'>Data Security </span></p>

<p class=MsoNormal style='margin-left:36.0pt;text-align:justify;text-justify:
inter-ideograph;line-height:115%'><span style='font-size:12.0pt;line-height:
115%;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraph style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span style='font-size:12.0pt;
line-height:115%;color:#333333'>We use commercially reasonable physical,
managerial and technical safeguards to preserve the integrity and security of
your data and will not knowingly allow access to anyone outside of GLOCO, other
than you or as described in this Policy. </span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%'><span style='font-size:12.0pt;line-height:115%;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraph style='margin-left:54.0pt;text-indent:-18.0pt;
line-height:115%'><span style='font-size:12.0pt;line-height:115%;color:#333333'>2.7<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span
style='font-size:12.0pt;line-height:115%;color:#333333'>Your Rights</span></p>

<p class=MsoNormal style='margin-left:36.0pt;text-align:justify;text-justify:
inter-ideograph;line-height:115%'><span style='font-size:12.0pt;line-height:
115%;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpFirst style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span style='font-size:12.0pt;
line-height:115%;color:#333333'>Under the PDPA, you have the following rights
to:-</span></p>

<p class=MsoListParagraphCxSpLast style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span style='font-size:12.0pt;
line-height:115%;color:#333333'>&nbsp;</span></p>

<p class=MsoNormal style='margin-left:72.0pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt;line-height:115%'><span style='font-size:
12.0pt;line-height:115%;font-family:Symbol;color:#333333'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:115%;color:#333333'>Request
for access to your data and obtain copies of such data;</span></p>

<p class=MsoNormal style='margin-left:72.0pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt;line-height:115%'><span style='font-size:
12.0pt;line-height:115%;font-family:Symbol;color:#333333'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:115%;color:#333333'>Require
us to correct any Personal Data relating to you which is inaccurate for the
purpose for which it is being used; and</span></p>

<p class=MsoNormal style='margin-left:72.0pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt;line-height:115%'><span style='font-size:
12.0pt;line-height:115%;font-family:Symbol;color:#333333'>·<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:115%;color:#333333'>To
take action to stop the use of, erase, and/or dispose of inaccurate data.</span></p>

<p class=MsoNormal style='margin-left:54.0pt;text-align:justify;text-justify:
inter-ideograph;line-height:115%'><span style='font-size:12.0pt;line-height:
115%;color:#333333'>&nbsp;</span></p>

<p class=MsoNormal style='margin-left:54.0pt;text-align:justify;text-justify:
inter-ideograph;line-height:115%'><span style='font-size:12.0pt;line-height:
115%;color:#333333'>Should you wish to exercise the rights above, you may do so
through a written request together with a prescribed fee. We shall, subject to
exemptions, comply with the request and/or take reasonable steps to process
such request within 30 working days.</span></p>

<p class=MsoNormal style='margin-left:54.0pt;text-align:justify;text-justify:
inter-ideograph;line-height:115%'><span style='font-size:12.0pt;line-height:
115%;color:#333333'>&nbsp;</span></p>

<p class=MsoNormal style='margin-left:54.0pt;text-align:justify;text-justify:
inter-ideograph;line-height:115%'><span style='font-size:12.0pt;line-height:
115%;color:#333333'>The request may be forwarded to us through:</span></p>

<p class=MsoNormal style='margin-left:54.0pt;text-align:justify;text-justify:
inter-ideograph;line-height:115%'><b><span style='font-size:12.0pt;line-height:
115%;color:#333333'>GLOCO MALAYSIA SDN. BHD</span></b></p>

<p class=MsoNormal style='margin-left:54.0pt;text-align:justify;text-justify:
inter-ideograph;line-height:115%'><span style='font-size:12.0pt;line-height:
115%;color:#333333'>A-G-13A &amp; A-01-13A, Block A, Merchant Square, </span></p>

<p class=MsoNormal style='margin-left:54.0pt;text-align:justify;text-justify:
inter-ideograph;line-height:115%'><span style='font-size:12.0pt;line-height:
115%;color:#333333'>No.1, Jalan Tropicana Selatan 1,</span></p>

<p class=MsoNormal style='margin-left:54.0pt;text-align:justify;text-justify:
inter-ideograph;line-height:115%'><span style='font-size:12.0pt;line-height:
115%;color:#333333'>PJU 3, 47410 Petaling Jaya, </span></p>

<p class=MsoNormal style='margin-left:54.0pt;text-align:justify;text-justify:
inter-ideograph;line-height:115%'><span style='font-size:12.0pt;line-height:
115%;color:#333333'>Selangor Darul Ehsan, Malaysia.</span></p>

<p class=MsoNormal style='margin-left:54.0pt;text-align:justify;text-justify:
inter-ideograph;line-height:115%'><span style='font-size:12.0pt;line-height:
115%;color:#333333'>Email : infoMY@gloco.com </span></p>

<p class=MsoNormal style='margin-left:54.0pt;text-align:justify;text-justify:
inter-ideograph;line-height:115%'><span style='font-size:12.0pt;line-height:
115%;color:#333333'>Tel : 1300 88 6008</span></p>

<p class=MsoNormal style='margin-left:54.0pt;text-align:justify;text-justify:
inter-ideograph;line-height:115%'><span style='font-size:12.0pt;line-height:
115%;color:#333333'>Fax : 60-378830990</span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%'><span style='font-size:12.0pt;line-height:115%;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraph style='margin-left:54.0pt;text-indent:-18.0pt;
line-height:115%'><span style='font-size:12.0pt;line-height:115%;color:#333333'>2.8<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span
style='font-size:12.0pt;line-height:115%;color:#333333'>Disposal of Data</span></p>

<p class=MsoNormal style='margin-left:36.0pt;text-align:justify;text-justify:
inter-ideograph;line-height:115%'><span style='font-size:12.0pt;line-height:
115%;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpFirst style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span style='font-size:12.0pt;
line-height:115%;color:#333333'>As Personal Data and Sensitive Personal Data
should not be retained for longer than its required purpose, we have the
obligation to ensure that your Personal Data are destroyed and/or permanently
deleted after a period of time. We will delete your Personal Data and Sensitive
Personal Data in accordance with our internal procedures. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span style='font-size:12.0pt;
line-height:115%;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpLast style='margin-left:54.0pt;text-indent:-18.0pt;
line-height:115%'><span style='font-size:12.0pt;line-height:115%;color:#333333'>2.9<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span
style='font-size:12.0pt;line-height:115%;color:#333333'>Change Policy</span></p>

<p class=MsoNormal style='margin-left:36.0pt;text-align:justify;text-justify:
inter-ideograph;line-height:115%'><span style='font-size:12.0pt;line-height:
115%;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpFirst style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span style='font-size:12.0pt;
line-height:115%;color:#333333'>We reserve the right to alter any of the clauses
contained herein in compliance with local legislation, and for any other
purpose that we deem necessary. As such, we advise that you keep yourself
updated on these terms. If there are any inconsistency between these terms and
the additional terms, the additional terms will prevail to the extent of the
inconsistency.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span style='font-size:12.0pt;
line-height:115%;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpLast style='margin-left:54.0pt;text-indent:-18.0pt;
line-height:115%'><span style='font-size:12.0pt;line-height:115%;color:#333333'>2.10<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:115%;color:#333333'>Governing
Law</span></p>

<p class=MsoNormal style='margin-left:36.0pt;text-align:justify;text-justify:
inter-ideograph;line-height:115%'><span style='font-size:12.0pt;line-height:
115%;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpFirst style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span style='font-size:12.0pt;
line-height:115%;color:#333333'>This Policy is governed by and shall be
construed in accordance with the laws of Malaysia and hereby is submitted to
the jurisdiction of the Malaysian courts.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span style='font-size:12.0pt;
line-height:115%;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt;line-height:115%'><b><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>3.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></b><b><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>PRIVACY STATEMENTS</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph;line-height:115%'><b><span lang=EN-US style='font-size:12.0pt;
line-height:115%;color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>3.1<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>ALL USERS NOTE:</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>This section applies to
all users.</span></p>

<p class=MsoListParagraphCxSpLast style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><b><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>&nbsp;</span></b></p>

<p class=MsoNormal style='margin-left:128.55pt;text-align:justify;text-justify:
inter-ideograph;line-height:115%'><span lang=EN-US style='font-size:12.0pt;
line-height:115%;color:#333333'>3.1.1 Accordingly, a condition of each User’s
use of and access to the Services is their acceptance of the Terms of Use,
which also involves acceptance of the terms of this Privacy Policy. Any User
that does not agree with any provisions of the same has the option to
discontinue the Services provided by GLOCO immediately.</span></p>

<p class=MsoNormal style='margin-left:128.55pt;text-align:justify;text-justify:
inter-ideograph;line-height:115%'><span lang=EN-US style='font-size:12.0pt;
line-height:115%;color:#333333'>3.1.2 An indicative list of information that
GLOCO may require you to provide to enable your use of the Services is provided
in the Schedule annexed to this Privacy Policy.</span></p>

<p class=MsoNormal style='margin-left:128.55pt;text-align:justify;text-justify:
inter-ideograph;line-height:115%'><span lang=EN-US style='font-size:12.0pt;
line-height:115%;color:#333333'>3.1.3 All the information provided to GLOCO by
a User, including Personal Information or any Sensitive Personal Data or
Information, is voluntary. You understand that GLOCO may use certain
information of yours, which has been designated as Personal Information or
‘Sensitive Personal Data or Information’ under the Personal Data Protection Act
2010, (a) for the purpose of providing you the Services, (b) for commercial
purposes and in an aggregated or non-personally identifiable form for research,
statistical analysis and business intelligence purposes, (c) for sale or
transfer of such research, statistical or intelligence data in an aggregated or
non-personally identifiable form to third parties and affiliates (d) for
communication purpose so as to provide You a better way of booking appointments
and for obtaining feedback in relation to the Practitioners and their practice,
(e) debugging customer support related issues.. GLOCO also reserves the right
to use information provided by or about the End-User for the following
purposes:</span></p>

<p class=MsoNormal style='margin-left:145.7pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-145.7pt;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>i.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>Publishing such information on the Website.</span></p>

<p class=MsoNormal style='margin-left:145.7pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-145.7pt;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>ii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>Contacting End-Users for offering new products or services.</span></p>

<p class=MsoNormal style='margin-left:145.7pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-145.7pt;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>iii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>Contacting End-Users for taking product and Service feedback.</span></p>

<p class=MsoNormal style='margin-left:145.7pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-145.7pt;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>iv.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>Analyzing software usage patterns for improving product design
and utility.</span></p>

<p class=MsoNormal style='margin-left:145.7pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-145.7pt;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>v.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>Analyzing anonymized practice information for commercial use.</span></p>

<p class=MsoNormal style='margin-left:128.55pt;text-align:justify;text-justify:
inter-ideograph;line-height:115%'><span lang=EN-US style='font-size:12.0pt;
line-height:115%;color:#333333'>If you have voluntarily provided your Personal
Information to GLOCO for any of the purposes stated above, you hereby consent
to such collection and use of such information by GLOCO. However, GLOCO shall
not contact you on Your telephone number(s) for any purpose including those
mentioned in this sub-section 4.1(iii), if such telephone number is registered
with the Do Not Call registry (“DNC Registry”) under the PDPA without your
express, clear and un-ambiguous written consent.</span></p>

<p class=MsoNormal style='margin-left:128.55pt;text-align:justify;text-justify:
inter-ideograph;line-height:115%'><span lang=EN-US style='font-size:12.0pt;
line-height:115%;color:#333333'>3.1.4 Collection, use and disclosure of
information which has been designated as Personal Information or Sensitive
Personal Data or Information’ under the Personal Data Protection Act 2010 requires
your express consent. By affirming your assent to this Privacy Policy, you
provide your consent to such use, collection and disclosure as required under
applicable law.</span></p>

<p class=MsoNormal style='margin-left:128.55pt;text-align:justify;text-justify:
inter-ideograph;line-height:115%'><span lang=EN-US style='font-size:12.0pt;
line-height:115%;color:#333333'>3.1.5 GLOCO does not control or endorse the
content, messages or information found in any Services and, therefore, GLOCO
specifically disclaims any liability with regard to the Services and any
actions resulting from your participation in any Services, and you agree that
you waive any claims against GLOCO relating to same, and to the extent such
waiver may be ineffective, you agree to release any claims against GLOCO
relating to the same.</span></p>

<p class=MsoNormal style='margin-left:128.55pt;text-align:justify;text-justify:
inter-ideograph;line-height:115%'><span lang=EN-US style='font-size:12.0pt;
line-height:115%;color:#333333'>3.1.6 You are responsible for maintaining the
accuracy of the information you submit to us, such as your contact information
provided as part of account registration. If your personal information changes,
you may correct, delete inaccuracies, or amend information by making the change
on our member information page or by contacting us through&nbsp;</span><span
lang=EN-US><a href="mailto:infoMY@GLOCO.com"><span style='font-size:12.0pt;
line-height:115%'>infoMY@GLOCO.com</span></a></span><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>. We will make good
faith efforts to make requested changes in our then active databases as soon as
reasonably practicable. If you provide any information that is untrue,
inaccurate, out of date or incomplete (or becomes untrue, inaccurate, out of
date or incomplete), or GLOCO has reasonable grounds to suspect that the
information provided by you is untrue, inaccurate, out of date or incomplete, GLOCO
may, at its sole discretion, discontinue the provision of the Services to you.
There may be circumstances where GLOCO will not correct, delete or update your
Personal Data, including (a) where the Personal Data is opinion data that is
kept solely for evaluative purpose; and (b) the Personal Data is in documents
related to a prosecution if all proceedings relating to the prosecution have
not been completed.</span></p>

<p class=MsoNormal style='margin-left:128.55pt;text-align:justify;text-justify:
inter-ideograph;line-height:115%'><span lang=EN-US style='font-size:12.0pt;
line-height:115%;color:#333333'>3.1.7 If you wish to cancel your account or
request that we no longer use your information to provide you Services, contact
us through&nbsp; </span><span lang=EN-US><a href="mailto:infoMY@GLOCO.com"><span
style='font-size:12.0pt;line-height:115%'>infoMY@GLOCO.com</span></a></span><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>. We will
retain your information for as long as your account with the Services is active
and as needed to provide you the Services. We shall not retain such information
for longer than is required for the purposes for which the information may
lawfully be used or is otherwise required under any other law for the time
being in force. After a period of time, your data may be anonymized and aggregated,
and then may be held by us as long as necessary for us to provide our Services
effectively, but our use of the anonymized data will be solely for analytic
purposes. Please note that your withdrawal of consent, or cancellation of
account may result in GLOCO being unable to provide you with its Services or to
terminate any existing relationship GLOCO may have with you.</span></p>

<p class=MsoNormal style='margin-left:128.55pt;text-align:justify;text-justify:
inter-ideograph;line-height:115%'><span lang=EN-US style='font-size:12.0pt;
line-height:115%;color:#333333'>3.1.8 If you wish to opt-out of receiving
non-essential communications such as promotional and marketing-related
information regarding the Services, please send us an email at </span><span
lang=EN-US><a href="mailto:infoMY@GLOCO.com"><span style='font-size:12.0pt;
line-height:115%'>infoMY@GLOCO.com</span></a></span><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>.</span></p>

<p class=MsoNormal style='margin-left:128.55pt;text-align:justify;text-justify:
inter-ideograph;line-height:115%'><span lang=EN-US style='font-size:12.0pt;
line-height:115%;color:#333333'>3.1.9 GLOCO may require the User to pay with a
credit card, wire transfer, debit card or cheque for Services for which
subscription amount(s) is/are payable. GLOCO will collect such User’s credit
card number and/or other financial institution information such as bank account
numbers and will use that information for the billing and payment processes,
including but not limited to the use and disclosure of such credit card number
and information to third parties as necessary to complete such billing
operation. Verification of credit information, however, is accomplished solely
by the User through the authentication process. User’s credit-card/debit card
details are transacted upon secure sites of approved payment gateways which are
digitally under encryption, thereby providing the highest possible degree of
care as per current technology. However, GLOCO provides you an option not to
save your payment details. User is advised, however, that internet technology
is not full proof safe and User should exercise discretion on using the same.</span></p>

<p class=MsoNormal style='margin-left:128.55pt;text-align:justify;text-justify:
inter-ideograph;line-height:115%'><span lang=EN-US style='font-size:12.0pt;
line-height:115%;color:#333333'>3.1.10 Due to the communications standards on
the Internet, when a User or the End-User or anyone who visits the Website, GLOCO
automatically receives the URL of the site from which anyone visits. GLOCO also
receives the Internet Protocol (IP) address of each User’s computer (or the
proxy server a User used to access the World Wide Web), User’s computer
operating system and type of web browser the User is using, email patterns, as
well as the name of User’s ISP. This information is used to analyze overall
trends to help GLOCO improve its Service. The linkage between User’s IP address
and User’s personally identifiable information is not shared with or disclosed
to third parties. Notwithstanding the above, GLOCO may share and/or disclose
some of the aggregate findings (not the specific data) in anonymized form
(i.e., non-personally identifiable) with advertisers, sponsors, investors,
strategic partners, and others in order to help grow its business.</span></p>

<p class=MsoNormal style='margin-left:128.55pt;text-align:justify;text-justify:
inter-ideograph;line-height:115%'><span lang=EN-US style='font-size:12.0pt;
line-height:115%;color:#333333'>3.1.11 The Website uses temporary cookies to
store certain (that is not sensitive personal data or information) that is used
by GLOCO and its service providers for the technical administration of the
Website, research and development, and for User administration. In the course
of serving advertisements or optimizing services to its Users, GLOCO may allow
authorized third parties to place or recognize a unique cookie on the User’s
browser. The cookies however, do not store any Personal Information of the
User. You may adjust your internet browser to disable cookies. If cookies are
disabled you may still use the Website, but the Website may be limited in the
use of some of the features.</span></p>

<p class=MsoNormal style='margin-left:128.55pt;text-align:justify;text-justify:
inter-ideograph;line-height:115%'><span lang=EN-US style='font-size:12.0pt;
line-height:115%;color:#333333'>3.1.12 A User may have limited access to the
Website without creating an account on the Website. Unregistered Users can make
appointments with the doctors by providing their name and phone number. In
order to have access to all the features and benefits on our Website, a User
must first create an account on our Website. To create an account, a User is
required to provide the following information, which such User recognizes and
expressly acknowledges is Personal Information allowing others, including GLOCO,
to identify the User: name, User ID, email address, country, ZIP/postal code,
age, phone number, password chosen by the User and valid financial account
information. Other information requested on the registration page, including
the ability to receive promotional offers from GLOCO, is optional. GLOCO may,
in future, include other optional requests for information from the User to
help GLOCO to customize the Website to deliver personalized information to the
User.</span></p>

<p class=MsoNormal style='margin-left:128.55pt;text-align:justify;text-justify:
inter-ideograph;line-height:115%'><span lang=EN-US style='font-size:12.0pt;
line-height:115%;color:#333333'>3.1.13 This Privacy Policy applies to Services
that are owned and operated by GLOCO. GLOCO does not exercise control over the
sites displayed as search results or links from within its Services. These
other sites may place their own cookies or other files on the Users’ computer,
collect data or solicit personal information from the Users, for which GLOCO is
not responsible or liable. Accordingly, GLOCO does not make any representations
concerning the privacy practices or policies of such third parties or terms of
use of such websites, nor does GLOCO guarantee the accuracy, integrity, or
quality of the information, data, text, software, sound, photographs, graphics,
videos, messages or other materials available on such websites. The inclusion
or exclusion does not imply any endorsement by GLOCO of the website, the
website's provider, or the information on the website. If you decide to visit a
third party website linked to the Website, you do this entirely at your own
risk. GLOCO encourages the User to read the privacy policies of that website.</span></p>

<p class=MsoNormal style='margin-left:128.55pt;text-align:justify;text-justify:
inter-ideograph;line-height:115%'><span lang=EN-US style='font-size:12.0pt;
line-height:115%;color:#333333'>3.1.14 The Website may enable User to
communicate with other Users or to post information to be accessed by others,
whereupon other Users may collect such data. Such Users, including any
moderators or administrators, are not authorized GLOCO representatives or
agents, and their opinions or statements do not necessarily reflect those of GLOCO,
and they are not authorized to bind GLOCO to any contract. GLOCO hereby
expressly disclaims any liability for any reliance or misuse of such
information that is made available by Users or visitors in such a manner.</span></p>

<p class=MsoNormal style='margin-left:128.55pt;text-align:justify;text-justify:
inter-ideograph;line-height:115%'><span lang=EN-US style='font-size:12.0pt;
line-height:115%;color:#333333'>3.1.15 GLOCO does not collect information about
the visitors of the Website from other sources, such as public records or
bodies, or private organisations, save and except for the purposes of
registration of the Users (the collection, use, storage and disclosure of which
each End User must agree to under the Terms of Use in order for GLOCO to
effectively render the Services).</span></p>

<p class=MsoNormal style='margin-left:128.55pt;text-align:justify;text-justify:
inter-ideograph;line-height:115%'><span lang=EN-US style='font-size:12.0pt;
line-height:115%;color:#333333'>3.1.16 GLOCO maintains a strict
&quot;No-Spam&quot; policy, which means that GLOCO does not intend to sell,
rent or otherwise give your e-mail address to a third party without your consent.</span></p>

<p class=MsoNormal style='margin-left:128.55pt;text-align:justify;text-justify:
inter-ideograph;line-height:115%'><span lang=EN-US style='font-size:12.0pt;
line-height:115%;color:#333333'>3.1.17 GLOCO has implemented best international
market practices and security policies, rules and technical measures to protect
the personal data that it has under its control from unauthorised access,
improper use or disclosure, unauthorised modification and unlawful destruction
or accidental loss. However, for any data loss or theft due to unauthorized
access to the User’s electronic devices through which the User avails the
Services, GLOCO shall not be held liable for any loss whatsoever incurred by
the User.</span></p>

<p class=MsoNormal style='margin-left:128.55pt;text-align:justify;text-justify:
inter-ideograph;line-height:115%'><span lang=EN-US style='font-size:12.0pt;
line-height:115%;color:#333333'>3.1.18 GLOCO implements reasonable security
practices and procedures and has a comprehensive documented information
security programme and information security policies that contain managerial,
technical, operational and physical security control measures that are
commensurate with respect to the information being collected and the nature of GLOCO’s
business.</span></p>

<p class=MsoNormal style='margin-left:128.55pt;text-align:justify;text-justify:
inter-ideograph;line-height:115%'><span lang=EN-US style='font-size:12.0pt;
line-height:115%;color:#333333'>3.1.19 GLOCO takes your right to privacy very
seriously and other than as specifically stated in this Privacy Policy, will
only disclose your Personal Information in the event it is required to do so by
law, rule, regulation, law enforcement agency, governmental official, legal
authority or similar requirements or when GLOCO, in its sole discretion, deems
it necessary in order to protect its rights or the rights of others, to prevent
harm to persons or property, to fight fraud and credit risk, or to enforce or
apply the Terms of Use.</span></p>

<p class=MsoListParagraphCxSpFirst style='text-align:justify;text-justify:inter-ideograph;
line-height:115%'><b><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-indent:-18.0pt;
line-height:115%'><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>3.2<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>PRACTITIONERS
NOTE:</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>This section applies to
all Practitioners.</span></p>

<p class=MsoListParagraphCxSpLast style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><b><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>&nbsp;</span></b></p>

<p class=MsoNormal style='margin-left:128.55pt;text-align:justify;text-justify:
inter-ideograph;line-height:115%'><span lang=EN-US style='font-size:12.0pt;
line-height:115%;color:#333333'>3.2.1 As part of the registration as well as
the application creation and submission process that is available to
Practitioners on GLOCO, certain information, including Personal Information or
Sensitive Personal Data or Information is collected from the Practitioners.</span></p>

<p class=MsoNormal style='margin-left:128.55pt;text-align:justify;text-justify:
inter-ideograph;line-height:115%'><span lang=EN-US style='font-size:12.0pt;
line-height:115%;color:#333333'>3.2.2 All the statements in this Privacy Policy
apply to all Practitioners, and all Practitioners are therefore required to
read and understand the privacy statements set out herein prior to submitting
any Personal Information or Sensitive Personal Data or Information to GLOCO,
failing which they are required to leave the Services, including the Website
immediately.</span></p>

<p class=MsoNormal style='margin-left:128.55pt;text-align:justify;text-justify:
inter-ideograph;line-height:115%'><span lang=EN-US style='font-size:12.0pt;
line-height:115%;color:#333333'>3.2.3 Practitioners’ personally identifiable
information, which they choose to provide to GLOCO, is used to help the
Practitioners describe and identify themselves. This information is exclusively
owned by GLOCO You will be the owner of your information and you consent to GLOCO
collecting, using, processing and/or disclosing this information for the
purposes hereinafter stated. GLOCO may use such information for commercial
purposes and in an aggregated or non-personally identifiable form for research,
statistical analysis and business intelligence purposes, and may sell or
otherwise transfer such research, statistical or intelligence data in an
aggregated or non-personally identifiable form to third parties and affiliates.
GLOCO also reserves the right to use information provided by or about the
Practitioner for the following purposes:</span></p>

<ul style='margin-top:0cm' type=disc>
 <ol style='margin-top:0cm' start=1 type=i>
  <ul style='margin-top:0cm' type=square>
   <ol style='margin-top:0cm' start=1 type=i>
    <li class=MsoNormal style='color:#333333;text-align:justify;text-justify:
        inter-ideograph;line-height:115%'><span lang=EN-US style='font-size:
        12.0pt;line-height:115%'>Publishing such information on the Website.</span></li>
   </ol>
  </ul>
 </ol>
</ul>

<p class=MsoNormal style='margin-left:145.7pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-145.7pt;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>ii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>Contacting Practitioners for offering new products or services subject
to the telephone number registered with the DNC Registry.</span></p>

<p class=MsoNormal style='margin-left:145.7pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-145.7pt;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>iii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>Contacting Practitioners for taking product feedback.</span></p>

<p class=MsoNormal style='margin-left:145.7pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-145.7pt;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>iv.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>Analyzing software usage patterns for improving product design
and utility.</span></p>

<p class=MsoNormal style='margin-left:145.7pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-145.7pt;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>v.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>Analyzing anonymized practice information including financial,
and inventory information for commercial use.</span></p>

<p class=MsoNormal style='margin-left:128.55pt;text-align:justify;text-justify:
inter-ideograph;line-height:115%'><span lang=EN-US style='font-size:12.0pt;
line-height:115%;color:#333333'>3.2.4 GLOCO automatically enables the listing
of Practitioners’ information on its Website for every ‘Doctor’ or ‘Clinic’
added to a Practice using its software. The Practitioner information listed on
Website is displayed when End-Users search for doctors on Website, and the
Practitioner information listed on Website is used by End-Users to request for
doctor appointments. Any personally identifiable information of the
Practitioners listed on the Website is not generated by GLOCO and is provided
to GLOCO by Practitioners who wish to enlist themselves on the Website, or is
collected by GLOCO from the public domain. GLOCO displays such information on
its Website on an as-is basis making no representation or warranty on the
accuracy or completeness of the information. As such, we strongly encourage
Practitioners to check the accuracy and completeness of their information from
time to time, and inform us immediately of any discrepancies, changes or
updates to such information. GLOCO will, however, take reasonable steps to
ensure the accuracy and completeness of this information.</span></p>

<p class=MsoNormal style='margin-left:128.55pt;text-align:justify;text-justify:
inter-ideograph;line-height:115%'><span lang=EN-US style='font-size:12.0pt;
line-height:115%;color:#333333'>3.2.5 GLOCO may also display information for
Practitioners who have not signed up or registered for the Services, provided
that the Practitioners have consented to GLOCO collecting, processing and/or
disclosing their information on the Website. Such Practitioners are verified by
GLOCO or its associates, and GLOCO makes every effort to capture accurate
information for such Practitioners. However, GLOCO does not undertake any
liability for any incorrect or incomplete information appearing on the Website
for such Practitioners.</span></p>

<p class=MsoListParagraphCxSpFirst style='text-align:justify;text-justify:inter-ideograph;
line-height:115%'><b><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-indent:-18.0pt;
line-height:115%'><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>3.3<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>END-USERS
NOTE:</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>This section applies to
all End-Users.</span></p>

<p class=MsoListParagraphCxSpLast style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><b><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>&nbsp;</span></b></p>

<p class=MsoNormal style='margin-left:128.55pt;text-align:justify;text-justify:
inter-ideograph;line-height:115%'><span lang=EN-US style='font-size:12.0pt;
line-height:115%;color:#333333'>3.3.1 As part of the registration/application
creation and submission process that is available to End-Users on this Website,
certain information, including Personal Information or Sensitive Personal Data
or Information is collected from the End-Users.</span></p>

<p class=MsoNormal style='margin-left:128.55pt;text-align:justify;text-justify:
inter-ideograph;line-height:115%'><span lang=EN-US style='font-size:12.0pt;
line-height:115%;color:#333333'>3.3.2 All the statements in this Privacy Policy
apply to all End-Users, and all End-Users are therefore required to read and
understand the privacy statements set out herein prior to submitting any
Personal Information or Sensitive Personal Data or Information to GLOCO,
failing which they are required to leave the GLOCO immediately.</span></p>

<p class=MsoNormal style='margin-left:128.55pt;text-align:justify;text-justify:
inter-ideograph;line-height:115%'><span lang=EN-US style='font-size:12.0pt;
line-height:115%;color:#333333'>3.3.3 If you have inadvertently submitted any
such information to GLOCO prior to reading the privacy statements set out
herein, and you do not agree with the manner in which such information is
collected, processed, stored, used or disclosed, then you may access, modify
and delete such information by using options provided on the Website. In
addition, you can, by sending an email to </span><span lang=EN-US><a
href="mailto:infoMY@GLOCO.com"><span style='font-size:12.0pt;line-height:115%'>infoMY@GLOCO.com</span></a></span><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>, inquire
whether GLOCO is in possession of your personal data, and you may also require GLOCO
to delete and destroy all such information.</span></p>

<p class=MsoNormal style='margin-left:128.55pt;text-align:justify;text-justify:
inter-ideograph;line-height:115%'><span lang=EN-US style='font-size:12.0pt;
line-height:115%;color:#333333'>3.3.4 End-Users’ personally identifiable
information, which they choose to provide on the Website is used to help the
End-Users describe/identify themselves. Other information that does not
personally identify the End-Users as an individual, is collected by GLOCO from
End-Users (such as, patterns of utilization described above) and is exclusively
owned by GLOCO. GLOCO may also use such information in an aggregated or
non-personally identifiable form for research, statistical analysis and
business intelligence purposes, and may sell or otherwise transfer such
research, statistical or intelligence data in an aggregated or non-personally
identifiable form to third parties and affiliates. In particular, GLOCO
reserves with it the right to use anonymized End-User demographics information
and anonymized End-User health information for the following purposes:</span></p>

<ul style='margin-top:0cm' type=disc>
 <ol style='margin-top:0cm' start=1 type=i>
  <ul style='margin-top:0cm' type=square>
   <ol style='margin-top:0cm' start=1 type=i>
    <li class=MsoNormal style='color:#333333;text-align:justify;text-justify:
        inter-ideograph;line-height:115%'><span lang=EN-US style='font-size:
        12.0pt;line-height:115%'>Analyzing software usage patterns for improving
        product design and utility.</span></li>
   </ol>
  </ul>
 </ol>
</ul>

<p class=MsoNormal style='margin-left:145.7pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-145.7pt;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>ii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>Analyzing such information for research and development of new
technologies.</span></p>

<p class=MsoNormal style='margin-left:145.7pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-145.7pt;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>iii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>Using analysis of such information in other commercial product
offerings of GLOCO.</span></p>

<p class=MsoNormal style='margin-left:145.7pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-145.7pt;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>iv.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>Sharing analysis of such information with third parties for commercial
use.</span></p>

<p class=MsoNormal style='margin-left:128.55pt;text-align:justify;text-justify:
inter-ideograph;line-height:115%'><span lang=EN-US style='font-size:12.0pt;
line-height:115%;color:#333333'>3.3.5 GLOCO will communicate with the End-Users
through email, phone and notices posted on the Website or through other means
available through the service, including text and other forms of messaging. The
End-Users can change their e-mail and contact preferences at any time by
logging into their &quot;Account&quot; at </span><span lang=EN-US><a
href="http://www.ghealth.com"><span style='font-size:12.0pt;line-height:115%'>www.ghealth.com</span></a></span><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'> and
changing the account settings.</span></p>

<p class=MsoNormal style='margin-left:128.55pt;text-align:justify;text-justify:
inter-ideograph;line-height:115%'><span lang=EN-US style='font-size:12.0pt;
line-height:115%;color:#333333'>3.3.6 At times, GLOCO conducts a User survey to
collect information about End-Users' preferences. These surveys are optional
and if End-Users choose to respond, their responses will be kept anonymous.
Similarly, GLOCO may offer contests to qualifying End-Users in which we ask for
contact and demographic information such as name, email address and mailing
address. The demographic information that GLOCO collects in the registration
process and through surveys is used to help GLOCO improve its Services to meet
the needs and preferences of End-Users.</span></p>

<p class=MsoNormal style='margin-left:128.55pt;text-align:justify;text-justify:
inter-ideograph;line-height:115%'><span lang=EN-US style='font-size:12.0pt;
line-height:115%;color:#333333'>3.3.7 GLOCO may keep records of electronic
communications and telephone calls received and made for making appointments or
other purposes for the purpose of administration of Services, customer support,
research and development and for better listing of Practitioners.</span></p>

<p class=MsoNormal style='margin-left:128.55pt;text-align:justify;text-justify:
inter-ideograph;line-height:115%'><span lang=EN-US style='font-size:12.0pt;
line-height:115%;color:#333333'>3.3.8 All GLOCO employees and data processors,
who have access to, and are associated with the processing of sensitive
personal data or information, are obliged to respect the confidentiality of
every End-Users’ Personal Information or Sensitive Personal Data and
Information. GLOCO has put in place procedures and technologies as per good
industry practices and in accordance with the applicable laws, to maintain
security of all personal data from the point of collection to the point of
destruction. Any third-party data processor to which GLOCO transfers Personal
Data shall have to agree to comply with those procedures and policies, or put
in place adequate measures on their own.</span></p>

<p class=MsoNormal style='margin-left:128.55pt;text-align:justify;text-justify:
inter-ideograph;line-height:115%'><span lang=EN-US style='font-size:12.0pt;
line-height:115%;color:#333333'>3.3.9 GLOCO may also disclose or transfer End Users’
personal and other information provided by a User, to a third party as part of
reorganization or a sale of the assets of a GLOCO corporation division or
company. Any third party to which GLOCO transfers or sells its assets to will
have the right to continue to use the personal and other information that
End-Users provide to us, in accordance with the Terms of Use</span></p>

<p class=MsoNormal style='margin-left:128.55pt;text-align:justify;text-justify:
inter-ideograph;line-height:115%'><span lang=EN-US style='font-size:12.0pt;
line-height:115%;color:#333333'>3.3.10 To the extent necessary to provide
End-Users with the Services, GLOCO may provide their Personal Information to
third party contractors who work on behalf of or with GLOCO to provide
End-Users with such Services, to help GLOCO communicate with End-Users or to
maintain the Website. Generally these contractors do not have any independent
right to share this information, however certain contractors who provide
services on the Website, including the providers of online communications
services, may use and disclose the personal information collected in connection
with the provision of these Services in accordance with their own privacy
policies. In such circumstances, you consent to us disclosing your Personal
Information to contractors, solely for the intended purposes only.</span></p>

<p class=MsoListParagraphCxSpFirst style='text-align:justify;text-justify:inter-ideograph;
line-height:115%'><b><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-indent:-18.0pt;
line-height:115%'><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>3.4<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>CASUAL
VISITORS NOTE:</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>This section applies to
all Casual Visitors.</span></p>

<p class=MsoListParagraphCxSpLast style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><b><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>&nbsp;</span></b></p>

<p class=MsoNormal style='margin-left:128.55pt;text-align:justify;text-justify:
inter-ideograph;line-height:115%'><span lang=EN-US style='font-size:12.0pt;
line-height:115%;color:#333333'>3.4.1 No sensitive personal data or information
is automatically collected by GLOCO from any casual visitors of this website,
who are merely perusing the Website.</span></p>

<p class=MsoNormal style='margin-left:128.55pt;text-align:justify;text-justify:
inter-ideograph;line-height:115%'><span lang=EN-US style='font-size:12.0pt;
line-height:115%;color:#333333'>3.4.2 Nevertheless, certain provisions of this
Privacy Policy are applicable to even such casual visitors, and such casual
visitors are also required to read and understand the privacy statements set
out herein, failing which they are required to leave this Website immediately.</span></p>

<p class=MsoNormal style='margin-left:128.55pt;text-align:justify;text-justify:
inter-ideograph;line-height:115%'><span lang=EN-US style='font-size:12.0pt;
line-height:115%;color:#333333'>3.4.3 If you, as a casual visitor, have
inadvertently browsed any other page of this Website prior to reading the
privacy statements set out herein, and you do not agree with the manner in
which such information is obtained, collected, processed, stored, used,
disclosed or retained, merely quitting this browser application should
ordinarily clear all temporary cookies installed by GLOCO. All visitors,
however, are encouraged to use the “clear cookies” functionality of their
browsers to ensure such clearing / deletion, as GLOCO cannot guarantee, predict
or provide for the behaviour of the equipment of all the visitors of the
Website.</span></p>

<p class=MsoNormal style='margin-left:128.55pt;text-align:justify;text-justify:
inter-ideograph;line-height:115%'><span lang=EN-US style='font-size:12.0pt;
line-height:115%;color:#333333'>3.4.4 You are not a casual visitor if you have
willingly submitted any personal data or information to GLOCO through any
means, including email, post or through the registration process on the
Website. All such visitors will be deemed to be, and will be treated as, Users
for the purposes of this Privacy Policy, and in which case, all the statements
in this Privacy Policy apply to such persons.</span></p>

<p class=MsoListParagraphCxSpFirst style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span style='font-size:12.0pt;
line-height:115%;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt;line-height:115%'><b><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>4.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></b><b><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>CONFIDENTIALITY
AND SECURITY</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph;line-height:115%'><b><span lang=EN-US style='font-size:12.0pt;
line-height:115%;color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>4.1<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>Your Personal
Information is maintained by GLOCO in electronic form on its equipment, and on
the equipment of its employees. Such information may also be converted to
physical form from time to time. GLOCO takes all necessary precautions to
protect your personal information both online and off-line, and implements
reasonable security practices and measures including certain managerial,
technical, operational and physical security control measures that are
commensurate with respect to the information being collected and the nature of GLOCO’s
business.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><b><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>4.2<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>No administrator at GLOCO
will have knowledge of your password. It is important for you to protect
against unauthorized access to your password, your computer and your mobile
phone. Be sure to log off from the Website when finished. GLOCO does not
undertake any liability for any unauthorised use of your account and password.
If you suspect any unauthorized use of your account, you must immediately
notify GLOCO by sending an email to </span><span lang=EN-US><a
href="mailto:infoMY@GLOCO.com"><span style='font-size:12.0pt;line-height:115%'>infoMY@GLOCO.com</span></a></span><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>. You shall
be liable to indemnify GLOCO due to any loss suffered by it due to such
unauthorized use of your account and password.</span></p>

<p class=MsoListParagraphCxSpMiddle style='line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>4.3<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>GLOCO makes all User
information accessible to its employees, agents or partners and third parties
only on a need-to-know basis, and binds only its employees to strict
confidentiality obligations.</span></p>

<p class=MsoListParagraphCxSpMiddle style='line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>4.4<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>Part of the
functionality of GLOCO is assisting the doctors to maintain and organise such
information. GLOCO may, therefore, retain and submit all such records to the
appropriate authorities, or to doctors who request access to such information.</span></p>

<p class=MsoListParagraphCxSpMiddle style='line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>4.5<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>Part of the
functionality of the GLOCO is assisting the patients to access information
relating to them. GLOCO may, therefore, retain and submit all such records to
the relevant patients, or to their doctors.</span></p>

<p class=MsoListParagraphCxSpMiddle style='line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>4.6<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>Notwithstanding the
above, GLOCO is not responsible for the confidentiality, security or
distribution of your Personal Information by our partners and third parties
outside the scope of our agreement with such partners and third parties.
Further, GLOCO shall not be responsible for any breach of security or for any
actions of any third parties or events that are beyond the reasonable control
of GLOCO including but not limited to, acts of government, computer hacking,
unauthorised access to computer data and storage device, computer crashes,
breach of security and encryption, poor quality of Internet service or
telephone service of the User etc.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span style='font-size:12.0pt;
line-height:115%;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt;line-height:115%'><b><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>5.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></b><b><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>HOW WE USE
COOKIES</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph;line-height:115%'><b><span lang=EN-US style='font-size:12.0pt;
line-height:115%;color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>5.1<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>&quot;Cookies&quot; is
information placed on an individual's computer hard drive to enable the
individual to more easily communicate and interact with a website. We may use
cookies to customise your experience on our website. Cookies help us to monitor
how many times a user has visited a particular website and identify which pages
have been accessed. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>A cookie does not give
us access to your computer or any information about you, other than the data
you choose to share with us. If you prefer you may decline any cookies from
www.ghealth.com and the mobile application ‘gHealth’ through the management of
your web browser. This may prevent you from taking full advantage of the
website.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span style='font-size:12.0pt;
line-height:115%;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt;line-height:115%'><b><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>6.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></b><b><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>INTERNET
PROTOCOL ADDRESS</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph;line-height:115%'><b><span lang=EN-US style='font-size:12.0pt;
line-height:115%;color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>6.1<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>An internet protocol
address is a number that is assigned to your computer when you use the
internet. This information does not in itself contain any personally
identifiable information. However your internet protocol address may be used to
help identify you or more specifically your computer during a particular
session and to gather broad demographic data. Please be aware that your
internet protocol address may be collected to assist us in identifying problems
with our server and administering the website.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt;line-height:115%'><b><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>7.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></b><b><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>SHARING OF
YOUR PERSONAL INFORMATION</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph;line-height:115%'><b><span lang=EN-US style='font-size:12.0pt;
line-height:115%;color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>7.1<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>When using your
information for the purpose for which it was collected, we may need to disclose
it within our group of companies and to our related and/or associated
companies, licensees, business partners and/or service providers, whether
within or outside Malaysia. When this happens we will require such third
parties with whom we share your information with to handle your information
using the same level of care that we apply. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>We may use your
personal information to send you promotional information about third parties
which we think you may find interesting. If you have previously agreed to us
using your personal information for direct marketing purposes, you may change
your mind at any time by writing to or emailing us. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:90.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-90.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'><span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>i.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>Analyzing software usage patterns for improving product design
and utility.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:90.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-90.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'><span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>ii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>Analyzing such information for research and development of new
technologies.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:90.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-90.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'><span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>iii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>Using analysis of such information in other commercial product
offerings of GLOCO or its business partners and/or associates.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:90.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-90.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'><span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>iv.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>Sharing analysis of such information with third parties for
commercial use.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt;line-height:115%'><b><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>8.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></b><b><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>LINKS TO
OTHER WEBSITES</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph;line-height:115%'><b><span lang=EN-US style='font-size:12.0pt;
line-height:115%;color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>8.1<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>Please keep in mind
that www.ghealth.com and the mobile application ‘gHealth’ may contain links or
references to other websites outside of our control. Once you have used these
links to leave our website, you should note that we do not have any control
over that other website. Therefore, we shall not be responsible for the
protection and privacy of any information which you provide whilst visiting
such websites and this privacy statement does not govern such websites. You are
required to exercise caution and look at the privacy statement applicable to
the website in question.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-18.0pt;line-height:
115%'><b><span lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>9.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></b><b><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>CHANGE TO PRIVACY
POLICY</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph;line-height:115%'><b><span lang=EN-US style='font-size:12.0pt;
line-height:115%;color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>9.1<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>GLOCO may update this
Privacy Policy at any time, with or without advance notice. In the event there
are significant changes in the way GLOCO treats User’s personally identifiable
information, or in the Privacy Policy itself, GLOCO will display a notice on
the Website or send Users an email, as provided for above, so that you may
review the changed terms prior to continuing to use the Services. As always, if
you object to any of the changes to our terms, and you no longer wish to use
the Services, you may contact </span><span lang=EN-US><a
href="mailto:infoMY@GLOCO.com"><span style='font-size:12.0pt;line-height:115%'>infoMY@GLOCO.com</span></a></span><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'> to
deactivate your account. Unless stated otherwise, GLOCO’s current Privacy
Policy applies to all information that GLOCO has about you and your account.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>If a User uses the Services
or accesses the Website after a notice of changes has been sent to such User or
published on the Website, such User hereby provides his/her/its consent to the
changed terms.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-18.0pt;line-height:
115%'><b><span lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>10.<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span></b><b><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>CHILDREN'S
AND MINOR'S PRIVACY</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph;line-height:115%'><b><span lang=EN-US style='font-size:12.0pt;
line-height:115%;color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>10.1<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>GLOCO strongly encourages parents and guardians to supervise the
online activities of their minor children and consider using parental control
tools available from online services and software manufacturers to help provide
a child-friendly online environment. These tools also can prevent minors from
disclosing their name, address, and other personally identifiable information
online without parental permission. Although the GLOCO Website and Services are
not intended for use by minors, GLOCO respects the privacy of minors who may
inadvertently use the internet or the mobile application.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-18.0pt;line-height:
115%'><b><span lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>11.<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span></b><b><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>CONSENT TO
THIS POLICY</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph;line-height:115%'><b><span lang=EN-US style='font-size:12.0pt;
line-height:115%;color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>11.1<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>You acknowledge that this Privacy Policy is a part of the Terms
of Use of the Website and the other Services, and you unconditionally agree
that becoming a User of the Website and its Services signifies your (i) assent
to this Privacy Policy, and (ii) consent to GLOCO using, collecting, processing
and/or disclosing your Personal Information in the manner and for the purposes
set out in this Privacy Policy. Your visit to the Website and use of the
Services is subject to this Privacy Policy and the Terms of Use.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-18.0pt;line-height:
115%'><b><span lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>12.<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span></b><b><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>CONTACTING
US</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph;line-height:115%'><b><span lang=EN-US style='font-size:12.0pt;
line-height:115%;color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>12.1<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>For any inquiries or complaint or to request access and/or
correction of the personal data or relating to this Privacy Policy and the
Terms of Use or otherwise relating to any use of your personal data by us, you
may contact us at the following contact details.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>Mailing Address: </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><b><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>GLOCO MALAYSIA SDN. BHD</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>A-G-13A &amp; A-01-13A,
Block A, Merchant Square, </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>No.1, Jalan Tropicana
Selatan 1,</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>PJU 3, 47410 Petaling
Jaya, </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>Selangor Darul Ehsan,
Malaysia.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>Email : </span><span
lang=EN-US><a href="mailto:infoMY@gloco.com"><span style='font-size:12.0pt;
line-height:115%'>infoMY@gloco.com</span></a></span><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'> </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>Tel : 1300 88 6008</span></p>

<p class=MsoListParagraphCxSpLast style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>Fax : 60 - 378830990</span></p>

<p class=MsoNormal style='line-height:115%'><span lang=EN-US style='font-size:
12.0pt;line-height:115%'>&nbsp;</span></p>

</div>

</body>

</html>
