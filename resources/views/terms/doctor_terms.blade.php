<html>

<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name=Generator content="Microsoft Word 12 (filtered)">
<style>
<!--
 /* Font Definitions */
 @font-face
	{font-family:Wingdings;
	panose-1:5 0 0 0 0 0 0 0 0 0;}
@font-face
	{font-family:SimSun;
	panose-1:2 1 6 0 3 1 1 1 1 1;}
@font-face
	{font-family:Mangal;
	panose-1:2 4 5 3 5 2 3 3 2 2;}
@font-face
	{font-family:"Cambria Math";
	panose-1:2 4 5 3 5 4 6 3 2 4;}
@font-face
	{font-family:Cambria;
	panose-1:2 4 5 3 5 4 6 3 2 4;}
@font-face
	{font-family:Tahoma;
	panose-1:2 11 6 4 3 5 4 4 2 4;}
@font-face
	{font-family:"Microsoft YaHei";
	panose-1:2 11 5 3 2 2 4 2 2 4;}
@font-face
	{font-family:"\@SimSun";
	panose-1:2 1 6 0 3 1 1 1 1 1;}
@font-face
	{font-family:"\@Microsoft YaHei";
	panose-1:2 11 5 3 2 2 4 2 2 4;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin:0cm;
	margin-bottom:.0001pt;
	font-size:10.0pt;
	font-family:"Times New Roman","serif";}
h1
	{mso-style-link:"Heading 1 Char";
	margin-top:0cm;
	margin-right:0cm;
	margin-bottom:0cm;
	margin-left:21.6pt;
	margin-bottom:.0001pt;
	text-align:center;
	text-indent:-21.6pt;
	page-break-after:avoid;
	font-size:10.0pt;
	font-family:"Arial","sans-serif";
	font-weight:bold;
	text-decoration:underline;}
h2
	{mso-style-link:"Heading 2 Char";
	margin-top:12.0pt;
	margin-right:0cm;
	margin-bottom:3.0pt;
	margin-left:28.8pt;
	text-indent:-28.8pt;
	page-break-after:avoid;
	font-size:14.0pt;
	font-family:"Arial","sans-serif";
	font-weight:bold;
	font-style:italic;}
h4
	{mso-style-link:"Heading 4 Char";
	margin-top:0cm;
	margin-right:0cm;
	margin-bottom:0cm;
	margin-left:43.2pt;
	margin-bottom:.0001pt;
	text-indent:-43.2pt;
	page-break-after:avoid;
	font-size:12.0pt;
	font-family:"Times New Roman","serif";
	font-weight:normal;
	text-decoration:underline;}
h5
	{mso-style-link:"Heading 5 Char";
	margin-right:0cm;
	margin-left:0cm;
	font-size:10.0pt;
	font-family:"Times New Roman","serif";
	font-weight:bold;}
p.MsoCommentText, li.MsoCommentText, div.MsoCommentText
	{mso-style-link:"Comment Text Char";
	margin:0cm;
	margin-bottom:.0001pt;
	font-size:10.0pt;
	font-family:"Times New Roman","serif";}
p.MsoCaption, li.MsoCaption, div.MsoCaption
	{margin-top:6.0pt;
	margin-right:0cm;
	margin-bottom:6.0pt;
	margin-left:0cm;
	font-size:12.0pt;
	font-family:"Times New Roman","serif";
	font-style:italic;}
p.MsoTitle, li.MsoTitle, div.MsoTitle
	{mso-style-link:"Title Char";
	margin:0cm;
	margin-bottom:.0001pt;
	text-align:center;
	font-size:20.0pt;
	font-family:"Times New Roman","serif";
	font-weight:bold;}
p.MsoBodyText, li.MsoBodyText, div.MsoBodyText
	{mso-style-link:"Body Text Char";
	margin-top:0cm;
	margin-right:0cm;
	margin-bottom:6.0pt;
	margin-left:0cm;
	font-size:10.0pt;
	font-family:"Times New Roman","serif";}
p.MsoSubtitle, li.MsoSubtitle, div.MsoSubtitle
	{mso-style-link:"Subtitle Char";
	margin-top:12.0pt;
	margin-right:0cm;
	margin-bottom:6.0pt;
	margin-left:0cm;
	text-align:center;
	page-break-after:avoid;
	font-size:14.0pt;
	font-family:"Arial","sans-serif";
	font-style:italic;}
a:link, span.MsoHyperlink
	{color:blue;
	text-decoration:underline;}
a:visited, span.MsoHyperlinkFollowed
	{color:purple;
	text-decoration:underline;}
p
	{margin-right:0cm;
	margin-left:0cm;
	font-size:12.0pt;
	font-family:"Times New Roman","serif";}
p.MsoCommentSubject, li.MsoCommentSubject, div.MsoCommentSubject
	{mso-style-link:"Comment Subject Char";
	margin:0cm;
	margin-bottom:.0001pt;
	font-size:10.0pt;
	font-family:"Times New Roman","serif";
	font-weight:bold;}
p.MsoAcetate, li.MsoAcetate, div.MsoAcetate
	{mso-style-link:"Balloon Text Char";
	margin:0cm;
	margin-bottom:.0001pt;
	font-size:8.0pt;
	font-family:"Tahoma","sans-serif";}
p.MsoListParagraph, li.MsoListParagraph, div.MsoListParagraph
	{margin-top:0cm;
	margin-right:0cm;
	margin-bottom:0cm;
	margin-left:36.0pt;
	margin-bottom:.0001pt;
	font-size:10.0pt;
	font-family:"Times New Roman","serif";}
p.MsoListParagraphCxSpFirst, li.MsoListParagraphCxSpFirst, div.MsoListParagraphCxSpFirst
	{margin-top:0cm;
	margin-right:0cm;
	margin-bottom:0cm;
	margin-left:36.0pt;
	margin-bottom:.0001pt;
	font-size:10.0pt;
	font-family:"Times New Roman","serif";}
p.MsoListParagraphCxSpMiddle, li.MsoListParagraphCxSpMiddle, div.MsoListParagraphCxSpMiddle
	{margin-top:0cm;
	margin-right:0cm;
	margin-bottom:0cm;
	margin-left:36.0pt;
	margin-bottom:.0001pt;
	font-size:10.0pt;
	font-family:"Times New Roman","serif";}
p.MsoListParagraphCxSpLast, li.MsoListParagraphCxSpLast, div.MsoListParagraphCxSpLast
	{margin-top:0cm;
	margin-right:0cm;
	margin-bottom:0cm;
	margin-left:36.0pt;
	margin-bottom:.0001pt;
	font-size:10.0pt;
	font-family:"Times New Roman","serif";}
span.Heading1Char
	{mso-style-name:"Heading 1 Char";
	mso-style-link:"Heading 1";
	font-family:"Arial","sans-serif";
	font-weight:bold;
	text-decoration:underline;}
span.Heading2Char
	{mso-style-name:"Heading 2 Char";
	mso-style-link:"Heading 2";
	font-family:"Arial","sans-serif";
	font-weight:bold;
	font-style:italic;}
span.Heading4Char
	{mso-style-name:"Heading 4 Char";
	mso-style-link:"Heading 4";
	text-decoration:underline;}
span.TitleChar
	{mso-style-name:"Title Char";
	mso-style-link:Title;
	font-weight:bold;}
span.SubtitleChar
	{mso-style-name:"Subtitle Char";
	mso-style-link:Subtitle;
	font-family:"Arial","sans-serif";
	font-style:italic;}
span.BodyTextChar
	{mso-style-name:"Body Text Char";
	mso-style-link:"Body Text";}
span.Heading5Char
	{mso-style-name:"Heading 5 Char";
	mso-style-link:"Heading 5";
	font-family:"Times New Roman","serif";
	font-weight:bold;}
span.item-no
	{mso-style-name:item-no;}
span.apple-converted-space
	{mso-style-name:apple-converted-space;}
span.CommentTextChar
	{mso-style-name:"Comment Text Char";
	mso-style-link:"Comment Text";}
span.CommentSubjectChar
	{mso-style-name:"Comment Subject Char";
	mso-style-link:"Comment Subject";
	font-weight:bold;}
span.BalloonTextChar
	{mso-style-name:"Balloon Text Char";
	mso-style-link:"Balloon Text";
	font-family:"Tahoma","sans-serif";}
p.gmail-msolistparagraph, li.gmail-msolistparagraph, div.gmail-msolistparagraph
	{mso-style-name:gmail-msolistparagraph;
	margin-right:0cm;
	margin-left:0cm;
	font-size:12.0pt;
	font-family:"Times New Roman","serif";}
.MsoChpDefault
	{font-size:10.0pt;}
@page WordSection1
	{size:612.0pt 792.0pt;
	margin:72.0pt 72.0pt 72.0pt 72.0pt;}
div.WordSection1
	{page:WordSection1;}
 /* List Definitions */
 ol
	{margin-bottom:0cm;}
ul
	{margin-bottom:0cm;}
-->
</style>

</head>

<body lang=EN-MY link=blue vlink=purple>

<div class=WordSection1>

<p class=MsoNormal style='margin-bottom:6.0pt;text-align:justify;text-justify:
inter-ideograph'><span lang=EN-US style='font-size:12.0pt;color:#333333'>GLOCO
MALAYSIA SDN. BHD. together with its parent and sister companies (“us”, “we”,
or “<b>GLOCO</b>”, which also includes its affiliates) is the author and
publisher of the internet resource www.ghealth.com (“<b>Website</b>”) on the
world wide web as well as the software and applications provided by GLOCO,
including but not limited to the mobile application ‘gHealth’, the software and
applications of the brand names ‘gHealth’ and GLOCO’s software and applications
(together with the Website, referred to as the “<b>Services</b>”).</span></p>

<p class=MsoNormal style='margin-bottom:6.0pt;text-align:justify;text-justify:
inter-ideograph'><span lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpFirst style='margin-bottom:6.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt'><b><span lang=EN-US
style='font-size:12.0pt;color:#333333'>1.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></b><b><span lang=EN-US style='font-size:12.0pt;color:#333333'>NATURE
AND APPLICABILITY OF TERMS</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:6.0pt;text-align:justify;
text-justify:inter-ideograph'><b><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpLast style='margin-bottom:6.0pt;text-align:justify;
text-justify:inter-ideograph'><span lang=EN-US style='font-size:12.0pt;
color:#333333'>Please carefully go through these terms and conditions&nbsp;<b>(“Terms”)</b>&nbsp;and
the privacy policy available at </span><span lang=EN-US><a
href="http://www.ghealth.com"><span style='font-size:12.0pt'>http://www.ghealth.com</span></a></span><span
lang=EN-US style='font-size:12.0pt;color:#333333'> <b>(“Privacy Policy”)</b>&nbsp;before
you decide to access the Website or avail the services made available on the
Website by GLOCO. These Terms and the Privacy Policy together constitute a
legal agreement&nbsp;<b>(“Agreement”)</b>&nbsp;between you and GLOCO in
connection with your visit to the Website and your use of the Services (as
defined below).</span></p>

<p class=gmail-msolistparagraph style='margin-top:5.0pt;margin-right:0cm;
margin-bottom:6.0pt;margin-left:36.0pt;text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='color:#333333'>The Agreement applies to you whether you are
a&nbsp;medical practitioner or health care provider (whether an individual
professional or an organization) or similar institution wishing to be listed,
or already listed, on the Website, including designated, authorized associates
of such practitioners or institutions <b>(“Practitioner(s)”, “you&quot;,
&quot;Users&quot;).</b></span></p>

<p class=MsoListParagraphCxSpFirst style='margin-bottom:6.0pt;text-align:justify;
text-justify:inter-ideograph'><span lang=EN-US style='font-size:12.0pt;
color:#333333'>This Agreement applies to those services made available by GLOCO
on the Website, which are offered to the Users&nbsp;<b>(“Services”)</b>,
including, <b><i>but not limited to,</i></b> the listing of Practitioners and
their profiles and contact details, to be made available to the other Users and
visitors to the Website</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:6.0pt;text-align:justify;
text-justify:inter-ideograph'><span lang=EN-US style='color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:6.0pt;text-align:justify;
text-justify:inter-ideograph'><span lang=EN-US style='font-size:12.0pt;
color:#333333'>The Services may change <a name="_GoBack"></a>from time to time,
at the sole discretion of GLOCO, and the Agreement will apply to your visit to
and your use of the Website to avail the Service, as well as to all information
provided by you on the Website at any given point in time.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:6.0pt;text-align:justify;
text-justify:inter-ideograph'><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:6.0pt;text-align:justify;
text-justify:inter-ideograph'><span lang=EN-US style='font-size:12.0pt;
color:#333333'>This Agreement defines the terms and conditions under which you
are allowed to use the Website and describes the manner in which we shall treat
your account while you are registered as a member with us. If you have any
questions about any part of the Agreement, feel free to contact us at </span><span
lang=EN-US><a href="mailto:infoMY@gloco.com"><span style='font-size:12.0pt'>infoMY@gloco.com</span></a></span><span
lang=EN-US style='font-size:12.0pt;color:#333333'>. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:6.0pt;text-align:justify;
text-justify:inter-ideograph'><span lang=EN-GB style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:6.0pt;text-align:justify;
text-justify:inter-ideograph'><span lang=EN-GB style='font-size:12.0pt;
color:#333333'>This Agreement shall supersede and cancel all previous
agreements either in written form or orally between the parties in respect of
this Website, the mobile application ‘gHealth’ and GLOCO’s software and
applications.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:6.0pt;text-align:justify;
text-justify:inter-ideograph'><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:6.0pt;text-align:justify;
text-justify:inter-ideograph'><span lang=EN-US style='font-size:12.0pt;
color:#333333'>By downloading or accessing the Website to use the Services, you
irrevocably accept all the conditions stipulated in this Agreement, the&nbsp;</span><span
lang=EN-US><a href="https://www.ghealth.com/doctor/subscribers" target="_blank"><span
style='font-size:12.0pt;color:#1EAEDB'>Subscription Terms of Service</span></a></span><span
lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;and&nbsp;</span><span
lang=EN-US><a href="https://ghealth.com/privacy" target="_blank"><span
style='font-size:12.0pt;color:#1EAEDB'>Privacy Policy</span></a></span><span
lang=EN-US style='font-size:12.0pt;color:#333333'>, as available on the
Website, and agree to abide by them. This Agreement supersedes all previous
oral and written terms and conditions (if any) communicated to you relating to
your use of the Website to avail the Services. By availing any Service, you
signify your acceptance of the terms of this Agreement.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:6.0pt;text-align:justify;
text-justify:inter-ideograph'><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:6.0pt;text-align:justify;
text-justify:inter-ideograph'><span lang=EN-US style='font-size:12.0pt;
color:#333333'>We reserve the right to modify or terminate any portion of the
Agreement for any reason and at any time, and such modifications shall be
informed to you in writing. You should read the Agreement at regular intervals.
Your use of the Website following any such modification constitutes your
agreement to follow and be bound by the Agreement so modified.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:6.0pt;text-align:justify;
text-justify:inter-ideograph'><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:6.0pt;text-align:justify;
text-justify:inter-ideograph'><span lang=EN-US style='font-size:12.0pt;
color:#333333'>You acknowledge that you will be bound by this Agreement for
availing any of the Services offered by us. If you do not agree with any part
of the Agreement, please do not use the Website or avail any Services.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:6.0pt;text-align:justify;
text-justify:inter-ideograph'><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:6.0pt;text-align:justify;
text-justify:inter-ideograph'><span lang=EN-US style='font-size:12.0pt;
color:#333333'>Your access to use of the Website and the Services will be solely
at the discretion of GLOCO.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:6.0pt;text-align:justify;
text-justify:inter-ideograph'><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpLast style='margin-bottom:6.0pt;text-align:justify;
text-justify:inter-ideograph'><span lang=EN-US style='font-size:12.0pt;
color:#333333'>The Agreement is published in compliance of, and is governed by
the provisions of Malaysian law, including but not limited to:</span></p>

<p class=MsoNormal style='margin-left:68.6pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-68.6pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>i.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>The
Contract Act 1950;</span></p>

<p class=MsoNormal style='margin-left:68.6pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-68.6pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>ii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>The
Copyright Act 1987;</span></p>

<p class=MsoNormal style='margin-left:68.6pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-68.6pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>iii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>The Personal
Data Protection Act 2010; and</span></p>

<p class=MsoNormal style='margin-left:68.6pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-68.6pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>iv.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>The
rules, regulations, guidelines and clarifications framed there under.</span></p>

<p class=MsoListParagraphCxSpFirst style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:36.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-18.0pt'><b><span lang=EN-US style='font-size:12.0pt;color:#333333'>2.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></b><b><span
lang=EN-US style='font-size:12.0pt;color:#333333'>CONDITIONS OF USE</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:36.0pt;text-align:justify;text-justify:inter-ideograph'><b><span
lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:36.0pt;text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;color:#333333'>You must be 18 years of age
or older to register, use the Services, or visit or use the Website in any
manner. By registering, visiting and using the Website or accepting this
Agreement, you represent and warrant to GLOCO that you are 18 years of age or
older, and that you have the right, authority and capacity to use the Website
and the Services available through the Website, and agree to and abide by this
Agreement.</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>            </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:36.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-18.0pt'><b><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></b><b><span
lang=EN-US style='font-size:12.0pt;color:#333333'>TERMS OF USE PRACTITIONERS</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:36.0pt;text-align:justify;text-justify:inter-ideograph'><b><span
lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:36.0pt;text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;color:#333333'>The terms in this Clause 3
are applicable only to Practitioners.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:36.0pt;text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:54.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-18.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.1<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;color:#333333'>LISTING POLICY</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:54.0pt;text-align:justify;text-justify:inter-ideograph'><b><span
lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.1.1<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>GLOCO,
directly and indirectly, collects information regarding the Practitioners’
profiles, contact details, and practice. GLOCO reserves the right to take down
any Practitioner’s profile as well as the right to display the profile of the
Practitioners, with or without notice to the concerned Practitioner. This
information is collected for the purpose of facilitating interaction with the
End-Users and other Users. If any information displayed on the Website in
connection with you and your profile is found to be incorrect, you are required
to inform GLOCO immediately to enable GLOCO to make the necessary amendments.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph'><b><span
lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.1.2<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>GLOCO
shall not be liable and responsible for the ranking of the Practitioners on
external websites and search engines.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph'><span lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.1.3<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>GLOCO
shall not be responsible or liable in any manner to the Users for any losses,
damage, injuries or expenses incurred by the Users as a result of any
disclosures or publications made by GLOCO, where the User has expressly or
implicitly consented to the making of disclosures or publications by GLOCO. If
the User had revoked such consent under the terms of the Privacy Policy, then GLOCO
shall not be responsible or liable in any manner to the User for any losses,
damage, injuries or expenses incurred by the User as a result of any
disclosures made by GLOCO prior to its actual receipt of such revocation.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph'><span lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.1.4<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>GLOCO
reserves the right to moderate the suggestions made by the Practitioners
through feedback and the right to remove any abusive or inappropriate or
promotional content added on the Website. However, GLOCO shall not be liable if
any inactive, inaccurate, fraudulent, or non- existent profiles of
Practitioners are added to the Website.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph'><span lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.1.5<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>Practitioners
explicitly agree that GLOCO reserves the right to publish the Content provided
by Practitioners to a third party including content platforms.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph'><span lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.1.6<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>When you
are listed on </span><span lang=EN-US><a href="http://www.ghealth.com"><span
style='font-size:12.0pt'>www.ghealth.com</span></a></span><span lang=EN-US
style='font-size:12.0pt;color:#333333'>, End-Users may see a ‘show number’
option. When End-Users choose this option, they choose to call your number
through a free telephony service provided by GLOCO, and the records of such
calls are recorded and stored in GLOCO’s servers. Such records are dealt with
only in accordance with the terms of the Privacy Policy. Such call facility
provided to End-Users and to you by GLOCO should be used only for appointment
and booking purposes, and not for consultation on health-related issues. GLOCO
accepts no liability if the call facility is not used in accordance with the
foregoing.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph'><span lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.1.7<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>You as a
Practitioner hereby represent and warrant that you will use the Services in
accordance with applicable law. Any contravention of applicable law as a result
of your use of these Services is your sole responsibility, and GLOCO accepts no
liability for the same.</span></p>

<p class=MsoListParagraphCxSpMiddle><b><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph'><b><span
lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:54.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-18.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.2<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;color:#333333'>PROFILE OWNERSHIP AND EDITING RIGHTS</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:54.0pt;text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:54.0pt;text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;color:#333333'>GLOCO ensures easy access to
the Practitioners by providing a tool to update your profile information. GLOCO
reserves the right of ownership of all the Practitioner’s profile and
photographs and to moderate the changes or updates requested by Practitioners.
However, GLOCO takes the independent decision whether to publish or reject the
requests submitted for the respective changes or updates. You hereby represent
and warrant that you are fully entitled under law to upload all content
uploaded by you as part of your profile or otherwise while using GLOCO’s
services, and that no such content breaches any third party rights, including
intellectual property rights. Upon becoming aware of a breach of the foregoing
representation, GLOCO may modify or delete parts of your profile information at
its sole discretion with or without notice to you.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:36.0pt;text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:54.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-18.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.3<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;color:#333333'>REVIEWS AND FEEDBACK DISPLAY RIGHTS OF
GLOCO</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:54.0pt;text-align:justify;text-justify:inter-ideograph'><b><span
lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.3.1<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>All
Critical Content is content created by the Users of </span><span lang=EN-US><a
href="http://www.ghealth.com"><span style='font-size:12.0pt'>www.ghealth.com</span></a></span><span
lang=EN-US style='font-size:12.0pt;color:#333333'> and “gHealth” <b>(“Website”)</b>&nbsp;and
the clients of GLOCO customers and Practitioners, including the End-Users. As a
platform, GLOCO does not take responsibility for Critical Content. The role of GLOCO
and other legal rights and obligations relating to the Critical Content are
further detailed in Clauses 3.9 and 5 of these Terms. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph'><b><span
lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.3.2<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>GLOCO
reserves the right to collect feedback and Critical Content for all the
Practitioners, Clinics and Healthcare Providers listed on the Website.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph'><span lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.3.3<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>GLOCO
shall have no obligation to pre-screen, review, flag, filter, modify, refuse or
remove any or all Critical Content from any Service, except as required by
applicable law.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph'><span lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.3.4<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>You
understand that by using the Services you may be exposed to Critical Content or
other content that you may find offensive or objectionable. GLOCO shall not be
liable for any effect on Practitioner’s business due to Critical Content of a
negative nature. In these respects, you may use the Service at your own risk. GLOCO
however, as an ‘intermediary, takes steps as required to comply with applicable
law as regards the publication of Critical Content. The legal rights and
obligations with respect to Critical Content and any other information sought
to be published by Users are further detailed in Clause 4 of these Terms.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph'><span lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.3.5<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>GLOCO will
take down information under standards consistent with applicable law, and shall
in no circumstances be liable or responsible for Critical Content, which has
been created by the Users. The principles set out in relation to third party
content in the terms of Service for the Website shall be applicable mutatis
mutandis in relation to Critical Content posted on the Website.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph'><span lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.3.6<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>If GLOCO
determines that you have provided inaccurate information or enabled fraudulent
feedback, GLOCO reserves the right to immediately suspend any of your accounts
with GLOCO and makes such declaration on the website alongside your name/your
clinics name as determined by GLOCO for the protection of its business and in
the interests of Users.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:36.0pt;text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:54.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-18.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.4<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;color:#333333'>RELEVANCE ALGORITHM</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:54.0pt;text-align:justify;text-justify:inter-ideograph'><b><span
lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:54.0pt;text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;color:#333333'>GLOCO has designed the
relevance algorithm in the best interest of the End-User and may adjust the
relevance algorithm from time to time to improve the quality of the results
given to the patients. It is a pure merit driven, proprietary algorithm which
cannot be altered for specific Practitioners. GLOCO shall not be liable for any
effect on the Practitioner’s business interests due to the change in the
Relevance Algorithm.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:36.0pt;text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:54.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-18.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.5<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;color:#333333'>INDEPENDENT SERVICES</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:54.0pt;text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:54.0pt;text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;color:#333333'>Your use of each Service
confers upon you only the rights and obligations relating to such Service, and
not to any other service that may be provided by GLOCO.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:36.0pt;text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:54.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-18.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.6<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;color:#333333'>GLOCO REACH RIGHTS</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:54.0pt;text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:54.0pt;text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;color:#333333'>GLOCO reserves the rights to
display sponsored ads on the Website. These ads would be marked as “Sponsored
ads”. Without prejudice to the status of other content, GLOCO will not be
liable for the accuracy of information or the claims made in the Sponsored ads.
GLOCO does not encourage the Users to visit the Sponsored ads page or to avail
any services from them. GLOCO will not be liable for the services of the
providers of the Sponsored ads.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:54.0pt;text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:54.0pt;text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;color:#333333'>You represent and warrant
that you will use these Services in accordance with applicable law. Any
contravention of applicable law as a result of your use of these Services is
your sole responsibility, and GLOCO accepts no liability for the same.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:36.0pt;text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:54.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-18.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.7<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;color:#333333'>gHealth CONSULTATION</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:54.0pt;text-align:justify;text-justify:inter-ideograph'><b><span
lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.7.1<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>gHealth
Consultation enables Practitioners to connect with the Users by responding to
the health related queries posted by them on their Health Accounts using the
special software application provided to Practitioners by GLOCO&nbsp;<b>(“Doctor
App”)</b>.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph'><b><span
lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.7.2<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>The User
may post two types of queries on their Health Accounts (i) public query, which
is sent through gHealth Consultation to multiple Practitioners and enables any
of the chosen Practitioner to respond to the query posted; and (ii) private
query, which will be only sent to the Practitioner that the User has chosen.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph'><span lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.7.3<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>The
public queries posted by the Users are sent to multiple Practitioners through a
fully automated system installed in the Doctor App. The automated system
chooses Practitioners based on inter alia the information furnished by each
Practitioner, the number of questions answered by the Practitioner and the
rating or reviews procured by the Practitioner. GLOCO does not guarantee in any
manner that any query will be directed to a particular Practitioner and will
not be responsible for the Practitioners chosen to respond to the queries.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph'><span lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.7.4<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>Practitioner
agrees that, when providing any written response to a User’s query that
constitutes a performance of his/her services, the Practitioner shall not post
language that may be considered abusive, objectionable or demeaning to the User
or in general.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph'><span lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.7.5<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>In the
event, the Practitioner indicates as part of his response the pre-consultation
fee payable for the advice solicited through the private query, GLOCO shall
provide the User an option to directly remit the amount to the Practitioner
through the Health Account and subsequently deduct a portion from the amount
paid as its fee for the performance of the services.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph'><span lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.7.6<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>GLOCO
shall remit the fees to the Practitioner in accordance with the terms agreed
between the Practitioners and GLOCO in the Software License and/or Services
Agreement executed between them.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph'><span lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.7.7<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>GLOCO
reserves the right to revise the fee terms at any time at their discretion. The
Practitioner’s continued use of the services and Doctor App shall constitute
his/her consent to such revision.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph'><span lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.7.8<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>Practitioner
hereby agrees that it shall use the Doctor App for the purpose specified in
these Terms of Use and shall not use the Doctor App for any unauthorized and unlawful
purpose, including impersonating another person.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph'><span lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpLast style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.7.9<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>Practitioner
hereby represents and warrants that he/she</span></p>

<p class=MsoNormal style='margin-left:171.5pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-171.5pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>i.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>is
qualified to provide medical services within the territory of Malaysia;</span></p>

<p class=MsoNormal style='margin-left:171.5pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-171.5pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>ii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>has
obtained all licenses as required by law to provide medical services and has
not committed any act or omission that might prejudice its continuance or
renewal; and</span></p>

<p class=MsoNormal style='margin-left:171.5pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-171.5pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>iii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>has
provided GLOCO true, accurate, complete and up to date details about their
qualification and credentials.</span></p>

<p class=MsoListParagraphCxSpFirst style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.7.10<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><span
lang=EN-US style='font-size:12.0pt;color:#333333'>Practitioner agrees that
he/she shall at all times abide by the applicable medical regulations including
the code of conduct.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph'><b><span
lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.7.11<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><span
lang=EN-US style='font-size:12.0pt;color:#333333'>Practitioners shall promptly
renew their licenses required to provide medical services and notify GLOCO
about the same.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph'><span lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpLast style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.7.12<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><span
lang=EN-US style='font-size:12.0pt;color:#333333'>GLOCO reserves the right to
terminate any account of the Practitioner in case:</span></p>

<p class=MsoNormal style='margin-left:171.5pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-171.5pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>iv.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>the
Practitioner breaches any terms and conditions of this terms of use or privacy
policy or applicable laws&#894;</span></p>

<p class=MsoNormal style='margin-left:171.5pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-171.5pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>v.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>GLOCO is
unable to verify or authenticate any information provided to it by a
Practitioner&#894; or</span></p>

<p class=MsoNormal style='margin-left:171.5pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-171.5pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>vi.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>GLOCO in
its sole and absolute discretion believes that actions of the Practitioner may
cause legal liability for GLOCO or other Users and / or may adversely affect the
services rendered by GLOCO.</span></p>

<p class=MsoListParagraphCxSpFirst style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.7.13<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><span
lang=EN-US style='font-size:12.0pt;color:#333333'>Practitioner hereby agrees
that, for any User that contacts the Practitioner using gHealth Consultation,
only he/she shall be allowed to perform the services for the User and that the
Practitioner may under no circumstances be permitted to transfer the
performance of Your Services to any other person, whether under their
supervision or not. The Practitioner accepts all responsibility and liability
for the use of gHealth Consultation, including the performance of its services,
by any other party claiming to be the Practitioner and hereby agrees to
indemnify GLOCO against any claim or loss that may be faced by GLOCO consequent
to such use.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph'><b><span
lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.7.14<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><span
lang=EN-US style='font-size:12.0pt;color:#333333'>Practitioner hereby agrees to
hold in strictest confidence all information provided by a User to him/her
under all circumstances. Practitioner agrees that he/she shall not disclose any
information or documentation provided by a User to any other person, nor shall
he/she allow, by act or omission, such information or documentation to be
acquired by any other person.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph'><span lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.7.15<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><span
lang=EN-US style='font-size:12.0pt;color:#333333'>Practitioner agrees to render
his/her services and fulfill their obligations towards their patients using
their best efforts, skill and ability.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph'><span lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.7.16<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><span
lang=EN-US style='font-size:12.0pt;color:#333333'>Practitioner agrees and
understands that some or all his/her may be made available to the general
public through the Doctor App or otherwise by the Company and that he/she has
no objection to the same.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph'><span lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.7.17<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><span
lang=EN-US style='font-size:12.0pt;color:#333333'>Practitioner hereby agrees to
assign to GLOCO in perpetuity all intellectual property rights residing in the
responses provided by him/her for use by GLOCO worldwide.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph'><span lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.7.18<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><span
lang=EN-US style='font-size:12.0pt;color:#333333'>Practitioner hereby agrees
not to seek the contact details of any User or to contact any User except
through the Doctor App.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph'><span lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.7.19<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><span
lang=EN-US style='font-size:12.0pt;color:#333333'>Any communication sent by or
through GLOCO to the Practitioner is based solely on information uploaded by
the Users. GLOCO shall not be responsible for the incompleteness or inaccuracy
such information, including if as a result of such inaccuracy, a communication
is sent to an unintended recipient.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph'><span lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.7.20<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><span
lang=EN-US style='font-size:12.0pt;color:#333333'>Practitioner shall be liable
to indemnify and hold GLOCO harmless from and against all actions, claims,
damages, losses and expenses, including court costs and reasonable attorneys’
fees, arising out of or resulting from any breach, default, contravention,
non-observance, non-performance, improper performance of any of its obligations
or the terms, conditions, covenants and provisions contained in this Terms of
Use.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:36.0pt;text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:54.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-18.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.8<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;color:#333333'>ghealth.com/gHealth FEED</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:54.0pt;text-align:justify;text-justify:inter-ideograph'><b><span
lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.8.1<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>ghealth.com/gHealth
feed is an online content platform available on the website, wherein
Practitioners who have a GLOCO profile and Users who have a health account can
login and post health and wellness related content.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph'><b><span
lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.8.2<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>A
Practitioner can use health feed by logging in from their profile, creating
original content comprising text, audio, video, images data or any combination
of the same (“as defined in Clause 3.7.2”), and uploading said Content to GLOCO’s
servers. The Practitioner can upload their own images or choose an image from
the gallery that GLOCO provides. GLOCO shall post such Content ghealth.com/gHealth
feed at its own option and subject to these Terms and Conditions. The Content
uploaded via ghealth.com/gHealth feed does not constitute medical advice and
may not be construed as such by any person.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph'><span lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.8.3<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>The
Practitioner acknowledges that they are the original authors and creators of
any Content or comments uploaded by them via ghealth.com/gHealth feed and that
no Content or comment uploaded by them would constitute infringement of the
intellectual property rights of any other person. GLOCO reserves the right to
remove any Content or comment which it may determine at its own discretion as
violating the intellectual property rights of any other person. The
Practitioner agrees to absolve GLOCO from and indemnify GLOCO against all
claims that may arise as a result of any third party intellectual property
right claim that may arise from the Practitioner’s uploading of any Content on ghealth.com/gHealth
feed. The Practitioner also agrees to absolve GLOCO from and indemnify GLOCO
against all claims that may arise as a result of any third party intellectual
property claim if the Practitioner downloads an image from GLOCO’s gallery and
utilizes it for his/her personal or commercial gain.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph'><span lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.8.4<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>The
Practitioner hereby assigns to GLOCO, in perpetuity and worldwide, all
intellectual property rights in any Content or comment created by the
Practitioner and uploaded by the Practitioner via ghealth.com/gHealth feed.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph'><span lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.8.5<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>GLOCO
shall have the right to edit or remove the Content and any comments in such
manner as it may deem fit at any time.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph'><span lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.8.6<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>The
Practitioner may also use ghealth.com/gHealth feed in order to view original
content created by Users or other Practitioners and also create and upload
comments on such Content including their own content where allowed.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph'><span lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.8.7<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>Practitioner
acknowledges that the content on ghealth.com/gHealth feed reflects the views
and opinions of the authors of such content and does not necessarily reflect GLOCO’s
views.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph'><span lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.8.8<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>Practitioner
agrees not to post any comments or upload any Content which are defamatory,
obscene, objectionable or in nature and GLOCO reserves the right to remove any
comments which it may determine at its own discretion to violate these Terms
and Conditions or be violative of any law or statute in force at the time. The
Practitioner agrees to absolve GLOCO from and indemnify GLOCO against all
claims that may arise as a result of any legal claim arising from the nature of
the Content or the comments posted by the Practitioner on ghealth.com/gHealth feed.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:36.0pt;text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:54.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-18.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.9<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;color:#333333'>Further Terms for Practitioners</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:54.0pt;text-align:justify;text-justify:inter-ideograph'><b><span
lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.9.1<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>The
Practitioner shall reply to the patient after receiving patient’s
communication. In case of non-compliance with regard to reverting/ adhering to
the applicable laws/rules/regulations/guidelines by the Practitioner, GLOCO has
the right to show other available Practitioners to the patient or remove
Practitioners from the platform/GLOCO application/site.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph'><b><span
lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.9.2<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>The
Practitioner understands and agrees that, GLOCO shall at its sole discretion,
at any time be entitled to, show other Practitioners available for
consultation.</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.9.3<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>The
Practitioner further understands that, there is a responsibility on the
Practitioner to treat the patients on this model, as the Practitioner would have
otherwise treated the patient on a physical one-on-one consultation model.</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.9.4<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>The
Practitioner has the discretion to cancel any consultation at any point in time
in cases where the Practitioner feels, it is beyond his/her expertise or
his/her capacity to treat the patient. In such cases, patient has the option of
choosing other Practitioners.</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.9.5<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>The
Practitioner shall at all times ensure that all the applicable laws that govern
the Practitioner shall be followed and utmost care shall be taken in terms of
the consultation being rendered.</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.9.6<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>The
Practitioner acknowledges that should GLOCO find the Practitioner to be in
violation of any of the applicable laws/rules/ regulations/guidelines set out
by the authorities then GLOCO shall be entitled to cancel the consultation with
such patient or take such other legal action as may be required.</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.9.7<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>It is
further understood by the Practitioner that the information that is disclosed
by the patient at the time of consultation, shall be confidential in nature and
subject to patient and Practitioner privilege.</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.9.8<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>The
Practitioner shall ensure patient’s consent is obtained prior to uploading the
prescription/health records of the patient on the account of the patient during
such consultation.</span></p>

<p class=MsoListParagraphCxSpMiddle><b><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.9.9<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>The
Practitioner agrees that GLOCO may impose the following fees and charges for
the Services which may be amended by GLOCO at any time in its sole discretion
subject to three (3) months’ notification being made to the Practitioner:-</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:126.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-126.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'><span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>i.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>gHealth
Features/gHealth Consultation: 20% on the consultation fee charged by the
Practitioners to Users;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:126.0pt;text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:126.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-126.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'><span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>ii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>Appointment
booking and interaction with Practitioners: 20% on the appointment fee or
booking fee or interaction fee charged by the Practitioners to Users.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt'><b><span lang=EN-US style='font-size:12.0pt;
color:#333333'>4.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></b><b><span lang=EN-US style='font-size:12.0pt;color:#333333'>RIGHTS
AND OBLIGATIONS RELATING TO CONTENT</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:36.0pt;text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpLast style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:54.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-18.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>4.1<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;color:#333333'>GLOCO hereby informs Practitioners that
they are not permitted to host, display, upload, modify, publish, transmit,
update or share any information that:</span></p>

<p class=MsoNormal style='margin-left:120.05pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>ii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>is
grossly harmful, harassing, blasphemous, defamatory, obscene, pornographic,
pedophilic, libelous, invasive of another's privacy, hateful, or racially,
ethnically objectionable, disparaging, relating or encouraging money laundering
or gambling, or otherwise unlawful in any manner whatever;</span></p>

<p class=MsoNormal style='margin-left:120.05pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>iii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>harm
minors in any way;</span></p>

<p class=MsoNormal style='margin-left:120.05pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>iv.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>infringes
any patent, trademark, copyright or other proprietary rights;</span></p>

<p class=MsoNormal style='margin-left:120.05pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>v.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>violates
any law for the time being in force;</span></p>

<p class=MsoNormal style='margin-left:120.05pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>vi.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>deceives
or misleads the addressee about the origin of such messages or communicates any
information which is grossly offensive or menacing in nature;</span></p>

<p class=MsoNormal style='margin-left:120.05pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>vii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>impersonate
another person;</span></p>

<p class=MsoNormal style='margin-left:120.05pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>viii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>contains
software viruses or any other computer code, files or programs designed to
interrupt, destroy or limit the functionality of any computer resource;</span></p>

<p class=MsoNormal style='margin-left:120.05pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>ix.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>threatens
the unity, integrity, defence, security or sovereignty of Malaysia, friendly
relations with foreign states, or public order or causes incitement to the
commission of any cognizable offence or prevents investigation of any offence
or is insulting any other nation.</span></p>

<p class=MsoListParagraph style='margin-top:8.55pt;margin-right:0cm;margin-bottom:
8.55pt;margin-left:54.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-18.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>4.2<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;color:#333333'>Practitioners are also prohibited from:</span></p>

<p class=MsoNormal style='margin-left:120.05pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>x.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>violating
or attempting to violate the integrity or security of the Website or any GLOCO
Content;</span></p>

<p class=MsoNormal style='margin-left:120.05pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>xi.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>transmitting
any information (including job posts, messages and hyperlinks) on or through
the Website that is disruptive or competitive to the provision of Services by GLOCO;</span></p>

<p class=MsoNormal style='margin-left:120.05pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>xii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>intentionally
submitting on the Website any incomplete, false or inaccurate information;</span></p>

<p class=MsoNormal style='margin-left:120.05pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>xiii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>making
any unsolicited communications to other Users;</span></p>

<p class=MsoNormal style='margin-left:120.05pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>xiv.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>using any
engine, software, tool, agent or other device or mechanism (such as spiders,
robots, avatars or intelligent agents) to navigate or search the Website;</span></p>

<p class=MsoNormal style='margin-left:120.05pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>xv.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>attempting
to decipher, decompile, disassemble or reverse engineer any part of the
Website;</span></p>

<p class=MsoNormal style='margin-left:120.05pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>xvi.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>copying
or duplicating in any manner any of the GLOCO Content or other information
available from the Website;</span></p>

<p class=MsoNormal style='margin-left:120.05pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>xvii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>framing
or hot linking or deep linking any GLOCO Content.</span></p>

<p class=MsoNormal style='margin-left:120.05pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>xviii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>circumventing
or disabling any digital rights management, usage rules, or other security
features of the Software.</span></p>

<p class=MsoListParagraphCxSpFirst style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:54.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-18.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>4.3<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;color:#333333'>GLOCO, upon obtaining knowledge by
itself or been brought to actual knowledge by an affected person in writing or
through email signed with electronic signature about any such information as
mentioned above, shall be entitled to disable such information that is in
contravention of Clauses 4.1 and 4.2. GLOCO shall also be entitled to preserve
such information and associated records for at least 90 (ninety) days for
production to governmental authorities for investigation purposes.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:54.0pt;text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:54.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-18.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>4.4<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;color:#333333'>In case of non-compliance with any
applicable laws, rules or regulations, or the Agreement (including the Privacy
Policy) by a Practitioner, GLOCO has the right to immediately terminate the
access or usage rights of the Practitioner to the Website and Services and to
remove non-compliant information from the Website.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph'><span lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:54.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-18.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>4.5<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;color:#333333'>GLOCO may disclose or transfer
User-generated information to its affiliates or governmental authorities in
such manner as permitted or required by applicable law, and you hereby consent
to such transfer. The applicable law only permits GLOCO to transfer sensitive
personal data or information including any information, to any other body
corporate or a person in Malaysia, or located in any other country, that
ensures the same level of data protection that is adhered to by GLOCO as provided
for under the applicable law, only if such transfer is necessary for the
performance of the lawful contract between GLOCO or any person on its behalf
and the Practitioner or where the Practitioner has consented to data transfer.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph'><span lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpLast style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:54.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-18.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>4.6<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;color:#333333'>GLOCO respects the intellectual property
rights of others and we do not hold any responsibility for any violations of
any intellectual property rights</span></p>

<p class=MsoNormal style='margin-top:8.55pt;margin-right:0cm;margin-bottom:
8.55pt;margin-left:0cm;text-align:justify;text-justify:inter-ideograph'><b><span
lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpFirst style='text-align:justify;text-justify:inter-ideograph;
text-indent:-18.0pt'><b><span lang=EN-US style='font-size:12.0pt;color:#333333'>5.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></b><b><span
lang=EN-US style='font-size:12.0pt;color:#333333'>TERMINATION</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:36.0pt;text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpLast style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:54.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-18.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>5.1<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;color:#333333'>GLOCO reserves the right to suspend or
terminate a Practitioner’s access to the Website and the Services with or
without notice and to exercise any other remedy available under law, in cases
where,</span></p>

<p class=MsoNormal style='margin-left:120.05pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>xix.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>Such Practitioner
breaches any terms and conditions of the Agreement;</span></p>

<p class=MsoNormal style='margin-left:120.05pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>xx.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>A third
party reports violation of any of its right as a result of your use of the Services;</span></p>

<p class=MsoNormal style='margin-left:120.05pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>xxi.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>GLOCO is
unable to verify or authenticate any information provide to GLOCO by a Practitioner;</span></p>

<p class=MsoNormal style='margin-left:120.05pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>xxii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>GLOCO has
reasonable grounds for suspecting any illegal, fraudulent or abusive activity
on part of such Practitioner; or</span></p>

<p class=MsoNormal style='margin-left:120.05pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>xxiii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>GLOCO
believes in its sole discretion that Practitioner’s actions may cause legal
liability for such Practitioner, other Practitioners or for GLOCO or are
contrary to the interests of the Website.</span></p>

<p class=MsoListParagraphCxSpFirst style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:54.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-18.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>5.2<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;color:#333333'>Once temporarily suspended, indefinitely
suspended or terminated, the Practitioner may not continue to use the Website
under the same account, a different account or re-register under a new account.
On termination of an account due to the reasons mentioned herein, such Practitioner
shall no longer have access to data, messages, files and other material kept on
the Website by such Practitioner. The Practitioner shall ensure that he/she/it
has continuous backup of any medical services the Practitioner has rendered in
order to comply with the Practitioner’s record keeping process and practices.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:54.0pt;text-align:justify;text-justify:inter-ideograph'><b><span
lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:54.0pt;text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt'><b><span lang=EN-US style='font-size:12.0pt;
color:#333333'>6.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></b><b><span lang=EN-US style='font-size:12.0pt;color:#333333'>LIMITATION
OF LIABILITY</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph'><b><span lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpLast style='text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;color:#333333'>In no event, including but
not limited to negligence, shall GLOCO, or any of its directors, officers,
employees, agents or content or service providers (collectively, the “Protected
Entities”) be liable for any direct, indirect, special, incidental,
consequential, exemplary or punitive damages arising from, or directly or
indirectly related to, the use of, or the inability to use, the Website or the
content, materials and functions related thereto, the Services, Practitioner’s
provision of information via the Website, lost business or lost End-Users, even
if such Protected Entity has been advised of the possibility of such damages.
In no event shall the Protected Entities be liable for:</span></p>

<p class=MsoNormal style='margin-left:68.6pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-68.6pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>v.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>provision
of or failure to provide all or any service by Practitioners to End- Users
contacted or managed through the Website;</span></p>

<p class=MsoNormal style='margin-left:68.6pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-68.6pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>vi.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>any
content posted, transmitted, exchanged or received by or on behalf of any Practitioner
or other person on or through the Website;</span></p>

<p class=MsoNormal style='margin-left:68.6pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-68.6pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>vii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>any
unauthorized access to or alteration of your transmissions or data; or</span></p>

<p class=MsoNormal style='margin-left:68.6pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-68.6pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>viii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>any other
matter relating to the Website or the Service.</span></p>

<p class=MsoListParagraphCxSpFirst style='text-align:justify;text-justify:inter-ideograph'><b><span
lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpLast style='text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;color:#333333'>In no event shall the total
aggregate liability of the Protected Entities to a Practitioner for all
damages, losses, and causes of action (whether in contract or tort, including,
but not limited to, negligence or otherwise) arising from this Agreement or a Practitioner’s
use of the Website or the Services exceed, in the aggregate RM100.00 (Ringgit
Malaysia One Hundred Only).</span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpFirst style='text-align:justify;text-justify:inter-ideograph'><b><span
lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt'><b><span lang=EN-US style='font-size:12.0pt;
color:#333333'>7.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></b><b><span lang=EN-US style='font-size:12.0pt;color:#333333'>RETENTION
AND REMOVAL</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph'><b><span lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpLast style='text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;color:#333333'>GLOCO may retain such
information collected from Practitioners from its Website or Services for as
long as necessary, depending on the type of information; purpose, means and
modes of usage of such information; and according to the Personal Data
Protection Act 2010. Computer web server logs may be preserved as long as
administratively necessary.</span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpFirst style='text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt'><b><span lang=EN-US style='font-size:12.0pt;
color:#333333'>8.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></b><b><span lang=EN-US style='font-size:12.0pt;color:#333333'>APPLICABLE
LAW AND DISPUTE SETTLEMENT</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:36.0pt;text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:54.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-18.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>8.1<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;color:#333333'>You agree that this Agreement and any
contractual obligation between GLOCO and Practitioner will be governed by the
laws of Malaysia.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:54.0pt;text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:54.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-18.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>8.2<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;color:#333333'>Any dispute, claim or controversy
arising out of or relating to this Agreement, including the determination of
the scope or applicability of this Agreement to arbitrate, or your use of the
Website or the Services or information to which it gives access, shall be determined
by arbitration in Malaysia, before a sole arbitrator appointed by GLOCO. The
arbitration shall be conducted in the English language and the UNCITRAL
Arbitration Rules shall apply. The award shall be final and binding on the
parties to the dispute.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph'><span lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:54.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-18.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>8.3<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;color:#333333'>Subject to the above Clause 9.2, the
courts at Malaysia shall have exclusive jurisdiction over any disputes arising
out of or in relation to this Agreement, your use of the Website or the
Services or the information to which it gives access.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph'><span lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt'><b><span lang=EN-US style='font-size:12.0pt;
color:#333333'>9.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></b><b><span lang=EN-US style='font-size:12.0pt;color:#333333'>CONTACT
INFORMATION </span></b></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph'><b><span lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph'><span lang=EN-US style='font-size:12.0pt;color:#333333'>If a Practitioner
has any questions concerning GLOCO, the Website, this Agreement, the Services,
or anything related to any of the foregoing, GLOCO customer support can be
reached at the following email address: infoMY@gloco.com or via the contact
information available from </span><span lang=EN-US><a
href="http://www.ghealth.com"><span style='font-size:12.0pt'>www.ghealth.com</span></a></span><span
lang=EN-US style='font-size:12.0pt;color:#333333'>.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:54.0pt;text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph'><span lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt'><b><span lang=EN-US style='font-size:12.0pt;
color:#333333'>10.<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span></b><b><span
lang=EN-US style='font-size:12.0pt;color:#333333'>SEVERABILITY</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph'><b><span lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph'><span lang=EN-US style='font-size:12.0pt;color:#333333'>.If
any provision of the Agreement is held by a court of competent jurisdiction or
arbitral tribunal to be unenforceable under applicable law, then such provision
shall be excluded from this Agreement and the remainder of the Agreement shall
be interpreted as if such provision were so excluded and shall be enforceable
in accordance with its terms; provided however that, in such event, the
Agreement shall be interpreted so as to give effect, to the greatest extent
consistent with and permitted by applicable law, to the meaning and intention
of the excluded provision as determined by such court of competent jurisdiction
or arbitral tribunal.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:54.0pt;text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph'><span lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt'><b><span lang=EN-US style='font-size:12.0pt;
color:#333333'>11.<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span></b><b><span
lang=EN-US style='font-size:12.0pt;color:#333333'>WAIVER</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph'><b><span lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpLast style='text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;color:#333333'>No provision of this
Agreement shall be deemed to be waived and no breach excused, unless such
waiver or consent shall be in writing and signed by GLOCO. Any consent by GLOCO
to, or a waiver by GLOCO of any breach by you, whether expressed or implied,
shall not constitute consent to, waiver of, or excuse for any other different
or subsequent breach.</span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><b><u><span
lang=EN-US style='font-size:12.0pt;color:#333333'><span style='text-decoration:
 none'>&nbsp;</span></span></u></b></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><b><u><span
lang=EN-US style='font-size:12.0pt;color:#333333'><span style='text-decoration:
 none'>&nbsp;</span></span></u></b></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><b><span
lang=EN-US style='font-size:12.0pt;color:#333333'>YOU HAVE READ THESE TERMS OF
USE AND AGREE TO ALL OF THE PROVISIONS CONTAINED ABOVE</span></b></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt'>&nbsp;</span></p>

</div>

</body>

</html>
