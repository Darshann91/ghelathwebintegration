<html>

<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name=Generator content="Microsoft Word 12 (filtered)">
<style>
<!--
 /* Font Definitions */
 @font-face
	{font-family:Wingdings;
	panose-1:5 0 0 0 0 0 0 0 0 0;}
@font-face
	{font-family:SimSun;
	panose-1:2 1 6 0 3 1 1 1 1 1;}
@font-face
	{font-family:Mangal;
	panose-1:2 4 5 3 5 2 3 3 2 2;}
@font-face
	{font-family:"Cambria Math";
	panose-1:2 4 5 3 5 4 6 3 2 4;}
@font-face
	{font-family:"Microsoft YaHei";
	panose-1:2 11 5 3 2 2 4 2 2 4;}
@font-face
	{font-family:"Segoe UI";
	panose-1:2 11 5 2 4 2 4 2 2 3;}
@font-face
	{font-family:"\@SimSun";
	panose-1:2 1 6 0 3 1 1 1 1 1;}
@font-face
	{font-family:"\@Microsoft YaHei";
	panose-1:2 11 5 3 2 2 4 2 2 4;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin:0cm;
	margin-bottom:.0001pt;
	font-size:10.0pt;
	font-family:"Times New Roman","serif";}
h1
	{mso-style-link:"Heading 1 Char";
	margin-top:0cm;
	margin-right:0cm;
	margin-bottom:0cm;
	margin-left:21.6pt;
	margin-bottom:.0001pt;
	text-align:center;
	text-indent:-21.6pt;
	page-break-after:avoid;
	font-size:10.0pt;
	font-family:"Arial","sans-serif";
	font-weight:bold;
	text-decoration:underline;}
h2
	{mso-style-link:"Heading 2 Char";
	margin-top:12.0pt;
	margin-right:0cm;
	margin-bottom:3.0pt;
	margin-left:28.8pt;
	text-indent:-28.8pt;
	page-break-after:avoid;
	font-size:14.0pt;
	font-family:"Arial","sans-serif";
	font-weight:bold;
	font-style:italic;}
h4
	{mso-style-link:"Heading 4 Char";
	margin-top:0cm;
	margin-right:0cm;
	margin-bottom:0cm;
	margin-left:43.2pt;
	margin-bottom:.0001pt;
	text-indent:-43.2pt;
	page-break-after:avoid;
	font-size:12.0pt;
	font-family:"Times New Roman","serif";
	font-weight:normal;
	text-decoration:underline;}
h5
	{mso-style-link:"Heading 5 Char";
	margin-right:0cm;
	margin-left:0cm;
	font-size:10.0pt;
	font-family:"Times New Roman","serif";
	font-weight:bold;}
p.MsoCommentText, li.MsoCommentText, div.MsoCommentText
	{mso-style-link:"Comment Text Char";
	margin:0cm;
	margin-bottom:.0001pt;
	font-size:10.0pt;
	font-family:"Times New Roman","serif";}
p.MsoCaption, li.MsoCaption, div.MsoCaption
	{margin-top:6.0pt;
	margin-right:0cm;
	margin-bottom:6.0pt;
	margin-left:0cm;
	font-size:12.0pt;
	font-family:"Times New Roman","serif";
	font-style:italic;}
p.MsoTitle, li.MsoTitle, div.MsoTitle
	{mso-style-link:"Title Char";
	margin:0cm;
	margin-bottom:.0001pt;
	text-align:center;
	font-size:20.0pt;
	font-family:"Times New Roman","serif";
	font-weight:bold;}
p.MsoBodyText, li.MsoBodyText, div.MsoBodyText
	{mso-style-link:"Body Text Char";
	margin-top:0cm;
	margin-right:0cm;
	margin-bottom:6.0pt;
	margin-left:0cm;
	font-size:10.0pt;
	font-family:"Times New Roman","serif";}
p.MsoSubtitle, li.MsoSubtitle, div.MsoSubtitle
	{mso-style-link:"Subtitle Char";
	margin-top:12.0pt;
	margin-right:0cm;
	margin-bottom:6.0pt;
	margin-left:0cm;
	text-align:center;
	page-break-after:avoid;
	font-size:14.0pt;
	font-family:"Arial","sans-serif";
	font-style:italic;}
a:link, span.MsoHyperlink
	{color:blue;
	text-decoration:underline;}
a:visited, span.MsoHyperlinkFollowed
	{color:purple;
	text-decoration:underline;}
p
	{margin-right:0cm;
	margin-left:0cm;
	font-size:12.0pt;
	font-family:"Times New Roman","serif";}
p.MsoCommentSubject, li.MsoCommentSubject, div.MsoCommentSubject
	{mso-style-link:"Comment Subject Char";
	margin:0cm;
	margin-bottom:.0001pt;
	font-size:10.0pt;
	font-family:"Times New Roman","serif";
	font-weight:bold;}
p.MsoAcetate, li.MsoAcetate, div.MsoAcetate
	{mso-style-link:"Balloon Text Char";
	margin:0cm;
	margin-bottom:.0001pt;
	font-size:9.0pt;
	font-family:"Segoe UI","sans-serif";}
p.MsoListParagraph, li.MsoListParagraph, div.MsoListParagraph
	{margin-top:0cm;
	margin-right:0cm;
	margin-bottom:0cm;
	margin-left:36.0pt;
	margin-bottom:.0001pt;
	font-size:10.0pt;
	font-family:"Times New Roman","serif";}
p.MsoListParagraphCxSpFirst, li.MsoListParagraphCxSpFirst, div.MsoListParagraphCxSpFirst
	{margin-top:0cm;
	margin-right:0cm;
	margin-bottom:0cm;
	margin-left:36.0pt;
	margin-bottom:.0001pt;
	font-size:10.0pt;
	font-family:"Times New Roman","serif";}
p.MsoListParagraphCxSpMiddle, li.MsoListParagraphCxSpMiddle, div.MsoListParagraphCxSpMiddle
	{margin-top:0cm;
	margin-right:0cm;
	margin-bottom:0cm;
	margin-left:36.0pt;
	margin-bottom:.0001pt;
	font-size:10.0pt;
	font-family:"Times New Roman","serif";}
p.MsoListParagraphCxSpLast, li.MsoListParagraphCxSpLast, div.MsoListParagraphCxSpLast
	{margin-top:0cm;
	margin-right:0cm;
	margin-bottom:0cm;
	margin-left:36.0pt;
	margin-bottom:.0001pt;
	font-size:10.0pt;
	font-family:"Times New Roman","serif";}
span.Heading1Char
	{mso-style-name:"Heading 1 Char";
	mso-style-link:"Heading 1";
	font-family:"Arial","sans-serif";
	font-weight:bold;
	text-decoration:underline;}
span.Heading2Char
	{mso-style-name:"Heading 2 Char";
	mso-style-link:"Heading 2";
	font-family:"Arial","sans-serif";
	font-weight:bold;
	font-style:italic;}
span.Heading4Char
	{mso-style-name:"Heading 4 Char";
	mso-style-link:"Heading 4";
	text-decoration:underline;}
span.TitleChar
	{mso-style-name:"Title Char";
	mso-style-link:Title;
	font-weight:bold;}
span.SubtitleChar
	{mso-style-name:"Subtitle Char";
	mso-style-link:Subtitle;
	font-family:"Arial","sans-serif";
	font-style:italic;}
span.BodyTextChar
	{mso-style-name:"Body Text Char";
	mso-style-link:"Body Text";}
span.Heading5Char
	{mso-style-name:"Heading 5 Char";
	mso-style-link:"Heading 5";
	font-family:"Times New Roman","serif";
	font-weight:bold;}
span.apple-converted-space
	{mso-style-name:apple-converted-space;}
span.item-no
	{mso-style-name:item-no;}
span.CommentTextChar
	{mso-style-name:"Comment Text Char";
	mso-style-link:"Comment Text";}
span.CommentSubjectChar
	{mso-style-name:"Comment Subject Char";
	mso-style-link:"Comment Subject";
	font-weight:bold;}
span.BalloonTextChar
	{mso-style-name:"Balloon Text Char";
	mso-style-link:"Balloon Text";
	font-family:"Segoe UI","sans-serif";}
.MsoChpDefault
	{font-size:10.0pt;}
@page WordSection1
	{size:612.0pt 792.0pt;
	margin:72.0pt 72.0pt 72.0pt 72.0pt;}
div.WordSection1
	{page:WordSection1;}
 /* List Definitions */
 ol
	{margin-bottom:0cm;}
ul
	{margin-bottom:0cm;}
-->
</style>

</head>

<body lang=EN-MY link=blue vlink=purple>

<div class=WordSection1>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%'><b><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>Terms of Use for Practitioners, Practices and Healthcare
Providers for the use of </span></b><span lang=EN-US><a
href="http://www.ghealth.com"><b><span style='font-size:12.0pt;line-height:
115%'>www.ghealth.com</span></b></a></span><b><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>, the mobile
application “gHealth” and the other Services provided by GLOCO MALAYSIA SDN.
BHD. and its associated companies (“GLOCO”).</span></b></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%'><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>&nbsp;</span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%'><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>GLOCO MALAYSIA SDN. BHD. together with its related companies
(“us”, “we”, or “<b>GLOCO</b>”, which also includes its affiliates) is the
author and publisher of the internet resource www.ghealth.com (“<b>Website</b>”)
on the world wide web as well as the software and applications provided by
GLOCO, including but not limited to the mobile application ‘gHealth’, the
software and applications of the brand names ‘gHealth’ and GLOCO’s software and
applications (together with the Website, referred to as the “<b>Services</b>”).</span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%'><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>&nbsp;</span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%'><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>These Terms of Use constitute the agreement (the&nbsp;<b>“&#8203;Agreement&#8203;”</b>&nbsp;or&nbsp;<b>“&#8203;Terms
of Use&#8203;”</b>) between GLOCO and the user of GLOCO’s Subscriber Services
(“User”, as defined in Section 2 of this agreement). Your use of GLOCO’s Subscriber
Services, which include </span><span lang=EN-US><a href="http://www.ghealth.com"><span
style='font-size:12.0pt;line-height:115%'>www.ghealth.com</span></a></span><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>, the mobile
application “gHealth” and the other Services provided by GLOCO </span><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>which
include various ancillary Services accessible at &#8203;</span><span
lang=EN-US><a href="http://www.gloco.com"><span style='font-size:12.0pt;
line-height:115%'>http://www.gloco.com</span></a></span><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>&nbsp;and&nbsp;</span><span
lang=EN-US><a href="https://www.ghealth.com&#8203;"><span style='font-size:
12.0pt;line-height:115%'>https://www.ghealth.com&#8203;</span></a></span><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>, &#8203;for
which a Subscriber amount is payable for usage &#8203;(hereinafter individually
referred to as the “&#8203;Subscriber &#8203;Service&#8203;” and collectively
referred to as the&nbsp;<b>“&#8203;Subscriber&#8203; Services&#8203;”</b>) is
subject to the following terms and conditions.</span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%'><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>&nbsp;</span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%'><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>This Agreement, among other things, provides the terms and
conditions for use of Subscriber Services, primarily a web based practice
management hosted and managed remotely through the website and through native
mobile applications as described in Section 3.9 of this Agreement. The sites </span><span
lang=EN-US><a href="http://www.gloco.com"><span style='font-size:12.0pt;
line-height:115%'>http://www.gloco.com</span></a></span><span lang=EN-US
style='font-size:12.0pt;line-height:115%'>&nbsp;and&nbsp;</span><span
lang=EN-US><a href="https://www.ghealth.com&#8203;"><span style='font-size:
12.0pt;line-height:115%'>https://www.ghealth.com&#8203;</span></a></span><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>&nbsp;are owned
and operated by GLOCO.</span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%'><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>&nbsp;</span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%'><span lang=EN-GB style='font-size:12.0pt;line-height:115%;
color:#333333'>This Agreement shall supersede and cancel all previous
agreements either in written form or orally between the parties in respect of
this Website, the mobile application ‘gHealth’ and GLOCO’s software and
applications.</span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%'><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>&nbsp;</span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%'><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>This Agreement is an electronic record and generated by a
computer system and does not require any physical or digital signatures.</span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%'><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpFirst style='text-align:justify;text-justify:inter-ideograph;
text-indent:-18.0pt;line-height:115%'><b><span lang=EN-US style='font-size:
12.0pt;line-height:115%;color:#333333'>1.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></b><b><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>YOUR AGREEMENT WITH GLOCO</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph;line-height:115%'><b><span lang=EN-US style='font-size:12.0pt;
line-height:115%;color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph;line-height:115%'><span lang=EN-US style='font-size:12.0pt;
line-height:115%;color:#333333'>We reserve the right to modify the Terms of Use
at any time without giving you any prior notice. Your use of the Subscriber
Services following any such modification constitutes your agreement to follow
and be bound by the Terms of Use as modified. Any additional terms and
conditions, disclaimers, privacy policies and other policies applicable to
general and specific areas of these Subscriber Services or to particular Subscriber
Services are also considered as Terms of Use. By agreeing to these terms, you
also agree to the terms of use of the Service, which are available at&nbsp;</span><span
lang=EN-US><a href="https://www.practo.com/company/terms" target="_blank"><span
style='font-size:12.0pt;line-height:115%'>http://www.gloco.com</span><span
style='font-size:12.0pt;line-height:115%;color:#1EAEDB'>&nbsp;</span><span
style='font-size:12.0pt;line-height:115%;color:black;text-decoration:none'>and</span><span
style='font-size:12.0pt;line-height:115%;color:#1EAEDB'>&nbsp;</span><span
style='color:windowtext;text-decoration:none'><u><span style='font-size:12.0pt;
line-height:115%;color:blue'>https://www.ghealth.com&#8203;</span></u></span><span
style='font-size:12.0pt;line-height:115%;color:#1EAEDB'>&#8203;</span></a></span><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph;line-height:115%'><span lang=EN-US style='font-size:12.0pt;
line-height:115%;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpLast style='text-align:justify;text-justify:inter-ideograph;
line-height:115%'><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>You acknowledge that you will be bound by this Agreement for
availing any of the Subscriber Services offered by us.</span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%'><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpFirst style='text-align:justify;text-justify:inter-ideograph;
line-height:115%'><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>Your access to use the Subscriber Services will be solely at the
discretion of GLOCO.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph;line-height:115%'><span lang=EN-US style='font-size:12.0pt;
line-height:115%;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt;line-height:115%'><b><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>2.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></b><b><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>WHO IS GLOCO?</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph;line-height:115%'><b><span lang=EN-US style='font-size:12.0pt;
line-height:115%;color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpLast style='text-align:justify;text-justify:inter-ideograph;
line-height:115%'><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>GLOCO is the is the author and publisher of the internet
resource www.ghealth.com (“<b>Website</b>”) on the world wide web as well as
the software and applications provided by GLOCO, including but not limited to
the mobile application ‘gHealth’, and the software and applications of the
brand names ‘gHealth’ (together with the Website, referred to as the
“Services”). The Subscriber Services have been designed for use at businesses,
institutions, establishments and organisations engaged in the healthcare
practices (“&#8203;Practices&#8203;”) by healthcare providers
(“&#8203;Practitioners&#8203;”, which term shall also include designated
associates of the healthcare providers who would use Software), and clients of
the healthcare providers (“&#8203;End-Users&#8203;”, which term shall also
include members of public who search for Practitioners on the website
anonymously or as a registered user of the Service) to find, manage and
organise information including but not limited to personal or non­-personal
information, practice and business information, appointments, prescriptions,
medical records, billing, inventory and accounting details. All users of the Subscriber
Services are together termed as (“&#8203;Users&#8203;”or “you” or “your”).</span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%'><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraph style='text-align:justify;text-justify:inter-ideograph;
line-height:115%'><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>GLOCO makes no express or implied representations or warranties
about its Subscriber Services and disclaims any implied warranties, including,
but not limited to, warranties or implied warranties of merchantability or
fitness for a particular purpose or use or non­infringement. GLOCO does not
authorize anyone to make a warranty on GLOCO’s behalf and you may not rely on
any statement of warranty as a warranty by GLOCO.</span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%'><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpFirst style='text-align:justify;text-justify:inter-ideograph;
text-indent:-18.0pt;line-height:115%'><b><span lang=EN-US style='font-size:
12.0pt;line-height:115%;color:#333333'>3.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></b><b><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>TERMS OF USE</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph;line-height:115%'><b><span lang=EN-US style='font-size:12.0pt;
line-height:115%;color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>3.1<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>By using the Subscriber
Services, you agree that you have read and understood these Terms of Use and
you agree to be bound by these Terms of Use and use these Subscriber Services
in compliance with these Terms of Use. PLEASE READ THESE TERMS OF USE
CAREFULLY. IF YOU DO NOT AGREE TO BE BOUND BY (OR CANNOT COMPLY WITH) ANY OF
THE TERMS BELOW, DO NOT CLICK THE &quot;I AGREE&quot; BOX, DO NOT COMPLETE THE
REGISTRATION PROCESS, AND DO NOT ATTEMPT TO USE THE SERVICE. You expressly
represent and warrant that you will not use these Subscriber Services if you do
not understand, agree to become a party to, and abide by all of the terms and
conditions specified below. Any violation of these Terms of Use may result in
legal liability upon you. Nothing in these Terms of Use should be construed to
confer any rights to any third party or any other person. YOUR USE OF GLOCO’S SUBSCRIBER
SERVICES MEANS YOU ARE CONSENTING TO THIS AGREEMENT.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>3.2<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>You must be 18 years of
age or older to register&#894; use these Subscriber Services in any manner. By
registering, or accepting this Agreement, you represent and warrant to GLOCO
that you are 18 years of age or older, and that you have the right, authority
and capacity to use the Subscriber Services available through the GLOCO and
agree to and abide by this Agreement. You also represent and warrant that you
are not a person barred from receiving the Subscriber Services under the laws
of Malaysia or other countries including the country in which you are resident
or from which you use the Subscriber Services.</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>3.3<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>The Agreement is
published in compliance of, and is governed by the provisions of Malaysian law,
including but limited to:</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:72.0pt;text-indent:-72.0pt'><span
lang=EN-US style='font-size:12.0pt;color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>i.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>The
Contract Act 1950;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:72.0pt;text-indent:-72.0pt'><span
lang=EN-US style='font-size:12.0pt;color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>ii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>The
Copyright Act 1987;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:72.0pt;text-indent:-72.0pt'><span
lang=EN-US style='font-size:12.0pt;color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>iii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>The
Personal Data Protection Act 2010; and</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:72.0pt;text-indent:-72.0pt'><span
lang=EN-US style='font-size:12.0pt;color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>iv.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>The
rules, regulations, guidelines and clarifications framed there under.</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>3.4<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>A condition of the
User’s use of and access to the Subscriber Services available provided by GLOCO
to Users is the User’s acceptance of this Agreement. Any User that does not
agree with any provisions of the same is required to leave this computer
resource/the Subscriber Services immediately and immediately discontinue use of
all Subscriber Services available at the GLOCO.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>3.5<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>GLOCO authorizes the
User to view and access the content available on the Subscriber Services solely
for ordering, receiving, delivering and communicating only as per this
Agreement. The contents of the Subscriber Services, information, text,
graphics, images, logos, button icons, software code, design, and the
collection, arrangement and assembly of content on the Subscriber Services
(collectively, &quot;&#8203;GLOCO Content&#8203;&quot;), are the property of GLOCO
and are protected under copyright, trademark and other laws. User shall not
modify the GLOCO Content or reproduce, display, publicly perform, distribute,
or otherwise use the GLOCO Content in any way for any public or commercial
purpose or for personal gain.</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>3.6<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>Multiple Users are not
permitted to share the same/single log­in.</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>3.7<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>If you are an employee,
associate, consultant, intern or are in any way associated to the Practitioner
that has subscribed to the Subscriber Services and the subscribing Practitioner
has authorized you, explicitly or implicitly, to use the Subscriber Services,
this Agreement is a three-­way agreement between you, the Practitioner and GLOCO.
Both the Practitioner and GLOCO may seek recourse against you for any violation
of the terms of this Agreement.</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>3.8<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>Users may not transfer
(including by way of sub-license, lease, assignment or other transfer,
including by operation of law) their log­in or right to use the Subscriber
Services to any third party. You, the User, are solely responsible for the way
anyone you have authorized to use the Subscriber Services and for ensuring that
all of such Users comply with all of the terms and conditions of this
Agreement. Any violation of the terms and/or conditions of this Agreement by
any such User shall be deemed to be a violation thereof by you.</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>3.9<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>These Terms of Use will
also be applicable to Users who access Software features using native mobile
applications published by GLOCO including but not limited to its applications
for devices running on platforms such as iOS, Android, Windows, Blackberry and
any derivatives or any other platforms. Additional terms of use may be
applicable to Users while accessing Software using such mobile applications.</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>3.10<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>You agree that any registration information you give to GLOCO
will always be true, accurate, correct, complete and up to date, to the best of
our knowledge. Any phone number used to register with the Subscriber Services
be registered in your name and you might be asked to provide supporting
documents to prove the same.</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>3.11<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>You agree that you will not use the Subscriber Services provided
by GLOCO for any unauthorized and unlawful purpose. You will not impersonate
another person, including, without limitation, a Practitioner, a Practice or
User.</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>3.12<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>You agree to use the Subscriber Services only for purposes that
are permitted by (a) the Terms of Use and (b) any applicable law, regulation
and generally accepted practices or guidelines in the relevant jurisdictions
(including any laws regarding the export of data or software to and from Malaysia
or other relevant countries).</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>3.13<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>You agree not to access (or attempt to access) any of the Subscriber
Services by any means other than through the interface that is provided by GLOCO,
unless you have been specifically allowed to do so in a separate agreement with
GLOCO.</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>3.14<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>You agree that you will not engage in any activity that
interferes with or disrupts the Subscriber Services (or the servers and
networks which are connected to the Subscriber Services).</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>3.15<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>You agree that you will not reproduce, duplicate, copy,
transfer, license, rent, sell, trade or resell the Software or any other Subscriber
Services for any purpose whatsoever.</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>3.16<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>You agree that you are solely responsible for (and that GLOCO
has no responsibility to you or to any third party for) any breach of your
obligations under the Terms of Use and for the consequences (including any loss
or damage which GLOCO may suffer) of any such breach.</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>3.17<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>You shall indemnify GLOCO for any claims, losses or damages, or
for the costs of any regulatory or court proceedings suffered by GLOCO as a
result of your breach under any applicable law.</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>3.18<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>You expressly acknowledge and agree that your use of the Subscriber
Services is at your sole risk and that the Subscriber Services are provided
&quot;as is&quot; and &quot;as available”.</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>3.19<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>You agree that you will not make any unsolicited calls or use
any information displayed on the GLOCO, an online platform&#894; to breach any
applicable rules and guidelines related to unsolicited commercial
communications, including but not limited to regulations &amp; guidelines such
as guidelines for telemarketers, or otherwise violate applicable law while
using the Subscriber Services.</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>3.20<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>You agree that this Agreement and the Subscriber Services of GLOCO
are subject to any modification, or may be removed by GLOCO, as a result of
change in government regulations, policies and local laws as applicable.</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>3.21<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>You agree and understand that you are responsible for
maintaining the confidentiality of passwords associated with any log­in you use
to access the Software.</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>3.22<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>You agree and understand that you shall use your best endeavours
to get your patients or customers to subscribe to the Services of GLOCO
including but not limited to downloading and utilising gHealth. This includes you
through your clinics, doctors, agents, employees or staff campaigning gHealth
to your walk-in patients and/or promoting gHealth by electronic means to all
patients.</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>3.23<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>Following this Agreement and the Subscriber Services of GLOCO,
you shall give full cooperation to GLOCO, its agent, employee, staff,
consultant and/or its associated companies to set up a call centre in future to
assist you, your clinics, doctors, agents, employees or staff in handling appointment/booking
(whenever necessary).</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpLast style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>3.24<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>Your use of each Subscriber Service confers upon you only the
rights and obligations relating to such Subscriber Service, and not to any
other Subscriber Service or service that may be provided by GLOCO. For
instance, being a subscriber to GLOCO Ray does not automatically entitle you to
a higher ranking on GLOCO’s Practitioner search facility.</span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%'><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpFirst style='text-indent:-18.0pt'><b><span
lang=EN-US style='font-size:12.0pt;color:#333333'>4.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></b><b><span lang=EN-US style='font-size:12.0pt;color:#333333'>USE
OF SUBSCRIBER SERVICES</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph;line-height:115%'><b><span lang=EN-US style='font-size:12.0pt;
line-height:115%;color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>4.1<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>GLOCO provides Software
through its website, as a Software as a Service (SaaS) model. GLOCO is not
responsible for and does not deal with any of the patients managed by User
through the website or native mobile applications and only provides Software to
User through the website and native mobile applications. To the extent User
uses such software or downloads such software from the website, the software,
will be deemed to be licensed to User by GLOCO, for providing Subscriber
Services to User and enabling User to use those Software only. GLOCO does not
transfer either the title or the intellectual property rights to the Software
and other its Subscriber Services, and GLOCO (or its licensors) retain full and
complete title to the Software as well as all intellectual property rights
therein. User agrees to use the Subscriber Services and the materials provided
therein only for purposes that are permitted by: (a) this Agreement&#894; and
(b) any applicable law, regulation or generally accepted practices or
guidelines in the relevant jurisdictions. Information provided by a User to GLOCO
may be used, stored or re­published by GLOCO or its affiliates even after the
termination of these terms of Service.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>4.2<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>GLOCO may offer at its
discretion, a free trial of its Subscriber Services for a specified time
period. Users of the Software during the trial period are bound by the terms of
this Agreement and any applicable law, regulation and generally accepted
practices or guidelines in the relevant jurisdictions. Any data User enters
into the Software, and any customizations made to the Software by or for User,
during User’s free trial will be permanently lost at the expiry of the
specified time period unless the User upgrades his/her/its subscription to one
of the User Plans. GLOCO does not provide any warranty during the trial period.</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>4.3<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>GLOCO offers its Subscriber
Services on as ­is basis and has the sole right to modify any feature or
customize them at its discretion and there shall be no obligation to honour
customization requests of any User. The subscription fee hence charged is
exclusive of any customisation costs.</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>4.4<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>User shall not access
the Subscriber Services of GLOCO if the User or the organisation that he/she/it
represents is GLOCO’s direct competitor, except with GLOCO’s prior written
consent. In addition, the User shall not access the Subscriber Services for
purposes of monitoring their availability, performance or functionality, or for
any other benchmarking or competitive purposes.</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>4.5<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>GLOCO provides, at its
discretion basic support for the Subscriber Services at no additional charge,
and/or upgraded support if purchased separately and will use commercially
reasonable efforts to make the Subscriber Services available 24 hours a day, 7
days a week, except for (i) planned downtime (of which GLOCO shall give at
least 8 hours’ notice to Users via the Subscriber Services and which GLOCO
shall schedule to the extent practicable during the weekend hours from 6:00
p.m. Friday to 9:00 a.m. Monday</span><span lang=EN-US> (</span><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>MYT), or
(ii) any unavailability caused by circumstances beyond GLOCO’s reasonable
control, including without limitation, acts of God, acts of government, flood,
fire, earthquakes, civil unrest, acts of terror, strikes or other labour
problems, or internet service provider failures or delays. GLOCO will provide
the Subscriber Services only in accordance with applicable laws and government
regulations.</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>4.6<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>Notwithstanding
anything to the contrary contained herein, GLOCO does not warrant that its Subscriber
Services will always function without disruptions, delay or errors. A number of
factors may impact the use of the Subscriber Services (depending on the Subscriber
Services used) and native mobile applications and may result in the failure of
your communications including but not limited to: your local network, firewall,
your internet service provider, the public internet, your power supply and
telephony services. GLOCO takes no responsibility for any disruption, interruption
or delay caused by any failure of or inadequacy in any of these items or any
other items over which we have no control.</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>4.7<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>In the event the
Software are not available due to apparent default at GLOCO’s end or are
rendered unusable, GLOCO may at its discretion extend the subscription period
of the Practitioner only by such number of calendar days when the Subscriber
Services were not available. However, you shall agree that GLOCO is not
responsible and will not be held liable for the any failure of the intermediary
services such as, internet connectivity failure or telephonic disconnections.</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>4.8<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>The Subscriber Services
may be subject to certain limitations, such as, limits on disk storage space,
on the number of calls Users are permitted to make against GLOCO’s application
programming interface, and, other limitations dependent on the ‘User Plan’, for
example, number of SMS, number of appointments, number of users or accounts,
validity of subscription and any other limitations. Any such limitations are
specified in the User Plans. The Subscription Services have been designed to
provide real­time information to enable User to monitor such User’s compliance
with such limitations.</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>4.9<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>Notwithstanding
anything to the contrary contained herein, Practitioner alone shall be liable
for Practitioner’s dealings and interaction with patient, his/her
representatives or affiliates, searching for Practitioners through the Website
(the “&#8203;End­ User&#8203;”). contacted or managed through the Software and GLOCO
shall have no liability or responsibility in this regard. GLOCO does not
guarantee or make any representation with respect to the correctness,
completeness or accuracy of the information or detail provided by End-Users or
any third party through the Subscriber Services. The Subscriber Services are
not intended for and must not be used for emergency purposes such as emergency
appointments, emergency healthcare procedures or any other emergency
situations.</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>4.10<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>GLOCO may, at its sole discretion, suspend User’s ability to use
or access the Subscriber Services at any time while GLOCO investigates
complaints or alleged violations of this Agreement, or for any other reason.</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>4.11<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>GLOCO reserves the right to use all information captured in its Subscriber
Services in anonymised form for the purpose of its Subscriber Services
improvements, and providing analytics and businesses intelligence to the third
parties. On the basis of such information, GLOCO tries to make its Subscriber
Services more useful in following way:</span></p>

<p class=MsoListParagraphCxSpLast><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoNormal style='margin-left:120.05pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>i.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>Promotion of new Subscriber Services,</span></p>

<p class=MsoNormal style='margin-left:120.05pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>ii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>Analysing software usage patterns for improving Subscriber
Services design and utility&#894;</span></p>

<p class=MsoNormal style='margin-left:120.05pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>iii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>Analysing anonymised patients’ information for research and
development of new technologies and any other Subscriber Services
offerings&#894;</span></p>

<p class=MsoListParagraphCxSpFirst><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>Users can use the
rectification tools provided by GLOCO or contact GLOCO immediately for
rectifications. GLOCO shall bear no liability or responsibility in this regard.</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>4.12<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>GLOCO reserves the right to use the following types of
information stored in our software:</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:90.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-90.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'><span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>i.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>Practice information&#894;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:90.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-90.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'><span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>ii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>Practitioner information&#894;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:90.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-90.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'><span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>iii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>End-Users’ demographic information as anonymised form&#894;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:90.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-90.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'><span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>iv.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>End-Users’ information in relation to his health and history
(anonymised form)&#894;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>4.13<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>GLOCO automatically lists Practitioner information on its Subscriber
Services as per information added to a Practice using its Software. The
information listed is displayed when End-Users search for Practitioners or
Practices on the GLOCO, and this information listed on the Subscriber Services
may be used by End-Users to request for appointments. Such information on the Subscriber
Services may continue to appear even after the Practice removes the information
from its Software or the Practice ceases to be a paying subscriber or the
Practice terminates its subscription or in any other way discontinues its
relationship with GLOCO. GLOCO, on its own, does not list any Personally
Sensitive Information of such Practitioners. GLOCO do not provide any ranking
algorithm in relation to the listing made on its Website. GLOCO reserves the
right to list Practitioners who are not party to this Agreement and the
Practitioners who have subscribed to this Terms of Use are listed along with
them. Thereon, GLOCO reserves the right to modify the listing of Practitioners
on its Website. In case Practitioner or Practice wishes to change or remove
information as listed and displayed omits online platform, or disable
appointment requests ,the Practitioners or Practices can do so by using options
available on its online platform or its Subscriber Services or by contacting GLOCO
at </span><span lang=EN-US><a href="mailto:infoMY@gloco.com"><span
style='font-size:12.0pt;line-height:115%'>infoMY@gloco.com</span></a></span><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>4.14<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>The Subscriber Services available by GLOCO accepts online
appointment requests for all practitioners listed and displayed on its website.
GLOCO intends to take all reasonable steps to duly inform Practices via phone
and email for appointment requests made on Service. However, it is possible
that some appointment requests do not reach the Practices at all or in a timely
manner due to technical or operational reasons including but not limited to
cases when Practices do not respond to phone calls made by GLOCO or when
Practices do not read emails or text messages sent by GLOCO in timely manner. GLOCO
shall have no liability or responsibility in this regard.</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>4.15<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>While GLOCO makes every feasible effort to ensure a confirmed
appointment for a End­-User who requested an appointment on the Service, GLOCO
does not guarantee that the appointments will be confirmed in all cases.
Further, GLOCO has no liability if such appointment is confirmed but later
cancelled by any of the End-Users, or the Practitioners are not available as
per the given appointment time.</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>4.16<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>Certain Subscriber Services (including ancillary Subscriber
Services) may be subject to additional limitations, restrictions, terms and/or
conditions specific to such Software (“&#8203;Specific Terms&#8203;”). In such
cases, the applicable Specific Terms will be and your access to and use of the
relevant Subscriber Services will be contingent upon your acceptance of and
compliance with such Specific Terms.</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>4.17<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>GLOCO reserves the right to add new functionality, remove
existing functionality, and modify existing functionality to its Subscriber
Services as and when it deems fit, and make any such changes available in newer
versions of its Subscriber Services or native mobile application or all of
these at its discretion. All Users of its Subscriber Services will be duly
notified upon release of such newer versions and GLOCO reserves the right to
automatically upgrade all Users to the latest version of its Software as and
when it deems fit.</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpLast style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>4.18<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>Payment, Fees and Taxes</span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%'><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpFirst style='margin-left:90.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-36.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>4.18.1<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>GLOCO may
add new Subscriber Services for additional fees and charges or may proactively
amend fees and charges for existing Subscriber Services, at any time in its
sole discretion. Fees stated prior to the Subscriber Services being provided,
as amended at GLOCO’s sole discretion from time to time, shall apply.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:90.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-36.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>4.18.2<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>GLOCO may
impose to following fees and charges for the following Subscriber Services on
you (which may be amended by GLOCO at any time in its sole discretion subject
to three (3) months’ notification being made to you):-</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:90.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:126.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-126.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'><span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>i.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>gHealth Features/gHealth Consultation – a platform being made
available to Users to assist them to obtain consultation from Practitioners and
does not intend to replace the physical consultation with the Practitioner :
20% on the consultation fee charged by the Practitioners to Users;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:126.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-126.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'><span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>ii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>Appointment booking and interaction with Practitioners : 20% on
the appointment fee or booking fee or interaction fee charged by the
Practitioners to Users.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:126.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:90.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-36.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>4.18.3<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>If you
purchase any subscription based paid Service, you authorize GLOCO to charge you
applicable fees at the beginning of every subscription period or at such
intervals as applicable to the said Service, and you authorise GLOCO make such
modification to the fee structure as required during the subsistence of a
subscription period and also agree to abide by such modified fee structure.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:90.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-36.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>4.18.4<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>You agree
that the billing credentials provided by you for any purchases from GLOCO will
be accurate and you shall not use billing credentials that are not lawfully
owned by you.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:90.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-36.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>4.18.5<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>The User
agrees to pay all subscription fees, consulting fees and other fees applicable
to User’s use of Subscriber Services and the User shall not circumvent the fee
structure. The fee is dependent on the User Plan that User purchases and on any
additional usage beyond limitations of the User plans but not on actual usage
of the Subscriber Services. The subscription fee is non­refundable.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:90.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-36.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>4.18.6<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>Each User is
solely responsible for payment of all taxes, legal compliances, and statutory
registrations and reporting. GLOCO is in no way responsible for any of the
User’s taxes or legal or statutory compliances, except for its own due
diligence.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:90.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-36.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>4.18.7<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>GLOCO may
make available an offline fee payment facility, supported by a third party
vendor. GLOCO is not responsible for any loss or damage caused to the User
using this payment facility provided by such third party vendor.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:90.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-36.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>4.18.8<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>All fees are
exclusive of taxes. Government Service Tax and other statutory taxes as
applicable are levied on every purchase.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:90.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-36.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>4.18.9<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>The payment
process would be considered to be complete only on receipt of the fees into GLOCO's
designated bank account.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:90.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-36.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>4.18.10</span><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>Fees not
received within the specified due dates attract late charges of 8% per annum
from the due ­date of payment, and any such charges may be levied at GLOCO's
sole discretion.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:90.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-36.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>4.18.11</span><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>GLOCO
reserves the right to modify the fee structure by providing a 30 (thirty) days’
prior notice, either by notice on the Subscriber Services or through email to
the authorized User, which shall be considered as valid and agreed
communication. Upon the User not communicating any response to GLOCO to such
notice, GLOCO shall apply the modified fee structure effective from the expiry
of the said notice period.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:90.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-36.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>4.18.12</span><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>In order to
process the fee payments, GLOCO might require details of User’s bank account,
credit card number and other such financial information. Users are directed to
check our </span><span lang=EN-US><a href="https://ghealth.com/privacy"
target="_blank"><span style='font-size:12.0pt;line-height:115%;color:#1EAEDB'>privacy
policy</span></a></span><a name="_GoBack"></a><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>&nbsp;on how GLOCO uses
the confidential information provided by Users.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:90.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-36.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>4.18.13</span><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>Notwithstanding
anything to the contrary contained herein, in case the payments are made by a
User through credit card, an invoice for subsequent subscription
period/renewals shall be generated 10 (ten) days prior to the expiry of the
existing subscription period and an email will be sent to such User registered
with GLOCO intimating such User about expiration of the current subscription
period and that the credit card of such User registered with GLOCO will be
charged automatically against payment of subscription fee for subsequent
subscription period, along with a copy of the invoice for the subsequent
subscription period/renewal. Subject to the provisions below, if a User is not
willing to continue or renew the subscription of Subscriber Services, the same
shall be communicated to GLOCO by the User within 5 (five) days of receipt of
such intimation from GLOCO. In the absence of such intimation to discontinue
the subscription, GLOCO shall be entitled to charge the credit card of the User
registered with GLOCO on the day the current subscription period expires.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:90.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-36.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>4.18.14</span><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>GLOCO shall
send an intimation of receipt of fee from the Users through an email within 7
(seven) working days of receipt of fee into GLOCO’s designated bank account.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:90.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-36.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>4.18.15</span><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>In case of
non­payment of any fee beyond the date a payment becomes overdue (overdue
date), GLOCO reserves the right to take any or all of the following actions as
it deems appropriate (i) reduce all Subscriber Service credits in Users’ Subscriber
Services account to 0 (zero) anytime after 7 (seven) days from the overdue
date, including but not limited to SMS and Call credits. (ii) discontinue the Subscriber
Services to the User anytime after 30 (thirty) days from the overdue date.
(iii) delete all information in User’s account anytime after 90 (ninety) days
from the overdue date.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:90.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-36.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>4.18.16</span><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>Fees and
charges shall be calculated solely based on records maintained by GLOCO or its
third party billing provider. No other information of any kind shall be
acceptable by us or have any effect under this agreement. Decision of GLOCO
shall be final and binding in relation to any fees payable by Users.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:90.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-36.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>4.18.17</span><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>You can
cancel your access to the Subscriber Services using any of the cancellation
methods listed in the Terms of Use or by contacting our customer support by
email at </span><span lang=EN-US><a href="mailto:infoMY@gloco.com"><span
style='font-size:12.0pt;line-height:115%'>infoMY@gloco.com</span></a></span><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>. The one
time set­up fees shall not be refunded to the User.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:90.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-36.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>4.18.18</span><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>GLOCO will
not be liable to you or to any third party for any modification, suspension, or
discontinuance of the Subscriber Services, or parts thereof, except that you
are only entitled to a prorated refund representing the unused (as of the date
of termination) portion of any subscription fees, paid deposits or payments for
Subscriber Services other than the non­refundable one time set­up fees as due
prior to permanent discontinuation the Subscriber Services or upon the expiry
of 45 (forty five) days from the date of your written notice to GLOCO. GLOCO
shall have the right to deduct any taxes that are due in relation to the refund
amount (if any). The subscription fees are non­transferable and the payment
made by the User for a particular Subscriber Service cannot be transferred or
carried over to another Service.</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpLast style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>4.19<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>gHealth Data Sharing/GLOCO Data Sharing: Terms of Use</span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%'><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpFirst style='margin-left:90.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-36.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>4.19.1<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>gHealth Data
Sharing/GLOCO Data Sharing has been integrated with gHealth to enable
Practitioners to electronically share any type of Health Records with patients’
GLOCO Account if Practitioners want. These Health Records may include and are
not limited to prescriptions, files, vital signs, immunization plans, growth
charts, clinical notes, treatment plans, invoices and payments etc and this
list will keep evolving as more types of Health Records are added to gHealth.
Once shared, these Health Records can be accessed by End-Users with a free
facility ‘Records’ on its mobile application ‘gHealth’. The specific terms
relating to the gHealth Data Sharing/GLOCO Data Sharing are as below, without
prejudice to the rest of these Terms and the Privacy Policy:</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:90.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:126.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-126.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'><span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>i.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>End-Users’ Records will display and contain the same Health
Records that was created by the End-User's or provided by you in the designated
section for Health Records in gHealth. GLOCO shall not validate the Health
Records and will not be responsible for any errors in or incompleteness of such
Health Records provided by you. You hereby represent and warrant that to the
extent that you provide any such Health Records, it is true and complete to the
best of your knowledge.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:126.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:126.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-126.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'><span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>ii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>By subscribing to gHealth or switching on “gHealth Data Sharing/GLOCO
Data Sharing” setting or similar settings in the Services, you are granting an
irrevocable right of ownership to the relevant End-Users to Health Records,
including but not limited to any medical records and information that is
classified as sensitive and non-sensitive personal information of the patients
or customers.</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:126.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-126.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'><span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>iii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>By subscribing to gHealth or any GLOCO’s Services or switching
on “gHealth Data Sharing/GLOCO Data Sharing” setting or similar settings in the
Services, you are also granting GLOCO the right to upload or share into its
designated database or Cloud database Health Records, including but not limited
to any medical records and information that is classified as sensitive and
non-sensitive personal information of the patients or customers.</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:126.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-126.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'><span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>iv.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>You agree that you have already obtained the necessary consent
and/or approval from the relevant patients or customers before subscribing to
gHealth or any GLOCO’s Services and granting GLOCO the right to upload or share
into its designated database or Cloud database Health Records, including but
not limited to any medical records and information that is classified as
sensitive and non-sensitive personal information of the patients or customers.</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:126.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-126.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'><span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>v.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>GLOCO is not responsible for your failure to obtain the necessary
consent and/or approval from the relevant patients or customers before
subscribing to gHealth or any GLOCO’s Services and granting GLOCO the right to
upload or share into its designated database or Cloud database.</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:126.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-126.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'><span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>vi.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>You agree the Health Records shall be entered into appropriate
designated sections at your sole risk and responsibility after obtaining prior
consent of the patients and customers, if any, and GLOCO shall not be
responsible or liable for Health Records whatsoever including failure to add
the Health Records in the designated section for Health Records in gHealth.</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:126.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-126.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'><span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>vii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>For your patients and customers that are not referred to you by GLOCO,
it is your responsibility to ensure that such patients’ and customers’ mobile
numbers and email IDs are correctly provided and mentioned for the intended
owner of the Records while using the Services. In case of any errors or changes
in details, you are required to inform GLOCO of the same as soon as you become
aware of such errors or changes.</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:126.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-126.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'><span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>viii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>GLOCO is not responsible for verifying the accuracy or
incompleteness of such persons’ details or Health Records provided in the
Records, and shall not be liable for any errors in the same.</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:126.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-126.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'><span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>ix.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>Changing the contact number will not affect the Health Records
already added to the End-Users’ Records.</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:126.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-126.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'><span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>x.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>GLOCO is not liable, if for any reason Health Records are not
delivered to your patient or customer, or are delivered late or not accessed,
despite its best efforts. While GLOCO will endeavor to take all reasonable
steps through its dedicated support team to resolve any technical or
operational difficulties for the delivery of the Health Records to your patient
or customer, GLOCO makes no promise or guarantee for any uninterrupted access
to the Records to your patients or customers.</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:126.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-126.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'><span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>xi.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>GLOCO reserves the right to recall or partially recall any
shared Health Record due to inadvertent or incorrect sharing by you or any
other reason as it may deem fit.</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:126.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-126.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'><span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>xii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>Owner of the clinic is solely responsible to manage the “gHealth
Data Sharing/GLOCO Data Sharing” setting or similar settings in the Services settings
for the clinic.</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:126.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-126.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'><span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>xiii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>Owner is solely responsible to inform all relevant gHealth users
within the clinic about the intended usage of the feature.</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:126.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-126.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'><span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>xiv.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>Any change to the setting will be effective only from the date
of change, not retrospectively unless otherwise communicated by GLOCO.</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:126.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-126.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'><span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>xv.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>GLOCO has the right to communicate and inform End-Users without
any prior notice to the Practitioner about the shared Health Records through
SMS, email or any other platform on behalf of the Practitioner.</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:126.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-126.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'><span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>xvi.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>GLOCO has the right to independently resolve any technical or
operational issue of the End-User regarding the access of the Health Records.</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:126.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-126.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'><span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>xvii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>GLOCO has the right to not to provide the access of the Health
Records to the user without any prior notice to avoid Health Records theft.</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:126.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-126.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'><span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>xviii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>GLOCO is not responsible for any unintended access of Records
due to change in phone number of the End User.</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:126.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-126.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'><span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>xix.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>GLOCO will follow the law of land in case of any constitutional
court or jurisdiction mandates to share the Health Records for any reason.</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpLast style='margin-left:126.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-126.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'><span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>xx.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>You agree and acknowledge that GLOCO may need to access the
Health Record for cases such as any technical or operational issue of the End
User in access or ownership of the Records.</span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%'><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpFirst style='text-align:justify;text-justify:inter-ideograph;
text-indent:-18.0pt'><b><span lang=EN-US style='font-size:12.0pt;color:#333333'>5.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></b><b><span
lang=EN-US style='font-size:12.0pt;color:#333333'>COLLECTION, USE, STORAGE AND
TRANSFER OF PERSONAL INFORMATION</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph;line-height:115%'><b><span lang=EN-US style='font-size:12.0pt;
line-height:115%;color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>5.1<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>The terms “personal
information” and “sensitive personal data or information” are defined under the
Personal Data Protection Act 2010, and are reproduced in the privacy policy
(“&#8203;Privacy Policy&#8203;”) available at &#8203;</span><span lang=EN-US><a
href="http://www.ghealth.com/privacy"><span style='font-size:12.0pt;line-height:
115%'>www.ghealth.com/privacy</span></a></span><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>5.2<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>The Privacy Policy sets
out:</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:90.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-90.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'><span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>i.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>The type of information collected from Users, including
sensitive personal data or information&#894;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:90.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-90.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'><span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>ii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>The purpose, means and modes of usage of such information&#894; and</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:90.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-90.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'><span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>iii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>How and to whom GLOCO will disclose such information.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>5.3<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>The User is expected to
read and understand the Privacy Policy, so as to ensure that he or she has the
knowledge of:</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:90.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-90.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'><span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>i.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>the fact that the information is being collected&#894;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:90.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-90.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'><span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>ii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>the purpose for which the information is being collected&#894;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:90.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-90.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'><span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>iii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>the intended recipients of the information&#894;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:90.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-90.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'><span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>iv.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>the name and address of the agency that is collecting the
information and the agency that will retain the information&#894; and</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:90.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-90.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'><span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>v.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>the various rights available to such Users in respect of such
information.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>5.4<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>Each Practice and its
users of Software will be responsible for obtaining explicit consent from their
End-Users before storing any End­-User information in Software.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>5.5<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>GLOCO shall not be
responsible in any manner for the authenticity of the personal information or
sensitive personal data or information supplied by the User to GLOCO or any
other person acting on behalf of GLOCO.</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>5.6<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>The use of the Subscriber
Services involves every User’s registration information and browsing history
being stored and submitted to the appropriate authorities. The consent and
procedure for such collection and submission is provided in the privacy policy.
The other information collected by GLOCO from Users as part of the registration
process is described in the privacy policy. The consent and revocation
procedures in relation to the same are set out in the Privacy Policy.</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>5.7<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>The User is responsible
for maintaining the confidentiality of the User’s log­in account access
information and password. The User shall be responsible for all usage of the
User’s log­in or password, whether or not authorized by the User. The User
shall immediately notify GLOCO of any actual or suspected unauthorized use of
the User’s log­in or password. Although GLOCO will not be liable for your
losses caused by any unauthorized use of your account such as stolen or hacked
passwords, you may be liable for the losses to GLOCO or any others parties due
to such unauthorized use.</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>5.8<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>If a User provides any
information that is untrue, inaccurate, not current or incomplete (or becomes
untrue, inaccurate, not current or incomplete), or GLOCO has reasonable grounds
to suspect that such information is untrue, inaccurate, not current or incomplete,
GLOCO has the right to discontinue the Subscriber Services to the User at its
sole discretion.</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>5.9<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>GLOCO may, at its
discretion, use information stored in its Subscriber Services from time to time
for the purposes of debugging customer support related issues.</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>5.10<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>GLOCO may, at its discretion engage third party contractors or
uses third party’s services to process or use information stored in its Subscriber
Services from time to time for the purposes of health related issues and/or for
any services incidental or related to the Services.</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpLast style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>5.11<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>GLOCO collects and uses Users personal and demographics
information in ways as stated in Privacy Policy that can be found at&nbsp;</span><span
lang=EN-US><a href="http://www.ghealth.com/privacy"><span style='font-size:
12.0pt;line-height:115%'>www.ghealth.com/privacy</span></a></span><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>. GLOCO
intends to seek User’s permission if it wishes to use any User’s personal data
for any purpose not specified herein or in the Privacy Policy. Any information
provided by the User may be retained by GLOCO and used at its discretion after
termination of this Agreement or expiry of a subscription by the User, and
thereupon the Agreement and Privacy Policy of the Website shall be applicable
to such information.</span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%'><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpFirst style='text-indent:-18.0pt'><b><span
lang=EN-US style='font-size:12.0pt;color:#333333'>6.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></b><b><span lang=EN-US style='font-size:12.0pt;color:#333333'>COVENANTS</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph;line-height:115%'><b><span lang=EN-US style='font-size:12.0pt;
line-height:115%;color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>6.1<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>GLOCO hereby informs
the User that the User is not permitted to host, display, upload, modify,
publish, transmit, update or share any information that:</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:90.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-90.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'><span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>i.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>belongs to another person and to which the User does not have
any right to&#894;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:90.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-90.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'><span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>ii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>is grossly harmful, harassing, blasphemous, defamatory, obscene,
pornographic, paedophilic, libellous, invasive of another's privacy, hateful,
or racially, ethnically objectionable, disparaging, relating or encouraging
money laundering or gambling, or otherwise unlawful in any manner whatever&#894;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:90.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-90.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'><span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>iii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>harm minors in any way&#894;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:90.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-90.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'><span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>iv.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>infringes any patent, trademark, copyright or other proprietary
rights&#894;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:90.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-90.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'><span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>v.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>violates any law for the time being in force&#894;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:90.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-90.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'><span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>vi.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>deceives or misleads the addressee (or End­ User or User) about
the origin of such messages or communicates any information which is grossly
offensive or menacing in nature&#894;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:90.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-90.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'><span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>vii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>impersonate another person&#894;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:90.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-90.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'><span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>viii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>contains software viruses or any other computer code, files or
programs designed to interrupt, destroy or limit the functionality of any
computer resource&#894;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:90.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-90.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'><span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>ix.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>threatens the unity, integrity, defence, security or sovereignty
of Malaysia, friendly relations with foreign states, or public order or causes
incitement to the commission of any cognisable offence or prevents
investigation of any offence or is insulting any other nation.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>6.2<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>The User is also
prohibited from:</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:90.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-90.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'><span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>i.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>violating or attempting to violate the integrity or security of
the Subscriber Services or any GLOCO Software&#894;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:90.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-90.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'><span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>ii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>transmitting any information (including job posts, messages and
hyperlinks) on or through the Subscriber Services that is disruptive or
competitive to the provision of Subscriber Services by GLOCO&#894;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:90.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-90.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'><span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>iii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>intentionally submitting on the Subscriber Services any
incomplete, false or inaccurate information&#894;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:90.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-90.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'><span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>iv.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>making any unsolicited communications to other Users&#894;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:90.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-90.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'><span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>v.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>using any engine, software, tool, agent or other device or
mechanism (such as spiders, robots, avatars or intelligent agents) to navigate
or search the Service&#894;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:90.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-90.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'><span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>vi.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>attempting to decipher, decompile, disassemble or reverse
engineer any part of the Subscriber Services unless explicitly permitted by
GLOCO&#894;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:90.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-90.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'><span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>vii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>copying or duplicating in any manner any of the GLOCO content or
other information available from the Service&#894;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:90.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-90.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'><span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>viii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>framing or hot-linking or deep-linking any GLOCO content.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:90.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-90.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'><span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>ix.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>circumventing or disabling any digital rights management, usage
rules, or other security features of the Software.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>6.3<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>GLOCO, upon obtaining
knowledge by itself or being brought to actual knowledge by an affected person
in writing or through email signed with electronic signature about any such
information as mentioned in S. 6.2 above, shall be entitled to disable such
information that is in contravention of S. 6.2. GLOCO shall be entitled to
preserve such information and associated records for at least 90 (ninety) days
for service on to governmental or investigative authorities for investigation
purposes.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>6.4<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>In case of non­compliance
with any applicable laws, rules or regulations, or the Agreement (including the
privacy policy) by a User, GLOCO has the right to immediately terminate the access
or usage rights of the User to the Subscriber Services and to remove non-compliant
information.</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>6.5<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>GLOCO may disclose or
transfer User Information (as defined in the privacy policy) to its affiliates,
and you hereby consent to such transfer. The Personal Data Protection Act 2010 only
permits GLOCO to transfer sensitive personal data or information including any
information, to any other body corporate or a person in Malaysia, or located in
any other country, that ensures the same level of data protection that is
adhered to by GLOCO as provided for under the Personal Data Protection Act 2010,
only if such transfer is necessary for the performance of the lawful contract
between GLOCO or any person on its behalf and the user or where the User has
consented to data transfer.</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpLast style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>6.6<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>GLOCO respects the
intellectual property rights of others and we do not hold any responsibility
for any violations of any intellectual property rights.</span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%'><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpFirst style='text-indent:-18.0pt'><b><span
lang=EN-US style='font-size:12.0pt;color:#333333'>7.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></b><b><span lang=EN-US style='font-size:12.0pt;color:#333333'>LIABILITY</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph;line-height:115%'><b><span lang=EN-US style='font-size:12.0pt;
line-height:115%;color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>7.1<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>GLOCO shall not be
responsible or liable in any manner to the Users for any losses, damage,
injuries or expenses incurred by the Users as a result of any disclosures made
by GLOCO, where the User has consented to the making of disclosures by GLOCO.
If the User had revoked such consent under the terms of the privacy policy,
then GLOCO shall not be responsible or liable in any manner to the User for any
losses, damage, injuries or expenses incurred by the User as a result of any
disclosures made by GLOCO prior to its actual receipt of such revocation.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>7.2<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>The User shall not hold
GLOCO responsible or liable in any way for any disclosures by GLOCO.</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>7.3<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>The Software provided
by GLOCO or any of its licensors or providers are provided &quot;as is”,&quot;
as available”, and without any warranties or conditions (express or implied,
including the implied warranties of merchantability, accuracy, fitness for a
particular purpose, title and non-­infringement, arising by statute or
otherwise in law or from a course of dealing or usage or trade). GLOCO does not
provide or make any representation, warranty or guaranty, express or implied
about the Subscriber Services. GLOCO does not verify any content or information
provided by Users on its Subscriber Services and to the fullest extent
permitted by law, disclaims all liability arising out of the User’s use or
reliance upon the Subscriber Services, , the GLOCO Content, representations and
warranties made by the Users or the content or information provided by the
Users on the Subscriber Services or any opinion or suggestion given or
expressed by GLOCO or any User in relation to any User or Subscriber Services
provided by such User.</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>7.4<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>GLOCO assumes no
responsibility, and shall not be liable for ways in which End­-User data is
used by Practitioners and other authorized users of Software at a Practice. It
is the responsibility of the Practice alone to ensure that the End-­User data
either stored in Software or taken out from Software by printing or exporting
to PDF, CSV or any other computer file format or data stored offline in mobile
devices of users accessing Software through mobile applications published by GLOCO,
is used in compliance to local privacy laws applicable to the Practice’s
business transactions with End-Users.</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>7.5<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>The Subscriber Services
of GLOCO may be linked to the services of third parties, affiliates and
business partners. GLOCO has no control over, and not liable or responsible for
content, accuracy, validity, reliability, quality of such Subscriber Services
or made available by/through our Subscriber Services. Inclusion of any link on
the Subscriber Services does not imply that GLOCO endorses the linked site.
User may use the links and these Subscriber Services at User’s own risk.</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>7.6<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>GLOCO assumes no
responsibility, and shall not be liable for, any damages to, or viruses that
may infect User’s equipment on account of User’s access to, use of, or browsing
the Subscriber Services or the downloading of any material, data, text, images,
video content, or audio content from the Service. If a User is dissatisfied
with the Service, User’s sole remedy is to discontinue using the Subscriber
Services of GLOCO.</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>7.7<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>The Subscriber Services
may enable User to communicate with other Users or to post information to be
accessed by others, whereupon other Users may collect such data. Such Users,
including any moderators or administrators, are not authorized GLOCO
representatives or agents, and their opinions or statements do not necessarily
reflect those of GLOCO, and they are not authorized to bind GLOCO to any
contract. GLOCO hereby expressly disclaims any liability for any reliance or
misuse of such information that is made available by Users or visitors in such
a manner.</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>7.8<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>In no event, including
but not limited to negligence, shall GLOCO, or any of its directors, officers,
employees, agents or content or service providers (collectively, the
“&#8203;protected entities&#8203;”) be liable for any direct, indirect,
special, incidental, consequential, exemplary or punitive damages arising from,
or directly or indirectly related to, the use of, or the inability to use, the Subscriber
Services or the content, materials and functions related thereto, User’s
provision of information via the Subscriber Services of the GLOCO, lost
business or lost sales, even if such protected entity has been advised of the
possibility of such damages. In no event shall the protected entities be liable
for provision of or failure to provide all or any Subscriber Services by
Practitioners to End-Users contacted or managed through the Service. In no
event shall the protected entities be liable for or in connection with any
content posted, transmitted, exchanged or received by or on behalf of any User
or other person on or through the Subscriber Services. In no event shall the
total aggregate liability of the protected entities to a User for all damages,
losses, and causes of action (whether in contract or tort, including, but not
limited to, negligence or otherwise) arising from the terms and conditions or a
User’s use of the Subscriber Services exceed, in the aggregate RM100.00.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>7.9<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>In no event shall the
protected entities be liable for failure on the part of the Users to provide
agreed Subscriber Services or to make himself/herself available at the
appointed time, cancellation or rescheduling of appointments. In no event shall
the protected entities be liable for any comments or feedback given by any of
the Users in relation to the Subscriber Services provided by a User.</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>7.10<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>The listing order of Practitioners and/or Practices on the Subscriber
Services is based on numerous factors including End-Users’ comments and
feedbacks. In no event shall the protected entities and  GLOCO be liable or
responsible for the listing order of Practitioners and/or Practices on the
Service. Further, GLOCO shall not be responsible for adverse feedback or
comments, or ratings on the Subscriber Services which are a subject matter of
automated processes, and GLOCO disclaims any liability for lost business or
reputation of a User due to information, data or ratings that’s are available
on the Service. GLOCO at its discretion hold the sole right to display the
listing order of the Practitioner and/ or Practices.</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>7.11<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>The reviews and the feedbacks are displayed by the GLOCO at its
discretion. You agree that GLOCO may contact you through telephone, email, sms,
or at your contact details for the limited purpose of:</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:90.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-90.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'><span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>i.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>Obtaining feedback in relation to GLOCO’s Subscriber
Services&#894; </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:90.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-90.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'><span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>ii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>Offering other software/products/insurance in relation to
GLOCO’s Subscriber Services; and/or</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:90.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-90.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'><span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>iii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>Obtaining feedback in relation to any Practitioners or the HCPs
listed on the Service.</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpLast style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>7.12<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>The protected entities and the GLOCO shall not be liable for any
act or omission of any other company or companies furnishing a portion of the
Service, or from any act or omission of a third party, including those vendors
participating in GLOCO Subscriber Services made to you, or for any unauthorized
interception of Customer’s communications or other breaches of privacy
attributable in part to the acts or omissions of Customer or third parties, or
for damages associated with the Service, or equipment that it does not furnish,
or for damages that result from the operation of Customer provided systems,
equipment, facilities or services that are interconnected with the Service.</span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%'><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpFirst style='text-indent:-18.0pt'><b><span
lang=EN-US style='font-size:12.0pt;color:#333333'>8.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></b><b><span lang=EN-US style='font-size:12.0pt;color:#333333'>INDEMNITY</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph;line-height:115%'><b><span lang=EN-US style='font-size:12.0pt;
line-height:115%;color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>8.1<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>User agrees to
indemnify and hold harmless GLOCO, its affiliates, officers, directors,
employees, consultants, licensors, agents, and representatives from any and all
third party vendors, from claims, losses, liability, damages, and/or costs
(including reasonable attorney fees and costs) arising from his/her/ its access
to or use of Software, violation of this Agreement, or infringement, or
infringement by any other user of his/her/its account, of any intellectual
property or other right of any person or entity. GLOCO will notify you promptly
of any such claim, loss, liability, or demand, and in addition to your
foregoing obligations, you agree to provide us with reasonable assistance, at
your expense, in defending any such claim, loss, liability, damage, or cost.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-18.0pt'><b><span
lang=EN-US style='font-size:12.0pt;color:#333333'>9.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></b><b><span lang=EN-US style='font-size:12.0pt;color:#333333'>SPAMMING</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph;line-height:115%'><b><span lang=EN-US style='font-size:12.0pt;
line-height:115%;color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>9.1<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>GLOCO has a zero­tolerance
spam policy. GLOCO employs controls on user permission to receive Content from GLOCO’s
Subscriber Services and has easily accessible ways for users to block or not
receive content if they chose to. However, GLOCO’s policy on spam is clearly
stated below:</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:90.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-90.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'><span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>i.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>Spamming is defined as the practice of (i) sending unsolicited
messages, likely with commercial content, (ii) in large quantities (iii) to an
indiscriminate set of recipients. The result of this practice is termed “Spam”.</span></p>

<p class=MsoListParagraphCxSpLast style='margin-left:90.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-90.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'><span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>ii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>The sender of any message deemed to be &quot;spam&quot; is
liable for RM100.00­ for each End-­User that receives each unauthorized
message. The sender of ‘Spam’ will pay all fees owed to GLOCO within thirty
(30) days of such transmission.</span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%'><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpFirst style='text-indent:-18.0pt'><b><span
lang=EN-US style='font-size:12.0pt;color:#333333'>10.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></b><b><span lang=EN-US style='font-size:12.0pt;color:#333333'>TERM,
TERMINATION AND DISPUTES</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph;line-height:115%'><b><span lang=EN-US style='font-size:12.0pt;
line-height:115%;color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>10.1<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>This Agreement will remain in full force and effect while the
User is a user of any of the Subscriber Services in any form or capacity.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>10.2<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>The User can request for termination of his/her/its membership
with GLOCO at any time by providing 30 (thirty) days’ prior written notice to </span><span
lang=EN-US><a href="mailto:infoMY@gloco.com"><span style='font-size:12.0pt;
line-height:115%'>infoMY@gloco.com</span></a></span><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>. During this 30­day
period, GLOCO will investigate and ascertain the fulfilment of any ongoing Subscriber
Services or pending dues related to subscription fees or any other fees by the
User. GLOCO may require the User to continue his/her/its subscription until the
completion or termination of an on­going Subscriber Services or subscription
period, should the situation warrant and at GLOCO’s discretion. The User shall
be obligated to clear any dues with GLOCO for any of its Subscriber Services
for which the User has procured. GLOCO shall not be liable to you or any third
party for any termination of your access to the Site and/or the Subscriber
Services.</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>10.3<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>GLOCO reserves the right to terminate any account in cases:</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:90.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-90.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'><span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>i.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>A User breaches any terms and conditions of this terms of use or
privacy policy&#894;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:90.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-90.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'><span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>ii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>GLOCO is unable to verify or authenticate any information
provide to GLOCO by a User&#894; or</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:90.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-90.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'><span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>iii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>GLOCO believes in its sole discretion that User’s actions may
cause legal liability for such User, other Users or for GLOCO or are contrary
to the interests of the Service.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>10.4<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>Once temporarily suspended, indefinitely suspended or
terminated, the User may not continue to use the Subscriber Services under the
same account, a different account or re­register under a new account, unless
explicitly permitted by GLOCO. On termination of an account due to the reasons
mentioned herein, such User shall no longer have access to data, messages, files
and other content kept on the Subscriber Services by such User. The User shall
ensure that he/she/it maintains has continuous backup of any User­ provided
content, data or information on the Service, in order to comply with
his/her/its record keeping process and practices. Nothing contained in these
Terms of Use shall restrict GLOCO’s use of the data or right to publish
information made available by a User in the public domain through the Subscriber
Services or any other platform managed by GLOCO after the termination or expiry
of a subscription or cessation of operation of these Terms in relation to a
specific User.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>10.5<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>Return of User’s Data: Upon request by a User made within 60
(sixty) days after the effective date of termination of a Subscriber Services
subscription due to non­payment, GLOCO will make available to the User for
download a copy of such User’s data in comma separated value (.csv) format or
any other format as determined by GLOCO. After such 60 (sixty) days period, GLOCO
shall have no obligation to maintain or provide any of such User’s data and
shall thereafter, unless legally prohibited, delete all User’s data in its
systems or otherwise in its possession or under its control. In cases where
User terminates the subscription voluntarily, it will be the sole
responsibility of the User to make a copy of their data before terminating the
subscription ­ Users data will not be available after termination of
subscription in such cases.</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>10.6<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>GLOCO reserves the right, at its sole discretion, to pursue all
of its legal remedies, including but not limited to deletion of the User’s
content from the Subscriber Services and immediate termination of the User’s
account with or without ability to access the Softwares, upon any breach by the
User of this Agreement or if GLOCO is unable to verify or authenticate any
information the User submits to GLOCO, or if the User fails to provide (or
after providing such consent, later revokes) the consents necessary or
desirable for GLOCO to provide the Subscriber Services to the User.</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>10.7<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>This Agreement and any contractual obligation between GLOCO and
User will be governed by the laws of Malaysia, subject to the exclusive
jurisdiction of Courts in Malaysia.</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>10.8<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>Even after termination, certain obligations mentioned under
Covenants, Liability, Indemnity, Intellectual Property, Dispute Resolution will
continue and survive termination.</span></p>

<p class=MsoListParagraphCxSpMiddle><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpLast style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>10.9<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>Any amendment in these Terms shall replace all previous versions
of the same.</span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%'><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpFirst style='text-indent:-18.0pt'><b><span
lang=EN-US style='font-size:12.0pt;color:#333333'>11.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></b><b><span lang=EN-US style='font-size:12.0pt;color:#333333'>THEFT
OF SUBSCRIBER SERVICES</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph;line-height:115%'><b><span lang=EN-US style='font-size:12.0pt;
line-height:115%;color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>11.1<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>You agree to notify GLOCO immediately, in writing or by mail to
&#8203;</span><span lang=EN-US><a href="mailto:infoMY@gloco.com"><span
style='font-size:12.0pt;line-height:115%'>infoMY@gloco.com</span></a></span><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'> or by
calling GLOCO customer care on 1300 88 6008, if your content is stolen or if
you become aware at any time that your account with any Subscriber Service is being
misused or being used fraudulently. When you call or write, you must provide
your account details and a detailed description of the circumstances of the
theft or fraudulent use of the Subscriber Services. Failure to do so promptly
or within a reasonably prompt time period after discovery of the improper use
may result in the termination of your Subscriber Services and additional
charges to you.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpLast style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>11.2<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>You will be liable for all use of the Subscriber Services if
your account is misused and also for any and all stolen Subscriber Services or
fraudulent use of the Service. Notwithstanding anything herein to the contrary,
GLOCO shall not be liable to extend the subscription period or waive­ off any
fees on account of such theft or fraudulent use. This includes, but is not
limited to, modem hijacking, wireless hijacking, or other fraud arising out of
a failure of your internal or corporate security procedures. GLOCO will not
issue refunds for fraudulent use resulting from your negligent or wilful acts
or those of an authorized user of your Subscriber Services.</span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%'><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraph style='text-indent:-18.0pt'><b><span lang=EN-US
style='font-size:12.0pt;color:#333333'>12.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></b><b><span lang=EN-US style='font-size:12.0pt;color:#333333'>MISUSE
OF SUBSCRIBER SERVICES</span></b></p>

<p class=MsoNormal style='margin-left:18.0pt;text-align:justify;text-justify:
inter-ideograph;line-height:115%'><b><span lang=EN-US style='font-size:12.0pt;
line-height:115%;color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraph style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>12.1<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>GLOCO may restrict, suspend or terminate the account of any User
who abuses or misuses the Subscriber Services. Misuse includes creating
multiple or false profiles, infringing any intellectual property rights,
violating any of the terms and conditions of these Terms of Use, or any other
behavior that GLOCO, in its sole discretion, deems contrary to its purpose. In
addition, and without limiting the foregoing, GLOCO has adopted a policy of
terminating accounts of users who, in GLOCO’s sole discretion, are deemed to be
repeat infringers of any Terms of Use even after being warned by GLOCO.</span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%'><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraph style='text-indent:-18.0pt'><b><span lang=EN-US
style='font-size:12.0pt;color:#333333'>13.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></b><b><span lang=EN-US style='font-size:12.0pt;color:#333333'>SEVERABILITY
AND WAIVER</span></b></p>

<p class=MsoNormal style='margin-left:18.0pt;text-align:justify;text-justify:
inter-ideograph;line-height:115%'><b><span lang=EN-US style='font-size:12.0pt;
line-height:115%;color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpFirst style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>13.1<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>If any provision of this terms of use is held to be invalid or
unenforceable, such provision shall be struck and the remaining provisions
shall be enforced.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpLast style='text-indent:-18.0pt'><b><span
lang=EN-US style='font-size:12.0pt;color:#333333'>14.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span></b><b><span lang=EN-US style='font-size:12.0pt;color:#333333'>CONTACT
INFORMATION</span></b></p>

<p class=MsoNormal style='margin-left:18.0pt;text-align:justify;text-justify:
inter-ideograph;line-height:115%'><b><span lang=EN-US style='font-size:12.0pt;
line-height:115%;color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpFirst style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>14.1<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>If any User has any question, issue, complaint regarding any of
our Subscriber Services, please contact our customer service at &#8203;</span><span
lang=EN-US><a href="mailto:infoMY@gloco.com"><span style='font-size:12.0pt;
line-height:115%'>infoMY@gloco.com</span></a></span><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'> or by calling GLOCO
customer care on 1300 88 6008&#8203;.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;line-height:115%'><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpLast style='margin-left:54.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt;line-height:115%'><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'>14.2<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;line-height:115%;
color:#333333'>If a User has any questions concerning GLOCO, the Service, this
Agreement, or anything related to any of the foregoing, GLOCO can be reached at
the following email address </span><span lang=EN-US><a
href="mailto:infoMY@gloco.com"><span style='font-size:12.0pt;line-height:115%'>infoMY@gloco.com</span></a></span><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#333333'> or via the
contact information available from the following hyperlink: &#8203;</span><span
lang=EN-US><a href="http://www.ghealth.com/contact"><span style='font-size:
12.0pt;line-height:115%'>www.ghealth.com/contact</span></a></span><u><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:#1EAEDB'> </span></u><span
lang=EN-US style='font-size:12.0pt;line-height:115%;color:black'>or </span><span
lang=EN-US><a href="http://www.gloco.com"><span style='font-size:12.0pt;
line-height:115%'>www.gloco.com</span></a></span><u><span lang=EN-US
style='font-size:12.0pt;line-height:115%;color:#1EAEDB'> </span></u></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:115%'><span lang=EN-US style='font-size:12.0pt;line-height:115%'>&nbsp;</span></p>

</div>

</body>

</html>
