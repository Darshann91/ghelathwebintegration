<html>

<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name=Generator content="Microsoft Word 12 (filtered)">
<style>
<!--
 /* Font Definitions */
 @font-face
	{font-family:Wingdings;
	panose-1:5 0 0 0 0 0 0 0 0 0;}
@font-face
	{font-family:SimSun;
	panose-1:2 1 6 0 3 1 1 1 1 1;}
@font-face
	{font-family:Mangal;
	panose-1:2 4 5 3 5 2 3 3 2 2;}
@font-face
	{font-family:"Cambria Math";
	panose-1:2 4 5 3 5 4 6 3 2 4;}
@font-face
	{font-family:Tahoma;
	panose-1:2 11 6 4 3 5 4 4 2 4;}
@font-face
	{font-family:"Microsoft YaHei";
	panose-1:2 11 5 3 2 2 4 2 2 4;}
@font-face
	{font-family:"\@SimSun";
	panose-1:2 1 6 0 3 1 1 1 1 1;}
@font-face
	{font-family:"\@Microsoft YaHei";
	panose-1:2 11 5 3 2 2 4 2 2 4;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin:0cm;
	margin-bottom:.0001pt;
	font-size:10.0pt;
	font-family:"Times New Roman","serif";}
h1
	{mso-style-link:"Heading 1 Char";
	margin-top:0cm;
	margin-right:0cm;
	margin-bottom:0cm;
	margin-left:21.6pt;
	margin-bottom:.0001pt;
	text-align:center;
	text-indent:-21.6pt;
	page-break-after:avoid;
	font-size:10.0pt;
	font-family:"Arial","sans-serif";
	font-weight:bold;
	text-decoration:underline;}
h2
	{mso-style-link:"Heading 2 Char";
	margin-top:12.0pt;
	margin-right:0cm;
	margin-bottom:3.0pt;
	margin-left:28.8pt;
	text-indent:-28.8pt;
	page-break-after:avoid;
	font-size:14.0pt;
	font-family:"Arial","sans-serif";
	font-weight:bold;
	font-style:italic;}
h4
	{mso-style-link:"Heading 4 Char";
	margin-top:0cm;
	margin-right:0cm;
	margin-bottom:0cm;
	margin-left:43.2pt;
	margin-bottom:.0001pt;
	text-indent:-43.2pt;
	page-break-after:avoid;
	font-size:12.0pt;
	font-family:"Times New Roman","serif";
	font-weight:normal;
	text-decoration:underline;}
h5
	{mso-style-link:"Heading 5 Char";
	margin-right:0cm;
	margin-left:0cm;
	font-size:10.0pt;
	font-family:"Times New Roman","serif";
	font-weight:bold;}
p.MsoCommentText, li.MsoCommentText, div.MsoCommentText
	{mso-style-link:"Comment Text Char";
	margin:0cm;
	margin-bottom:.0001pt;
	font-size:10.0pt;
	font-family:"Times New Roman","serif";}
p.MsoCaption, li.MsoCaption, div.MsoCaption
	{margin-top:6.0pt;
	margin-right:0cm;
	margin-bottom:6.0pt;
	margin-left:0cm;
	font-size:12.0pt;
	font-family:"Times New Roman","serif";
	font-style:italic;}
p.MsoTitle, li.MsoTitle, div.MsoTitle
	{mso-style-link:"Title Char";
	margin:0cm;
	margin-bottom:.0001pt;
	text-align:center;
	font-size:20.0pt;
	font-family:"Times New Roman","serif";
	font-weight:bold;}
p.MsoBodyText, li.MsoBodyText, div.MsoBodyText
	{mso-style-link:"Body Text Char";
	margin-top:0cm;
	margin-right:0cm;
	margin-bottom:6.0pt;
	margin-left:0cm;
	font-size:10.0pt;
	font-family:"Times New Roman","serif";}
p.MsoSubtitle, li.MsoSubtitle, div.MsoSubtitle
	{mso-style-link:"Subtitle Char";
	margin-top:12.0pt;
	margin-right:0cm;
	margin-bottom:6.0pt;
	margin-left:0cm;
	text-align:center;
	page-break-after:avoid;
	font-size:14.0pt;
	font-family:"Arial","sans-serif";
	font-style:italic;}
a:link, span.MsoHyperlink
	{color:blue;
	text-decoration:underline;}
a:visited, span.MsoHyperlinkFollowed
	{color:purple;
	text-decoration:underline;}
p
	{margin-right:0cm;
	margin-left:0cm;
	font-size:12.0pt;
	font-family:"Times New Roman","serif";}
p.MsoCommentSubject, li.MsoCommentSubject, div.MsoCommentSubject
	{mso-style-link:"Comment Subject Char";
	margin:0cm;
	margin-bottom:.0001pt;
	font-size:10.0pt;
	font-family:"Times New Roman","serif";
	font-weight:bold;}
p.MsoAcetate, li.MsoAcetate, div.MsoAcetate
	{mso-style-link:"Balloon Text Char";
	margin:0cm;
	margin-bottom:.0001pt;
	font-size:8.0pt;
	font-family:"Tahoma","sans-serif";}
p.MsoListParagraph, li.MsoListParagraph, div.MsoListParagraph
	{margin-top:0cm;
	margin-right:0cm;
	margin-bottom:0cm;
	margin-left:36.0pt;
	margin-bottom:.0001pt;
	font-size:10.0pt;
	font-family:"Times New Roman","serif";}
p.MsoListParagraphCxSpFirst, li.MsoListParagraphCxSpFirst, div.MsoListParagraphCxSpFirst
	{margin-top:0cm;
	margin-right:0cm;
	margin-bottom:0cm;
	margin-left:36.0pt;
	margin-bottom:.0001pt;
	font-size:10.0pt;
	font-family:"Times New Roman","serif";}
p.MsoListParagraphCxSpMiddle, li.MsoListParagraphCxSpMiddle, div.MsoListParagraphCxSpMiddle
	{margin-top:0cm;
	margin-right:0cm;
	margin-bottom:0cm;
	margin-left:36.0pt;
	margin-bottom:.0001pt;
	font-size:10.0pt;
	font-family:"Times New Roman","serif";}
p.MsoListParagraphCxSpLast, li.MsoListParagraphCxSpLast, div.MsoListParagraphCxSpLast
	{margin-top:0cm;
	margin-right:0cm;
	margin-bottom:0cm;
	margin-left:36.0pt;
	margin-bottom:.0001pt;
	font-size:10.0pt;
	font-family:"Times New Roman","serif";}
span.Heading1Char
	{mso-style-name:"Heading 1 Char";
	mso-style-link:"Heading 1";
	font-family:"Arial","sans-serif";
	font-weight:bold;
	text-decoration:underline;}
span.Heading2Char
	{mso-style-name:"Heading 2 Char";
	mso-style-link:"Heading 2";
	font-family:"Arial","sans-serif";
	font-weight:bold;
	font-style:italic;}
span.Heading4Char
	{mso-style-name:"Heading 4 Char";
	mso-style-link:"Heading 4";
	text-decoration:underline;}
span.TitleChar
	{mso-style-name:"Title Char";
	mso-style-link:Title;
	font-weight:bold;}
span.SubtitleChar
	{mso-style-name:"Subtitle Char";
	mso-style-link:Subtitle;
	font-family:"Arial","sans-serif";
	font-style:italic;}
span.BodyTextChar
	{mso-style-name:"Body Text Char";
	mso-style-link:"Body Text";}
span.Heading5Char
	{mso-style-name:"Heading 5 Char";
	mso-style-link:"Heading 5";
	font-family:"Times New Roman","serif";
	font-weight:bold;}
span.item-no
	{mso-style-name:item-no;}
span.apple-converted-space
	{mso-style-name:apple-converted-space;}
span.CommentTextChar
	{mso-style-name:"Comment Text Char";
	mso-style-link:"Comment Text";}
span.CommentSubjectChar
	{mso-style-name:"Comment Subject Char";
	mso-style-link:"Comment Subject";
	font-weight:bold;}
span.BalloonTextChar
	{mso-style-name:"Balloon Text Char";
	mso-style-link:"Balloon Text";
	font-family:"Tahoma","sans-serif";}
.MsoChpDefault
	{font-size:10.0pt;}
@page WordSection1
	{size:612.0pt 792.0pt;
	margin:72.0pt 72.0pt 36.0pt 72.0pt;}
div.WordSection1
	{page:WordSection1;}
 /* List Definitions */
 ol
	{margin-bottom:0cm;}
ul
	{margin-bottom:0cm;}
-->
</style>

</head>

<body lang=EN-MY link=blue vlink=purple>

<div class=WordSection1>

<p class=MsoNormal style='margin-bottom:6.0pt;text-align:justify;text-justify:
inter-ideograph'><span lang=EN-US style='font-size:12.0pt;color:#333333'>GLOCO
MALAYSIA SDN. BHD. together with its sister companies (“us”, “we”, or “<b>GLOCO</b>”,
which also includes its affiliates) is the author and publisher of the internet
resource www.ghealth.com (“<b>Website</b>”) on the world wide web as well as
the software and applications provided by GLOCO, including but not limited to
the mobile application ‘gHealth’, the software and applications of the brand
names ‘gHealth’ and GLOCO’s software and applications (together with the
Website, referred to as the “<b>Services</b>”).</span></p>

<p class=MsoNormal style='margin-bottom:6.0pt;text-align:justify;text-justify:
inter-ideograph'><span lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpFirst style='margin-bottom:6.0pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt'><b><span lang=EN-US
style='font-size:12.0pt;color:#333333'>1.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></b><b><span lang=EN-US style='font-size:12.0pt;color:#333333'>NATURE
AND APPLICABILITY OF TERMS</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:6.0pt;text-align:justify;
text-justify:inter-ideograph'><b><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:6.0pt;text-align:justify;
text-justify:inter-ideograph'><span lang=EN-US style='font-size:12.0pt;
color:#333333'>Please carefully go through these terms and conditions&nbsp;<b>(“Terms”)</b>&nbsp;and
the privacy policy available at </span><span lang=EN-US><a
href="http://www.ghealth.com"><span style='font-size:12.0pt'>http://www.ghealth.com</span></a></span><span
lang=EN-US style='font-size:12.0pt;color:#333333'> <b>(“Privacy Policy”)</b>&nbsp;before
you decide to access the Website or avail the services made available on the
Website by GLOCO. These Terms and the Privacy Policy together constitute a
legal agreement&nbsp;<b>(“Agreement”)</b>&nbsp;between you and GLOCO in
connection with your visit to the Website and your use of the Services (as
defined below).</span></p>

<p class=MsoListParagraphCxSpLast style='margin-bottom:6.0pt;text-align:justify;
text-justify:inter-ideograph'><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoNormal style='margin-left:36.0pt'><span lang=EN-US
style='font-size:12.0pt;color:#333333'>The Agreement applies to you whether you
are –</span></p>

<p class=MsoNormal style='margin-left:36.0pt;text-align:justify;text-justify:
inter-ideograph'><span lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;i.&nbsp;A
patient, his/her representatives or affiliates, searching for Practitioners
through the Website&nbsp;<b>(“End-User”, “you” or “User”);</b>&nbsp;or</span></p>

<p class=MsoListParagraphCxSpFirst style='margin-bottom:6.0pt;text-align:justify;
text-justify:inter-ideograph'><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;ii.&nbsp; Otherwise a user of the Website&nbsp;<b>(“you”
or “User”)</b></span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:6.0pt;text-align:justify;
text-justify:inter-ideograph'><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:6.0pt;text-align:justify;
text-justify:inter-ideograph'><span lang=EN-US style='font-size:12.0pt;
color:#333333'>This Agreement applies to those services made available by GLOCO
on the Website, which are offered to the Users&nbsp;<b>(“Services”)</b>,
including, amongst other things, the facility to (i) create and maintain
‘Health Accounts’, (ii) search for Practitioners<a name="_GoBack"></a> by name,
specialty, and geographical area, or any other criteria that may be developed
and made available by GLOCO, and (iii) to make appointments with Practitioners</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:6.0pt;text-align:justify;
text-justify:inter-ideograph'><span lang=EN-US style='color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:6.0pt;text-align:justify;
text-justify:inter-ideograph'><span lang=EN-US style='font-size:12.0pt;
color:#333333'>The Services may change from time to time, at the sole discretion
of GLOCO, and the Agreement will apply to your visit to and your use of the
Website to avail the Service, as well as to all information provided by you on
the Website at any given point in time.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:6.0pt;text-align:justify;
text-justify:inter-ideograph'><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:6.0pt;text-align:justify;
text-justify:inter-ideograph'><span lang=EN-US style='font-size:12.0pt;
color:#333333'>This Agreement defines the terms and conditions under which you
are allowed to use the Website and describes the manner in which we shall treat
your account while you are registered as a member with us. If you have any
questions about any part of the Agreement, feel free to contact us at </span><span
lang=EN-US><a href="mailto:infoMY@gloco.com"><span style='font-size:12.0pt'>infoMY@gloco.com</span></a></span><span
lang=EN-US style='font-size:12.0pt;color:#333333'>. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:6.0pt'><span
lang=EN-GB style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:6.0pt;text-align:justify;
text-justify:inter-ideograph'><span lang=EN-GB style='font-size:12.0pt;
color:#333333'>This Agreement shall supersede and cancel all previous
agreements either in written form or orally between the parties in respect of
this Website, the mobile application ‘gHealth’ and GLOCO’s software and applications.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:6.0pt;text-align:justify;
text-justify:inter-ideograph'><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:6.0pt;text-align:justify;
text-justify:inter-ideograph'><span lang=EN-US style='font-size:12.0pt;
color:#333333'>By downloading or accessing the Website to use the Services, you
irrevocably accept all the conditions stipulated in this Agreement, the&nbsp;</span><span
lang=EN-US><a href="https://www.ghealth.com/user/subscribers" target="_blank"><span
style='font-size:12.0pt;color:#1EAEDB'>Subscription Terms of Service</span></a></span><span
lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;and&nbsp;</span><span
lang=EN-US><a href="https://ghealth.com/privacy" target="_blank"><span
style='font-size:12.0pt;color:#1EAEDB'>Privacy Policy</span></a></span><span
lang=EN-US style='font-size:12.0pt;color:#333333'>, as available on the
Website, and agree to abide by them. This Agreement supersedes all previous
oral and written terms and conditions (if any) communicated to you relating to
your use of the Website to avail the Services. By availing any Service, you
signify your acceptance of the terms of this Agreement.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:6.0pt;text-align:justify;
text-justify:inter-ideograph'><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:6.0pt;text-align:justify;
text-justify:inter-ideograph'><span lang=EN-US style='font-size:12.0pt;
color:#333333'>We reserve the right to modify or terminate any portion of the
Agreement for any reason and at any time, and such modifications shall be
informed to you in writing. You should read the Agreement at regular intervals.
Your use of the Website following any such modification constitutes your
agreement to follow and be bound by the Agreement so modified.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:6.0pt;text-align:justify;
text-justify:inter-ideograph'><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:6.0pt;text-align:justify;
text-justify:inter-ideograph'><span lang=EN-US style='font-size:12.0pt;
color:#333333'>You acknowledge that you will be bound by this Agreement for
availing any of the Services offered by us. If you do not agree with any part
of the Agreement, please do not use the Website or avail any Services.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:6.0pt;text-align:justify;
text-justify:inter-ideograph'><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:6.0pt;text-align:justify;
text-justify:inter-ideograph'><span lang=EN-US style='font-size:12.0pt;
color:#333333'>Your access to use of the Website and the Services will be solely
at the discretion of GLOCO.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:6.0pt;text-align:justify;
text-justify:inter-ideograph'><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpLast style='margin-bottom:6.0pt;text-align:justify;
text-justify:inter-ideograph'><span lang=EN-US style='font-size:12.0pt;
color:#333333'>The Agreement is published in compliance of, and is governed by
the provisions of Malaysian law, including but not limited to:</span></p>

<p class=MsoNormal style='margin-left:68.6pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-68.6pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>i.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>The
Contract Act 1950;</span></p>

<p class=MsoNormal style='margin-left:68.6pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-68.6pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>ii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>The
Copyright Act 1987;</span></p>

<p class=MsoNormal style='margin-left:68.6pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-68.6pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>iii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>The Personal
Data Protection Act 2010; and</span></p>

<p class=MsoNormal style='margin-left:68.6pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-68.6pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>iv.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>The
rules, regulations, guidelines and clarifications framed there under.</span></p>

<p class=MsoListParagraphCxSpFirst style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:36.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-18.0pt'><b><span lang=EN-US style='font-size:12.0pt;color:#333333'>2.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></b><b><span
lang=EN-US style='font-size:12.0pt;color:#333333'>CONDITIONS OF USE</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:36.0pt;text-align:justify;text-justify:inter-ideograph'><b><span
lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:36.0pt;text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;color:#333333'>You must be 18 years of age
or older to register, use the Services, or visit or use the Website in any
manner. By registering, visiting and using the Website or accepting this
Agreement, you represent and warrant to GLOCO that you are 18 years of age or
older, and that you have the right, authority and capacity to use the Website
and the Services available through the Website, and agree to and abide by this
Agreement.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:36.0pt;text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:36.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-18.0pt'><b><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></b><b><span
lang=EN-US style='font-size:12.0pt;color:#333333'>TERMS OF USE APPLICABLE TO
ALL USERS OTHER THAN PRACTITIONERS</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:36.0pt;text-align:justify;text-justify:inter-ideograph'><b><span
lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:36.0pt;text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;color:#333333'>The terms in this Clause 3
are applicable only to Users other than Practitioners.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:36.0pt;text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:54.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-18.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.1<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;color:#333333'>END-USER ACCOUNT AND DATA PRIVACY</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:54.0pt;text-align:justify;text-justify:inter-ideograph'><b><span
lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.1.1<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>The terms
“personal information” and “sensitive personal data or information” are defined
under the Personal Data Protection Act 2010.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph'><b><span
lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.1.2<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>GLOCO may
by its Services, collect information relating to the devices through which you
access the Website, and anonymous data of your usage. The information collected
will be used only for improving the quality of GLOCO’s services and to build
new services.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph'><span lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.1.3<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>The
Website allows GLOCO to have access to registered Users’ non-sensitive personal
data or information, for communication purpose so as to provide you a better
way of booking appointments and for obtaining feedback in relation to the Practitioners
and their practice.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph'><span lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpLast style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.1.4<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>The
Privacy Policy sets out,&nbsp;<i>inter-alia</i>:</span></p>

<p class=MsoNormal style='margin-left:171.5pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'>3.1.4.1<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span
lang=EN-US style='font-size:12.0pt;color:#333333'>The type of information
collected from Users, including sensitive personal data or information;</span></p>

<p class=MsoNormal style='margin-left:171.5pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'>3.1.4.2<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span
lang=EN-US style='font-size:12.0pt;color:#333333'>The purpose, means and modes
of usage of such information;</span></p>

<p class=MsoNormal style='margin-left:171.5pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'>3.1.4.3<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span
lang=EN-US style='font-size:12.0pt;color:#333333'>How and to whom GLOCO will
disclose such information; and,</span></p>

<p class=MsoNormal style='margin-left:171.5pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'>3.1.4.4<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span
lang=EN-US style='font-size:12.0pt;color:#333333'>Other information mandated by
the Personal Data Protection Act 2010.</span></p>

<p class=MsoListParagraph style='margin-top:8.55pt;margin-right:0cm;margin-bottom:
8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.1.5<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>The User
is expected to read and understand the Privacy Policy, so as to ensure that he
or she has the knowledge of,&nbsp;<i>inter-alia</i>:</span></p>

<p class=MsoNormal style='margin-left:171.5pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'>3.1.5.1<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span
lang=EN-US style='font-size:12.0pt;color:#333333'>the fact that certain
information is being collected;</span></p>

<p class=MsoNormal style='margin-left:171.5pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'>3.1.5.2<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span
lang=EN-US style='font-size:12.0pt;color:#333333'>the purpose for which the
information is being collected;</span></p>

<p class=MsoNormal style='margin-left:171.5pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'>3.1.5.3<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span
lang=EN-US style='font-size:12.0pt;color:#333333'>the intended recipients of
the information;</span></p>

<p class=MsoNormal style='margin-left:171.5pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'>3.1.5.4<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span
lang=EN-US style='font-size:12.0pt;color:#333333'>the nature of collection and
retention of the information; and</span></p>

<p class=MsoNormal style='margin-left:171.5pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'>3.1.5.5<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span
lang=EN-US style='font-size:12.0pt;color:#333333'>the name and address of the
agency that is collecting the information and the agency that will retain the
information; and</span></p>

<p class=MsoNormal style='margin-left:171.5pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'>3.1.5.6<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span
lang=EN-US style='font-size:12.0pt;color:#333333'>the various rights available
to such Users in respect of such information.</span></p>

<p class=MsoListParagraphCxSpFirst style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.1.6<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>GLOCO shall
not be responsible in any manner for the authenticity of the personal
information or sensitive personal data or information supplied by the User to
GLOCO or to any other person acting on behalf of GLOCO.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph'><b><span
lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.1.7<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>The User
is responsible for maintaining the confidentiality of the User’s account access
information and password, if the User is registered on the Website. The User
shall be responsible for all usage of the User’s account and password, whether
or not authorized by the User. The User shall immediately notify GLOCO of any
actual or suspected unauthorized use of the User’s account or password.
Although GLOCO will not be liable for your losses caused by any unauthorized
use of your account, you may be liable for the losses of GLOCO or such other
parties as the case may be, due to any unauthorized use of your account.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph'><span lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.1.8<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>If a User
provides any information that is untrue, inaccurate, not current or incomplete
(or becomes untrue, inaccurate, not current or incomplete), or GLOCO has
reasonable grounds to suspect that such information is untrue, inaccurate, not
current or incomplete, GLOCO has the right to discontinue the Services to the
User at its sole discretion.</span></p>

<p class=MsoListParagraphCxSpMiddle><b><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.1.9<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>The Users
are granting GLOCO the right to upload or share into its designated database or
Cloud database their personal information, personal data or information or Health
Records for the purposes of services provided by GLOCO.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph'><span lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpLast style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.1.10<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><span
lang=EN-US style='font-size:12.0pt;color:#333333'>GLOCO and/or its designated
staff, personal, agents, consultants and/or third party service providers may
use such information collected from the Users from time to time for the
purposes of debugging customer support related issues.</span><span lang=EN-US> </span><span
lang=EN-US style='font-size:12.0pt;color:#333333'>You also agree that GLOCO may
contact you through telephone, email, sms, or at your contact details for the
limited purpose of:</span></p>

<p class=MsoNormal style='margin-left:171.5pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'>3.1.10.1<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>Obtaining
feedback in relation to GLOCO’s Subscription Services as defined in Section 2
of Terms of Use&#894;e</span></p>

<p class=MsoNormal style='margin-left:171.5pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'>3.1.10.2<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>Offering
other software/products/insurance in relation to GLOCO’s Subscription Services;
and/or</span></p>

<p class=MsoNormal style='margin-left:171.5pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'>3.1.10.3<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>Obtaining
feedback in relation to any Practitioners or the HCPs listed on the Service.</span></p>

<p class=MsoNormal style='margin-left:135.5pt;text-align:justify;text-justify:
inter-ideograph'><span lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpFirst style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.1.11<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><span
lang=EN-US style='font-size:12.0pt;color:#333333'>Against every Practitioner
listed in Website, you may see a ‘show number’ option. When you choose this
option, you choose to call the number through a free telephony service provided
by GLOCO, and the records of such calls are recorded and stored in GLOCO’s
servers. Such records are dealt with only in accordance with the terms of the
Privacy Policy. Such call facility provided to you by GLOCO should be used only
for appointment and booking purposes, and not for consultation on
health-related issues. GLOCO accepts no liability if the call facility is not
used in accordance with the foregoing.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:36.0pt;text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:54.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-18.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.2<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;color:#333333'>RELEVANCE ALGORITHM</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:54.0pt;text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:54.0pt;text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;color:#333333'>GLOCO’s relevance algorithm
for the Practitioners is a fully automated system that lists the Practitioners,
their profile and information regarding their Practice on its Website. These
listings of Practitioners do not represent any fixed objective ranking or endorsement
by GLOCO. GLOCO will not be liable for any change in the relevance of the
Practitioners on search results, which may take place from time to time. The
listing of Practitioners will be based on automated computation of the various
factors including inputs made by the Users including their comments and
feedback. Such factors may change from time to time, in order to improve the
listing algorithm. GLOCO in no event will be held responsible for the accuracy
and the relevancy of the listing order of the Practitioners on the Website.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:36.0pt;text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:54.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-18.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.3<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;color:#333333'>LISTING CONTENT AND DISSEMINATING
INFORMATION</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:54.0pt;text-align:justify;text-justify:inter-ideograph'><b><span
lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.3.1<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>GLOCO
collects, directly or indirectly, and displays on the Website, relevant
information regarding the profile and practice of the Practitioners listed on
the Website, such as their specialization, qualification, fees, location,
visiting hours, and similar details. GLOCO takes reasonable efforts to ensure
that such information is updated at frequent intervals. Although GLOCO screens
and vets the information and photos submitted by the Practitioners, it cannot
be held liable for any inaccuracies or incompleteness represented from it,
despite such reasonable efforts.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph'><b><span
lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.3.2<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>The
Services provided by GLOCO or any of its licensors or service providers are
provided on an &quot;as is&quot; and “as available’ basis, and without any
warranties or conditions (express or implied, including the implied warranties
of merchantability, accuracy, fitness for a particular purpose, title and
non-infringement, arising by statute or otherwise in law or from a course of
dealing or usage or trade). GLOCO does not provide or make any representation,
warranty or guarantee, express or implied about the Website or the Services. GLOCO
does not guarantee the accuracy or completeness of any content or information
provided by Users on the Website. To the fullest extent permitted by law, GLOCO
disclaims all liability arising out of the User’s use or reliance upon the
Website, the Services, representations and warranties made by other Users, the
content or information provided by the Users on the Website, or any opinion or
suggestion given or expressed by GLOCO or any User in relation to any User or
services provided by such User.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph'><span lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.3.3<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>The
Website may be linked to the website of third parties, affiliates and business
partners. GLOCO has no control over, and not liable or responsible for content,
accuracy, validity, reliability, quality of such websites or made available
by/through our Website. Inclusion of any link on the Website does not imply
that GLOCO endorses the linked site. User may use the links and these services
at User’s own risk.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph'><span lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.3.4<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>GLOCO
assumes no responsibility, and shall not be liable for, any damages to, or
viruses that may infect User’s equipment on account of User’s access to, use
of, or browsing the Website or the downloading of any material, data, text,
images, video content, or audio content from the Website. If a User is
dissatisfied with the Website, User’s sole remedy is to discontinue using the
Website.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph'><span lang=EN-US style='font-size:12.0pt;color:#333333'>      </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.3.5<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>If GLOCO
determines that you have provided fraudulent, inaccurate, or incomplete
information, including through feedback, GLOCO reserves the right to
immediately suspend your access to the Website or any of your accounts with GLOCO
and makes such declaration on the website alongside your name/your clinic’s
name as determined by GLOCO for the protection of its business and in the
interests of Users. You shall be liable to indemnify GLOCO for any losses
incurred as a result of your misrepresentations or fraudulent feedback that has
adversely affected GLOCO or its Users.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph'><b><span lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:54.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-18.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.4<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;color:#333333'>APPOINTMENT BOOKING AND INTERACTION WITH
PRACTITIONERS</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:54.0pt;text-align:justify;text-justify:inter-ideograph'><b><span
lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.4.1<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>While GLOCO
will try to ensure a confirmed appointment for an End-User who requested an
appointment on Website, GLOCO does not guarantee that a patient will get a
confirmed appointment. Further, GLOCO has no liability if such appointment is
confirmed but later cancelled by Practices or Practitioners, or the
Practitioners are not available as per the given appointment time.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph'><b><span
lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.4.2<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>GLOCO
provides value added telephonic services which connect Users directly to the
Practitioners and the information exchanged between the User and the
Practitioners is stored and used in accordance with the Privacy Policy.
However, it is at the discretion of the User, to avail the Service. If a User
has used the telephony service, GLOCO reserves the right to share the contact
details of the User with the Practitioners contacted.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph'><span lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.4.3<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>You
understand and agree that any interactions and associated issues with other
Users including but not limited to your health issues and your experiences is
strictly between you and the other Users. You shall not hold GLOCO responsible
for any such interactions and associated issues. For avoidance of doubt, GLOCO
is not involved in providing any healthcare or medical advice or diagnosis and
hence is not responsible for any outcome between you and the Practitioner you
interact with, pursuant to any interactions on the Website. If you decide to
engage with a Practitioner to provide medical services to you, you do so at
your own risk. The results of any search you perform on the Website for
Practitioners should not be construed as an endorsement by GLOCO of any such
particular Practitioner. GLOCO shall not be responsible for any breach of
service or service deficiency by any Practitioner. We cannot assure nor
guarantee the ability or intent of the Practitioner(s) to fulfill their
obligations towards you. We advise you to perform your own investigation prior
to selecting a Practitioner.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph'><b><span
lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.4.4<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>Payment
and Cancellation Policy</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph'><span lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;color:#333333'>In the event, the doctor with
whom the appointment has been booked is not available in the Clinic/Hospital,
the patient has to (a) cancel the current appointment or (b) consult another
doctor with clinic's/hospital's consent. Cancellation of the appointment by the
patient should be done one (1) hour prior to the time blocked for appointment.
In such an event, the entire amount will be refunded to the patient within 30
business days from the date of cancellation in accordance with the mode of
refund proposed by the patient. In case a booking confirmation e-mail gets
delayed due to technical reasons or as a result of incorrect e-mail ID provided
by the user etc, an appointment would be considered as 'booked'. If the patient
does not show up at the time of Appointment, GLOCO will refund the entire
amount after approval from Clinic/Hospital within 30 business days in
accordance with the payment methods provided by the patient.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;color:#333333'>GLOCO shall not be liable for
any refunds to the customer in the event the customer's booked slot/ time for
an appointment has been delayed.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;color:#333333'>Any grievances and claims
related to the appointment/ refund should be reported to GLOCO support team
at&nbsp;</span><span lang=EN-US><a href="mailto:infoMY@gloco.com"><span
style='font-size:12.0pt'>infoMY@gloco.com</span></a></span><span lang=EN-US
style='font-size:12.0pt'> </span><span lang=EN-US style='font-size:12.0pt;
color:#333333'>within two (2) days of appointment date with the doctor.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph'><span lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpLast style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.4.5<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>Without
prejudice to the generality of the above, GLOCO will not be liable for:</span></p>

<p class=MsoNormal style='margin-left:171.5pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'>3.4.5.1<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span
lang=EN-US style='font-size:12.0pt;color:#333333'>any wrong medication or
treatment quality being given by the Practitioner(s), or any medical negligence
on part of the Practitioner(s);</span></p>

<p class=MsoNormal style='margin-left:171.5pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'>3.4.5.2<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span
lang=EN-US style='font-size:12.0pt;color:#333333'>any type of inconvenience
suffered by the User due to a failure on the part of the Practitioner to
provide agreed services or to make himself/herself available at the appointed
time, no show by the Practitioner, inappropriate treatment, or similar
difficulties;</span></p>

<p class=MsoNormal style='margin-left:171.5pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'>3.4.5.3<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span
lang=EN-US style='font-size:12.0pt;color:#333333'>any misconduct or
inappropriate behaviour by the Practitioner or the Practitioner’s staff;</span></p>

<p class=MsoNormal style='margin-left:171.5pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'>3.4.5.4<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span
lang=EN-US style='font-size:12.0pt;color:#333333'>cancellation or rescheduling
of booked appointment or any variance in the fees charged;</span></p>

<p class=MsoNormal style='margin-left:171.5pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'>3.4.5.5<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span
lang=EN-US style='font-size:12.0pt;color:#333333'>any medical eventualities
that might occur subsequent to using the services of a Practitioner, whom the
User has selected on the basis of the information available on the Website or
with whom the User has booked an appointment through the Website.</span></p>

<p class=MsoListParagraphCxSpFirst style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.4.6<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>Further, GLOCO
shall not be liable, under any event, for any comments or feedback given by any
of the Users in relation to the services provided by another User. All such
feedback should be made in accordance with applicable law. The option of Users
to give feedback remains at GLOCO’s sole discretion and may be modified or
withdrawn at its sole discretion. GLOCO may moderate such feedback at any time.
GLOCO shall not be obliged to act in any manner to give effect to the content
of Users’ feedback, such as suggestions for delisting of a particular
Practitioner from the Website.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph'><span lang=EN-US style='font-size:12.0pt;color:#333333'>      </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:54.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-18.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.5<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;color:#333333'>NO DOCTOR-PATIENT RELATIONSHIP; NOT FOR
EMERGENCY USE</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph'><span lang=EN-US style='font-size:12.0pt;color:#333333'>      </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.5.1<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>Please
note that some of the content, text, data, graphics, images, information,
suggestions, guidance, and other material (collectively, “Information”) that
may be available on the Website (including information provided in direct
response to your questions or postings) may be provided by individuals in the
medical profession. The provision of such Information does not create a
licensed medical professional/patient relationship, between GLOCO and you and
does not constitute an opinion, medical advice, or diagnosis or treatment of
any particular condition, but is only provided to assist you with locating
appropriate medical care from a qualified practitioner.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph'><b><span
lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.5.2<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>It is
hereby expressly clarified that, the Information that you obtain or receive
from GLOCO, and its employees, contractors, partners, sponsors, advertisers,
licensors or otherwise on the Website is for informational purposes only. We
make no guarantees, representations or warranties, whether expressed or
implied, with respect to professional qualifications, quality of work,
expertise or other information provided on the Website. In no event shall we be
liable to you or anyone else for any decision made or action taken by you in
reliance on such information.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph'><span lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.5.3<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>The
Services are not intended to be a substitute for getting in touch with
emergency healthcare. If you are an End-User facing a medical emergency (either
on your or a another person’s behalf), please contact an ambulance service or
hospital directly.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph'><b><span lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:54.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-18.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.6<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;color:#333333'>gHealth Features/gHealth Consultation</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph'><span lang=EN-US style='font-size:12.0pt;color:#333333'>      </span></p>

<p class=MsoListParagraphCxSpLast style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.6.1<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>gHealth
Features/gHealth Consultation is a feature on the application that enables
Practitioners and patients to connect with each other through: 'Consult Basic'
model and 'Consult Direct' model:</span></p>

<p class=MsoNormal style='margin-left:171.5pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'>3.6.1.1<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span
lang=EN-US style='font-size:12.0pt;color:#333333'>The Consult Basic model:
Enables Users to connect with Practitioners by posting health related queries
on the application, which will be visible on the public health feed.
Practitioners can respond to such queries using a special software application
provided to Practitioners by GLOCO. The public queries posted by the Users are
sent to multiple Practitioners, chosen through a fully automated system. The
automated system chooses Practitioners based on inter alia the information
furnished by each Practitioner, the number of questions answered by the
Practitioner and the rating or reviews procured by the Practitioner. GLOCO will
not be responsible for the Practitioners chosen to respond to the queries.</span></p>

<p class=MsoNormal style='margin-left:171.5pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'>3.6.1.2<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span
lang=EN-US style='font-size:12.0pt;color:#333333'>The Consult Direct model: The
User may post queries on their Health Accounts which will be only sent to the
Practitioner that the User has chosen from the list made available to the User
on the application.</span></p>

<p class=MsoNormal style='margin-left:188.65pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-54.0pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'>3.6.1.2.1<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>The User
can choose a particular Practitioner, post a private query for such
Practitioner, the Practitioner may indicate the pre-consultation fee payable
for the advice solicited and in such cases GLOCO shall provide the User an
option to directly remit the amount to the Practitioner through the payment
gateway option provided. Upon completion of payment, the Practitioner shall
provide his/her response to the query. The amount paid to the Practitioner is
non-refundable unless there has been an issue of non-compliance of applicable
law/rules/regulations/guidelines or of the terms contained hereunder by the
Practitioner, in which case investigation shall be undertaken and the amount
will be refund, subject to the discretion of GLOCO. In any other cases, GLOCO
shall not be liable to provide refund or be responsible for the nature of
advice provided by the Practitioner.</span></p>

<p class=MsoNormal style='margin-left:188.65pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-54.0pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'>3.6.1.2.2<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>GLOCO
shall charge a platform fee on the Practitioner and/patient, which is a nominal
amount, for the service being provided by GLOCO. The amount levied and payable
by Practitioner shall appear on the Practitioner’s account and the amount
levied and payable by the patient shall appear when the patient is making
payment to the Practitioner, respectively.</span></p>

<p class=MsoNormal style='margin-left:188.65pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-54.0pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'>3.6.1.2.3<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>The
payment gateway option is being provided to the patients to make collection of
payment easier. In case wrong bank account details are provided by Practitioner,
GLOCO will not be responsible for loss of money, if any. In case of there being
any technical failure, at the time of transaction and there is a problem in
making payment, you could contact </span><span lang=EN-US><a
href="mailto:infoMY@GLOCO.com."><span style='font-size:12.0pt'>infoMY@GLOCO.com.</span></a></span></p>

<p class=MsoListParagraphCxSpFirst style='text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;color:#333333'>      </span></p>

<p class=MsoListParagraphCxSpLast style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.6.2<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>Terms for
Practitioners:</span></p>

<p class=MsoNormal style='margin-left:171.5pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'>3.6.2.1<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span
lang=EN-US style='font-size:12.0pt;color:#333333'>The Practitioner shall reply
to the patient after receiving patient’s communication. In case of
non-compliance with regard to reverting/ adhering to the applicable
laws/rules/regulations/guidelines by the Practitioner, GLOCO has the right to
show other available Practitioners to the patient or remove Practitioners from
the platform/GLOCO application/site.</span></p>

<p class=MsoNormal style='margin-left:171.5pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'>3.6.2.2<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span
lang=EN-US style='font-size:12.0pt;color:#333333'>The Practitioner understands
and agrees that, GLOCO shall at its sole discretion, at any time be entitled
to, show other Practitioners available for consultation.</span></p>

<p class=MsoNormal style='margin-left:171.5pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'>3.6.2.3<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span
lang=EN-US style='font-size:12.0pt;color:#333333'>The Practitioner further
understands that, there is a responsibility on the Practitioner to treat the
patients on this model, as the Practitioner would have otherwise treated the
patient on a physical one-on-one consultation model.</span></p>

<p class=MsoNormal style='margin-left:171.5pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'>3.6.2.4<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span
lang=EN-US style='font-size:12.0pt;color:#333333'>The Practitioner has the
discretion to cancel any consultation at any point in time in cases where the
Practitioner feels, it is beyond his/her expertise or his/her capacity to treat
the patient. In such cases, patient has the option of choosing other
Practitioners.</span></p>

<p class=MsoNormal style='margin-left:171.5pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'>3.6.2.5<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span
lang=EN-US style='font-size:12.0pt;color:#333333'>The Practitioner shall at all
times ensure that all the applicable laws that govern the Practitioner shall be
followed and utmost care shall be taken in terms of the consultation being
rendered.</span></p>

<p class=MsoNormal style='margin-left:171.5pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'>3.6.2.6<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span
lang=EN-US style='font-size:12.0pt;color:#333333'>The Practitioner acknowledges
that should GLOCO find the Practitioner to be in violation of any of the
applicable laws/rules/ regulations/guidelines set out by the authorities then GLOCO
shall be entitled to cancel the consultation with such patient or take such
other legal action as may be required.</span></p>

<p class=MsoNormal style='margin-left:171.5pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'>3.6.2.7<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span
lang=EN-US style='font-size:12.0pt;color:#333333'>It is further understood by
the Practitioner that the information that is disclosed by the patient at the
time of consultation, shall be confidential in nature and subject to patient
and Practitioner privilege.</span></p>

<p class=MsoNormal style='margin-left:171.5pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'>3.6.2.8<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span
lang=EN-US style='font-size:12.0pt;color:#333333'>The Practitioner shall ensure
patient’s consent is obtained prior to uploading the prescription/health
records of the patient on the account of the patient during such consultation.</span></p>

<p class=MsoListParagraphCxSpFirst style='text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;color:#333333'>      </span></p>

<p class=MsoListParagraphCxSpLast style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.6.3<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>Terms for
Patients:</span></p>

<p class=MsoNormal style='margin-left:171.5pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'>3.6.3.1<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span
lang=EN-US style='font-size:12.0pt;color:#333333'>The User hereby grants
consent to GLOCO to feature the queries posted by the User on the Consult Basic
model and agrees that any such information provided by the User will be subject
to GLOCO Privacy Policy.</span></p>

<p class=MsoNormal style='margin-left:171.5pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'>3.6.3.2<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span
lang=EN-US style='font-size:12.0pt;color:#333333'>gHealth Features/gHealth
Consultation is intended for general purposes only and is not meant to be used
in emergencies/serious illnesses requiring physical consultation. Further, if
the Practitioner adjudges that a physical examination would be required and
advises ‘in-person consultation’, it is the sole responsibility of the patient,
to book an appointment for physical examination and in-person consultation
whether the same is with the Practitioner listed hereunder or otherwise. In
case of any negligence on the part of the patient in acting on the same and the
condition of the patient deteriorates, GLOCO shall not be held liable. All the
conditions prescribed in Section 3.5 of this Terms of Use shall apply to the
Users.</span></p>

<p class=MsoNormal style='margin-left:171.5pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'>3.6.3.3<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span
lang=EN-US style='font-size:12.0pt;color:#333333'>The User understands and
agrees that gHealth Features/gHealth Consultation is merely a consulting model,
any interactions and associated issues with the Practitioner on GLOCO gHealth
Features/gHealth Consultation including but not limited to the User’s health
issues and/or the User’s experiences is strictly between the User and the
Practitioner. The User shall not hold GLOCO responsible for any such
interactions and associated issues.</span></p>

<p class=MsoNormal style='margin-left:171.5pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'>3.6.3.4<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span
lang=EN-US style='font-size:12.0pt;color:#333333'>As a User, you understand
that GLOCO is not a medical service provider, nor is it involved in providing
any healthcare or medical advice or diagnosis, it shall hence not be
responsible for any outcome from the consultation between the User and the
Practitioner.</span></p>

<p class=MsoNormal style='margin-left:171.5pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'>3.6.3.5<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span
lang=EN-US style='font-size:12.0pt;color:#333333'>gHealth Features/gHealth
Consultation is a platform being made available to Users to assist them to
obtain consultation from Practitioners and does not intend to replace the
physical consultation with the Practitioner.</span></p>

<p class=MsoNormal style='margin-left:171.5pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'>3.6.3.6<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span
lang=EN-US style='font-size:12.0pt;color:#333333'>You understand that, your
conversations with the Practitioner may be retained in our database, for legal
and/or administrative requirements (example: to resolve issues raised by Users
either for refund or to conduct investigation in case of issues raised by
Users).</span></p>

<p class=MsoNormal style='margin-left:171.5pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'>3.6.3.7<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span
lang=EN-US style='font-size:12.0pt;color:#333333'>If you decide to engage with
a Practitioner to procure medical services, you shall do so at your own risk. GLOCO
shall not be responsible for any breach of service or service deficiency by any
Practitioner.</span></p>

<p class=MsoNormal style='margin-left:171.5pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'>3.6.3.8<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span
lang=EN-US style='font-size:12.0pt;color:#333333'>If you decide to use the
payment gateway to make payments online, it is solely at your discretion.
Should there be any issues with regard to the payment not reaching the
respective Practitioner’s account, please reach out to </span><span lang=EN-US><a
href="mailto:infoMY@GLOCO.com."><span style='font-size:12.0pt'>infoMY@GLOCO.com.</span></a></span></p>

<p class=MsoNormal style='margin-left:171.5pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'>3.6.3.9<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><b><span
lang=EN-US style='font-size:12.0pt;color:#333333'>Cancellations/Refunds:</span></b><span
lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;In case of patients,
where the Practitioner has acted in contravention with any
guidelines/applicable laws/rules/regulations, GLOCO shall provide complete
refund to the patient, subject to investigation undertaken by GLOCO. However,
where the cancellation is due to the abusive nature of the patient, such
patient shall not be eligible for any refund and GLOCO/Practitioner shall be
entitled to take any legal action, depending upon the gravity of the matter.<br>
<br>
</span></p>

<p class=MsoListParagraphCxSpFirst style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:54.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-18.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.7<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;color:#333333'>ghealth.com/gHealth Feed</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph'><span lang=EN-US style='font-size:12.0pt;color:#333333'>      </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.7.1<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>ghealth.com/gHealth
feed is an online content platform available on the website, wherein
Practitioners who have created a GLOCO profile and Users who have created a
health account can login and post health and wellness related content.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph'><b><span
lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.7.2<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>A User
can use ghealth.com/gHealth feed by logging in from their health account,
creating original content comprising text, audio, video, images, data or any
combination of the same&nbsp;<b>(“Content”)</b>, and uploading said Content to GLOCO’s
servers. GLOCO will make available to the User a gallery of images licensed by GLOCO
from a third party stock image provider&nbsp;<b>(“GLOCO Gallery”)</b>. The User
can upload their own images or choose an image from the GLOCO Gallery. GLOCO
does not provide any warranty as to the ownership of the intellectual property
in the GLOCO Gallery and the User acknowledges that the User will use the
images from the GLOCO Gallery at their own risk. GLOCO shall post such Content
to ghealth.com/gHealth feed at its own option and subject to these Terms and
Conditions. The Content uploaded via ghealth.com/gHealth feed does not
constitute medical advice and may not be construed as such by any person.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph'><span lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.7.3<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>The User
acknowledges that they are the original authors and creators of any Content
uploaded by them via ghealth.com/gHealth feed and that no Content uploaded by
them would constitute infringement of the intellectual property rights of any
other person. GLOCO reserves the right to remove any Content which it may
determine at its own discretion as violating the intellectual property rights
of any other person. The User agrees to absolve GLOCO from and indemnify GLOCO
against all claims that may arise as a result of any third party intellectual
property right claim that may arise from the user’s uploading of any Content on
ghealth.com/gHealth feed. The User may not use the images in the GLOCO Gallery
for any purpose other than those directly related to the creation and uploading
of Content to ghealth.com/gHealth feed. The User also agrees to absolve GLOCO
from and indemnify GLOCO against all claims that may arise as a result of any
third party intellectual property claim if the User downloads, copies or
otherwise utilizes an image from the GLOCO Gallery for his/her personal or
commercial gain.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph'><span lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.7.4<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>The user
hereby assigns to GLOCO, in perpetuity and worldwide, all intellectual property
rights in any Content created by the User and uploaded by the User via ghealth.com/gHealth
feed.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph'><span lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.7.5<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>GLOCO
shall have the right to edit or remove the Content and any comments in such
manner as it may deem necessary or appropriate from ghealth.com/gHealth feed at
any time.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph'><span lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.7.6<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>The User
agrees not to upload Content which is defamatory, obscene or objectionable in
nature and GLOCO reserves the right to remove any Content which it may determine
at its own discretion to violate these Terms and Conditions or be violative of
any law or statute in force at the time. The User agrees to absolve GLOCO from
and indemnify GLOCO against all claims that may arise as a result of any legal
claim arising from the nature of the Content posted by the User on ghealth.com/gHealth
feed.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph'><span lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.7.7<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>A User
may also use ghealth.com/gHealth feed in order to view original content created
by Practitioners and to create and upload comments on such Content, where
allowed.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph'><span lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.7.8<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>User
acknowledges that the Content on ghealth.com/gHealth feed reflects the views
and opinions of the authors of such Content and do not necessarily reflect the
views of GLOCO.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph'><span lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.7.9<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>User
agrees that the content they access on ghealth.com/gHealth feed does not in any
way constitute medical advice and that the responsibility for any act or
omission by the User arising from the User’s interpretation of the Content, is
solely attributable to the user. The User agrees to absolve GLOCO from and
indemnify GLOCO against all claims that may arise as a result of the User’s
actions resulting from the User’s viewing of Content on ghealth.com/gHealth feed.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph'><span lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.7.10<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><span
lang=EN-US style='font-size:12.0pt;color:#333333'>The User acknowledges that
all intellectual property rights in the Content on ghealth.com/gHealth feed
vests with GLOCO. The User agrees not to infringe upon GLOCO’s intellectual
property by copying or plagiarizing content on ghealth.com/gHealth feed. GLOCO
reserves its right to initiate all necessary legal remedies available to them
in case of such an infringement by the User. All comments created and uploaded
by the User on ghealth.com/gHealth feed will be the sole intellectual property
of GLOCO. The User agrees not to post any comments on ghealth.com/gHealth feed
that violate the intellectual property of any other person. GLOCO reserves the
right to remove any comments which it may determine at its own discretion as
violating the intellectual property rights of any other person. The User agrees
to absolve GLOCO from and indemnify GLOCO against all claims that may arise as
a result of any third party intellectual property right claim that may arise
from the User’s uploading of any comment on ghealth.com/gHealth feed.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph'><span lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.7.11<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><span
lang=EN-US style='font-size:12.0pt;color:#333333'>User agrees not to post any
comments which are defamatory, obscene, objectionable or in nature and GLOCO
reserves the right to remove any comments which it may determine at its own
discretion to violate these Terms and Conditions or be violative of any law or
statute in force at the time. The User agrees to absolve GLOCO from and
indemnify GLOCO against all claims that may arise as a result of any legal
claim arising from the nature of the comments posted by the User on ghealth.com/gHealth
feed.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph'><b><span lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:54.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-18.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.8<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;color:#333333'>CONTENT OWNERSHIP AND COPYRIGHT
CONDITIONS OF ACCESS</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph'><span lang=EN-US style='font-size:12.0pt;color:#333333'>      </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.8.1<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>The
contents listed on the Website are (i) User generated content, or (ii) belong
to GLOCO. The information that is collected by GLOCO directly or indirectly
from the End-Users and the Practitioners shall belong to GLOCO. Copying of the
copyrighted content published by GLOCO on the Website for any commercial purpose
or for the purpose of earning profit will be a violation of copyright and GLOCO
reserves its rights under applicable law accordingly.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph'><b><span
lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.8.2<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>GLOCO
authorizes the User to view and access the content available on or from the
Website solely for ordering, receiving, delivering and communicating only as
per this Agreement. The contents of the Website, information, text, graphics,
images, logos, button icons, software code, design, and the collection,
arrangement and assembly of content on the Website (collectively,&nbsp;<b>&quot;GLOCO
Content&quot;</b>), are the property of GLOCO and are protected under
copyright, trademark and other laws. User shall not modify the GLOCO Content or
reproduce, display, publicly perform, distribute, or otherwise use the GLOCO
Content in any way for any public or commercial purpose or for personal gain.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph'><span lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.8.3<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>User
shall not access the Services for purposes of monitoring their availability,
performance or functionality, or for any other benchmarking or competitive
purposes.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph'><b><span lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:54.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-18.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.9<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;color:#333333'>REVIEWS AND FEEDBACK</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:54.0pt;text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:54.0pt;text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;color:#333333'>By using this Website, you
agree that any information shared by you with GLOCO or with any Practitioner
will be subject to our Privacy Policy.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:54.0pt;text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:54.0pt;text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;color:#333333'>You are solely responsible
for the content that you choose to submit for publication on the Website,
including any feedback, ratings, or reviews (“<b>Critical Content</b>”)
relating to Practitioners or other healthcare professionals or other service
providers. GLOCO disclaims all responsibility with respect to the content of
Critical Content. GLOCO shall not be liable to pay any consideration to any
User for re-publishing any content across any of its platforms.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:54.0pt;text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpLast style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:54.0pt;text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;color:#333333'>Your publication of reviews
and feedback on the Website is governed by Clause 5 of these Terms. Without
prejudice to the detailed terms stated in Clause 5, you hereby agree not to
post or publish any content on the Website that (a) infringes any third-party
intellectual property or publicity or privacy rights, or (b) violates any
applicable law or regulation. GLOCO, at its sole discretion, may choose not to
publish your reviews and feedback, if so required by applicable law, and in
accordance with Clause 5 of these Terms. You agree that GLOCO may contact you through
telephone, email, SMS, or any other electronic means of communication for the
purpose of:</span></p>

<p class=MsoNormal style='margin-left:68.6pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-68.6pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>v.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>Obtaining
feedback in relation to Website or GLOCO’s services; and/or</span></p>

<p class=MsoNormal style='margin-left:68.6pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-68.6pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>vi.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>Obtaining
feedback in relation to any Practitioners listed on the Website; and/or</span></p>

<p class=MsoNormal style='margin-left:68.6pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-68.6pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>vii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>Resolving
any complaints, information, or queries by Practitioners regarding your
Critical Content;</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:6.0pt;
margin-left:51.45pt;text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;color:#333333'>and you agree to provide your
fullest co-operation further to such communication by GLOCO. GLOCO’s Feedback
Collection and Fraud Detection Policy, is annexed as the Schedule hereto, and
remains subject always to these Terms.</span></p>

<p class=MsoListParagraphCxSpFirst style='text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;color:#333333'>      </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:54.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-18.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.10<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>RECORDS</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:54.0pt;text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpLast style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:54.0pt;text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;color:#333333'>GLOCO may provide End-Users
with a free facility known as ‘Records’ on its mobile application ‘GHealth Club’.
Information available in your Records is of two types:</span></p>

<p class=MsoNormal style='margin-left:68.6pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-68.6pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>viii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>User-created:
Information uploaded by you or information generated during your interaction
with GLOCO ecosystem, eg: appointment, medicine order placed by you.</span></p>

<p class=MsoNormal style='margin-left:68.6pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-68.6pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>ix.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>Practice-created:
Health Records generated by your interaction with a Practitioner who uses ‘GHealth
Club’ or other Services of GLOCO software.</span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:6.0pt;
margin-left:51.45pt;text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;color:#333333'>The specific terms relating
to such Health Account are as below, without prejudice to the rest of these
Terms and the Privacy Policy:</span></p>

<p class=MsoListParagraphCxSpFirst style='text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;color:#333333'>      </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.10.1<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><span
lang=EN-US style='font-size:12.0pt;color:#333333'>Your Records is only created
after you have signed up and explicitly accepted these Terms.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.10.2<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><span
lang=EN-US style='font-size:12.0pt;color:#333333'>Any Practice created Health
Record is provided on an as-is basis at the sole intent, risk and
responsibility of the Practitioner and GLOCO does not validate the said
information and makes no representation in connection therewith. You should contact
the relevant Practitioner in case you wish to point out any discrepancies or
add, delete, or modify the Health Record in any manner.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.10.3<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><span
lang=EN-US style='font-size:12.0pt;color:#333333'>The Health Records are
provided on an as-is basis. While we strive to maintain the highest levels of
service availability, GLOCO is not liable for any interruption that may be
caused to your access of the Services.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.10.4<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><span
lang=EN-US style='font-size:12.0pt;color:#333333'>The reminder provided by the
Records is only a supplementary way of reminding you to perform your activities
as prescribed by your Practitioner. In the event of any medicine reminders
provided by GLOCO, you should refer to your prescription before taking any
medicines. GLOCO is not liable if for any reason reminders are not delivered to
you or are delivered late or delivered incorrectly, despite its best efforts.
In case you do not wish to receive the reminders, you can switch it off through
the GLOCO app.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.10.5<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><span
lang=EN-US style='font-size:12.0pt;color:#333333'>It is your responsibility to
keep your correct mobile number and email ID updated in the Records. The Health
Records will be sent to the Records associated with this mobile number and/or
email ID. Every time you change any contact information (mobile or email), we
will send a confirmation. GLOCO is not responsible for any loss or
inconvenience caused due to your failure in updating the contact details with GLOCO.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.10.6<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><span
lang=EN-US style='font-size:12.0pt;color:#333333'>GLOCO uses industry–level
security and encryption to your Health Records. However, GLOCO does not
guarantee to prevent unauthorized access if you lose your login credentials or
they are otherwise compromised. In the event you are aware of any unauthorized
use or access, you shall immediately inform GLOCO of such unauthorized use or
access. Please safeguard your login credentials and report any actual suspected
breach of account to </span><span lang=EN-US><a href="mailto:infoMY@GLOCO.com"><span
style='font-size:12.0pt'>infoMY@GLOCO.com</span></a></span><span lang=EN-US
style='font-size:12.0pt;color:#333333'>. </span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.10.7<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><span
lang=EN-US style='font-size:12.0pt;color:#333333'>If you access your
dependents’ Health Records by registering your dependents with your own
Records, you are deemed to be responsible for the Health Records of your
dependents and all obligations that your dependents would have had, had they
maintained their own separate individual Records. You agree that it shall be
your sole responsibility to obtain prior consent of your dependent and shall
have right to share, upload and publish any sensitive personal information of
your dependent. GLOCO assumes no responsibility for any claim, dispute or
liability arising in this regard, and you shall indemnify GLOCO and its
officers against any such claim or liability arising out of unauthorized use of
such information.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.10.8<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><span
lang=EN-US style='font-size:12.0pt;color:#333333'>In case you want to delete
your Records, you can do so by contacting our service support team. However
only your account and any associated Health Records will be deleted, and your
Health Records stored by your Practitioners will continue to be stored in their
respective accounts.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.10.9<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><span
lang=EN-US style='font-size:12.0pt;color:#333333'>You may lose your “User created”
record, if the data is not synced with the server.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.10.10</span><span
lang=EN-US style='font-size:12.0pt;color:#333333'>If the Health Record is
unassessed for a stipulated time, you may not be able to access your Health
Records due to security reasons.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.10.11</span><span
lang=EN-US style='font-size:12.0pt;color:#333333'>GLOCO is not liable if for
any reason, Health Records are not delivered to you or are delivered late
despite its best efforts.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.10.12</span><span
lang=EN-US style='font-size:12.0pt;color:#333333'>The Health Records are shared
with the phone numbers that are provided by your Practitioner. GLOCO is not
responsible for adding the Heath Records with incorrect numbers if those
incorrect numbers are provided by the Practitioner.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.10.13</span><span
lang=EN-US style='font-size:12.0pt;color:#333333'>GLOCO is not responsible or
liable for any content, fact, Health Records, medical deduction or the language
used in your Health Records whatsoever. Your Practitioner is solely responsible
and liable for your Health Records and any information provided to us including
but not limited to the content in them.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.10.14</span><span
lang=EN-US style='font-size:12.0pt;color:#333333'>GLOCO has the ability in its
sole discretion to retract Health Records without any prior notice if they are
found to be shared incorrectly or inadvertently.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.10.15</span><span
lang=EN-US style='font-size:12.0pt;color:#333333'>GLOCO will follow the law of
land in case of any constitutional court or jurisdiction mandates to share the
Health Records for any reason.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.10.16</span><span
lang=EN-US style='font-size:12.0pt;color:#333333'>You agree and acknowledge
that GLOCO may need to access the Health Record for cases such as any technical
or operational issue of the End User in access or ownership of the Records.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.10.17</span><span
lang=EN-US style='font-size:12.0pt;color:#333333'>You acknowledge that the
Practitioners you are visiting may engage GLOCO's software or third party
software for the purposes of the functioning of the Practitioner’s business and
GLOCO's services including but not limited to the usage and for storage of
Records (as defined in Section 3.10) in Malaysia and outside Malaysia, in
accordance with the applicable laws.</span></p>

<p class=MsoListParagraphCxSpLast style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:90.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-36.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>3.10.18</span><span
lang=EN-US style='font-size:12.0pt;color:#333333'>To the extent that your
Records have been shared with GLOCO or stored on any of the GLOCO products used
by Practitioner’s you are visiting, and may in the past have visited, You
hereby agree to the storage of your Records by GLOCO pertaining to such
previously visited clinics and hospitals who have tie ups with GLOCO for the
purposes of their business and for GLOCO's services including but not limited
to the usage and for storage of Records (as defined in Section 3.10) in Malaysia
and outside Malaysia, in accordance with the applicable laws and further agree,
upon creation of your account with GLOCO, to the mapping of such Records as may
be available in GLOCO’s database to your User account.</span></p>

<p class=MsoNormal style='margin-top:8.55pt;margin-right:0cm;margin-bottom:
8.55pt;margin-left:0cm;text-align:justify;text-justify:inter-ideograph'><b><span
lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpFirst style='text-align:justify;text-justify:inter-ideograph;
text-indent:-18.0pt'><b><span lang=EN-US style='font-size:12.0pt;color:#333333'>4.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></b><b><span
lang=EN-US style='font-size:12.0pt;color:#333333'>RIGHTS AND OBLIGATIONS
RELATING TO CONTENT</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:36.0pt;text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpLast style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:54.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-18.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>4.1<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;color:#333333'>GLOCO hereby informs Users that they are
not permitted to host, display, upload, modify, publish, transmit, update or
share any information that:</span></p>

<p class=MsoNormal style='margin-left:120.05pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>i.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>belongs
to another person and to which the User does not have any right to;</span></p>

<p class=MsoNormal style='margin-left:120.05pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>ii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>is
grossly harmful, harassing, blasphemous, defamatory, obscene, pornographic,
pedophilic, libelous, invasive of another's privacy, hateful, or racially,
ethnically objectionable, disparaging, relating or encouraging money laundering
or gambling, or otherwise unlawful in any manner whatever;</span></p>

<p class=MsoNormal style='margin-left:120.05pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>iii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>harm
minors in any way;</span></p>

<p class=MsoNormal style='margin-left:120.05pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>iv.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>infringes
any patent, trademark, copyright or other proprietary rights;</span></p>

<p class=MsoNormal style='margin-left:120.05pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>v.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>violates
any law for the time being in force;</span></p>

<p class=MsoNormal style='margin-left:120.05pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>vi.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>deceives
or misleads the addressee about the origin of such messages or communicates any
information which is grossly offensive or menacing in nature;</span></p>

<p class=MsoNormal style='margin-left:120.05pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>vii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>impersonate
another person;</span></p>

<p class=MsoNormal style='margin-left:120.05pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>viii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>contains
software viruses or any other computer code, files or programs designed to
interrupt, destroy or limit the functionality of any computer resource;</span></p>

<p class=MsoNormal style='margin-left:120.05pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>ix.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>threatens
the unity, integrity, defence, security or sovereignty of Malaysia, friendly
relations with foreign states, or public order or causes incitement to the
commission of any cognizable offence or prevents investigation of any offence
or is insulting any other nation.</span></p>

<p class=MsoListParagraph style='margin-top:8.55pt;margin-right:0cm;margin-bottom:
8.55pt;margin-left:54.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-18.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>4.2<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;color:#333333'>Users are also prohibited from:</span></p>

<p class=MsoNormal style='margin-left:120.05pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>x.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>violating
or attempting to violate the integrity or security of the Website or any GLOCO
Content;</span></p>

<p class=MsoNormal style='margin-left:120.05pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>xi.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>transmitting
any information (including job posts, messages and hyperlinks) on or through
the Website that is disruptive or competitive to the provision of Services by GLOCO;</span></p>

<p class=MsoNormal style='margin-left:120.05pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>xii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>intentionally
submitting on the Website any incomplete, false or inaccurate information;</span></p>

<p class=MsoNormal style='margin-left:120.05pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>xiii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>making
any unsolicited communications to other Users;</span></p>

<p class=MsoNormal style='margin-left:120.05pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>xiv.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>using any
engine, software, tool, agent or other device or mechanism (such as spiders,
robots, avatars or intelligent agents) to navigate or search the Website;</span></p>

<p class=MsoNormal style='margin-left:120.05pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>xv.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>attempting
to decipher, decompile, disassemble or reverse engineer any part of the
Website;</span></p>

<p class=MsoNormal style='margin-left:120.05pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>xvi.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>copying
or duplicating in any manner any of the GLOCO Content or other information
available from the Website;</span></p>

<p class=MsoNormal style='margin-left:120.05pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>xvii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>framing
or hot linking or deep linking any GLOCO Content.</span></p>

<p class=MsoNormal style='margin-left:120.05pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>xviii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>circumventing
or disabling any digital rights management, usage rules, or other security
features of the Software.</span></p>

<p class=MsoListParagraphCxSpFirst style='margin-bottom:8.55pt;text-align:justify;
text-justify:inter-ideograph'><span lang=EN-US style='font-size:12.0pt;
color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:54.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-18.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>4.3<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;color:#333333'>GLOCO, upon obtaining knowledge by
itself or been brought to actual knowledge by an affected person in writing or
through email signed with electronic signature about any such information as
mentioned above, shall be entitled to disable such information that is in
contravention of Clauses 4.1 and 4.2. GLOCO shall also be entitled to preserve
such information and associated records for at least 90 (ninety) days for
production to governmental authorities for investigation purposes.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:54.0pt;text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:54.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-18.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>4.4<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;color:#333333'>In case of non-compliance with any
applicable laws, rules or regulations, or the Agreement (including the Privacy
Policy) by a User, GLOCO has the right to immediately terminate the access or
usage rights of the User to the Website and Services and to remove
non-compliant information from the Website.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph'><span lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:54.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-18.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>4.5<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;color:#333333'>GLOCO may disclose or transfer
User-generated information to its affiliates or governmental authorities in
such manner as permitted or required by applicable law, and you hereby consent
to such transfer. The applicable law only permits GLOCO to transfer sensitive
personal data or information including any information, to any other body
corporate or a person in Malaysia, or located in any other country, that
ensures the same level of data protection that is adhered to by GLOCO as provided
for under the applicable law, only if such transfer is necessary for the
performance of the lawful contract between GLOCO or any person on its behalf
and the User or where the User has consented to data transfer.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph'><span lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpLast style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:0cm;margin-left:54.0pt;margin-bottom:.0001pt;text-align:justify;
text-justify:inter-ideograph;text-indent:-18.0pt'><span lang=EN-US
style='font-size:12.0pt;color:#333333'>4.6<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>GLOCO
respects the intellectual property rights of others and we do not hold any
responsibility for any violations of any intellectual property rights.</span></p>

<p class=MsoNormal style='margin-bottom:8.55pt;text-align:justify;text-justify:
inter-ideograph'><b><span lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpFirst style='text-align:justify;text-justify:inter-ideograph;
text-indent:-18.0pt'><b><span lang=EN-US style='font-size:12.0pt;color:#333333'>5.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></b><b><span
lang=EN-US style='font-size:12.0pt;color:#333333'>TERMINATION</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:36.0pt;text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpLast style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:54.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-18.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>5.1<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;color:#333333'>GLOCO reserves the right to suspend or
terminate a User’s access to the Website and the Services with or without
notice and to exercise any other remedy available under law, in cases where,</span></p>

<p class=MsoNormal style='margin-left:120.05pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>xix.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>Such User
breaches any terms and conditions of the Agreement;</span></p>

<p class=MsoNormal style='margin-left:120.05pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>xx.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>A third party
reports violation of any of its right as a result of your use of the Services;</span></p>

<p class=MsoNormal style='margin-left:120.05pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>xxi.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>GLOCO is
unable to verify or authenticate any information provide to GLOCO by a User;</span></p>

<p class=MsoNormal style='margin-left:120.05pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>xxii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>GLOCO has
reasonable grounds for suspecting any illegal, fraudulent or abusive activity
on part of such User; or</span></p>

<p class=MsoNormal style='margin-left:120.05pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>xxiii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>GLOCO
believes in its sole discretion that User’s actions may cause legal liability
for such User, other Users or for GLOCO or are contrary to the interests of the
Website.</span></p>

<p class=MsoListParagraphCxSpFirst style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:54.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-18.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>5.2<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;color:#333333'>Once temporarily suspended, indefinitely
suspended or terminated, the User may not continue to use the Website under the
same account, a different account or re-register under a new account. On
termination of an account due to the reasons mentioned herein, such User shall
no longer have access to data, messages, files and other material kept on the
Website by such User. The User shall ensure that he/she/it has continuous
backup of any medical services the User has rendered in order to comply with
the User’s record keeping process and practices.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:54.0pt;text-align:justify;text-justify:inter-ideograph'><b><span
lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt'><b><span lang=EN-US style='font-size:12.0pt;
color:#333333'>6.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></b><b><span lang=EN-US style='font-size:12.0pt;color:#333333'>LIMITATION
OF LIABILITY</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph'><b><span lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpLast style='text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;color:#333333'>In no event, including but
not limited to negligence, shall GLOCO, or any of its directors, officers,
employees, agents or content or service providers (collectively, the “Protected
Entities”) be liable for any direct, indirect, special, incidental,
consequential, exemplary or punitive damages arising from, or directly or
indirectly related to, the use of, or the inability to use, the Website or the
content, materials and functions related thereto, the Services, User’s
provision of information via the Website, lost business or lost End-Users, even
if such Protected Entity has been advised of the possibility of such damages.
In no event shall the Protected Entities be liable for:</span></p>

<p class=MsoNormal style='margin-left:68.6pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-68.6pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>x.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>provision
of or failure to provide all or any service by Practitioners to End- Users
contacted or managed through the Website;</span></p>

<p class=MsoNormal style='margin-left:68.6pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-68.6pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>xi.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>any
content posted, transmitted, exchanged or received by or on behalf of any User
or other person on or through the Website;</span></p>

<p class=MsoNormal style='margin-left:68.6pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-68.6pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>xii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>any
unauthorized access to or alteration of your transmissions or data; or</span></p>

<p class=MsoNormal style='margin-left:68.6pt;text-align:justify;text-justify:
inter-ideograph;text-indent:-68.6pt'><span lang=EN-US style='font-size:12.0pt;
color:#333333'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>xiii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US style='font-size:12.0pt;color:#333333'>any other
matter relating to the Website or the Service.</span></p>

<p class=MsoListParagraph style='text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;color:#333333'>In no event shall the total
aggregate liability of the Protected Entities to a User for all damages,
losses, and causes of action (whether in contract or tort, including, but not
limited to, negligence or otherwise) arising from this Agreement or a User’s
use of the Website or the Services exceed, in the aggregate RM100.00 (Ringgit
Malaysia One Hundred Only).</span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpFirst style='text-align:justify;text-justify:inter-ideograph;
text-indent:-18.0pt'><b><span lang=EN-US style='font-size:12.0pt;color:#333333'>7.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></b><b><span
lang=EN-US style='font-size:12.0pt;color:#333333'>RETENTION AND REMOVAL</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph'><b><span lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpLast style='text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;color:#333333'>GLOCO may retain such
information collected from Users from its Website or Services for as long as
necessary, depending on the type of information; purpose, means and modes of
usage of such information; and according to the Personal Data Protection Act
2010. Computer web server logs may be preserved as long as administratively
necessary.</span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpFirst style='text-align:justify;text-justify:inter-ideograph;
text-indent:-18.0pt'><b><span lang=EN-US style='font-size:12.0pt;color:#333333'>8.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></b><b><span
lang=EN-US style='font-size:12.0pt;color:#333333'>APPLICABLE LAW AND DISPUTE
SETTLEMENT</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:36.0pt;text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:54.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-18.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>8.1<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;color:#333333'>You agree that this Agreement and any
contractual obligation between GLOCO and User will be governed by the laws of
Malaysia.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:54.0pt;text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:54.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-18.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>8.2<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;color:#333333'>Any dispute, claim or controversy
arising out of or relating to this Agreement, including the determination of
the scope or applicability of this Agreement to arbitrate, or your use of the
Website or the Services or information to which it gives access, shall be determined
by arbitration in Malaysia, before a sole arbitrator appointed by GLOCO. The
arbitration shall be conducted in the English language and the UNCITRAL
Arbitration Rules shall apply. The award shall be final and binding on the
parties to the dispute.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph'><span lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:54.0pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-18.0pt'><span lang=EN-US style='font-size:12.0pt;color:#333333'>8.3<span
style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span lang=EN-US
style='font-size:12.0pt;color:#333333'>Subject to the above Clause 8.2, the
courts at Malaysia shall have exclusive jurisdiction over any disputes arising
out of or in relation to this Agreement, your use of the Website or the
Services or the information to which it gives access.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph'><span lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt'><b><span lang=EN-US style='font-size:12.0pt;
color:#333333'>9.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></b><b><span lang=EN-US style='font-size:12.0pt;color:#333333'>CONTACT
INFORMATION </span></b></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph'><b><span lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph'><span lang=EN-US style='font-size:12.0pt;color:#333333'>If a
User has any questions concerning GLOCO, the Website, this Agreement, the
Services, or anything related to any of the foregoing, GLOCO customer support
can be reached at the following email address: infoMY@gloco.com or via the contact
information available from </span><span lang=EN-US><a
href="http://www.ghealth.com"><span style='font-size:12.0pt'>www.ghealth.com</span></a></span><span
lang=EN-US style='font-size:12.0pt;color:#333333'>.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:54.0pt;text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt'><b><span lang=EN-US style='font-size:12.0pt;
color:#333333'>10.<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span></b><b><span
lang=EN-US style='font-size:12.0pt;color:#333333'>SEVERABILITY</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph'><b><span lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph'><span lang=EN-US style='font-size:12.0pt;color:#333333'>.If
any provision of the Agreement is held by a court of competent jurisdiction or
arbitral tribunal to be unenforceable under applicable law, then such provision
shall be excluded from this Agreement and the remainder of the Agreement shall
be interpreted as if such provision were so excluded and shall be enforceable
in accordance with its terms; provided however that, in such event, the
Agreement shall be interpreted so as to give effect, to the greatest extent
consistent with and permitted by applicable law, to the meaning and intention
of the excluded provision as determined by such court of competent jurisdiction
or arbitral tribunal.</span></p>

<p class=MsoListParagraphCxSpMiddle style='margin-top:8.55pt;margin-right:0cm;
margin-bottom:8.55pt;margin-left:54.0pt;text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph;text-indent:-18.0pt'><b><span lang=EN-US style='font-size:12.0pt;
color:#333333'>11.<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span></b><b><span
lang=EN-US style='font-size:12.0pt;color:#333333'>WAIVER</span></b></p>

<p class=MsoListParagraphCxSpMiddle style='text-align:justify;text-justify:
inter-ideograph'><b><span lang=EN-US style='font-size:12.0pt;color:#333333'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpLast style='text-align:justify;text-justify:inter-ideograph'><span
lang=EN-US style='font-size:12.0pt;color:#333333'>No provision of this
Agreement shall be deemed to be waived and no breach excused, unless such
waiver or consent shall be in writing and signed by GLOCO. Any consent by GLOCO
to, or a waiver by GLOCO of any breach by you, whether expressed or implied,
shall not constitute consent to, waiver of, or excuse for any other different
or subsequent breach.</span></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><b><u><span
lang=EN-US style='font-size:12.0pt;color:#333333'><span style='text-decoration:
 none'>&nbsp;</span></span></u></b></p>

<p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph'><b><span
lang=EN-US style='font-size:12.0pt;color:#333333'>YOU HAVE READ THESE TERMS OF
USE AND AGREE TO ALL OF THE PROVISIONS CONTAINED ABOVE</span></b></p>

</div>

</body>

</html>
