<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>gHealth</title>
    <style type="text/css" media="screen">

        .ExternalClass * {line-height: 100%}

        /* Début style responsive (via media queries) */

        @media only screen and (max-width: 480px) {
            *[id=email-penrose-conteneur] {width: 100% !important;}
            table[class=resp-full-table] {width: 100%!important; clear: both;}
            td[class=resp-full-td] {width: 100%!important; clear: both;}
            img[class="email-penrose-img-header"] {width:100% !important; max-width: 340px !important;}
        }

        /* Fin style responsive */

    </style>

</head>
<body style="background-color:#ecf0f1">
<div align="center" style="background-color:#ecf0f1;">

    <!-- Début en-tête -->

    <table id="email-penrose-conteneur" width="660" align="center" style="padding:20px 0px;" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td>
                <table width="660" class="resp-full-table" align="center" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="50%" style="text-align:left;">
                            <a href="#" style="text-decoration:none;"><h3 style="font-size: 25px;font-family: 'Helvetica Neue', helvetica, arial, sans-serif;font-weight: bold;color: #6B6B6B;margin: 0;">gHealth</h3></a>
                        </td>
                        <td width="50%" style="text-align:right;">
                            <table align="right" border="0" cellspacing="0" cellpadding="0">

                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <!-- Fin en-tête -->

    <table id="email-penrose-conteneur" width="660" align="center" style="border-right:1px solid #e2e8ea; border-bottom:1px solid #e2e8ea; border-left:1px solid #e2e8ea; background-color:#ffffff;" border="0" cellspacing="0" cellpadding="0">

        <!-- Début bloc "mise en avant" -->

        <tr>
            <td style="background-color:#2ecc71">
                <table width="660" class="resp-full-table" align="center" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td class="resp-full-td" valign="top" style="padding:20px; text-align:center;">
							<span style="font-size:25px; font-family:'Helvetica Neue', helvetica, arial, sans-serif; font-weight:700; color:#ffffff"><a href="#" style="color:#ffffff; outline:none; text-decoration:none;">Thank you for choosing gHealth
							    </a></span>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr>
            <td>
                <table width="660" class="resp-full-table" align="center" border="0" cellspacing="0" cellpadding="0" style="padding:20px;">
                    <tr>

                        <td width="100%">

                            <!-- Début bloc info 1 -->

                            <table width="300" align="left" class="resp-full-table" style="margin:0 auto;float:none;background-color:#2ecc71; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td width="100%" class="resp-full-td" valign="top" style="text-align: justify; padding:20px;">
                                        <a href="#" style="outline:none; text-decoration:none"><span style="font-size:25px; font-weight: bold; font-family:'Helvetica Neue', helvetica, arial, sans-serif; color:#ffffff;">Invoice</span></a><br />

                                        <hr align="left" style="width:100px; margin-left:0px; text-align:left; background-color:#ffffff; color:#ffffff; height: 2px; border: 0 none;" />

                                        <div style="margin: 10px 0px;font-size:16px; font-family:'Helvetica Neue', helvetica, arial, sans-serif; color:#ffffff;font-weight: 700;"><span>Doctor</span><span style="float:right;">Dr. {{$doctor->first_name." ".$doctor->last_name}}</span></div>

                                        <div style="margin: 10px 0px;font-size:16px; font-family:'Helvetica Neue', helvetica, arial, sans-serif; color:#ffffff;font-weight: 700;"><span>User</span><span style="float:right;">{{$user->first_name." ".$user->last_name}}</span></div>

                                        <?php if($requests->request_type == 1){
                                            $request_type = "Chat";
                                        }elseif($requests->request_type == 2){
                                            $request_type = "Phone";
                                        }elseif($requests->request_type == 3){
                                            $request_type = "Video";
                                        }elseif($requests->request_type == 4){
                                            $request_type = "Ondemand";
                                        }elseif($requests->request_type == 5){
                                            $request_type = "Booking";
                                        }?>

                                        <div style="margin: 10px 0px;font-size:16px; font-family:'Helvetica Neue', helvetica, arial, sans-serif; color:#ffffff;font-weight: 700;"><span>Consult Type</span><span style="float:right;"><?php echo  $request_type;?></span></div>

                                        <div style="margin: 10px 0px;font-size:16px; font-family:'Helvetica Neue', helvetica, arial, sans-serif; color:#ffffff;font-weight: 700;"><span>Payment Type</span><span style="float:right;">{{$requests->payment_mode}}</span></div>

                                        <?php if($requests->request_type == 5){?>

                                                <div style="margin: 10px 0px;font-size:16px; font-family:'Helvetica Neue', helvetica, arial, sans-serif; color:#ffffff;font-weight: 700;"><span>Booking Fee</span><span style="float:right;">{{$doctor->booking_fee}}</span></div>

                                        <?php }elseif($requests->request_type == 4){ ?>

                                                <div style="margin: 10px 0px;font-size:16px; font-family:'Helvetica Neue', helvetica, arial, sans-serif; color:#ffffff;font-weight: 700;"><span>Consultation Fee</span><span style="float:right;">{{$doctorFee->ondemand_fee}}</span></div>

                                                <div style="margin: 10px 0px;font-size:16px; font-family:'Helvetica Neue', helvetica, arial, sans-serif; color:#ffffff;font-weight: 700;"><span>Distance</span><span style="float:right;">{{$requests->distance}}</span></div>

                                                <div style="margin: 10px 0px;font-size:16px; font-family:'Helvetica Neue', helvetica, arial, sans-serif; color:#ffffff;font-weight: 700;"><span>Distance Cost</span><span style="float:right;">{{$distanceCost}}</span></div>


                                        <?php }elseif($requests->request_type == 3){ ?>

                                        <div style="margin: 10px 0px;font-size:16px; font-family:'Helvetica Neue', helvetica, arial, sans-serif; color:#ffffff;font-weight: 700;"><span>Consultation Fee</span><span style="float:right;">{{$doctorFee->video_fee}}</span></div>

                                        <?php }elseif($requests->request_type == 2){ ?>

                                        <div style="margin: 10px 0px;font-size:16px; font-family:'Helvetica Neue', helvetica, arial, sans-serif; color:#ffffff;font-weight: 700;"><span>Consultation Fee</span><span style="float:right;">{{$doctorFee->phone_fee}}</span></div>

                                        <?php } elseif($requests->request_type == 1){ ?>

                                        <div style="margin: 10px 0px;font-size:16px; font-family:'Helvetica Neue', helvetica, arial, sans-serif; color:#ffffff;font-weight: 700;"><span>Consultation Fee</span><span style="float:right;">{{$doctorFee->chat_fee}}</span></div>

                                        <?php } ?>


                                        <hr style="background-color:#ffffff; color:#ffffff; height: 1px; border: 0 none">

                                        <div style="font-weight:bold;margin: 10px 0px;font-size:16px; font-family:'Helvetica Neue', helvetica, arial, sans-serif; color:#ffffff;font-weight: 700;"><span>Total</span><span style="float:right;"> {{$requests->amount}}</span></div>
                                    </td>
                                </tr>

                            </table>

                        </td>
                    </tr>
                </table>
            </td>
        </tr>

        <!-- Fin bloc "mise en avant" -->
        <!-- Début article 1 -->

        <tr>
            <td style="border-bottom: 1px solid #e2e8ea">
                <table width="660" class="resp-full-table" align="center" border="0" cellspacing="0" cellpadding="0" style="padding:20px;">
                    <tr>
                        <td width="100%">

                            <table width="100%" align="right" class="resp-full-table" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td width="100%" class="resp-full-td" valign="top" style="text-align : justify;">
                                        <div style="background:#2ECC71;padding: 10px;font-size:25px; font-family:'Helvetica Neue', helvetica, arial, sans-serif; font-weight:700; color:#ffffff">TOTAL<span style="float:right;">{{$requests->amount}}</span></div>
                                    </td>
                                </tr>

                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

        <!-- Fin article 1 -->

    </table>

    <!-- Début footer -->

    <table id="email-penrose-conteneur" width="600" align="center" style="padding:20px 0px;" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td>
                <table width="600" class="resp-full-table" align="center" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="100%" class="resp-full-td" style="text-align: center;">

                            <hr align="left" style="margin-left:0px; text-align:left; background-color:#aeb2b3; color:#aeb2b3; height: 1px; border: 0 none;" />

                            <span style="font-size:12px; font-family:'Helvetica Neue', helvetica, arial, sans-serif; color:#aeb2b3">gHealth</span>




                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <!-- Fin footer -->

</div>
</body>
</html>