@extends('home.patient.layouts.layout-web2')
@section('content')			
<div class="outer_seven" ng-app="categoriesApp" ng-controller="mainCtrl">

    <!-- ONLINE CONSULTATION BLOCK -->
    <div class="section-two">
        <div class="container">
            
            <ng-view></ng-view>
        
            <div class="col-sm-4 col-md-4 margin80">
                <div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                        <div class="panel-heading collapse-blue">
                            <h4 class="panel-title white-text">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" data-target="#collapse1" id="online_consult" href="#online-consult"><i class="fa fa-laptop"></i>&nbsp;&nbsp;Online Consultation</a>
                            </h4>
                        </div>
                        <div id="collapse1" class="panel-collapse collapse in">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-xl-12">
                                        <div class="search-group">
                                            <div class="select-option col-xs-12 col-sm-12 col-md-12 col-xl-12 paddin0">
                                                <select class="form-control btn btn-default" style="border-radius:0px;" ng-model="selectedSpeciality" ng-change="getOnlineConsultDoctorsList()">
                                                    <option value="0">---Choose speciality--</option>
                                                    <option ng-repeat="speciality in specialities" value="[[speciality.id]]">[[speciality.speciality]]</option>
                                                </select>
                                            </div>
                                            <!-- <div class="search-div col-xs-12 col-sm-12 col-md-12 col-xl-4 paddin0">
                                                <input type="text" class="form-control" placeholder="Specialities,Doctors,Clinics,Symptoms,etc.," style="border-radius:0px;">
                                            </div> -->
                                        </div>
                                    </div>
                                </div>
                                <!-- <h5><b>Filter Result</b></h5>
                                <div class="row">
                                    <div class="form-group col-lg-4 col-xs-12">
                                        <i class="fa fa-dot-circle-o" aria-hidden="true"></i>&nbsp;&nbsp;Radius								
                                    </div>
                                    <div class="form-group col-lg-8 col-xs-12">
                                        <input id="ex1" data-slider-id='ex1Slider' type="text" data-slider-min="0" data-slider-max="10" data-slider-step="1" data-slider-value="4"/>	
                                    </div>
                                </div> -->
                                <!-- <div class="row">
                                    <div class="form-group">
                                        <div class="form-group col-lg-4 col-xs-12">
                                            <i class="fa fa-dot-circle-o" aria-hidden="true"></i>&nbsp;&nbsp;Review	
                                            <input type="range" value="3" step="0.25" id="backing7">
                                            <div class="rateit" data-rateit-backingfld="#backing7" data-rateit-resetable="false"  data-rateit-ispreset="true"
                                                data-rateit-min="0" data-rateit-max="5">
                                            </div>
                                        </div>
                                        <div class="form-group col-lg-8 col-xs-12">
                                            <input id="ex2" data-slider-id='ex2Slider' type="text" data-slider-min="0" data-slider-max="10" data-slider-step="1" data-slider-value="4"/>
                                        </div>
                                    </div>
                                </div> -->
                                <!-- </div> -->
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading collapse-blue">
                            <h4 class="panel-title white-text">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" data-target="#collapse2" id="book_appointment" href="#book_appointment">									    
                                <i class="fa fa-calendar"></i>&nbsp;&nbsp;Book Appointment</a>
                            </h4>
                        </div>
                        <div id="collapse2" class="panel-collapse collapse">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-xl-12">
                                        <div class="search-group">
                                            <!-- <div class="search-div col-xs-12 col-sm-12 col-md-12 col-xl-12 paddin0">
                                                <input type="text" class="form-control" placeholder="Type" style="border-radius:0px;">
                                            </div>
                                            <div class="search-div col-xs-12 col-sm-12 col-md-12 col-xl-12 paddin0">
                                                <input type="text" class="form-control" placeholder="Location" style="border-radius:0px;">
                                            </div> -->
                                            <div class="select-option col-xs-12 col-sm-12 col-md-12 col-xl-12 paddin0">
                                                <select class="form-control btn btn-default" style="border-radius:0px;" ng-model="selectedSpeciality" ng-change="getDoctorListBooking()">
                                                    <option value="0">---Choose speciality--</option>
                                                    <option ng-repeat="speciality in specialities" value="[[speciality.id]]">[[speciality.speciality]]</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- <h5><b>Filter Result</b></h5> -->
                                <!-- <div class="row">
                                    <div class="form-group col-lg-4 col-xs-12">
                                        <i class="fa fa-dot-circle-o" aria-hidden="true"></i>&nbsp;&nbsp;Radius								
                                    </div>
                                    <div class="form-group col-lg-8 col-xs-12">
                                        <input id="ex3" data-slider-id='ex3Slider' type="text" data-slider-min="0" data-slider-max="10" data-slider-step="1" data-slider-value="4"/>
                                    </div>
                                </div> -->
                                <!-- <div class="row">
                                    <div class="form-group">
                                        <div class="form-group col-lg-4 col-xs-12">
                                            <i class="fa fa-dot-circle-o" aria-hidden="true"></i>&nbsp;&nbsp;Review	
                                            <input type="range" value="3" step="0.25" id="backing15">
                                            <div class="rateit" data-rateit-backingfld="#backing15" data-rateit-resetable="false"  data-rateit-ispreset="true"
                                                data-rateit-min="0" data-rateit-max="5">
                                            </div>
                                        </div>
                                        <div class="form-group col-lg-8 col-xs-12">
                                            <input id="ex4" data-slider-id='ex4Slider' type="text" data-slider-min="0" data-slider-max="10" data-slider-step="1" data-slider-value="4"/>
                                        </div>
                                    </div>
                                </div> -->
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading collapse-blue">
                            <h4 class="panel-title white-text">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" data-target="#collapse3" id='ask_question'><i class="fa fa-envelope"></i>&nbsp;&nbsp;Ask A Question</a>
                            </h4>
                        </div>
                        <div id="collapse3" class="panel-collapse collapse">
                            <div class="panel-body">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-xl-12 white paddin0">
                                    <div class="search-group">
                                        <div class="search-div col-xs-12 col-sm-12 col-md-8 col-xl-8 paddin0">
                                            <input type="text" class="form-control" placeholder="Keywords" style="border-radius:0px;">
                                        </div>
                                        <div class="search-button col-xs-12 col-sm-12 col-md-4 col-xl-4 paddin0">
                                            <a href="{{url('/user/settings#/ask-question')}}">
                                            <button type="submit" class="btn btn-default search-btn form-control article_view" style="border-radius:0px;">Search</button>
                                            </a>
                                        </div>
                                        <div class="search-button col-xs-12 col-sm-12 col-md-12 col-xl-12 paddin0">
                                            <a href="{{url('/user/home#categories')}}">
                                            <button type="submit" class="btn btn-default page-btn form-control" style="border-radius:0px;"><strong>I WANT TO CONSULT</strong></button>
                                            </a>
                                        </div>
                                        <div class="search-button col-xs-12 col-sm-12 col-md-12 col-xl-12 paddin0">
                                            <a href="{{url('/user/home/articles')}}">
                                            <button type="submit" class="btn btn-default page-btn form-control article_view" style="border-radius:0px;"><strong>HEALTH ARTICLE</strong></button>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="panel panel-default">
                        <div class="panel-heading collapse-blue">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse4" id="health_store_tab"><i class="fa fa-cart-plus"></i>&nbsp;&nbsp;Health Store</a>
                            </h4>
                        </div>
                        <div id="collapse4" class="panel-collapse collapse">
                            <div class="panel-body">
                                <form class="form-group">
                                    <label class="radio-inline">
                                    <input type="radio" name="optradio" id="package">Health Package
                                    </label>
                                    <br>
                                    <label class="radio-inline">
                                    <input type="radio" name="optradio" id="product">Health Product
                                    </label>
                                </form>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-xl-12 paddin0">
                                    <div class="search-group">
                                        <div class="select-option col-xs-12 col-sm-12 col-md-12 col-xl-12 paddin0">
                                            <select class="form-control btn btn-default" style="border-radius:0px;">
                                                <option class="">categories</option>
                                                <option>3</option>
                                            </select>
                                        </div>
                                        <div class="search-div col-xs-12 col-sm-12 col-md-12 col-xl-4 paddin0">
                                            <input type="text" class="form-control" placeholder="Specialities,Doctors,Clinics,Symptoms,etc.," style="border-radius:0px;">
                                        </div>
                                        <div class="search-button col-xs-12 col-sm-12 col-md-12 col-xl-12 paddin0">
                                            <button type="submit" class="btn btn-default search-btn form-control" style="border-radius:0px;">SEARCH</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> -->
                </div>
                <!-- END OF ACCORDATION BLOCK -->
                <!-- EXTRA INFO BLOCK -->
                <!-- <div class="container-fluid">
                    <div class="row">
                        <div class="well" style="height:300px;">
                            <span class="glyphicon glyphicon-record"></span>
                            <label>Top Recommended Package</label>
                            <i>Option for adverticement</i>
                        </div>
                        <label>click here for <a href="/signin" class="blue">Free Registration</a></label>
                        <div class="panel panel-primary">
                            <div class="panel-heading">Popular Health Tips</div>
                            <div class="panel-body health-tip">
                                <a href="#" class="article_link">
                                    <div class="col-sm-6 tip" >
                                        <img src="images/jogging.jpg" width=100% height=100 />
                                        <span class="blue">View this Tip</span>
                                    </div>
                                </a>
                                <a href="#" class="article_link">
                                    <div class="col-sm-6 tip">
                                        <img src="images/parenting.jpg" width=100% height=100 />
                                        <span class="blue">View this Tip</span>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div> -->
                <!-- END OF EXTRA INFO -->
            </div>
        </div>
    </div>
</div>
</div>
<!-- </div>		 -->
<style>

    .white-text
    {
        color: white !important;
    }

    #hover-section{
    background-image: url({{asset('web/images/bg/Keyboard.jpg')}});
    /*display: contents;*/
    position: center center;
    }
    #time_slot{
    display: none;	
    }
    .for_list{
    display: none;
    }
    #booking_calendar > .ui-datepicker-inline
    {
        width: 100%;
    }
    .slots-div
    {
        margin-bottom: 5px
    }
    .slot-btn
    {
        border-radius: 100%;
        width: 70px;
        height: 70px;
        background-color: #9DD5EC;
        box-shadow: 2px 2px 5px rgba(0, 0, 0, 0.27);
        position: relative;
        cursor: pointer;
    }
    .slot-btn:hover
    {
        box-shadow: 4px 4px 23px rgba(0, 0, 0, 0.50);
    }
    .slot-btn > .slot-btn-tick
    {
        top: -5px;
        left: 2px;
        font-size: 24px;
        color: rgba(255, 152, 0, 0.95);
        border: none; */
        border-radius: 0%; 
        padding: 0px;
        position: absolute;
    }
    .slot-btn > .slot-btn-text
    {
        position: relative;
        display: inline-block;
        top: 19px;
        left: 7px; 
        color: white;
    }

    .payment_method_options
    {
        font-size: 30px;
        border: 1px solid rgba(0, 0, 0, 0.29);
        padding: 15px;
        border-radius: 100%;
        box-shadow: 0px 0px 1px rgba(0, 0, 0, 0.38);
        cursor: pointer;
        color: #96CCE4;
        transition: all 0.1s;
    }

    .payment_method_options:hover
    {
        border: 1px solid rgba(0, 0, 0, 0.29);
        box-shadow: 0px 0px 8px rgba(0, 0, 0, 0.60);
    }
    

    .color-payment-method-background
    {
        background-color: rgba(178, 216, 24, 0.44);
    }

    .card-list-item 
    {
        margin-right: 5px;
        margin-left: 5px;
    }

    .card-list-item > .text
    {
        font-size: 16px;
        font-weight: 600;
        color: rgba(0, 0, 0, 0.5);
    }

    .ui-datepicker td
    {
        text-align: center !important;
        padding-bottom: 5px;
    }

    .ui-state-default
    {
        width: 30px;
        height: 30px;
        border-radius: 50%;
        display: inline-block !important;
        padding-top: 6px !important;
        text-align: center !important;
        box-shadow: 0px 0px 11px rgba(0, 0, 0, 0.38);
    }

    .ui-datepicker td span, .ui-datepicker td a
    {
        text-align: center;
    }

    .pac-container {
        z-index: 10000 !important;
    }

    .place_search
    {
        padding: 30px;
        border: 1px solid rgba(0, 0, 0, 0.16);
        margin-bottom: 15px;
    }


    #video-audio-modal .modal-footer .btn
    {
        border-radius: 50%;
        width: 50px;
        height: 50px;
    }

    #video-audio-modal .modal-footer .btn:hover
    {
        border-radius: 50%;
        width: 50px;
        height: 50px;
    }


    #video-audio-modal .modal-content:-webkit-full-screen{
        width: 100%;
        height: 100%;
    }
    #video-audio-modal .modal-content:-moz-full-screen{
        width: 100%;
        height: 100%;
    }
    #video-audio-modal .modal-content:-ms-fullscreen{
        width: 100%;
        height: 100%;
    }
    #video-audio-modal .modal-content:fullscreen 
    {
        width: 100%;
        height: 100%;
    }

    .modal-content:-webkit-full-screen .modal-body{
        height: 100%;
    }
    .modal-content:-moz-full-screen .modal-body{
        height: 100%;
    }
    .modal-content:-ms-fullscreen .modal-body{
        height: 100%;
    }
    .modal-content:fullscreen .modal-body
    {
        height: 100%;
    }

    .modal-content:-webkit-full-screen #opentok-subscriber{
        height: 100% !important;
    }
    .modal-content:-moz-full-screen #opentok-subscriber{
        height: 100% !important;
    }
    .modal-content:-ms-fullscreen #opentok-subscriber{
        height: 100% !important;
    }
    .modal-content:fullscreen #opentok-subscriber
    {
        height: 100% !important;
    }

   #doctor-rating
   {
        width:100% !important;
        text-align:center !important;
   }

   #doctor-rating > div
   {
        display: inline-block;
   }


    @media (min-width: 768px)
    {

        #rating-modal .modal-dialog
        {
            width: 30%;
        }
    }   


    .chat-typing
    {
        position: absolute;
        bottom: 55px;
        left: 19px;
        color: white;
        background: rgba(0, 0, 0, 0.69);
        padding: 3px;
        border-radius: 50px 50px 50px 0px;
        font-size: 10px;
        display: none;
    }

    .chat-typing .fa
    {
        font-size: 10px !important;
        color: white !important;
        border: none !important;
    }

    .yellow
    {
        color:yellow !important;
    }

    .white
    {
        color:white !important;
    }

    .red
    {
        color: red !important;
    }

</style>
@endsection
@section('bottom-scripts')
<script type="text/javascript">

var csrf_token                      = "{{csrf_token()}}";
var online_consult_template_url     = "{{url('user/home/categories/template/online-consult')}}";
var get_specialities_url            = "{{url('get-specialities')}}";
var get_online_consult_doctors_list = "{{url('user/get-online-consult-doctors-list')}}";
var initial_spcialitiy_id           = "{{is_null(request()->cat_id) ? 0 : request()->cat_id}}";
var user_currency                   = "{{$auth_user->currency}}";
var user_fname                      = "{{$auth_user->first_name}}";
var user_lname                      = "{{$auth_user->last_name}}";
var user_photo                      = "{{$auth_user->picture}}";
var google_map_api_key              = "{{env('GOOGLE_MAP_API_KEY')}}";
var book_appointment_template_url   = "{{url('user/home/categories/template/book-appointment')}}";
var get_booking_doctors_list_url    = "{{url('user/get-booking-doctors-list')}}";
var get_available_slots             = "{{url('user/get-available-slots')}}";
var get_saved_cards                 = "{{url('user/get-cards')}}";
var cards_32_base_url               = "{{asset('web/cards_icons/32')}}";
var make_default_card               = "{{url('user/make-default-card')}}";
var book_slot                       = "{{url('user/book-slot')}}";
var create_request_url              = "{{url('user/create-request')}}";
var get_request                     = "{{url('user/get-request')}}";
var generate_opentok                = "{{url('user/opentok/generate/session')}}";
var cancel_request_url              = "{{url('user/cancel-request')}}";
var openTokKey                      = "{{config('opentok.api_key')}}";
var clearDebt                       = "{{url('user/debt/clear')}}";
var rate_doctor                     = "{{url('user/doctor/rating')}}";
var socket_host                     = "{{config('SocketIOServer.socket_server_address')}}";
var socket_port                     = "{{config('SocketIOServer.socket_server_port')}}";
var upload_image_url          = "{{url('user/upload/attachment/image')}}";
var last_ongoing_request      = "{{url('user/request/ongoing')}}";
var make_fav_doctor           = "{{url('user/doctor/make-favourite')}}";
</script>

<script type="text/javascript">
function empty(){}
function initGoogleAutoComplete()
{

    /*google.maps.event.addDomListener(window, 'load', function(){*/

        element = document.getElementById('place_search');
        if(element == null) return;
        var places = new google.maps.places.Autocomplete(element);

        google.maps.event.addListener(places, 'place_changed', function() {
            var place = places.getPlace();
            var latitude = place.geometry.location.lat();
            var longitude = place.geometry.location.lng(); 
            /*$('#booking_latitude').val(latitude).trigger('input');
            $('#booking_longitude').val(longitude).trigger('input');*/

            $('#booking_latitude').val(latitude);
            $('#booking_longitude').val(longitude);
            var scope = angular.element($("#book_slots_payment_modal")).scope();
            scope.$apply(function(){
                scope.bookingLatitude = latitude;
                scope.bookingLongitude = longitude;
            });
        });
        

   /* });*/

}

</script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.2.0/jquery.rateyo.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.2.0/jquery.rateyo.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAP_API_KEY')}}&libraries=places&callback=empty" async defer></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/1.7.3/socket.io.js"></script>
<script src="https://static.opentok.com/v2/js/opentok.min.js"></script>
<script type="text/javascript" src="{{asset('web/js/patient_categories.js')}}"></script>

@endsection