<div class="outer_index_second">
    <div class="outer">
        <nav class="navbar navbar-default">
            <div class="container review_page_outer">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="{{url('user/home')}}">
                    <img src="/web/images/ghealthlogo.png">
                    </a>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav black navbar-right hidden-xs visible-lg hidden-sm">
                        <li>
                            <a href="{{url('user/home')}}">
                            <i class="fa fa-home fa-2x" aria-hidden="true"></i>
                            </a>
                        </li>
                        
                        <li>
                            <a href="{{url('/user/settings#/doctor-messages')}}">
                            <i class="fa fa-envelope fa-2x" aria-hidden="true"></i>
                            </a>
                        </li>
                        <li>
                            <a href="{{url('/user/settings#/schedules')}}">
                            <i class="fa fa-calendar fa-2x" aria-hidden="true"></i>
                            </a>
                        </li>
                        <li>
                            <a href="http://ghealthstore.online/#/?token={{$auth_user->ecom_token}}">
                            <i class="fa fa-cart-arrow-down fa-2x" aria-hidden="true"></i>
                            </a>
                        </li>
                        <li>
                            <a href="{{url('user/settings')}}">
                            <span class="welcome_user">
                            <span class="fa-2x">Hi {{$auth_user->first_name}}</span>
                            <i class="fa fa-user pull-right fa-2x"></i>
                            </span>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav black navbar-right  visible-xs visible-sm">
                        <li>
                            <a href="/index_choice">
                            Home
                            <i class="fa fa-home pull-right fa-19x" aria-hidden="true"></i>
                            </a>
                        </li>
                        <li>
                            <a href="/medical_record">
                            Medical Record
                            <i class="fa fa-book  pull-right fa-19x" aria-hidden="true"></i>
                            </a>
                        </li>
                        <li>
                            <a href="/doc_record">
                            Inbox
                            <i class="fa fa-envelope  pull-right  fa-19x" aria-hidden="true"></i><span class="badge">1</span></i>
                            </a>
                        </li>
                        <li>
                            <a href="/choide">
                            Appointment
                            <i class="fa fa-calendar  pull-right fa-19x" aria-hidden="true"></i>
                            </a>
                        </li>
                        <li>
                            <a href="/empty">
                            Cart
                            <i class="fa fa-cart-arrow-down  pull-right  fa-19x" aria-hidden="true"></i>
                            </a>
                        </li>
                        <li>
                            <a href="/doc_profile">
                            <span class="welcome_user">
                            <span class="fa-2x">Hi {{$auth_user->first_name}}</span>
                            <i class="fa fa-user pull-right fa-19x"></i>
                            </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</div>