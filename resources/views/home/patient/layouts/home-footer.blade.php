
<div class="footer-second">
    <div class="container">
        <div class="row">
            <div class="footer-bottom2">
                <div class="col-sm-5">
                    <h4>Toll Free Number : 1 300 88 6008</h4>
                    <h5><strong>gHealth.com </strong>2015-2020 @ All Rights Reserved.</h5>
                </div>
                <div class="col-sm-3">
                    <h4>
                        <i class="fa fa-globe" aria-hidden="true"></i>
                        www.ghealth.com
                    </h4>
                </div>
                <div class="col-sm-4 text-center">                          
                    <a href="https://www.facebook.com/Ghealthcom-420340984988107/"><i class="fa fa-facebook-square" aria-hidden="true"></i></a>
                    <a href="https://plus.google.com/u/1/114922129084557930193"><i class="fa fa-google-plus-square" aria-hidden="true"></i></a>
                    <a href="https://twitter.com/gHealthcom"><i class="fa fa-twitter-square" aria-hidden="true"></i></a>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    
    window.onscroll = function() {scrollFunction()};

function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        document.getElementById("myBtn").style.display = "block";
    } else {
        document.getElementById("myBtn").style.display = "none";
    }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
    document.body.scrollTop = 0; // For Chrome, Safari and Opera 
    document.documentElement.scrollTop = 0; // For IE and Firefox
}
</script>