<!DOCTYPE html>
<html lang="en">
    <head>
        <!--meta-->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>gHealth</title>
        <link rel="shortcut icon" type="image/png" href="{{asset('web/images/fav_16_16.png')}}"/>
        <!-- css -->
        <link href ="{{asset('web/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
        <link href ="{{asset('web/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
        <link href ="{{asset('web/css/index.min.css')}}" rel="stylesheet" type="text/css">
        <link href ="{{asset('web/css/listgrid.css')}}" rel="stylesheet" type="text/css">
        <link rel  ="stylesheet" href="{{asset('web/css/lightbox.min.css')}}">
        <link href ="{{asset('web/css/chat.css')}}" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="{{asset('web/css/jquery-ui.css')}}">
        <link rel="stylesheet" href="{{asset('web/css/rateit.css')}}">
        <link rel="stylesheet" href="{{asset('web/css/bootstrap-slider.css')}}">
        <link href="https://fonts.googleapis.com/css?family=Muli:400,700" rel="stylesheet">
        <style type="text/css">
        	.outer 
        	{
        		transition : background-image 0.5s;
        	}
        	.images-container { 
			  padding: .5vw;
			  font-size: 0;
			  display: -ms-flexbox;
			  -ms-flex-wrap: wrap;
			  -ms-flex-direction: column;
			  -webkit-flex-flow: row wrap; 
			  flex-flow: row wrap; 
			  display: -webkit-box;
			  display: flex;
			}
		/*	.images-container div { 
			  -webkit-box-flex: auto;
			  -ms-flex: auto;
			 flex: auto;
			    /* width: 200px; */
			  /*  margin: 3px;
			    padding: 0px;
			    height: auto;*/
			    /* border-radius: 5px; */
			  /*  overflow: hidden;
			}*/
			.images-container div img { 
			  width: 100%;
			  height: 200px; 
			}
			@media screen and (max-width: 400px) {
			  .images-container div { margin: 0; }
			  .images-container { padding: 0; }
			  
			}

			.img_wrap
			{
				position: relative;
			}
			.img_wrap .img_text
			{
				position: absolute;
				width:100%;
				height:100%;
				opacity: 0;
				left: 0;
				right: 0;
				bottom: 0;
				top: 0;
				background: rgba(0,0,0,0.4);
				transition: all 1s ease;
				margin:0px;
				padding:0px;
			}
			.img_wrap .img_text p
			{
				 position: absolute;
			    left: 50%;
			    width: 100%;
			    height: 100%;
			    top: 50%;
			    transform: translate(-50%, -13%);
			    color: #fff;
			    font-size: 20px;
			    text-align: center;
			    font-weight: 700;
			}
			.img_wrap:hover .img_text
			{
				opacity: 1;
			}

			#myBtn {
    			display: none; /* Hidden by default */
    			position: fixed; /* Fixed/sticky position */
    			bottom: 20px; /* Place the button at the bottom of the page */
    			right: 30px; /* Place the button 30px from the right */
    			z-index: 99; /* Make sure it does not overlap */
    			border: none; /* Remove borders */
    			outline: none; /* Remove outline */
    			background-color: #95cae4; /* Set a background color */
    			color: white; /* Text color */
    			cursor: pointer; /* Add a mouse pointer on hover */
    			padding: 15px; /* Some padding */
    			border-radius: 10px; /* Rounded corners */
			}

			#myBtn:hover {
    			background-color: #555; /* Add a dark-grey background on hover */
			}
            .navbar .navbar-brand img {
    width: 210px;
    transform: translatey(-14px);
}
        </style>
    </head>
    <body>
        <!--outer-begins-->
        @include('home.patient.layouts.home-header')
        @yield('content')
        @include('home.patient.layouts.home-footer')
        <!--js-->
        <script src="{{asset('web/js/jquery.min.js')}}"></script>
        <script src="{{asset('web/js/bootstrap.min.js')}}"></script>
        <script src="{{asset('web/js/lightbox-plus-jquery.min.js')}}"></script>
        <script src="{{asset('web/js/jquery.rateit.js')}}"></script>
        <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script> 
        <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js" async></script>
        <script src="{{asset('web/js/Polling.js')}}"></script>
        <script>
            $('#book-appointment').click(function(){
            	$('.outer').css('background-image', 'url({{asset('web/images/bg/Keyboard.jpg')}})');		
            });
            $('#question-tab').click(function(){
            	$('.outer').css('background-image','url({{asset('web/images/bg/chairs.jpg')}})');
            });
            $('#health-store').click(function(){
            	$('.outer').css('background-image','url({{asset('web/images/bg/health_store.jpg')}})');
            });
            $('#online-consult').click(function(){
            	$('.outer').css('background-image','url({{asset('web/images/bg/doctor.jpg')}})');
            });
        </script>
        <script>
            $(document).ready(function(){
            	$("#datepicker").datepicker();	
            });
        </script>
        @yield('bottom-scripts')
    </body>
</html>