<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>gHealth</title>
        <link rel="shortcut icon" type="image/png" href="{{asset('web/images/fav_16_16.png')}}"/>
        <!-- css -->
        <link href="{{asset('web/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('web/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
        <!-- <link href="css/index.min.css" rel="stylesheet" type="text/css"> -->
        <link href="{{asset('web/css/white.min.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('web/css/listgrid.css')}}" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="{{asset('web/css/lightbox.min.css')}}">
        <link href="{{asset('web/css/chat.css')}}" rel="stylesheet" type="text/css">
        <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" rel="stylesheet" type="text/css">
        <script src="{{asset('web/js/lightbox-plus-jquery.min.js')}}"></script>
        <!-- for rating -->
        <link rel="stylesheet" href="{{asset('web/css/rateit.css')}}">
        <script src="{{asset('web/js/jquery.rateit.js')}}"></script>
        <!-- For Ranger -->
        <link rel="stylesheet" href="{{asset('web/css/bootstrap-slider.css')}}">
        <!--google-fonts-->
        <link href="//fonts.googleapis.com/css?family=Muli:400,700" rel="stylesheet">
        <link rel="stylesheet" href="{{asset('web/css/jquery-ui.css')}}">
        <!--internal css-->
        <style>
            .loader_msg_outer
            {
                position: fixed;
                width: 100%;
                height: 100%;
                top: 0px;
                left: 0px;
                background: rgba(0, 0, 0, 0.6);
                font-family: cursive;
                z-index: 99999999;
                display: flex;
                text-align: center;
                display: none;
            }

            .loader_msg_outer > .loader_msg_inner
            {
                display: inline-block;
                background: white;
                padding: 10px;
                border-radius: 5px;
                position: relative;
                margin: auto;
            }
            .nav>.visible-xs>li>a {
            	padding: 10px 10px !important;
            }

            .schedule-date-hightlight a
            {
                background-color: green !important;
                color: white !important;
            }
            .navbar .navbar-brand img {
                width: 210px;
                transform: translatey(-13px);
            }
            .fa-2x {
                font-size: 21px;
                padding-top: 6px;
            }
        </style>
        @yield('top-scripts')
    </head>
    <body>
    	<div class="loader_msg_outer">
            <div class="loader_msg_inner">
                <img src="{{asset('web/images/loader.gif')}}">
                <span class="loader_msg_text"></span>
            </div>
        </div>
        <!--outer-begins-->
        <div class="outer_index_second">
            <div class="outer">
                <nav class="navbar navbar-default white">
                    <div class="nav_outer review_page_outer">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand black" href="{{url('user/home')}}">
                           <img src="/web/images/ghealthlogo.png">
                            </a>
                        </div>
                        <div class="collapse navbar-collapse clearfix" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav black navbar-right hidden-xs visible-lg hidden-sm">
                                <li>
                                    <a href="{{url('user/home')}}">
                                    <i class="fa fa-home fa-2x" aria-hidden="true"></i>
                                    </a>
                                </li>
                                
                                <li>
                                    <a href="{{url('/user/settings#/doctor-messages')}}">
                                    <i class="fa fa-envelope fa-2x" aria-hidden="true"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{url('/user/settings#/schedules')}}">
                                    <i class="fa fa-calendar fa-2x" aria-hidden="true"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{url('http://ghealthstore.online/#/cart')}}">
                                    <i class="fa fa-cart-arrow-down fa-2x" aria-hidden="true"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{url('user/settings')}}">
                                    <span class="welcome_user">
                                    <span class="fa-2x">{{$auth_user->first_name}}</span>
                                    <i class="fa fa-user pull-right fa-2x"></i>
                                    </span>
                                    </a>
                                </li>
                            </ul>
                            <ul class="nav navbar-nav black navbar-right  visible-xs visible-sm">
                                <li>
                                    <a href="{{url('user/home')}}">
                                    Home
                                    <i class="fa fa-home pull-right fa-19x" aria-hidden="true"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="/user_record">
                                    Medical Record
                                    <i class="fa fa-book  pull-right fa-19x" aria-hidden="true"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="/consult">
                                    Inbox
                                    <i class="fa fa-envelope  pull-right  fa-19x" aria-hidden="true"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="/schedule2">
                                    Appointment
                                    <i class="fa fa-calendar  pull-right fa-19x" aria-hidden="true"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="/empty">
                                    Cart
                                    <i class="fa fa-cart-arrow-down  pull-right  fa-19x" aria-hidden="true"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{url('user/settings')}}">
                                    <span class="welcome_user">
                                    <span class="fa-2x">{{$auth_user->first_name}}</span>
                                    <i class="fa fa-user pull-right fa-19x"></i>
                                    </span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
        @yield('content')
        <div class="footer" style="visibility: hidden;">
            <div class="section-three">
                <div class="container">
                    <div class="row">
                        <div class="footer-bottom1">
                            <!-- <div class="col-sm-12"> -->
                            <div class="col-sm-3">
                                <h5 class="text-uppercase footer-header">
                                    <strong>for patient</strong>
                                </h5>
                                <ul>
                                    <li>
                                        <a href="#">Search for Clinics</a>
                                    </li>
                                    <li>
                                        <a href="#">Search for Doctors</a>
                                    </li>
                                    <li>
                                        <a href="#">Online Appointment</a>
                                    </li>
                                    <li>
                                        <a href="#">Online Stores</a>
                                    </li>
                                    <li>
                                        <a href="#">Lifetime Health Record</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-sm-3 ">
                                <h5 class="text-uppercase footer-header">
                                    <strong>for doctor</strong>
                                </h5>
                                <ul>
                                    <li>
                                        <a href="#">Online Consultation</a>
                                    </li>
                                    <li>
                                        <a href="#">Appointments</a>
                                    </li>
                                    <li>
                                        <a href="#">Health Articles</a>
                                    </li>
                                    <li>
                                        <a href="#">Doctor Profile	</a>
                                    </li>
                                    <li>
                                        <a href="#">Patient Visit Record</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-sm-2">
                                <h5 class="text-lowecase footer-header">
                                    <strong>gHealth</strong>
                                </h5>
                                <ul>
                                    <li>
                                        <a href="#">About Us</a>
                                    </li>
                                    <li>
                                        <a href="#">Find Your Doctor</a>
                                    </li>
                                    <li>
                                        <a href="#">Stages</a>
                                    </li>
                                    <li>
                                        <a href="#">Features</a>
                                    </li>
                                    <li>
                                        <a href="#">Contact Us</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-sm-4">
                                <h5 class="text-uppercase footer-header">
                                    <strong>more</strong>
                                </h5>
                                <ul>
                                    <li>
                                        <a href="#">Help</a>
                                    </li>
                                    <li>
                                        <a href="#">Privacy and Policy</a>
                                    </li>
                                    <li>
                                        <a href="#">Terms and Conditions</a>
                                    </li>
                                    <li></li>
                                    <li></li>
                                  
                                </ul>
                            </div>
                            <!-- </div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-second">
            <div class="container">
                <div class="row">
                    <div class="footer-bottom2">
                        <div class="col-sm-5">
                            <h4>Toll Free Number : 1 300 88 6008</h4>
                            <h5><strong>gHealth.com </strong>2015-2020 @ All Rights Reserved.</h5>
                        </div>
                        <div class="col-sm-3">
                            <h4>
                                <i class="fa fa-globe" aria-hidden="true"></i>
                                www.ghealth.com
                            </h4>
                        </div>
                        <div class="col-sm-4 text-center">							
                            <a href="https://www.facebook.com/Ghealthcom-420340984988107/"><i class="fa fa-facebook-square" aria-hidden="true"></i></a>
                            <a href="https://plus.google.com/u/1/114922129084557930193"><i class="fa fa-google-plus-square" aria-hidden="true"></i></a>
                            <a href="https://twitter.com/gHealthcom"><i class="fa fa-twitter-square" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--js-->
        <script src="{{asset('web/js/jquery.min.js')}}"></script>
        <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script src="{{asset('web/js/bootstrap.min.js')}}"></script>
        <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
		<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular-route.js"></script>
        <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
        <script src="{{asset('web/js/Polling.js')}}"></script>
        <script type="text/javascript">
            $('#book-appointment').click(function(){
            	$('.outer').css('background-image', 'url({{asset('web/images/bg/Keyboard.jpg')}}');		
            });
            $('#question-tab').click(function(){
            	$('.outer').css('background-image','url({{asset('web/images/bg/chairs.jpg')}}');
            });
            $('#health-store').click(function(){
            	$('.outer').css('background-image','url({{asset('web/images/bg/Keyboard.jpg')}}');
            });
            $('#online-consult').click(function(){
            	$('.outer').css('background-image','url({{asset('web/images/bg/doctor.jpg')}}');
            });

        	function showLoaderMessage(show, message = "")
            {
                if(show) {
                    $(".loader_msg_outer").find(".loader_msg_text").html(message);
                    $(".loader_msg_outer").fadeIn();
                    $(".loader_msg_outer").css('display', 'flex');

                } else {
                    setTimeout(function(){ 
                        $(".loader_msg_outer").fadeOut();
                    }, 500);
                }
            }
        </script>
        @yield('bottom-scripts')
    </body>
</html>