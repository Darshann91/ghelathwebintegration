<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8 profile-block" style="top: 24px;">
    <h4 class="heading profile-edit-heading blue-bottom margin-to-zero padding-bottom-23">Video Consult Histories</h4>



    <span ng-repeat="history in videoConsultHistories">
    <div class="panel-group">
        <div class="panel panel-default">
            <div class="panel-heading" style="display:flex;background-color:initial">
                <div class="col-md-2">
                    <img ng-src="[[history.doctor_picture]]" width="75px" height="75px" style="border-radius:2px">
                </div>
                <div class="col-md-4">
                	[[history.doctor_name]]<br>
                	<label>Mobile: [[history.phone]]</label>
                </div>
                <div class="col-md-6">
                    <label>Date: 24 May 2016</label><br>
                    <label>Duration: </label>
                    <text>[[history.duration]] Mins</text>
                    <br>
                    <label>Fee:</label>
                    <text>[[currency]] [[history.fee]]</text>
                </div>
                <!-- <div class="col-md-1" style="text-align:center;font-size:32px;">
                    <i class="fa fa-caret-down" data-toggle="collapse"  style="cursor:pointer"></i>
                </div> -->
            </div>
        </div>
    </div>
    </span>

    <span ng-show="!isFetchingVideoHistories">
    <div title="Load more .." class="load-more" ng-click="getVideoConsultHistories()" ng-show="loadMore">
    	<i class="fa fa-chevron-down"></i>
    </div>
    <div title="No more chat histories to show" class="load-more" ng-hide="loadMore">
    	No more video histories to show
    </div>
    <div ng-if="!videoConsultHistories.length">No video histories</div>
    <span>
    

</div>
<style type="text/css">
	.load-more
	{
		text-align: center;
	    border: 1px solid rgba(0, 0, 0, 0.09);
	    top: -9px;
	    position: relative;
	    cursor: pointer;
	}
</style>