<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8 profile-block" style="top: 24px;">
    <h4 class="heading profile-edit-heading blue-bottom margin-to-zero padding-bottom-23">Doctor Messages</h4>

    <div class="col-lg-12">
        <div class="well message-item" ng-repeat="message in messages">
            <label>#Message</label>
            <div class="sender">
                <span style="text-align:center;">
                    <img ng-src="[[message.doctor_picture]]" title="[[message.doctor_name]]">
                    <br>
                    <span style="font-size:9px">[[message.doctor_name]]</span>
                </span>
                <div class="content">[[message.sent]]</div>
            </div>
            <label style="width: 100%;text-align: right;">#Reply</label>
            <div class="receiver" ng-class="message.reply==''?'reply-box':''">
                <div class="content" ng-show="message.reply">[[message.reply]]</div>
                <textarea class="content" placeholder="Type your reply and send" ng-hide="message.reply" style="margin-right: -60px;margin-left: 2px;border: none;padding-right: 62px;float: left;" ng-model="message.reply_text" ng-init="message.reply_text=''"></textarea>
                <span style="text-align:center;" title="[[auth_user_name]]">
                    <img ng-src="[[auth_user_picture]]"><br>
                    <span style="font-size:9px" class="crop">[[auth_user_name]]</span>
                </span>
                <hr style="margin-top: 56px;margin-bottom:7px;position:relative;width:100%" ng-hide="message.reply">
                <button type="button" ng-hide="message.reply" class="btn btn-info" style="width:100%" ng-click="sendMessageReply(message)">Send Reply</button>
            </div>

        </div>
    </div>

</div>