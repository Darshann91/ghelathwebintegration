<!--LEFT BOOK APPOINTMENT BLOCK -->
<div class="col-sm-8 col-md-8 book_appointment_blocks" id="book_appointment_block">
    <div class="row">
        <div class="col-lg-12">
            <div class="breadcrumb_outer">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{url('/user/home')}}">Home</a></li>
                    <li class="breadcrumb-item"><a href="{{url('/user/home')}}">Book Appointment</a></li>
                    <li class="breadcrumb-item active">Categories</li>
                </ol>
                <h4>Make online appointment with your doctor for consultation.</h4>
            </div>
        </div>
    </div>
    <div class="">
        <div class="col-md-12 col-sm-12 col-xs-12 doc-details cover2 margin20 box effect6" ng-repeat="doctor in bookingDoctorsList" ng-show="has_doctors">
            <div class="col-lg-4 col-md-4 col-xs-12">
                <div class="w3-content text-center" style="">
                    <a class="example-image-link" href="[[doctor.d_photo]]" data-lightbox="example-set3" data-title="Click the right half of the image to move forward."><img class="mySlides3 example-image img-circle" ng-src="[[doctor.d_photo]]" alt="" style="width:100%;"></a>
                    <a class="example-image-link" href="[[doctor.c_pic1]]" data-lightbox="example-set3" data-title="Click the right half of the image to move forward."><img class="mySlides3 example-image img-circle" ng-src="[[doctor.c_pic1]]" alt="" style="width:100%;display:none"></a>
                    <a class="example-image-link" href="[[doctor.c_pic2]]" data-lightbox="example-set3" data-title="Click the right half of the image to move forward."><img class="mySlides3 example-image img-circle" ng-src="[[doctor.c_pic2]]" alt="" style="width:100%;display:none"></a>
                    <a class="example-image-link" href="[[doctor.c_pic3]]" data-lightbox="example-set3" data-title="Click the right half of the image to move forward."><img class="mySlides3 example-image img-circle" ng-src="[[doctor.c_pic3]]" alt="" style="width:100%;display:none"></a>
                    <div class="w3-row-padding w3-section">
                        <div class="w3-col s3">
                            <img class="demo w3-border w3-hover-shadow" alt="" ng-src="[[doctor.d_photo]]" style="width:100%;">
                        </div>
                        <div class="w3-col s3">
                            <img class="demo w3-border w3-hover-shadow" alt="" ng-src="[[doctor.c_pic1]]" style="width:100%;">
                        </div>
                        <div class="w3-col s3">
                            <img class="demo w3-border w3-hover-shadow" alt="" ng-src="[[doctor.c_pic2]]" style="width:100%;">
                        </div>
                        <div class="w3-col s3">
                            <img class="demo w3-border w3-hover-shadow" alt="" ng-src="[[doctor.c_pic3]]" style="width:100%;height:auto">
                        </div>
                    </div>
                    
                </div>
            </div>
            <div class="col-lg-5 col-md-5 col-xs-12 consult-address">
                <h5><strong>[[doctor.d_name]]</strong></h5>
                <h6><strong>[[doctor.c_name]]</strong></h6>
                <i class="fa fa-map-marker"></i>
                <address>[[doctor.c_address]]</address>
                <span>
                <i class="fa fa-star" title="Rating"></i>
                [[doctor.rating]]
                </span>
                
            </div>
            <div class="col-lg-3 col-md-3 col-xs-12 linein">
                <div class="form-group doc-location has-feedback">
                    
                    <img ng-src="[['//maps.googleapis.com/maps/api/staticmap?zoom=17&size=200x200&maptype=roadmap&markers=color:red|' + doctor.d_latitude + ',%20' + doctor.d_longitude + '&key=' + google_map_api_key]]" width="100%" height="100px">
                </div>
                <div class="doc-Charge text-right">
                    <h5><i class="fa fa-credit-card"></i>[[user_currency]] [[doctor.appointment_fee]] onwards</h5>
                </div>
                <div class="form-group paddin0">
                    <button class="schedule btn btn-primary search-btn form-control" style="background-color:#1379CB;border-radius:0px;" ng-click="makeFavouriteDoctor(doctor)" title="Make doctor favourite"><i class="fa fa-heart" ng-class="doctor.is_favorite ? 'red' : 'white'" style="font-size:24px"></i></button>
                </div>
                <div class="form-group paddin0">
                    <button class="schedule btn btn-primary search-btn form-control" style="background-color:#1379CB;border-radius:0px;" ng-click="openBookAppointment(doctor)">Book Appointment</button>
                </div>
            </div>
        </div>
        <div ng-show="!has_doctors">No more doctors found</div>
        <div class="page-nav text-right">
            <div aria-label="Page navigation">
                <ul class="pagination">
                    <li ng-click="previousBookingDoctorsList()" ng-hide="hidePrevious">
                        <a href="#/book_appointment" aria-label="Previous">
                            <span aria-hidden="true">&laquo; Previous</span>
                        </a>
                    </li>
                    <li ng-click="nextBookingDoctorsList()" ng-hide="hideNext">
                        <a href="#/book_appointment" aria-label="Next">
                            <span aria-hidden="true">Next &raquo;</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <hr>
</div>
<!-- END OF BOOK APPOINTMENT BLOCK -->

<!-- SCHEDULE APPOINTMENT BLOCK -->
<div class="col-sm-8 col-md-8 book_appointment_blocks" id="schedule_appointment_block">
    <div class="row">
        <div class="col-lg-12">
            <div class="breadcrumb_outer">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item">
                        <a href="{{url('/user/home')}}">Book Appointment</a></li>
                    
                    <li class="breadcrumb-item active">Schedule Appointment</li>
                </ol>
                <h4>Schedule on appointment with your doctor for consultation.</h4>
            </div>
        </div>
    </div>
    <div class="row top-bottom wrap">
        <div class="col-md-12 col-sm-12 col-xs-12 doc-details margin40">
            <div class="doc-block">
                <div class="col-lg-4 col-md-4 col-xs-12">
                    <div class="w3-content text-center" style="">

                        <a class="example-image-link" href="[[selectedDoctor.d_photo]]" data-lightbox="example-set7"><img class="mySlides7 example-image img-circle img-responsive" ng-src="[[selectedDoctor.d_photo]]" alt="" style="width:100%;"></a>

                        <a class="example-image-link" href="[[selectedDoctor.c_pic1]]" data-lightbox="example-set7"><img class="mySlides7 example-image img-circle img-responsive" ng-src="[[selectedDoctor.c_pic1]]" alt="" style="width:100%;display:none"></a>

                        <a class="example-image-link" href="[[selectedDoctor.c_pic2]]" data-lightbox="example-set7"><img class="mySlides7 example-image img-circle img-responsive" ng-src="[[selectedDoctor.c_pic2]]" alt="" style="width:100%;display:none"></a>

                        <a class="example-image-link" href="[[selectedDoctor.c_pic3]]" data-lightbox="example-set7"><img class="mySlides7 example-image img-circle img-responsive" ng-src="[[selectedDoctor.c_pic3]]" alt="" style="width:100%;display:none"></a>

                        <div class="w3-row-padding w3-section">
                            <div class="w3-col s3">
                                <img class="demo w3-border w3-hover-shadow" alt="" ng-src="[[selectedDoctor.c_pic1]]" style="width:100%;">
                            </div>
                            <div class="w3-col s3">
                                <img class="demo w3-border w3-hover-shadow" alt="" ng-src="[[selectedDoctor.c_pic2]]" style="width:100%;">
                            </div>
                            <div class="w3-col s3">
                                <img class="demo w3-border w3-hover-shadow" alt="" ng-src="[[selectedDoctor.c_pic3]]" style="width:100%;height:auto">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-4 col-xs-12 consult-address">
                    <h5><strong>[[selectedDoctor.d_name]]</strong></h5>
                    <h6><strong>[[selectedDoctor.c_name]]</strong></h6>
                    <i class="fa fa-map-marker"></i>
                    <address>[[selectedDoctor.c_address]]</address>
                    <!-- <label>Distance: 12 Km away</label>
                    <br>
                    <label>ETA: 20 mins</label> -->

                </div>

                <div class="col-xs-12 col-sm-3 col-md-3 linein">
                    <div class="review">
                        <h5><i class="fa fa-credit-card"></i>&nbsp;&nbsp;[[user_currency]] [[selectedDoctor.appointment_fee]] onwards</h5>
                        <h5><i class="fa fa-briefcase blue"></i>&nbsp;&nbsp;Your Experience</h5>
                        <h5><i class="fa fa-heart blue"></i>&nbsp;&nbsp;[[selectedDoctor.helped]] People Connected</h5>
                        <span>
		                <i class="fa fa-star" title="Rating"></i>
		                [[selectedDoctor.rating]]
		                </span>
                    </div>
                </div>
            </div>
            <div class="">
                <div class="top-bottom">
                    <div class="col-lg-12">
                        <h5><strong>Schedule an Appointment</strong></h5>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                        <div id="booking_calendar"></div>
                        <input type="text" id="selected_booking_date" ng-model="selected_booking_date" ng-change="bookingDateSelected()" ng-model-onblur style="display: none">

                    </div>

                    <div class="col-lg-12" style="display: inline-block;">
                        <h5><strong>Select an slot from below list</strong></h5>
                    </div>

                    <!-- SCHEDULE THE DATE -->
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                        <div class="panel panel-default slots-div">
                            <div class="panel-heading">Morning Slots</div>
                            <div class="panel-body">
                                <div style="text-align: center;" ng-hide="mormingSlots.length">No slots available</div>
                                <span class="slot-btn" ng-repeat="slot in mormingSlots" ng-click="selectSlot(slot)">
						  			<i class="fa fa-check-circle slot-btn-tick" ng-show="selectedSlot.available_slot_id==slot.available_slot_id"></i>
						  			<h5 class="slot-btn-text">[[slot.start_time]]am</h5>
						  		</span>
                            </div>
                        </div>
                        <div class="panel panel-default slots-div">
                            <div class="panel-heading">Afternoon Slots</div>
                            <div class="panel-body">
                                <div style="text-align: center;" ng-hide="afternoonSlots.length">No slots available</div>
                                <span class="slot-btn" ng-repeat="slot in afternoonSlots" ng-click="selectSlot(slot)">
						  			<i class="fa fa-check-circle slot-btn-tick" ng-show="selectedSlot.available_slot_id==slot.available_slot_id"></i>
						  			<h5 class="slot-btn-text">[[slot.start_time]]pm</h5>
						  		</span>
                            </div>
                        </div>
                        <div class="panel panel-default slots-div">
                            <div class="panel-heading">Evening Slots</div>
                            <div class="panel-body">
                                <div style="text-align: center;" ng-hide="eveningSlots.length">No slots available</div>
                                <span class="slot-btn" ng-repeat="slot in eveningSlots" ng-click="selectSlot(slot)">
						  			<i class="fa fa-check-circle slot-btn-tick" ng-show="selectedSlot.available_slot_id==slot.available_slot_id"></i>
						  			<h5 class="slot-btn-text">[[slot.start_time]]pm</h5>
						  		</span>
                            </div>
                        </div>

                    </div>

                    <div class="col-lg-12 top-bottom">
                        <div class="form-group col-lg-4 pull-right">
                            <button class="btn btn-primary reg-btn form-control border0" ng-click="bookNow()">Book Now</button>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
</div>
<!-- END OF SCHEDULE DOC DETAILS -->
<div id="book_slots_payment_modal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" style="text-align:center">Choose Payment</h4>
      </div>
      <div class="modal-body">
        
      		<div class="choose_payment_method row" style="text-align:center;" ng-show="paymentStep=='choose_payment_type'">
      			<div class="col-md-5">
      				<i class="fa fa-credit-card payment_method_options" ng-click="fetchUserCards($event)"></i>
      				<br>
      				<span>Pay with Card</span>
      			</div>
      			<dir class="col-md-2">
      				OR
      			</dir>
      			<div class="col-md-5">
      				<i class="fa fa-money payment_method_options" ng-click="selectCod($event)"></i>
      				<br>
      				<span>Pay with Cash</span>
      			</div>
      		</div>
      		<div class="row" ng-show="paymentStep=='list_cards'">
      			<div style="margin-left:5px">Choose Your Card :</div>
      			<ul class="list-group">
				  <li class="list-group-item card-list-item" ng-repeat="card in userCards">
				  	<img ng-src="[[getCardIconURLByType(card.card_type)]]">
				  	<span class="text" ng-if="card.type!='paypal'">XXXX-XXXX-XXXX-[[card.last_four]]</span>
				  	<span class="text" ng-if="card.type=='paypal'">[[card.email]]</span> 
				  	<span><input type="radio" class="pull-right" 
                                name="defaultCard" 
                                ng-click="makdeDefaultCard(card)" 
                                ng-checked="card.is_default"
                            >
                    </span>
				  </li>
				</ul>
      		</div>
      		<div ng-show="paymentStep=='no_cards'"> 
      			
      		</div>

      </div>
      <div class="modal-footer" ng-show="choose_payment_modal_footer" style="text-align:center;">
            
            <!--<div class="input-group place_search">
                <input type="text" class="form-control" placeholder="Type your location and choose" aria-describedby="basic-addon2" id="place_search" googleplace ng-model="place_search">
                <span class="input-group-addon" id="basic-addon2"><i class="fa fa-map-marker"></i></span>
            </div> -->
            <input type="hidden" id="booking_latitude" ng-model="bookingLatitude">
            <input type="hidden" id="booking_longitude" ng-model="bookingLongitude">
           
        <button type="button" class="btn btn-primary" ng-click="bookSlot()">Complete Booking</button>
      </div>
    </div>

  </div>
</div>