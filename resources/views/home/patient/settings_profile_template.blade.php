<div role="tabpanel" class="tab-pane" id="profile">
    <div class="col-xs-12 col-md-8 col-lg-8 profile-block">
        <h4 class="heading profile-edit-heading blue-bottom margin-to-zero">My Profile</h4>
        <div class="row">
            <div class="col-lg-8 paddin0">
                <div class="form-group">
                    <div class="col-sm-12 col-md-6 col-lg-6 col-xs-12">
                        <input type="text" placeholder="Enter Your Frist Name" class="form-control margin-top0" ng-model="patient.first_name" disabled>
                    </div>
                    <div class="col-lg-6 col-sm-12 col-xs-12 col-md-6">
                        <input type="text" placeholder="Enter Your Last Name" class="form-control" ng-model="patient.last_name" disabled>
                    </div>
                    <div class="col-lg-12" class="form-control">
                        <input type="text" placeholder="Email Address" class="form-control" ng-model="patient.email" style="margin-bottom: 10px !important;" disabled>
                    </div>
                    <div class="col-lg-6 col-md-6 form-group">
                        <select class="form-control border0" ng-model="patient.language">
                            <option value="">--Language--</option>
                            <option value="Bahasa Indonesia (in)">Bahasa Indonesia(in)</option>
                            <option value="Burmese (my)">Burmese(my)</option>
                            <option value="Chinese (zh)">Chinese(zh)</option>
                            <option value="English (en)">English(en)</option>
                            <option value="Farsi (fa)">Farsi(fa)</option>
                            <option value="Japanese (ja)">Japanese(ja)</option>
                            <option value="Thai (th)">Thai(th)</option>
                            <option value="Vietnamese (vi)">Vietnamese(vi)</option>
                        </select>
                    </div>
                    <div class="col-lg-6 col-md-6 form-group">
                        <select class="form-control border0" ng-model="patient.currency">
                            <option value="">--Currency--</option>
                            <option value="MYR">MYR</option>
                            <option value="USD">USD</option>
                            <option value="CNY">CNY</option>
                            <option value="THB">THB</option>
                            <option value="SGD">SGD</option>
                            <option value="IDR">IDR</option>
                            <option value="VND">VND</option>
                            <option value="MMK">MMK</option>
                            <option value="IRR">IRR</option>
                            <option value="JPY">JPY</option>                           
                            
                            
                        </select>
                    </div>
                    <div class="col-lg-6 col-md-6 form-group">
                        <select class="form-control border0" ng-model="patient.nationality">
                            <option value="">--Nationality--</option>
                            @foreach($countryCodes as $code)
                            <option value="{{$code['country']}}">{{$code['country']}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="inline-form">
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <input type="radio" ng-model="patient.gender" value="male"> Male
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <input type="radio" ng-model="patient.gender" value="female">&nbsp;Female
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="row">
                    <img class="img-responsive img-circle" ng-src="[[profileDetails.picture]]">
                </div>
            </div>
            <div class="col-lg-12 col-md-12 form-group"></div>
            <div class="col-lg-4 col-md-4 form-group">
                <select class="form-control border0" ng-model="patient.country_code" style="margin-top:4px" disabled>
                    @foreach($countryCodes as $code)
                    <option value="{{$code['country_phone_code']}}">{{$code['country_phone_code']}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-lg-4 col-md-4 form-group">
                <input type="text" class="form-control" placeholder="Enter Phone Number" ng-model="patient.phone" disabled>
            </div>
            <div class="col-lg-4 col-md-4 form-group">
                <input type="button" class="form-control btn-primary" value="Request OTP" id="send_otp_btn" ng-click="sendOTP()">
            </div>
            <div class="col-lg-4 col-md-4 form-group">
                <input type="text" ng-model="patient.otp" class="form-control" placeholder="Enter Otp You received">
            </div>
            <div class="col-lg-4 col-md-4 form-group">
                <input type="file" class="form-control" placeholder="Enter consult fee" id="picture" value="Profile picture">
            </div>
            <div class="col-lg-4 col-md-4 form-group">
                <div class="input-group">
                    <input type="text" class="form-control border0" id="dob" placeholder="Choose Date of Birth" ng-model="patient.dob" style="margin:0 !important;">
                    <div class="input-group-addon">
                        <span class="glyphicon glyphicon-th"></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row  top-bottom form-group" style="text-align: center">
            <button type="button" class="btn btn-primary form-control" ng-click="updateUser()" style="width: 100px">UPDATE</button>
        </div>
    </div>
</div>