<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8 profile-block" style="top: 24px;">
    <h4 class="heading profile-edit-heading blue-bottom margin-to-zero padding-bottom-23">Chat Consult Histories</h4>



    <span ng-repeat="history in chatConsultHistories">
    <div class="panel-group">
        <div class="panel panel-default">
            <div class="panel-heading" style="display:flex;background-color:initial">
                <div class="col-md-2">
                    <img ng-src="[[history.doctor_picture]]" width="75px" height="75px" style="border-radius:2px">
                </div>
                <div class="col-md-4">
                	[[history.doctor_name]]<br>
                	<label>Mobile: [[history.phone]]</label>
                </div>
                <div class="col-md-5">
                    <label>Date: 24 May 2016</label><br>
                    <label>Duration: </label>
                    <text>[[history.duration]] Mins</text>
                    <br>
                    <label>Fee:</label>
                    <text>[[currency]] [[history.fee]]</text>
                </div>
                <div class="col-md-1" style="text-align:center;font-size:32px;">
                    <i class="fa fa-caret-down" data-toggle="collapse" data-target="#collapse_[[history.id]]" ng-click="getChatMessages(history.id)" style="cursor:pointer"></i>
                </div>
            </div>
            <div id="collapse_[[history.id]]" class="panel-collapse collapse">
                <div class="panel-body msg_container_base" style="padding-left:10px;padding-right:10px;height:350px">
                    <span ng-repeat="message in chatMessages[history.id]" style="display:initial;" >
                    

                    	<div class="row msg_container base_sent margin-left-right-0" ng-if="message.type == 'receive' && message.data_type=='TEXT'">
                                <div class="col-md-8 col-xs-8 ">
                                    <div class="messages msg_sent">
                                        <p>[[message.message]]</p>
                                        <time>[[message.created_at | chat_timestamp_filter]]</time>
                                    </div>
                                </div>
                                <div class="col-md-1.5 col-xs-1.5 avatar-left">
                                    <img ng-src="[[user_picture]]" class="img-circle chat-user-img" alt="doc">
                                </div>
                            </div>
                            <div class="row msg_container base_sent margin-left-right-0" ng-if="message.type == 'receive' && message.data_type=='IMAGE'">
                                <div class="col-md-8 col-xs-8 ">
                                    <div class="messages msg_sent">
                                        <img ng-src="[[message.message]]" style="width:100%">
                                        <time>[[message.created_at | chat_timestamp_filter]]</time>
                                    </div>
                                </div>
                                <div class="col-md-1.5 col-xs-1.5 avatar-left">
                                    <img ng-src="[[user_picture]]" class="img-circle chat-user-img" alt="doc">
                                </div>
                            </div>
                            <div class="row msg_container base_receive margin-left-right-0" ng-if="message.type == 'sent' && message.data_type=='TEXT'">
                                <div class="col-md-1.5 col-xs-1.5 avatar-right">
                                    <img ng-src="[[history.doctor_picture]]" class="img-circle chat-user-img" alt="doc">
                                </div>
                                <div class="col-md-8 col-xs-8">
                                    <div class="messages msg_receive">
                                        <p>[[message.message]]</p>
                                        <time>[[message.created_at | chat_timestamp_filter]]</time>
                                    </div>
                                </div>
                            </div>
                            <div class="row msg_container base_receive margin-left-right-0" ng-if="message.type == 'sent' && message.data_type=='IMAGE'">
                                <div class="col-md-1.5 col-xs-1.5 avatar-right">
                                    <img ng-src="[[history.doctor_picture]]" class="img-circle chat-user-img" alt="doc">
                                </div>
                                <div class="col-md-8 col-xs-8">
                                    <div class="messages msg_receive">
                                        <img ng-src="[[message.message]]" style="width:100%">
                                        <time>[[message.created_at | chat_timestamp_filter]]</time>
                                    </div>
                                </div>
                            </div>



                    </span>
                </div>
            </div>
        </div>
    </div>
    </span>

    <span ng-show="!isFetchingChatHistories">
    <div title="Load more .." class="load-more" ng-click="getChatConsultHistories()" ng-show="loadMore">
    	<i class="fa fa-chevron-down"></i>
    </div>
    <div title="No more chat histories to show" class="load-more" ng-hide="loadMore">
    	No more chat histories to show
    </div>
    <div ng-if="!chatConsultHistories.length">No chat histories</div>
    <span>
    

</div>
<style type="text/css">
	.load-more
	{
		text-align: center;
	    border: 1px solid rgba(0, 0, 0, 0.09);
	    top: -9px;
	    position: relative;
	    cursor: pointer;
	}
</style>