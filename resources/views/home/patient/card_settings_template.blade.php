<div role="tabpanel" class="tab-pane billinginfo features" id="billing">
    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8 billing-info-block"\>
        <h3 class="heading blue-bottom">Saved Cards</h3>
        <div class="row top-botttom">
            <div class="col-lg-12">
                <!-- <h4><strong>Saved Cards</strong></h4> -->
                <div class="list-group">
                    <a  class="list-group-item card-list-item" ng-if="!savedCards.length">No Saved Cards. Add card please</a>
                    <a  class="list-group-item card-list-item" ng-repeat="card in savedCards">
                        <img ng-src="[[getCardTypeImage(card.card_type)]]">
                        <span style="font-weight:700" ng-if="card.type!='paypal'">XXXX-XXXX-XXXX-[[card.last_four]]</span>
                        <span style="font-weight:700" ng-if="card.type=='paypal'">[[card.email]]</span>
                        <div class="card-radio" title="Click to make this card detault">
                            <input type="radio" style="width:24px;height:24px;cursor:pointer;" ng-checked="[[card.is_default]]" ng-click="makeDefaultCard($index)">
                        </div>
                    </a>
                </div>
                <i class="fa fa-plus add-card-button" title="Add Card" style="cursor:pointer" ng-click="addCardModal()"></i>
            </div>
        </div>
    </div>
</div>
<div id="add-card-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="background: #286090;color: white;font-weight: 700;">
                <button type="button" class="close" data-dismiss="modal" style="color:white">&times;</button>
                <h4 class="modal-title">Add New Card</h4>
            </div>
            <div class="modal-body" style="padding: 5px;">
                <form style="border: 1px solid rgba(0, 0, 0, 0.22);padding: 5px;">
                    <div id="dropin-container"></div>
                    <div style="text-align:center">
                        <button type="submit" class="btn btn-primary">ADD CARD</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>