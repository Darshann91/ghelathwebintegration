@extends('home.patient.layouts.layout-web')
@section('content')

<!-- HEALTH ARTICLE BLOCK -->
							<div class="row article-gridlist">
								<div class="col-sm-12">
									<div class="row top-space" style="margin-right:1px;">
							          	<div class="btn-group" style="float:right;">

							          		<span class="dropdown">
											  	<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">All Health Topics<span class="caret"></span></button>
											  	<ul class="dropdown-menu">
												    <li><a href="#" id="topic1">Health Topics</a></li>
												    <li><a href="#" id="topic2">All Health Topics</a></li>
											  	</ul>
											</span>
											<span class="text-right">
									            <a href="#" id="list5" class="btn btn-default btn-sm">
									              <span class="glyphicon glyphicon-th-list">    
									              </span></a> 
									            <a href="#" id="grid5" class="btn btn-default btn-sm">
									              <span class="glyphicon glyphicon-th"></span>
									            </a>
									        </span>
							          	</div>
							        </div>

							        <div class="col-xs-12" id="health_article1" style="overflow-y:scroll;height:700px;">
							            <div id="products" class="row list-group scroll">
							                
							                @foreach($articles as $article)
							                <div class="item  col-xs-3 col-lg-3">
						                  		<a href="#" class="article_link">
						                     		
						                     		<div class="for_grid">
						                     			<a href="{{route('showArticle',$article->id)}}">
						                      			<img src="{{$article->picture}}" width=100% height=400 alt="article1"/>
						                      			</a>
						                      			<h4 class="text-center top-space" ><strong>{{$article->heading}}</strong></h4>
						                      			<a class="blue" href="{{route('showArticle',$article->id)}}">
						                      			<p style="color:blue" class="text-center top-space" >View Full Article</p>
						                      			</a>

						            
						                      		</div>
							                    </a>
							                </div>	

							                @endforeach        
								          								          	
							                
								        </div>
							        </div>

							       
							      

								</div>
							</div>
							<!-- END OF HEALTH ARTICLE-1 BLOCK -->


@endsection