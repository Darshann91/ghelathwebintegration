@extends('home.patient.layouts.layout-web2') @section('content')
<style>
    .active-menu {
     border: none;
     box-shadow: none;
}
</style>
<div class="outer_user_details" ng-app="userSettings">
    <div class="section-two">
        <div class="container">
            <div class="row top-bottom">
                <!-- LEFT DIV BLOCK -->
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
                    <div class="accordion top-bottom" id="accordion1">
                        <ul class="list-group">
                            <a href="#profile" class="features" data-toggle="tab" role="tab">
                                <li class="list-group-item"><i class="fa fa-user" aria-hidden="true"></i>&nbsp;&nbsp; My Profile</li>
                            </a>
                            <a href="#schedules" class="myschedule features" data-toggle="tab" role="tab">
                                <li class="list-group-item"><i class="fa fa-calendar" aria-hidden="true"></i>&nbsp;&nbsp; My Schedule</li>
                            </a>
                            <a href="#cards" class="features" data-toggle="tab" role="tab">
                                <li class="list-group-item">
                                    <i class="fa fa-credit-card" aria-hidden="true"></i>&nbsp;&nbsp; Cards
                                </li>
                            </a>
                            <a href="#change-password" class="features" data-toggle="tab" role="tab">
                                <li class="list-group-item"><i class="fa fa-user-secret"></i>&nbsp;&nbsp; Change Password</li>
                            </a>
                        </ul>
                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <ul class="list-group">
                                    <li class="list-group-item" data-toggle="collapse">
                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" data-target="#collapseTwo"><i class="fa fa-history"></i></i>&nbsp;&nbsp; Consult History</a>
                                    </li>
                                </ul>
                            </div>
                            <div id="collapseTwo" class="accordion-body collapse">
                                <div class="accordion-inner">
                                    <ul class="list-group" style="margin-left:0;">
                                        <a href="#chat-consult-history" class="features" data-toggle="tab" role="tab">
                                            <li class="list-group-item">
                                                <i class="fa fa-comments"></i>&nbsp;&nbsp; Chat Consult
                                            </li>
                                        </a>
                                        <a href="#phone-consult-history" class="features" data-toggle="tab" role="tab">
                                            <li class="list-group-item">
                                                <i class="fa fa-phone"></i>&nbsp;&nbsp;Phone Consult
                                            </li>
                                        </a>
                                        <a href="#video-consult-history" class="features" data-toggle="tab" role="tab">
                                            <li class="list-group-item">
                                                <i class="fa fa-video-camera" aria-hidden="true"></i>&nbsp;&nbsp;Video Consult
                                            </li>
                                        </a>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <ul class="list-group">
                            <a href="#ask-question" class="features" data-toggle="tab" role="tab">
                                <li class="list-group-item">
                                    <i class="fa fa-question"></i>&nbsp;&nbsp;Ask Question
                                </li>
                            </a>

                            <a href="#doctor-messages" class="features" data-toggle="tab" role="tab">
                                <li class="list-group-item">
                                    <i class="fa fa-comment"></i>&nbsp;&nbsp;Doctor Messages
                                </li>
                            </a>

                            <a href="{{url('user/logout')}}" class="features">
                                <li class="list-group-item" style="border-bottom: 1px solid #ddd;">
                                    <i class="fa fa-sign-out" aria-hidden="true"></i>&nbsp;&nbsp;Sign Out
                                </li>
                            </a>
                        </ul>
                    </div>
                </div>
                <!-- END LEFT DIV BLOCK -->
                <!-- RIGHT DIV BLOCK -->
                <div class="tab-content">
                    <ng-view></ng-view>
                </div>
            </div>
        </div>
    </div>
    @section('top-scripts')

    <style type="text/css">
        .add-card-button {
            font-size: 32px;
            background: #95CAE4;
            border: #95CAE4;
            padding: 10px;
            color: white;
            border-radius: 50%;
            width: 50px;
            height: 50px;
            padding-left: 12px;
            box-shadow: 1px 1px 8px 2px rgba(0, 0, 0, 0.26);
            position: absolute;
            right: 11px;
            bottom: -58px;
        }
        
        .card-selected {
            background-color: #f5f5f5 !important;
        }
        
        .card-list-item {
            border-radius: 10px !important;
            border: 1px solid #ddd !important;
        }
        
        .card-radio {
            display: inline-block;
            float: right;
        }
        
        .padding-bottom-23 {
            padding-bottom: 23px
        }
        
        .change-passwor-container {
            display: table-cell;
            vertical-align: middle;
            float: initial;
            padding: 30px !important;
            border: 1px solid rgba(0, 0, 0, 0.12);
            box-shadow: 1px 1px 8px 2px rgba(0, 0, 0, 0.1);
            text-align: center;
            border-radius: 2px;
        }
        
        .password-change-header {
            background: rgba(0, 0, 0, 0.08);
            /*color: white;*/
            padding: 5px;
            border-radius: 2px;
        }
        
        .chat-user-img {
            width: 50px !important;
            height: 50px !important;
        }
        
        .margin-left-right-0 {
            margin-left: 0px !important;
            margin-right: 0px !important;
        }
        
        .active-menu {
            background: rgba(0, 0, 0, 0.58);
            color: white;
            border: 1px solid black;
            box-shadow: inset 0px 0px 2px 0px white;
        }
        
        .speciality-img {
            width: 50px;
            height: 50px;
            border: 2px solid rgba(0, 0, 0, 0.35);
            border-radius: 2px;
            padding: 1px;
        }
        
        .spciality-list-item {
            padding-left: 5px;
            padding-right: 5px;
            padding-top: 5px;
            cursor: pointer;
            transform: all 0.3s;
        }
        
        .spciality-list-item:hover {
            background-color: rgba(0, 0, 255, 0.06);
        }
        
        .speciality-text {
            color: rgba(0, 0, 0, 0.66);
            margin-left: 3px;
            font-size: 15px;
        }
        
        .doctor-search-icon {
            display: inline-block;
            position: absolute;
            top: 5px;
            right: 7px;
            cursor: pointer;
            background: rgba(0, 0, 0, 0.47);
            padding: 5px;
            color: white !important;
        }
        
        .message-item {
            box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
        }
        
        .message-item > .sender,
        .message-item > .receiver {
            display: inline-flex;
            padding: 2px;
            border: 1px solid rgba(0, 0, 0, 0.14);
            width: 100%;
        }
        
        .message-item > .sender img,
        .message-item > .receiver img {
            width: 50px;
            height: 50px;
        }
        
        .message-item > .sender > .content {
            margin-left: 10px;
        }
        
        .message-item > .receiver > .content:focus {
            border: none;
            outline: 0px;
        }
        
        .message-item > .receiver > .content {
            width: 100%;
        }
        
        .curtain {
            position: fixed;
            top: 0px;
            left: 0px;
            width: 100%;
            height: 100%;
            background: rgba(0, 0, 0, 0.47);
        }
        
        .reply-box {
            padding: 10px !important;
            background: white;
            display: inline-block !important;
        }
        
        .reply-box > img {
            float: right;
        }
        .crop 
        {
            overflow:hidden; 
            white-space:nowrap; 
            text-overflow:ellipsis; 
            width:100px; 
        }​
    </style>
    <link rel="stylesheet" type="text/css" href="{{asset('web/css/slide_three_radio_button.css')}}"> @endsection @endsection @section('bottom-scripts')
    <!-- angular route code -->
    <script type="text/javascript">
        var user_name = "{{$auth_user->first_name.' '.$auth_user->last_name}}";
        var user_currency = "{{$auth_user->currency}}";
        var user_picture = "{{$auth_user->picture}}";
        var profile_template_url = "{{url('user/settings/template/profile')}}";
        var profile_details_url = "{{url('user/settings/profile/details')}}";
        var profile_details_update_url = "{{url('user/settings/profile/update')}}";
        var csrf_token = "{{csrf_token()}}";
        var otp_send_url = "{{url('otp/send')}}";
        var schedules_template_url = "{{url('user/settings/template/schedules')}}";
        var get_schedules_url = "{{url('user/settings/my-schedules')}}";
        var get_schedule_dates = "{{url('user/settings/schedule/dates')}}";
        var get_cards_url = "{{url('user/get-cards')}}";
        var user_cards_template_url = "{{url('user/settings/template/cards')}}";
        var card_image_32_base_url = "{{asset('web/cards_icons/32')}}";
        var get_braintree_client_token = "{{url('user/braintree/client-token')}}";
        var save_card_url = "{{url('user/save-card')}}";
        var make_default_card = "{{url('user/make-default-card')}}";
        var change_password_template_url = "{{url('user/settings/template/change-password')}}";
        var change_password = "{{url('user/settings/password/change')}}";
        var chat_consult_history_template = "{{url('user/settings/template/chat-consult-history')}}";
        var chat_consult_histories = "{{url('user/settings/history/consult/chat')}}";
        var chat_consult_messages = "{{url('user/settings/history/consult/chat/messages')}}";
        var phone_consult_history_template = "{{url('user/settings/template/phone-consult-history')}}";
        var phone_consult_histories = "{{url('user/settings/history/consult/phone')}}";
        var video_consult_history_template = "{{url('user/settings/template/video-consult-history')}}";
        var video_consult_histories = "{{url('user/settings/history/consult/video')}}";
        var ask_question_template = "{{url('user/settings/template/ask-question')}}";
        var ask_question = "{{url('user/settings/ask-question')}}";
        var get_specialities = "{{url('get-specialities')}}";
        var search_doctors_url = "{{url('user/settings/search/doctors')}}";
        var doctor_messages_template = "{{url('user/settings/template/doctor-messages')}}";
        var doctor_messages = "{{url('user/messages-list')}}";
        var send_message_reply = "{{url('user/doctor/send-reply')}}";
    </script>
    <script src="https://js.braintreegateway.com/js/braintree-2.31.0.min.js"></script>
    <script type="text/javascript" src="{{url('web/js/patient_settings.js')}}"></script>
    @endsection