@extends('home.patient.layouts.layout-web')
@section('content')

<div class="col-sm-12 col-md-12" id="health_article_block">

				

			<div class="row">
								<div class="col-lg-12">
									<div class="breadcrumb_outer">
										<ol class="breadcrumb">
										  <li class="breadcrumb-item"><a href="/">Home</a></li>
										  
										  <li class="breadcrumb-item active">Health Article</li>
										</ol>
										<h4>Get health advice from doctor for your health queries.</h4>
									</div>
								</div>
								
				</div>

				<div class="col-xl-12 col-xs-12 col-md-12 margin_top20">
								<span><h4 class="blue">Health Article</h4></span>
								<img src="{{$article->picture}}" class="img-responsive" style="width:40%;height:300px;" />
				</div>


				<div class="row">
								<div class="col-xl-12 col-xs-12 col-md-12 margin_top20" style="overflow-y:scroll;height:420px;">
									<h4><strong>{{$article->heading}}</strong></h4>
									
									<hr>
									<p>{{urldecode($article->content)}}<p>


								</div>
				</div>



				<div class="row gray_bg top-bottom">
								<div class="">
									<div class="col-xs-12 col-sm-12 col-md-3 col-xl-3">
										<img src="{{$doctor->picture}}" class="img-circle" alt="" width=100 height=100>
									</div>
									<div class="col-xs-12 col-sm-12 col-md-5 col-xl-5">
										<h4><strong>Dr. {{$doctor->first_name." ".$doctor->last_name}}</strong></h4>
										<h5 class="blue"><strong>{{$clinic->clinic_name}}</strong></h5>
										<i class="fa fa-map-marker"></i>&nbsp;&nbsp;{{$clinic->c_address}}
									</div>
									
								</div>
							</div>


							

							<h4 class="text-center top-space" ><strong>
									<a class="blue" href="{{route('showAllArticles')}}">
						                      			<p style="color:blue" class="text-center top-space" >Show All Articles</p>
						    </a>
							</strong></h4>



<div>

@endsection