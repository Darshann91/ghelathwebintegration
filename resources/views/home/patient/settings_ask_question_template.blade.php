<div class="col-xs-12 col-md-8 col-lg-8 profile-block" style="top:24px;">
    <h4 class="heading profile-edit-heading blue-bottom margin-to-zero padding-bottom-23">Ask Question</h4>
    <div class="row" style="margin-left:0px;margin-right:0px;display:table;width: 100%;margin-bottom:15px">
        <div class="col-lg-10 col-md-8 col-sm-10 col-xs-12 change-passwor-container">
            <p class="password-change-header">Feel free to ask any questions to any doctor. Choose you spciality and find doctor and ask question to doctor</p>
            <!-- <form class="form-horizontal"> -->
                <div class="form-group">
                    <label class="control-label col-sm-4">Choose Speciality</label>
                    <div class="col-sm-8">
                        <div class="dropdown">
                            <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" style="width:100%">
                            <img ng-src="[[selectedSpeciality.picture]]" style="width:20px;height:20px">
                            [[selectedSpeciality.speciality]]</button>
                            <ul class="dropdown-menu" style="max-height: 232px;overflow-y: scroll;width:100%">
                                <li ng-repeat="speciality in specialities" class="spciality-list-item" ng-click="selectSpeciality($index)">
                                    <img ng-src="[[speciality.picture]]" class="speciality-img">
                                    <span class="speciality-text">[[speciality.speciality]]</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4">Search Doctor</label>
                    <div class="col-sm-8">
                        <div class="dropdown" id="search_doctor">
                            <input type="text" class="form-control dropdown-toggle" placeholder="Type doctor first name or last name or email" data-toggle="dropdown" style="width:100%;padding-right:35px" autocomplete="off" name="doctor_name" ng-model="search_doctor" ng-focus="search_doctor='';selectedDoctor=null" ng-keyup="$event.keyCode == 13 && searchDoctors()">
                            <i class="fa fa-search doctor-search-icon" ng-click="searchDoctors()"></i>
                            <ul class="dropdown-menu" style="max-height: 232px;overflow-y: scroll;width:100%">
                                <li ng-repeat="doctor in searchedDoctors" class="spciality-list-item" ng-click="selectDoctor($index)">
                                    <img ng-src="[[doctor.picture]]" class="speciality-img">
                                    <span class="speciality-text">[[doctor.first_name]] [[doctor.last_name]]</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4">Subject</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" placeholder="Enter Subject" ng-model="subject">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4">Question</label>
                    <div class="col-sm-8">
                        <textarea class="form-control" placeholder="Please describe your question in details to get more accurate answer from specialist" ng-model="content" style="height:100px !important"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-primary" ng-click="submitQuestion()">SUBMIT</button>
                    </div>
                </div>
           <!--  </form> -->
        </div>
    </div>
</div>