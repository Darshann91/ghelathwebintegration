<div role="tabpanel" class="tab-pane active features" id="schedules">
	<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8 my-schedule-block">
		<div class="row top-bottom">
			<h4 class="heading blue-bottom profile-edit-heading margin-to-zero">My Schedule</h4>
			<div class="col-lg-4 pull-right">
				<div class="search-block">
                    <input type="text" placeholder="Select Date" class="form-control" id="schedule-choose-date" ng-model="selectedDate" ng-change="getSchedulesByDate()" ng-init="selectedDate='{{date('Y-m-d')}}'">
                </div>
                <div class="search-icon">
                    <i class="fa fa-calendar"></i>
                </div>
			</div>
			<div class="col-lg-12 blue-left margin20" ng-repeat="schedule in schedules" emit-last-repeater-element>
				<div class="row gray-top-bottom">
					
					<div class="col-lg-8 col-sm-8 margin20">
						<label>[[schedule.doctor_name]]</label>
						<address>
							[[schedule.c_name]]<br>
							[[schedule.c_street]]<br>
							[[schedule.c_city]], [[schedule.c_country]]<br>
						</address>
						<label>Time: [[schedule.start_time]] [[schedule.ampm]] - [[schedule.end_time]] [[schedule.ampm]]</label>
					</div>
					<div class="col-lg-4 col-sm-4 medium-clock">
						<!-- <i class="fa fa-clock-o"></i> -->
						<canvas class="canvas_clock" data-hour-min="[[schedule.start_time]]" style="top: 14px;position: relative;">
						</canvas>
						
					</div>
				</div>
			</div>
			<div class="row" ng-show="showNoSchedule()">
                <div class="col-md-12 form-group">
                    <label>[[no_schedules_found_text]]</label>
                </div>
            </div>
		</div>
	</div>
</div>