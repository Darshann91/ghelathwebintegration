<div class="col-sm-8 col-md-8 online-doctor-bolcks" id="online-consult">
    <div class="row">
        <div class="col-lg-12">
            <div class="breadcrumb_outer">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="#">Home</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="{{url('/user/home#categories')}}">Online Consult</a>
                    </li>
                    <li class="breadcrumb-item active">Categories</li>
                </ol>
                <h4>Search and make the Online Consultation with your Doctor.</h4>
            </div>
        </div>
    </div>
    <div class="" ng-repeat="doctor in onlineDoctorsList" ng-show="has_doctors">
        <div class="col-md-12 col-sm-12 col-xs-12 doc-details cover2 box effect6">
            <!-- <div class="col-xs-12 col-sm-12 col-md-4"> -->
            <div class="col-lg-4 col-md-4 col-xs-12">
                <div class="w3-content" style="position:relative;max-width:100%;height:auto;">
                    <a class="example-image-link" href="[[doctor.d_photo]]" data-lightbox="example-set"><img class="mySlides  example-image" ng-src="[[doctor.d_photo]]" style="width:100%;height:auto;"></a>
                    <a class="example-image-link" href="[[doctor.c_pic1]]" data-lightbox="example-set" ><img class="mySlides example-image" ng-src="[[doctor.c_pic1]]" style="width:100%;height:auto;display:none"></a>
                    <a class="example-image-link" href="[[doctor.c_pic2]]" data-lightbox="example-set" ><img class="mySlides example-image" ng-src="[[doctor.c_pic2]]" style="width:100%;height:auto;display:none"></a>
                    <a class="example-image-link" href="[[doctor.c_pic3]]" data-lightbox="example-set"><img class="mySlides example-image" ng-src="[[doctor.c_pic3]]" style="width:100%;height:auto;display:none"></a>
                    <div class="w3-row-padding w3-section">
                        <div class="w3-col s3">
                            <img class="demo w3-border w3-hover-shadow" ng-src="[[doctor.c_pic1]]" style="width:100%;">
                        </div>
                        <div class="w3-col s3">
                            <img class="demo w3-border w3-hover-shadow" ng-src="[[doctor.c_pic2]]" style="width:100%;" >
                        </div>
                        <div class="w3-col s3">
                            <img class="demo w3-border w3-hover-shadow" ng-src="[[doctor.c_pic3]]" style="width:100%;">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-5 consult-address">
                <h5><strong>[[doctor.d_name]]</strong> </h5>
                <h6><strong>[[doctor.c_name]]</strong></h6>
                <i class="fa fa-map-marker"></i>
                <address>[[doctor.c_address]]</address>
                <span>
                <i class="fa fa-star" title="Rating"></i>
                [[doctor.rating]]
                </span>
                
            </div>
            <style>
            </style>
            <div class="col-xs-12 col-sm-12 col-md-3 linein">
                <div class="doc-review gray-bottom">
                    <h6 ng-show="doctor.experience!==''"><i class="fa fa-stethoscope" style="vertical-align: middle;"></i>[[doctor.experience]] years experience</h6>
                    <h6><i class="fa fa-users" style="vertical-align: middle;"></i>Consulted [[doctor.helped]] people</h6>
                    <h6><i class="fa fa-money" style="vertical-align: middle;"></i>[[user_currency]] [[getMinAmount(doctor.c_fee, doctor.p_fee, doctor.v_fee)]] onwards</h6>
                </div>
                <div class="active-contact">
                    <h6><i class="fa fa-comment"></i>Chat Consult</h6>
                    <h6><i class="fa fa-phone"></i>Phone Consult</h6>
                    <h6><i class="fa fa-video-camera"></i>Video Consult</h6>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-xl-12 paddin0" style="    margin-bottom: 5px;">
                    <button type="button" ng-click="makeFavouriteDoctor(doctor)" title="Make doctor favourite" class="btn btn-primary search-btn form-control consult"  style="background-color:#1379CB;border-radius:0px;"><i class="fa fa-heart" ng-class="doctor.is_favorite ? 'red' : 'white'" style="font-size:24px"></i></button>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-xl-12 paddin0">
                    <button type="button" id="consult" ng-click="selectDoctor(doctor)" class="btn btn-primary search-btn form-control consult"  style="background-color:#1379CB;border-radius:0px;">Consult now</button>
                </div>
            </div>
        </div>
    </div>
    <div ng-show="!has_doctors">No more doctors found</div>
    <div class="row">
        <div aria-label="Page navigation"  class="text-right">
            <ul class="pagination pagination-sm">
                <li class="page-item" ng-click="previousOnlineDoctorsList()" style="cursor:pointer" ng-hide="hidePrevious">
                    <a class="page-link" aria-label="Previous">
                    <span aria-hidden="true">&laquo; Previous</span>
                    <span class="sr-only">Previous</span>
                    </a>
                </li>
                
                <li class="page-item" ng-click="nextOnlineDoctorsList()" style="cursor:pointer" ng-hide="hideNext">
                    <a class="page-link" aria-label="Next">
                    <span aria-hidden="true">Next &raquo;</span>
                    <span class="sr-only">Next</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <hr>
    <!-- HEALTH ARTICLE BLOCK -->
    <!-- <div class="row article-gridlist top-bottom">
        <div class="">
            <div class="row top-bottom" style="margin-right:1px;">
                <div class="btn-group" style="float:right;">
                    <a href="#" id="list" class="btn btn-default btn-sm">
                    <span class="glyphicon glyphicon-th-list">    
                    </span></a> 
                    <a href="#" id="grid" class="btn btn-default btn-sm">
                    <span class="glyphicon glyphicon-th"></span>
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12" style="overflow-y:scroll;overflow-x:hidden;height:300px;">
                    <div id="products" class="row list-group scroll">
                        <div class="item  col-xs-6 col-lg-6">
                            <a href="#" class="article_link">
                                <div class="for_list">
                                    <div class="col-lg-8 col-md-8 col-sm-8">
                                        <h4 class="blue">Eight Worm up excercises</h4>
                                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text.
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4">
                                        <img src="{{asset("web/images/jogging.jpg")}}" width=100% alt="article1"/>
                                        <p class="text-center">View Full Article</p>
                                    </div>
                                </div>
                                <div class="for_grid">
                                    <img src="{{asset("web/images/jogging.jpg")}}" width=100% height=200 alt="article1"/>
                                    <p class="text-center top-space">View Full Article</p>
                                </div>
                            </a>
                        </div>
                        <div class="item  col-xs-6 col-lg-6">
                            <a href="#" class="article_link">
                                <div class="for_list">
                                    <div class="col-lg-8 col-md-8 col-sm-8">
                                        <h4 class="blue">Eight Worm up excercises</h4>
                                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text.
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4">
                                        <img src="{{asset("web/images/checkup.jpg")}}" width=100% alt="article1"/>
                                        <p class="text-center">View Full Article</p>
                                    </div>
                                </div>
                                <div class="for_grid">
                                    <img src="{{asset("web/images/checkup.jpg")}}" width=100% height=200 alt="article1"/>
                                    <p class="text-center top-space">View Full Article</p>
                                </div>
                            </a>
                        </div>
                        <div class="item  col-xs-6 col-lg-6">
                            <a href="#" class="article_link">
                                <div class="for_list">
                                    <div class="col-lg-8 col-md-8 col-sm-8">
                                        <h4 class="blue">Eight Worm up excercises</h4>
                                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text.
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4">
                                        <img src="{{asset("web/images/psycho.jpg")}}" width=100% alt="article1"/>
                                        <p class="text-center">View Full Article</p>
                                    </div>
                                </div>
                                <div class="for_grid">
                                    <img src="{{asset("web/images/psycho.jpg")}}" width=100% height=200 alt="article1"/>
                                    <p class="text-center top-space">View Full Article</p>
                                </div>
                            </a>
                        </div>
                        <div class="item  col-xs-6 col-lg-6">
                            <a href="#" class="article_link">
                                <div class="for_list">
                                    <div class="col-lg-8 col-md-8 col-sm-8">
                                        <h4 class="blue">Eight Worm up excercises</h4>
                                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text.
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4">
                                        <img src="{{asset("web/images/parenting.jpg")}}" width=100% alt="article1"/>
                                        <p class="text-center">View Full Article</p>
                                    </div>
                                </div>
                                <div class="for_grid">
                                    <img src="{{asset("web/images/parenting.jpg")}}" width=100% height=200 alt="article1"/>
                                    <p class="text-center top-space">View Full Article</p>
                                </div>
                            </a>
                        </div>
                        <div class="item  col-xs-6 col-lg-6">
                            <a href="#" class="article_link">
                                <div class="for_list">
                                    <div class="col-lg-8 col-md-8 col-sm-8">
                                        <h4 class="blue">Eight Worm up excercises</h4>
                                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text.
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4">
                                        <img src="{{asset("web/images/apple.jpg")}}" width=100% alt="article1"/>
                                        <p class="text-center">View Fll Article</p>
                                    </div>
                                </div>
                                <div class="for_grid">
                                    <img src="{{asset("web/images/apple.jpg")}}" width=100% height=200 alt="article1"/>
                                    <p class="text-center top-space">View Full Article</p>
                                </div>
                            </a>
                        </div>
                        <div class="item  col-xs-6 col-lg-6">
                            <a href="#" class="article_link">
                                <div class="for_list">
                                    <div class="col-lg-8 col-md-8 col-sm-8">
                                        <h4 class="blue">Eight Worm up excercises</h4>
                                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text.
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4">
                                        <img src="{{asset("web/images/nutrition.jpg")}}" width=100% alt="article1"/>
                                        <p class="text-center">View Full Article</p>
                                    </div>
                                </div>
                                <div class="for_grid">
                                    <img src="{{asset("web/images/nutrition.jpg")}}" width=100% height=200 alt="article1"/>
                                    <p class="text-center top-space">View Full Article</p>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 text-center top-bottom">
                <a href="#">View Full Articlez</a>
            </div>
        </div>
        </div> -->
</div>
<!-- END OF HEALTH ARTICLE BLOCK -->
<!-- END OF ONLINE CONSULTATION BLOCK -->
<!-- CHAT CONSULT BLOCK-->
<div class="col-lg-8 col-md-8 online-doctor-bolcks" id="chat_consult_block">
    <div class="row">
        <div class="col-lg-12">
            <div class="breadcrumb_outer">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item"><a href="/search#online_btn" onClick="window.location.reload()">Online Consult</a></li>
                    <li class="breadcrumb-item"><a href="/search#online_btn" onClick="window.location.reload()">Categories Dental</a></li>
                    <li class="breadcrumb-item"><a href="#" class="consult">Consult Now</a></li>
                    <li class="breadcrumb-item active">Chat Consult</li>
                </ol>
                <h4>Search and make your online consultation with your doctor.</h4>
            </div>
        </div>
    </div>
    <div class="row wrap top-bottom">
        <div class="">
            <div class="chat-window col-xs-12 col-md-12" id="chat_window_1" style="margin-top:3%;">
                <div class="">
                    <div class="panel panel-default chat-box">
                        <div class="panel-heading top-bar" style="">
                            <span><i class="fa fa-clock-o" style="
                                border: none;
                                right: 0px;
                                position: absolute;
                                top: -2px;
                            "><span>[[hour]]:[[min]]:[[sec]]</span></i></span>
                            <div class="col-md-6 col-xs-6 avatar-left">
                                <h3 class="panel-title">
                                    <img ng-src="[[selectedDoctor.d_photo]]" class="img-circle" alt="doc"><span>&nbsp;&nbsp;[[selectedDoctor.d_name]]</span>
                                </h3>
                            </div>
                        </div>
                        <div class="panel-body msg_container_base" style="padding-left:10px;padding-right:10px;height:350px" id="online_message_container">
                            <span ng-repeat="message in messages" style="display:initial;">
                            <div class="row msg_container base_sent" ng-if="message.type == 'receive' && message.data_type=='TEXT'">
                                <div class="col-md-8 col-xs-8 ">
                                    <div class="messages msg_sent">
                                        <p>[[message.message]]</p>
                                        <time>[[message.timestamp | chat_timestamp_filter]]</time>
                                    </div>
                                </div>
                                <div class="col-md-1.5 col-xs-1.5 avatar-left">
                                    <img ng-src="[[user_photo]]" class="img-circle" alt="doc">
                                </div>
                            </div>
                            <div class="row msg_container base_sent" ng-if="message.type == 'receive' && message.data_type=='IMAGE'">
                                <div class="col-md-8 col-xs-8 ">
                                    <div class="messages msg_sent">
                                        <img ng-src="[[message.message]]" style="width:100%">
                                        <time>[[message.timestamp | chat_timestamp_filter]]</time>
                                    </div>
                                </div>
                                <div class="col-md-1.5 col-xs-1.5 avatar-left">
                                    <img ng-src="[[user_photo]]" class="img-circle" alt="doc">
                                </div>
                            </div>
                            <div class="row msg_container base_receive" ng-if="message.type == 'sent' && message.data_type=='TEXT'">
                                <div class="col-md-1.5 col-xs-1.5 avatar-right">
                                    <img ng-src="[[selectedDoctor.d_photo]]" class="img-circle" alt="doc">
                                </div>
                                <div class="col-md-8 col-xs-8">
                                    <div class="messages msg_receive">
                                        <p>[[message.message]]</p>
                                        <time>[[message.timestamp | chat_timestamp_filter]]</time>
                                    </div>
                                </div>
                            </div>
                            <div class="row msg_container base_receive" ng-if="message.type == 'sent' && message.data_type=='IMAGE'">
                                <div class="col-md-1.5 col-xs-1.5 avatar-right">
                                    <img ng-src="[[selectedDoctor.d_photo]]" class="img-circle" alt="doc">
                                </div>
                                <div class="col-md-8 col-xs-8">
                                    <div class="messages msg_receive">
                                        <img ng-src="[[message.message]]" style="width:100%">
                                        <time>[[message.timestamp | chat_timestamp_filter]]</time>
                                    </div>
                                </div>
                            </div>
                            </span>
                            <span class="chat-typing" style="display:none"><i ng-class="chat_notif_popup_fa_icon">[[chat_notif_popup_text]]</i></span>
                        </div>
                        <div class="container-fluid">
                            <div class="row chat" style="margin-right:0px;margin-left:0px;padding:5px">
                                <div class="col-xs-1 col-sm-1 col-md-1 chat-icon" style="padding: 0px;text-align: center;" ng-click="openUploadFileWindow()">
                                    <i class="fa fa-paperclip" style="font-size: 24px;top: 8px;position: relative;font-weight: 500;cursor:pointer;"></i>
                                    <input type="file" id="chat_upload_file" style="display: none" onchange="angular.element(this).scope().uploadAttachment(this.files)">
                                </div>
                                <div class="col-xs-10 col-sm-10 col-md-10 col-xs-10">
                                    <input type="text" class="form-control" placeholder="Write your message ....." style="border-radius:0px;" ng-model="messageText" ng-keyup="$event.keyCode == 13 && sendMessageText()" ng-keydown="emitTyping()">
                                </div>
                                <div class="col-xs-1 col-sm-1 col-md-1 chat-icon" style="padding: 0px;text-align: center;">
                                    <button style="font-size: 24px;top: -4px;position: relative;font-weight: 500;border: none;background: none;" ng-click="sendMessageText()"><i class="fa fa-paper-plane"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- <div class="top-bottom">
            <div class="col-sm-6 col-md-5 col-xs-12 contact-by">
                <h4>Dr.Marina Bt.Suliman</h4>
                <h5>Klinik Pergigian Rakan-rakan</h5>
                <span><i class="fa fa-comment active"></i></span>
                <span id="phone_consult_c"><i class="fa fa-phone"></i></span>
                <span id="video_consult_c"><i class="fa fa-video-camera"></i></span>
            </div>
            <div class="col-sm-6 col-md-5 col-xs-12 clock">
                <div class="col-sm-12">
                    <span><i class="fa fa-clock-o"></i></span>
                    <span class="timer-text">00:08:38</span>
                </div>
                <div class="col-sm-12">
                    <span><i class="fa fa-clock-o"></i></span>
                    <span class="timer-text">MYR 5.75</span>
                </div>
            </div>
            <div class="col-sm-12 col-md-2 col-xs-12 text-center contact-through">
                <span id="end_consult_c"><i class="fa fa-phone"></i></span>
            </div>
            </div> -->
    </div>
</div>
<!-- END OF CHAT CONSULT -->
<!-- VIDEO CONSULT BLOCK -->
<div class="col-sm-8 col-md-8" id="video_consult_block">
    <div class="row">
        <div class="col-lg-12">
            <div class="breadcrumb_outer">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item">
                        <a href="/search#online_btn" onClick="window.location.reload()">Online Consult</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="/search#online_btn" onClick="window.location.reload()">Categories Dental</a>
                    </li>
                    <li class="breadcrumb-item"><a href="#" class="consult">Consult Now</a></li>
                    <li class="breadcrumb-item active">Video Consult</li>
                </ol>
                <h4>Search and make your online consultation with your doctor.</h4>
            </div>
        </div>
    </div>
    <div class="row wrap">
        <img src="/web/images/d1.jpg" width="100%" height=370 />
        <img class="over img-responsive" src="/web/images/d1.jpg" width="150" height=100 />
        <div class="row">
            <div class="col-sm-6 col-md-5 col-xs-12 contact-by">
                <h4><strong>Dr.Marina Bt.Suliman</strong></h4>
                <h5>Klinik Pergigian Rakan-rakan</h5>
                <span id="chat_consult_v"><i class="fa fa-comment"></i></span>
                <span id="phone_consult_v"><i class="fa fa-phone"></i></span>
                <span><i class="fa fa-video-camera active"></i></span>
            </div>
            <div class="col-sm-6 col-md-5 col-xs-12 clock">
                <div class="col-sm-12">
                    <span><i class="fa fa-clock-o"></i></span>
                    <span class="timer-text">00:08:38</span>
                </div>
                <div class="col-sm-12">
                    <span><i class="fa fa-clock-o"></i></span>
                    <span class="timer-text">MYR 5.75</span>
                </div>
            </div>
            <div class="col-sm-12 col-md-2 col-xs-12 text-center contact-through">
                <span id="end_consult_v"><i class="fa fa-phone"></i></span>
            </div>
        </div>
    </div>
</div>
<!-- END OF VIDEO CONSULT BLOCK -->
<!-- DOCTOR PROFILE BLOCK FILE 10 -->
<div class="col-sm-8 col-md-8 online-doctor-bolcks" style="display: none" id="online_doctor_profile_block">
    <div class="row">
        <div class="col-lg-12">
            <div class="breadcrumb_outer">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item">
                        <a href="/search#online_btn" onClick="window.location.reload()">Online Consult</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="/search#online_btn" onClick="window.location.reload()">Categories Dental</a>
                    </li>
                    <li class="breadcrumb-item active">Doctor Profile</li>
                </ol>
                <h4>Search and make your online consultation with your doctor.</h4>
            </div>
        </div>
    </div>
    <div class="row top-bottom wrap">
        <div class="col-sm-8">
            <div class="w3-content" style="position:relative;max-width:100%;height:370px;">
                <a class="example-image-link" href="[[selectedDoctor.d_photo]]" data-lightbox="example-set6" data-title="Click the right half of the image to move forward."><img class="mySlides6 example-image" ng-src="[[selectedDoctor.d_photo]]" style="width:100%"></a>
                <a class="example-image-link" href="[[selectedDoctor.c_pic1]]" data-lightbox="example-set6" data-title="Click the right half of the image to move forward."><img class="mySlides6 example-image" ng-src="[[selectedDoctor.c_pic1]]" style="width:100%;display:none"></a>
                <a class="example-image-link" href="[[selectedDoctor.c_pic2]]" data-lightbox="example-set6" data-title="Click the right half of the image to move forward."><img class="mySlides6 example-image" ng-src="[[selectedDoctor.c_pic2]]" style="width:100%;display:none"></a>
                <a class="example-image-link" href="[[selectedDoctor.c_pic3]]" data-lightbox="example-set6" data-title="Click the right half of the image to move forward."><img class="mySlides6 example-image" ng-src="[[selectedDoctor.c_pic3]]" style="width:100%;display:none"></a>
                <div class="w3-row-padding w3-section">
                    <div class="w3-col s3">
                        <img class="demo w3-border w3-hover-shadow" ng-src="[[selectedDoctor.c_pic1]]" style="width:100%;height:70px;">
                    </div>
                    <div class="w3-col s3">
                        <img class="demo w3-border w3-hover-shadow" ng-src="[[selectedDoctor.c_pic2]]" style="width:100%;height:70px;">
                    </div>
                    <div class="w3-col s3">
                        <img class="demo w3-border w3-hover-shadow" ng-src="[[selectedDoctor.c_pic3]]" style="width:100%;height:70px;">
                    </div>
                </div>
            </div>

        </div>
        <div class="col-sm-4">
            <br>
            <form class="form-group main-third text-center">
                <img src="[[selectedDoctor.d_photo]]" class="img-circle" alt="doc" width=125 height=125 />
                <h4>[[selectedDoctor.d_name]]</h4>
                <h6>[[selectedDoctor.c_name]]</h6>
            </form>
            <hr>
            <form class="about-doc">
                <span class="glyphicon glyphicon-briefcase" style=""> </span><label>Work Experience</label><br>
                <span class="glyphicon glyphicon-heart" style=""></span><label> Consulted [[selectedDoctor.helped]] people</label>
                <span class="fa fa-star" style="color:yellow;"></span><label> Rating [[selectedDoctor.rating]]</label> 
            </form>
        </div>
    </div>
    <div class="row location-left-block top-bottom">
        <div class="col-sm-3">
            <img ng-src="[[selectedDoctorMapLink]]" width="100%" height="125px">
        </div>
        <div class="col-sm-5 location">
            <i class="fa fa-map-marker"></i>Place
            <form class="form-group address" style="">
                <p>[[selectedDoctor.c_address]]</p>
            </form>
        </div>
        <div class="col-sm-4 doc-rating text-right">
        </div>
    </div>
    <hr>
    <div class="col-sm-12 contact top-bottom">
        <div class="row">
            <div class="col-sm-4">
                <div class="radio">
                    <label>
                    <input type="radio" ng-model="consult_type" style="font-size:1em;" value="chat" checked>
                    <i class="fa fa-comment"></i>
                    <span>Text Consult</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-4" style="margin-top:1%;">
                <span><i class="fa fa-credit-card"></i>&nbsp;[[user_currency]] [[selectedDoctor.c_fee]] per minute onwards.</span>        
            </div>
            <div class="col-sm-4">
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <div class="radio">
                    <label>
                    <input type="radio" ng-model="consult_type" value="phone">
                    <i class="fa fa-phone"></i>
                    <span>Phone Consult</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-4" style="margin-top:1%;">
                <span><i class="fa fa-credit-card"></i>&nbsp;[[user_currency]] [[selectedDoctor.p_fee]] per minute onwards.</span>
            </div>
            <div class="col-sm-4">
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <div class="radio">
                    <label>
                    <input type="radio" ng-model="consult_type" value="video">
                    <i class="fa fa-video-camera"></i>
                    <span>Video Consult</span>
                    </label>
                </div>
            </div>
            <div class="col-sm-4" style="margin-top:1%;">
                <span><i class="fa fa-credit-card"></i>&nbsp;[[user_currency]] [[selectedDoctor.v_fee]] per minute onwards.</span>
            </div>
            <div class="col-sm-4">
                <div class="from-group">
                    <button ng-click="consultNow()" class="btn btn-default search-btn form-control"> CONSULT NOW</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div id="request_doctor_modal" class="modal fade" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" style="text-align:center;">
        <!-- Modal content-->
        <div class="modal-content" style="display:inline-block;text-align:initial;border-radius:0px;padding: 20px;">
            <div class="modal-body" style="text-align:center">
                <img src="{{asset('web/images/loader.gif')}}">
                <h4>Requesting Doctor...</h4>
            </div>
            <div class="modal-footer" style="padding:0px">
                <button type="button" class="btn btn-info" style="width: 100%;height: 100%;margin: 0px;border-radius: 0px;" ng-click="cancelRequest()">CANCEL</button>
            </div>
        </div>
    </div>
</div>
<div id="video-audio-modal" class="modal fade" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Online Consultation</h4>
            </div>
            <div class="modal-body" style="padding:1px">
                <div id="opentok-publisher" style="position: absolute;width: 100px;height: 100px;z-index: 99;border: 1px solid white;top: 20px;left: 21px;"></div>
                <div id="opentok-subscriber" style="width: 100%;height: 400px;"></div>
            </div>
            <div class="modal-footer" style="text-align: center;position: absolute;width: 100%;height: 78px;bottom: 0px;background: rgba(0, 0, 0, 0.32);z-index:999999999"">
                <button type="button" class="btn btn-defuult" title="Full/Small Screen" ng-click="toggleFullScreen()"><i class="fa fa-arrows-alt" style="font-size: 28px;color:white"></i></button>
                <time style="position: absolute;right: 5px;top: 28px;color: white;font-size: 16px;font-weight: 700;">Time [[hour]]:[[min]]:[[sec]]</time>
            </div>
        </div>
    </div>
</div>
<div id="invoice" class="modal fade" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content" style="border-radius:3px;font-weight: 700;">
            <div class="modal-header">
                <h4 class="modal-title">INVOICE</h4>
            </div>
            <div class="modal-body">
                <table class="table table-responsive">
                    <tbody>
                        <tr style="text-align:center">
                            <td colspan="2" style="border-top:0px">
                                <img ng-src="[[invoice.doctor_picture]]" style="width:70px;height: 70px;border-radius:50%;margin-right: 5px;">
                                <br>
                                <span>[[invoice.doctor_name]]</span>
                            </td>
                        </tr>
                        <tr>
                            <td>PAYMENT MODE</td>
                            <td>[[invoice.payment_mode]]</td>
                        </tr>
                        <tr>
                            <td>CONSULTATION FEE</td>
                            <td>[[invoice.base_price]] [[user_currency]]</td>
                        </tr>
                        <tr style="background-color:rgba(0, 0, 0, 0.32);;color:white">
                            <td>TOTAL</td>
                            <td>[[invoice.total]] [[user_currency]]</td>
                        </tr>
                        <tr>
                            <td colspan="2" style="padding: 8px 0px 0px 0px;">
                                <button type="button" class="btn btn-info" style="width: 100%;height: 100%;margin: 0px;border-radius: 0px;" ng-click="closeInvoice()"><i class="fa fa-check" style="font-size: 24px;margin-right: 4px;"></i></button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div id="rating-modal" class="modal fade" role="dialog" style="border-radius:3px;font-weight: 700;" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">FEEDBACK</h4>
            </div>
            <div class="modal-body" style="text-align: center;">
                <img ng-src="[[invoice.doctor_picture]]" style="width:70px;height: 70px;border-radius:50%;margin-right: 5px;">
                <br>
                <span>[[invoice.doctor_name]]</span>
                <div id="doctor-rating">
                    <div rating model="rating"></div>
                </div>
                <div class="form-group" style="text-align: initial;">
                    <label for="comment">Comment:</label>
                    <textarea class="form-control" rows="5" style="border: 1px solid rgba(0, 0, 0, 0.12);" placeholder="Type your comment here ..." ng-model="rating.comment"></textarea>
                </div>
                <button type="button" ng-click="giveRating()" class="btn btn-info" style="width: 100%;height: 100%;margin: 0px;border-radius: 0px;"><i class="fa fa-check" style="font-size: 24px;margin-right: 4px;"></i></button>
            </div>
        </div>
    </div>
</div>
<!-- END OF FILE 10