<div class="col-xs-12 col-md-8 col-lg-8 profile-block" style="top:24px;">
    <h4 class="heading profile-edit-heading blue-bottom margin-to-zero padding-bottom-23">Change Your Password</h4>
    <div class="row" style="margin-left:0px;margin-right:0px;display:table;">
        <div class="col-lg-10 col-md-8 col-sm-10 col-xs-12 change-passwor-container">
            <p class="password-change-header">Enter your new account password below. Once confirmed, you'll be logged into your accoutn and your new password will be active.</p>
            <form class="form-horizontal">
                <div class="form-group">
                    <label class="control-label col-sm-4">Current Password</label>
                    <div class="col-sm-8">
                        <input type="password" class="form-control" placeholder="Enter Current Password" ng-model="password.old_password">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4">New Password</label>
                    <div class="col-sm-8">
                        <input type="password" class="form-control" placeholder="Enter New Password" ng-model="password.new_password">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-4">Confirm Password</label>
                    <div class="col-sm-8">
                        <input type="password" class="form-control" placeholder="Enter Confirm Password" ng-model="password.confirm_password">
                    </div>
                </div>
                
                
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-primary" ng-click="changePassword()">CHANGE</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>