@extends('layouts.layout_user_register')
			

@section('content')
	<!--login-content-->
	<div class="login-content">
		<ul class="nav nav-tabs">
			<li class="txt-edit">
				<a href="45.html">Register</a>
			</li>
		</ul>
		<p class="pull-right txt-edit">
			<a href="register" class="reg-doc">
				Register as A Doctor
			</a>
		</p>
		<form class="register-form">
			<div class="form-group">
				<label>Your Email Address </label>
				<input type="text" class="form-control">
			</div>
			<div class="form-group">
				<label>Your User Name </label>
				<input type="text" class="form-control">
			</div>
			<div class="form-group">
				<label>Choose Your Password </label>
				<input type="password" class="form-control" placeholder="6+ characters">
			</div>
			<div class="form-group">
				<label>Confirm Your Password </label>
				<input type="password" class="form-control">
			</div>
			<div class="sign-in-btn">
				<a href="#" class="btn btn-primary btn-justified reg-btn">Sign Up </a>
			</div>
		</form>
	</div>


@endsection