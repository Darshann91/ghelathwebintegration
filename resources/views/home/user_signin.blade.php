@extends('layouts.layout_user_login')



@section('content')		
			
	<!--login-content-->
	<div class="login-content">
		<ul class="nav nav-tabs">
			<li class="col-lg-6 active clearfix" style="padding-left: 0px;">
				<a class="pull-left" data-toggle="tab" href="#user-signin" style="padding-left: 0px;color:#000;">
					<b class="txt">User Sign In </b>
				</a>
			</li>
			<li class="col-lg-6 clearfix" style="padding-right: 0px;">
				<a class="pull-right" data-toggle="tab" href="#doct-signin" style="padding-right: 0px;color:#000;">
					<b class="txt">Doctor Sign In </b>
				</a>
			</li>
		</ul>
		<div class="tab-content">
			<div id="user-signin" class="tab-pane fade in active">
			 <form action="{{ url('/user/login') }}" method="POST">
				<div class="form-group">
					<label>User Name/ Email ID</label>
					<input type="text" name="email" class="form-control"/>
				</div>
				<div class="form-group pwd-grp">
					<label>Password</label>
					<input type="text" name="password" class="form-control"/>
				</div>
				<a href="#" target="_blank">Forgot Password?</a>
				<div class="sign-in-btn">
					<button type="submit" class="btn btn-primary btn-justified sign-in">Sign In </button>
				</div>

			 </form>
				
				<div class="social-btn">
					<p>Log in by Social Log in:</p>
					<div class="container-fluid">
						<div class="row">
							<div class="col-xl-12 col-md-12 col-sm-12 col-xs-12">
								<div class="col-xs-3">
								</div>
								<div class="col-xs-2" style="padding-left:0px;">
									<i class="fa fa-facebook fb-icon"></i>
								</div>
								<div class="col-xs-2" style="padding-left:12px;padding-top: 9px;">
									OR
								</div>
								<div class="col-xs-2">
									<i class="fa fa-google-plus gp-icon"></i>
								</div>
							</div>
						</div>
					</div>
					<p class="text-center mem">I am not a member Sign UP</p>
				</div>
			</div>
			<div id="doct-signin" class="tab-pane fade">
				<div class="form-group">
					<label>User ID</label>
					<input type="text" class="form-control"/>
				</div>
				<div class="form-group pwd-grp">
					<label>Password</label>
					<input type="password" class="form-control"/>
				</div>
				<a href="#" target="_blank">Forgot Password?</a>
				<div class="sign-in-btn">
					<a href="1.2.html" class="btn btn-primary btn-justified sign-in">Sign In </a>
				</div>
				<div class="social-btn">
					<p>Log in by Social Log in:</p>
					<div class="container-fluid">
						<div class="row">
							<div class="col-xl-12 col-md-12 col-sm-12 col-xs-12">
								<div class="col-xs-3">
								</div>
								<div class="col-xs-2" style="padding-left:0px;">
									<i class="fa fa-facebook fb-icon"></i>
								</div>
								<div class="col-xs-2" style="padding-left:9px;padding-top: 9px;">
									OR
								</div>
								<div class="col-xs-2">
									<i class="fa fa-google-plus gp-icon"></i>
								</div>
							</div>
						</div>
					</div>
					<a href="{{ url('/user/register') }}"><p class="text-center mem">I am not a member Sign Up</a></p>
				</div>
			</div>
		</div>
	</div>

	<!--js-->
	
	<script src="web/js/bootstrapValidator.js"></script>
	<script type="web/text/javascript">
    $(document).ready(function() {
        $('#defaultForm').bootstrapValidator();
    });
	</script>

@endsection
		