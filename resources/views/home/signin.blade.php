@extends('layouts.layout_signin')
@section('content')		
	<style type="text/css">
		.red-border {
			border-color: red;
		}
		.tw-icon
		{
			padding: 7px;
		}
		.social-seperator
		{
			margin-right: 10px;
    		margin-left: 10px;
		}
		.fa {
			cursor: pointer;
            transition: all ease 0.5s;
            -webkit-transition: all ease 0.5s;
		}
        .fa:hover
        {
                background: #95cae4;
                color: #fff;
                box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.4);
       }
         .modal-btn{
            background-color: #95cae4;
            border-color: #95cae4;
            color:#fff;
        }
        .modal-btn:hover, .modal-btn:focus,.modal-btn:active, .modal-btn:visited{
             background-color: #95cae4;
            border-color: #95cae4;
            color:#fff;
        }
	</style>
	<!--login-content-->
	<div class="login-content">
		<div class="alert alert-success error_div" style="display:none">
		  	<strong class="error_div_strong"></strong> <span class="error_div_text"></span>
		</div>
		<ul class="nav nav-tabs">
			<li class="col-lg-6 active clearfix" style="padding-left: 0px;">
				<a class="pull-left" data-toggle="tab" href="#user-signin" style="padding-left: 0px;color:#000;">
					<b class="txt">User Sign In </b>
				</a>
			</li>
			<li class="col-lg-6 clearfix" style="padding-right: 0px;">
				<a class="pull-right" data-toggle="tab" href="#doct-signin" style="padding-right: 0px;color:#000;">
					<b class="txt">Doctor Sign In </b>
				</a>
			</li>
		</ul>
		<div class="tab-content">
			<div id="user-signin" class="tab-pane fade in active">
			 <form action="{{ url('/user/login') }}" method="POST" id="user-login-form" novalidate>
			 	{!! csrf_field() !!}
				<div class="form-group">
					<label>User Name/ Email ID</label>
					<input type="text" name="email" class="form-control"/>
				</div>
				<div class="form-group pwd-grp">
					<label>Password</label>
					<input type="password" name="password" class="form-control"/>
				</div>
				<a href="#pwd" data-toggle="modal" target="_blank">Forgot Password?</a>
				<div class="sign-in-btn">
					<button type="submit" class="btn btn-primary btn-justified sign-in">Sign In </button>
				</div>

			 </form>
				
				<div class="social-btn">
					<p>Log in by Social Log in:</p>
					<div class="container-fluid">
						<div class="row">
							<div class="col-xl-12 col-md-12 col-sm-12 col-xs-12" style="text-align: center">
								<i class="fa fa-facebook fb-icon social-btn" data-url="{{url('facebook')}}" data-type="user"></i>
								<span class="social-seperator">OR</span>
								<i class="fa fa-google-plus gp-icon social-btn" data-url="{{url('google')}}" data-type="user"></i>
								<span class="social-seperator">OR</span>
								<i class="fa fa-twitter tw-icon social-btn" data-url="{{url('twitter')}}" data-type="user"></i>
							</div>
						</div>
					</div>
					<a href="{{ url('/user/register') }}"><p class="text-center mem">I am not a member Sign UP</p></a>
				</div>
			</div>
			<div id="doct-signin" class="tab-pane fade">
				<form action="{{ url('/doctor/login') }}" method="POST" id="doctor-login-form" novalidate>
				{!! csrf_field() !!}
				<div class="form-group">
					<label>User ID</label>
					<input type="text" name="email" class="form-control"/>
				</div>
				<div class="form-group pwd-grp">
					<label>Password</label>
					<input type="password" name ="password" class="form-control"/>
				</div>
				<a href="#pwd" data-toggle="modal" target="_blank">Forgot Password?</a>
				<div class="sign-in-btn">
					<button type="submit" class="btn btn-primary btn-justified sign-in">Sign In </button>
				</div>
				<div class="social-btn">
					<p>Log in by Social Log in:</p>
					<div class="container-fluid">
						<div class="row">
							<div class="col-xl-12 col-md-12 col-sm-12 col-xs-12" style="text-align: center">
								<i class="fa fa-facebook fb-icon social-btn" data-url="{{url('facebook')}}" data-type="doctor"></i>
								<span class="social-seperator">OR</span>
								<i class="fa fa-google-plus gp-icon social-btn" data-url="{{url('google')}}" data-type="doctor"></i>
								<span class="social-seperator">OR</span>
								<i class="fa fa-twitter tw-icon social-btn" data-url="{{url('twitter')}}" data-type="doctor"></i>
							</div>
						</div>
					</div>
					<p class="text-center mem"><a href="{{ url('/doctor/register') }}">I am not a member Sign Up</a></p>
				</div>
			</div>
		</div>
        <!-- Modal -->
        

        <div class="modal fade" id="pwd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <form role="form">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Forgot Password?</h4>
              </div>
              <div class="modal-body">
               <div class="form-group">
                   <label>Enter email</label>
                    <input id = "fgtemail" type="text" class="form-control" placeholder="Enter email" required />
               </div>
              </div>
              <div class="modal-footer">
                <!--<input type="submit" value="Submit" class="btn modal-btn" id="fbtn"  /> -->
                <button id="fgt" type="button" onclick="submitContactForm()" class="btn modal-btn">Submit</button> 
              </div>
            </div>
            </form>
          </div>
        </div>
       
        
	</div>

	<!--js-->
	
	<script src="{{asset('web/js/bootstrapValidator.js')}}"></script>
	<script type="text/javascript">

	function hideMessage()
    {
    	$(".error_div").hide();
    }


    function showError(text)
    {
    	var elem = $(".error_div");
    	elem.removeClass('alert-success').addClass('alert-danger');
    	elem.find(' > .error_div_strong').text('Error!');
    	elem.find(' > .error_div_text').text(text);
    	elem.show();
    }

    function showSuccess(text)
    {
    	var elem = $(".error_div");
    	elem.removeClass('alert-danger').addClass('alert-success');
    	elem.find(' > .error_div_strong').text('Success!');
    	elem.find(' > .error_div_text').text(text);
    	elem.show();
    }


    function emailFieldError(div, text)
    {
    	var elem = $('#'+div).find("input[name='email']");
    	elem.addClass('red-border');
    	elem.attr('placeholder', text);
    	elem.val('');
    }


    function passwordFieldError(div, text)
    {
    	var elem = $('#'+div).find("input[name='password']");
    	elem.addClass('red-border');
    	elem.attr('placeholder', text);	
    	elem.val('');
    }


    function initAll()
    {
    	$('#user-login-form').find("input[name='email']").removeClass('red-border').attr('placeholder', '');
    	$('#user-login-form').find("input[name='password']").removeClass('red-border').attr('placeholder', '');
    	$('#doctor-login-form').find("input[name='email']").removeClass('red-border').attr('placeholder', '');
    	$('#doctor-login-form').find("input[name='password']").removeClass('red-border').attr('placeholder', '');
    	hideMessage();
    }



    function redirectToRegister(URL, dataObject)
    {
    	URL += "?";
    	if(dataObject.social_provider != "") {
    		URL += 'login_by='+dataObject.social_provider;
    	}
    	if(dataObject.user_dob != "") {
    		URL += '&dob='+dataObject.user_dob;
    	}

    	if(dataObject.user_email != "") {
    		URL += '&email='+dataObject.user_email;
    	}
    	if(dataObject.user_first_name != "") {
    		URL += '&first_name='+dataObject.user_first_name;
    	}
    	if(dataObject.user_last_name != "") {
    		URL += '&last_name='+dataObject.user_last_name;
    	}
    	if(dataObject.user_social_id != "") {
    		URL += '&social_unique_id='+dataObject.user_social_id;
    	}
    	if(dataObject.user_image_base64 != "") {
    		URL += '&picture='+dataObject.user_image_base64;
    	}
    	
    	window.location.href = URL;
    }

    function submitContactForm(){
        var reg = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
        var email = $('#fgtemail').val();
        var URL = "{{url('/forgotpassword')}}";

        if(email.trim() == ''){
            alert('Please enter your email.');
            $('#fgtemail').focus();
            return false;
        }else if(email.trim() != '' && !reg.test(email)){
            alert('Please enter valid email.');
            $('#fgtemail').focus();
        return false;
        }else{

            $.ajax({

                type: 'POST',
                url : URL,
                data: 'email='+email,

                success:function(msg){
                    if(msg == 'ok'){
                        alert('Please check your inbox.');
                    }else{
                        alert('Something went wrong');
                    }
                }

            });
        }
    }




    $(document).ready(function() {

        $('#defaultForm').bootstrapValidator();



        $(".social-btn").on('click', function(){

        	var url = $(this).data('url');
        	if(url == undefined) return ;

        	var user_type = $(this).data('type');

        	showLoaderMessage(true, "Waiting ..");

        	var child = window.open(url+"?user_type="+user_type,'_blank','height='+(screen.height/2)+', width='+(screen.width/2));

            $(child.document.body).html("<div style=\"text-align: center;margin-top: 25%;\"><img src=\"{{asset('web/images/loader.gif')}}\" style=\"vertical-align: middle;\"</div>");

        	timer = setInterval(function(){
 				if (child.closed) {
	        		try {
	        			json_response = child.document.getElementById("json_response").value;
	        			var object = JSON.parse(json_response);

	        			if(object.login_status && user_type == 'user') {
	        				window.location.href = "{{url('user/home')}}";
	        			} else if(object.login_status && user_type == 'doctor') {
	        				window.location.href = "{{url('doctor/home')}}";
	        			} else {
	        				var url = user_type == 'user' ? "{{url('user/register')}}" : "{{url('doctor/register')}}"
	        				redirectToRegister(url, object)
	        			}
 				
	        		} catch (err) {	  
	        			console.log(err);      
	        			toastr.error('Opps! Some error happened. Try after sometime');
                        showLoaderMessage(false);
	        		}

	        		clearInterval(timer);
	        		showLoaderMessage(false);
	        	}
	        }, 500);

        });

       


        $("#user-login-form").on('submit', function(event){
        	event.preventDefault();

            showLoaderMessage(true, "Authenticating ...");

        	initAll();

        	var data = $(this).serializeArray();
        	var URL = "{{url('user/login')}}";
        	$.post(URL, data, function(response){
        		console.log(response);

        		if(!response.success) {

        			if(response.error_type == 'VALIDATION_ERROR') {
        				showError(response.error_text);
        				
        				if(response.error_data.email)  {
        					emailFieldError('user-login-form', response.error_data.email);	
        				}

        				if(response.error_data.password)  {
        					passwordFieldError('user-login-form', response.error_data.password);	
        				}
        				
        				$("#user-login-form").find("> input[name='email']").border
        			} else if(response.error_type == "LOGIN_FAILED"){
        				showError(response.error_text);
        			}

        		} else {
        			showSuccess(response.success_text+" Redirecting...");
     				window.location.href = response.success_data.redirect_url;
        		}
                showLoaderMessage(false);
        	});

        });


        $("#doctor-login-form").on('submit', function(event){
        	event.preventDefault();
            showLoaderMessage(true, "Authenticating ...");
        	initAll();

        	var data = $(this).serializeArray();
        	var URL = "{{url('doctor/login')}}";
        	$.post(URL, data, function(response){
        		console.log(response);

        		if(!response.success) {

        			if(response.error_type == 'VALIDATION_ERROR') {
        				showError(response.error_text);
        				
        				if(response.error_data.email)  {
        					emailFieldError('doctor-login-form', response.error_data.email);	
        				}

        				if(response.error_data.password)  {
        					passwordFieldError('doctor-login-form', response.error_data.password);	
        				}
        				
        				$("#doctor-login-form").find("> input[name='email']").border
        			} else if(response.error_type == "LOGIN_FAILED"){
        				showError(response.error_text);
        			}

        		} else {
        			showSuccess(response.success_text+" Redirecting to ...");
     				window.location.href = response.success_data.redirect_url;
        		}
                showLoaderMessage(false);
        	});

        });



    });


    
	</script>

@endsection
		