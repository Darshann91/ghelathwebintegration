<!-- <div class="col-sm-8" id="appointment_block"> -->
<div class="row" style="margin-left:0px">
    <div class="container">
        <div class="breadcrumb_outer">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item"><a href="#">Appointment</a></li>
                <li class="breadcrumb-item active">Up Coming Appointment</li>
            </ol>
            <h4>Appointment scheduled by patients for consultation.</h4>
        </div>
    </div>
</div>
<div class="col-xs-12 col-sm-12 col-lg-12">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs appointment-tabs" role="tablist">
        <li role="presentation" class="active appointment-tab-li" id="upcoming_li">
            <a data-target="#up_coming" aria-controls="up_coming" role="tab" data-toggle="tab" ng-click="appointmentTabClicked('up_coming')">
                <h4><i class="fa fa-star"></i>&nbsp;&nbsp;Up Coming</h4>
            </a>
        </li>
        <!-- <li role="presentation" id="history_li" class="appointment-tab-li">
            <a data-target="#history" id="" aria-controls="history" role="tab" data-toggle="tab" ng-click="appointmentTabClicked('history')">
                <h4><i class="fa fa-history"></i>&nbsp;&nbsp;History</h4>
            </a>
            </li> -->
        <li role="presentation" id="myhour_li" class="appointment-tab-li" >
            <a data-target="#my_hour" id="third" aria-controls="my_hour" role="tab" data-toggle="tab" ng-click="appointmentTabClicked('my_hour')">
                <h4><i class="fa fa-cogs"></i>&nbsp;&nbsp;My Hour</h4>
            </a>
        </li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="up_coming">
            <div class="list-group appointment-list-group">
                <a class="list-group-item active list-group-item-header">
                    <h4 class="list-group-item-heading">Today</h4>
                </a>
                <a class="list-group-item list-item" ng-repeat="schedule in todaysSchedules">
                    <h4 class="list-group-item-heading">[[schedule.user_name]]</h4>
                    <p class="list-group-item-text">[[schedule.address]]</p>
                    <p class="list-group-item-text">Time: <strong>[[schedule.start_time]][[schedule.ampm]]</strong></p>
                </a>
                <a class="list-group-item" ng-show="!todaysSchedules.length">
                    <p class="list-group-item-text" style="text-align:center">No booking found for today</p>
                </a>
            </div>
            <div class="list-group appointment-list-group">
                <a class="list-group-item active list-group-item-header">
                    <h4 class="list-group-item-heading">Tomorrow</h4>
                </a>
                <a class="list-group-item list-item" ng-repeat="schedule in tomorrowSchedules">
                    <h4 class="list-group-item-heading">[[schedule.user_name]]</h4>
                    <p class="list-group-item-text">[[schedule.address]]</p>
                    <p class="list-group-item-text">Time: <strong>[[schedule.start_time]][[schedule.ampm]]</strong></p>
                </a>
                <a class="list-group-item" ng-show="!tomorrowSchedules.length">
                    <p class="list-group-item-text" style="text-align:center">No booking found for tomorrow</p>
                </a>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="my_hour">
            <div class="gray-bottom top-bottom">
                <h4 class="blue">
                    Doctor On Demand Status
                    <div class="slideThree" style="top:7px">  
                        <input type="checkbox" ng-model="consult.doc_ondemand_status" ng-click="setConsultStatus()" ng-true-value="1" ng-false-value="0" ng-init="consult.doc_ondemand_status={{$consultStatus['chat_consult']}}"/>
                        <label></label>
                    </div>
                </h4>
            </div>
            <div class="gray-bottom onoff">
                <h4 class="blue">Online Consultation Status</h4>
                <ul class="list-group">
                    <li class="list-group-item list-group-consult-status">
                        <i class="fa fa-comments consult-item-icon"></i>Chat Consult
                        <div class="slideThree" style="float:right">  
                            <input type="checkbox" ng-model="consult.chat_consult_status" ng-click="setConsultStatus()" ng-true-value="1" ng-false-value="0" ng-init="consult.chat_consult_status={{$consultStatus['chat_consult']}}"/>
                            <label></label>
                        </div>
                    </li>
                    <li class="list-group-item list-group-consult-status">
                        <i class="fa fa-phone consult-item-icon"></i>Phone Consult
                        <div class="slideThree" style="float:right">  
                            <input type="checkbox" ng-model="consult.phone_consult_status" ng-click="setConsultStatus()" ng-true-value="1" ng-false-value="0" ng-init="consult.phone_consult_status={{$consultStatus['phone_consult']}}"/>
                            <label></label>
                        </div>
                    </li>
                    <li class="list-group-item list-group-consult-status">
                        <i class="fa fa-video-camera consult-item-icon"></i>Video Consult
                        <div class="slideThree" style="float:right">  
                            <input type="checkbox" ng-model="consult.video_consult_status" ng-click="setConsultStatus()" ng-true-value="1" ng-false-value="0" ng-init="consult.video_consult_status={{$consultStatus['video_consult']}}"/>
                            <label></label>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="">
                <div class="blue-bottom">
                    <h4 class="blue">Appointment Slots</h4>
                </div>
                <div class="well well-sm" style="margin-top: 5px;">
                    <label>Select date : </label>
                    <div class="input-group">
                        <input type="text" class="form-control" jqdatepicker ng-model="appointment.slotDate" select-finish="appointmentDateChanged()">
                        <span class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                        </span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <!-- FIRST TABLE -->
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th colspan="4" class="text-center">Morning
                                        <i class="fa fa-check-square-o pull-right" ng-click="unSelctAllSlots('m',filteredslotsmorning)" ng-show="isAllMorningSlotsSelected" title="Uncheck all" style="cursor: pointer;"></i>
                                        <i class="glyphicon glyphicon-unchecked pull-right" ng-click="selctAllSlots('m',filteredslotsmorning)" ng-show="!isAllMorningSlotsSelected" title="Check all" style="cursor: pointer;"></i>
                                    </th>
                                </tr>
                            </thead>
                            <tbody class="text-center">
                                <tr ng-repeat="slot in filteredslotsmorning=(slots| filter:mormingSlotsFilter)" ng-if="$index % 4 == 0">
                                    <td class="slot-time-item" ng-click="slotTimeSelected('m',filteredslotsmorning[$index].id)">
                                        [[filteredslotsmorning[$index].start_time]] - [[filteredslotsmorning[$index].end_time]] am
                                        <i class="fa fa-check select-slot" ng-show="isSlotSaved(filteredslotsmorning[$index].id)"></i>
                                    </td>
                                    <td ng-if="filteredslotsmorning.length > ($index + 1)" class="slot-time-item" ng-click="slotTimeSelected('m',filteredslotsmorning[$index+1].id)">
                                        [[filteredslotsmorning[$index + 1].start_time]] - [[filteredslotsmorning[$index + 1].end_time]] am
                                        <i class="fa fa-check select-slot" ng-show="isSlotSaved(filteredslotsmorning[$index + 1].id)"></i>
                                    </td>
                                    <td ng-if="filteredslotsmorning.length > ($index + 2)" class="slot-time-item" ng-click="slotTimeSelected('m',filteredslotsmorning[$index+2].id)">
                                        [[filteredslotsmorning[$index + 2].start_time]] - [[filteredslotsmorning[$index + 2].end_time]] am
                                        <i class="fa fa-check select-slot" ng-show="isSlotSaved(filteredslotsmorning[$index + 2].id)"></i>
                                    </td>
                                    <td ng-if="filteredslotsmorning.length > ($index + 3)" class="slot-time-item" ng-click="slotTimeSelected('m',filteredslotsmorning[$index+3].id)">
                                        [[filteredslotsmorning[$index + 3].start_time]] - [[filteredslotsmorning[$index + 3].end_time]] am
                                        <i class="fa fa-check select-slot" ng-show="isSlotSaved(filteredslotsmorning[$index + 3].id)"></i>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <!-- SECOND TABLE -->
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th colspan="4" class="text-center">Afternoon
                                        <i class="fa fa-check-square-o pull-right" ng-click="unSelctAllSlots('a',filteredslotsafternoon)" ng-show="isAllAfternoonSlotsSelected" title="Uncheck all" style="cursor: pointer;"></i>
                                        <i class="glyphicon glyphicon-unchecked pull-right" ng-click="selctAllSlots('a',filteredslotsafternoon)" ng-show="!isAllAfternoonSlotsSelected" title="Check all" style="cursor: pointer;"></i>
                                    </th>
                                </tr>
                            </thead>
                            <tbody class="text-center">
                                <tr ng-repeat="slot in filteredslotsafternoon=(slots| filter:afternoonSlotsFilter)" ng-if="$index % 4 == 0">
                                    <td class="slot-time-item" ng-click="slotTimeSelected('a',filteredslotsafternoon[$index].id)">
                                        [[filteredslotsafternoon[$index].start_time]] - [[filteredslotsafternoon[$index].end_time]] pm
                                        <i class="fa fa-check select-slot" ng-show="isSlotSaved(filteredslotsafternoon[$index].id)"></i>
                                    </td>
                                    <td ng-if="filteredslotsafternoon.length > ($index + 1)" class="slot-time-item" ng-click="slotTimeSelected('a',filteredslotsafternoon[$index+1].id)">
                                        [[filteredslotsafternoon[$index + 1].start_time]] - [[filteredslotsafternoon[$index + 1].end_time]] pm
                                        <i class="fa fa-check select-slot" ng-show="isSlotSaved(filteredslotsafternoon[$index + 1].id)"></i>
                                    </td>
                                    <td ng-if="filteredslotsafternoon.length > ($index + 2)" class="slot-time-item" ng-click="slotTimeSelected('a',filteredslotsafternoon[$index+2].id)">
                                        [[filteredslotsafternoon[$index + 2].start_time]] - [[filteredslotsafternoon[$index + 2].end_time]] pm
                                        <i class="fa fa-check select-slot" ng-show="isSlotSaved(filteredslotsafternoon[$index + 2].id)"></i>
                                    </td>
                                    <td ng-if="filteredslotsafternoon.length > ($index + 3)" class="slot-time-item" ng-click="slotTimeSelected('a',filteredslotsafternoon[$index+3].id)">
                                        [[filteredslotsafternoon[$index + 3].start_time]] - [[filteredslotsafternoon[$index + 3].end_time]] pm
                                        <i class="fa fa-check select-slot" ng-show="isSlotSaved(filteredslotsafternoon[$index + 3].id)"></i>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <!-- THIRD TABLE -->
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th colspan="4" class="text-center">Evening
                                        <i class="fa fa-check-square-o pull-right" ng-click="unSelctAllSlots('e',filteredslots)" ng-show="isAllEveningSlotsSelected" title="Uncheck all" style="cursor: pointer;"></i>
                                        <i class="glyphicon glyphicon-unchecked pull-right" ng-click="selctAllSlots('e',filteredslots)" ng-show="!isAllEveningSlotsSelected" title="Check all" style="cursor: pointer;"></i>
                                    </th>
                                </tr>
                            </thead>
                            <tbody class="text-center">
                                <tr ng-repeat="slot in filteredslots=(slots|filter:eveningSlotsFilter)" ng-if="$index % 4 == 0">
                                    <td class="slot-time-item" ng-click="slotTimeSelected('e',filteredslots[$index].id)">
                                        [[filteredslots[$index].start_time]] - [[filteredslots[$index].end_time]] pm
                                        <i class="fa fa-check select-slot" ng-show="isSlotSaved(filteredslots[$index].id)"></i>
                                    </td>
                                    <td ng-if="filteredslots.length > ($index + 1)" class="slot-time-item" ng-click="slotTimeSelected('e',filteredslots[$index+1].id)">
                                        [[filteredslots[$index + 1].start_time]] - [[filteredslots[$index + 1].end_time]] pm
                                        <i class="fa fa-check select-slot" ng-show="isSlotSaved(filteredslots[$index + 1].id)"></i>
                                    </td>
                                    <td ng-if="filteredslotsfilteredslots.length > ($index + 2)" class="slot-time-item" ng-click="slotTimeSelected('e',filteredslots[$index+2].id)">
                                        [[filteredslotsfilteredslots[$index + 2].start_time]] - [[filteredslotsfilteredslots[$index + 2].end_time]] pm
                                        <i class="fa fa-check select-slot" ng-show="isSlotSaved(filteredslotsfilteredslots[$index + 2].id)"></i>
                                    </td>
                                    <td ng-if="filteredslots.length > ($index + 3)" class="slot-time-item" ng-click="slotTimeSelected('e',filteredslots[$index+3].id)">
                                        [[filteredslots[$index + 3].start_time]] - [[filteredslots[$index + 3].end_time]] pm
                                        <i class="fa fa-check select-slot" ng-show="isSlotSaved(filteredslots[$index + 3].id)"></i>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group col-lg-4 col-xs-12 pull-right">
                                <button type="submit" class="btn btn-primary reg-btn border0 form-control" ng-click="slotsSelectionDone()">done</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="recurring-slots-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="background: #96cce4;">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Recurrent Time Slots</h4>
            </div>
            <div class="modal-body">
                <p>Select days : </p>
                <span id="recurringDatepickerPositoin"></span>
                <div jqdatepicker ng-model="appointment.recurringSlotDate" select-finish="recurringSlotDateSelected()" date-highlight="true" id="recurringDatepicker"></div>
                <br>
            </div>
            <div class="modal-footer" style="text-align:center">
                <button type="button" class="btn btn-default" ng-click="saveSlots()">Save Slots</button>
            </div>
        </div>
    </div>
</div>
<!-- </div> -->
<!-- APPOINTMENT BLOCK ENDS