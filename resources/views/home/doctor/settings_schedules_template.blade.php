<!-- MY SCHEDULE -->
<div role="tabpanel" class="tab-pane" id="schedule">
    <div class="col-xs-12 col-md-8 col-lg-8 schedule-block">
        <div class="row gray-bottom">
            <h4 class="heading blue-bottom profile-edit-heading margin-to-zero">My Schedule</h4>
            <div class="col-lg-4 pull-right">
                <div class="search-block">
                    <input type="text" placeholder="Select Date" class="form-control" id="schedule-choose-date" ng-model="selectedDate" ng-change="getSchedulesByDate()" ng-init="selectedDate='{{date('Y-m-d')}}'">
                </div>
                <div class="search-icon">
                    <i class="fa fa-calendar"></i>
                </div>
            </div>
            <div class="col-md-12">
            <div class="row" ng-repeat="schedule in schedules" style="margin-top: 10px;">
                <div class="col-md-8 form-group blue-left" style="margin-bottom:0px !important;">
                    <label>[[schedule.user_name]]</label>
                    <h6>[[schedule.address]]</h6>
                    <label>Time: [[schedule.start_time]] [[schedule.ampm]] - [[schedule.end_time]] [[schedule.ampm]]</label>
                </div>
                <div class="col-md-4" style="text-align:center;padding-top: 15px;padding-bottom: 15px;" ng-hide="schedule.is_completed==1">
                        
                        <button type="button" class="btn btn-success" ng-click="endBooking(schedule)">Accept Booking</button>
                        <button type="button" class="btn btn-danger" ng-click="cancelBooking(schedule)">Cancel Booking</button>

                </div>
            </div>
            </div>
            <div class="row" ng-show="showNoSchedule()">
                <div class="col-md-12 form-group">
                    <label>[[no_schedules_found_text]]</label>
                </div>
            </div>
        </div>
    </div>
</div>


<div id="invoice" class="modal fade" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content" style="border-radius:3px;font-weight: 700;">
            <div class="modal-header">
                <h4 class="modal-title">INVOICE</h4>
            </div>
            <div class="modal-body">
                <table class="table table-responsive">
                    <tbody>
                        <tr style="text-align:center">
                            <td colspan="2" style="border-top:0px">
                                <img ng-src="[[endBookingResponse.data.user_picture]]" style="width:70px;height: 70px;border-radius:50%;margin-right: 5px;">
                                <br>
                                <span>[[endBookingResponse.data.user_name]]</span>
                            </td>
                        </tr>
                        <tr>
                            <td>PAYMENT MODE</td>
                            <td>[[endBookingResponse.invoice[0].payment_mode]]</td>
                        </tr>
                        <tr>
                            <td>BASE PRICE</td>
                            <td>[[endBookingResponse.invoice[0].base_price]] [[doc_currency]]</td>
                        </tr>
                        <tr style="background-color:rgba(0, 0, 0, 0.32);;color:white">
                            <td>TOTAL</td>
                            <td>[[endBookingResponse.invoice[0].total]] [[doc_currency]]</td>
                        </tr>
                        <tr>
                            <td colspan="2" style="padding: 8px 0px 0px 0px;">
                                <button type="button" class="btn btn-info" style="width: 100%;height: 100%;margin: 0px;border-radius: 0px;" ng-click="closeInvoice()"><i class="fa fa-check" style="font-size: 24px;margin-right: 4px;"></i></button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>