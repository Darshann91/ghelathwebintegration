@extends('home.doctor.layouts.layout-web3') @section('content')
<div class="outer_user" ng-app="userSettings">
    <div class="section-two">
        <div class="container">
            <div class="row top-bottom">
                <!-- LEFT DIV BLOCK -->
                <div class="col-xs-12 col-md-4 col-lg-4">
                    <div class="accordion" id="accordion1">
                        <ul class="list-group">
                            <a href="#profile" data-toggle="tab" role="tab">
                                <li class="list-group-item"><i class="fa fa-user" aria-hidden="true"></i>&nbsp;&nbsp; Profile</li>
                            </a>
                            <a href="#schedules" data-toggle="tab" role="tab">
                                <li class="list-group-item"><i class="fa fa-calendar" aria-hidden="true"></i>&nbsp;&nbsp; My Schedule</li>
                            </a>
                            <a href="#messages" data-toggle="tab" role="tab">
                                <li class="list-group-item">
                                    <i class="fa fa-comment"></i>&nbsp;&nbsp; Messages
                                </li>
                            </a>
                        </ul>
                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <ul class="list-group">
                                    <li class="list-group-item" data-toggle="">
                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">
                                            <!-- <ul class="list-group"> -->
                                            <i class="fa fa-clock-o" aria-hidden="true"></i>&nbsp;&nbsp; Consult History
                                        </a>
                                    </li>
                                </ul>
                                <!-- </a> -->
                            </div>
                            <div id="collapseTwo" class="accordion-body collapse">
                                <div class="accordion-inner">
                                    <ul class="list-group" style="margin:0;">
                                        <a href="#chat-consult" data-toggle="tab" role="tab">
                                            <li class="list-group-item">
                                                <i class="fa fa-comment" aria-hidden="true"></i>&nbsp;&nbsp; Chat Consult
                                            </li>
                                        </a>
                                        <a href="#phone-consult" data-toggle="tab" role="tab">
                                            <li class="list-group-item">
                                                <i class="fa fa-phone" aria-hidden="true"></i>&nbsp;&nbsp;Phone Consult
                                            </li>
                                        </a>
                                        <a href="#video-consult" data-toggle="tab" role="tab">
                                            <li class="list-group-item">
                                                <i class="fa fa-video-camera" aria-hidden="true"></i>&nbsp;&nbsp;Video Consult
                                            </li>
                                        </a>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <ul class="list-group">
                            <a href="#report-issue" data-toggle="tab" role="tab">
                                <li class="list-group-item">
                                    <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>&nbsp;&nbsp;Report Issue
                                </li>
                            </a>
                            <a href="{{url('doctor/logout')}}">
                                <li class="list-group-item" style="border-bottom: 1px solid #ddd;">
                                    <i class="fa fa-sign-out" aria-hidden="true"></i>&nbsp;&nbsp;Sign Out
                                </li>
                            </a>
                        </ul>
                    </div>
                </div>
                <!-- END LEFT DIV BLOCK -->
                <!-- RIGHT DIV BLOCK -->
                <div class="tab-content">
                    <ng-view></ng-view>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- OUTER USER PROFILE -->
@endsection @section('top-scripts')
<style type="text/css">
    .message-item {
        box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
    }
    
    .message-item > .sender,
    .message-item > .receiver {
        display: inline-flex;
        padding: 2px;
        border: 1px solid rgba(0, 0, 0, 0.14);
        width: 100%;
    }
    
    .message-item > .sender img,
    .message-item > .receiver img {
        width: 50px;
        height: 50px;
    }
    
    .message-item > .sender > .content {
        margin-left: 10px;
    }
    
    .message-item > .receiver > .content {
        width: 100%;
    }
    
    .fav-user-item {
        display: inline-flex;
        padding: 2px;
        border: 1px solid rgba(0, 0, 0, 0.14);
        width: 100%;
        cursor: pointer;
        transition: .3s all;
    }
    
    .fav-user-item:hover {
        background: rgba(0, 0, 0, 0.08);
    }
    
    .active-fav-user-item {
        background: rgba(0, 0, 0, 0.32)
    }
    
    .fav-user-item > img {
        width: 40px;
        height: 40px;
    }
    
    .fav-user-item label {
        margin-bottom: initial;
        font-weight: initial;
    }
    
    .fav-user-container {
        max-height: 350px;
        overflow-y: scroll;
        margin-top: 10px;
        height: 350px;
    }
    
    .message-create-form {
        box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
        padding: 15px;
        margin-top: 10px;
        position: relative;
    }
    
    .fav-user-active {
        background: rgba(0, 0, 0, 0.1);
    }
    
    @media (min-width: 768px) {
        #send-message-modal .modal-dialog {
            width: 850px;
        }
    }
    .crop 
        {
            overflow:hidden; 
            white-space:nowrap; 
            text-overflow:ellipsis; 
            width:100px; 
        }​

        
</style>
@endsection @section('bottom-scripts')
<!-- angular route code -->
<script type="text/javascript">
    var auth_doctor_picture        = "{{$auth_doctor->picture}}";
    var currency                  = "{{$auth_doctor->currency}}";
    var auth_doctor_name           = "{{$auth_doctor->first_name}} {{$auth_doctor->last_name}}";
    var profile_template_url       = "{{url('doctor/settings/template/profile')}}";
    var profile_details_url        = "{{url('doctor/settings/profile/details')}}";
    var profile_details_update_url = "{{url('doctor/settings/profile/update')}}";
    var csrf_token                 = "{{csrf_token()}}";
    var otp_send_url               = "{{url('otp/send')}}";
    var schedules_template_url     = "{{url('doctor/settings/template/schedules')}}";
    var get_schedules_url          = "{{url('doctor/settings/my-schedules')}}";
    var get_schedule_dates         = "{{url('doctor/settings/schedule/dates')}}";
    var messages_template_url      = "{{url('doctor/settings/template/messages')}}";
    var messages_list              = "{{url('doctor/messages-list')}}";
    var fav_patients               = "{{url('doctor/favourite/patients')}}";
    var send_message               = "{{url('doctor/user/send-message')}}";
    var end_booking                = "{{url('doctor/appointments/end')}}";
    var cancel_booking             = "{{url('doctor/appointments/cancel')}}";
</script>
<script type="text/javascript" src="{{url('web/js/doctor_settings.js')}}"></script>
@endsection