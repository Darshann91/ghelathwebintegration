@extends('home.doctor.layouts.layout-signup')
@section('content')

<style type="text/css">
	.error_div
	{
		color:red;
		display: none;
	}
	.loader
	{
		width: 18px;
		display: none;
	}
	.outer .doctor-reg .Gloco-inner
	{
		    padding: 27px 0px;
	}
</style>


	<!--login-content-->
	<div class="doctor-reg">
		<div class="container-fluid">
			<div class="row">
				<h4 class="welcome-header">Welcome to gHealth Doctor Registration</h4>
				<div class="col-md-12 paddin-left">
					<div class="Gloco-inner">
					<div class="alert alert-success reg-success" style="display: none">
					</div>
					<form id="doctor-register-form" method="POST" enctype="multipart/form-data">

						@if(request()->has('login_by'))
							<input type="hidden" name="login_by" value="{{request()->login_by}}">
							<input type="hidden" name="social_unique_id" value="{{request()->social_unique_id}}">
						@endif
						<div class="col-md-12 paddin" style="padding-top: 0px;padding-bottom:0px;">

							<div class="col-md-6 form-group paddin">
								<label>First Name</label>
								<input type="text" class="form-control form-cntrl" placeholder="First Name" name="first_name" value="{{request()->first_name}}"/>
								<span class="error_div">*</span>
							</div>
							<div class="col-md-6 form-group paddin-right" style="">
								<label>Last Name</label>
								<input type="text" class="form-control form-cntrl" placeholder="Last Name" name="last_name" value="{{request()->last_name}}"/>
								<span class="error_div">*</span>
							</div>
						</div>
						<div class="col-md-12 paddin" style="padding-top:0px;padding-bottom:0px;">
							<div class="col-md-6 form-group paddin" style="margin-bottom:0px;padding-top:0px;">
								<label>Email</label>
								<input type="text" class="form-control form-cntrl" placeholder="Email Address" name="email" value="{{request()->email}}"/>
								<span class="error_div">*</span>
							</div>
							<div class="form-group col-md-6">
								<label>Gender</label>
								<div>
									<label><input type="radio" name="gender" value="male" checked> &nbsp; Male &nbsp; </label>
									<label><input type="radio" name="gender" value="female" @if(request()->gender == 'female') checked @endif> &nbsp; Female</label>
								</div>
							</div>
						</div>
						<div class="col-md-12 paddin" style="padding-top: 0px;padding-bottom:0px;">

							<!-- <div class="col-md-6 form-group paddin">
								<input type="text" class="form-control form-cntrl" placeholder="Enter Date of Birth(DD-MM-YYYY)" name="dob"/>
								<span class="error_div">*</span>
							</div> -->
							<div class="col-md-6 form-group paddin">
								<label>Date of Birth</label>
								<div class="input-group date">
							    	<input type="text" class="form-control" id="dob" placeholder="Choose Date of Birth" name="dob" value="{{request()->dob}}">
							    	<div class="input-group-addon">
							        	<span class="glyphicon glyphicon-th"></span>
							    	</div>
							    </div>
							    <span class="error_div dob-error-div">*</span>
							</div>
							<div class="col-md-6 form-group paddin-right" style="">
							<label>Medical Registration Number</label>
								<input type="text" class="form-control form-cntrl" placeholder="Medical Registration Number" name="med_number"/>
								<span class="error_div">*</span>
							</div>
						</div>

						<div class="col-md-12 paddin" style="padding-top: 0px;padding-bottom:0px;">

							<div class="col-md-6 form-group paddin">
							<label>IC</label>
								<input type="text" class="form-control form-cntrl" placeholder="IC" name="ssn"/>
								<span class="error_div">*</span>
							</div>
							<div class="col-md-6 form-group paddin-right" style="">
								<label>Nationality</label>
								<select class="form-control form-cntrl" name="nationality">
										<option value="">--Nationality--</option>
										@foreach($countryCodes as $code)
											<option value="{{$code['country']}}">{{$code['country']}}</option>
										@endforeach
								</select>
								<span class="error_div nationality-error-div">*</span>
							</div>
						</div>
						
						<div class="col-md-12 paddin" style="padding-top:0px;padding-bottom:0px;">
							<div class="form-group col-md-6 paddin pwd-paddin" style="">
								<label>Password</label>
								<input type="password" class="form-control form-cntrl" placeholder="Enter Password (6+ characters)" name="password"/> 
								<span class="error_div">*</span>
							</div>
							<div class="col-md-6 form-group paddin-right" style="margin-bottom:0px;">
								<label>Confirm Password</label>
								<input type="password" class="form-control form-cntrl" placeholder="Enter Confirm Password" name="password_confirmation"/> 
								<span class="error_div">*</span>
							</div>
						</div>
						<div class="col-md-12 paddin" style="padding-top:0px;padding-bottom:0px;">
							<div class="col-md-6  form-group paddin" style="">
							<label>Language</label>
								<select class="form-control" name="language">
									<option value="">--Language--</option>
									<option value="English (en)">English(en)</option>
									<option value="Chinese (zh)">Chinese(zh)</option>
									<option value="Thailand (th)">Thailand(th)</option>
									<option value="Indonesia (in)"> Bahasa Indonesia(in)</option>
									<option value="Vietnamese (vi)">Vietnamese(vi)</option>
									<option value="Burmese (my)">Burmese(my)</option>
									<option value="Farsi (fa)">Farsi(fa)</option>
									<option value="Japanese (ja)">Japanese(ja)</option>
								</select>
								<span class="error_div">*</span>
							</div>
							<div class="col-md-6  form-group paddin-right" style="">
							<label>Currency</label>
								<select class="form-control form-cntrl" name="currency">
										<option value="">--Currency--</option>
										<option value="MYR">MYR</option>
										<option value="USD">USD</option>
										<option value="CNY">CNY</option>
										<option value="THB">THB</option>
										<option value="SGD">SGD</option>
										<option value="IDR">IDR</option>
										<option value="VND">VND</option>
										<option value="MMK">MMK</option>
										<option value="IRR">IRR</option>
										<option value="JPY">JPY</option>
								</select>
								<span class="error_div">*</span>
							</div>
						</div>
						<div class="col-md-12 form-group paddin" style="padding-top:0px;padding-bottom:0px;">
							<div class="col-md-4 paddin-left" style="height: 150px;overflow:hidden">
								<img src="" id="profile_picuture" width="100%" height="100%" style="background: rgba(0, 0, 0, 0.08);">
							</div>
							<div class="col-md-8 paddin" style="padding-bottom:0px;">
								<input type="file" class="form-control form-cntrl" name="picture" accept=".jpg, .bmp, .png, .gif. .jpeg, .JPG, .JPEG" onchange="shotInputImage(this)" />
								<span class="error_div picture_error_div">*</span>
							</div>
						</div>
						<div class="col-md-12 paddin" style="padding-top:0px;padding-bottom:0px;">
							<div class="col-md-3  form-group paddin-left" style="">
								<select class="form-control form-cntrl" name="country_code">
									@foreach($countryCodes as $code)
										<option value="{{$code['country_phone_code']}}">{{$code['country_phone_code']}}</option>
									@endforeach
								</select>
							</div>
							<div class="col-md-6  form-group paddin-left" style="">
								<input type="number" class="form-control form-cntrl" placeholder="Enter Phone Number" name="phone"/>
								<span class="error_div">*</span>
							</div>
							<div class="col-md-3  form-group paddin" style="padding-top: 0px;padding-bottom: 0px;margin-bottom: 0px;">
								<button type="button" id="send-otp-btn" class="btn btn-primary form-control form-cntr">SEND OTP</button>
							</div>
						</div>
						<div class="col-md-12 form-group paddin" style="padding-top:0px;padding-bottom:0px;">
							<div class="col-md-12 paddin" style="padding-bottom:0px;">
								<input type="number" class="form-control form-cntrl" placeholder="Enter Received OTP" name="otp"/>
								<span class="error_div">*</span>
							</div>
						</div>
						
						
						<div class="col-md-12 paddin" style="padding-top:0px;padding-bottom:0px;">
							<div class="checkbox">
							  	<label><input type="checkbox" value="policy" name="policy">By signing up, I agree to Terms and Privacy Policy</label><br>
							  	<span class="error_div policy_error_div">*</span>
							</div>
						</div>

						<button type="submit" href="#" class="btn btn-primary reg-btn">
							<img src="{{asset('web/images/loader.svg')}}" class="loader">
							REGISTER
						</button>
						</form>
					</div>
				</div>
				<!-- <div class="col-md-4">
					<div class="join-us">
						<h4>Why choose gHealth.com ?</h4>
					</div>
					<p class="text-center join-us-p">Join Us Now!</p>
				</div> -->
				
			</div><!--row-->
		</div><!--container-fluid-->
	</div>
@endsection

@section('scripts_and_styles')
<script type="text/javascript">

$("input[name='picture']").filestyle({
	iconName: "glyphicon glyphicon-picture", 
	buttonText:"Profile picture"
});


function getUrlVars()
{
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

function dataURItoBlob(dataURI) {
    // convert base64/URLEncoded data component to raw binary data held in a string
    var byteString;
    if (dataURI.split(',')[0].indexOf('base64') >= 0)
        byteString = atob(dataURI.split(',')[1]);
    else
        byteString = unescape(dataURI.split(',')[1]);

    // separate out the mime component
    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

    // write the bytes of the string to a typed array
    var ia = new Uint8Array(byteString.length);
    for (var i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
    }

    return new Blob([ia], {type:mimeString});
}

function shotInputImage(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#profile_picuture')
                .attr('src', e.target.result)
                .width(150)
                .height(200);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

function hideErrors()
{
	$(".error_div").hide();
	$(".policy_error_div").hide();
	$(".picture_error_div").hide();
}


function showError(field, errorText)
{
	$("input[name='"+field+"'] + .error_div").show().text("*"+errorText);
}

function hideError(field)
{
	$("input[name='"+field+"'] + .error_div").hide();	
}


function showLoader(show)
{
	if(show) 
		$(".loader").show();
	else
		$(".loader").hide();
}


$(document).ready(function(){

	$("#dob").datepicker({
	    dateFormat: 'dd-mm-yy',
	    maxDate: '0',
	    changeMonth: true,
	    changeYear: true,
	    yearRange: "-100:+0",
	});

	
	@if(request()->has('picture'))
		$("#profile_picuture").attr('src', getUrlVars()['picture']);
	@endif




	$("#doctor-register-form").on('submit', function(event){

		event.preventDefault();

		hideErrors();

		showLoaderMessage(true, "Registerting ...");

		var data = new FormData(this);
		var URL = "{{url('doctor/register')}}";

		@if(request()->has('picture'))
			var blob = dataURItoBlob($("#profile_picuture").attr('src'));
			data.delete('picture');
			data.append('picture', blob);

		@endif



		/*if($("input[name='picture']")[0].files[0] != undefined) {
    		data.append('picutre', $("input[name=picture]")[0].files[0]);
    	}*/
		console.log(data);
    	
    	$.ajax({
      		url: URL,
      		type: 'POST',
      		data: data,
      		processData: false,
      		contentType: false,
      		success : function(response) {
      			

      			if(!response.status && response.error_type == "VALIDATION_ERROR") {

      				for (var property in response.error_data) {
      					
      					if(property == 'policy')  {
      						$(".policy_error_div").show().text('*Accept terms and privacy policy');
      						continue;
      					} 

      					if(property == 'picture') {
      						$(".picture_error_div").show().text("*"+response.error_data.picture);
      						continue;
      					}

      					if(property == 'nationality') {
      						$(".nationality-error-div").show().text("*"+response.error_data.nationality);
      						continue;
      					}

      					if(property == 'dob') {
      						$(".dob-error-div").show().text("*"+response.error_data.dob);
      						continue;
      					}

      					//console.log(response.error_data[property]);
      					showError(property, response.error_data[property]);
					}

      			} else if(!response.status && response.error_type=="INVALID_OTP") {
      				showError('otp', response.error_text);
      			} else if(!response.status && response.error_type=="UNKNOWN_ERROR"){
      				toastr.error('Unknown error. Try after sometime');
      			} else {
      				$(".reg-success").html('<strong>Success!</strong> Registration done successfully. Redirecting to login ..').show();
      				toastr.success('Registration done successfully. Redirecting to login ..');
      				setTimeout(function(){
      					window.location.href = "{{url('/login')}}";
      				}, 2000);
      			}

      			showLoaderMessage(false);
      		}
    	});

	});	



	$("#send-otp-btn").on("click", function(){

		showLoaderMessage(true, "Sending otp ...");
		$(this).attr("disabled", true);

		var phone = $("input[name='phone']").val();
		var countryCode = $("select[name='country_code']").val();
		var URL = "{{url('otp/send')}}";
		$.post(URL, {phone:phone, country_code:countryCode, _token:"{{csrf_token()}}"}, function(response){

			if(!response.status && response.error_type == "VALIDATION_ERROR") {

				toastr.error(response.error_text);

			} else if(!response.status && response.error_type == "UNKNOWN_ERROR") {

				toastr.error('Some error. Try after sometime.')

			} else if(response.status && response.success_type == "OTP_SENT") {

				toastr.success(response.success_text);
			}

			$("#send-otp-btn").removeAttr("disabled");
			showLoaderMessage(false);
		});

	});



});





</script>
@endsection
