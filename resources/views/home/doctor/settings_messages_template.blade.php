<div role="tabpanel" class="tab-pane" id="profile">
    <div class="col-xs-12 col-md-8 col-lg-8 profile-block">
        <h4 class="heading profile-edit-heading blue-bottom margin-to-zero">Messages</h4>
        <div class="row">
            <div class="col-lg-12" style="margin-bottom: 10px">
                <button type="button" class="btn btn-info" ng-click="openSendMessageModal()" style="width:100%">Send Message To User</button>
            </div>
            <div class="col-lg-12">
                <div class="well message-item" ng-repeat="message in messages">
                    <label>#Message</label>
                    <div class="sender">
                        <span>
                            <img ng-src="[[auth_picture]]">
                            <br>
                            <span style="font-size:9px" class="crop">[[auth_name]]</span>
                        </span>
                        <div class="content">[[message.sent]]</div>
                    </div>
                    <label style="width: 100%;text-align: right;" ng-hide="message.reply == ''">#Reply</label>
                    <label style="width: 100%;text-align: right;" ng-show="message.reply == ''">#No reply by patient</label>
                    <div class="receiver" ng-hide="message.reply == ''">
                        <div class="content">[[message.reply]]</div>
                        <span>
                            <img ng-src="[[message.picture]]">
                            <br>
                            <span style="font-size:9px" class="crop">[[message.user_name]]</span>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="send-message-modal" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content" style="border-radius: 0px">
            <div class="modal-header">
                <button type="button" class="close" ng-click="cloaseSendMessageModal()">&times;</button>
                <h4 class="modal-title">Send new message to user</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-4">
                        <label>#Fav Users List</label>
                        <div class="fav-user-container">
                            <div class="fav-user-item" ng-repeat="user in favPatients" ng-click="selectPatient(user)" ng-class="selectedPatient.user_id == user.user_id? 'fav-user-active' : ''">
                                <img ng-src="[[user.picture]]">
                                <div style="margin-left:5px">
                                    <label>[[user.user_name]]</label>
                                    <br>
                                    <label>[[user.phone]]</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8" style="padding-left: 0px;padding-right: 10px;">
                        <label>Create Message</label>
                        <form class="message-create-form">
                            <div class="form-group">
                                <label for="">User Name:</label>
                                <input type="text" class="form-control" style="border: 1px solid rgba(0, 0, 0, 0.1);" disabled ng-model="selectedPatient.user_name">
                            </div>
                            <div class="form-group">
                                <label for="">Message</label>
                                <textarea class="form-control" style="border: 1px solid rgba(0, 0, 0, 0.1);min-height: 179px;" ng-model="message"></textarea>
                            </div>
                            <button type="submit" class="btn btn-default" ng-click="sendMessage()">Send</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>