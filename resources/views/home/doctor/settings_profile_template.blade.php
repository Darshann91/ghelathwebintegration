<div role="tabpanel" class="tab-pane" id="profile">
    <div class="col-xs-12 col-md-8 col-lg-8 profile-block">
        <h4 class="heading profile-edit-heading blue-bottom margin-to-zero">My Profile</h4>
        <div class="row">
            <div class="col-lg-8 paddin0">
                <div class="form-group">
                    <div class="col-xs-4 paddin-right0">
                        <select class="form-control border0" disabled>
                            <option>Dr.</option>
                            <option>Mr.</option>
                        </select>
                    </div>
                    <div class="col-xs-8">
                        <input type="text" placeholder="Enter Your Frist Name" class="form-control margin-top0" ng-model="doctor.first_name" disabled>
                    </div>
                    <div class="col-lg-12">
                        <input type="text" placeholder="Enter Your Last Name" class="form-control" ng-model="doctor.last_name" disabled>
                    </div>
                    <div class="inline-form">
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <input type="radio" ng-model="doctor.gender" value="male">&nbsp; Male
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <input type="radio" ng-model="doctor.gender" value="female">&nbsp; Female
                    </div>
                    <div class="col-lg-12" class="form-control">
                        <input type="text" placeholder="Email Address" class="form-control" ng-model="doctor.email" disabled>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="row">
                    <img class="img-responsive img-circle" ng-src="[[profileDetails.doctor.picture]]">
                </div>
            </div>
            <div class="col-lg-6 col-md-6 form-group">
                <input type="password" ng-model="doctor.old_password" class="form-control" placeholder="Enter Old Password">
            </div>
            <div class="col-lg-6 col-md-6 form-group">
                <input type="password" ng-model="doctor.new_password" class="form-control" placeholder="Enter New Passowrd">
            </div>
            <div class="col-lg-6 col-md-6 form-group">
                <select class="form-control border0" ng-model="doctor.language">
                    <option value="">--Language--</option>
                     <option value="Bahasa Indonesia (in)">Bahasa Indonesia(in)</option>
                            <option value="Burmese (my)">Burmese(my)</option>
                            <option value="Chinese (zh)">Chinese(zh)</option>
                            <option value="English (en)">English(en)</option>
                            <option value="Farsi (fa)">Farsi(fa)</option>
                            <option value="Japanese (ja)">Japanese(ja)</option>
                            <option value="Thai (th)">Thai(th)</option>
                            <option value="Vietnamese (vi)">Vietnamese(vi)</option>
                </select>
            </div>
            <div class="col-lg-6 col-md-6 form-group">
                <select class="form-control border0" ng-model="doctor.currency">
                    <option value="">--Currency--</option>
                    
                            <option value="MYR">MYR</option>
                            <option value="USD">USD</option>
                            <option value="CNY">CNY</option>
                            <option value="THB">THB</option>
                            <option value="SGD">SGD</option>
                            <option value="IDR">IDR</option>
                            <option value="VND">VND</option>
                            <option value="MMK">MMK</option>
                            <option value="IRR">IRR</option>
                            <option value="JPY">JPY</option> 
                </select>
            </div>
            <div class="col-lg-6 col-md-6 form-group">
                <div class="input-group">
                    <input type="text" class="form-control border0" id="dob" placeholder="Choose Date of Birth" ng-model="doctor.dob" style="margin:0 !important;" disabled>
                    <div class="input-group-addon">
                        <span class="glyphicon glyphicon-th"></span>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 form-group">
                <select class="form-control border0" ng-model="doctor.nationality">
                    <option value="">--Nationality--</option>
                    @foreach($countryCodes as $code)
                    <option value="{{$code['country']}}">{{$code['country']}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-lg-4 col-md-4 form-group">
                <select class="form-control border0" ng-model="doctor.country_code" disabled>
                    @foreach($countryCodes as $code)
                    <option value="{{$code['country_phone_code']}}">{{$code['country_phone_code']}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-lg-4 col-md-4 form-group">
                <input type="text" class="form-control" placeholder="Enter Phone Number" ng-model="doctor.phone" disabled>
            </div>
            <div class="col-lg-4 col-md-4 form-group">
                <input type="button" class="form-control btn-primary" value="Request OTP" id="send_otp_btn" ng-click="sendOTP()">
            </div>
            <div class="col-lg-4 col-md-4 form-group">
                <input type="text" ng-model="doctor.otp" class="form-control" placeholder="Enter Otp You received">
            </div>
            <div class="col-lg-4 col-md-4 form-group">
                <input type="text" ng-model="doctor.ssn" class="form-control" placeholder="Enter Social Security Number">
            </div>
            <div class="col-lg-4 col-md-4 form-group">
                <input type="text" ng-model="doctor.med_number" class="form-control" placeholder="Enter Medical Registration Number">
            </div>
            <div class="col-lg-4 col-md-4 form-group">
                <input type="text" class="form-control" placeholder="Enter booking fee" ng-model="doctor.booking_fee">
            </div>
            <div class="col-lg-4 col-md-4 form-group">
                <input type="text" class="form-control" placeholder="Enter on demand fee" ng-model="doctor.ondemand_fee">
            </div>
            <div class="col-lg-4 col-md-4 form-group">
                <input type="text" class="form-control" placeholder="Enter phone fee" ng-model="doctor.phone_fee">
            </div>
            <div class="col-lg-4 col-md-4 form-group">
                <input type="text" class="form-control" placeholder="Enter video fee" ng-model="doctor.video_fee">
            </div>
            <div class="col-lg-4 col-md-4 form-group">
                <input type="text" class="form-control" placeholder="Enter chat fee" ng-model="doctor.chat_fee">
            </div>
            <div class="col-lg-4 col-md-4 form-group">
                <input type="text" class="form-control" placeholder="Enter free minutes" ng-model="doctor.free_minute">
            </div>
            <div class="col-lg-4 col-md-4 form-group">
                <input type="text" class="form-control" placeholder="Enter consult fee minutes" ng-model="doctor.minutes_consult_fee">
            </div>
            <div class="col-lg-4 col-md-4 form-group">
                <input type="text" class="form-control" placeholder="Enter consult fee" ng-model="doctor.minutes_consult_fee">
            </div>
            <div class="col-lg-4 col-md-4 form-group">
                <input type="file" class="form-control" placeholder="Enter consult fee" id="picture">
            </div>
        </div>
        <div class="row  top-bottom form-group" style="text-align: center">
            <button type="button" class="btn btn-primary form-control" ng-click="updateDoctor()" style="width:100px">Update</button>
            <!-- <div class="col-lg-4">
                <button type="submit" class="btn btn-primary reg-btn form-control">Cancel</button>
                </div> -->
        </div>
        <div class="panel-group">
            <div class="panel panel-default">
                <div class="panel-heading">Clinic Details</div>
                <div class="panel-body">
                    <div class="col-md-4">
                        <div class="thumbnail">
                            <img ng-src="[[profileDetails.clinic_details.clinic_image1]]" alt="" style="width:100%">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="thumbnail">
                            <img ng-src="[[profileDetails.clinic_details.clinic_image2]]" alt="Lights" style="width:100%">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="thumbnail">
                            <img ng-src="[[profileDetails.clinic_details.clinic_image3]]" alt="Lights" style="width:100%">
                            <div class="caption">
                                <p>Lorem ipsum...</p>
                            </div>
                        </div>
                    </div>
                    <div>
                        <label>Clinic Name : [[profileDetails.clinic_details.clinic_name]]</label>
                        <label>Clinic Phone : [[profileDetails.clinic_details.clinic_phone]]</label>
                        <label>Clinic Fax : [[profileDetails.clinic_details.clinic_fax]]</label>
                        <label>Clinic Address : [[profileDetails.clinic_details.clinic_address]]</label>
                        <label>Clinic Latitude : [[profileDetails.clinic_details.latitude]]</label>
                        <label>Clinic Longitude : [[profileDetails.clinic_details.longitude]]</label>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">Educations</div>
                <div class="panel-body">
                    <div>
                        <div class="col-md-4">
                            <div class="thumbnail">
                                <img ng-src="[[profileDetails.speciality_details.picture]]" alt="" style="width:100%">
                            </div>
                        </div>
                        <label>spcciality:[[profileDetails.speciality_details.speciality]]</label>
                        <label>experience:[[profileDetails.doctor.experience]]</label>
                    </div>
                    <div>
                        <label>Education1</label>
                        <label>Education1 degree: [[profileDetails.doctor.education1.degree]]</label>
                        <label>Education1 university: [[profileDetails.doctor.education1.university]]</label>
                        <label>Education1 university: [[profileDetails.doctor.education1.year]]</label>
                    </div>
                    <div>
                        <label>Education2</label>
                        <label>Education1 degree: [[profileDetails.doctor.education2.degree]]</label>
                        <label>Education2 university: [[profileDetails.doctor.education2.university]]</label>
                        <label>Education2 university: [[profileDetails.doctor.education2.year]]</label>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>