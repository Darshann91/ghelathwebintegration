@extends('home.doctor.layouts.layout-web3') @section('content')
<div class="outer_doctor_spec" ng-app="homeApp" ng-controller="homeCtrl" id="homeDiv">
    <user-card>
        <div class="container">
            <h4 class="title">[[requestTypeTitle]]</h4>
            <div class="profile-picture">
                <img ng-src="[[currentRequestData.user_picture]]" width="100%" height="100%">
            </div>
            <div class="details">
                <span>[[currentRequestData.user_name]], [[currentRequestData.user_mobile]]</span>
            </div>
            <div class="button-container">
                <button type="button" class="btn btn-success" ng-click="acceptService()">Accept</button>
                <button type="button" class="btn btn-danger" ng-click="rejectService()">Reject</button>
            </div>
        </div>
    </user-card>
    <div id="video-audio-modal" class="modal fade" role="dialog" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Online Consultation</h4>
                </div>
                <div class="modal-body" style="padding:1px">
                    <div id="opentok-publisher" style="position: absolute;width: 100px;height: 100px;z-index: 99;border: 1px solid white;top: 20px;left: 21px;"></div>
                    <div id="opentok-subscriber" style="width: 100%;height: 400px;"></div>
                </div>
                <div class="modal-footer" style="text-align: center;position: absolute;width: 100%;height: 78px;bottom: 0px;background: rgba(0, 0, 0, 0.32);z-index:999999999">
                    <button type="button" class="btn btn-danger" ng-click="completeConsult()" title="End Consultation"><i class="fa fa-times" style="font-size: 28px;"></i></button>
                    <button type="button" class="btn btn-defuult" title="Full/Small Screen" ng-click="toggleFullScreen()"><i class="fa fa-arrows-alt" style="font-size: 28px;color:white"></i></button>
                    <time style="position: absolute;right: 5px;top: 28px;color: white;font-size: 16px;font-weight: 700;">Time [[hour]]:[[min]]:[[sec]]</time>
                </div>
            </div>
        </div>
    </div>
    <div id="invoice" class="modal fade" role="dialog" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content" style="border-radius:3px;font-weight: 700;">
                <div class="modal-header">
                    <h4 class="modal-title">INVOICE</h4>
                </div>
                <div class="modal-body">
                    <table class="table table-responsive">
                        <tbody>
                            <tr style="text-align:center">
                                <td colspan="2" style="border-top:0px">
                                    <img ng-src="[[currentRequestData.user_picture]]" style="width:70px;height: 70px;border-radius:50%;margin-right: 5px;">
                                    <br>
                                    <span>[[currentRequestData.user_name]]</span>
                                </td>
                            </tr>
                            <tr>
                                <td>PAYMENT MODE</td>
                                <td>[[invoice.payment_mode]]</td>
                            </tr>
                            <tr>
                                <td>CONSULTATION FEE</td>
                                <td>[[invoice.base_price]] [[currency]]</td>
                            </tr>
                            <tr style="background-color:rgba(0, 0, 0, 0.32);;color:white">
                                <td>TOTAL</td>
                                <td>[[invoice.total]] [[currency]]</td>
                            </tr>
                            <tr>
                                <td colspan="2" style="padding: 8px 0px 0px 0px;">
                                    <button type="button" class="btn btn-info" style="width: 100%;height: 100%;margin: 0px;border-radius: 0px;" ng-click="closeInvoice()"><i class="fa fa-check" style="font-size: 24px;margin-right: 4px;background:none"></i></button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div id="rating-modal" class="modal fade" role="dialog" style="border-radius:3px;font-weight: 700;" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">FEEDBACK</h4>
                </div>
                <div class="modal-body" style="text-align: center;">
                    <img ng-src="[[currentRequestData.user_picture]]" style="width:70px;height: 70px;border-radius:50%;margin-right: 5px;">
                    <br>
                    <span>[[currentRequestData.user_name]]</span>
                    <div id="doctor-rating">
                        <div rating model="rating"></div>
                    </div>
                    <div class="form-group" style="text-align: initial;">
                        <label for="comment">Comment:</label>
                        <textarea class="form-control" rows="5" style="border: 1px solid rgba(0, 0, 0, 0.12);" placeholder="Type your comment here ..." ng-model="rating.comment"></textarea>
                    </div>
                    <button type="button" ng-click="giveRating()" class="btn btn-info" style="width: 100%;height: 100%;margin: 0px;border-radius: 0px;"><i class="fa fa-check" style="font-size: 24px;margin-right: 4px;"></i></button>
                </div>
            </div>
        </div>
    </div>
    <div id="chat-modal" class="modal fade" role="dialog" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header" style="padding:0px">
                    <div class="panel-heading top-bar" style="">
                        <span><i class="fa fa-clock-o" style="border: none;right: 7px;position: absolute;top: 25px;font-size: 20px;margin-right: 2px;"><span style="margin-left: 5px;">[[hour]]:[[min]]:[[sec]]</span></i></span>
                        <div class="col-md-6 col-xs-6 avatar-left">
                            <h3 class="panel-title">
                                <img ng-src="[[currentRequestData.user_picture]]" class="img-circle width-height-50px" alt="doc"><span>&nbsp;&nbsp;[[currentRequestData.user_name]]</span>
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="modal-body" style="padding:0px;">
                    <div class="online-doctor-bolcks" id="chat_consult_block">
                        <div class="panel panel-default chat-box" style="height: 350px;">
                            <div class="panel-body msg_container_base" style="max-height:initial;height:350px;overflow-y: scroll;overflow-x: hidden;" id="online_message_container" >
                            <span ng-repeat="message in messages">
                                <div class="row msg_container base_sent" style="margin-left:0px;margin-right:0px" ng-if="message.type == 'sent' && message.data_type=='TEXT'">
                                    <div class="col-md-8 col-xs-8 ">
                                        <div class="messages msg_sent">
                                            <p>[[message.message]]</p>
                                            <time>[[message.timestamp | chat_timestamp_filter]]</time>
                                        </div>
                                    </div>
                                    <div class="col-md-1.5 col-xs-1.5 avatar-left">
                                        <img ng-src="[[self_picture]]" class="img-circle width-height-50px" alt="doc">
                                    </div>
                                </div>

                                <div class="row msg_container base_sent" style="margin-left:0px;margin-right:0px" ng-if="message.type == 'sent' && message.data_type=='IMAGE'">
                                    <div class="col-md-8 col-xs-8 ">
                                        <div class="messages msg_sent">
                                            <img ng-src="[[message.message]]" style="width:100%">
                                            <time>[[message.timestamp | chat_timestamp_filter]]</time>
                                        </div>
                                    </div>
                                    <div class="col-md-1.5 col-xs-1.5 avatar-left">
                                        <img ng-src="[[self_picture]]" class="img-circle width-height-50px" alt="doc">
                                    </div>
                                </div>

                                <div class="row msg_container base_receive" style="margin-left:0px;margin-right:0px" ng-if="message.type == 'receive' && message.data_type=='TEXT'">
                                    <div class="col-md-1.5 col-xs-1.5 avatar-right">
                                        <img ng-src="[[currentRequestData.user_picture]]" class="img-circle width-height-50px" alt="doc">
                                    </div>
                                    <div class="col-md-8 col-xs-8">
                                        <div class="messages msg_receive">
                                            <p>[[message.message]]</p>
                                            <time>[[message.timestamp | chat_timestamp_filter]]</time>
                                        </div>
                                    </div>
                                </div>


                                <div class="row msg_container base_receive" style="margin-left:0px;margin-right:0px" ng-if="message.type == 'receive' && message.data_type=='IMAGE'">
                                    <div class="col-md-1.5 col-xs-1.5 avatar-right">
                                        <img ng-src="[[currentRequestData.user_picture]]" class="img-circle width-height-50px" alt="doc">
                                    </div>
                                    <div class="col-md-8 col-xs-8">
                                        <div class="messages msg_receive">
                                            <img ng-src="[[message.message]]" style="width:100%">
                                            <time>[[message.timestamp | chat_timestamp_filter]]</time>
                                        </div>
                                    </div>
                                </div>

                            </span>

                            <span class="chat-typing"><i ng-class="chat_notif_popup_fa_icon"> [[chat_notif_popup_text]]</i></span>

                            </div>

                        </div>
                        <div class="row chat" style="margin-right:0px;margin-left:0px;padding:5px">
                            <div class="col-xs-1 col-sm-1 col-md-1 chat-icon" style="padding: 0px;text-align: center;" ng-click="openUploadFileWindow()">
                                <i class="fa fa-paperclip" style="font-size: 24px;top: 8px;position: relative;font-weight: 500;cursor:pointer;" ></i>
                                <input type="file" id="chat_upload_file" style="display: none" onchange="angular.element(this).scope().uploadAttachment(this.files)">
                            </div>
                            <div class="col-xs-10 col-sm-10 col-md-10 col-xs-10">
                                <input type="text" class="form-control" placeholder="Write your message ....." style="border-radius:0px;" ng-model="messageText" ng-keyup="$event.keyCode == 13 && sendMessageText()" ng-keydown="emitTyping()">
                            </div>
                            <div class="col-xs-1 col-sm-1 col-md-1 chat-icon" style="padding: 0px;text-align: center;">
                                <button style="font-size: 24px;top: 2px;position: relative;font-weight: 500;border: none;background: none;" ng-click="sendMessageText()"><i class="fa fa-paper-plane"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer" style="text-align:center;">
                    <button type="button" class="btn btn-primary" ng-click="completeConsult()">Complete Consult</button>
                </div>
            </div>
        </div>
    </div>
    <div class="section-two">
        <div class="container">
            <div class="row">
                <div class="col-sm-8">
                    <ng-view></ng-view>
                </div>
                <div class="col-sm-4 col-xs-12 margin80">
                    <div class="panel-group" id="accordion">
                        <div class="panel panel-default">
                            <div class="panel-heading collapse-blue">
                                <h4 class="panel-title white-text">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" data-target="#collapseOne" id="appointment">
                                    <i class="fa fa-calendar"></i>&nbsp;&nbsp;Appointment
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <form class="form-group">
                                        <label class="radio-inline">
                                        <input type="radio" name="appointment" id="tab_first" ng-model="$root.selectedAppointmentTab" value="up_coming" ng-click="appointmentTabClicked()">
                                        <label>Upcoming Appointment</label>
                                        </label>
                                        <br>
                                        <label class="radio-inline">
                                        <input type="radio" name="appointment" id="tab_third" ng-model="$root.selectedAppointmentTab" value="my_hour" ng-click="appointmentTabClicked()">
                                        <label>Manage My Hour</label>
                                        </label>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading collapse-blue">
                                <h4 class="panel-title white-text">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" id="article">
                                    <i class="fa fa-file-text-o"></i>&nbsp;&nbsp;Health Article
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <form class="form-group">
                                        <div class="radio2">
                                            <label>
                                            <a href="www.google.com" <input type="radio" name="health-article" id="health_article1"> Health Articles
                                            </label> </a>
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-xl-12 panel-collapse collapse" id="collapse4" style="padding:0px;">
                                                <div class="search-group">
                                                    <div class="select-option col-xs-12 col-sm-12 col-md-12 col-xl-12" style="padding:0px;">
                                                        <select class="form-control btn btn-default" style="border-radius:0px;">
                                                            <option class="">categories</option>
                                                            <option>3</option>
                                                        </select>
                                                    </div>
                                                    <div class="search-button col-xs-12 col-sm-12 col-md-12 col-xl-12" style="padding:0px;">
                                                        <button type="submit" class="btn btn-default search-btn form-control" style="border-radius:0px;">SEARCH</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="radio2">
                                            <label>
                                            <input type="radio" name="health-article" id="health_article2"> My Article
                                            </label>
                                        </div>
                                        <div class="radio2">
                                            <label>
                                            <input type="radio" name="health-article" id="health_article3"> Write New Post
                                            </label>
                                        </div>
                                        <!-- </div>  -->
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading collapse-blue">
                                <h4 class="panel-title white-text">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" id="store">
                                    <i class="fa fa-cart-plus"></i>&nbsp;&nbsp;Health Store
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseThree" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <form class="form-group">
                                        <div class="radio2">
                                            <label>
                                            <input type="radio" name="health-store" id="health_store1"> Health Store
                                            </label>
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-xl-12 panel-collapse collapse" id="collapse5" style="padding:0px;">
                                                <div class="search-group">
                                                    <div class="select-option col-xs-12 col-sm-12 col-md-12 col-xl-12" style="padding:0px;">
                                                        <input type="text" name="" placeholder="Types" class="form-control">
                                                    </div>
                                                    <div class="select-option col-xs-12 col-sm-12 col-md-12 col-xl-12" style="padding:0px;">
                                                        <select class="form-control btn btn-default" style="border-radius:0px;">
                                                            <option class="">categories</option>
                                                            <option>3</option>
                                                        </select>
                                                    </div>
                                                    <div class="search-button col-xs-12 col-sm-12 col-md-12 col-xl-12" style="padding:0px;">
                                                        <button type="submit" class="btn btn-default search-btn form-control" style="border-radius:0px;">SEARCH</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="radio2">
                                            <label>
                                            <input type="radio" name="health-store" id="health_store2"> My Store
                                            </label>
                                        </div>
                                        <div class="radio2">
                                            <label>
                                            <input type="radio" name="health-store" id="health_store3"> New Item
                                            </label>
                                        </div>
                                        <!-- </div>  -->
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END OF ACCORDATION BLOCK -->
                    <!-- EXTRA INFO BLOCK -->
                    <div class="container-fluid" style="padding:0;">
                        <div>
                            <div class="well" style="height:300px;">
                                <span class="glyphicon glyphicon-record"></span>
                                <label>Top Recommended Package</label>
                                <div class="col-xs-12 col-sm-12 col-lg-12 like">
                                    <div class="wrap-no-border">
                                        <img class="img-responsive" src="/web/images/gym.jpg" width=100% style="">
                                        <i class="fa fa-heart"></i>
                                        <div class="new" style="margin:10px 0;">
                                            <h5><strong>Weight Loss Package</strong></h5>
                                            <h6 class="text-left">Some Text<strong class="pull-right">MYR 285</strong></h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <label>click here for <a href="{{url('user/register')}}" class="blue">Free Registration</a></label>
                        <div class="panel panel-primary">
                            <div class="panel-heading">Popular Health Tips</div>
                            <div class="panel-body health-tip">
                                <a href="#" class="main_health_article">
                                    <div class="col-sm-6 tip">
                                        <img src="/web/images/jogging.jpg" width=100% height=100 />
                                        <span class="blue">View this Tip</span>
                                    </div>
                                </a>
                                <a href="#" class="main_health_article">
                                    <div class="col-sm-6 tip">
                                        <img src="/web/images/parenting.jpg" width=100% height=100 />
                                        <span class="blue">View this Tip</span>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection @section('top-scripts')
<style>

    .white-text
    {
        color: white !important;
    }

    #collapse4,
    #collapse5,
    #health_article_block,
    #health_store_block,
    #empty_second_block,
    #time_slot,
    .for_list,
    #health_articleTwo {
    display: none;
    }
    .list-group-item-header {
    background: white !important;
    border-color: white !important;
    color: black !important;
    border-bottom: 1px solid rgba(0, 0, 0, 0.11) !important;
    margin-bottom: 10px;
    padding-left: 0px;
    }
    .list-item {
    border-left: 3px solid #33b5e5;
    margin-bottom: 5px !important;
    border-right: 0px;
    border-top: 0px;
    }
    .appointment-tab-li {
    /*width: 33.33%;*/
    width: 50%;
    text-align: center;
    }
    .appointment-tab-li>a {
    padding: 0px 15px !important;
    }
    .appointment-tabs {
    border-bottom: 3px solid #33b5e5;
    background: #F1F1F1;
    }
    .nav-tabs>li.active>a,
    .nav-tabs>li.active>a:focus,
    .nav-tabs>li.active>a:hover {
    background-color: #33b5e5;
    border: 1px solid #33b5e5;
    color: white;
    }
    .appointment-list-group {
    margin-top: 10px !important;
    }
    .consult-item-icon {
    font-size: 25px;
    color: #33b5e5;
    margin-right: 7px;
    }
    .list-group-consult-status {
    border-left: 3px solid #33b5e5;
    }
    .select-slot {
    background-color: green !important;
    color: white;
    }
    .slot-time-item {
    cursor: pointer;
    -webkit-transition: all 0.3s;
    /* Safari */
    transition: all 0.3s;
    }
    .slot-time-item:hover {
    box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.37);
    }
    #recurring-slots-modal .ui-widget.ui-widget-content {
    width: 100%;
    }
    slot-dat-list span {
    display: inline-block;
    padding: 10px;
    background: rgba(0, 0, 0, 0.04);
    border-radius: 17px;
    color: black;
    margin-bottom: 2px;
    margin: 2px;
    box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.13);
    border: 1px solid rgba(0, 0, 0, 0.08);
    }
    .event a {
    border: 1px solid #003eff !important;
    background: #007fff !important;
    font-weight: normal !important;
    color: #ffffff !important;
    }
    #recurring-slots-modal .ui-state-active {
    border: 1px solid #c5c5c5;
    background: #f6f6f6;
    font-weight: normal;
    color: #454545;
    }
    #video-audio-modal .modal-footer .btn {
    border-radius: 50%;
    width: 50px;
    height: 50px;
    }
    #video-audio-modal .modal-footer .btn:hover {
    border-radius: 50%;
    width: 50px;
    height: 50px;
    }
    #video-audio-modal .modal-content:-webkit-full-screen {
    width: 100%;
    height: 100%;
    }
    #video-audio-modal .modal-content:-moz-full-screen {
    width: 100%;
    height: 100%;
    }
    #video-audio-modal .modal-content:-ms-fullscreen {
    width: 100%;
    height: 100%;
    }
    #video-audio-modal .modal-content:fullscreen {
    width: 100%;
    height: 100%;
    }
    .modal-content:-webkit-full-screen .modal-body {
    height: 100%;
    }
    .modal-content:-moz-full-screen .modal-body {
    height: 100%;
    }
    .modal-content:-ms-fullscreen .modal-body {
    height: 100%;
    }
    .modal-content:fullscreen .modal-body {
    height: 100%;
    }
    .modal-content:-webkit-full-screen #opentok-subscriber {
    height: 100% !important;
    }
    .modal-content:-moz-full-screen #opentok-subscriber {
    height: 100% !important;
    }
    .modal-content:-ms-fullscreen #opentok-subscriber {
    height: 100% !important;
    }
    .modal-content:fullscreen #opentok-subscriber {
    height: 100% !important;
    }
    user-card {
    position: fixed;
    width: 100%;
    height: 100%;
    left: 0px;
    top: 0px;
    z-index: 999;
    background: rgba(0, 0, 0, 0.78);
    /* display: flex;*/
    justify-content: center;
    display: none;
    }
    user-card .container {
    max-width: 350px;
    height: 400px;
    display: inline-block;
    align-self: center;
    background: #616161;
    padding: 0px 40px 0px 40px;
    box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    width: 100%;
    }
    user-card .container .profile-picture {
    overflow: hidden;
    }
    user-card .container .title {
    text-align: center;
    color: white;
    }
    user-card .container .details {
    color: white;
    text-align: center;
    padding: 10px;
    }
    user-card .container .button-container {
    text-align: center;
    }
    user-card .container .button-container button {
    width: 49%;
    }
    #doctor-rating {
    width: 100% !important;
    text-align: center !important;
    }
    #doctor-rating > div {
    display: inline-block;
    }
    @media (min-width: 768px) {
    #rating-modal .modal-dialog {
    width: 30%;
    }
    }
    .width-height-50px {
    width: 50px !important;
    height: 50px !important;
    }

    .chat-typing
    {
        position: absolute;
        bottom: 55px;
        left: 5px;
        color: white;
        background: rgba(0, 0, 0, 0.69);
        padding: 9px;
        border-radius: 50px 50px 50px 0px;
        font-size: 10px;
        display: none;
    }
    .panel-primary>.panel-heading
    {
        background-color: #33b5e5;
        border-color: #33b5e5;
    }
</style>
<link rel="stylesheet" type="text/css" href="{{asset('web/css/slide_three_radio_button.css')}}">
@endsection @section('bottom-scripts')
<script type="text/javascript">
    var doctor_picture            = "{{$auth_doctor->picture}}";
    var currency                  = "{{$auth_doctor->currency}}";
    var csrf_token                = "{{$csrf_token}}";
    var appointments_template_url = "{{url('doctor/home/template/appointments')}}";
    var get_schedules_by_date_url = "{{url('doctor/settings/my-schedules')}}";
    var today                     = "{{date('Y-m-d')}}";
    var tomorrow                  = "{{(new DateTime('tomorrow'))->format('Y-m-d')}}";
    var set_consult_status_url    = "{{url('doctor/set-consult-status')}}";
    var get_slots_url             = "{{url('doctor/get-slots')}}";
    var get_saved_slots_by_date   = "{{url('doctor/get-saved-slots-by-date')}}";
    var save_slots                = "{{url('doctor/save-slots')}}";
    var incoming_request_polling  = "{{url('doctor/incoming-request')}}";
    var reject_service            = "{{url('doctor/service/reject')}}";
    var accept_service            = "{{url('doctor/service/accept')}}";
    var opentok_generate_session  = "{{url('doctor/opentok/generate/session')}}";
    var incoming_call_sound       = "{{asset('web/audio/Incoming_Call.mp3')}}";
    var openTokKey                = "{{config('opentok.api_key')}}";
    var serviceOnlineCompleted    = "{{url('doctor/service/completed/online')}}";
    var rate_user                 = "{{url('doctor/user/rating')}}"
    var socket_host               = "{{config('SocketIOServer.socket_server_address')}}";
    var socket_port               = "{{config('SocketIOServer.socket_server_port')}}";
    var upload_image_url          = "{{url('doctor/upload/attachment/image')}}";
</script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.2.0/jquery.rateyo.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.2.0/jquery.rateyo.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/1.7.3/socket.io.js"></script>
<script src="https://static.opentok.com/v2/js/opentok.min.js"></script>
<script type="text/javascript" src="{{asset('web/js/doctor_home.js')}}"></script>
@endsection