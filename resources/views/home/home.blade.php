@extends('layouts.home_layout')
@section('content')
<style>
    .medical_category_inner{
       width: 213px !important;
        height: 200px;
        margin: 10px;
    }
    .images-container div {
    flex: none;
    margin:3px 0;
}
@media (min-width: 1200px)
{
.col-lg-2 {
    width: 19.666667%;
}}

</style>



<div class="outer">
			<section class="top-screen">
				<div class="container">
					<div class="tab-content">
						<ul class="nav nav-tabs">
							<li class="active">
								<a href="#consult" id="online-consult" data-toggle="tab">
									<i class="fa fa-laptop" aria-hidden="true"></i>
										Online Consult
								</a>
							</li>
							<li>
								<a href="#book" id="book-appointment" data-toggle="tab">
									<i class="fa fa-calendar" aria-hidden="true"></i>
										Book Appointment
								</a>
							</li>
							<li>
								<a href="#ask" id="question-tab"data-toggle="tab">
									<i class="fa fa-envelope" aria-hidden="true"></i>
										Ask A Question 
								</a>
							</li>
							<li>
								<a href="#health" id="health-store" data-toggle="tab">
									<i class="fa fa-cart-plus" aria-hidden="true"></i>
										Online Store
								</a>
							</li>
						</ul>
						<!--tab-content-->
						<div class="tab-content inner-tab">
							<!--online-consult-->
							<div id="consult" class="tab-pane fade-in active">
								<div class="online-consult" style="color: #000000">
									<h3 class="tagline-one">Search Your Doctors Anytime,Anywhere.</h3>
									<h3 class="tagline-two">Search and make online consultation with your doctor.</h3>
								</div>
								<div class="bottom-grid">
									<div class="col-xs-12 col-sm-12 col-md-12 col-xl-12 paddin0">
										<div class="search-group">
											<div class="search-div col-xs-12 col-sm-5 col-md-4 col-xl-4 paddin0">
												<input type="text" class="form-control" placeholder="Specialities,Doctors,Clinics,Symptoms,etc.," style="border-radius:0px;">
											</div>
											<div class="select-option col-xs-12 col-sm-4 col-md-3 col-xl-3 paddin0">
												<select id="selectCat" class="form-control btn btn-default" style="border-radius:0px;">
													
													@foreach($specialities as $speciality)
                                            		<option value="{{$speciality['id']}}">{{$speciality['speciality']}}</option>
                                            		@endforeach

												</select>
											</div>
											<div class="search-button col-xs-12 col-sm-2 col-md-1 col-xl-1 paddin0">
												
												<button type="submit" onclick="onClick()" id="online_btn" class="btn btn-default search-btn form-control" style="border-radius:0px;">Search</button></a>
											</div>
										</div>
									</div>
								</div>
								
							</div>
							
							<!--book-online-->
							<div id="book" class="tab-pane fade-in ">
								<div class="book-online" style="color: #000000">
									<h3 class="tagline-one">Booking An Appointment Online.</h3>
									<h3 class="tagline-two">Make online appointment with your doctor for consultation.</h3>
								</div>
								<div class="bottom-grid">
									<div class="col-xs-12 col-sm-12 col-md-12 col-xl-12 paddin0">
										<div class="search-group">
											<div class="search-div col-xs-12 col-sm-5 col-md-3 col-xl-3 paddin0">
												<input type="text" class="form-control" placeholder="keywords" style="border-radius:0px;">
											</div>
											<div class="search-div col-xs-12 col-sm-5 col-md-3 col-xl-3 map-col paddin0">
												<input type="text" class="form-control map-input" placeholder="Location" style="border-radius:0px;">
												<i class="fa fa-map-marker map-icon"></i>
											</div>
											<div class="select-option col-xs-12 col-sm-4 col-md-3 col-xl-3 paddin0">
												<select id = "selectCatApp" class="form-control btn btn-default" style="border-radius:0px;">
													
													@foreach($specialities as $speciality)
                                            		<option value="{{$speciality['id']}}">{{$speciality['speciality']}}</option>
                                            		@endforeach

												</select>
											</div>
											<div class="search-button col-xs-12 col-sm-2 col-md-1 col-xl-1 paddin0">
												
												<button onclick="onClickApp()" type="submit" id="book_btn" class="btn btn-default search-btn form-control" style="border-radius:0px;">Search</button></a>
											</div>
										</div>
									</div>
								</div>
								
							</div>
							
							<!--ask-question-->
							<div id="ask" class="tab-pane fade-in">
								<div class="ask-q" style="color: #000000">
									<h3 class="tagline-one">Get Doctor to Answer Your Question.</h3>
									<h3 class="tagline-two">Get health advice from doctor for your health queries.</h3>
								</div>
								<div class="bottom-grid">
									<div class="col-xs-12 col-sm-12 col-md-12 col-xl-12 paddin0">
										<div class="search-group">
											<div class="search-div col-xs-12 col-sm-7 col-md-7 col-xl-7 search-col paddin0">
												<i class="fa fa-search search-icon"></i>
												<input type="text" class="form-control search-icon-include search-input" placeholder="Type your question or keyword here" style="border-radius:0px;">
											</div>
											<div class="search-button col-xs-12 col-sm-2 col-md-1 col-xl-1 paddin0">
												<a href="{{url('/user/settings#/ask-question')}}">
												<button type="submit" id="ask_btn" class="btn btn-default search-btn form-control" style="border-radius:0px;">Search</button></a>
											</div>
										</div>
									</div>
									<a href="{{url('/user/home/articles')}}">
										<button type="button" class="btn btn-default read-btn blue">HEALTH ARTICLES </button></a>
								</div>
								
							</div>
							
							<!--health-store-->
							<div id="health" class="tab-pane fade-in">
								<div class="health-store" style="color: #000000">
									<h3 class="tagline-one">Purchase Health Care Products Online.</h3>
									<h3 class="tagline-two">Shop online to get your healthcare products and services.</h3>
									
								</div>
								<div class="bottom-grid">
									<div class="col-xs-12 col-sm-12 col-md-12 col-xl-12 paddin0">
										<div class="search-group">
											<div class="search-div col-xs-12 col-sm-5 col-md-4 col-xl-4 paddin0">
												<input type="text" class="form-control" placeholder="Keywords" style="border-radius:0px;">
											</div>
											<div class="select-option col-xs-12 col-sm-4 col-md-3 col-xl-3 paddin0">
												<select class="form-control btn btn-default" style="border-radius:0px;">
													@foreach($healthCategories as $healthCategory)
                                            		<option value="">{{$healthCategory->name}}</option>
                                            		@endforeach
												</select>
											</div>
											<div class="search-button col-xs-12 col-sm-2 col-md-1 col-xl-1 paddin0">
												<a href="http://ghealthstore.online">
												<button type="submit" id="health_btn" class="btn btn-default search-btn form-control" style="border-radius:0px;">Search</button></a>
											</div>
										</div>
									</div>
								</div>
								
							</div>
						</div>
					</div>
					
				</div><!--container-->
			</section>
		</div><!--outer-inner-->

		<div class="outer-inner">
			<div id="categories" class="section-two">
        <h2>Medical Categories</h2>
        <div class="categorgy-grid">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 images-container">

                    	@foreach($specialities as $speciality)
                    	<div class="col-lg-2 col-md-2 col-sm-3 col-xm-3">
                    		 <div class="medical_category_inner">
                    		<a href="{{url('user/home/categories/'.$speciality['id'])}}">
                    		<div class="img_wrap">
                    			<img src="{{$speciality['picture']}}" width=
                    			"200" height="200" alt>
                    			<div class="img_text">
                    			
                    				<p>{{$speciality['speciality']}}</p>
                    			</div>
                    		</div>
                    		</a>
                    		</div>
                    	</div>
                    	@endforeach
                    </div>
                </div>
            </div>
        </div>
       <!--  <div class="view-more">
            <a href="#" target="_blank">Go to the List <i class="fa fa-angle-right"></i><i class="fa fa-angle-right"></i></a>
        </div> -->
    </div>
			

			<div id="articles" class="health-article">
        <div class="section-three">
            <!-- <h2>Health Article</h2> -->
            <div class="scroll-image">
                <div id="myCarousel" class="carousel slide container slider-color" data-ride="carousel">
                    <h2>Health Article</h2>
                    <div class="carousel-inner paddin-lr col-lg-12 col-md-12 col-sm-12" role="listbox">
                        <div class="item active">
                            <div class="col-lg-12 col-xs-12">
                                <div class="row top-bottom">
                                    <div class="col-lg-3 col-sm-6 col-xs-12">

                                        <a href="{{route('showArticle',$articles[0]['id'])}}">

                                        <div class="text-center">
                                        <img src="{{$articles[0]['picture']}}" alt="Chania" class="img-responsive">
                                        </div>
                                        </a>
                                    </div>
                                    <?php
                                        $string = urldecode($articles[0]['content']);
                                        if (strlen($string) > 50) {

                                        // truncate string
                                            $stringCut = substr($string, 0, 600);

                                        // make sure it ends in a word so assassinate doesn't become ass...
                                        $string = substr($stringCut, 0, strrpos($stringCut, ' ')).'........'; 
                                        }
                                     ?>
                                    <div class="col-lg-8 col-sm-6 col-xs-12">
                                        <div class="article_outer">
                                        <p class="lead" style="margin-bottom:5px;">{{$articles[0]['heading']}}</p>
                                        <p><?php echo $string; ?><a class="blue" href="{{route('showArticle',$articles[0]['id'])}}">
                                        Read More
                                        </a>
                                        </p>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            
                        </div>

                        

                        @foreach(array_slice($articles,1) as $article)

                        <div class="item">
                            <div class="col-lg-12 col-xs-12">
                                <div class="row top-bottom">
                                    <div class="col-lg-3 col-sm-6 col-xs-12">

                                        <a href="{{route('showArticle',$article['id'])}}">

                                        <div class="text-center">
                                        	
                                             <img src="{{$article['picture']}}" alt="Chania" class="img-responsive" width="200" height="200" /></a>
                                        </div>
                                       
                                    </div>
                                    <?php
                                        $string = urldecode($article['content']);
                                        if (strlen($string) > 50) {

                                        // truncate string
                                            $stringCut = substr($string, 0, 600);

                                        // make sure it ends in a word so assassinate doesn't become ass...
                                        $string = substr($stringCut, 0, strrpos($stringCut, ' ')).'........'; 
                                        }
                                     ?>
                                    <div class="col-lg-8 col-sm-6 col-xs-12">
                                        <div class="article_outer"> 
                                        	<p class="lead" style="margin-bottom:5px;">{{$article['heading']}}</p>
                                            <p><?php echo $string; ?><a class="blue" href="{{route('showArticle',$article['id'])}}">
                                                Read More
                                            </a>
                                        </p>
                                        
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        @endforeach
                        
                    </div>
                    <!-- Left and right controls -->
                    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
			
			<div class="play-store">
				<div class="container">
					<div class="col-md-8">
						<div class="play-store-inner">
							<h1>Stay with Us, Stay Healthy! </h1>
							<p>Download our mobile app and install.<br/>
							It's free </p>
							<div class="store-btn">
								<a href="https://itunes.apple.com/us/app/ghealth-user/id1229358810?ls=1&mt=8"><button type="button" href="" class="btn btn-default btn-lg"><i class="fa fa-apple"></i> App Store </button></a>
								<a href="https://play.google.com/store/apps/details?id=com.patient.ghealth"><button type="button" class="btn btn-default btn-lg"><i class="fa fa-play"></i> Google Play </button></a>
							</div>
						</div>
					</div> 
					<div class="col-md-4">
						<img src="/web/images/gHealth_App_1.jpg" class="img-responsive" alt="" />
					</div>
				</div>
			</div>
		</div>
		<button onclick="topFunction()" id="myBtn" title="Go to top">Top</button>




@endsection

<script type="text/javascript">
    function onClick()
    {
        var e = document.getElementById("selectCat");
        var value = e.options[e.selectedIndex].value;
        window.location="{{url('/user/home/categories/')}}/"+value+"";
    }

    function onClickApp()
    {
        var e = document.getElementById("selectCatApp");
        var value = e.options[e.selectedIndex].value;
        window.location="{{url('/user/home/categories/')}}/"+value+"#/book_appointment";
    }

    
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        document.getElementById("myBtn").style.display = "block";
    } else {
        document.getElementById("myBtn").style.display = "none";
    }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
    document.body.scrollTop = 0; // For Chrome, Safari and Opera 
    document.documentElement.scrollTop = 0; // For IE and Firefox
}
</script>