<!DOCTYPE html>
<html lang="en">
	<head>
		<!--meta-->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Login on gHealth</title>
		<link rel="shortcut icon" type="image/png" href="{{asset('web/images/fav_16_16.png')}}"/>
		
		<!-- css -->
		<link href="{{asset('web/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
		<link href="{{asset('web/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
		<link href="{{asset('web/css/style.min.css')}}" rel="stylesheet" type="text/css">
		<link href="{{asset('web/css/bootstrapValidator.css')}}" rel="stylesheet" type="text/css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" rel="stylesheet" type="text/css">
		<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
		<!--google-fonts-->
		<link href='https://fonts.googleapis.com/css?family=Muli' rel='stylesheet' type='text/css'>
		
		<!--internal css-->
	    <style>
	    	.loader_msg_outer
            {
                position: fixed;
                width: 100%;
                height: 100%;
                top: 0px;
                left: 0px;
                background: rgba(0, 0, 0, 0.6);
                font-family: cursive;
                z-index: 999;
                display: flex;
                text-align: center;
                display: none;
            }

            .loader_msg_outer > .loader_msg_inner
            {
                display: inline-block;
                background: white;
                padding: 10px;
                border-radius: 5px;
                position: relative;
                margin: auto;
            }
            .navbar-default .navbar-brand img
            {
            	width: 210px;
    			transform: translateY(-17px);
            }
            .navbar-default 
            {
            	background: transparent;
            }
		</style>
	</head>
	
	<body>
		<div class="loader_msg_outer">
            <div class="loader_msg_inner">
                <img src="{{asset('web/images/loader.gif')}}">
                <span class="loader_msg_text"></span>
            </div>
        </div>
		<!--outer-begins-->
		<div class="outer">
			<div class="container">
				<nav class="navbar navbar-default">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="/">
							<img src="/web/images/ghealthlogo.png" syt>
						</a>
					</div>
					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
						<ul class="nav navbar-nav navbar-right">
							<li>
								<a href="{{url('user/register')}}">
									<strong>Register as User</strong>
								</a>
							</li>
							<li>
								<a href="{{url('doctor/register')}}">
									<strong>Register as Doctor</strong>
								</a>
							</li>
						</ul>
					</div>
				</nav>
			</div><!--container-->
			
			<!--login-content-->
			@yield('content')
			
		</div>
		<script type="text/javascript">
			function showLoaderMessage(show, message = "")
            {
                if(show) {

                    $(".loader_msg_outer").css('display', 'flex');
                    $(".loader_msg_outer").find(".loader_msg_text").html(message);

                } else {
                    setTimeout(function(){ 
                        $(".loader_msg_outer").hide();
                    }, 300);
                }
            }
		</script>
	</body>
</html>
		