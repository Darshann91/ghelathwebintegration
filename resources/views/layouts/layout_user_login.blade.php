<!DOCTYPE html>
<html lang="en">
	<head>
		<!--meta-->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Login on gHealth</title>
		<link rel="shortcut icon" type="image/png" href="{{asset('web/images/fav_16_16.png')}}"/>
		
		<!-- css -->
		<link href="{{asset('web/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
		<link href="{{asset('web/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
		<link href="{{asset('web/css/style.min.css')}}" rel="stylesheet" type="text/css">
		<link href="{{asset('web/css/bootstrapValidator.css')" rel="stylesheet" type="text/css">
		
		<!--google-fonts-->
		<link href='https://fonts.googleapis.com/css?family=Muli' rel='stylesheet' type='text/css'>
		
		<!--internal css-->
	    <style>
			
		</style>
	</head>
	
	<body>
		<!--outer-begins-->
		<div class="outer">
			<div class="container">
				<nav class="navbar navbar-default">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="/">
							<strong>gHealth.com</strong>
						</a>
					</div>
					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
						<ul class="nav navbar-nav navbar-right">
							<li>
								<a href="signin">
									<strong>Register</strong>
								</a>
							</li>
						</ul>
					</div>
				</nav>
			</div><!--container-->
			
			<!--login-content-->
			@yield('content')
			
		</div>
		<!--outer -ends-->	
	</body>
</html>
		