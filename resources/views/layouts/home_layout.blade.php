<!DOCTYPE html>
<html lang="en">
	<head>
		<!--meta-->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>gHealth</title>
		<link rel="shortcut icon" type="image/png" href="{{asset('web/images/fav_16_16.png')}}"/>
		
		<!-- css -->
		<link href="web/css/bootstrap.min.css" rel="stylesheet" type="text/css">
		<link href="web/css/font-awesome.min.css" rel="stylesheet" type="text/css">
		<link href="web/css/index.min.css" rel="stylesheet" type="text/css">
		<link href="web/css/listgrid.css" rel="stylesheet" type="text/css">

		<script src="web/js/lightbox-plus-jquery.min.js"></script>

		<!-- for rating -->
		<link rel="stylesheet" href="web/css/rateit.css">
		<script src="web/js/jquery.rateit.js"></script>

		<!-- For Ranger -->
		<link rel="stylesheet" href="web/css/bootstrap-slider.css">
		
		<!--google-fonts-->
		<!-- <link href='https://fonts.googleapis.com/css?family=Muli' rel='stylesheet' type='text/css'> -->
		<!-- <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css"> -->
		
		<link href="https://fonts.googleapis.com/css?family=Muli:400,700" rel="stylesheet">
		<!--internal css-->
	    <style type= "text/css">
	    	.outer 
        	{
        		transition : background-image 0.5s;
        	}
        	.images-container { 
			  padding: .5vw;
			  font-size: 0;
			  display: -ms-flexbox;
			  -ms-flex-wrap: wrap;
			  -ms-flex-direction: column;
			  -webkit-flex-flow: row wrap; 
			  flex-flow: row wrap; 
			  display: -webkit-box;
			  display: flex;
			}
			.images-container div { 
			  -webkit-box-flex: auto;
			  -ms-flex: auto;
			 flex: auto;
			    /* width: 200px; */
			    margin: 3px;
			    padding: 0px;
			    height: auto;
			    /* border-radius: 5px; */
			    overflow: hidden;
			}
			.images-container div img { 
			  width: 100%;
			  height: 200px; 
			}
			@media screen and (max-width: 400px) {
			  .images-container div { margin: 0; }
			  .images-container { padding: 0; }
			  
			}

			.img_wrap
			{
				position: relative;
			}
			.img_wrap .img_text
			{
				position: absolute;
				width:100%;
				height:100%;
				opacity: 0;
				left: 0;
				right: 0;
				bottom: 0;
				top: 0;
				background: rgba(0,0,0,0.4);
				transition: all 1s ease;
				margin:0px;
				padding:0px;
			}
			.img_wrap .img_text p
			{
				 position: absolute;
			    left: 50%;
			    width: 100%;
			    height: 100%;
			    top: 50%;
			    transform: translate(-50%, -13%);
			    color: #fff;
			    font-size: 20px;
			    text-align: center;
			    font-weight: 700;
			}
			.img_wrap:hover .img_text
			{
				opacity: 1;
			}

			#myBtn {
    			display: none; /* Hidden by default */
    			position: fixed; /* Fixed/sticky position */
    			bottom: 20px; /* Place the button at the bottom of the page */
    			right: 30px; /* Place the button 30px from the right */
    			z-index: 99; /* Make sure it does not overlap */
    			border: none; /* Remove borders */
    			outline: none; /* Remove outline */
    			background-color: #95cae4; /* Set a background color */
    			color: white; /* Text color */
    			cursor: pointer; /* Add a mouse pointer on hover */
    			padding: 15px; /* Some padding */
    			border-radius: 10px; /* Rounded corners */
			}

			#myBtn:hover {
    			background-color: #555; /* Add a dark-grey background on hover */
			}
			.navbar .navbar-brand img
			{
				width: 210px;
   				transform: translatey(-18px);
			}
			
		</style>
		<script>
			$(document).ready(function() {
			    $('#list').click(function(event){event.preventDefault();$('#products .item').addClass('list-group-item');});
			    $('#grid').click(function(event){event.preventDefault();$('#products .item').removeClass('list-group-item');$('#products .item').addClass('grid-group-item');});
			});
		</script>
	</head>
	
	<body>
		<!--outer-begins-->
		
		
		<nav class="navbar navbar-default">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="/">
						<img src="/web/images/ghealthlogo.png">
					</a>
				</div>
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav navbar-right">
						<li>
							<a href="{{url('login')}}">
								<strong>Sign In</strong>
							</a>
						</li>
						<li>
							<a href="{{url('user/register')}}">
								<strong>Register as User</strong>
							</a>
						</li>
						<li>
							<a href="{{url('doctor/register')}}">
								<strong>Register as Doctor</strong>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</nav>
			


		<!-- YOUR CODE GOES HERE!!! -->
		@yield('content')



		<!-- <div class="footer">
			<div class="section-three">
				<div class="container">
					<div class="row">
						<div class="footer-bottom1"> -->
							<!-- <div class="col-sm-12"> -->
								<!-- <div class="col-sm-3">
									<h5 class="text-uppercase footer-header">
										<strong>for patient</strong>
									</h5>
									<ul>
										<li>
											<a href="#">Search for Clinics</a></li>
										<li>
											<a href="#">Search for Doctors</a></li>
										<li>
											<a href="#">Online Appointment</a></li>
										<li>
											<a href="#">Health Stores</a></li>
										<li>
											<a href="#">Lifetime Health Record</a></li>
									</ul>
								</div>
								<div class="col-sm-3 ">
									<h5 class="text-uppercase footer-header">
										<strong>for doctor</strong>
									</h5>
									<ul>
										<li>
											<a href="#">Online Consultation</a></li>
										<li>
											<a href="#">Appointments</a></li>
										<li>
											<a href="#">Health Articles</a></li>
										<li>
											<a href="#">Doctor Profile	</a></li>
										<li>
											<a href="#">Patient Visit Record</a></li>
									</ul>
								</div>
								<div class="col-sm-2">
									<h5 class="text-lowecase footer-header">
										<strong>gHealth</strong>
									</h5>
									<ul>
										<li>
											<a href="#">About Us</a></li>
										<li>
											<a href="#">Find Your Doctor</a></li>
										<li>
											<a href="#">Stages</a></li>
										<li>
											<a href="#">Features</a></li>
										<li>
											<a href="#">Contact Us</a></li>
									</ul>
								</div>
								<div class="col-sm-4">
									<h5 class="text-uppercase footer-header">
										<strong>more</strong>
									</h5>
									<ul>
										<li>
											<a href="#">Help</a></li>
										<li>
											<a href="#">Privacy and Policy</a></li>
										<li>
											<a href="#">Terms and Conditions</a></li>
										<li></li>
										<li></li>
										<li>
											<div class="country_outer">
												<span class="small-font">
													<span>
														<img src="https://img.freeflagicons.com/thumb/round_icon/malaysia/malaysia_640.png" width="27" />MALAYSIA |
													</span>
													<span>
														English (US) |
													</span>
													<span>
														RM(MYR) |
													</span>
													<span class="icon_span">
														<i class="glyphicon glyphicon-menu-up"></i>
													</span>
												</span>
											</div>
										</li>
									</ul>
							
								</div> -->
							<!-- </div> -->
<!-- 						</div>
					</div>
				</div>
			</div>
		</div> -->
		<div class="footer-second">
			<div class="container">
				<div class="row">
					<div class="footer-bottom2">
						<div class="col-sm-5">
							<h4>Toll Free Number : 1 300 88 6008</h4>
							<h5><strong>gHealth.com </strong>2015-2020 @ All Rights Reserved.</h5>
						</div>

						<div class="col-sm-3">
							<h4>
								<i class="fa fa-globe" aria-hidden="true"></i>
									www.ghealth.com
							</h4>
						</div>

						<div class="col-sm-4 text-center">							
							
							<a href="https://www.facebook.com/Ghealthcom-420340984988107/"><i class="fa fa-facebook-square" aria-hidden="true"></i></a>
							<a href="https://plus.google.com/u/1/114922129084557930193"><i class="fa fa-google-plus-square" aria-hidden="true"></i></a>
							<a href="https://twitter.com/gHealthcom"><i class="fa fa-twitter-square" aria-hidden="true"></i></a>
							

						</div>
					</div>
				</div>
			</div>
		</div>
		

	<!--js-->
	<script src="web/js/jquery.min.js"></script>
    <script src="web/js/bootstrap.min.js"></script>
	<script>
	$('#book-appointment').click(function(){
		$('.outer').css('background-image', 'url(web/images/bg/Keyboard.jpg)');		
	});
	$('#question-tab').click(function(){
		$('.outer').css('background-image','url(web/images/bg/chairs.jpg)');
	});	
	$('#health-store').click(function(){
		$('.outer').css('background-image','url(web/images/bg/health_store.jpg)');
	});
	$('#online-consult').click(function(){
		$('.outer').css('background-image','url(web/images/bg/doctor.jpg)');
	});
	</script>

	<!--js-->

    
	</body>
</html>