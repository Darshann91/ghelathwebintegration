@extends('admin.layout')
@section('content')

    <div class="title-search-block">
        <div class="title-block">
            <div class="row">
                <div class="col-md-6">
                    <h3 class="title">
                        Clinics
                        <a href="{{route('addClinic')}}" class="btn btn-primary btn-sm rounded-s">
                            Add New
                        </a><!--
				 --><div class="action dropdown">
                            <button class="btn  btn-sm rounded-s btn-secondary dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                More actions...
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                <a class="dropdown-item" href="#"><i class="fa fa-pencil-square-o icon"></i>Mark as a draft</a>
                                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#confirm-modal"><i class="fa fa-close icon"></i>Delete</a>
                            </div>
                        </div>
                    </h3>

                </div>
            </div>
        </div>
        <div class="items-search">
            <form class="form-inline">
                <div class="input-group"> <input type="text" class="form-control boxed rounded-s" placeholder="Search for..."> <span class="input-group-btn">
					<button class="btn btn-secondary rounded-s" type="button">
						<i class="fa fa-search"></i>
					</button>
				</span> </div>
            </form>
        </div>
    </div>
    <div class="card items">
        <section class="example">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Photo</th>
                        <th>#</th>
                        <th>Clinic Name</th>
                        <th>Clinic Address</th>
                        <th>Clinic Phone</th>
                        <th>Clinic Fax</th>
                        <th>Actions</th>
                    </tr>
                    </thead>

                    <?php foreach($clinics as $clinic){ ?>

                    <tbody>
                    <tr>
                        <td><img src={{$clinic->c_image1}} width="30"></td>
                        <td>{{$clinic->id}}</td>
                        <td>{{$clinic->clinic_name}}</td>
                        <td>{{$clinic->c_address}}</td>
                        <td>{{$clinic->c_phone}}</td>
                        <td>{{$clinic->c_fax}}</td>
                        <td><div class="action dropdown">
                                <button class="btn  btn-sm rounded-s btn-secondary dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Actions
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                    <a class="dropdown-item"  href="{{route('editClinic',$clinic->id)}}">Edit</a>
                                    <a class="dropdown-item"  onclick="deleteFunction()" href="#">Delete</a>
                                </div>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                    <script>
                        function deleteFunction(){
                            if(confirm("Are you sure ?") == true){
                                document.location.href = "{{route('clinicDelete',$clinic->id)}}";
                            }
                        }
                    </script>
                    <?php } ?>
                </table>
            </div>
        </section>
    </div>


    {{--   <nav class="text-xs-right">
          <ul class="pagination">
              <li class="page-item"> <a class="page-link" href="">
  Prev
</a> </li>
              <li class="page-item active"> <a class="page-link" href="">
  1
</a> </li>
              <li class="page-item"> <a class="page-link" href="">
  2
</a> </li>
              <li class="page-item"> <a class="page-link" href="">
  3
</a> </li>
              <li class="page-item"> <a class="page-link" href="">
  4
</a> </li>
              <li class="page-item"> <a class="page-link" href="">
  5
</a> </li>
              <li class="page-item"> <a class="page-link" href="">
  Next
</a> </li>
          </ul>
      </nav> --}}

    {{$clinics->render()}}
@stop
