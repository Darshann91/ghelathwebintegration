@extends('admin.layout')
@section('content')



 
    <div class="title-block">
    <h3 class="title">
        Consults
    </h3>
                        <p class="title-description"> List of all the consults </p>
                    </div>
                    
                    <section class="section">
                       
                       <div class="card items">
                        <ul class="item-list striped">
                            <li class="item item-list-header hidden-sm-down">
                                <div class="item-row">
                                  
                                    
                                    <div class="item-col item-col-header item-col-title">
                                        <div> <span>Id</span> </div>
                                    </div>
                                    <div class="item-col item-col-header item-col-title">
                                        <div> <span>User Name</span> </div>
                                    </div>
                                    <div class="item-col item-col-header item-col-title">
                                        <div> <span>Doctor Name</span> </div>
                                    </div>
                                    <div class="item-col item-col-header item-col-title">
                                        <div> <span>Consult Type</span> </div>
                                    </div>
                                    <div class="item-col item-col-header item-col-title">
                                        <div> <span>Date</span> </div>
                                    </div>
                                    <div class="item-col item-col-header item-col-title">
                                        <div> <span>Start Time</span> </div>
                                    </div>
                                    <div class="item-col item-col-header item-col-title">
                                        <div> <span>End Time</span> </div>
                                    </div>
                                    <div class="item-col item-col-header item-col-title">
                                        <div> <span>Total</span> </div>
                                    </div>
                                    <div class="item-col item-col-header item-col-title">
                                        <div> <span>Status</span> </div>
                                    </div>
                                    <div class="item-col item-col-header item-col-title">
                                        <div> <span>Payment Mode</span> </div>
                                    </div>
                                    <div class="item-col item-col-header fixed item-col-actions-dropdown"> </div>
                                </div>
                            </li>

                            
                                
                            <?php foreach($consults as $consult){ ?>
                            <li class="item">
                                <div class="item-row">
                           
                                                                      
                                    <div class="item-col item-col-sales">
                                        <div> {{$consult->request_id}}</div>
                                    </div>

                                     <div class="item-col item-col-sales">
                                        <div>{{$consult->user_first_name." ".$consult->user_last_name}} </div>
                                    </div>

                                     <div class="item-col item-col-sales">
                                        <div> {{$consult->doctor_first_name." ".$consult->doctor_last_name}} </div>
                                    </div>
                                    <?php if($consult->request_type==1){
                                        $consult_type = 'Chat';
                                    }elseif($consult->request_type==2){
                                        $consult_type = 'Phone';
                                    }elseif($consult->request_type==3){
                                        $consult_type = 'Video';
                                    }elseif($consult->request_type==4){
                                        $consult_type = 'Ondemand';
                                    }elseif($consult->request_type==5){
                                        $consult_type = 'Booking';
                                    }else{
                                        $consult_type = 'NA';
                                    } ?>

                                     <div class="item-col item-col-sales">
                                        <div> {{$consult_type}} </div>
                                    </div>

                                    <div class="item-col item-col-sales">
                                        <div> Date </div>
                                    </div>

                                    <div class="item-col item-col-sales">
                                        <div> {{$consult->start_time}} </div>
                                    </div>

                                    <div class="item-col item-col-sales">
                                        <div> {{$consult->end_time}} </div>
                                    </div>

                                    <div class="item-col item-col-sales">
                                        <div> {{$consult->amount}} </div>
                                    </div>
                                    <?php if($consult->status == 0){
                                        $status = 'New Request';
                                    }elseif($consult->status == 1){
                                        $status = 'Request Waiting';
                                    }elseif($consult->status == 2){
                                        $status = 'In Progress';
                                    }elseif($consult->status == 3){
                                        $status = 'Pending for completion ';
                                    }elseif($consult->status == 4){
                                        $status = 'Rating pending';
                                    }elseif($consult->status == 5){
                                        $status = 'Completed';
                                    }elseif($consult->status == 6){
                                        $status = 'Cancelled';
                                    }elseif($consult->status == 7){
                                        $status = 'No Doctor';
                                    }elseif($consult->status == 8){
                                        $status = 'COD confirm wait';
                                    }else{
                                        $status = 'NA';
                                    } ?>

                                    <div class="item-col item-col-sales">
                                        <div> {{$status}} </div>
                                    </div>

                                    <div class="item-col item-col-sales">
                                        <div> {{$consult->payment_mode}} </div>
                                    </div>
                                   
                                  
                                    <div class="item-col fixed item-col-actions-dropdown">
                                        <div class="item-actions-dropdown">
                                            <a class="item-actions-toggle-btn"> <span class="inactive">
                                    <i class="fa fa-cog"></i>
                                </span> <span class="active">
                                <i class="fa fa-chevron-circle-right"></i>
                                </span> </a>
                                            <div class="item-actions-block">
                                                <ul class="item-actions-list">
                                                    <li>
                                                        <a class="edit" href="{{route('doctorPayment',$consult->request_id)}}" > <i class="fa fa-money "></i> </a>
                                                    </li>
                                                    <li>
                                                        <a class="edit" href="#"> <i class="fa fa-pencil"></i> </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                         <?php } ?>
                    

                        </ul>
                    </div>
                    </section>
 


@stop