@extends('admin.layout')
@section('content')

<div class="title-block">
<h3 class="title"> Doctor Fee Details </h3>
</div>

<div class="card items">
                        <ul class="item-list striped">
                            <li class="item item-list-header hidden-sm-down">
                                <div class="item-row">
                                    <div class="item-col fixed item-col-check"> <label class="item-check" id="select-all-items">
						<input type="checkbox" class="checkbox">
						<span></span>
					</label> </div>
                                    
                                    <div class="item-col item-col-header item-col-title">
                                        <div> <span>Name</span> </div>
                                    </div>
                                    <div class="item-col item-col-header item-col-title">
                                        <div> <span>Chat Fee</span> </div>
                                    </div>
                                    <div class="item-col item-col-header item-col-title">
                                        <div> <span>Phone Fee</span> </div>
                                    </div>
                                    <div class="item-col item-col-header item-col-title">
                                        <div> <span>Video Fee</span> </div>
                                    </div>
                                    <div class="item-col item-col-header item-col-title">
                                        <div> <span>Ondemand Fee</span> </div>
                                    </div>
                                    <div class="item-col item-col-header fixed item-col-actions-dropdown"> </div>
                                </div>
                            </li>

                            
                                
                            
                            <li class="item">
                                <div class="item-row">
                                    <div class="item-col fixed item-col-check"> <label class="item-check" id="select-all-items">
							<input type="checkbox" class="checkbox">
							<span></span>
						</label> </div>
                                                                      
                                    <div class="item-col item-col-sales">
                                        <div> {{$doctor->first_name." ".$doctor->last_name}}</div>
                                    </div>

                                     <div class="item-col item-col-sales">
                                        <div> {{$doctor_fee->chat_fee}} </div>
                                    </div>

                                     <div class="item-col item-col-sales">
                                        <div> {{$doctor_fee->phone_fee}} </div>
                                    </div>

                                     <div class="item-col item-col-sales">
                                        <div> {{$doctor_fee->video_fee}} </div>
                                    </div>

                                    <div class="item-col item-col-sales">
                                        <div> {{$doctor_fee->ondemand_fee}} </div>
                                    </div>
                                   
                                  
                                    <div class="item-col fixed item-col-actions-dropdown">
                                        <div class="item-actions-dropdown">
                                            <a class="item-actions-toggle-btn"> <span class="inactive">
									<i class="fa fa-cog"></i>
								</span> <span class="active">
								<i class="fa fa-chevron-circle-right"></i>
								</span> </a>
                                            <div class="item-actions-block">
                                                <ul class="item-actions-list">
                                                    <li>
                                                        <a class="edit" href="{{route('doctorFee',$doctor->id)}}" > <i class="fa fa-money "></i> </a>
                                                    </li>
                                                    <li>
                                                        <a class="edit" href="item-editor.html"> <i class="fa fa-pencil"></i> </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                         
                    

                        </ul>
                    </div>



@stop