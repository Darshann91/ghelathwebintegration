@extends('admin.layout')
@section('content')



    <div class="card card-block sameheight-item">
        <div class="title-block">
            <h3 class="title">
                Add New Speciality
            </h3> </div>
        <form action="{{route('specialityEditSubmit')}}" method="post" role="form" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="<?php echo Session::token(); ?>">
            <input type="hidden" name="speciality_id" value="<?php echo $speciality->id; ?>">
            <div class="form-group">
                <label class="control-label">Speciality Name English (en)</label> <input type="text" name="s_name" value="{{$speciality->speciality}}" class="form-control underlined" required>

                <label class="control-label">Speciality Name Chinese (zh)</label> <input type="text" name="s_name_chinese" value="{{$speciality->speciality_chinese}}" class="form-control underlined" required>

                <label class="control-label">Speciality Name Thai (th)</label> <input type="text" name="s_name_thai" value="{{$speciality->speciality_thai}}" class="form-control underlined" required>

                <label class="control-label">Speciality Name Bahasa Indonesia (in)</label> <input type="text" name="s_name_indonesian" value="{{$speciality->speciality_indonesian}}" class="form-control underlined" required> 

                <label class="control-label">Speciality Name Burmese (my)</label> <input type="text" name="s_name_myanmar" value="{{$speciality->speciality_myanmar}}" class="form-control underlined" required> 

                 <label class="control-label">Speciality Name Farsi (fa)</label> <input type="text" name="s_name_iran" value="{{$speciality->speciality_iran}}" class="form-control underlined" required> 

                  <label class="control-label">Speciality Name Vietnamese (vi)</label> <input type="text" name="s_name_vietnamese" value="{{$speciality->speciality_vietnamese}}" class="form-control underlined" required> 

                   <label class="control-label">Speciality Name Japanese (ja)</label> <input type="text" name="s_name_japan" value="{{$speciality->speciality_japan}}" class="form-control underlined" required> 
                </div>

            <div class="form-group">
                <label class="control-label">Upload Picture</label> <img src={{$speciality->picture}} width="60"> <input type="file" name="s_picture" class="form-control underlined" > </div>

            <button type="submit" class="btn btn-block btn-primary">Submit</button>

        </form>
    </div>


@stop