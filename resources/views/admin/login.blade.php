<!doctype html>
<meta name="csrf-token" content="{{ csrf_token() }}" />
<html class="no-js" lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title> G Health Admin </title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->
        <link rel="stylesheet" href="/admins/css/vendor.css">
        <!-- Theme initialization -->
        <script>
            var themeSettings = (localStorage.getItem('themeSettings')) ? JSON.parse(localStorage.getItem('themeSettings')) :
            {};
            var themeName = themeSettings.themeName || '';
            if (themeName)
            {
                document.write('<link rel="stylesheet" id="theme-style" href="/admins/css/app-' + themeName + '.css">');
            }
            else
            {
                document.write('<link rel="stylesheet" id="theme-style" href="/admins/css/app.css">');
            }
        </script>
    </head>

    <body>
        <div class="auth">
            <div class="auth-container">
                <div class="card">
                    <header class="auth-header">
                        <h1 class="auth-title">
        <div class="logo">
        	<span class="l l1"></span>
        	<span class="l l2"></span>
        	<span class="l l3"></span>
        	<span class="l l4"></span>
        	<span class="l l5"></span>
        </div>        G Health Admin 
      </h1> </header>
                    <div class="auth-content">
                        
                        <p class="text-xs-center">LOGIN TO CONTINUE</p>
                        <form id="login-form" action="{{ url('/admin/login') }}" method="POST" novalidate="">
                         <input type="hidden" name="_token" value="<?php echo Session::token(); ?>">
                            <div class="form-group"> <label for="username">Username</label> <input type="text" class="form-control underlined" name="email" id="username" value="{{Request::old('username')}}" placeholder="Your email address" required> </div>
                            <div class="form-group"> <label for="password">Password</label> <input type="password" class="form-control underlined" name="password" id="password" placeholder="Your password" required> </div>
                            <div class="form-group"> <label for="remember">
            <input class="checkbox" id="remember" type="checkbox"> 
            <span>Remember me</span>

          </label> </div>
                            <div class="form-group"> <button type="submit" class="btn btn-block btn-primary">Login</button> </div>
                            {{-- <input type="hidden" name="_token" value="{{Session::token()}}"> --}}
                            {{-- <input type="hidden" name="_token" value="{{{ csrf_token() }}}"> --}}
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- Reference block for JS -->
        <div class="ref" id="ref">
            <div class="color-primary"></div>
            <div class="chart">
                <div class="color-primary"></div>
                <div class="color-secondary"></div>
            </div>
        </div>
        <script src="/admins/js/vendor.js"></script>
        <script src="/admins/js/app.js"></script>
        
</body>
<!--/ END Body -->
</html>
    </body>

</html>