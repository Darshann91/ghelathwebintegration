<?php use App\Doctor; ?>
<?php use App\Clinic; ?>
@extends('admin.layout')
@section('content')

    @if (session()->has('flash_notification.message'))
        <div class="alert alert-{{ session('flash_notification.level') }}">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

            {!! session('flash_notification.message') !!}
        </div>
    @endif

    <div class="title-search-block">
        <div class="title-block">
            <div class="row">
                <div class="col-md-6">
                    <h3 class="title">
                        Time Slots
                        <a href="{{route('editTimeSlot',['slotid'=>0,'specid'=>$speciality_id])}}" class="btn btn-primary btn-sm rounded-s">
                            Add New
                        </a><!--
				 --><div class="action dropdown">
                            <button class="btn  btn-sm rounded-s btn-secondary dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                More actions...
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                <a class="dropdown-item" href="#"><i class="fa fa-pencil-square-o icon"></i>Mark as a draft</a>
                                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#confirm-modal"><i class="fa fa-close icon"></i>Delete</a>
                            </div>
                        </div>
                    </h3>

                </div>
            </div>
        </div>
        <div class="items-search">
            <form class="form-inline">
                <div class="input-group"> <input type="text" class="form-control boxed rounded-s" placeholder="Search for..."> <span class="input-group-btn">
					<button class="btn btn-secondary rounded-s" type="button">
						<i class="fa fa-search"></i>
					</button>
				</span> </div>
            </form>
        </div>
    </div>
    <div class="card items">
        <section class="example">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>From</th>
                        <th>To</th>
                        <th>Type</th>

                    </tr>
                    </thead>

                    <?php foreach($doctorSlots as $doctorSlot){ ?>


                    <tbody>
                    <tr>
                        <td>{{$doctorSlot->id}}</td>
                        <td>{{$doctorSlot->start_time}}</td>
                        <td>{{$doctorSlot->end_time}}</td>
                        <td>{{$doctorSlot->type}}</td>


                        <td><div class="action dropdown">
                                <button class="btn  btn-sm rounded-s btn-secondary dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Actions
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenu1">

                                    <a class="dropdown-item" href="{{route('editTimeSlot',['slotid'=>$doctorSlot->id,'specid'=>$speciality_id])}}" >Edit</a>


                                    {{--<a class="dropdown-item"  href="{{route('viewDocuments',$doctor->id)}}">View Documents</a>--}}
                                </div>
                            </div></td>
                    </tr>
                    </tbody>
                    <?php } ?>
                </table>
            </div>
        </section>
    </div>
    {{--   <nav class="text-xs-right">
          <ul class="pagination">
              <li class="page-item"> <a class="page-link" href="">
  Prev
</a> </li>
              <li class="page-item active"> <a class="page-link" href="">
  1
</a> </li>
              <li class="page-item"> <a class="page-link" href="">
  2
</a> </li>
              <li class="page-item"> <a class="page-link" href="">
  3
</a> </li>
              <li class="page-item"> <a class="page-link" href="">
  4
</a> </li>
              <li class="page-item"> <a class="page-link" href="">
  5
</a> </li>
              <li class="page-item"> <a class="page-link" href="">
  Next
</a> </li>
          </ul>
      </nav> --}}


@stop


