@extends('admin.layout')
@section('content')


    <script>
        var placeSearch, autocomplete;

        function initAutocomplete() {
            // Create the autocomplete object, restricting the search to geographical
            // location types.
            autocomplete = new google.maps.places.SearchBox(
                    (document.getElementById('address')));


            // When the user selects an address from the dropdown, populate the address
            // fields in the form.
            //autocomplete.addListener('place_changed', fillInAddress);
        }

        function geolocate() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function(position) {
                    var geolocation = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                    };
                    var circle = new google.maps.Circle({
                        center: geolocation,
                        radius: position.coords.accuracy
                    });
                    autocomplete.setBounds(circle.getBounds());
                });
            }
        }

        function geocodeAddress() {
            var geocoder = new google.maps.Geocoder();
            var address = document.getElementById("address").value;
           // console.log(address);

            geocoder.geocode( { 'address': address}, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK)
                {
                    // do something with the geocoded result
                    var c_lat = results[0].geometry.location.lat();
                    var c_lng = results[0].geometry.location.lng();
                    console.log(c_lat);
                    console.log(c_lng);

                    document.getElementById('c_lat').value = c_lat;
                    document.getElementById('c_lng').value = c_lng;
                }
            });

        }

    </script>

    <script>


    </script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAfuV5QLffohu3MWWeDmDYfYLNNKg4m9Mc&libraries=places&callback=initAutocomplete"
            async defer></script>

    <div class="card card-block sameheight-item">
        <div class="title-block">
            <h3 class="title">
                Add New Clinic
            </h3> </div>
        <form action="{{route('clinicSubmit')}}" method="post" role="form" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="<?php echo Session::token(); ?>">
            <div class="form-group">
                <label class="control-label">Clinic Name</label> <input type="text" name="clinic_name" class="form-control underlined" placeholder="Enter clinic name" required> </div>

            <div class="form-group">
                <label class="control-label">Clinic Picture 1</label> <input type="file" name="c_image1" class="form-control underlined" > </div>

            <div class="form-group">
                <label class="control-label">Clinic Picture 2</label> <input type="file" name="c_image2" class="form-control underlined" > </div>

            <div class="form-group">
                <label class="control-label">Clinic Picture 3</label> <input type="file" name="c_image3" class="form-control underlined" > </div>

            <div class="form-group">
                <label class="control-label">Clinic Address</label> <input type="text" id="address" name="address" onfocus="geolocate()" onfocusout="geocodeAddress()" class="form-control underlined" required> </div>
            <div class="form-group">
                <label class="control-label">Clinic Phone</label> <input type="text" name="c_phone" class="form-control underlined" required> </div>
            <div class="form-group">
                <label class="control-label">Clinic Fax</label> <input type="text" name="c_fax" class="form-control underlined"> </div>

            <input type="hidden" id="c_lat" name="c_lat">
            <input type="hidden" id="c_lng" name="c_lng">

            <button type="submit" class="btn btn-block btn-primary">Submit</button>

        </form>
    </div>


@stop