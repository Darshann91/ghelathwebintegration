@extends('admin.layout')
@section('content')

    @if (session()->has('flash_notification.message'))
        <div class="alert alert-{{ session('flash_notification.level') }}">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

            {!! session('flash_notification.message') !!}
        </div>
    @endif

                    <div class="title-search-block">
                        <div class="title-block">
                            <div class="row">
                                <div class="col-md-6">
                                    <h3 class="title">
                    Specialities
                    <a href="{{route('addSpeciality')}}" class="btn btn-primary btn-sm rounded-s">
                        Add New
                    </a><!--
                 --><div class="action dropdown">
                        <button class="btn  btn-sm rounded-s btn-secondary dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            More actions...
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenu1">
                            <a class="dropdown-item" href="#"><i class="fa fa-pencil-square-o icon"></i>Mark as a draft</a>
                            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#confirm-modal"><i class="fa fa-close icon"></i>Delete</a>
                        </div>
                    </div>
                </h3>
                                   
                                </div>
                            </div>
                        </div>
                        <div class="items-search">
                            <form class="form-inline">
                                <div class="input-group"> <input type="text" class="form-control boxed rounded-s" placeholder="Search for..."> <span class="input-group-btn">
                    <button class="btn btn-secondary rounded-s" type="button">
                        <i class="fa fa-search"></i>
                    </button>
                </span> </div>
                            </form>
                        </div>
                    </div>
                    <div class="card items">
                        <ul class="item-list striped">
                            <li class="item item-list-header hidden-sm-down">
                                <div class="item-row">
                                    <div class="item-col fixed item-col-check"> <label class="item-check" id="select-all-items">
                        <input type="checkbox" class="checkbox">
                        <span></span>
                    </label> </div>
                                     <div class="item-col item-col-header fixed item-col-img md">
                                        <div> <span>Photo</span> </div>
                                    </div> 
                                    <div class="item-col item-col-header item-col-title">
                                        <div> <span>Speciality</span> </div>
                                    </div>
                                    <div class="item-col item-col-header item-col-title">
                                        <div> <span>Total Doctors</span> </div>
                                    </div> 
                                    {{-- <div class="item-col item-col-header item-col-title">
                                        <div> <span>Description</span> </div>
                                    </div> --}}
                                    <div class="item-col item-col-header fixed item-col-actions-dropdown"> </div>
                                </div>
                            </li>
                            <?php foreach($specialities as $speciality){ ?>
                            <li class="item">
                                <div class="item-row">
                                    <div class="item-col fixed item-col-check"> <label class="item-check" id="select-all-items">
                            <input type="checkbox" class="checkbox">
                            <span></span>
                        </label> </div>
                        
                                   <div class="item-col fixed item-col-img md">
                                        <a href="#">
                                            <div class="item-img rounded" style="background-image: url({{{$speciality->picture}}})"></div>
                                        </a>
                                    </div>
                                  
                                    <div class="item-col item-col-sales">
                                        <div> {{$speciality->speciality}} </div>
                                    </div>

                                      <div class="item-col item-col-sales">
                                        <div> 10 </div>
                                    </div> 

                                     {{-- <div class="item-col item-col-sales">
                                        <div> dentistry  </div>
                                    </div> --}}
                                   
                                  
                                  <div class="item-col fixed item-col-actions-dropdown">
                                        <div class="item-actions-dropdown">
                                            <a class="item-actions-toggle-btn"> <span class="inactive">
                                    <i class="fa fa-cog"></i>
                                </span> <span class="active">
                                <i class="fa fa-chevron-circle-right"></i>
                                </span> </a>
                                            <div class="item-actions-block">
                                                <ul class="item-actions-list">

                                                    <li>
                                                        <a class="edit" href="{{route('addTimeSlots',$speciality->id)}}"> Add /Edit Time Slots </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="item-col fixed item-col-actions-dropdown">
                                        <div class="item-actions-dropdown">
                                            <a class="item-actions-toggle-btn"> <span class="inactive">
                                    <i class="fa fa-pencil"></i>
                                </span> <span class="active">
                                <i class="fa fa-chevron-circle-right"></i>
                                </span> </a>
                                            <div class="item-actions-block">
                                                <ul class="item-actions-list">

                                                    <li>
                                                        <a class="edit" href="{{route('editSpecialities',$speciality->id)}}"> Edit Specialities </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </li>
                            <?php } ?>

                        
                    

                        </ul>
                    </div>
                    <nav class="text-xs-right">
                        <ul class="pagination">
                            <li class="page-item"> <a class="page-link" href="">
                Prev
            </a> </li>
                            <li class="page-item active"> <a class="page-link" href="">
                1
            </a> </li>
                            <li class="page-item"> <a class="page-link" href="">
                2
            </a> </li>
                            <li class="page-item"> <a class="page-link" href="">
                3
            </a> </li>
                            <li class="page-item"> <a class="page-link" href="">
                4
            </a> </li>
                            <li class="page-item"> <a class="page-link" href="">
                5
            </a> </li>
                            <li class="page-item"> <a class="page-link" href="">
                Next
            </a> </li>
                        </ul>
                    </nav>
                

                @stop