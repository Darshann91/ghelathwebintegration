@extends('admin.layout')
@section('content')

		<div class="card card-block sameheight-item">
                                    <div class="title-block">
                                        <h3 class="title">
						Add New Article Category
					</h3> </div>
                                    <form action="{{route('articleSubmit')}}" method="post" role="form" enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="<?php echo Session::token(); ?>">
                                        <div class="form-group"> <label class="control-label">Category Name</label> <input type="text" name="category_name" class="form-control underlined" required> </div>
                                        <div class="form-group"> 
                                        <label class="control-label">Upload Picture</label> <input type="file" name="c_picture" class="form-control underlined" required> </div>
                                       
                                        <button type="submit" class="btn btn-block btn-primary">Submit</button>
                                        
                                    </form>
                                </div>


@stop