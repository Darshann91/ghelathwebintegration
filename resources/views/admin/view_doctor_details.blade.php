@extends('admin.layout')
@section('content')

    <div class="title-search-block">
        <div class="title-block">
            <div class="row">
                <div class="col-md-6">
                    <h3 class="title">
                        Doctor Details

                    </h3>

                </div>
            </div>
        </div>

    </div>
    <div class="col-md-12">
        <div class="card card-block sameheight-item">
            <div class="title-block">
                <div class="item-col item-col-header fixed item-col-img md">
                    <img src={{$doctor->picture}} width="300"  >
                </div>
            </div>

            <form action="{{route('viewDoctorDetailsSubmit')}}" method="post" role="form" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="<?php echo Session::token(); ?>">
                <input type="hidden" name="doctor_id" value="{{$doctor->id}}">
            <div class="title-block">
                <div class="card-items">

                    <div class="form-group"> <label class="control-label">First Name </label> <input type="text" name="first_name" value="{{$doctor->first_name}}" class="form-control underlined"></div>

                </div>

            </div>

            <div class="title-block">
                <div class="card-items">

                    <div class="form-group"> <label class="control-label">Last Name </label> <input type="text" name="last_name" value="{{$doctor->last_name}}" class="form-control underlined"></div>

                </div>

            </div>

                <div class="title-block">
                    <div class="card-items">

                        <div class="form-group"> <label class="control-label">Phone </label> <input type="text" name="phone" value="{{$doctor->phone}}" class="form-control underlined"></div>

                    </div>

                </div>

                <div class="title-block">
                    <div class="card-items">

                        <div class="form-group"> <label class="control-label">Email </label> <input type="text" name="email" value="{{$doctor->email}}" class="form-control underlined"></div>

                    </div>

                </div>

                <div class="title-block">
                    <div class="card-items">

                        <div class="form-group"> <label class="control-label">Date of Birth </label> <input type="text" name="dob" value="{{$doctor->dob}}" class="form-control underlined"></div>

                    </div>

                </div>

                <div class="title-block">
                    <div class="card-items">

                        <div class="form-group"> <label class="control-label">Gender </label> <input type="text" name="gender" value="{{$doctor->gender}}" class="form-control underlined"></div>

                    </div>

                </div>

                <div class="title-block">
                    <div class="card-items">

                        <div class="form-group"> <label class="control-label">Med Number </label> <input type="text" name="med_number" value="{{$doctor->med_number}}" class="form-control underlined"></div>

                    </div>

                </div>

                <div class="title-block">
                    <div class="card-items">
                        <div class="form-group"> <label class="control-label">Speciality </label>
                            <select class="form-control" id="sel1" name="speciality">
                                <option value="{{$current_speciality_id}}" selected="selected"> {{$current_speciality}}</option>
                                <?php foreach ($speciality as $key) { ?>
                                <option value="{{$key->id}}">{{$key->speciality}}</option>
                                <?php } ?>
                            </select>

                    </div>

                </div>
                </div>

                <div class="title-block">
                    <div class="card-items">

                        <div class="form-group"> <label class="control-label">Experience </label> <input type="text" name="experience" value="{{$doctor->experience}}" class="form-control underlined"></div>

                    </div>

                </div>

                <div class="title-block">
                    <div class="card-items">

                        <div class="form-group"> <label class="control-label">Degree 1 </label> <input type="text" name="degree1" value="{{$doctor->degree1}}" class="form-control underlined"></div>

                    </div>

                </div>

                <div class="title-block">
                    <div class="card-items">

                        <div class="form-group"> <label class="control-label">University 1 </label> <input type="text" name="univ1" value="{{$doctor->univ1}}" class="form-control underlined"></div>

                    </div>

                </div>

                <div class="title-block">
                    <div class="card-items">

                        <div class="form-group"> <label class="control-label">Year 1 </label> <input type="text" name="year1" value="{{$doctor->year1}}" class="form-control underlined"></div>

                    </div>

                </div>

                <div class="title-block">
                    <div class="card-items">

                        <div class="form-group"> <label class="control-label">Degree 2 </label> <input type="text" name="degree2" value="{{$doctor->degree2}}" class="form-control underlined"></div>

                    </div>

                </div>

                <div class="title-block">
                    <div class="card-items">

                        <div class="form-group"> <label class="control-label">University 2 </label> <input type="text" name="univ2" value="{{$doctor->univ2}}" class="form-control underlined"></div>

                    </div>

                </div>

                <div class="title-block">
                    <div class="card-items">

                        <div class="form-group"> <label class="control-label">Year 2 </label> <input type="text" name="year2" value="{{$doctor->year2}}" class="form-control underlined"></div>

                    </div>

                </div>

                <div class="title-block">
                    <div class="card-items">

                        <div class="form-group"> <label class="control-label">Nationality </label> <input type="text" name="nationality" value="{{$doctor->nationality}}" class="form-control underlined"></div>

                    </div>

                </div>

                <div class="title-block">
                    <div class="card-items">

                        <div class="form-group"> <label class="control-label">SSN </label> <input type="text" name="ssn" value="{{$doctor->ssn}}" class="form-control underlined"></div>

                    </div>

                </div>

                <div class="title-block">
                    <div class="card-items">

                        <button type="submit" class="btn btn-block btn-primary">Submit</button>

                    </div>

                </div>



           </form>

        </div>
    </div>


@stop