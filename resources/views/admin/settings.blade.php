@extends('admin.layout')
@section('content')



 
    <div class="title-block">
    <h3 class="title">
        Settings
    </h3>
                        <p class="title-description"> Settings for the application </p>
                    </div>
                    
                    <section class="section">
                        <div class="row sameheight-container">
                            <div class="col-md-6">
                                <div class="card card-block sameheight-item">
                                    <div class="title-block">
                                        <h3 class="title">
                        General Settings
                    </h3> </div>
                                    <form action="{{route('saveGeneral')}}" method="post" role="form">
                                        <input type="hidden" name="_token" value="<?php echo Session::token(); ?>">
                                        <div class="form-group"> <label class="control-label">Percentage to Admin</label> <input type="number" name="percentage" min="0" max="100" value="{{$percentage}}" class="form-control underlined"> </div>

                                        <div class="form-group"> <button type="submit" class="btn btn-primary">Submit</button> </div>
                                       
                                        
                                    </form>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="card card-block sameheight-item">
                                    <div class="title-block">
                                        <h3 class="title">
                        Credentials Settings
                    </h3> </div>
                                    <form role="form">
                                        <div class="form-group"> <label class="control-label">GCM </label> <input type="text" class="form-control underlined"> </div>

                                      <div class="form-group"> <button type="submit" class="btn btn-primary">Submit</button> </div>
                                        
                                    </form>
                                </div>
                            </div>
                        </div>
                    </section>
 


@stop