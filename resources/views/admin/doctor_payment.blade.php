@extends('admin.layout')
@section('content')

    <div class="title-search-block">
        <div class="title-block">
            <div class="row">
                <div class="col-md-6">
                    <h3 class="title">
                        Doctor Payment Details

                    </h3>

                </div>
            </div>
        </div>

    </div>
    <div class="col-md-6">
        <div class="card card-block sameheight-item">
            <div class="title-block">
                <div class="item-col item-col-header fixed item-col-img md">
                    <img src={{$doctor->picture}} width="300"  >
                </div>
            </div>

            <form action="{{route('viewDoctorDetailsSubmit')}}" method="post" role="form" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="<?php echo Session::token(); ?>">
                <input type="hidden" name="doctor_id" value="{{$doctor->id}}">
                <div class="title-block">
                    <div class="card-items">

                        <div class="form-group"> <label class="control-label">{{$doctor->first_name." ".$doctor->last_name}} </label> </div>

                    </div>

                </div>


                <div class="title-block">
                    <div class="card-items">

                        <div class="form-group"> <label class="control-label">Phone: {{$doctor->phone}} </label></div>

                    </div>

                </div>

                <div class="title-block">
                    <div class="card-items">

                        <div class="form-group"> <label class="control-label">Email: {{$doctor->email}} </label> </div>

                    </div>

                </div>

                <div class="title-block">
                    <div class="card-items">

                        <div class="form-group"> <label class="control-label">Consult Id: {{$requests->id}} </label> </div>

                    </div>

                </div>

                <?php if($requests->request_type==1){
                    $consult_type = 'Chat';
                }elseif($requests->request_type==2){
                    $consult_type = 'Phone';
                }elseif($requests->request_type==3){
                    $consult_type = 'Video';
                }elseif($requests->request_type==4){
                    $consult_type = 'Ondemand';
                }elseif($requests->request_type==5){
                    $consult_type = 'Booking';
                }else{
                    $consult_type = 'NA';
                } ?>

                <div class="title-block">
                    <div class="card-items">

                        <div class="form-group"> <label class="control-label">Consult Type: {{$consult_type}} </label> </div>

                    </div>

                </div>




            </form>

        </div>
    </div>


    <div class="col-md-6">
        <div class="card card-block sameheight-item">
            <div class="title-block">

            </div>

            <form action="{{route('viewDoctorDetailsSubmit')}}" method="post" role="form" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="<?php echo Session::token(); ?>">
                <input type="hidden" name="doctor_id" value="{{$doctor->id}}">



                <div class="title-block">
                    <div class="card-items">

                        <div class="form-group"> <label class="control-label">Consult Amount: {{$doctor->currency." ".$requests->amount }} </label></div>

                    </div>

                </div>

                <div class="title-block">
                    <div class="card-items">

                        <div class="form-group"> <label class="control-label">Amount to be Paid: {{$doctor->currency." ".$amountToBePaid }} </label></div>

                    </div>

                </div>

                <?php if($requests->is_doctor_paid == 0){
                    $paidStatus = "Not Paid";
                }else{
                    $paidStatus = "Paid";
                }?>
                <div class="title-block">
                    <div class="card-items">

                        <div class="form-group"> <label class="control-label">Status: {{$paidStatus}} </label> </div>

                    </div>

                </div>





                <div class="title-block">
                    <div class="card-items">

                        <a href="{{route("changePaymentStatus",$requests->id)}}" class="btn btn-block btn-primary">Change Status</a>

                    </div>

                </div>



            </form>

        </div>
    </div>


@stop