@extends('admin.layout')
@section('content')

    <div class="card card-block sameheight-item">
        <div class="title-block">
            <h3 class="title">
                Add / Edit Time Slots
            </h3> </div>
        <form action="{{route('timeSlotSubmit')}}" method="post" role="form" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="<?php echo Session::token(); ?>">
            <input type="hidden" name="slotId" value="{{$slotId}}">
            <input type="hidden" name="specialityId" value="{{$speciality_id}}">
            <div class="form-group">
                <label class="control-label">Start Time (Format- 08:30)</label> <input type="text" name="start_time" value="{{$start_time}}" class="form-control underlined" required> </div>

            <div class="form-group">
                <label class="control-label">End Time (Format- 04:45)</label> <input type="text" name="end_time" value="{{$end_time}}" class="form-control underlined" required> </div>

            <div class="form-group">
                <label class="control-label">Type (Format- m,a,e )</label> <input type="text" name="type" value="{{$type}}" class="form-control underlined" required> </div>


            <button type="submit" class="btn btn-block btn-primary">Submit</button>

        </form>
    </div>


@stop