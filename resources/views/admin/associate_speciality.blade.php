@extends('admin.layout')
@section('content')




    <div class="title-block">
        <h3 class="title">
            Settings
        </h3>
        <p class="title-description"> Associate Doctor with speciality </p>
    </div>

    <section class="section">
        <div class="row sameheight-container">
            <div class="col-md-12">
                <div class="card card-block sameheight-item">
                    <div class="title-block">
                        <h3 class="title">
                            Associate
                        </h3> </div>
                    <form action="{{route('associateSpecialitySubmit')}}" method="post" role="form">
                        <input type="hidden" name="_token" value="<?php echo Session::token(); ?>">
                        <input type="hidden" name="doctor_id" value="{{$doctor->id}}">
                        <label>Dr. {{$doctor->first_name." ".$doctor->last_name}}</label>
                        <div class="form-group">
                            <label for="sel1">Select Speciality:</label>
                            <select class="form-control" id="sel1" name="speciality">
                                <option value="{{$current_speciality_id}}" selected="selected"> {{$current_speciality}}</option>
                                <?php foreach ($speciality as $key) { ?>
                                <option value="{{$key->id}}">{{$key->speciality}}</option>
                                <?php } ?>
                            </select>
                        </div>

                        <div class="form-group"> <button type="submit" class="btn btn-primary">Submit</button> </div>


                    </form>
                </div>
            </div>

        </div>
    </section>



@stop