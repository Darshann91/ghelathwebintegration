@extends('admin.layout')
@section('content')

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
  

<script type="text/javascript">
	

	google.charts.load('current', {packages: ['corechart', 'bar']});
google.charts.setOnLoadCallback(drawBasic);

function drawBasic() {

      var data = new google.visualization.DataTable();
      data.addColumn('timeofday', 'Time of Day');
      data.addColumn('number', 'Motivation Level');

      data.addRows([
        [{v: [8, 0, 0], f: '8 am'}, 1],
        [{v: [9, 0, 0], f: '9 am'}, 2],
        [{v: [10, 0, 0], f:'10 am'}, 3],
        [{v: [11, 0, 0], f: '11 am'}, 4],
        [{v: [12, 0, 0], f: '12 pm'}, 5],
        [{v: [13, 0, 0], f: '1 pm'}, 6],
        [{v: [14, 0, 0], f: '2 pm'}, 7],
        [{v: [15, 0, 0], f: '3 pm'}, 8],
        [{v: [16, 0, 0], f: '4 pm'}, 9],
        [{v: [17, 0, 0], f: '5 pm'}, 10],
      ]);

      var options = {
        title: 'Income',
        hAxis: {
          title: 'Months',
          format: '',
          viewWindow: {
            min: [],
            max: []
          }
        },
        vAxis: {
          title: 'Income'
        }
      };

      var chart = new google.visualization.ColumnChart(
        document.getElementById('chart_div'));

      chart.draw(data, options);
    }

</script>



<script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
          ['Chat Consults',     {{$chat}}],
          ['Phone Consults',      {{$phone}}],
          ['Video Consults',  {{$video}}],
          ['Ondemand Consults', {{$ondemand}}],
          ['Bookings',    {{$booking}}]
        ]);

        var options = {
          title: 'Consults ',
          is3D:true,
          colors:['#0A5407','#60C92B','#9a0101','#808080','#024072']
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        chart.draw(data, options);
      }
    </script>

    <div id="piechart" style="width: 900px; height: 500px;"></div>

	<div id="chart_div" style="width: 900px; height: 500px;"></div>



@stop
