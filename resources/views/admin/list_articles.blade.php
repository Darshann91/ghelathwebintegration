<?php use App\Doctor; ?>
<?php use App\Clinic; ?>
@extends('admin.layout')
@section('content')

    @if (session()->has('flash_notification.message'))
        <div class="alert alert-{{ session('flash_notification.level') }}">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

            {!! session('flash_notification.message') !!}
        </div>
    @endif

    <div class="title-search-block">
        <div class="title-block">
            <div class="row">
                <div class="col-md-6">
                    <h3 class="title">
                        Doctors
                        <a href="{{route('addCustomers')}}" class="btn btn-primary btn-sm rounded-s">
                            Add New
                        </a><!--
				 --><div class="action dropdown">
                            <button class="btn  btn-sm rounded-s btn-secondary dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                More actions...
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                <a class="dropdown-item" href="#"><i class="fa fa-pencil-square-o icon"></i>Mark as a draft</a>
                                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#confirm-modal"><i class="fa fa-close icon"></i>Delete</a>
                            </div>
                        </div>
                    </h3>

                </div>
            </div>
        </div>
        <div class="items-search">
            <form class="form-inline">
                <div class="input-group"> <input type="text" class="form-control boxed rounded-s" placeholder="Search for..."> <span class="input-group-btn">
					<button class="btn btn-secondary rounded-s" type="button">
						<i class="fa fa-search"></i>
					</button>
				</span> </div>
            </form>
        </div>
    </div>
    <div class="card items">
        <section class="example">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Photo</th>
                        <th>#</th>
                        <th>Article Heading</th>
                        <th>Description</th>
                        <th>Article Category</th>
                        <th>Article By</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                    </thead>

                    <?php foreach($articles as $article){ ?>
                    <?php
                    if($article->doctor_id){
                        $doctor=Doctor::find($article->doctor_id);
                        $doctor_name = $doctor->first_name." ".$doctor->last_name;

                    }

                    ?>

                    <tbody>
                    <tr>
                        <td><img src={{$article->picture}} width="30"></td>
                        <td>{{$article->id}}</td>
                        <td>{{$article->heading}}</td>
                        <td>{{$article->content}}</td>
                        <td>{{$article->article_category_id}}</td>
                        <td>Dr {{$doctor_name}}</td>
                        <?php if($article->is_approved == 0) { ?>
                        <td><a href="#" class="btn btn-outline-danger" role="button">Not Approved</a></td>
                        <?php } else { ?>
                        <td><a href="#" class="btn btn-outline-success" role="button">Approved</a></td>
                        <?php } ?>

                        <td><div class="action dropdown">
                                <button class="btn  btn-sm rounded-s btn-secondary dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Actions
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                    <?php if($article->is_approved == 0) { ?>
                                    <a class="dropdown-item" href="{{route('approveArticle',$article->id)}}" >Approve</a>
                                    <?php } else { ?>
                                    <a class="dropdown-item" href="{{route('approveArticle',$article->id)}}" >Deactivate</a>
                                    <?php } ?>


                                    {{--<a class="dropdown-item"  href="{{route('viewDocuments',$doctor->id)}}">View Documents</a>--}}
                                </div>
                            </div></td>
                    </tr>
                    </tbody>
                    <?php } ?>
                </table>
            </div>
        </section>
    </div>
    {{--   <nav class="text-xs-right">
          <ul class="pagination">
              <li class="page-item"> <a class="page-link" href="">
  Prev
</a> </li>
              <li class="page-item active"> <a class="page-link" href="">
  1
</a> </li>
              <li class="page-item"> <a class="page-link" href="">
  2
</a> </li>
              <li class="page-item"> <a class="page-link" href="">
  3
</a> </li>
              <li class="page-item"> <a class="page-link" href="">
  4
</a> </li>
              <li class="page-item"> <a class="page-link" href="">
  5
</a> </li>
              <li class="page-item"> <a class="page-link" href="">
  Next
</a> </li>
          </ul>
      </nav> --}}


@stop


