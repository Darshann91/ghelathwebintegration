@extends('admin.layout')
@section('content')




    <div class="title-block">
        <h3 class="title">
            Settings
        </h3>
        <p class="title-description"> Associate Doctor with clinic </p>
    </div>

    <section class="section">
        <div class="row sameheight-container">
            <div class="col-md-12">
                <div class="card card-block sameheight-item">
                    <div class="title-block">
                        <h3 class="title">
                            Associate
                        </h3> </div>
                    <form action="{{route('associateClinicSubmit')}}" method="post" role="form">
                        <input type="hidden" name="_token" value="<?php echo Session::token(); ?>">
                        <input type="hidden" name="doctor_id" value="{{$doctor->id}}">
                        <label>Dr. {{$doctor->first_name." ".$doctor->last_name}}</label>
                        <div class="form-group">
                            <label for="sel1">Select Clinic:</label>
                            <select class="form-control" id="sel1" name="clinic">
                                <?php foreach ($clinics as $clinic) { ?>
                                <option value="{{$clinic->id}}">{{$clinic->clinic_name}}</option>
                                <?php } ?>
                            </select>
                        </div>

                        <div class="form-group"> <button type="submit" class="btn btn-primary">Submit</button> </div>


                    </form>
                </div>
            </div>

        </div>
    </section>



@stop