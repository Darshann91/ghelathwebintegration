@extends('admin.layout')
@section('content')

    <div class="card card-block sameheight-item">
        <div class="title-block">
            <h3 class="title">
                Add New Speciality
            </h3> </div>
        <form action="{{route('addEducationSubmit')}}" method="post" role="form" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="<?php echo Session::token(); ?>">
            <div class="form-group">
                <label class="control-label">Dr. {{$doctor->first_name." ".$doctor->last_name}}</label></br>
                <label class="control-label">{{$doctor->email}}</label>
            </div>

            <div class="form-group">
                <label class="control-label">Degree</label>
                <input type="text" name="degree1" class="form-control underlined" required>
            </div>

            <div class="form-group">
                <label class="control-label">University</label>
            <input type="text" name="univ1" class="form-control underlined" required>
            </div>

            <div class="form-group">
                <label class="control-label">Year</label>
                <input type="text" name="year1" class="form-control underlined" required>
            </div>


            <div class="form-group">
                <label class="control-label">Degree</label>
                <input type="text" name="degree2" class="form-control underlined" required>
            </div>

            <div class="form-group">
                <label class="control-label">University</label>
                <input type="text" name="univ2" class="form-control underlined" required>
            </div>

            <div class="form-group">
                <label class="control-label">Year</label>
                <input type="text" name="year2" class="form-control underlined" required>
            </div>

            <input type="hidden" name="doctor_id" value="{{$doctor->id}}">
            <button type="submit" class="btn btn-block btn-primary">Submit</button>

        </form>
    </div>


@stop