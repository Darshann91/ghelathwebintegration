<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Doctor extends Authenticatable
{
    protected $table = 'doctors';

    public function getRememberToken()
    {
        return null; // not supported
    }

    public function setRememberToken($value)
    {
        // not supported
    }

    //Relationship with other models
    public function requests()
    {
    	return $this->hasMany('App\Requests');
    }

    public function doctor_issues()
    {
    	return $this->hasMany('App\UserIssue');
    }

    public function user_ratings()
    {
    	return $this->hasMany('App\UserRating');
    }

    public function doctor_ratings()
    {
    	return $this->hasMany('App\DoctorRating');
    }

    public function articles()
    {
    	return $this->hasMany('App\Article');
    }

    public function available_slots()
    {
    	return $this->hasMany('App\AvailableSlot');
    }

    public function doctor_consults()
    {
    	return $this->hasOne('App\DoctorConsult');
    }

    public function doctor_fee()
    {
    	return $this->hasOne('App\DoctorFee');
    }



}
