<?php

namespace App\Repositories;

use App\Helpers\Helper;
use Twilio\Rest\Client;
use Services_Twilio;
use App\Otp;

class OTPRepository 
{

	protected static function twilioSid()
	{
		return env('TWILIO_SID');
	}


	protected static function twilioToken()
	{
		return env('TWILIO_TOKEN');
	}

	protected static function twilioNumber()
	{
		return env('TWILIO_FROM');
	}


	protected static function findOrCreateOtp($countryCode, $phone)
	{
		$otp = Otp::where('country_code', $countryCode)
                    ->where('phone', $phone)
                    ->first();

        return !is_null($otp) ? $otp : new Otp;
	}



	protected static function createNumber($countryCode, $phone)
	{
		//country code formate : IN (+91)
		$code = str_replace(")", "", explode("(", $countryCode)[1]);
		return $code.$phone;
	}


	public static function sendOTP($countryCode, $phone, $message, &$error = null)
	{
		
        $otp = self::findOrCreateOtp($countryCode, $phone);
       
        $otp->country_code = $countryCode;
        $otp->phone = $phone;
        $otp->otp = self::generateOTPCode();
        $otp->save();

        $phone = self::createNumber($countryCode, $phone);
        $message = str_replace("{{otp_code}}", $otp->otp, $message);

		try{
        	$client = new Services_Twilio(self::twilioSid(), self::twilioToken());
            $msg = $client->account->messages->sendMessage(
            	self::twilioNumber(), 
            	$phone, 
            	$message 
            );


      	} catch(\Exception $e){
             $error = $e->getMessage();
             return false;
        }

        return true;
	}

	protected static function generateOTPCode()
	{
		return rand(1000, 9999);
	}


	protected static function otpExpired($startTimeString, $endTimeString)
	{
		$diff = Helper::time_diff_minutes($startTimeString, $endTimeString);
		return ($diff > 10) ? true : false;
	}


	protected static function otpCheck($otp, $otpCode)
	{
		return ($otp->otp == $otpCode) ? true : false;
	}


	public static function verifyOTP($countryCode, $phone, $otpCode)
	{
		$otp = Otp::where('country_code', $countryCode)
                    ->where('phone', $phone)
                    ->first();

        if(!$otp) { return false; }

        $startTimeString = $otp->updated_at->toDateTimeString();
        $currTimeString = date('Y-m-d H:i:s');

        if(self::otpCheck($otp, $otpCode) && !self::otpExpired($startTimeString, $currTimeString)) {
        	return true;
        } 

        return false;
	}




	public static function countryCodePatMatched($countryCode)
	{
		return preg_match(self::countryCodePatString(), $countryCode);
	}


	public static function countryCodePatString()
	{
		return "/^[a-zA-Z0-9]+[ ][(][+][0-9]+[)]$/";
	}

}