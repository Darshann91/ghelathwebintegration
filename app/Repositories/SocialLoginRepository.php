<?php

namespace App\Repositories;

use App\Doctor;
use App\User;

class SocialLoginRepository 
{

	protected $userName = "";
	protected $userEmail = "";
	protected $userAvatarURL = "";
	protected $userImageBase64Encoded = "";
	protected $socialID = "";
	protected $userGender = "";
	protected $user_dob = "";

	public function __construct(User $user, Doctor $doctor)
	{
		$this->user = $user;
		$this->doctor = $doctor;
	}


	public function setDob($dob)
	{
		$this->user_dob = $dob;
		return $this;
	}


	public function setGender($gender)
	{
		$this->userGender = in_array($gender, ['male', 'female']) ? $gender : 'male';
		return $this;
	}


	public function setSocialID($id)
	{
		$this->socialID = $id;
		return $this;
	}

	public function setUserEmail($email)
	{
		$this->userEmail = $email;
		return $this;
	}


	public function setUsername($name)
	{
		$this->userName = $name;
		return $this;
	}


	public function setUserAvatarURL($url)
	{
		$this->userAvatarURL = $url;
		return $this;
	}



	public function getUserFirstname()
	{
		if(isset($this->user_first_name)) {
			return $this->user_first_name;
		}

		if($this->userName == "") {
			return "";
		}
 		
 		$arr = explode(" ", $this->userName);
 		$this->user_last_name = isset($arr[1]) ? $arr[1] : "";
		return $this->user_first_name = isset($arr[0]) ? $arr[0] : "";
	}



	public function getUserLastname()
	{
		if(isset($this->user_last_name)) {
			return $this->user_last_name;
		}

		if($this->userName == "") {
			return "";
		}
 	
		$arr = explode(" ", $this->userName);
		$this->user_first_name = isset($arr[0]) ? $arr[0] : "";
		return $this->user_last_name = isset($arr[1]) ? $arr[1] : "";
	}



	public function getBase64Avatar()
	{
		try {

			$imagedata = file_get_contents($this->userAvatarURL);
			$base64 = 'data:'.$this->getMimeType($imagedata).';base64,'.base64_encode($imagedata);
			return $this->userAvatarURL = $base64;

		} catch(\Exception $e) {
			return "";
		}
		if($this->userAvatarURL == "") {
			return "";
		}
	}



	protected function getMimeType($imageData)
	{
		$finfo = new \finfo(FILEINFO_MIME);
		$mime = $finfo->buffer($imageData);
		return explode(";", $mime)[0];
	}




	public function getResponseArray($extraData = [])
	{
		$res = [
			'user_first_name'   => $this->getUserFirstname(),
			'user_last_name'    => $this->getUserLastname(),
			'user_email'        => $this->userEmail,
			'user_social_id'    => $this->socialID,
			'user_gender'       => $this->userGender,
			'user_dob'          => $this->user_dob,
			'user_image_base64' => $this->getBase64Avatar()
		];

		$merged = array_merge($res, $extraData);
		return $this->nullSafe($merged);
	}


	protected function nullSafe($array = [])
	{
		$temp = [];
		foreach($array as $key => $value) {
			$temp[$key] = is_null($value) ? "" : $value;
		}
		return $temp;
	}


	public function getJson($extraData = [])
	{
		return json_encode($this->getResponseArray($extraData));
	}



	public function successResponse($extraData = [], $customOnly = false)
	{
		$json = $customOnly ? json_encode($extraData) : $this->getJson($extraData);
		//json_response
		return '<!DOCTYPE html>
			<html>
				<body>
					<textarea style="display:none" id="json_response">'.$json.'</textarea>
					<script type="text/javascript">
				        setTimeout(function(){
				            self.close();
				        }, 50);
				    </script>
				</body>
			</html>';
	}




	public function errorResponse($errorText, $hidden = true)
	{
		$hiddenError = $hidden ? 'display:none' : '';
		return '<!DOCTYPE html>
			<html>
				<body>
					<h1>Some error happened. Try after some time.</h1>
					<p style="'.$hiddenError.'">'.$errorText.'</p>
					<script type="text/javascript">
				        setTimeout(function(){
				            self.close();
				        }, 50);
				    </script>
				</body>
			</html>';


	}


	public function getStoredUser($type, $socialID = null, $email = null)
	{
		$id = (!$socialID) ? $this->socialID : $socialID;
		$mail = (!$email) ? $this->userEmail : $email;

		if($type == 'user') {
			return $this->user->where('social_unique_id', $id)
								->orWhere('email', $email)->first();
		} else if($type == 'doctor') {
			return $this->doctor->where('social_unique_id', $id)
									->orWhere('email', $email)->first();
		}
	}

}