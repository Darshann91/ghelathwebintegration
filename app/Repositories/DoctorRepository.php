<?php

namespace App\Repositories;

use App\Repositories\OTPRepository as OtpRepo;
use App\Jobs\SendVerificationEmail;
use App\Helpers\Helper;
use App\Speciality;
use App\DoctorConsult;
use App\Clinic;
use App\DoctorFee;
use Validator;
use App\Doctor;
use Hash;
use Log;
use DB;



class DoctorRepository
{


	public function __construct(
		Doctor $doctor, 
		Speciality $speciality, 
		DoctorConsult $doctorConsult, 
		DoctorFee $doctorFee,
		Clinic $clinic
	)
	{
		$this->doctor        = $doctor;
		$this->speciality    = $speciality;
		$this->doctorConsult = $doctorConsult;
		$this->doctorFee     = $doctorFee;
		$this->clinic        = $clinic;
	}


	public static function validateDoctorRegiserData($data = [], &$errors = []) 
	{
		$validator = Validator::make($data, [
			'first_name'            => 'required|max:255',
			'last_name'             => 'required|max:255',
			'email'                 => 'required|email|max:255|unique:doctors,email',
			'dob'                   => 'required|date_format:d-m-Y|before:'.self::currDate(),
			'password'              => 'required|min:6|confirmed',
			'password_confirmation' => 'required|min:6',
			'picture'               => 'required|image|mimes:jpeg,jpg,bmp,png',
			'gender'                => 'required|in:male,female',
			'nationality'           => 'required',
			'country_code'          => 'required|regex:'.OtpRepo::countryCodePatString(),
			'currency'              => 'required',
			'phone'                 => 'required|numeric',
			'otp'                   => 'required',
			'ssn'                   => 'required',
			'med_number'            => 'required',
			'policy'                => 'required'
        ]);

        if($validator->fails()) {
            $errors = self::formatErrors($validator->errors()->getMessages());
            return false;
        } 

        return true;
	}


	protected static function formatErrors($errors)
    {
        $temp = [];
        foreach($errors as $errorKey => $errorVal) {
            $temp[$errorKey] = $errorVal[0];
        }
        return $temp;
    }



	protected static function currDate()
	{
		return date('d-m-Y');
	}


	public function formatMyScheduleDate($dateString)
	{
		$date = date_create_from_format("Y-m-d", $dateString);
		return $date ? $date->format('Y-m-d 00:00:00') : date('Y-m-d 00:00:00');
	}


	public static function generateMD5HashToken()
	{
		return md5(rand(10,100000));
	}



	public static function jsonFormatResponse($isSuccess, $type, $message, $data = [])
	{
		if($isSuccess) {

			return [
				'status' => true,
				'success_type' => $type,
				'success_text' => $message,
				'success_data' => empty($data) ? "" : $data,
			];

		} else {
			return [
				'status' => false,
				'error_type' => $type,
				'error_text' => $message,
				'error_data' => empty($data) ? "" : $data
			];
		}
	}



	public static function createDoctor($data)
	{
		$doctor = new Doctor;

		DB::beginTransaction();

		try {

			$doctor->first_name   = $data['first_name'];
			$doctor->last_name    = $data['last_name'];
			$doctor->email        = $data['email'];
			$doctor->phone        = $data['phone'];
			$doctor->password     = Hash::make($data['password']);
			$doctor->dob          = $data['dob'];
			$doctor->gender       = $data['gender'];
			$doctor->country_code = $data['country_code'];
			$doctor->nationality  = $data['nationality'];
			$doctor->ssn          = $data['ssn'];
			$doctor->currency     = $data['currency'];
			$doctor->med_number   = $data['med_number'];
			$doctor->device_type  = 'android';
			$doctor->device_token = 'random';
			$doctor->login_by 	  = self::loginBy($data);

			if(isset($data['language']))
				$doctor->language = $data['language'];
			
			if(isset($data['timezone']))
				$doctor->timezone = $data['timezone'];

			if(isset($data['social_unique_id']))
				$doctor->social_unique_id = $data['social_unique_id'];
			
			//upload picture and get name of picture    
			$doctor->picture = Helper::upload_picture($data['picture']);
			
			$doctor->save();


			//init extra tables for doctor
			self::initDoctorConsult($doctor->id);
			self::initDoctorFee($doctor->id);

		} catch(\Exception $e) {
			Log::info('Doctor create failed :'.$e->getMessage());
			DB::rollBack();
			$doctor = null;
		}

		DB::commit();

		return $doctor;
	}



	public static function loginBy($data)
	{
		return (isset($data['login_by']) && in_array($data['login_by'], self::logins())) ? $data['login_by'] : 'manual';
	}


	protected static function logins()
	{
		return ['manual', 'facebook', 'google', 'twitter'];
	} 


	public static function initDoctorConsult($doctorID)
	{
		$doctorConsult = new DoctorConsult;
        $doctorConsult->doctor_id = $doctorID;
        $doctorConsult->phone_consult = 0;
        $doctorConsult->chat_consult = 0;
        $doctorConsult->video_consult = 0;
        $doctorConsult->doc_ondemand = 0;
        
        return $doctorConsult->save();
	}


	public static function initDoctorFee($doctorID)
	{
		$doctorFee = new DoctorFee;
        $doctorFee->doctor_id = $doctorID;
        $doctorFee->chat_fee = 0;
        $doctorFee->video_fee = 0;
        $doctorFee->phone_fee =0;
        $doctorFee->ondemand_fee = 0;

        return $doctorFee->save();
	}


	public static function clockDoctorData($doctor)
	{
		if(isset($doctor->password)) unset($doctor->password);
		if(isset($doctor->email_md5)) unset($doctor->email_md5);
		return $doctor;
	}





	/**
	*	used to get country codes and country names as associative array
	*/
	public static function getCountryCodes()
	{ 
		$content = self::countryJSONFileContent();
		$countryCodesArr = json_decode($content, true);
		$temp = [];
		foreach($countryCodesArr as $value) {
			$temp[] = [
				'country' => $value['name'],
				'country_phone_code' => "{$value['alpha-2']} ({$value['phone-code']})"
			];
		}
		return $temp;
	}	




	/**
	*	used to sort country codes based on country names
	*/
	public static function getCountryCodesSortByName()
	{
		$countryCodes = self::getCountryCodes();

		usort($countryCodes, function($a, $b){

            if($a['country'] === $b['country']) {
                return 0;
            }

            return ($a['country'] < $b['country']) ? -1 : 1;

        });

		return $countryCodes;

	}






	protected static function jsonContentParser($content)
	{
    	$content = str_replace(["\r\n", "\r"], "\n", $content);    	
    	return $content;
	}


	protected static function countryJSONFileContent()
	{
		return file_get_contents(public_path('web/countrycodes.json'));
	}


	
	//get doctor profile details
	public function getProfileDetails($doctor)
	{
		$response['doctor'] = $this->getDoctorDataFormated($doctor);
		$response['doctor_fees'] = $this->getDoctorFeesDataFormated($doctor);
		$response['clinic_details'] = $this->getClinicDataFormated($doctor);
		$response['speciality_details'] = $this->getDoctorSpecialityDataFormated($doctor);
		return $response;
	}


	public function getDoctorSpecialityDataFormated($doctor)
	{
		$speciality                    = $this->speciality->find($doctor->speciality_id);
		$spec['id']                    = (!is_null($speciality)) ? $speciality->id : "";
		$spec['speciality']            = (!is_null($speciality)) ? $speciality->speciality : "";
		$spec['picture']               = (!is_null($speciality)) ? $speciality->picture : "";
		$spec['speciality_chinese']    = (!is_null($speciality)) 
											? $speciality->speciality_chinese : "";
		$spec['speciality_thai']       = (!is_null($speciality)) 
											? $speciality->speciality_thai : "";
		$spec['speciality_indonesian'] = (!is_null($speciality)) 
											? $speciality->speciality_indonesian : "";
		return $spec;
 	}





	protected function getClinicDataFormated($doctor)
	{
		$clinic = $this->clinic->find($doctor->clinic_id);
		$clinicDetails['clinic_name']    = (!is_null($clinic)) ? $clinic->clinic_name : "";
		$clinicDetails['clinic_address'] = (!is_null($clinic)) ? $clinic->c_address : "";
		$clinicDetails['clinic_image1']  = (!is_null($clinic)) ? $clinic->c_image1 : "";
		$clinicDetails['clinic_image2']  = (!is_null($clinic)) ? $clinic->c_image2 : "";
		$clinicDetails['clinic_image3']  = (!is_null($clinic)) ? $clinic->c_image3 : "";
		$clinicDetails['clinic_phone']   = (!is_null($clinic)) ? $clinic->c_phone : "";
		$clinicDetails['clinic_fax']     = (!is_null($clinic)) ? $clinic->c_fax : "";
		$clinicDetails['clinic_id']      = (!is_null($clinic)) ? $clinic->id : "";
		$clinicDetails['latitude']       = (!is_null($clinic)) ? $clinic->c_latitude : "";
		$clinicDetails['longitude']      = (!is_null($clinic)) ? $clinic->c_longitude : "";

		return $clinicDetails;
	}



	protected function getDoctorFeesDataFormated($doctor)
	{
		$doctorFee = $this->doctorFee->where('doctor_id', $doctor->id)->first();
		
		if(!$doctorFee) {
			$doctorFee = $this->initDoctorFee($doctor->id);
		}

		return [
			'booking_fee'   => $doctor->booking_fee,
			'chat_fee'      => $doctorFee->chat_fee,
			'video_fee'     => $doctorFee->video_fee,
			'phone_fee'     => $doctorFee->phone_fee,
			'on_demand_fee' => $doctorFee->ondemand_fee,
			'free_minutes'  => $doctorFee->free_minute,
			'consult_rate' => [
				'minutes' => $doctorFee->consult_minute,
				'rate_amount' => $doctorFee->minutes_consult_fee,
			],
		];
	}




	protected function getDoctorDataFormated($doctor)
	{
		return [
			'id'               => $doctor->id,
			'first_name'       => $doctor->first_name,
			'last_name'        => $doctor->last_name,
			'email'            => $doctor->email,
			'phone'            => $doctor->phone,
			'dob'              => $doctor->dob,
			'gender'           => $doctor->gender, 
			'country_code'     => $doctor->country_code,
			'nationality'      => $doctor->nationality,
			'ssn'              => $doctor->ssn,
			'currency'         => $doctor->currency,
			'med_number'       => $doctor->med_number,
			'language'         => $doctor->language,
			'timezone'         => $doctor->timezone,
			'social_unique_id' => $doctor->social_unique_id,
			'picture'          => $doctor->picture,
			'experience'       => $doctor->experience,
			'education_1'      =>  [
				'degree'           => $doctor->degree1,
				'university'       => $doctor->univ1,
				'year'             => $doctor->year1,
			],
			'education_2'      =>  [
				'degree'           => $doctor->degree2,
				'university'       => $doctor->univ2,
				'year'             => $doctor->year2,
			],
		];

	}



	public function updateDoctorProfileDataValidate($doctorID, $data = [], &$errors = [])
	{
		
		$extraRules = (isset($data['picture']) && !is_null($data['picture'])) 
						? ['picture' => 'required|image|mimes:jpeg,jpg,bmp,png'] 
						: [];

		$extraRules['email'] = 'required|email|max:255|unique:doctors,email,'.$doctorID;

		$validator = Validator::make($data, $this->updateRules($extraRules));

        if($validator->fails()) {
            $errors = self::formatErrors($validator->errors()->getMessages());
            return false;
        } 

        return true;
	
	}


	protected function updateRules($extra = [])
	{
		$rules = [
			'first_name'          => 'required|max:255',
			'last_name'           => 'required|max:255',
			'dob'                 => 'required|date_format:d-m-Y|before:'.self::currDate(),
			'gender'              => 'required|in:male,female',
			'nationality'         => 'required',
			'country_code'        => 'required|regex:'.OtpRepo::countryCodePatString(),
			'currency'            => 'required',
			'phone'               => 'required|numeric',
			'otp'                 => 'required',
			'ssn'                 => 'required',
			'med_number'          => 'required',
			'booking_fee'         => 'required|numeric',
			'chat_fee'            => 'required|numeric',
			'phone_fee'           => 'required|numeric',
			'ondemand_fee'        => 'required|numeric',
			'free_minute'         => 'required|numeric',
			'consult_minute'      => 'required|numeric',
			'minutes_consult_fee' => 'required|numeric',
        ];

        return array_merge($rules, $extra);
	}



	public function updateDoctorProfile($doctor, $data, $changePassword)
	{
		DB::beginTransaction();

		try {

			$doctor->first_name   = $data['first_name'];
			$doctor->last_name    = $data['last_name'];
			$doctor->email        = $data['email'];
			$doctor->phone        = $data['phone'];
			$doctor->dob          = $data['dob'];
			$doctor->gender       = $data['gender'];
			$doctor->country_code = $data['country_code'];
			$doctor->nationality  = $data['nationality'];
			$doctor->ssn          = $data['ssn'];
			$doctor->currency     = $data['currency'];
			$doctor->med_number   = $data['med_number'];
			$doctor->booking_fee   = $data['booking_fee'];

			if(isset($data['language']))
				$doctor->language = $data['language'];
			
			if($changePassword)
				$doctor->password = Hash::make($data['new_password']);
			
			if(isset($data['timezone']))
				$doctor->timezone = $data['timezone'];

			if(isset($data['picture']))   
				$doctor->picture = Helper::upload_picture($data['picture']);
			
			$doctor->save();


			$this->doctorFeeUpdate($doctor->id, $data);


		} catch(\Exception $e) {
			Log::info('Doctor update failed :'.$e->getMessage());
			DB::rollBack();
			$doctor = null;
		}

		DB::commit();

		return $doctor;
	}



	public function doctorFeeUpdate($docID, $data)
	{
		$doctorFee = $this->doctorFee->where('doctor_id', $docID)->first();
		$doctorFee->chat_fee            = $data['chat_fee'];
		$doctorFee->phone_fee           = $data['phone_fee'];
		$doctorFee->ondemand_fee        = $data['ondemand_fee'];
		$doctorFee->free_minute         = $data['free_minute'];
		$doctorFee->consult_minute      = $data['consult_minute'];
		$doctorFee->minutes_consult_fee = $data['minutes_consult_fee'];
		return $doctorFee->save();
	}




	public function getScheduledDates($doctorID)
	{
        $bookedDates = DB::table('available_slots')
	            ->join('requests','available_slots.request_id','=','requests.id')
	            ->select('available_slots.date as date')
	            ->where('available_slots.doctor_id', $doctorID)
	            ->where('available_slots.booked', 1)
	            ->where('requests.status', 20)
	            ->where('available_slots.date','>=', date('Y-m-d'))
	            ->get();

	    $dates = [];

	    foreach($bookedDates as $book) {
	    	$dates[] = $this->onlyDateFromBookedDateString($book->date);
	    }

	    return $dates;
	}


	public function onlyDateFromBookedDateString($dateString)
	{
		return str_replace(" 00:00:00", "", $dateString);
	}




	public function getDoctorConsultStatus($doctorID)
    {

        $doctorConsult = DoctorConsult::where('doctor_id', $doctorID)->first();

        return [
			'exists'        => !is_null($doctorConsult) ? true : false,
			'id'            => !is_null($doctorConsult) ? $doctorConsult->id : null,
			'doctor_id'     => !is_null($doctorConsult) ? $doctorConsult->doctor_id : null,
			'phone_consult' => !is_null($doctorConsult) ? $doctorConsult->phone_consult : null,
			'chat_consult'  => !is_null($doctorConsult) ? $doctorConsult->chat_consult : null,
			'video_consult' => !is_null($doctorConsult) ? $doctorConsult->video_consult : null,
			'doc_ondemand'  => !is_null($doctorConsult) ? $doctorConsult->doc_ondemand : null,
        ];

    }




    public function setMediaOnline($doctor)
    {

		$doctorConsult = DoctorConsult::where('doctor_id',$doctor->id)->first();
		$doctorConsult->is_online = 1;
		$doctorConsult->save();

		$doctor->device_token = 'device_token';
		$doctor->session_token = uniqid('logged_out');
        $doctor->is_media_online = ( $doctorConsult->phone_consult == 1 
        							||  $doctorConsult->chat_consult == 1 
        							|| $doctorConsult->video_consult == 1) 
        							? 1 : 0;
        

        $doctor->is_available = 1;
        $doctor->save();

        return $doctor;
        
    }

}