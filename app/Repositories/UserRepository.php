<?php

namespace App\Repositories;

use App\FavoriteDoctor;
use App\Repositories\OTPRepository as OtpRepo;
use App\Repositories\DoctorRepository as DocRepo;
use App\Jobs\SendVerificationEmail;
use App\Helpers\Helper;
use App\Speciality;
use App\ChatMessage;
use Validator;
use App\User;
use App\Doctor;
use App\Requests;
use App\Article;
use Hash;
use Log;

class UserRepository
{

	public function __construct(
		User $user, 
		Speciality $speciality, 
		Requests $requests, 
		Doctor $doctor,
		Article $article,
		FavoriteDoctor $favoriteDoctor
	)
	{
		$this->user = $user;
		$this->speciality = $speciality;
		$this->requests = $requests;
		$this->doctor = $doctor;
		$this->article = $article;
		$this->favoriteDoctor =$favoriteDoctor;
	}


	public static function validateUserRegiserData($data = [], &$errors = []) 
	{
		$validator = Validator::make($data, [
			'first_name'            => 'required|max:255',
			'last_name'             => 'required|max:255',
			'email'                 => 'required|email|max:255|unique:users,email',
			'dob'                   => 'required|date_format:d-m-Y|before:'.self::currDate(),
			'password'              => 'required|min:6|confirmed',
			'password_confirmation' => 'required|min:6',
			'picture'               => 'required|image|mimes:jpeg,jpg,bmp,png',
			'gender'                => 'required|in:male,female',
			'nationality'           => 'required',
			'country_code'          => 'required|regex:'.OtpRepo::countryCodePatString(),
			'currency'              => 'required',
			'phone'                 => 'required|numeric',
			'otp'                   => 'required',
			'ssn'                   => 'required'
        ]);

        if($validator->fails()) {
            $errors = self::formatErrors($validator->errors()->getMessages());
            return false;
        } 

        return true;
	}


	protected static function formatErrors($errors)
    {
        $temp = [];
        foreach($errors as $errorKey => $errorVal) {
            $temp[$errorKey] = $errorVal[0];
        }
        return $temp;
    }



	protected static function currDate()
	{
		return date('d-m-Y');
	}



	public static function generateMD5HashToken()
	{
		return md5(rand(10,100000));
	}



	public static function jsonFormatResponse($isSuccess, $type, $message, $data = [])
	{
		if($isSuccess) {

			return [
				'status' => true,
				'success_type' => $type,
				'success_text' => $message,
				'success_data' => empty($data) ? "" : $data,
			];

		} else {
			return [
				'status' => false,
				'error_type' => $type,
				'error_text' => $message,
				'error_data' => empty($data) ? "" : $data
			];
		}
	}



	public static function createUser($data)
	{
		$user = new User;

		try {

			$user->first_name   = $data['first_name'];
			$user->last_name    = $data['last_name'];
			$user->email        = $data['email'];
			$user->phone        = $data['phone'];
			$user->password     = Hash::make($data['password']);
			$user->dob          = $data['dob'];
			$user->gender       = $data['gender'];
			$user->country_code = $data['country_code'];
			$user->nationality  = $data['nationality'];
			$user->ssn          = $data['ssn'];
			$user->currency     = $data['currency'];
			$user->device_token = "random string";
			$user->device_type  = 'android';
			$user->email_md5 = self::generateMD5HashToken();                   
			$user->login_by = DocRepo::loginBy($data);                   

			if(isset($data['language']))
				$user->language = $data['language'];
			
			if(isset($data['timezone']))
				$user->timezone = $data['timezone'];

			if(isset($data['social_unique_id']))
				$user->social_unique_id = $data['social_unique_id'];
			
			
			//upload picture and get name of picture    
			$user->picture = Helper::upload_picture($data['picture']);
			
			$user->save();

		} catch(\Exception $e) {
			Log::info('User create failed :'.$e->getMessage());
			$user = null;
		}

		return $user;
	}



	public static function sendUserVerificationMail($user)
	{
		dispatch(new SendVerificationEmail($user, self::verifyMailURL($user->email_md5)));
	}


	public static function verifyMailURL($token)
	{
		return url('verifyemail/'.$token);
	}


	public static function clockUserData($user)
	{
		if(isset($user->password)) unset($user->password);
		if(isset($user->email_md5)) unset($user->email_md5);
		return $user;
	}




	public function validateUpdateProfileData($userID, $data = [], &$errors = [])
	{
		$extraRules = $this->pictureRule($data);
		$extraRules['email'] = 'required|email|max:255|unique:users,email,'.$userID;

		$validator = Validator::make($data, $this->updateRules($extraRules));

        if($validator->fails()) {
            $errors = self::formatErrors($validator->errors()->getMessages());
            return false;
        } 

        return true;
	}



	protected function pictureRule($data = [])
	{
		return (isset($data['picture']) && !is_null($data['picture'])) 
						? ['picture' => 'required|image|mimes:jpeg,jpg,bmp,png'] 
						: [];
	}


	protected function updateRules($extra = [])
	{
		$rules = [
			'first_name'          => 'required|max:255',
			'last_name'           => 'required|max:255',
			'dob'                 => 'required|date_format:d-m-Y|before:'.self::currDate(),
			'gender'              => 'required|in:male,female',
			'nationality'         => 'required',
			'country_code'        => 'required|regex:'.OtpRepo::countryCodePatString(),
			'currency'            => 'required',
			'phone'               => 'required|numeric',
			'otp'                 => 'required',
			'ssn'                 => 'required'
        ];

        return array_merge($rules, $extra);
	}


	public function updateProfile($user, $data)
	{
		$user->first_name   = $data['first_name'];
		$user->last_name    = $data['last_name'];
		$user->email        = $data['email'];
		$user->phone        = $data['phone'];
		$user->dob          = $data['dob'];
		$user->gender       = $data['gender'];
		$user->country_code = $data['country_code'];
		$user->nationality  = $data['nationality'];
		$user->ssn          = $data['ssn'];
		$user->currency     = $data['currency'];

		if(isset($data['language']))
			$user->language = $data['language'];
		
		if(isset($data['timezone']))
			$user->timezone = $data['timezone'];

		if(isset($data['picture']))   
			$user->picture = Helper::upload_picture($data['picture']);
		
		$user->save();
		return $user;
	}





	public function getScheduledDates($userID)
	{

		$bookedDates = \DB::table('available_slots')
	                    ->join('requests','available_slots.request_id','=','requests.id')
	                    ->select('available_slots.date as date')
	                    ->where('requests.user_id','=',$userID)
	                    ->where('requests.status','=',20)
	                    ->where('available_slots.booked','=',1)
	                    ->where('available_slots.date','>=',date('Y-m-d'))
	                    ->get();		

	    $dates = [];

	    foreach($bookedDates as $book) {
	    	$dates[] = app('App\Repositories\DoctorRepository')->onlyDateFromBookedDateString($book->date);
	    }

	    return $dates;          
	}
        

        
        public function getAllSpecialities($toArray = false)
	{
		return $toArray 
				? $this->speciality->select(['id', 'speciality', 'picture'])->get()->toArray() 
				: $this->speciality->select(['id', 'speciality', 'picture'])->get();
	}

	public function getAllArticles($toArray = false)
	{
		return $toArray
		 ? $this->article->select(['id','heading','picture','content','doctor_id','article_category_id','is_approved'])->where('is_approved','=',1)->get()->toArray()
		 :$this->article->select(['id','heading','picture','content','doctor_id','article_category_id','is_approved'])->where('is_approved','=',1)->get();
	}


	public function getLastOnProgressRequest($userID)
	{
		$request = $this->requests->where('user_id', $userID)
						->where('status', 2)
						->first();

		return $request;
	}



	public function getChatMessages($userID, $requestID)
	{	
		$request = $this->requests->find($requestID);

		if($request->user_id != $userID) {
			return [];
		}

    	$chatMessages = ChatMessage::where('request_id',$requestID)->get();
  		return $chatMessages->toArray();

	}



	public function searchDoctors($specialityID, $doctorName, $nDocotr = 5) 
	{
		if($specialityID == 0) {
			
			return $this->doctor->where(function($query) use($doctorName){
				$query->where('first_name', 'like', $doctorName.'%')
						->orWhere('last_name', 'like', $doctorName.'%')
						->orWhere('email', 'like', $doctorName.'%');
			})->take($nDocotr)->get();
		} else {

			return $this->doctor->where('speciality_id', $specialityID)
								->where(function($query) use($doctorName){
									$query->where('first_name', 'like', $doctorName.'%')
											->orWhere('last_name', 'like', $doctorName.'%')
											->orWhere('email', 'like', $doctorName.'%');
								})->take($nDocotr)->get();

		}
		
	}



	public function isDoctorFavourited($userID, $doctorID)
	{
		return $this->favoriteDoctor->where('user_id', $userID)
									->where('doctor_id', $doctorID)
									->exists();
		
	}



	public function makeFavouriteDoctor($userID, $doctorID, $isFav)
	{

		$returnType = '';

		switch ($isFav) {

			case 1:
				
				if($this->isDoctorFavourited($userID, $doctorID)) {
					$returnType = 'ALREADY_EXISTS';
					break;
				}

				$favourite = new $this->favoriteDoctor;
				$favourite->doctor_id = $doctorID;
				$favourite->user_id = $userID;
				$favourite->save();

				$returnType = 'FAV_SUCCESS';
				
				break;

			case 0:
				
				$this->favoriteDoctor->where('user_id', $userID)
									->where('doctor_id', $doctorID)
									->delete();
				
				$returnType = 'UNFAV_SUCCESS';

				break;
		}

		return $returnType;

	}



}