<?php


namespace App\Repositories;


use ElephantIO\Client;
use ElephantIO\Engine\SocketIO\Version1X;


class SocketIOClientRepository
{


	protected $globalBroadCastEvent = "PHP_SERVER_BROADCAST_DATA";

	public function __construct()
	{
		$this->autoload();
		$this->host = rtrim(config('SocketIOServer.socket_server_address', '/'));
		$this->port = config('SocketIOServer.socket_server_port');
		$this->serverCommSecKey = config('SocketIOServer.server_internal_communication_key');
		
		$this->client = new Client(new Version1X($this->getConnectionString()));
	}


	protected function getConnectionString()
	{
		return $this->host . ':' . $this->port . '?serverSecretKey=' . $this->serverCommSecKey;
	}



	public function getClient()
	{
		return $this->client;
	}



	public function pushToServer($entityID, $entityType, $data = [])
	{
		try {

			$this->client->initialize();
			$this->client->emit($this->globalBroadCastEvent, [
				'serverSecretKey' => $this->serverCommSecKey,
				'id'              => $entityID,
				'type'            => $entityType,
				'data'            => $data
			]);
			$this->client->close();

			return $this;

		} catch(\Exception $e) {
			return false;
		}
	}


	protected function autoload()
	{
		require app_path('Libraries/Elephant.IO/autoload.php');
	}


	public function getHost()
	{
		return $this->host;
	}


	public function getPort()
	{
		return $this->port;
	}



	public function getServerCommSecKey()
	{
		return $this->serverCommSecKey;
	}

}