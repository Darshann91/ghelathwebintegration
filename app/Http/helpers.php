<?php

//Helper functions

function generate_token()
{
    return (Hash::make(rand() . time() . rand()));
}

function generate_expiry()
{
    return time() + 3600000;
}

function asset_url()
{
    return URL::to('/');
}

function web_url()
{
    return URL::to('/');
}

function is_token_active($ts)
{
    if ($ts >= time()) {
        return true;
    } else {
        return false;
    }
}

