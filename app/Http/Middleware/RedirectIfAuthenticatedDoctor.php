<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticatedDoctor
{

    public function handle($request, Closure $next, $guard = 'doctor-web')
    {
        if (Auth::guard($guard)->check()) {

        	//$doctor = Auth::guard($guard)->user();
       
	       /* if($doctor &&  strpos($doctor->session_token, 'logged_out') !== false) {
                
	            return $next($request);
	        }*/

            return redirect('/doctor/home');
        }

        return $next($request);
    }
}
