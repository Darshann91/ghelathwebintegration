<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AuthenticateDoctor
{
    
    public function handle($request, Closure $next, $guard = 'doctor-web')
    {
        if (Auth::guard($guard)->guest()) {
            if ($request->ajax() || $request->wantsJson()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->guest('doctor/login');
            }
        }

        /*$doctor = Auth::guard($guard)->user();
        
        
        if($doctor &&  strpos($doctor->session_token, 'logged_out') === false) {
            $doctor->session_token = uniqid();
            $doctor->save();
            Auth::guard($guard)->logout();
            return redirect()->guest('doctor/login');
        }*/


        return $next($request);
    }
}
