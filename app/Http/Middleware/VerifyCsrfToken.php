<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        //userAPI
    	
    	'/user/login',
        '/user/getdoctorlistonline',
        '/user/getdoctorlistbooking',
        '/user/updateprofile',
        '/user/getdoctorondemand',
        '/user/getnearbyonline',
        '/user/getnearbybooking',
        '/user/updatelocation',
        '/user/getspeciality',
        '/user/createconsult',
        '/user/finishOnlineConsult',
        '/user/getslots',
    	//doctorAPI
    	'/doctor/register',
    	'/doctor/login',
        '/doctor/setconsults',
        '/doctor/getconsults',
        '/doctor/setconsulttime',
        '/doctor/getconsulttime',
        '/doctor/updatelocation',
        '/doctor/getarticlecategory',
        '/doctor/addarticle',
        '/doctor/editarticle',
        '/doctor/pushtest',
        '/doctor/updateprofile',
        '/doctor/deletearticle'
        
    ];
}
