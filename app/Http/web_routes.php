<?php

/* Web routes for patient */

Route::get('/', 'HomeController@index')->name('index');
Route::get('login', 'Auth\AuthController@showLoginForm')->name('login');
Route::post('otp/send', 'OTPController@sendOtp')->name('send-otp');


/* Facebook login and register routes */
Route::get('facebook', 'FacebookController@providerRedirect')->middleware('guest');
Route::get('facebook/callback', 'FacebookController@providerCallback')->middleware('guest');


/* Google login and register routes */
Route::get('google', 'GoogleController@providerRedirect')->middleware('guest');
Route::get('google/callback', 'GoogleController@providerCallback')->middleware('guest');

/* Twitter login and register routes */
Route::get('twitter', 'TwitterController@providerRedirect')->middleware('guest');
Route::get('twitter/callback', 'TwitterController@providerCallback')->middleware('guest');


/* get all specialities */
Route::get('get-specialities', 'UserWebController@getSpecialities');


/* socket jwt token generate */
Route::get('socket/jwt/token', 'JWTController@createWebJWTSocketToken');


Route::group(['prefix' => 'user'], function(){

	Route::get('login','Auth\AuthController@showLoginForm');
	Route::post('login', 'Auth\AuthController@login');
	Route::get('logout', 'Auth\AuthController@logout');
	Route::get('logout', 'Auth\AuthController@logout');
	Route::get('register', 'UserWebController@showRegister')->middleware('guest');
	Route::post('register', 'UserWebController@register')->middleware('guest');

	Route::group(['middleware' => 'auth'], function(){

		Route::get('home', 'UserWebController@showHome');
		
		Route::get('settings', 'UserWebController@showSettings');
		Route::get('settings/template/profile', 'UserWebController@profileTemplate');
		Route::get('settings/profile/details', 'UserWebController@profileDetails');
		Route::post('settings/profile/update', 'UserWebController@upateProfile');
		Route::get('settings/template/schedules', 'UserWebController@schedulesTemplate');
		Route::get('settings/my-schedules', 'UserWebController@getMySchedules');
		Route::get('settings/schedule/dates', 'UserWebController@getScheduledDates');
		Route::get('settings/template/change-password', 'UserWebController@changePasswordTemplate');
		Route::post('settings/password/change', 'UserWebController@changePassword');
		Route::get('settings/template/chat-consult-history', 'UserWebController@chatConsultTemplate');
		Route::get('settings/history/consult/chat', 'UserWebController@getChatConsultHistories');
		Route::get('settings/history/consult/chat/messages' , 'UserWebController@getChatMessages');
		Route::get('settings/template/phone-consult-history', 'UserWebController@phoneConsultHistoryTemplate');
		Route::get('settings/history/consult/phone', 'UserWebController@getPhoneConsultHistories');
		Route::get('settings/template/video-consult-history', 'UserWebController@videoConsultHistoryTemplate');
		Route::get('settings/history/consult/video', 'UserWebController@getVideoConsultHistories');
		Route::get('settings/template/ask-question', 'UserWebController@askQuestionTemplate');
		Route::post('settings/ask-question', 'UserWebController@askQuestion');
		Route::get('settings/search/doctors', 'UserWebController@searchDoctors');
		Route::get('home/articles/{id}','UserWebController@showArticle')->name('showArticle');
		Route::get('home/articles','UserWebController@showAllArticles')->name('showAllArticles');
		Route::get('settings/template/doctor-messages', 'UserWebController@doctorMessagesTemplate');

		

		Route::get('home/categories/{cat_id}', 'UserWebController@showCategories');
		Route::get('home/categories/template/online-consult', 'UserWebController@onlineConsultTemple');
		Route::get('get-online-consult-doctors-list', 'UserWebController@getOnlineDoctorsList');
		Route::get('home/categories/template/book-appointment', 'UserWebController@bookAppointmentTemplate');
		Route::get('get-booking-doctors-list', 'UserWebController@getBookingDoctorsList');


		/* get available slots for a specified doctor id and date */
		Route::get('get-available-slots', 'UserWebController@getAvailableSlots');


		/* get user saved cards */
		Route::get('get-cards', 'UserWebController@getUserSavedCards');
		Route::get('settings/template/cards', 'UserWebController@userCardsTemplate');
		Route::post('make-default-card', 'UserWebController@makeUserCardDefault');
		Route::post('braintree/client-token', 'UserWebController@getBraintreeClientToken');
		Route::post('save-card', 'UserWebController@saveCard');

		/* book slot */
		Route::post('book-slot', 'UserWebController@bookSlot');


		/* create request */
		Route::post('create-request', 'UserWebController@createRequest');

		/* cancel request */
		Route::post('cancel-request', 'UserWebController@cancelRequest');

		/* get request doctor accept or reject service */
		Route::get('get-request', 'UserWebController@getRequest');

		/* generate or fetch opentok session and token */
		Route::get('opentok/generate/session', 'UserWebController@createOpentokSession');

		Route::post('debt/clear', 'UserWebController@clearDebt');


		Route::post('doctor/rating', 'UserWebController@rateDoctor');



		Route::post('upload/attachment/image', 'UserWebController@uploadImage');

		Route::get('request/ongoing', 'UserWebController@getLastOngoingRequest');


		Route::post('doctor/make-favourite', 'UserWebController@makeFavouriteDoctor');
		Route::get('messages-list', 'UserWebController@getMessages');
		Route::post('doctor/send-reply', 'UserWebController@sendMessageReply');
		


	});

});
/* End Web routes for patient */


/* Web routes for doctor */
Route::group(['prefix' => 'doctor'], function(){

	Route::get('login', 'Auth\DoctorAuthController@showLoginForm');
	Route::post('login', 'Auth\DoctorAuthController@login');
	Route::get('logout', 'Auth\DoctorAuthController@logout');
	Route::get('register', 'DoctorWebController@showRegister')->middleware('doctorGuest');
	Route::post('register', 'DoctorWebController@register')->middleware('doctorGuest');

	Route::group(['middleware' => 'doctor'], function(){

		Route::get('home', 'DoctorWebController@showHome');
		
		Route::get('settings', 'DoctorWebController@showSettings');
		Route::get('settings/template/profile', 'DoctorWebController@profileTemplate');
		Route::get('settings/profile/details', 'DoctorWebController@profileDetails');
		Route::post('settings/profile/update', 'DoctorWebController@upateProfileDetails');
		Route::get('settings/template/schedules', 'DoctorWebController@schedulesTemplate');
		Route::get('settings/my-schedules', 'DoctorWebController@getMySchedules');
		Route::get('settings/schedule/dates', 'DoctorWebController@getScheduleDates');



		Route::get('home/template/appointments', 'DoctorWebController@getAppointmentsTemplate');
		/*set consult status */
		Route::post('set-consult-status', 'DoctorWebController@setConsultStatus');

		/* get doctor time slots by specific speciality */
		Route::get('get-slots', 'DoctorWebController@getSlots');
		Route::get('get-saved-slots-by-date', 'DoctorWebController@getSavedSlotsByDate');
		Route::post('save-slots', 'DoctorWebController@saveSlots');




		/* incoming request from user polling */
		Route::get('incoming-request', 'DoctorWebController@getIncomingRequest');


		/* generate or fetch opentok session and token */
		Route::get('opentok/generate/session', 'DoctorWebController@createOpentokSession');

		/* voice, video, chat service request accept and reject */
		Route::post('service/accept', 'DoctorWebController@serviceAccept');
		Route::post('service/reject', 'DoctorWebController@serviceReject');
		Route::post('service/completed/online', 'DoctorWebController@serviceOnlineCompleted');


		Route::post('user/rating', 'DoctorWebController@rateUser');


		Route::post('upload/attachment/image', 'UserWebController@uploadImage');



		Route::get('favourite/patients', 'DoctorWebController@getPatientsFavourite');
		Route::get('messages-list', 'DoctorWebController@getMessages');
		Route::post('user/send-message', 'DoctorWebController@sendMessageToUser');

		Route::get('settings/template/messages', 'DoctorWebController@getMesssagesTemplate');


		/**
		*	doctor booking end and cancel booking 
		*/
		Route::post('appointments/end', 'DoctorWebController@endAppointment');
		Route::post('appointments/cancel', 'DoctorWebController@cancelAppointment');



	});

});
/* End Web routes for doctor */







/* socket and server internal communication remote prodedure call route */
Route::get('server-rpc', 'SocketCommunicationController@remoteProcedureCall');
Route::post('server-rpc', 'SocketCommunicationController@remoteProcedureCall');
