<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Repositories\UserRepository;
use Auth;

class GlobalComposer
{
   
    protected $view = null; 
   
    public function compose(View $view)
    {
        $this->view = $view;
        $this->shareAuthDoctor();
        $this->shareAuthUser();
        $this->shareCsrfToken();
    }



    protected function shareCsrfToken()
    {
        $this->view->with('csrf_token', csrf_token());
    }


    protected function shareAuthDoctor()
    {
        $auth_user = Auth::guard('doctor-web')->user();
        $this->view->with('auth_doctor', $auth_user);
    }



    protected function shareAuthUser()
    {
        $auth_user = Auth::guard('web')->user();
        $this->view->with('auth_user', $auth_user);
    }

}