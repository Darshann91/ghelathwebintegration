<?php
//Push notification davibennun -  change sslverifypeer->null, in
//zendframework-zend-http-src-adapter-socket.php
namespace App\Http\Controllers;

use Swap\Laravel\Facades\Swap;
use App\ChatMessage;
use App\Clinic;
use App\DoctorMessage;
use App\Document;
use App\FavoriteDoctor;
use App\Setting;
use App\Shop;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use Hash;
use Illuminate\Http\Request;
use Validator;
use Log;
use Response;
use App\Doctor;
use App\Speciality;
use App\DoctorConsult;
use App\DoctorTime;
use App\Article;
use App\ArticleCategory;
use App\PushNotification;
use App\DoctorFee;
use DB;
use App\Helpers\Helper;
use App\Requests;
use App\RequestsMeta;
use App\User;
use App\UserRating;
use App\RequestPayment;
use App\DoctorRating;
use App\DoctorAvailability;
use App\DoctorSlot;
use App\AvailableSlot;
use App\UserDebt;
use App\Payment;
use App\DoctorIssue;
use App\Otp;
use App\Question;
use DateTime;
use App\RecurringSlot;
use App\Jobs\SendUserInvoiceEmail;

use Tomcorbett\OpentokLaravel\Facades\OpentokApi;
use OpenTok\Role;
use OpenTokException;

use Twilio\Rest\Client;

use Services_Twilio;

use Braintree_Transaction;
use Braintree_Customer;
use Braintree_WebhookNotification;
use Braintree_Subscription;
use Braintree_CreditCard;


define('USER', 0);

define('doctor',1);

define('NONE', 0);

define('DEFAULT_FALSE', 0);
define('DEFAULT_TRUE', 1);

// Payment Constants
define('COD',   'cod');
define('PAYPAL', 'paypal');
define('CARD',  'card');

// Request table status
define('REQUEST_NEW',        0);
define('REQUEST_WAITING',      1);
define('REQUEST_INPROGRESS',    2);
define('REQUEST_COMPLETE_PENDING',  3);
define('WAITING_FOR_DOCTOR_CONFRIMATION_COD',  8);
define('REQUEST_RATING',      4);
define('REQUEST_COMPLETED',      5);
define('REQUEST_CANCELLED',      6);
define('REQUEST_NO_DOCTOR_AVAILABLE',7);


//Only when manual request
define('REQUEST_REJECTED_BY_DOCTOR', 9);

define('DOCTOR_NOT_AVAILABLE', 0);
define('DOCTOR_AVAILABLE', 1);

define('DOCTOR_AVAILABILITY_FREE' , 0);
define('DOCTOR_AVAILABILITY_SET' , 1);
define('DOCTOR_AVAILABILITY_BOOKED' , 2);


// Request table doctor_status

define('DOCTOR_NONE', 0);
define('DOCTOR_ACCEPTED', 1);
define('DOCTOR_STARTED', 2);
define('DOCTOR_ARRIVED', 3);
define('DOCTOR_SERVICE_STARTED', 4);
define('DOCTOR_SERVICE_COMPLETED', 5);
define('DOCTOR_RATED', 6);
define('DOCTOR_ACCEPTED_CHAT', 10);
define('DOCTOR_ACCEPTED_PHONE', 11);
define('DOCTOR_ACCEPTED_VIDEO', 12);



define('REQUEST_META_NONE',   0);
define('REQUEST_META_OFFERED',   1);
define('REQUEST_META_TIMEDOUT', 2);
define('REQUEST_META_DECLINED', 3);

define('WAITING_TO_RESPOND', 1);
define('WAITING_TO_RESPOND_NORMAL',0);

define('RATINGS', '0,1,2,3,4,5');

define('DEVICE_ANDROID', 'android');
define('DEVICE_IOS', 'ios');

// Consult/Service types

define('CHAT_CONSULT', 1);
define('PHONE_CONSULT', 2);
define('VIDEO_CONSULT', 3);
define('ONDEMAND_CONSULT', 4);
define('BOOKING', 5);


class DoctorApiController extends Controller
{


    public function __construct(Request $request)
    {
        $this->middleware('DoctorApiVal' , array('except' => ['getChatMessage','saveChatMessage','register' , 'login' , 'forgot_password','getSpeciality','generateOtp','recurring_slot_cron']));
    }


    //Doctor Register API

    public function register(Request $request)
    {
        //Temporary Country Validation -- start

        if($request->has('current_country')){
            if(strtolower($request->current_country) != 'malaysia')
            {
                $response_array = array('success' => false, 'error_message' => 'We are sorry but registration is not currently supported at your region', 'error_code' => 105);

                $response = response()->json($response_array, 200);
                return $response;

            }
        }

        //Temporary Country Validation -- end

        $response_array = array();
        $operation = false;
        $new_user = DEFAULT_TRUE;

        // validate basic field

        $basicValidator = Validator::make(
            $request->all(),
            array(
                'device_type' => 'required|in:'.DEVICE_ANDROID.','.DEVICE_IOS,
                'device_token' => 'required',
                'login_by' => 'required|in:manual,facebook,google,twitter',
                'otp' => 'required'
            )
        );

        if($basicValidator->fails()) {

            $error_messages = implode(',', $basicValidator->messages()->all());
            $response_array = array('success' => false, 'error' => Helper::get_error_message(101), 'error_code' => 101, 'error_message'=> $error_messages);
            Log::info('Registration basic validation failed');

        }else {

            $login_by = $request->login_by;
            $allowed_social_login = array('facebook','google','twitter');


             if(in_array($login_by,$allowed_social_login)){

                // validate social registration fields

                $socialValidator = Validator::make(
                            $request->all(),
                            array(
                                'social_unique_id' => 'required',
                                'first_name' => 'required|max:255',
                                'last_name' => 'max:255',
                                'email' => 'required|email|max:255|unique:doctors,email',
                                'picture' => 'mimes:jpeg,jpg,bmp,png',
                                'dob' => 'required',
                                'med_number' => 'required',
                                'country_code' => 'required',
                                'nationality' => 'required',
                                'currency' => 'required',
                                'language' => 'required'
                            )
                        );

                if($socialValidator->fails()) {

                    $error_messages = implode(',', $socialValidator->messages()->all());
                    $response_array = array('success' => false, 'error' => Helper::get_error_message(101), 'error_code' => 101, 'error_message'=> $error_messages);

                    Log::info('Registration social validation failed');

                }else {

                    $check_social_user = Doctor::where('email' , $request->email)->first();

                    if($check_social_user) {
                        $new_user = DEFAULT_FALSE;
                    }

                    Log::info('Registration passed social validation');
                    $operation = true;
                }

            }else {

                // Validate manual registration fields

                $manualValidator = Validator::make(
                    $request->all(),
                    array(
                        'first_name' => 'required|max:255',
                        'last_name' => 'required|max:255',
                        'email' => 'required|email|max:255|unique:doctors,email',
                        'dob' => 'required',
                        'password' => 'required|min:6',
                        'picture' => 'mimes:jpeg,jpg,bmp,png',
                        'med_number' => 'required',
                        'gender' => 'required',
                        'nationality' => 'required',
                        'country_code' =>'required',
                        'currency' => 'required',
                        'language' => 'required'
                    )
                );

                // validate email existence

                $emailValidator = Validator::make(
                    $request->all(),
                    array(
                        'email' => 'unique:doctors,email',
                    )
                );

                if($manualValidator->fails()) {

                    $error_messages = implode(',', $manualValidator->messages()->all());
                    $response_array = array('success' => false, 'error' => Helper::get_error_message(101), 'error_code' => 101, 'error_message'=> $error_messages);
                    Log::info('Registration manual validation failed');

                } elseif($emailValidator->fails()) {

                    $error_messages = implode(',', $emailValidator->messages()->all());
                    $response_array = array('success' => false, 'error' => Helper::get_error_message(101), 'error_code' => 101, 'error_message'=> $error_messages);
                    Log::info('Registration manual email validation failed');

                } else {
                    Log::info('Registration passed manual validation');
                    $operation = true;
                }
            }

            $otp = Otp::where('phone',$request->phone)->where('country_code',$request->country_code)->first();

            if($otp){

            if($request->otp == $otp->otp){

            if($operation)
            {
                if($new_user){
                    $doctor = new Doctor;
                }else{
                    $doctor = $check_social_user;
                }

                  if($request->has('first_name')) {
                    $doctor->first_name = $request->first_name;
                }

                if($request->has('last_name')) {
                    $doctor->last_name = $request->last_name;
                }

                if($request->has('email')) {
                    $doctor->email = $request->email;
                }

                if($request->has('phone')) {
                    $doctor->phone = $request->phone;
                }

                if($request->has('password'))
                    $doctor->password = Hash::make($request->password);

                if($request->has('dob')) {
                    $doctor->dob = $request->dob;
                }



                if($request->has('med_number')){
                    $doctor->med_number = $request->med_number;
                }

                if($request->has('nationality')){
                    $doctor->nationality = $request->nationality;
                }

                if($request->has('gender')){
                    $doctor->gender = $request->gender;
                }

                if($request->has('country_code')){
                    $doctor->country_code = $request->country_code;
                }

                if($request->has('ssn')){
                    $doctor->ssn = $request->ssn;
                }

                if($request->has('currency')){
                    $doctor->currency = $request->currency;
                }

                if($request->has('language')){
                    $doctor->language = $request->language;
                }

                if($request->has('timezone')){
                    $doctor->timezone = $request->timezone;
                }


                $doctor->session_token = Helper::generate_token();
                $doctor->session_token_expiry = Helper::generate_token_expiry();

                $check_device_exist = Doctor::where('device_token', $request->device_token)->first();

                if($check_device_exist){
                    $check_device_exist->device_token = "";
                    $check_device_exist->save();
                }

                $doctor->device_token = $request->has('device_token') ? $request->device_token : "";
                $doctor->device_type = $request->has('device_type') ? $request->device_type : "";
                $doctor->login_by = $request->has('login_by') ? $request->login_by : "";
                $doctor->social_unique_id = $request->has('social_unique_id') ? $request->social_unique_id : '';

                 // Upload picture
                $doctor->picture = Helper::upload_picture($request->file('picture'));

                $doctor->save();

                $speciality = Speciality::find($request->speciality_id);

                $doctorConsult = new DoctorConsult;

                $doctorConsult->doctor_id = $doctor->id;
                $doctorConsult->phone_consult = 0;
                $doctorConsult->chat_consult = 0;
                $doctorConsult->video_consult = 0;
                $doctorConsult->doc_ondemand = 0;
                $doctorConsult->save();

                $doctorFee = new DoctorFee;

                $doctorFee->doctor_id = $doctor->id;
                $doctorFee->chat_fee = 0;
                $doctorFee->video_fee = 0;
                $doctorFee->phone_fee =0;
                $doctorFee->ondemand_fee = 0;

                $doctorFee->save();

                $response_array = array(
                                'success' => true,
                                'id' => $doctor->id,
                                'first_name' => $doctor->first_name,
                                'last_name' => $doctor->last_name,
                                'phone' => $doctor->phone,
                                'email' => $doctor->email,
                                'picture' => $doctor->picture,
                                'login_by' => $doctor->login_by,
                                'social_unique_id' => $doctor->social_unique_id,
                                'device_token' => $doctor->device_token,
                                'device_type' => $doctor->device_type,
                                'session_token' => $doctor->session_token,
                                'med_number' =>$doctor->med_number,
                                'dob' => $doctor->dob,
                                'gender'=>$doctor->gender,
                                'nationality' => $doctor->nationality,
                                'country_code' => $doctor->country_code,
                                'c_name' =>$doctor->c_name,
                                'c_street' =>$doctor->c_street,
                                'c_city' =>$doctor->c_city,
                                'c_state' =>$doctor->c_state,
                                'c_country'=>$doctor->c_country,
                                'c_postal'=>$doctor->c_postal,
                                'c_address' =>"",
                                'c_pic1' =>$doctor->c_pic1,
                                'c_pic2' =>$doctor->c_pic2,
                                'c_pic3' =>$doctor->c_pic3,
                                'experience' =>$doctor->experience,
                                'univ1' =>$doctor->univ1,
                                'degree1' =>$doctor->degree1,
                                'year1' =>$doctor->year1,
                                'univ2' =>$doctor->univ2,
                                'degree2' =>$doctor->degree2,
                                'year2' =>$doctor->year2,
                                'speciality'=>"",
                                'speciality_id' => "",
                                'chat_fee' =>'0',
                                'phone_fee' =>'0',
                                'video_fee' =>'0',
                                'booking_fee' =>'0',
                                'ondemand_fee' =>'0',
                                'title' =>$doctor->title,
                                'ssn' => $doctor->ssn,
                                'is_shop' => '0',
                                'currency'=> $doctor->currency,
                                'language' =>$doctor->language,
                                'timezone' =>$doctor->timezone,
                                'free_minute' => '0',
                                'consult_minute' => '0',
                                'minutes_consult_fee' =>'0'

                            );
                $response_array = Helper::null_safe($response_array);
                     Log::info('Registration completed');


            }

            }else{
            $response_array = array( 'success' => false, 'error_message' => 'Otp does not match', 'error_code' => 105 );

        }
    }else{
        $response_array = array( 'success' => false, 'error_message' => 'Otp does not exist', 'error_code' => 105 );

    }

        }

        $response = response()->json($response_array, 200);
        return $response;
    }



    // Doctor Login API
    public function login(Request $request)
    {

        $response_array = array();
        $operation = false;

        $basicValidator = Validator::make(
            $request->all(),
            array(
                'device_token' => 'required',
                'device_type' => 'required|in:'.DEVICE_ANDROID.','.DEVICE_IOS,
                'login_by' => 'required|in:manual,facebook,google,twitter',
            )
        );

        if($basicValidator->fails()){

            $error_messages = implode(',',$basicValidator->messages()->all());
            $response_array = array('success' => false, 'error' => Helper::get_error_message(101), 'error_code' => 101, 'error_messages'=> $error_messages);

        }else{

            $login_by = $request->login_by;

            if($login_by == 'manual'){

                 /*validate manual login fields*/
                $manualValidator = Validator::make(
                    $request->all(),
                    array(
                        'email' => 'required|email',
                        'password' => 'required',
                    )
                );
                if ($manualValidator->fails()) {
                    $error_messages = implode(',',$manualValidator->messages()->all());
                    $response_array = array('success' => false, 'error' => Helper::get_error_message(101), 'error_code' => 101, 'error_messages'=> $error_messages);
                }else{

                    $email = $request->email;
                    $password = $request->password;

                    // Validate the user credentials
                    if($doctor = Doctor::where('email', '=', $email)->first()){

                        if(Hash::check($password, $doctor->password)){

                                /*manual login success*/
                                $operation = true;

                            }else{
                                $response_array = array( 'success' => false, 'error' => Helper::get_error_message(105), 'error_code' => 105 );
                            }


                    }else {
                        $response_array = array( 'success' => false, 'error' => Helper::get_error_message(105), 'error_code' => 105 );
                }
            }


        }else{

                /*validate social login fields*/
             $socialValidator = Validator::make(
                 $request->all(),
                 array(
                     'social_unique_id' => 'required',
                 )
             );

              if ($socialValidator->fails()) {
                    $error_messages = implode(',',$socialValidator->messages()->all());
                    $response_array = array('success' => false, 'error' => Helper::get_error_message(101), 'error_code' => 101, 'error_messages'=> $error_messages);
                } else {

                    $social_unique_id = $request->social_unique_id;
                    if ($doctor = Doctor::where('social_unique_id', '=', $social_unique_id)->first()){

                         $operation = true;

                    } else{
                        $response_array = array('success' => false, 'error' => Helper::get_error_message(125), 'error_code' => 125);
                    }
                }


        }

        // if($doctor){
        // if($doctor->is_approved == 1){

        if($operation){

            $device_token = $request->device_token;
            $device_type = $request->device_type;

            // Generate new tokens
            $doctor->session_token = Helper::generate_token();
            $doctor->session_token_expiry = Helper::generate_token_expiry();

            // Save device details
            $doctor->device_token = $device_token;
            $doctor->device_type = $device_type;
            $doctor->login_by = $login_by;
            $doctor->is_booking_online = 1;

            $doctor->save();

            $speciality = Speciality::find($doctor->speciality_id);
            $doctor_fee = DoctorFee::where('doctor_id',$doctor->id)->first();
            if($doctor_fee){
                $chat_fee = $doctor_fee->chat_fee;
                $phone_fee = $doctor_fee->phone_fee;
                $video_fee = $doctor_fee->video_fee;
                $ondemand_fee = $doctor_fee->ondemand_fee;
            }else{
                $chat_fee = "";
                $phone_fee = "";
                $video_fee = "";
                $ondemand_fee = "";

            }


            $clinic = Clinic::find($doctor->clinic_id);
            if($clinic)
            {

                $clinic_name = $clinic->clinic_name;
                $clinic_address = $clinic->c_address;
                $clinic_image1 = $clinic->c_image1;
                $clinic_image2 = $clinic->c_image2;
                $clinic_image3 = $clinic->c_image3;
                $clinic_phone = $clinic->c_phone;
                $clinic_fax = $clinic->c_fax;
                $clinic_id = $clinic->id;
            }else{
                $clinic_name = "";
                $clinic_address = "";
                $clinic_image1 = "";
                $clinic_image2 = "";
                $clinic_image3 = "";
                $clinic_phone = "";
                $clinic_fax = "";
                $clinic_id = "";
            }

            $shop = Shop::where('doctor_id',$doctor->id)->first();

            if($shop){
                $is_shop = 1;
            }else{
                $is_shop = '0';
            }

            $speciality = Speciality::find($doctor->speciality_id);
            if($speciality){
                $speciality_name = $speciality->speciality;
            }else{
                $speciality_name = "";
            }


            $response_array = array(
                                'success' => true,
                                'id' => $doctor->id,
                                'first_name' => $doctor->first_name,
                                'last_name' => $doctor->last_name,
                                'phone' => $doctor->phone,
                                'email' => $doctor->email,
                                'picture' => $doctor->picture,
                                'login_by' => $doctor->login_by,
                                'social_unique_id' => $doctor->social_unique_id,
                                'device_token' => $doctor->device_token,
                                'device_type' => $doctor->device_type,
                                'session_token' => $doctor->session_token,
                                'dob' => $doctor->dob,
                                'med_number' => $doctor->med_number,
                                'gender'=>$doctor->gender,
                                'nationality' => $doctor->nationality,
                                'country_code' =>$doctor->country_code,
                                'c_name' =>$clinic_name,
                                'c_address' => $clinic_address,
                                'c_street' =>$doctor->c_street,
                                'c_city' =>$doctor->c_city,
                                'c_state' =>$doctor->c_state,
                                'c_country'=>$doctor->c_country,
                                'c_postal'=>$doctor->c_postal,
                                'c_pic1' =>$clinic_image1,
                                'c_pic2' =>$clinic_image2,
                                'c_pic3' =>$clinic_image3,
                                'experience' =>$doctor->experience,
                                'univ1' =>$doctor->univ1,
                                'degree1' =>$doctor->degree1,
                                'year1' =>$doctor->year1,
                                'univ2' =>$doctor->univ2,
                                'degree2' =>$doctor->degree2,
                                'year2' =>$doctor->year2,
                                'speciality'=>$speciality_name,
                                'speciality_id' => $doctor->speciality_id,
                                'chat_fee' =>$chat_fee,
                                'phone_fee' =>$phone_fee,
                                'video_fee' =>$video_fee,
                                'booking_fee' =>$doctor->booking_fee,
                                'ondemand_fee' =>$ondemand_fee,
                                'title' =>$doctor->title,
                                'ssn' => $doctor->ssn,
                                'is_shop' => $is_shop,
                                'is_approved' => $doctor->is_approved,
                                'currency' => $doctor->currency,
                                'language' => $doctor->language,
                                'timezone' => $doctor->timezone,
                                'free_minute' => $doctor_fee->free_minute,
                                'consult_minute' => $doctor_fee->consult_minute,
                                'minutes_consult_fee' =>$doctor_fee->minutes_consult_fee
                            );

            $response_array = Helper::null_safe($response_array);
                     Log::info('Registration completed');


        }
    // }else{
    //     $response_array = array('success' => false, 'error' => 'Sorry you are not approved by the admin yet', 'error_code' => 125);

    //  }
    // }
      }

      $response = response()->json($response_array, 200);
      return $response;

    }

    public function getSpeciality()
    {
        $specialities = Speciality::all();
        $speciality = array();
        foreach ($specialities as $spec) {
            $special['id']=$spec->id;
            $special['speciality']=$spec->speciality;
            array_push($speciality, $special);
        }

        $response_array = array('success' => true ,'speciality' => $speciality);
        $response_array = Helper::null_safe($response_array);

        $response = Response::json($response_array, 200);
        return $response;

    }

    public function setConsults(Request $request)
    {

        $doctorConsult = DoctorConsult::where('doctor_id',$request->id)->first();

        $doctor = Doctor::find($request->id);

        if($doctorConsult){
            $doctorConsult->phone_consult=$request->phone_consult;
            $doctorConsult->chat_consult=$request->chat_consult;
            $doctorConsult->video_consult=$request->video_consult;
            $doctorConsult->doc_ondemand=$request->doc_ondemand;
            $doctorConsult->save();

        }else{
            $doctorConsult=new DoctorConsult;
            $doctorConsult->doctor_id=$request->id;
            $doctorConsult->phone_consult=$request->phone_consult;
            $doctorConsult->chat_consult=$request->chat_consult;
            $doctorConsult->video_consult=$request->video_consult;
            $doctorConsult->doc_ondemand=$request->doc_ondemand;
            $doctorConsult->save();
            }

        if(($request->chat_consult == 1)||($request->phone_consult == 1)||($request->video_consult == 1)){
            $doctorConsult->is_online = 1;
            $doctor->is_media_online =1;
        }
        if(($request->chat_consult == 0)&&($request->phone_consult == 0)&&($request->video_consult == 0)){
            $doctorConsult->is_online = 0;
            $doctor->is_media_online =0;
        }

        $doctorConsult->save();
        $doctor->save();

        $response_array = array('success' => true,
        'id' => $doctorConsult->id,
        'doctor_id' => $doctorConsult->doctor_id,
        'phone_consult' =>$doctorConsult->phone_consult,
        'chat_consult' =>$doctorConsult->chat_consult,
        'video_consult' =>$doctorConsult->video_consult,
        'doc_ondemand' =>$doctorConsult->doc_ondemand
        );
        //$response_array = Helper::null_safe($response_array);

        $response = response()->json($response_array, 200);
        return $response;


    }

    public function getConsults(Request $request)
    {

        $doctorConsult = DoctorConsult::where('doctor_id',$request->id)->first();
        if($doctorConsult){
        $response_array = array('success' => true,
            'id' => $doctorConsult->id,
            'doctor_id' => $doctorConsult->doctor_id,
            'phone_consult' =>$doctorConsult->phone_consult,
            'chat_consult' =>$doctorConsult->chat_consult,
            'video_consult' =>$doctorConsult->video_consult,
            'doc_ondemand' =>$doctorConsult->doc_ondemand
            );
        }else{
            $response_array = array('success' => false,'error'=>'consults not set','error_code'=>401 );
        }
        //$response_array = Helper::null_safe($response_array);

        $response = response()->json($response_array, 200);
        return $response;
    }

    public function setConsultTime(Request $request)
    {

    }

    public function getConsultTime(Request $request)
    {

    }

    public function updateProfile(Request $request)
    {
        $id = $request->id;
        $doctor = Doctor::find($id);

        $clinic = Clinic::find($doctor->clinic_id);
        if($clinic)
        {

            $clinic_name = $clinic->clinic_name;
            $clinic_address = $clinic->c_address;
            $clinic_image1 = $clinic->c_image1;
            $clinic_image2 = $clinic->c_image2;
            $clinic_image3 = $clinic->c_image3;
            $clinic_phone = $clinic->c_phone;
            $clinic_fax = $clinic->c_fax;
            $clinic_id = $clinic->id;
        }else{
            $clinic_name = "";
            $clinic_address = "";
            $clinic_image1 = "";
            $clinic_image2 = "";
            $clinic_image3 = "";
            $clinic_phone = "";
            $clinic_fax = "";
            $clinic_id = "";
        }

        if($request->has('title'))
            $doctor->title=$request->title;
        if($request->has('first_name'))
            $doctor->first_name = $request->first_name;
        if($request->has('last_name'))
            $doctor->last_name = $request->last_name;
        if($request->has('email'))
            $doctor->email = $request->email;
        if($request->has('password')){
            if($request->password==$request->confirm_password){
                $doctor->password = Hash::make($request->password);
            }
        }
        if($request->file('picture'))
             $doctor->picture = Helper::upload_picture($request->file('picture'));
        if($request->has('dob'))
            $doctor->dob= $request->dob;

        if($request->has('otp')){
            $otp = Otp::where('phone',$request->phone)->where('country_code',$request->country_code)->where('otp',$request->otp)->first();
            if($otp){
                $doctor->phone = $request->phone;
            }else{
                goto wrongOtp;
            }
        }


        if($request->has('experience'))
            $doctor->experience = $request->experience;
        if($request->has('c_name'))
            $doctor->c_name = $request->c_name;
        if($request->has('c_street'))
            $doctor->c_street= $request->c_street;
        if($request->has('c_city'))
            $doctor->c_city = $request->c_city;
        if($request->has('c_state'))
            $doctor->c_state = $request->c_state;
        if($request->has('c_country'))
            $doctor->c_country = $request->c_country;
        if($request->has('c_postal'))
            $doctor->c_postal = $request->c_postal;
        if($request->has('speciality_id'))
            $doctor->speciality_id = $request->speciality_id;
        if($request->has('med_number'))
            $doctor->med_number = $request->med_number;
        if($request->has('univ1')){
            $doctor->univ1 = $request->univ1;
        }else{
            $doctor->univ1 = "";
        }

        if($request->has('degree1')){
            $doctor->degree1 = $request->degree1;
        }else{
            $doctor->degree1 = "";
        }

        if($request->has('year1')){
            $doctor->year1 = $request->year1;
        }else{
            $doctor->year1 = "";
        }

        if($request->has('univ2')){
            $doctor->univ2 = $request->univ2;
        }else{
            $doctor->univ2 = "";
        }

        if($request->has('currency'))
            $doctor->currency = $request->currency;

        if($request->has('language'))
            $doctor->language = $request->language;

        if($request->has('timezone'))
            $doctor->timezone = $request->timezone;

        if($request->has('degree2')){
            $doctor->degree2 = $request->degree2;
        }else{
            $doctor->degree2 = "";
        }

        if($request->has('year2')){
            $doctor->year2 = $request->year2;
        }else{
            $doctor->year2 = "";
        }

        if($request->has('nationality'))
            $doctor->nationality = $request->nationality;
        if($request->has('country_code'))
            $doctor->country_code = $request->country_code;
        if($request->has('booking_fee')){
            $doctor->booking_fee = $request->booking_fee;
        }else{
            $doctor->booking_fee = 0;
        }



        if($request->has('old_password')){

            if(Hash::check($request->old_password, $doctor->password)){

                if($request->has('new_password')){
                    $doctor->password = Hash::make($request->new_password);
                }

            }else{

                goto passwordWrong;

            }

        }



        if($request->file('c_pic1'))
                {
                    $file_name = time();
                    $file_name .= rand();
                    $file_name = sha1($file_name);
                    $ext = $request->file('c_pic1')->getClientOriginalExtension();
                    $request->file('c_pic1')->move(public_path() . "/uploads", $file_name . "." . $ext);
                    $local_url = $file_name . "." . $ext;

                    // Upload to S3
                    if(\Config::get('app.s3_bucket') != "")
                    {
                        $s3 = \App::make('aws')->createClient('s3');
                        $pic = $s3->putObject(array(
                            'Bucket' => \Config::get('app.s3_bucket'),
                            'Key' => $file_name,
                            'SourceFile' => public_path() . "/uploads/" . $local_url,
                        ));

                        $s3->putObjectAcl(array(
                            'Bucket' => \Config::get('app.s3_bucket'),
                            'Key' => $file_name,
                            'ACL' => 'public-read'
                        ));

                        $s3_url = $s3->getObjectUrl(\Config::get('app.s3_bucket'), $file_name);
                    }
                    else
                    {
                        $s3_url = asset_url().'/uploads/'.$local_url;
                    }
                    $doctor->c_pic1 = $s3_url;
                }

                if($request->file('c_pic2'))
                {
                    $file_name = time();
                    $file_name .= rand();
                    $file_name = sha1($file_name);
                    $ext = $request->file('c_pic2')->getClientOriginalExtension();
                    $request->file('c_pic2')->move(public_path() . "/uploads", $file_name . "." . $ext);
                    $local_url = $file_name . "." . $ext;

                    // Upload to S3
                    if(\Config::get('app.s3_bucket') != "")
                    {
                        $s3 = \App::make('aws')->createClient('s3');
                        $pic = $s3->putObject(array(
                            'Bucket' => \Config::get('app.s3_bucket'),
                            'Key' => $file_name,
                            'SourceFile' => public_path() . "/uploads/" . $local_url,
                        ));

                        $s3->putObjectAcl(array(
                            'Bucket' => \Config::get('app.s3_bucket'),
                            'Key' => $file_name,
                            'ACL' => 'public-read'
                        ));

                        $s3_url = $s3->getObjectUrl(\Config::get('app.s3_bucket'), $file_name);
                    }
                    else
                    {
                        $s3_url = asset_url().'/uploads/'.$local_url;
                    }
                    $doctor->c_pic2 = $s3_url;
                }

                if($request->file('c_pic3'))
                {
                    $file_name = time();
                    $file_name .= rand();
                    $file_name = sha1($file_name);
                    $ext = $request->file('c_pic3')->getClientOriginalExtension();
                    $request->file('c_pic3')->move(public_path() . "/uploads", $file_name . "." . $ext);
                    $local_url = $file_name . "." . $ext;

                    // Upload to S3
                    if(\Config::get('app.s3_bucket') != "")
                    {
                        $s3 = \App::make('aws')->createClient('s3');
                        $pic = $s3->putObject(array(
                            'Bucket' => \Config::get('app.s3_bucket'),
                            'Key' => $file_name,
                            'SourceFile' => public_path() . "/uploads/" . $local_url,
                        ));

                        $s3->putObjectAcl(array(
                            'Bucket' => \Config::get('app.s3_bucket'),
                            'Key' => $file_name,
                            'ACL' => 'public-read'
                        ));

                        $s3_url = $s3->getObjectUrl(\Config::get('app.s3_bucket'), $file_name);
                    }
                    else
                    {
                        $s3_url = asset_url().'/uploads/'.$local_url;
                    }
                    $doctor->c_pic3 = $s3_url;
                }


                $doctor_fee = DoctorFee::where('doctor_id',$id)->first();
                if(!$doctor_fee){
                    $doctor_fee=new DoctorFee;
                    $doctor_fee->doctor_id=$id;
                }
                if($request->has('chat_fee')){
                    $doctor_fee->chat_fee=$request->chat_fee;
                }else{
                    $doctor_fee->chat_fee=0;
                }

                if($request->has('free_minute'))
                {
                    $doctor_fee->free_minute = $request->free_minute;
                }

                if($request->has('phone_fee')){
                    $doctor_fee->phone_fee=$request->phone_fee;
                }else{
                    $doctor_fee->phone_fee=0;
                }

                if($request->has('video_fee')){
                    $doctor_fee->video_fee=$request->video_fee;
                }else{
                    $doctor_fee->video_fee=0;
                }

                if($request->has('ondemand_fee')){
                    $doctor_fee->ondemand_fee=$request->ondemand_fee;
                }else{
                    $doctor_fee->ondemand_fee=0;
                }

                if($request->has('consult_minute'))
                    $doctor_fee->consult_minute = $request->consult_minute;

                if($request->has('minutes_consult_fee'))
                    $doctor_fee->minutes_consult_fee = $request->minutes_consult_fee;



                if($request->has('gender'))
                    $doctor->gender=$request->gender;

                if($request->has('currency'))
                    $doctor->currency=$request->currency;

                $doctor->save();
                $doctor_fee->save();

                $shop = Shop::where('doctor_id',$doctor->id)->first();

                 if($shop){
                    $is_shop = 1;
                }else{
                    $is_shop = '0';
                }



                $speciality = Speciality::find($doctor->speciality_id);
                $speciality = Speciality::find($doctor->speciality_id);
                if($speciality){
                    $speciality_name = $speciality->speciality;
                }else{
                    $speciality_name = "";
                }

                        $response_array = array('success' => true,
                         'id'=>$doctor->id,
                         'title'=>$doctor->title,
                         'first_name'=>$doctor->first_name,
                         'last_name'=>$doctor->last_name,
                         'email'=>$doctor->email,
                         'dob'=>$doctor->dob,
                         'gender'=>$doctor->gender,
                         'picture' => $doctor->picture,
                         'country_code' =>$doctor->country_code,
                         'nationality' =>$doctor->nationality,
                         'session_token'=>$doctor->session_token,
                         'device_token' =>$doctor->device_token,
                         'phone' =>$doctor->phone,
                         'c_name' =>$clinic_name,
                            'c_address'=>$clinic_address,
                         'c_street' =>$doctor->c_street,
                         'c_city' =>$doctor->c_city,
                         'c_state' =>$doctor->c_state,
                         'c_country'=>$doctor->c_country,
                         'c_postal'=>$doctor->c_postal,
                         'c_pic1' => $clinic_image1,
                         'c_pic2' => $clinic_image2,
                         'c_pic3' => $clinic_image3,
                         'experience' => $doctor->experience,
                         'speciality'=>$speciality_name,
                         'speciality_id' => $doctor->speciality_id,
                         'univ1' => $doctor->univ1,
                         'degree1' => $doctor->degree1,
                         'year1' => $doctor->year1,
                         'univ2' => $doctor->univ2,
                         'degree2' => $doctor->degree2,
                         'year2' => $doctor->year2,
                         'med_number' =>$doctor->med_number,
                         'chat_fee' => $doctor_fee->chat_fee,
                         'phone_fee' => $doctor_fee->phone_fee,
                         'video_fee' => $doctor_fee->video_fee,
                         'booking_fee' => $doctor->booking_fee,
                            'ondemand_fee' => $doctor_fee->ondemand_fee,
                            'ssn'=>$doctor->ssn,
                            'is_shop'=>$is_shop,
                            'currency' => $doctor->currency,
                            'language' => $doctor->language,
                            'timezone' => $doctor->timezone,
                            'free_minute' => $doctor_fee->free_minute,
                            'consult_minute' =>$doctor_fee->consult_minute,
                            'minutes_consult_fee' =>$doctor_fee->minutes_consult_fee

                         );
        $response_array = Helper::null_safe($response_array);

        $response = response()->json($response_array, 200);
        return $response;

        passwordWrong:
          $response_array = array( 'success' => false, 'error_message' => 'Please enter the correct password ');
          $response = response()->json($response_array, 200);
          return $response;


        wrongOtp:
        $response_array = array( 'success' => false, 'error_message' => 'Please enter the correct One Time Code');
        $response = response()->json($response_array, 200);
        return $response;
    }

    public function updateDoctorLocation(Request $request)
    {

        $validator = Validator::make(
            $request->all(),
            array(
                'latitude'=>'required',
                'longitude'=>'required',
                ));
        if($validator->fails())
        {
             $error_messages = implode(',',$validator->messages()->all());
             $response_array = array('success' => false, 'error' => Helper::get_error_message(157), 'error_code' => 101, 'error_messages'=> $error_messages);
        }else {
            $doctor = Doctor::find($request->id);
            $doctor->latitude = $request->latitude;
            $doctor->longitude = $request->longitude;
            $doctor->save();
            $response_array = array('success' => true,'id'=>$doctor->id, 'latitude' => $doctor->latitude,'longitude'=>$doctor->longitude);
            $response_array = Helper::null_safe($response_array);

        }
        $response = response()->json($response_array, 200);
        return $response;

    }

    public function getMyArticles(Request $request)
    {

        $articles = Article::where('doctor_id',$request->id)->get();
        $myArticle = array();
        foreach($articles as $article)
        {
            $category = ArticleCategory::find($article->article_category_id);
            $articl['id'] = $article->id;
            $articl['heading'] = $article->heading;
            $articl['picture'] = $article->picture;
            $articl['content'] = $article->content;
            $articl['article_category'] = $category->article_category;
            $articl['article_category_id'] = $article->article_category_id;
            $articl['is_approved'] = $article->is_approved;

            array_push($myArticle,$articl);
        }

        $response_array = array('success' => true ,'articles' => $myArticle);
        //$response_array = Helper::null_safe($response_array);
        $response = response()->json($response_array, 200);
        return $response;

    }

    public function getArticleCategory(Request $request)
    {
        $articleCategories = ArticleCategory::all();
        $category = array();

        foreach($articleCategories as $articleCategory)
        {
            $articleCat['id'] = $articleCategory->id;
            $articleCat['article_category'] = $articleCategory->article_category;
            array_push($category,$articleCat);
        }
        $response_array = array('success' => true ,'article_categories' => $category);
        $response_array = Helper::null_safe($response_array);
        $response = response()->json($response_array, 200);
        return $response;
    }

    public function addArticle(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            array(
                'article_category_id'=>'required|integer',
                'heading'=>'required',
                'picture'=>'required',
                'content'=>'required',
                ));
        if($validator->fails())
        {
             $error_messages = implode(',',$validator->messages()->all());
             $response_array = array('success' => false, 'error' => Helper::get_error_message(158), 'error_code' => 101, 'error_messages'=> $error_messages);
        }else {

            $article = new Article;
            $article->doctor_id = $request->id;
            $article->heading = $request->heading;
            $article->article_category_id = $request->article_category_id;
            $article->content = $request->content;
             // Upload picture
            $article->picture = Helper::upload_picture($request->file('picture'));
            $article->save();

            $response_array = array('success' => true ,
                        'article_id' => $article->id,
                        'doctor_id' => $article->doctor_id,
                        'article_heading' =>$article->heading,
                        'article_picture'=>$article->picture,
                        'article_content'=>$article->content,
                        'article_category_id'=>$article->article_category_id,
                        'is_approved' =>$article->is_approved);
            $response_array = Helper::null_safe($response_array);

        }
        $response = response()->json($response_array, 200);
        return $response;
    }

    public function editArticle(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            array(
                'article_id'=>'required|integer',
                ));
        if($validator->fails())
        {
             $error_messages = implode(',',$validator->messages()->all());
             $response_array = array('success' => false, 'error' => Helper::get_error_message(159), 'error_code' => 101, 'error_messages'=> $error_messages);
        }else {

            $article = Article::find($request->article_id);

            if($article)
            {
                if($request->has('article_category_id'))
                    $article->article_category_id = $request->article_category_id;
                if($request->has('heading'))
                    $article->heading = $request->heading;
                if($request->has('content'))
                    $article->content = $request->content;
                if($request->file('picture'))
                    $article->picture = Helper::upload_picture($request->file('picture'));
                $article->save();
            }
            $response_array = array('success' => true ,
            'article_id' => $article->id,
            'doctor_id' => $article->doctor_id,
            'article_heading' =>$article->heading,
            'article_picture'=>$article->picture,
            'article_content'=>$article->content,
            'article_category_id'=>$article->article_category_id,
                'is_approved' => $article->is_approved);
            $response_array = Helper::null_safe($response_array);


    }
    $response = response()->json($response_array, 200);
    return $response;
}

    public function pushTest(Request $request)
    {

        $doctor = Doctor::find($request->id);
        $deviceToken = $doctor->device_token;

        \PushNotification::app('appNameAndroid')
              ->to($deviceToken)
              ->send('Hello World, i`m a push message');

        $response_array = array('success' => true);

        $response_array = Helper::null_safe($response_array);

        $response = Response::json($response_array, 200);
        return $response;
    }


    public function deleteArticle(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            array(
                'article_id'=>'required|integer',
                ));
        if($validator->fails())
        {
             $error_messages = implode(',',$validator->messages()->all());
             $response_array = array('success' => false, 'error' => Helper::get_error_message(159), 'error_code' => 101, 'error_messages'=> $error_messages);
        }else {

            DB::table('articles')->delete($request->article_id);

            $articles = Article::where('doctor_id',$request->id)->get();
            $myArticle = array();
            foreach($articles as $article)
            {
                $category = ArticleCategory::find($article->article_category_id);
                $articl['id'] = $article->id;
                $articl['heading'] = $article->heading;
                $articl['picture'] = $article->picture;
                $articl['content'] = $article->content;
                $articl['article_category'] = $category->article_category;

                array_push($myArticle,$articl);
            }

            $response_array = array('success' => true,'articles'=>$myArticle);
            //$response_array = Helper::null_safe($response_array);

        }

        $response = Response::json($response_array, 200);
        return $response;
    }

    // Request process -- on demand

    public function service_reject(Request $request)
    {
        $validator = Validator::make(
                $request->all(),
                array(
                    'request_id' => 'required|integer|exists:requests,id',
                ));

        if ($validator->fails()) {

            $error_messages = implode(',', $validator->messages()->all());

            $response_array = array('success' => false, 'error' => $error_messages, 'error_messages' => Helper::get_error_message(101) ,'error_code' => 101);

        } else {
            $doctor = Doctor::find($request->id);
            $request_id = $request->request_id;
            $requests = Requests::find($request_id);
            $user = User::find($requests->user_id);
            //Check whether the request is cancelled by user.
            if($requests->status == REQUEST_CANCELLED) {
                $response_array = array(
                    'success' => false,
                    'error' => Helper::get_error_message(117),
                    'error_code' => 117
                );
            }else {
                // Verify if request was indeed offered to this doctor
                $request_meta = RequestsMeta::where('request_id', '=', $request_id)
                    ->where('doctor_id', '=', $doctor->id)
                    ->where('status', '=', REQUEST_META_OFFERED)->first();

                if (!$request_meta) {
                    // This request has not been offered to this doctor. Abort.
                    $response_array = array(
                        'success' => false,
                        'error' => Helper::get_error_message(135),
                        'error_code' => 135);
                } else {
                    // Decline this offer
                    $request_meta->status = REQUEST_CANCELLED;
                    $request_meta->save();


                    $response_array = Helper::null_safe(array(
                        'success' => true,
                        'id' => $request->id,
                        'request_id' => $request->request_id,
                        'message' => Helper::get_message(118),
                        ));

                    $response_array_push = Helper::null_safe(array(
                        'success' => true,
                        'request_id' => $request->request_id,
                        'status' => REQUEST_REJECTED_BY_DOCTOR,
                        'message' => 'Your request has been rejected',
                        'title' => 'REQUEST REJECTED BY DOCTOR',
                        ));

                    // Check for manual request status
                    // Change status as doctors rejected in request table
                     Requests::where('id', '=', $requests->id)->update( array('status' => REQUEST_REJECTED_BY_DOCTOR) );
                    // Send push notification to user "doctor rejected your request"
                     $deviceToken = $user->device_token;

                    \PushNotification::app('appNameAndroid')
                        ->to($deviceToken)
                        ->send($response_array_push);

                    //Requests::where('id', '=', $requests->id)->delete();
                    RequestsMeta::where('request_id', '=', $requests->id)->delete();
                }
            }

        }

        $response = response()->json($response_array , 200);
        return $response;
    }

    public function service_accept(Request $request)
    {
        $validator = Validator::make(
                $request->all(),
                array(
                    'request_id' => 'required|integer|exists:requests,id',
                    'request_date' =>'',
                    'start_time' => '',
                    'end_time' => '',
                ));

        if ($validator->fails()) {
            $error_messages = implode(',', $validator->messages()->all());
            $response_array = array('success' => false, 'error' => $error_messages, 'error_messages' => Helper::get_error_message(101) ,'error_code' => 101);
        } else {

            $doctor = Doctor::find($request->id);
            $request_id = $request->request_id;
            $requests = Requests::find($request_id);
            //Check whether the request is cancelled by user.
            if($requests->status == REQUEST_CANCELLED) {
                $response_array = array(
                    'success' => false,
                    'error' => Helper::get_error_message(117),
                    'error_code' => 117
                );
            }else{
                // Verify if request was indeed offered to this doctor
                $request_meta = RequestsMeta::where('request_id', '=', $request_id)
                    ->where('doctor_id', '=', $doctor->id)
                    ->where('status', '=', REQUEST_WAITING)->first();

                if (!$request_meta) {
                    // This request has not been offered to this doctor. Abort.
                    $response_array = array(
                        'success' => false,
                        'error' => Helper::get_error_message(149),
                        'error_code' => 149);
                } else {
                    // Accept the offer
                    $doctorConsult = DoctorConsult::where('doctor_id',$request->id)->first();
                    if($requests->request_type == 1){
                        $doctorConsult->chat_consult = 0;
                        $doctor->is_media_online = 0;
                        $requests->doctor_online_status = 1;
                    }elseif($requests->request_type == 2){
                        $doctorConsult->phone_consult = 0;
                        $doctor->is_media_online = 0;
                        $requests->doctor_online_status =2;
                    }elseif($requests->request_type == 3){
                        $doctorConsult->video_consult = 0;
                        $doctor->is_media_online = 0;
                        $requests->doctor_online_status = 3;
                    }elseif ($requests->request_type == 4){
                        $doctorConsult->doc_ondemand = 0;
                        $doctor->is_media_online = 0;
                    }
                    $requests->confirmed_doctor = $doctor->id;
                    $requests->status = REQUEST_INPROGRESS;
                    $requests->doctor_status = DOCTOR_ACCEPTED;
                    $requests->save();

                    $doctorConsult->save();

                    // update is available state
                    // Check the request is later or not and update is available state
                    if($requests->later == 0) {
                        $doctor->is_available = DOCTOR_NOT_AVAILABLE;
                    } else {
                        // Block the doctor availability time before an hour and after 2 hours
                         $check_availability = DoctorAvailability::where('doctor_id',$id)
                                    ->where('available_date',$request->request_date)
                                    ->where('start_time',$request->start_time)
                                    ->where('end_time',$request->end_time)
                                    ->first();
                        if($check_availability) {
                            $check_availability->status = DOCTOR_AVAILABILITY_BOOKED;
                            $check_availability->save();
                        }
                    }
                    $doctor->save();

                    $clinic = Clinic::find($doctor->clinic_id);

                    $clinic_lat = $clinic->c_latitude;
                    $clinic_lng = $clinic->c_longitude;
                    $clinic_address = $clinic->c_address;

                    // Send Push Notification to User
                    $title = "Request Accepted";
                    $message = "Request Accepted Message";

                    // Send pushnotification

                    // No longer need request specific rows from RequestMeta
                    //RequestsMeta::where('request_id', '=', $request_id)->delete();

                    $user = User::find($requests->user_id);

                    $requestData = array(
                        'request_id' => $requests->id,
                        'user_id' => $requests->user_id,
                        'request_type' => $requests->request_type,
                        'request_start_time' =>$request->request_start_time,
                        'start_time' => $requests->start_time,
                        'status' => $requests->status,
                        'doctor_status' => $requests->doctor_status,
                        'request_type' => $requests->request_type,
                        'amount' => $requests->amount,
                        's_longitude' => $requests->s_longitude,
                        's_latitude' => $requests->s_latitude,
                        'is_paid' =>$requests->is_paid,
                        'created_at' =>$requests->created_at,
                        'user_name' => $user->first_name." ".$user->last_name,
                        'user_picture' => $user->picture,
                        'user_mobile' => $user->phone,
                        'user_rating' => round(UserRating::where('user_id', $requests->user_id)->avg('rating'),1) ?: 0,
                        'service_time_diff' => "00:00:00",
                        'unit' => '$',
                        'medicine_fee' => 10,
                        'referral_bonus' => 0,
                        'promo_bonus' => 0,
                    );

                    $invoice_data = array();

                     $inv['doctor_id'] = $request->id;
                     $inv['total_time'] = 0;
                     $inv['payment_mode'] =$requests->payment_mode;
                     $inv['base_price'] = 0;
                     $inv['time_price'] = 0;
                     $inv['tax_price'] = 0;
                     $inv['total'] = 0;
                     $inv['distance'] = 0;

                    $inv['per_km'] = 0;
                    $inv['distance_cost'] = 0;

                     $invoice_data[] = $inv;

                    //Gettin source_address and c_address by google api
                    $json1 = json_decode(file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?latlng='.$requests->s_latitude.','.$requests->s_longitude.'&key=AIzaSyC4AEUI8uxPs42kVRzSUb4B718ZK0hKxew'),true);
                    if(!$json1['results']){
                        $source_address ='';
                    }else{
                        $source_address=$json1['results'][0]['formatted_address'];
                    }

                    $requests->s_address = $source_address;
                    $requests->save();

                    $json2 = json_decode(file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?latlng='.$doctor->latitude.','.$doctor->longitude.'&key=AIzaSyC4AEUI8uxPs42kVRzSUb4B718ZK0hKxew'),true);
                    if(!$json2['results']){
                        $c_address ='';
                    }else{
                        $c_address=$json2['results'][0]['formatted_address'];
                    }

                    //Get Distance time
                    if($requests->s_latitude!=0 && $requests->s_longitude!=0){
                        $distance =Helper::getDistanceTime($doctor->latitude,$doctor->longitude,$requests->s_latitude,$requests->s_longitude);
                        $distArray = explode("$$",$distance);
                        $eta = $distArray[0].' away';
                        $times = $distArray[1];
                    }else{
                        $eta =0;
                        $times =0;
                    }



                    $doctor = Doctor::find($request->id);
                    $doctorData = array(
                        'request_id' => $requests->id,
                        'user_id' => $requests->user_id,
                        'doctor_id' => $request->id,
                        'status' => $requests->status,
                        'doctor_status' => $requests->doctor_status,
                        's_longitude' => $requests->s_longitude,
                        's_latitude' => $requests->s_latitude,
                        'doctor_name' => $doctor->first_name." ".$doctor->last_name,
                        'doctor_picture' => $doctor->picture,
                        'doctor_mobile' => $doctor->phone,
                        'd_latitude' => $doctor->latitude,
                        'd_longitude' => $doctor->longitude,
                        'c_address' =>$c_address,
                        'source_address' => $source_address,
                        'eta' => $eta,
                        'time' => $times,
                        'doctor_rating' => round(DoctorRating::where('doctor_id', $request->id)->avg('rating'),1) ?: 0,
                    );
                    $response_array = Helper::null_safe(array(
                        'success' => true,
                        'title' =>'Doctor Accepted',
                        'data' => $requestData,
                        'invoice' => $invoice_data
                        ));
                    if($requests->request_type==1){
                        $response_array_push = Helper::null_safe(array(
                        'success' => true,
                        'title' =>'Doctor Accepted',
                        'data' => $doctorData,
                        'status' => DOCTOR_ACCEPTED_CHAT,
                        ));

                    }elseif($requests->request_type==2){
                        $response_array_push = Helper::null_safe(array(
                        'success' => true,
                        'title' =>'Doctor Accepted',
                        'data' => $doctorData,
                        'status' => DOCTOR_ACCEPTED_PHONE,
                        ));

                    }elseif($requests->request_type==3){
                        $response_array_push = Helper::null_safe(array(
                        'success' => true,
                        'title' =>'Doctor Accepted',
                        'data' => $doctorData,
                        'status' => DOCTOR_ACCEPTED_VIDEO,
                        ));

                    }else{

                        $response_array_push = Helper::null_safe(array(
                        'success' => true,
                        'title' =>'Doctor Accepted',
                        'data' => $doctorData,
                        'status' => DOCTOR_ACCEPTED,
                        ));

                    }


                    $deviceToken = $user->device_token;

                    if($user->device_type == 'ios'){
                         \PushNotification::app('appNameIOSPatient')
                                ->to($deviceToken)
                                ->send('Doctor has accepted your request');
                    }else{
                        \PushNotification::app('appNameAndroid')
                        ->to($deviceToken)
                        ->send($response_array_push);

                    }

                    
                }
            }
        }

        $response = response()->json($response_array , 200);
        return $response;
    }

    public function doctorstarted(Request $request)
    {
        $doctor = Doctor::find($request->id);

        $validator = Validator::make(
            $request->all(),
            array(
                'request_id' => 'required|integer|exists:requests,id,confirmed_doctor,'.$doctor->id,
            ),
            array(
                'exists' => 'The :attribute doesn\'t belong to doctor:'.$doctor->id
            )
        );

        if ($validator->fails())
        {
            $error_messages = implode(',', $validator->messages()->all());

            $response_array = array('success' => false, 'error' => $error_messages, 'error_messages' => Helper::get_error_message(101) ,'error_code' => 101);
        }
        else
        {
            $request_id = $request->request_id;
            $current_state = DOCTOR_STARTED;

            $check_status = array(REQUEST_CANCELLED,REQUEST_NO_DOCTOR_AVAILABLE,);

            $requests = Requests::where('id', '=', $request_id)
                                ->where('confirmed_doctor', '=', $doctor->id)
                                ->where('doctor_status' , DOCTOR_ACCEPTED)
                                ->where('status', REQUEST_INPROGRESS)
                                ->first();

            // Current state being validated in order to prevent accidental change of state
            if ($requests && intval($requests->doctor_status) != $current_state)
            {
                $requests->status = REQUEST_INPROGRESS;
                $requests->doctor_status = DOCTOR_STARTED;
                $requests->save();

                $new_state = $requests->status;

                $doctor = Doctor::find($request->id);

                $clinic = Clinic::find($doctor->clinic_id);

                $clinic_lat = $clinic->c_latitude;
                $clinic_lng = $clinic->c_longitude;


                //Gettin source_address and c_address by google api
                $json1 = json_decode(file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?latlng='.$requests->s_latitude.','.$requests->s_longitude.'&key=AIzaSyC4AEUI8uxPs42kVRzSUb4B718ZK0hKxew'),true);
                if(!$json1['results']){
                    $source_address ='';
                }else{
                    $source_address=$json1['results'][0]['formatted_address'];
                }

                $json2 = json_decode(file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?latlng='.$clinic_lat.','.$clinic_lng.'&key=AIzaSyC4AEUI8uxPs42kVRzSUb4B718ZK0hKxew'),true);
                if(!$json2['results']){
                    $c_address ='';
                }else{
                    $c_address=$json2['results'][0]['formatted_address'];
                }

                //Get Distance time
                $distance =Helper::getDistanceTime($doctor->latitude,$doctor->longitude,$requests->s_latitude,$requests->s_longitude);
                $distArray = explode("$$",$distance);

                // Send Push Notification to User
                $title = "Doctor Started";
                $message = "Doctor started";
                $user = User::find($requests->user_id);
                // Should add push notification

                 $doctorData = array(
                        'request_id' => $requests->id,
                        'user_id' => $requests->user_id,
                        'doctor_id' => $request->id,
                        'status' => $requests->status,
                        'doctor_status' => $requests->doctor_status,
                        's_longitude' => $requests->s_longitude,
                        's_latitude' => $requests->s_latitude,
                        'doctor_name' => $doctor->first_name." ".$doctor->last_name,
                        'doctor_picture' => $doctor->picture,
                        'doctor_mobile' => $doctor->phone,
                        'd_latitude' => $doctor->latitude,
                        'd_longitude' => $doctor->longitude,
                        'c_address' => $c_address,
                        'source_address' => $source_address,
                        'eta' => $distArray[0].' away',
                        'time' => $distArray[1],
                        'doctor_rating' => DoctorRating::where('doctor_id', $request->id)->avg('rating') ?: 0,
                    );

                $response_array = Helper::null_safe(array(
                        'success' => true,
                        'status' => $new_state,
                        'title' => $title
                ));

                $response_array_push = Helper::null_safe(array(
                        'success' => true,
                        'status' => DOCTOR_STARTED,
                        'title' => $title,
                        'data' => $doctorData
                ));
                $deviceToken = $user->device_token;

                if($user->device_type == 'ios')
                {
                    \PushNotification::app('appNameIOSPatient')
                                ->to($deviceToken)
                                ->send('Doctor has started');

                }else{
                    \PushNotification::app('appNameAndroid')
                        ->to($deviceToken)
                        ->send($response_array_push);

                }
                
            } else {
                $response_array = array('success' => false, 'error' => Helper::get_error_message(145), 'error_code' => 145);
                // Log::info('doctor status Error:: Old state='.$requests->doctor_status.' and current state='.$current_state);
            }
        }

        $response = response()->json($response_array , 200);
        return $response;
    }

    public function arrived(Request $request)
    {
        $doctor = Doctor::find($request->id);
        $validator = Validator::make(
            $request->all(),
            array(
                'request_id' => 'required|integer|exists:requests,id,confirmed_doctor,'.$doctor->id,
            ),
            array(
                'exists' => 'The :attribute doesn\'t belong to doctor:'.$doctor->id
            )
        );

        if ($validator->fails())
        {
            $error_messages = implode(',', $validator->messages()->all());
            $response_array = array('success' => false, 'error' => $error_messages, 'error_messages' => Helper::get_error_message(101) ,'error_code' => 101);
        }
        else
        {

            $request_id = $request->request_id;
            $current_state = DOCTOR_ARRIVED;

            $requests = Requests::where('id', '=', $request_id)
                                ->where('confirmed_doctor', '=', $doctor->id)
                                ->where('doctor_status' , DOCTOR_STARTED)
                                ->where('status', REQUEST_INPROGRESS)
                                ->first();

            // Current state being validated in order to prevent accidental change of state
            if ($requests && intval($requests->doctor_status) != $current_state)
            {
                $requests->status = REQUEST_INPROGRESS;
                $requests->doctor_status = DOCTOR_ARRIVED;
                $requests->save();

                // Send Push Notification to User
                $title = "DOCTOR ARRIVED";
                $message = "DOCTOR ARRIVED";
                $doctor = Doctor::find($request->id);
                $clinic = Clinic::find($doctor->clinic_id);

                $clinic_lat = $clinic->c_latitude;
                $clinic_lng = $clinic->c_longitude;


                //Gettin source_address and c_address by google api
                $json1 = json_decode(file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?latlng='.$requests->s_latitude.','.$requests->s_longitude.'&key=AIzaSyC4AEUI8uxPs42kVRzSUb4B718ZK0hKxew'),true);
                if(!$json1['results']){
                    $source_address ='';
                }else{
                    $source_address=$json1['results'][0]['formatted_address'];
                }

                $json2 = json_decode(file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?latlng='.$clinic_lat.','.$clinic_lng.'&key=AIzaSyC4AEUI8uxPs42kVRzSUb4B718ZK0hKxew'),true);
                if(!$json2['results']){
                    $c_address ='';
                }else{
                    $c_address=$json2['results'][0]['formatted_address'];
                }

                //Get Distance time
                $distance =Helper::getDistanceTime($doctor->latitude,$doctor->longitude,$requests->s_latitude,$requests->s_longitude);
                $distArray = explode("$$",$distance);

                 $doctorData = array(
                        'request_id' => $requests->id,
                        'user_id' => $requests->user_id,
                        'doctor_id' => $request->id,
                        'status' => $requests->status,
                        'doctor_status' => $requests->doctor_status,
                        's_longitude' => $requests->s_longitude,
                        's_latitude' => $requests->s_latitude,
                        'doctor_name' => $doctor->first_name." ".$doctor->last_name,
                        'doctor_picture' => $doctor->picture,
                        'doctor_mobile' => $doctor->phone,
                        'd_latitude' =>$doctor->latitude,
                        'd_longitude' => $doctor->longitude,
                        'c_address' => $c_address,
                        'source_address' => $source_address,
                        'eta' => $distArray[0].' away',
                        'time' => $distArray[1],
                        'doctor_rating' => DoctorRating::where('doctor_id', $request->id)->avg('rating') ?: 0,
                    );

                // Add pushnotification
                $user = User::find($requests->user_id);
                $response_array = Helper::null_safe(array(
                        'success' => true,
                        'status' => REQUEST_INPROGRESS,
                        'title' => "Doctor arrived"
                ));
                $response_array_push = Helper::null_safe(array(
                        'success' => true,
                        'status' => DOCTOR_ARRIVED,
                        'title' => 'DOCTOR_ARRIVED',
                        'data' => $doctorData
                ));

                 $deviceToken = $user->device_token;

                 if($user->device_type == 'ios')
                 {
                     \PushNotification::app('appNameIOSPatient')
                                ->to($deviceToken)
                                ->send('Doctor has arrived');
                 }else{
                      \PushNotification::app('appNameAndroid')
                        ->to($deviceToken)
                        ->send($response_array_push);

                 }
               
            } else {
                $response_array = array('success' => false, 'error' => Helper::get_error_message(146), 'error_code' => 146);
                // Log::info('doctor status Error:: Old state='.$requests->doctor_status.' and current state='.$current_state);
            }
        }

        $response = response()->json($response_array , 200);
        return $response;
    }

    public function servicestarted(Request $request)
    {
        $doctor = Doctor::find($request->id);
        $validator = Validator::make(
            $request->all(),
            array(
                'request_id' => 'required|integer|exists:requests,id,confirmed_doctor,'.$doctor->id,
            ),
            array(
                'exists' => 'The :attribute doesn\'t belong to doctor:'.$doctor->id
            )
        );

        if ($validator->fails())
        {
            $error_messages = implode(',', $validator->messages()->all());
            $response_array = array('success' => false, 'error' => $error_messages, 'error_messages' => Helper::get_error_message(101) ,'error_code' => 101);
        }
        else
        {

            $request_id = $request->request_id;
            $current_state = DOCTOR_SERVICE_STARTED;

            $requests = Requests::where('id', '=', $request_id)
                                ->where('confirmed_doctor', '=', $doctor->id)
                                ->where('doctor_status' , DOCTOR_ARRIVED)
                                ->where('status', REQUEST_INPROGRESS)
                                ->first();
            $check_requests_later = Requests::where('id',$request_id)->where('later',1)->first();

            // Current state being validated in order to prevent accidental change of state
            if ($requests && intval($requests->doctor_status) != $current_state)
            {
                if($request->hasFile('before_image'))
                {
                    $image = $request->file('before_image');
                    $requests->before_image = Helper::upload_picture($image);
                }
                $requests->start_time = date("Y-m-d H:i:s");
                $requests->status = REQUEST_INPROGRESS;
                $requests->doctor_status = DOCTOR_SERVICE_STARTED;
                $requests->save();

                // Send Push Notification to User
                $title = Helper::tr('request_started_title');
                $message = Helper::tr('request_started_message');

                // ADD PUSH NOTIFICATION
                $doctor = Doctor::find($request->id);

                $clinic = Clinic::find($doctor->clinic_id);

                $clinic_lat = $clinic->c_latitude;
                $clinic_lng = $clinic->c_longitude;


                //Gettin source_address and c_address by google api
                $json1 = json_decode(file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?latlng='.$requests->s_latitude.','.$requests->s_longitude.'&key=AIzaSyC4AEUI8uxPs42kVRzSUb4B718ZK0hKxew'),true);
                if(!$json1['results']){
                    $source_address ='';
                }else{
                    $source_address=$json1['results'][0]['formatted_address'];
                }

                $json2 = json_decode(file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?latlng='.$clinic_lat.','.$clinic_lng.'&key=AIzaSyC4AEUI8uxPs42kVRzSUb4B718ZK0hKxew'),true);
                if(!$json2['results']){
                    $c_address ='';
                }else{
                    $c_address=$json2['results'][0]['formatted_address'];
                }

                //Get Distance time
                $distance =Helper::getDistanceTime($doctor->latitude,$doctor->longitude,$requests->s_latitude,$requests->s_longitude);
                $distArray = explode("$$",$distance);


                 $doctorData = array(
                        'request_id' => $requests->id,
                        'user_id' => $requests->user_id,
                        'doctor_id' => $request->id,
                        'status' => $requests->status,
                        'doctor_status' => $requests->doctor_status,
                        's_longitude' => $requests->s_longitude,
                        's_latitude' => $requests->s_latitude,
                        'doctor_name' => $doctor->first_name." ".$doctor->last_name,
                        'doctor_picture' => $doctor->picture,
                        'doctor_mobile' => $doctor->phone,
                        'd_latitude' =>$doctor->latitude,
                        'd_longitude' => $doctor->longitude,
                        'c_address' => $c_address,
                        'source_address' => $source_address,
                        'eta' => $distArray[0].' away',
                        'time' => $distArray[1],
                        'doctor_rating' => DoctorRating::where('doctor_id', $request->id)->avg('rating') ?: 0,
                    );


                $user = User::find($requests->user_id);
                $response_array = Helper::null_safe(array(
                        'success' => true,
                        'request_id' => $request->request_id,
                        'status' => REQUEST_INPROGRESS,
                        'title' => "Treatment started"
                ));

                $response_array_push = Helper::null_safe(array(
                        'success' => true,
                        'request_id' => $request->request_id,
                        'status' => DOCTOR_SERVICE_STARTED,
                        'data' => $doctorData,
                        'title' => 'SERVICE STARTED'
                ));
                 $deviceToken = $user->device_token;
                 if($user->device_type == 'ios')
                 {
                      \PushNotification::app('appNameIOSPatient')
                                ->to($deviceToken)
                                ->send('Treatment started');
                     
                 }else{
                     \PushNotification::app('appNameAndroid')
                        ->to($deviceToken)
                        ->send($response_array_push);

                 }
                
            }
            elseif($check_requests_later->later == 1)
            {
                $requests->start_time = date("Y-m-d H:i:s");
                $requests->status = REQUEST_INPROGRESS;
                $requests->doctor_status = DOCTOR_SERVICE_STARTED;
                $requests->save();

                // ADD PUSH NOTIFICATION
                $user = User::find($requests->user_id);
                $response_array = Helper::null_safe(array(
                        'success' => true,
                        'request_id' => $request->request_id,
                        'status' => REQUEST_INPROGRESS,
                ));
                 $deviceToken = $user->device_token;
                \PushNotification::app('appNameAndroid')
                        ->to($deviceToken)
                        ->send($response_array_push);
            }
            else
            {
                $response_array = array('success' => false, 'error' => Helper::get_error_message(147), 'error_code' => 147);
                Log::info(Helper::get_error_message(147));
            }
        }

        $response = response()->json($response_array , 200);
        return $response;
    }

    public function servicecompleted(Request $request)
    {
        $doctor = Doctor::find($request->id);
        $validator = Validator::make(
            $request->all(),
            array(
                'request_id' => 'required|integer|exists:requests,id,confirmed_doctor,'.$doctor->id,
            ),
            array(
                'exists' => 'The :attribute doesn\'t belong to doctor: '.$doctor->first_name.''.$doctor->last_name
            )
        );

        if ($validator->fails())
        {
            $error_messages = implode(',', $validator->messages()->all());
            $response_array = array('success' => false, 'error' => $error_messages, 'error_messages' => Helper::get_error_message(101) ,'error_code' => 101);
        }  else {

            $request_id = $request->request_id;
            $current_state = DOCTOR_SERVICE_COMPLETED;

            RequestsMeta::where('request_id', '=', $request_id)->delete();

            $doctorConsult = DoctorConsult::where('doctor_id',$request->id)->first();

            $doctorConsult->doc_ondemand = 1;

            $doctorConsult->save();

            $doctor = Doctor::find($request->id);

            $doctor->is_media_online = 1;

            $doctor->save();




            $requests = Requests::where('id', '=', $request_id)
                                ->where('confirmed_doctor', '=', $doctor->id)
                                ->where('doctor_status' , DOCTOR_SERVICE_STARTED)
                                ->where('status', REQUEST_INPROGRESS)
                                ->first();


            $clinic = Clinic::find($doctor->clinic_id);

            $clinic_lat = $clinic->c_latitude;
            $clinic_lng = $clinic->c_longitude;

            //Get Distance time
            $distance =Helper::getDistanceTime($doctor->latitude,$doctor->longitude,$requests->s_latitude,$requests->s_longitude);
            $distArray = explode("$$",$distance);

            $km = explode(' ',$distArray[0]);

            if($km[1] == "m"){
                $kms = 0;
            }else{
                $kms = $km[0];
            }







            // Current state being validated in order to prevent accidental change of state
            if ($requests)
            {

                $user = User::find($requests->user_id);
                $doctor = Doctor::find($requests->confirmed_doctor);
                $doctorFee = DoctorFee::where('doctor_id',$doctor->id)->first();
                $requests->distance = $kms;
                if($doctorFee)
                {
                    $ondemand_fee = $doctorFee->ondemand_fee;
                }else{
                    $ondemand_fee = 0;
                }

                $setting = Setting::find(1);
                if($setting)
                {
                    $fee_km = $setting->fee_km;
                }else{
                    $fee_km = 0;
                }

                $total_amount = $ondemand_fee+($fee_km*$kms);
                $distanceCost = $fee_km*$kms;
                //Currency Conversion

                $currency = $doctor->currency;

                if($currency){
                    $multiplierObj = Swap::latest($currency.'/MYR');
                    $multiplier = $multiplierObj->getValue();
                }else{
                    $multiplier = 1;
                }

                $convertedAmount = $total_amount * $multiplier;

                //currency conversion end



                $requests->amount = $total_amount;
                $requests->real_distance = $distArray[0];

                if($requests->payment_mode == 'cash')
                {
                    $requests->status = WAITING_FOR_DOCTOR_CONFRIMATION_COD;
                    $requests->end_time = date("Y-m-d H:i:s");
                    $requests->doctor_status = DOCTOR_SERVICE_COMPLETED;

                    $requests->save();
                }else{

                    if($ondemand_fee == 0){
                        $requests->status = REQUEST_COMPLETED;
                        $requests->doctor_status = DOCTOR_SERVICE_COMPLETED;
                        $requests->is_paid = 1;
                        $requests->transaction_id = 'na';


                    }else{


                         //BRAINTREE
                    $payment = Payment::where('user_id',$requests->user_id)
                                        ->where('is_default',1)
                                        ->first();
                    $transaction = Helper::createTransaction($payment->customer_id,$requests->id,$convertedAmount);

                    if($transaction != 0){
                         $requests->status = REQUEST_COMPLETED;
                         $requests->doctor_status = DOCTOR_SERVICE_COMPLETED;
                         $requests->is_paid = 1;
                         $requests->transaction_id = $transaction;
                    }else{
                         $requests->status = REQUEST_COMPLETED;
                         $requests->doctor_status = DOCTOR_SERVICE_COMPLETED;
                         $requests->is_paid = 0;

                         //add debt to user debt
                         $debt = new UserDebt;
                         $debt->user_id = $user->id;
                         $debt->request_id = $requests->id;
                         $debt->amount = $total_amount;
                         $debt->save();

                    }

             //Braintree end


                    }






                }
                $requests->save();
                //The availability will be change after the doctor rated for user

                // Initialize variables
                $base_price = $price_per_minute = $tax_price = $total_time = $total_time_price = $total = 0;

                // Invoice details

                //Get base price from admin panel

                $base_price = $ondemand_fee;

                //Get price per minute detials from admin panel

                $price_per_minute = 2;

                // Get the tax details from admin panel

                $tax_price = 3;

                // Get the total time from requests table
                $get_time = Helper::time_diff($requests->start_time,$requests->end_time);
                $total_time = $get_time->i;

                // Calculate price
                $total_time_price = 0;

                $total = $total_amount;
                $user_payment_mode = $requests->payment_mode;
                // get payment mode from user table.

                // card payment details
                // Save the payment details
                if(!RequestPayment::where('request_id' , $requests->id)->first()) {
                    $request_payment = new RequestPayment;
                    $request_payment->request_id = $requests->id;
                    $request_payment->payment_mode = $user_payment_mode;
                    $request_payment->base_price = $base_price;
                    $request_payment->time_price = $total_time_price;
                    $request_payment->tax_price = $tax_price;
                    $request_payment->total_time = $total_time;
                    $request_payment->total = $total;
                    $request_payment->save();
                }

                $request_save = Requests::find($requests->id);
                $request_save->amount = $total;
                $request_save->save();

                $invoice_data = array();



                $requestData = array(
                        'request_id' => $requests->id,
                        'user_id' => $requests->user_id,
                        'request_type' => $requests->request_type,
                        'request_start_time' =>$request->request_start_time,
                        'start_time' => $requests->start_time,
                        'status' => $requests->status,
                        'doctor_status' => $requests->doctor_status,
                        'amount' => $requests->amount,
                        's_longitude' => $requests->s_longitude,
                        's_latitude' => $requests->s_latitude,
                        'is_paid' =>$requests->is_paid,
                        'created_at' =>$requests->created_at,
                        'user_name' => $user->first_name." ".$user->last_name,
                        'user_picture' => $user->picture,
                        'user_mobile' => $user->phone,
                        'user_rating' => round(UserRating::where('user_id', $requests->user_id)->avg('rating'),2) ?: 0,
                        'service_time_diff' => "00:00:00",
                        'unit' => '$',
                        'medicine_fee' => 10,
                        'referral_bonus' => 0,
                        'promo_bonus' => 0,
                    );

                $invoice_data['request_id'] = $requests->id;
                $invoice_data['user_id'] = $requests->user_id;
                $invoice_data['doctor_id'] = $requests->confirmed_doctor;
                $invoice_data['doctor_name'] = $doctor->first_name." ".$doctor->last_name;
                $invoice_data['doctor_address'] = $doctor->address;
                $invoice_data['user_name'] = $user->first_name." ".$user->last_name;
                $invoice_data['user_address'] = $requests->s_address;
                $invoice_data['base_price'] = round($ondemand_fee,2);
                $invoice_data['per_km'] = $fee_km;
                $invoice_data['distance_cost'] = $fee_km*$kms;
                $invoice_data['other_price'] = 0;
                $invoice_data['total_time_price'] = 0;
                $invoice_data['sub_total'] = $ondemand_fee ?:$ondemand_fee;
                $invoice_data['tax_price'] = 0;
                $invoice_data['total'] = round($total,2);
                $invoice_data['distance'] = $distArray[0];
                $invoice_data['payment_mode'] = $requests->payment_mode;
                $invoice_data['payment_mode_status'] = $user_payment_mode ? 1 : 0;
                $invoice_data['bill_no'] = "Not paid";


                     $invoice_dat = array();

                     $inv['doctor_id'] = $request->id;
                     $inv['doctor_name'] = $doctor->first_name." ".$doctor->last_name;
                     $inv['doctor_picture'] = $doctor->picture;
                     $inv['total_time'] = 0;
                     $inv['payment_mode'] =$requests->payment_mode;
                     $inv['base_price'] = $ondemand_fee;
                     $inv['per_km'] = $fee_km;
                     $inv['distance_cost'] = $fee_km*$kms;
                     $inv['time_price'] = 0;
                     $inv['tax_price'] = 0;
                     $inv['total'] = $total;
                     $inv['distance'] = $distArray[0];
                     $inv['medicine_fee'] = 0;
                     $inv['referral_bonus'] = 0;
                     $inv['promo_bonus'] = 0;

                     $invoice_dat[] = $inv;

                //Invoice for user push, seperated because of currency conversion

                $invoice_dat_user = array();

                $invo['doctor_id'] = $request->id;
                $invo['doctor_name'] = $doctor->first_name." ".$doctor->last_name;
                $invo['doctor_picture'] = $doctor->picture;
                $invo['total_time'] = 0;
                $invo['payment_mode'] =$requests->payment_mode;
                $invo['base_price'] = round(Helper::convertMoney($user->id,$doctor->currency,$ondemand_fee),2);
                $invo['per_km'] = round(Helper::convertMoney($user->id,$doctor->currency,$fee_km),2);
                $invo['distance_cost'] = round(Helper::convertMoney($user->id,$doctor->currency,$fee_km*$kms),2);
                $invo['time_price'] = 0;
                $invo['tax_price'] = 0;
                $invo['total'] = round(Helper::convertMoney($user->id,$doctor->currency,$total),2);
                $invo['distance'] = $distArray[0];
                $invo['medicine_fee'] = 0;
                $invo['referral_bonus'] = 0;
                $invo['promo_bonus'] = 0;

                $invoice_dat_user[] = $invo;

                // Send Push Notification to User
                $title = "REQUEST COMPLETED";
                // $message = $invoice_data;
                $message = "REQUEST COMPLETED";

                // ADD PUSH NOTIFICATION

                // Send invoice notification to the user and doctor AS EMAIL
                $user = User::find($requests->user_id);
                //Invoice details to doctor as well
                $response_array = Helper::null_safe(array(
                        'success' => true,
                        'request_id' => $request->request_id,
                        'status' => DOCTOR_SERVICE_COMPLETED,
                        'invoice' => $invoice_dat,
                        'title' => 'SERVICE COMPLETED',
                        'data' => $requestData
                ));

                $response_array_push = Helper::null_safe(array(
                    'success' => true,
                    'request_id' => $request->request_id,
                    'status' => DOCTOR_SERVICE_COMPLETED,
                    'invoice' => $invoice_dat_user,
                    'title' => 'SERVICE COMPLETED',
                    'data' => $requestData
                ));

                $this->dispatch(new SendUserInvoiceEmail($user,$doctor,$requests,$doctorFee,$distanceCost));

                 $deviceToken = $user->device_token;

                 if($user->device_type == 'ios')
                 {
                     \PushNotification::app('appNameIOSPatient')
                                ->to($deviceToken)
                                ->send('Treatment ended');

                 }else{
                      \PushNotification::app('appNameAndroid')
                        ->to($deviceToken)
                        ->send($response_array_push);

                 }
               

            } else {
                $response_array = array('success' => false, 'error' => Helper::get_error_message(148), 'error_code' => 148);
                Log::info(Helper::get_error_message(148));
            }
        }

        $response = response()->json($response_array , 200);
        return $response;
    }

    public function serviceCompletedOnline(Request $request)
    {

        $validator = Validator::make(
            $request->all(),
            array(
                'request_id' => 'required|numeric|exists:requests,id',
                'time' => 'required',
            ));

        if ($validator->fails())
        {
            $error_messages = implode(',', $validator->messages()->all());
            $response_array = array('success' => false, 'error' => $error_messages, 'error_messages' => Helper::get_error_message(101) ,'error_code' => 101);
        }else
        {
            $online_request = Requests::find($request->request_id);
            $time = Helper::hoursToMinutes($request->time);
            //dd($time);
            $user = User::find($online_request->user_id);
            $doctor = Doctor::find($online_request->confirmed_doctor);
            $doctor->is_media_online = 1;
            $doctor->save();
            $doctorConsult = DoctorConsult::where('doctor_id',$request->id)->first();

            RequestsMeta::where('request_id', '=', $request->request_id)->delete();


            if($online_request->request_type == CHAT_CONSULT){
                $doctorFee = DoctorFee::where('doctor_id',$online_request->confirmed_doctor)->first();
                if($doctorFee) {
                    $fee_per_minute = $doctorFee->chat_fee;
                    $base_price = $doctorFee->chat_fee;
                    $consult_minute = $doctorFee->consult_minute;
                    $minutes_consult_fee = $doctorFee->minutes_consult_fee;
                } else{
                    $fee_per_minute = 0;
                    $consult_minute = 1;
                    $minutes_consult_fee = 0;
                    $base_price = 0;
                }

                if($consult_minute == 0){
                    $consult_minute = 1;
                }
                    


                $chargableTime = ($time - $doctorFee->free_minute);

                if($chargableTime<0){

                    $chargableTime = 0;
                    $fee_per_minute = 0;
                }elseif ($chargableTime == 0)
                {
                    $chargableTime = 0;
                    $fee_per_minute = $doctorFee->chat_fee;
                }




                $multiplier = floor($chargableTime/$consult_minute);

                //dd($chargableTime);

                $preTotal = ($minutes_consult_fee*$multiplier);

                $total = $preTotal+$fee_per_minute;

                //dd($total);

                $online_request->doctor_online_status = 4;
                $doctorConsult->chat_consult = 1;
                $online_request->amount = $total;

            }

            if($online_request->request_type == PHONE_CONSULT){
                $doctorFee = DoctorFee::where('doctor_id',$online_request->confirmed_doctor)->first();
                if($doctorFee){
                    $fee_per_minute = $doctorFee->phone_fee;
                    $consult_minute = $doctorFee->consult_minute;
                    $minutes_consult_fee = $doctorFee->minutes_consult_fee;
                    $base_price = $doctorFee->phone_fee;
                }else{
                    $fee_per_minute = 0;
                    $consult_minute = 1;
                    $minutes_consult_fee = 0;
                    $base_price = 0;
                }

                 if($consult_minute == 0){
                    $consult_minute = 1;
                }


                $chargableTime = ($time - $doctorFee->free_minute);

                if($chargableTime<0){
                    $chargableTime = 0;
                    $fee_per_minute = 0;
                }elseif ($chargableTime == 0)
                {
                    $chargableTime = 0;
                    $fee_per_minute = $doctorFee->phone_fee;
                }


                $multiplier = floor($chargableTime/$consult_minute);

                $preTotal = ($minutes_consult_fee*$multiplier);

                $total = $preTotal+$fee_per_minute;

                $online_request->doctor_online_status = 5;

                $doctorConsult->phone_consult = 1;
                $online_request->amount = $total;

            }

            if($online_request->request_type == VIDEO_CONSULT){
                $doctorFee = DoctorFee::where('doctor_id',$online_request->confirmed_doctor)->first();
                if($doctorFee){
                    $fee_per_minute = $doctorFee->video_fee;
                    $consult_minute = $doctorFee->consult_minute;
                    $minutes_consult_fee = $doctorFee->minutes_consult_fee;
                    $base_price = $doctorFee->video_fee;
                }else{
                    $fee_per_minute = 0;
                    $consult_minute = 1;
                    $minutes_consult_fee = 0;
                    $base_price = 0;
                }

                 if($consult_minute == 0){
                    $consult_minute = 1;
                }



                $chargableTime = ($time - $doctorFee->free_minute);


                if($chargableTime<0){

                    $chargableTime = 0;
                    $fee_per_minute = 0;
                }elseif ($chargableTime == 0)
                {
                    $chargableTime = 0;
                    $fee_per_minute = $doctorFee->video_fee;
                }


                $multiplier = floor($chargableTime/$consult_minute);



                $preTotal = ($minutes_consult_fee*$multiplier);

                $total = $preTotal+$fee_per_minute;

                //dd($fee_per_minute);

                $online_request->doctor_online_status = 6;

                $doctorConsult->video_consult = 1;
                $online_request->amount = $total;

            }

            //dd($total);

            $online_request->duration = $time;
            $online_request->end_time = date("Y-m-d H:i:s");
            $online_request->doctor_status = DOCTOR_SERVICE_COMPLETED;
            $doctorConsult->save();

            if($total == 0){

                $online_request->status = REQUEST_COMPLETED;
                $online_request->is_paid = 1;
                $online_request->transaction_id = 'na';



            }else{


                //Currency Conversion

                $currency = $doctor->currency;

                if($currency){
                    $multiplierObj = Swap::latest($currency.'/MYR');
                    $multiplier = $multiplierObj->getValue();
                }else{
                    $multiplier = 1;
                }

                $convertedAmount = $total * $multiplier;

                //currency conversion end



                //BRAINTREE
             $payment = Payment::where('user_id',$online_request->user_id)
                                        ->where('is_default',1)
                                        ->first();
            $transaction = Helper::createTransaction($payment->customer_id,$online_request->id,$convertedAmount);

            if($transaction != 0){
                $online_request->status = REQUEST_COMPLETED;
                $online_request->is_paid = 1;
                $online_request->transaction_id = $transaction;
             }else{
                $online_request->status = REQUEST_COMPLETED;
                $online_request->is_paid = 0;

                //add debt to user debt
                $debt = new UserDebt;
                $debt->user_id = $user->id;
                $debt->request_id = $online_request->id;
                $debt->amount = $total;
                $debt->save();

             }

             //Braintree end



            }



            $online_request->save();




            $requestData = array(
                        'request_id' => $online_request->id,
                        'user_id' => $online_request->user_id,
                        'request_type' => $online_request->request_type,
                        'request_start_time' =>$online_request->request_start_time,
                        'start_time' => $online_request->start_time,
                        'status' => $online_request->status,
                        'doctor_status' => $online_request->doctor_status,
                        'amount' => $online_request->amount,
                        's_longitude' => $online_request->s_longitude,
                        's_latitude' => $online_request->s_latitude,
                        'is_paid' =>$online_request->is_paid,
                        'created_at' =>$online_request->created_at,
                        'user_name' => $user->first_name." ".$user->last_name,
                        'user_picture' => $user->picture,
                        'user_mobile' => $user->phone,
                        'user_rating' => round(UserRating::where('user_id', $online_request->user_id)->avg('rating'),2) ?: 0,
                        'service_time_diff' => "00:00:00",
                        'unit' => '$',
                        'medicine_fee' => 10,
                        'referral_bonus' => 0,
                        'promo_bonus' => 0,
                    );


                     $invoice_dat = array();

                     $inv['doctor_id'] = $request->id;
                     $inv['doctor_name'] = $doctor->first_name." ".$doctor->last_name;
                     $inv['doctor_picture'] = $doctor->picture;
                     $inv['total_time'] = $time;
                     $inv['payment_mode'] ='card';
                     $inv['base_price'] = $base_price;
                     $inv['time_price'] = $base_price;
                     $inv['tax_price'] = 0;
                     $inv['total'] = $total;
                     $inv['distance'] = 0;
                     $inv['medicine_fee'] = 0;
                     $inv['referral_bonus'] = 0;
                     $inv['promo_bonus'] = 0;

                     $invoice_dat[] = $inv;



            $invoice_dat_user = array();

            $invo['doctor_id'] = $request->id;
            $invo['doctor_name'] = $doctor->first_name." ".$doctor->last_name;
            $invo['doctor_picture'] = $doctor->picture;
            $invo['total_time'] = $time;
            $invo['payment_mode'] ='card';
            $invo['base_price'] = round(Helper::convertMoney($user->id,$doctor->currency,$base_price),2);
            $invo['time_price'] = round(Helper::convertMoney($user->id,$doctor->currency,$base_price),2);
            $invo['tax_price'] = 0;
            $invo['total'] = round(Helper::convertMoney($user->id,$doctor->currency,$total),2);
            $invo['distance'] = 0;
            $invo['medicine_fee'] = 0;
            $invo['referral_bonus'] = 0;
            $invo['promo_bonus'] = 0;

            $invoice_dat_user[] = $invo;


                     $title = 'Service Completed';



            $response_array = Helper::null_safe(array(
                        'success' => true,
                        'request_id' => $online_request->id,
                        'status' => DOCTOR_SERVICE_COMPLETED,
                        'invoice' => $invoice_dat,
                        'title' => $title,
                        'data' => $requestData
            ));

            $response_array_push = Helper::null_safe(array(
                'success' => true,
                'request_id' => $online_request->id,
                'status' => DOCTOR_SERVICE_COMPLETED,
                'invoice' => $invoice_dat_user,
                'title' => $title,
                'data' => $requestData
            ));

            $distanceCost = 0;

            $this->dispatch(new SendUserInvoiceEmail($user,$doctor,$online_request,$doctorFee,$distanceCost));

            $deviceToken = $user->device_token;
            if($user->device_type == 'ios')
            {
                \PushNotification::app('appNameIOSPatient')
                                ->to($deviceToken)
                                ->send('Treatment ended');
            }else{
                \PushNotification::app('appNameAndroid')
                ->to($deviceToken)
                ->send($response_array_push);

            }
            




        }

        $response = response()->json($response_array , 200);
        return $response;


    }

    public function rate_user(Request $request)
    {
        $doctor = Doctor::find($request->id);

        $validator = Validator::make(
            $request->all(),
            array(
                'request_id' => 'required|integer|exists:requests,id,confirmed_doctor,'.$doctor->id,
                'rating' => 'required|integer|in:'.RATINGS,
                'comments' => 'max:255'
            ),
            array(
                'exists' => 'The :attribute doesn\'t belong to doctor:'.$doctor->id,
                'unique' => 'The :attribute already rated.'
            )
        );

        if ($validator->fails()) {
            $error_messages = implode(',', $validator->messages()->all());
            $response_array = array('success' => false, 'error' => $error_messages, 'error_messages' => Helper::get_error_message(101) ,'error_code' => 101);
        } else {
            $request_id = $request->request_id;
            $comments = $request->comments;

            $req = Requests::where('id' ,$request_id)
                    ->whereIn('status' , array(REQUEST_COMPLETE_PENDING,REQUEST_RATING,REQUEST_COMPLETED))
                    ->whereIn('doctor_status' , array(DOCTOR_SERVICE_COMPLETED,20))
                    ->first();

            if ($req && intval($req->doctor_status) != DOCTOR_RATED) {

                if($request->has('rating')) {
                    //Save Rating
                    $rev_user = new DoctorRating();
                    $rev_user->doctor_id = $req->confirmed_doctor;
                    $rev_user->user_id = $req->user_id;
                    $rev_user->request_id = $req->id;
                    $rev_user->rating = $request->rating;
                    $rev_user->comment = $comments ?: '';
                    $rev_user->save();
                }

                $req->doctor_status = DOCTOR_RATED;
                $req->status = REQUEST_COMPLETED;
                $req->save();

                //Update doctor availability
                $doctor = Doctor::find($req->confirmed_doctor);
                $doctor->is_available = DOCTOR_AVAILABLE;
                $doctor->save();

                // Send Push notification to user
                $user = User::find($req->user_id);
                $deviceToken = $user->device_token;



                $response_array = Helper::null_safe(array('success' => true,'status' => REQUEST_COMPLETE_PENDING,'message' => Helper::get_message(116)));
            } else {
                $response_array = array('success' => false , 'error' => Helper::get_error_message(150) , 'error_code' => 150);
            }
        }
        return response()->json($response_array , 200);
    }

    public function cancelrequest(Request $request)
    {
        $doctor_id = $request->id;
        $validator = Validator::make(
            $request->all(),
            array(
                'request_id' => 'required|numeric|exists:requests,id,confirmed_doctor,'.$doctor_id,
            ));

        if ($validator->fails())
        {
            $error_messages = implode(',', $validator->messages()->all());
            $response_array = array('success' => false, 'error' => $error_messages, 'error_messages' => Helper::get_error_message(101) ,'error_code' => 101);
        }else
        {
            $request_id = $request->request_id;
            $requests = Requests::find($request_id);
            $requestStatus = $requests->status;
            $doctorStatus = $requests->doctor_status;
            $allowedCancellationStatuses = array(
                DOCTOR_NONE,
                DOCTOR_ACCEPTED,
                DOCTOR_STARTED,
            );

            // Check whether request cancelled previously
            if($requestStatus != REQUEST_CANCELLED)
            {
                // Check whether request eligible for cancellation
                if( in_array($doctorStatus, $allowedCancellationStatuses) )
                {
                    /*Update status of the request to cancellation*/
                    $requests->status = REQUEST_CANCELLED;
                    $requests->save();

                    // Send Push Notification to User
                    $title = "Request Cancelled by DOCTOR";
                    $message = "Request Cancelled by DOCTOR";

                    // Send notifications to the user

                    // Send email notification to the user
                    /*If request has confirmed doctor then release him to available status*/
                    if($requests->confirmed_doctor != DEFAULT_FALSE)
                    {
                        $doctor = Doctor::find( $requests->confirmed_doctor );

                        if($requests->later == 0) {
                            $doctor->is_available = DOCTOR_AVAILABLE;
                        } else {
                            $doctor->is_available = DOCTOR_AVAILABLE;
                            // Block the doctor availability time before an hour and after 2 hours
                             $check_availability = DoctorAvailability::where('doctor_id',$id)
                                        ->where('available_date',$request->request_date)
                                        ->where('start_time',$request->start_time)
                                        ->where('end_time',$request->end_time)
                                        ->first();
                            if($check_availability) {
                                $check_availability->status = DOCTOR_AVAILABILITY_SET;
                                $check_availability->save();
                            }
                        }

                        $doctor->save();
                    }

                    // No longer need request specific rows from RequestMeta
                    RequestsMeta::where('request_id', '=', $request_id)->delete();

                    // Send mail notification

                    $email_data = array();

                    $email_data['doctor_name'] = $email_data['username'] = "";

                     if($user = User::find($requests->user_id)) {
                        $email_data['username'] = $user->first_name." ".$user->last_name;
                    }

                    if($doctor = Doctor::find($requests->confirmed_doctor)) {
                        $email_data['doctor_name'] = $doctor->first_name. " " . $doctor->last_name;
                    }

                    // send email

                    $response_array = Helper::null_safe(array(
                        'success' => true,
                        'request_id' => $request->id,
                        'status' => REQUEST_CANCELLED,
                        'message' => Helper::get_message(117),
                    ));
                } else {
                    $response_array = array( 'success' => false, 'error' => Helper::get_error_message(114), 'error_code' => 114 );
                }
            } else {
                $response_array = array( 'success' => false, 'error' => Helper::get_error_message(113), 'error_code' => 113 );
            }
        }

        $response = response()->json($response_array, 200);
        return $response;
    }

        //  Calendar Functionality
    public function schedule_availability_time(Request $request) {

        $availability = $request->availability;

        $validator = Validator::make($request->all(),
                array('availability' => 'required|json'));

        if($validator->fails()) {
            $error_messages = implode(',', $validator->messages()->all());
            $response_array = array('success' => false,'error' => $error_messages, 'error_code' => 101,'error_messages' => Helper::get_error_message(101));
        } else {

            // Get the limited availability days from admin settings
            // $limit_days_query = Settings::where('key' , 'availability_limit_days')->first();
            // $limit_days = $limit_days_query->value;

            $limit_days = 10;

            // Check the input date is not exceed current date+ 10 days
            $date = strtotime("+".$limit_days." day");
            $check_limit_date = date('Y-m-d', $date);

            $availability = json_decode($availability, true);

            $avail_data = array();

            foreach($availability as $available) {
                // Check the start date is less than the end date
                if(strtotime($available['start']) <= strtotime($available['end'])) {

                    $current_date = Helper::formatDate($available['start']);
                    $start_time = Helper::formatHour($available['start']);
                    $end_time = Helper::formatHour($available['end']);


                    $check_availability = DoctorAvailability::where('doctor_id',$request->id)
                                        ->where('available_date',$current_date)
                                        ->where('start_time',date('H:i', strtotime($start_time)).':00')
                                        ->where('end_time',date('H:i', strtotime($end_time)).':00')
                                        ->first();

                    if(!$check_availability) {
                        $doctor_availablity = new DoctorAvailability;
                        $doctor_availablity->doctor_id = $request->id;
                        $doctor_availablity->start_time = date('H:i', strtotime($start_time)).':00';
                        $doctor_availablity->end_time = date('H:i', strtotime($end_time)).':00';
                        $doctor_availablity->available_date = $current_date;
                        $doctor_availablity->status = DEFAULT_TRUE;
                        $doctor_availablity->save();
                    }

                    $avail_data['start_time'] = date('H:i', strtotime($start_time)).':00';
                    $avail_data['end_time'] = date('H:i', strtotime($end_time)).':00';
                    $avail_data['date'] = $current_date;


                }
            }

            $response_array = array('success' => true , 'data' => $avail_data);
        }
        return response()->json($response_array,200);
    }

    public function get_availabilities(Request $request) {

        $availabilities = DoctorAvailability::where('doctor_id' , $request->id)
                            ->where('status',1)
                            ->where('available_date',$request->date)
                            ->select('doctor_id','id as availability_id' ,'available_date','start_time' ,'end_time' ,'status')
                            ->get();

        $data = array();

        foreach($availabilities as $availability){
            $doctor_data = array();
            $doctor_data['id'] = $availability->availability_id;
            // $doctor_data['title'] = "Available";
            $doctor_data['date'] =  date('Y-m-d',strtotime($availability->available_date));
            $doctor_data['start_time'] = date('H:i:s',strtotime($availability->start_time));
            $doctor_data['end_time'] = date('H:i:s',strtotime($availability->end_time));
            //$doctor_data['start'] = date('Y-m-d',strtotime($availability->available_date)).'T'.date('H:i:s',strtotime($availability->start_time)).'Z';
            //$doctor_data['end'] = date('Y-m-d',strtotime($availability->available_date)).'T'.date('H:i:s',strtotime($availability->end_time)).'Z';
            $doctor_data['allDay'] = false;
            if($availability->status == DOCTOR_AVAILABILITY_BOOKED) {
                $doctor_data['className'] = "booked";
            } else {
                $doctor_data['className'] = "no-booked";
            }

            if($availability->status == DEFAULT_FALSE) {
                $doctor_data['editable'] = false;
            }

            array_push($data, $doctor_data);
        }

        if($availabilities) {
            $response_array = Helper::null_safe(array('success' => true , 'data' => $data));
        } else {
            $response_array = array('success' => false ,'error' => Helper::get_error_message(130) , 'error_code' => 130);
        }

        return response()->json($response_array,200);

    }

    public function delete_availability(Request $request) {

        $validator = Validator::make($request->all() ,
                [
                    'availability_id' => 'required|exists:doctor_availabilities,id,doctor_id,'.$request->id,
                ]);

        if($validator->fails()) {
            $error_messages = implode(',', $validator->messages()->all());
            $response_array = array('success' => false,'error' => $error_messages, 'error_code' => 101,'error_messages' => Helper::get_error_message(101));
        } else {

            if($avail = DoctorAvailability::where('id',$request->availability_id)->first()) {

                if($avail->status != DOCTOR_AVAILABILITY_BOOKED)  {

                    if(DoctorAvailability::where('id',$request->availability_id)->delete()) {
                        $response_array = array('success' => true);
                    } else {
                        $response_array = array('success' => false ,'error' => Helper::get_error_message(155) , 'error_code' => 155);
                    }
                } else {
                    $response_array = array('success' => false ,'error' => Helper::get_error_message(157) , 'error_code' => 157);
                }

            } else {
                $response_array = array('success' => false ,'error' => Helper::get_error_message(157) , 'error_code' => 157);
            }
        }

        return response()->json($response_array,200);

    }


    //Get incoming request new

    public function get_incoming_request_new(Request $request)
    {

        $doctor = Doctor::find($request->id);

        $requests = Requests::where('doctor_id',$request->id)
                            ->where('status','<=',5)
                            ->where('is_cancelled',0)
                            ->orderBy('created_at','desc')
                            ->first();

        $doctor_timeout = 60;

        $invoice_data = array();
        $request_meta_data = array();


            $each_request_meta['user_rating'] = DB::table('user_ratings')->where('user_id', $requests->user_id)->avg('rating') ?: 0;
            $time_left_to_respond = $doctor_timeout - (time() - strtotime($requests->request_start_time) );
            $each_request_meta['time_left_to_respond'] = $time_left_to_respond;

            $each_request_meta['unit'] = '$';
            $each_request_meta['medicine_fee'] = 10;
            $each_request_meta['referral_bonus'] = "";
            $each_request_meta['promo_bonus'] = "";

            array_push($request_meta_data,$each_request_meta);


            $inv['doctor_id'] = $request->id;
            $inv['total_time'] = 0;
            $inv['payment_mode'] ='cash';
            $inv['base_price'] = 0;
            $inv['time_price'] = 0;
            $inv['tax_price'] = 0;
            $inv['total'] = 0;
            $inv['distance'] = 0;

            array_push($invoice_data,$inv);


        $response_array = array(
            'success' => true,
            'is_approved' => $doctor->is_approved,
            'data' => $request_meta_data,
            'invoice' => $invoice_data
        );

        $response = response()->json($response_array, 200);
        return $response;



    }

    // Get incoming requests

    public function get_incoming_request(Request $request)
    {
        $doctor = Doctor::find($request->id);

        // Don't check availability

        $request_meta = RequestsMeta::where('requests_meta.doctor_id',$doctor->id)
                        ->where('requests_meta.status',REQUEST_META_OFFERED)
                        ->where('requests_meta.is_cancelled',0)
                        ->leftJoin('requests', 'requests.id', '=', 'requests_meta.request_id')
                        ->leftJoin('users', 'users.id', '=', 'requests.user_id')
                        ->select('requests.id as request_id', 'requests.request_type as request_type','request_start_time as request_start_time', 'requests.status', 'requests.doctor_status','users.ssn as ssn', 'requests.amount', DB::raw('CONCAT(users.first_name, " ", users.last_name) as user_name'), 'users.picture as user_picture', 'users.nationality as nationality','users.id as user_id','users.phone as user_mobile','users.gender as gender','requests.s_latitude as s_latitude', 'requests.s_longitude as s_longitude','requests.is_paid')
                        ->get()->toArray();

        $count = Requests::where('doctor_id',$request->id)
            ->where('status','=',5)
            ->where('status','!=',6)
            ->count();


        $doctor_timeout = 60;

        $request_meta_data = array();
        $invoice_data = array();
        foreach($request_meta as $each_request_meta){

            $each_request_meta['user_rating'] = DB::table('user_ratings')->where('user_id', $each_request_meta['user_id'])->avg('rating') ?: 0;

            $time_left_to_respond = $doctor_timeout - (time() - strtotime($each_request_meta['request_start_time']) );
            $each_request_meta['time_left_to_respond'] = $time_left_to_respond;
            // Check the time is negative
            $each_request_meta['unit'] = '$';
            $each_request_meta['medicine_fee'] = 10;
            $each_request_meta['referral_bonus'] = "";
            $each_request_meta['promo_bonus'] = "";

            $request_meta_data[] = $each_request_meta;



            $inv['doctor_id'] = $request->id;
            $inv['total_time'] = 0;
            $inv['payment_mode'] ='cash';
            $inv['base_price'] = 0;
            $inv['time_price'] = 0;
            $inv['tax_price'] = 0;
            $inv['total'] = 0;
            $inv['distance'] = 0;

            $inv['base_price'] = 0;
            $inv['per_km'] = 0;
            $inv['distance_cost'] = 0;

            $invoice_data[] = $inv;

        }



        $response_array = array(
                'success' => true,
                'is_approved' => $doctor->is_approved,
                'data' => $request_meta_data,
                'count' => $count,
                'invoice' => $invoice_data
        );

        $response = response()->json($response_array, 200);
        return $response;
    }

    public function request_status_check(Request $request)
    {

        $validator = Validator::make(
            $request->all(),
            array(
                'request_id' => 'required|numeric|exists:requests,id',
            ));

        if ($validator->fails())
        {
            $error_messages = implode(',', $validator->messages()->all());
            $response_array = array('success' => false, 'error' => $error_messages, 'error_messages' => Helper::get_error_message(101) ,'error_code' => 101);
        }else
        {
            $check_status = array(REQUEST_CANCELLED,REQUEST_NO_DOCTOR_AVAILABLE);
            $requests = Requests::where('id',$request->request_id)
                            ->whereNotIn('status', $check_status)
                            ->where('confirmed_doctor','!=',0)
                            ->where('doctor_status','!=',6)->first();

             if($requests){

                $user = User::find($requests->user_id);
                $doctor = Doctor::find($request->id);
                if(!$requests->doctor_online_status)
                {
                    $doctor_online_status = 0;
                }else{
                    $doctor_online_status = $requests->doctor_online_status;
                }

                 $doctorFee = DoctorFee::where('doctor_id',$request->id)->first();
                 if($doctorFee)
                 {
                     if($requests->request_type == 1){
                         $ondemand_fee = $doctorFee->chat_fee;
                     }
                     if($requests->request_type == 2){
                         $ondemand_fee = $doctorFee->phone_fee;
                     }
                     if($requests->request_type == 3){
                         $ondemand_fee = $doctorFee->video_fee;
                     }
                     if($requests->request_type == 4){
                         $ondemand_fee = $doctorFee->ondemand_fee;
                     }
                     if($requests->request_type == 5){
                         $ondemand_fee = $doctor->bookingfee;
                     }

                 }else{
                     $ondemand_fee = 0;
                 }

                 $setting = Setting::find(1);
                 if($setting)
                 {
                     $fee_km = $setting->fee_km;
                 }else{
                     $fee_km = 0;
                 }

                 $distance_price = $fee_km*$requests->distance;
                 $total = $requests->amount;
                 $distance = $requests->real_distance;


            $requestData = array(
                        'request_id' => $requests->id,
                        'user_id' => $requests->user_id,
                        'request_type' => $requests->request_type,
                        'request_start_time' =>$request->request_start_time,
                        'start_time' => $requests->start_time,
                        'status' => $requests->status,
                        'doctor_status' => $requests->doctor_status,
                        'doctor_online_status' => $doctor_online_status,
                        'amount' => $requests->amount,
                        's_longitude' => $requests->s_longitude,
                        's_latitude' => $requests->s_latitude,
                        'is_paid' =>$requests->is_paid,
                        'created_at' =>$requests->created_at,
                        'user_name' => $user->first_name." ".$user->last_name,
                        'user_picture' => $user->picture,
                        'user_gender' => $user->gender,
                        'user_ssn' => $user->ssn,
                        'user_mobile' => $user->phone,
                        'user_rating' => round(UserRating::where('user_id', $requests->user_id)->avg('rating'),1) ?: 0,
                        'service_time_diff' => "00:00:00",
                        'unit' => '$',
                        'medicine_fee' => 10,
                        'referral_bonus' => 0,
                        'promo_bonus' => 0,
                    );

                    $invoice_data = array();

                     $inv['doctor_id'] = $request->id;
                     $inv['total_time'] = $requests->duration;
                     $inv['payment_mode'] = $requests->payment_mode;
                     $inv['base_price'] = $ondemand_fee;
                     $inv['per_km'] = $fee_km;
                     $inv['distance_cost'] = $distance_price;
                     $inv['time_price'] = 0;
                     $inv['tax_price'] = 0;

                     $inv['total'] = $total;
                     $inv['distance'] = $distance;

                     $invoice_data[] = $inv;

                    $response_array = Helper::null_safe(array(
                        'success' => true,
                        'data' => $requestData,
                        'invoice' => $invoice_data
                        ));


             } else{

                $response_array = array(
                        'success' => false,
                        'error_code' => 101
                        );

             }



        }

        $response = response()->json($response_array, 200);
        return $response;



    }


    public function request_status_check_old(Request $request)
    {
        $doctor = Doctor::find($request->id);

        $check_status = array(REQUEST_COMPLETED,REQUEST_CANCELLED,REQUEST_NO_DOCTOR_AVAILABLE);

        $requests = Requests::where('requests.confirmed_doctor', '=', $doctor->id)
                            ->whereNotIn('requests.status', $check_status)
                            ->whereNotIn('requests.doctor_status', array(DOCTOR_RATED))
                            ->orWhere(function($q) use ($doctor) {
                                     $q->where('requests.confirmed_doctor', $doctor->id)
                                        ->where('doctor_status', DOCTOR_SERVICE_COMPLETED)
                                       ->where('requests.status', REQUEST_COMPLETED);
                                 })
                            ->leftJoin('users', 'users.id', '=', 'requests.user_id')
                            ->orderBy('doctor_status','desc')
                            ->select(
                                'requests.id as request_id',
                                'requests.request_type as request_type',
                                'requests.after_image as after_image',
                                'requests.before_image as before_image',
                                'request_start_time as request_start_time',
                                'requests.start_time as start_time',
                                'requests.status', 'requests.doctor_status',
                                'requests.amount',
                                DB::raw('CONCAT(users.first_name, " ", users.last_name) as user_name'),
                                'users.picture as user_picture',
                                'users.phone as user_mobile',
                                'users.id as user_id',
                                'requests.s_latitude',
                                'requests.s_longitude',
                                'requests.is_paid',
                                'requests.created_at'
                            )->get()->toArray();

        $requests_data = array();
        $invoice = array();

        if($requests)
        {
            foreach($requests as $each_request){

                $each_request['user_rating'] = DB::table('user_ratings')->where('user_id', $each_request['user_id'])->avg('rating') ?: 0;
                // This time is used for after service started => In app if the doctor closed the app, while timer is running.

                $each_request['service_time_diff'] = "00:00:00";
                $each_request['unit'] = '$';
                $each_request['medicine_fee'] = 10;
                $each_request['referral_bonus'] = '';
                $each_request['promo_bonus'] = '';

                if($each_request['start_time'] != "0000-00-00 00:00:00") {

                    $time_diff = Helper::time_diff($each_request['start_time'],date('Y-m-d H:i:s'));

                    $each_request['service_time_diff'] = $time_diff->format('%h:%i:%s');

                }
                // unset($each_request['user_id']);
                $requests_data[] = $each_request;


                $allowed_status = array(REQUEST_COMPLETE_PENDING,WAITING_FOR_DOCTOR_CONFRIMATION_COD,REQUEST_COMPLETED,REQUEST_RATING);

                if( in_array($each_request['status'], $allowed_status)) {

                    $user = User::find($each_request['user_id']);

                    $invoice_query = RequestPayment::where('request_id' , $each_request['request_id'])
                                    ->leftJoin('requests' , 'request_payments.request_id' , '=' , 'requests.id')
                                    ->leftJoin('users' , 'requests.user_id' , '=' , 'users.id');
                    // Should add card payment
                    $invoice = $invoice_query->select('requests.confirmed_doctor as doctor_id' , 'request_payments.total_time',
                                        'request_payments.payment_mode as payment_mode' , 'request_payments.base_price',
                                        'request_payments.time_price' , 'request_payments.tax_price' , 'request_payments.total','request_payments.distance')
                                    ->get()->toArray();
                }
            }
        }

        $response_array = array(
            'success' => true,
            'data' => $requests_data,
            'invoice' => $invoice
        );

        $response = response()->json($response_array, 200);
        return $response;
    }

    public function cod_paid_confirmation(Request $request) {


        $validator = Validator::make(
            $request->all(),
            array(
                'request_id' => 'required|integer|exists:requests,id,confirmed_doctor,'.$request->id,
            ),
            array(
                'exists' => 'The :attribute doesn\'t belong to user:'.$request->id,
            )
        );

        if ($validator->fails()) {
            $error_messages = implode(',', $validator->messages()->all());
            $response_array = array('success' => false, 'error' => Helper::get_error_message(101), 'error_code' => 101, 'error_messages'=>$error_messages);

        } else {

            $requests = Requests::find($request->request_id);

            if(($requests->status == WAITING_FOR_DOCTOR_CONFRIMATION_COD && $requests->status != REQUEST_RATING) ||($requests->status==22)) {
                if($requests->status == 22){
                    $requests->status = REQUEST_RATING;
                }else{
                    $requests->status = REQUEST_RATING;
                }

                $requests->is_paid = DEFAULT_TRUE;
                $requests->save();

                 // Send Push Notification to User
                $title = "COD PAYMENT CONFIRMED";
                $message = "COD PAYMENT CONFIRMATION";

                // Send notifications to the user

                $data = array();

                $data['request_id'] = $requests->id;
                $data['user_id'] = $requests->user_id;
                $data['user_name'] = $data['user_picture'] = "";

                 $doctor = Doctor::find($request->id);
                 $doctorData = array(
                        'request_id' => $requests->id,
                        'user_id' => $requests->user_id,
                        'doctor_id' => $request->id,
                        'status' => $requests->status,
                        'doctor_status' => $requests->doctor_status,
                        's_longitude' => $requests->s_longitude,
                        's_latitude' => $requests->s_latitude,
                        'doctor_name' => $doctor->first_name." ".$doctor->last_name,
                        'doctor_picture' => $doctor->picture,
                        'doctor_mobile' => $doctor->phone,
                        'd_latitude' =>$doctor->latitude,
                        'd_longitude' => $doctor->longitude,
                        'doctor_rating' => DoctorRating::where('doctor_id', $request->id)->avg('rating') ?: 0,
                    );


                if($user = User::find($requests->user_id)) {
                    $data['user_name'] = $user->first_name.' '.$user->last_name;
                    $data['user_picture'] = $user->picture;
                }

                $response_array = array('success' => true , 'message' => 'COD payment confirmed' , 'data' => $data);



            } else {
                $response_array = array('success' => false , 'error' => Helper::get_error_message(155) ,'error_code' =>155);
            }

        }

        return response()->json($response_array, 200);
    }



    public function getSlots(Request $request)
    {
        $doctor = Doctor::find($request->id);

        $slots = DoctorSlot::where('speciality_id',$doctor->speciality_id)->get();
        $data = array();

        foreach($slots as $slot){
            $slo['id'] = $slot->id;
            $slo['type'] = $slot->type;
            $slo['start_time'] = $slot->start_time;
            $slo['end_time'] = $slot->end_time;

            array_push($data, $slo);
        }
        $response_array = array(
            'success' => true,
            'slots' => $data,
        );

        $response = response()->json($response_array, 200);
        return $response;

    }


    public function sendSlots(Request $request)
    {
            // dd($request->date);
         $validator = Validator::make(
            $request->all(),
            array(

                'date' => 'required'
            ));

        if ($validator->fails()) {
            $error_messages = implode(',', $validator->messages()->all());
            $response_array = array('success' => false, 'error' =>$error_messages , 'error_code' => 101, 'error_messages'=>$error_messages);

        } else
                {

            $slots = explode(",", $request->slots);

            $doctor = Doctor::find($request->id);
            $doctor->is_booking_online = 0;

            $slots = array_filter($slots);

        if(!empty($slots)){

            $doctor->is_booking_online = 1;
                        if($request->recurring_slots_status)
                        {
                            $get_number_days = explode(",", $request->date);

                            foreach($get_number_days as $get_number_day)
                            {
                                // dd($get_number_days[$i]);
                                $requested_date = date('Y-m-d H:i:s', strtotime($get_number_day));
                    $availslo = AvailableSlot::where('date',$requested_date)
                                             ->where('doctor_id',$request->id)
                                             ->whereNotIn('slot_id',$slots)
                                             ->delete();


                                $updated_date = date('Y-m-d H:i:s', strtotime($get_number_day));
                                $delete_available_slot = AvailableSlot::where('date',$updated_date)->where('doctor_id',$request->id)->where('booked',0)->delete();

                                foreach($slots as $slot)
                                {

                                        $availableSlot = AvailableSlot::where('slot_id',$slot)->where('date',$updated_date)->where('doctor_id',$request->id)->first();
                                        if(!$availableSlot){
                                                $availableSlot = new AvailableSlot;
                                                $availableSlot->slot_id = $slot;
                                                $availableSlot->doctor_id = $request->id;

                                                $updated_date = date('Y-m-d H:i:s', strtotime($updated_date));
                                                // dd($updated_date);
                                                $availableSlot->date = $updated_date;
                                                $availableSlot->slot_status = 1;
                                        }
                                        $availableSlot->save();
                                }
                            }
                        }
                        else { 
                            $requested_date = date('Y-m-d H:i:s', strtotime($request->date));
                            $availslo = AvailableSlot::where('date',$requested_date)
                                                                             ->where('doctor_id',$request->id)
                                                                             ->where('booked', 0)
                                                                             ->delete();
                                                                             //->whereNotIn('slot_id',$slots)
                                                                           
                            foreach($slots as $slot)
                            {
                                    $availableSlot = AvailableSlot::where('slot_id',$slot)->where('date',$requested_date)->where('doctor_id',$request->id)->first();

                                    if(!$availableSlot){
                                            $availableSlot = new AvailableSlot;
                                            $availableSlot->slot_id = $slot;
                                            $availableSlot->doctor_id = $request->id;
                                            $updated_date = date('Y-m-d H:i:s', strtotime($request->date));
                                            $availableSlot->date = $updated_date;
                                            $availableSlot->slot_status = 1;
                                    }
                                    $availableSlot->save();

                            }
                        }

            //Response to show the slots

            $availableSlots = AvailableSlot::where('doctor_id',$request->id)
                                    ->get();



            $doctorSlotArray = array();

            foreach($availableSlots as $availableSlot){


                $doctorSlot = DoctorSlot::find($availableSlot->slot_id);

                if($doctorSlot){


                    $slotData['id'] =  $doctorSlot->id;
                    $slotData['type'] = $doctorSlot->type;
                    $slotData['start_time'] = $doctorSlot->start_time;
                    $slotData['end_time'] = $doctorSlot->end_time;
                    $slotData['date'] = $availableSlot->date;




                }
                array_push($doctorSlotArray, $slotData);
            }





            //till here


            $response_array = array('success' => true,
                'slots' =>$doctorSlotArray);
        }else{

            $get_number_days = explode(",", $request->date);

            foreach($get_number_days as $get_number_day)
            {

                $requested_date = date('Y-m-d H:i:s', strtotime($get_number_day));
                AvailableSlot::where('date',$requested_date)
                    ->where('doctor_id',$request->id)
                    ->whereNotIn('slot_id',$slots)
                    ->delete();

                $availableSlots = AvailableSlot::where('doctor_id',$request->id)
                    ->get();



                $doctorSlotArray = array();

                foreach($availableSlots as $availableSlot){

                    $doctorSlot = DoctorSlot::find($availableSlot->slot_id);

                    if($doctorSlot){


                        $slotData['id'] =  $doctorSlot->id;
                        $slotData['type'] = $doctorSlot->type;
                        $slotData['start_time'] = $doctorSlot->start_time;
                        $slotData['end_time'] = $doctorSlot->end_time;
                        $slotData['date'] = $availableSlot->date;
                        



                    }else{
                        $slotData="";
                    }
                    array_push($doctorSlotArray, $slotData);
                }

            }



            $response_array = array('success' => true,
                'slots' =>$doctorSlotArray);
        }
        $doctor->save();
        }//else ends here

        $response = response()->json($response_array, 200);
        return $response;

    }



    public function getDoctorSlots(Request $request)
    {

        $validator = Validator::make(
            $request->all(),
            array(
                'date' => 'required'
            ));

        if ($validator->fails()) {
            $error_messages = implode(',', $validator->messages()->all());
            $response_array = array('success' => false, 'error' =>$error_messages , 'error_code' => 101, 'error_messages'=>$error_messages);

        } else {

//            $requestDate = date('Y-m-d 00:00:00',strtotime($request->date));
//            $testDate = date('Y-m-d H:i:s');
//            dd($requestDate==$testDate);

            $availableSlots = AvailableSlot::where('doctor_id',$request->id)
                                    ->whereDate('date','=',$request->date)
                                    ->where('slot_status',1)
                                    ->get();

            $doctorSlotArray = array();

            foreach($availableSlots as $availableSlot){

                $doctorSlot = DoctorSlot::find($availableSlot->slot_id);

                if($doctorSlot){


                    $slotData['id'] =  $doctorSlot->id;
                    $slotData['type'] = $doctorSlot->type;
                    $slotData['start_time'] = $doctorSlot->start_time;
                    $slotData['end_time'] = $doctorSlot->end_time;
                    $slotData['booked'] = $availableSlot->booked;


                    array_push($doctorSlotArray, $slotData);

                }

            }

            $response_array = array(
            'success' => true,
            'slots' => $doctorSlotArray,
             );


        }

        $response = response()->json($response_array, 200);
        return $response;

    }

    public function mySchedule(Request $request)
    {

        $validator = Validator::make(
            $request->all(),
            array(
                'date' => 'required'
            ));

        if ($validator->fails()) {
            $error_messages = implode(',', $validator->messages()->all());
            $response_array = array('success' => false, 'error' =>$error_messages , 'error_code' => 101, 'error_messages'=>$error_messages);

        } else {

                $availableSlots = AvailableSlot::whereDate('date','=',$request->date)
                                        ->where('doctor_id',$request->id)
                                        ->where('booked',1)
                                        ->get();

                $requests_array = array();

                foreach ($availableSlots as $availableSlot) {

                    $requests = Requests::where('id',$availableSlot->request_id)
                                        ->first();
                    $doctorSlot = DoctorSlot::find($availableSlot->slot_id);
                    if($doctorSlot->type == 'm'){
                        $ampm = 'am';

                    }else{
                        $ampm = 'pm';
                    }
                    if($availableSlot->slot_status==0)
                    {
                        $is_completed = 0;
                    }elseif($availableSlot->slot_status==2){
                        $is_completed = 1;
                    }else{
                        $is_completed = 0;

                    }
                    //dd($requests->user_id);
                    $user = User::find($requests->user_id);

                    $doctorSlot = DoctorSlot::find($availableSlot->slot_id);

                    $request_info['request_id'] = $requests->id;
                    $request_info['available_slot_id'] = $availableSlot->id;
                    $request_info['user_name'] = $user->first_name." ".$user->last_name;
                    $request_info['s_latitude'] = $requests->s_latitude;
                    $request_info['s_longitude'] = $requests->s_longitude;
                    $request_info['start_time'] = $doctorSlot->start_time;
                    $request_info['ampm'] = $ampm;
                    $request_info['end_time'] = $doctorSlot->end_time;
                    $request_info['payment_mode'] = $requests->payment_mode;
                    $request_info['date'] = date('Y-m-d',strtotime($availableSlot->date));
                    $request_info['is_completed'] = $is_completed;

                    $json = json_decode(file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?latlng='.$requests->s_latitude.','.$requests->s_longitude.'&key=AIzaSyC4AEUI8uxPs42kVRzSUb4B718ZK0hKxew'),true);

                     if(!$json['results']){
                        $address ='';
                    }else{
                        $address=$json['results'][0]['formatted_address'];
                    }

                    $request_info['address'] = $address;


                    array_push($requests_array, $request_info);



                }

                $response_array = array(
                'success' => true,
                'data' => $requests_array,
                );


        }
        $response = response()->json($response_array, 200);
        return $response;

    }

    public function getScheduleDates(Request $request)
    {
        $date = new DateTime(date('Y-m-d H:i:s'));
        $date->setTimezone(new \DateTimeZone(env('TIMEZONE')));

        $availableSlots = DB::table('available_slots')
                                ->join('requests','available_slots.request_id','=','requests.id')
                                ->select('available_slots.slot_id as slot_id','available_slots.date as date')
                                ->where('available_slots.doctor_id',$request->id)
                                ->where('available_slots.booked','=',1)
                                ->where('requests.status','=',20)
                                ->where('available_slots.date','>=',$date->format('Y-m-d'))
                                ->get();
        $slotArray = array();

        foreach($availableSlots as $availableSlot)
        {
            $slot['slot_id'] = $availableSlot->slot_id;
            $slot['date'] = date('Y-m-d',strtotime($availableSlot->date));
            array_push($slotArray, $slot);
        }

        $response_array = array(
                'success' => true,
                'data' => $slotArray,
                );

        $response = response()->json($response_array, 200);
        return $response;

    }

    public function endBooking(Request $request)
    {

         $validator = Validator::make(
            $request->all(),
            array(
                'request_id' => 'required'
            ));

        if ($validator->fails()) {
            $error_messages = implode(',', $validator->messages()->all());
            $response_array = array('success' => false, 'error' =>$error_messages , 'error_code' => 101, 'error_messages'=>$error_messages);

        } else {


            $requests = Requests::where('id',$request->request_id)
                            ->where('status',20)
                            ->first();
            if($requests){

                $doctor = Doctor::find($request->id);
                $user = User::find($requests->user_id);

                $availableSlot = AvailableSlot::where('request_id',$request->request_id)->first();

                $requests->amount = $doctor->booking_fee;
                if($requests->payment_mode == 'cash'){
                    $requests->status = 22;

                }else{

                    if($doctor->booking_fee == 0){
                        $requests->status = REQUEST_RATING;
                        $requests->is_paid = 0;
                        $requests->transaction_id ='na';


                    }else{

                        //Currency Conversion

                        $currency = $doctor->currency;

                        if($currency){
                            $multiplierObj = Swap::latest($currency.'/MYR');
                            $multiplier = $multiplierObj->getValue();
                        }else{
                            $multiplier = 1;
                        }

                        $convertedAmount = $doctor->booking_fee * $multiplier;

                        //currency conversion end



                        // BRAINTREE


                    $payment = Payment::where('user_id',$requests->user_id)
                                        ->where('is_default',1)
                                        ->first();

                    $transaction = Helper::createTransaction($payment->customer_id,$requests->id,$convertedAmount);

                    if($transaction != 0){
                        $requests->status = REQUEST_RATING;
                        $requests->is_paid = 1;
                        $requests->transaction_id = $transaction;
                    }else{
                        $requests->status = REQUEST_RATING;
                        $requests->is_paid = 0;

                        //add debt to user debt
                        $debt = new UserDebt;
                        $debt->user_id = $user->id;
                        $debt->request_id = $requests->id;
                        $debt->amount = $doctor->booking_fee;
                        $debt->save();

                    }



                    }





                }//Braintree End

                $availableSlot->slot_status = 2;
                $availableSlot->booked = 1;
                //$availableSlot->request_id = 0;
                $requests->save();
                $availableSlot->save();

                     $invoice_dat = array();
                     $invoice_dat_push = array();


                     $inv['doctor_id'] = $request->id;
                     $inv['doctor_name'] = $doctor->first_name." ".$doctor->last_name;
                     $inv['doctor_picture'] = $doctor->picture;
                     $inv['total_time'] = 0;
                     $inv['payment_mode'] =$requests->payment_mode;
                     $inv['base_price'] = $doctor->booking_fee;
                     $inv['time_price'] = 0;
                     $inv['tax_price'] = 0;
                     $inv['total'] = $doctor->booking_fee;;
                     $inv['distance'] = 0;
                     $inv['medicine_fee'] = 0;
                     $inv['referral_bonus'] = 0;
                     $inv['promo_bonus'] = 0;

                     $inv_push['doctor_id'] = $request->id;
                     $inv_push['doctor_name'] = $doctor->first_name." ".$doctor->last_name;
                     $inv_push['doctor_picture'] = $doctor->picture;
                     $inv_push['total_time'] = 0;
                     $inv_push['payment_mode'] =$requests->payment_mode;
                     $inv_push['base_price'] = round(Helper::convertMoney($user->id,$doctor->currency,$doctor->booking_fee),2);
                     $inv_push['time_price'] = 0;
                     $inv_push['tax_price'] = 0;
                     $inv_push['total'] = round(Helper::convertMoney($user->id,$doctor->currency,$doctor->booking_fee),2);
                     $inv_push['distance'] = 0;
                     $inv_push['medicine_fee'] = 0;
                     $inv_push['referral_bonus'] = 0;
                     $inv_push['promo_bonus'] = 0;

                     $invoice_dat[] = $inv;
                     $invoice_dat_push[] = $inv_push;



                      $requestData = array(
                        'request_id' => $requests->id,
                        'user_id' => $requests->user_id,
                        'request_type' => $requests->request_type,
                        'request_start_time' =>$request->start_time,
                        'start_time' => $requests->start_time,
                        'status' => $requests->status,
                        'doctor_status' => $requests->doctor_status,
                        'amount' => $requests->amount,
                        's_longitude' => $requests->s_longitude,
                        's_latitude' => $requests->s_latitude,
                        'is_paid' =>$requests->is_paid,
                        'created_at' =>$requests->created_at,
                        'user_name' => $user->first_name." ".$user->last_name,
                        'user_picture' => $user->picture,
                        'user_mobile' => $user->phone,
                        'user_rating' => round(UserRating::where('user_id', $requests->user_id)->avg('rating'),2) ?: 0,
                        'service_time_diff' => "00:00:00",
                        'unit' => '$',
                        'medicine_fee' => 10,
                        'referral_bonus' => 0,
                        'promo_bonus' => 0,
                    );

                     $response_array = array(
                        'success' => true,
                        'invoice' => $invoice_dat,
                        'data' => $requestData,
                        'request_id' => $requests->id,
                        'status' => DOCTOR_SERVICE_COMPLETED,
                        'title' =>'Booking Accepted'
                );

                     $response_array_push = array(
                        'success' => true,
                        'invoice' => $invoice_dat_push,
                        'data' => $requestData,
                        'request_id' => $requests->id,
                        'status' => 30,
                        'title' =>'Your appointment is confirmed'
                );

                $distanceCost =0;
                $doctorFee =DoctorFee::where('doctor_id',$request->id)->first();


                $this->dispatch(new SendUserInvoiceEmail($user,$doctor,$requests,$doctorFee,$distanceCost));

                     $deviceToken = $user->device_token;
                     if($user->device_type == 'ios')
                     {
                         \PushNotification::app('appNameIOSPatient')
                                ->to($deviceToken)
                                ->send('Appointment accepted');
                     }else{
                        \PushNotification::app('appNameAndroid')
                            ->to($deviceToken)
                            ->send($response_array_push);
                     }
                    

            } else{

                $response_array = array(
                        'success' => false,
                        'error' => 'request id wrong',
                );

            }



        }

        $response = response()->json($response_array, 200);
        return $response;


    }

    public function consultHistory(Request $request)
    {

        if(!$request->has('skip'))
            $skip = 0;
        else
            $skip = $request->skip;

        if(!$request->has('take'))
            $take = 1000;
        else
            $take = $request->take;

        $requests = Requests::whereIn('request_type',array(1,2,3))
                            ->where('status',5)
                            ->where('confirmed_doctor',$request->id)
                            ->skip($skip)
                            ->take($take)
                            ->get();

        $requestArray = array();

        foreach($requests as $key)
        {
            if($key->request_type == 1){
                $type = 'chat';
            }elseif($key->request_type == 2){
                $type = 'phone';
            }elseif($key->request_type == 3){
                $type = 'video';
            }else{
                $type = 'undefined';
            }

            $user = User::find($key->user_id);
            $info['id'] = $key->id;
            $info['type'] = $type;
            $info['user_name'] = $user->first_name." ".$user->last_name;
            $info['user_picture'] = $user->picture;
            $info['phone'] = $user->phone;
            $time = Helper::formatHour($key->created_at);
            $date = Helper::formatDate($key->created_at);
            $info['date'] = $date;
            $info['time'] = $time;
            $info['duration'] = $key->duration;
            $info['fee'] = $key->amount;

            array_push($requestArray, $info);
        }

        $response_array = array(
            'success' => true,
            'data' => $requestArray,
        );

        $response = response()->json($response_array, 200);
        return $response;

    }

    public function chatConsultHistory(Request $request)
    {
        if(!$request->has('skip'))
            $skip = 0;
        else
            $skip = $request->skip;

        if(!$request->has('take'))
            $take = 10000;
        else
            $take = $request->take;

        $requests = Requests::whereIn('request_type',array(1,2,3))
            ->where('status',5)
            ->where('confirmed_doctor',$request->id)
            ->where('request_type',1)
            ->skip($skip)
            ->take($take)
            ->get();

        $requestArray = array();

        foreach($requests as $key)
        {

            $user = User::find($key->user_id);
            $info['id'] = $key->id;
            $info['type'] = "chat";
            $info['user_name'] = $user->first_name." ".$user->last_name;
            $info['user_picture'] = $user->picture;
            $info['phone'] = $user->phone;
            $time = Helper::formatHour($key->created_at);
            $date = Helper::formatDate($key->created_at);
            $info['date'] = $date;
            $info['time'] = $time;
            $info['duration'] = $key->duration;
            $info['fee'] = $key->amount;

            array_push($requestArray, $info);
        }

        $response_array = array(
            'success' => true,
            'data' => $requestArray,
        );

        $response = response()->json($response_array, 200);
        return $response;

    }

    public function phoneConsultHistory(Request $request)
    {
        if(!$request->has('skip'))
            $skip = 0;
        else
            $skip = $request->skip;

        if(!$request->has('take'))
            $take = 10000;
        else
            $take = $request->take;

        $requests = Requests::whereIn('request_type',array(1,2,3))
            ->where('status',5)
            ->where('confirmed_doctor',$request->id)
            ->where('request_type',2)
            ->skip($skip)
            ->take($take)
            ->get();

        $requestArray = array();

        foreach($requests as $key)
        {

            $user = User::find($key->user_id);
            $info['id'] = $key->id;
            $info['type'] = "phone";
            $info['user_name'] = $user->first_name." ".$user->last_name;
            $info['user_picture'] = $user->picture;
            $info['phone'] = $user->phone;
            $time = Helper::formatHour($key->created_at);
            $date = Helper::formatDate($key->created_at);
            $info['date'] = $date;
            $info['time'] = $time;
            $info['duration'] = $key->duration;
            $info['fee'] = $key->amount;

            array_push($requestArray, $info);
        }

        $response_array = array(
            'success' => true,
            'data' => $requestArray,
        );

        $response = response()->json($response_array, 200);
        return $response;

    }

    public function videoConsultHistory(Request $request)
    {
        if(!$request->has('skip'))
            $skip = 0;
        else
            $skip = $request->skip;

        if(!$request->has('take'))
            $take = 10000;
        else
            $take = $request->take;

        $requests = Requests::whereIn('request_type',array(1,2,3))
            ->where('status',5)
            ->where('confirmed_doctor',$request->id)
            ->where('request_type',3)
            ->skip($skip)
            ->take($take)
            ->get();

        $requestArray = array();

        foreach($requests as $key)
        {

            $user = User::find($key->user_id);
            $info['id'] = $key->id;
            $info['type'] = "video";
            $info['user_name'] = $user->first_name." ".$user->last_name;
            $info['user_picture'] = $user->picture;
            $info['phone'] = $user->phone;
            $time = Helper::formatHour($key->created_at);
            $date = Helper::formatDate($key->created_at);
            $info['date'] = $date;
            $info['time'] = $time;
            $info['duration'] = $key->duration;
            $info['fee'] = $key->amount;

            array_push($requestArray, $info);
        }

        $response_array = array(
            'success' => true,
            'data' => $requestArray,
        );

        $response = response()->json($response_array, 200);
        return $response;

    }

    public function reportIssue(Request $request)
    {

        $validator = Validator::make(
            $request->all(),
            array(
                'content' => 'required'
            ));

        if ($validator->fails()) {
            $error_messages = implode(',', $validator->messages()->all());
            $response_array = array('success' => false, 'error' =>$error_messages , 'error_code' => 101, 'error_messages'=>$error_messages);

        } else {

            $doctorIssue = new DoctorIssue;
            $doctorIssue->doctor_id = $request->id;
            $doctorIssue->content = $request->content;
            if($request->file('picture'))
            $doctorIssue->picture = Helper::upload_picture($request->file('picture'));
            $doctorIssue->save();

            $response_array = array(
            'success' => true,
            'message' => 'Your issue has been send successfully',
        );


        }

        $response = response()->json($response_array, 200);
        return $response;

    }



    public function generateOtp(Request $request)
    {
         $validator = Validator::make(
            $request->all(),
            array(
                'phone' => 'required',
                'country_code' => 'required'
            ));

        if ($validator->fails()) {
            $error_messages = implode(',', $validator->messages()->all());
            $response_array = array('success' => false, 'error_message' =>$error_messages , 'error_code' => 101, 'error_messages'=>$error_messages);

        } else {
            $c_code = $request->country_code;
            $country_code = str_replace(")", "(", $c_code);

            $country = explode('(', $country_code);

            $accountSid = env('TWILIO_SID');
            $authToken = env('TWILIO_TOKEN');
            $twilioNumber = env('TWILIO_FROM');


            $otp_number = rand(1000,9999);

            $otp = Otp::where('country_code',$request->country_code)
                        ->where('phone',$request->phone)
                        ->first();

            if(!$otp){
             $otp = new Otp;
            }

            $otp->country_code = $request->country_code;
            $otp->phone = $request->phone;
            $otp->otp = $otp_number;
            $otp->save();

            $phone = $country[1].$request->phone;
            $message = 'Your One Time Code is '.$otp_number;

            $client = new Services_Twilio($accountSid, $authToken);

            try{
                $m = $client->account->messages->sendMessage(
                $twilioNumber, // the text will be sent from your Twilio number
                $phone, // the phone number the text will be sent to
                $message // the body of the text message
            );


            }catch(Services_Twilio_RestException $e){
                return $e;

            }

            $response_array = array('success' => true);



        }
         $response = response()->json($response_array, 200);
         return $response;


    }


    public function getQuestion(Request $request)
    {

        if(!$request->has('skip'))
            $skip = 0;
        else
            $skip = $request->skip;

        if(!$request->has('take'))
            $take = 1000;
        else
            $take = $request->take;

        $questions = Question::where('doctor_id',$request->id)->skip($skip)
                                    ->take($take)->get();

        $count = Question::where('doctor_id',$request->id)->where('is_answered',0)->count();


        $questionArray = array();

        foreach($questions as $question){
            $user = User::find($question->user_id);

            $date = new DateTime($question->created_at->format('Y-m-d H:i'));
            $date->setTimezone(new \DateTimeZone(env('TIMEZONE')));


            $info['id'] = $question->id;
            $info['user_name'] = $user->first_name." ".$user->last_name;
            $info['subject'] = $question->subject;
            $info['content'] = $question->content;
            $info['answer'] = $question->answer;
            $info['time'] = $date->format('h:ia');
            $info['date'] = $date->format('Y-m-d');


            array_push($questionArray, $info);

        }
        $response_array = array('success' => true ,'count'=>$count,'questionArray' => $questionArray);

        $response = response()->json($response_array, 200);
        return $response;

    }


    public function answerQuestion(Request $request)
    {

        $validator = Validator::make(
            $request->all(),
            array(
                'question_id' => 'required',
                'answer' => 'required'
            ));

        if ($validator->fails()) {
            $error_messages = implode(',', $validator->messages()->all());
            $response_array = array('success' => false, 'error_message' =>$error_messages , 'error_code' => 101, 'error_messages'=>$error_messages);

        } else {

            $question = Question::find($request->question_id);

            if($question){
                $question->answer = $request->answer;
                $question->is_answered = 1;
                $question->save();

                $response_array = array('success' => true );

                $doctor = Doctor::find($request->id);
                $doctor_name = $doctor->first_name." ".$doctor->last_name;

                $user = User::find($question->user_id);
                $deviceToken = $user->device_token;

                $response_array_push = Helper::null_safe(array(
                    'success' => true,
                    'status' => 99,
                    'message' => 'Your question has been answered by Dr.'.$doctor_name,
                    'title' => 'Your question has been answered by Dr.'.$doctor_name,
                ));

                \PushNotification::app('appNameAndroid')
                    ->to($deviceToken)
                    ->send($response_array_push);

            }


        }

        $response = response()->json($response_array, 200);
        return $response;


    }


    public function viewAnswers(Request $request)
    {

        $questions = Question::where('doctor_id',$request->id)->where('is_answered',1)->get();

        $questionArray = array();

        foreach($questions as $question)
        {
            $user = User::find($question->user_id);

            $date = new DateTime($question->created_at->format('Y-m-d H:i'));
            $date->setTimezone(new \DateTimeZone(env('TIMEZONE')));

            $info['id'] = $question->id;
            $info['subject'] = $question->subject;
            $info['content'] = $question->content;
            $info['answer'] = $question->answer;
            $info['time'] = $date->format('h:ia');
            $info['date'] = $date->format('Y-m-d');
            $info['user_name'] = $user->first_name." ".$user->last_name;

            array_push($questionArray, $info);
        }

        $response_array = array('success' => true ,'questionArray' => $questionArray);
        $response = response()->json($response_array, 200);
        return $response;


    }


    public function generateOpentok(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            array(
                'request_id' => 'required',
            ));

        if ($validator->fails()) {
            $error_messages = implode(',', $validator->messages()->all());
            $response_array = array('success' => false, 'error_message' =>$error_messages , 'error_code' => 101, 'error_messages'=>$error_messages);

        } else {

            $requests = Requests::find($request->request_id);

            if(!$requests->session && !$requests->token){

                // new session
                $session = OpentokApi::createSession();
                $sessionId = $session->getSessionId();

                // check if it's been created or not (could have failed)
                if (empty($sessionId)) {
                    throw new \Exception("An open tok session could not be created");
                }

                // then create a token (session created in previous step)
                try {
                    // note we're create a publisher token here, for subscriber tokens we would specify.. yep 'subscriber' instead
                    $token = OpentokApi::generateToken($sessionId);
                } catch (OpenTokException $e) {
                    // do something here for failure
                }

                $requests->session = $sessionId;
                $requests->token = $token;

                $requests->save();


            }else{

                $sessionId = $requests->session;
                $token = $requests->token;

            }


            $response_array = array('success' => true ,'session_id' => $sessionId,'token' => $token);
        }


        $response = response()->json($response_array, 200);
        return $response;


    }


    public function uploadDocuments(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            array(
                'picture' => 'required',
            ));

        if ($validator->fails()) {
            $error_messages = implode(',', $validator->messages()->all());
            $response_array = array('success' => false, 'error_message' =>$error_messages , 'error_code' => 101, 'error_messages'=>$error_messages);

        } else {
            $document = new Document;
            $document->doctor_id = $request->id;
            $document->picture = Helper::upload_picture($request->file('picture'));
            $document->save();

            $response_array = array('success' => true ,'message' => 'Document added successfully');

        }

        $response = response()->json($response_array, 200);
        return $response;

    }

    public function changePassword(Request $request)
    {

        $validator = Validator::make(
            $request->all(),
            array(
                'old_password' => 'required',
                'new_password' => 'required'
            ));

        if ($validator->fails()) {
            $error_messages = implode(',', $validator->messages()->all());
            $response_array = array('success' => false, 'error_message' =>$error_messages , 'error_code' => 101, 'error_messages'=>$error_messages);

        } else {

            $doctor = Doctor::find($request->id);

            if (Hash::check($request->old_password, $doctor->password)) {

                    $doctor->password = Hash::make($request->new_password);

                    $doctor->save();

                    $response_array = array('success' => true, 'message' => 'Password changed successfully');


            } else {

                $response_array = array('success' => false, 'error_message' => 'Current password is wrong');

            }

        }

        $response = response()->json($response_array, 200);
        return $response;

    }

    public function logout(Request $request)
    {
        $doctor = Doctor::find($request->id);
        $doctorConsult = DoctorConsult::where('doctor_id',$request->id)->first();

        $doctorConsult->phone_consult = 0;
        $doctorConsult->chat_consult = 0;
        $doctorConsult->video_consult = 0;
        $doctorConsult->doc_ondemand = 0;
        $doctorConsult->is_online = 0;


        $doctorConsult->save();

        //$doctor->device_token = 'device_token';
        $doctor->is_media_online = 0;
        $doctor->is_available = 0;
        $doctor->save();

        $response_array = array('success' => true, 'message' => 'Logged out successfully');
        $response = response()->json($response_array, 200);
        return $response;

    }

    public function checkToken(Request $request)
    {
        $response_array = array('success' => true ,'message' =>'Token Valid');
        $response = response()->json($response_array, 200);
        return $response;
    }

    public function getChatHistory(Request $request)
    {
        $completedRequests = Requests::where('status',5)->where('request_type',1)->get();

        $requestArray = array();

        foreach ($completedRequests as $completedRequest)
        {
            $user = User::find($completedRequest->user_id);

            $info['request_id'] = $completedRequest->id;
            $info['user_id'] = $completedRequest->user_id;
            $info['user_name'] = $user->first_name." ".$user->last_name;
            $info['user_image'] = $user->picture;


            array_push($requestArray,$info);
        }

        $response_array = array('success' => true, 'requestArray' => $requestArray);
        $response = response()->json($response_array, 200);
        return $response;

    }

        public function recurring_slot_cron()
    {
            $recurring_slots = 0;
            $remaining_slots = 0;
            $current_date = date("Y-m-d");
            $get_recurring_details = RecurringSlot::whereDate('cron_date','=',$current_date)->where('status',1)->get();

            if($get_recurring_details)
            {

                foreach($get_recurring_details as $detail)
                {
                    //Change recurring days and time
                    $change_recurring_days = RecurringSlot::find($detail->id);

                        if($detail->days < 10)
                        {
                            $recurring_slots = $detail->days;
                            $remaining_slots = 0;
                            $change_recurring_days->status = 0;
                        }
                        else {
                            $recurring_slots = 10;
                            $remaining_slots = $detail->days - $recurring_slots;

                            $change_recurring_days->cron_date = date('Y-m-d H:i:s', strtotime($detail->cron_date . " +$recurring_slots day"));
                            $change_recurring_days->status = 1;
                        }

                        $change_recurring_days->days = $remaining_slots;
                        $change_recurring_days->save();

                        $slots = explode(",", $detail->slots_id);

                        for($i = 0; $i< $recurring_slots; $i++)
                        {
                            $updated_date = date('Y-m-d H:i:s', strtotime($detail->cron_date . " +$i day"));
                            // $delete_available_slot = AvailableSlot::where('date',$updated_date)->where('doctor_id',$request->id)->delete();

                            foreach($slots as $slot)
                            {
                                    $availableSlot = AvailableSlot::where('slot_id',$slot)->where('date',$updated_date)->where('doctor_id',$detail->doctor_id)->first();
                                    if(!$availableSlot){
                                            $availableSlot = new AvailableSlot;
                                            $availableSlot->slot_id = $slot;
                                            $availableSlot->doctor_id = $detail->doctor_id;

                                            $updated_date = date('Y-m-d H:i:s', strtotime($detail->cron_date . " +$i day"));
                                            // dd($updated_date);
                                            $availableSlot->date = $updated_date;
                                            $availableSlot->slot_status = 1;
                                    }
                                    $availableSlot->save();
                            }
                        }
                }
            }
            else {
                // do nothing
            }
            // Delete unwanted slots
            $delete_recurring_slots = RecurringSlot::where('status',0)->delete();
            return "success";
    }

    public function saveChatMessage(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            array(
                'request_id' => 'required',
                'message' => 'required'
            ));

        if ($validator->fails()) {
            $error_messages = implode(',', $validator->messages()->all());
            $response_array = array('success' => false, 'error_message' =>$error_messages , 'error_code' => 101, 'error_messages'=>$error_messages);

        } else {


            $chatMessage = new ChatMessage;

            $chatMessage->request_id = $request->request_id;
            $chatMessage->message = $request->message;
            $chatMessage->type = $request->type;
            $chatMessage->data_type = $request->data_type;
            $chatMessage->save();

            $response_array = array('success' => true, 'request_id' =>$chatMessage->request_id,'message' => $chatMessage->message );

        }

        $response = response()->json($response_array, 200);
        return $response;

    }

    public function getChatMessage(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            array(
                'request_id' => 'required',
            ));

        if ($validator->fails()) {
            $error_messages = implode(',', $validator->messages()->all());
            $response_array = array('success' => false, 'error_message' =>$error_messages , 'error_code' => 101, 'error_messages'=>$error_messages);

        } else {

            $chatMessages = ChatMessage::where('request_id',$request->request_id)->get();

            $chatArray = array();

            foreach ($chatMessages as $chatMessage)
            {

                $info['message'] = $chatMessage->message;
                $info['type'] = $chatMessage->type;
                $info['data_type'] = $chatMessage->data_type;

                array_push($chatArray,$info);

            }

            $response_array = array('success' => true, 'request_id' =>$request->request_id,'chatArray' =>$chatArray );


        }

        $response = response()->json($response_array, 200);
        return $response;

    }

    public function getPatientsFav(Request $request)
    {
        $favorites = FavoriteDoctor::where('doctor_id',$request->id)->get();

        $userArray = array();

        foreach ($favorites as $favorite)
        {
            $user = User::find($favorite->user_id);
            $info['user_id'] = $user->id;
            $info['user_name'] = $user->first_name.' '.$user->last_name;
            $info['phone'] = $user->phone;
            $info['email'] = $user->email;
            $info['dob'] = $user->dob;
            $info['gender'] = $user->gender;
            $info['picture'] = $user->picture;
            $info['nationality'] = $user->nationality;

            array_push($userArray,$info);


        }
        $response_array = array('success' => true, 'userArray' =>$userArray);

        $response = response()->json($response_array, 200);
        return $response;

    }

    public function sendMessageToUser(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            array(
                'message' => 'required',
                'user_id' => 'required'
            ));

        if ($validator->fails()) {
            $error_messages = implode(',', $validator->messages()->all());
            $response_array = array('success' => false, 'error_message' =>$error_messages , 'error_code' => 101, 'error_messages'=>$error_messages);

        } else {

                $doctorMessage = new DoctorMessage;
                $doctorMessage->doctor_id = $request->id;
                $doctorMessage->user_id = $request->user_id;
                $doctorMessage->sent = $request->message;

                $doctorMessage->save();

            $response_array = array('success' => true, 'message' =>'Message Sent succesfully');

            $response_array_push = array(
                'success' => true,
                'title' =>'Doctor messaged you',
                'message'=>$request->message,
                'doctor_id' => $request->id,
                'status' => 567
            );

        }
        $user = User::find($request->user_id);
        $deviceToken = $user->device_token;

        \PushNotification::app('appNameAndroid')
            ->to($deviceToken)
            ->send($response_array_push);

        $response = response()->json($response_array, 200);
        return $response;
    }

    public function getMessageList(Request $request)
    {
        $doctorMessages = DoctorMessage::where('doctor_id',$request->id)->get();

        $messageArray = array();

        foreach ($doctorMessages as $doctorMessage) {
            $user = User::find($doctorMessage->user_id);
            $info['id'] = $doctorMessage->id;
            $info['user_id'] = $user->id;
            $info['user_name'] = $user->first_name.' '.$user->last_name;
            $info['phone'] = $user->phone;
            $info['email'] = $user->email;
            $info['dob'] = $user->dob;
            $info['gender'] = $user->gender;
            $info['picture'] = $user->picture;
            $info['nationality'] = $user->nationality;


            $info['sent'] = $doctorMessage->sent;
            $info['reply'] = $doctorMessage->reply;

            array_push($messageArray,$info);
        }

        $response_array = array(
            'success' => true,
            'messageArray' => $messageArray
        );

        $response = response()->json($response_array, 200);
        return $response;
    }

    public function getRequests(Request $request)
    {

        $requestMeta = RequestsMeta::where('doctor_id',$request->id)
                                    ->where('is_cancelled',0)
                                    ->first();


        $info_data = array();
        $user_data = array();
        $invoice_data = array();

        if($requestMeta)
        { 
            $requests = Requests::find($requestMeta->request_id);

            $user = User::find($requests->user_id);
            $doctor = Doctor::find($request->id);

            

            $user_info['request_id'] = $requests->id;
            $user_info['user_id'] = $user->id;
            $user_info['user_name'] = $user->first_name . " " . $user->last_name;
            $user_info['user_mobile'] = $user->phone;
            $user_info['user_email'] = $user->email;
            $user_info['user_dob'] = $user->dob;
            $user_info['user_gender'] = $user->gender;
            $user_info['user_picture'] = $user->picture;
            $user_info['user_title'] = $user->title;
            $user_info['user_street'] = $user->b_street;
            $user_info['user_city'] = $user->b_city;
            $user_info['user_state'] = $user->b_state;
            $user_info['user_country'] = $user->b_country;
            $user_info['user_postal'] = $user->b_postal;
            $user_info['user_country'] = $user->b_country;
            $user_info['request_id'] = $requests->id;
            $user_info['s_latitude'] = $requests->s_latitude;
            $user_info['s_longitude'] = $requests->s_longitude;
            $user_info['request_type'] = $requests->request_type;
            $user_info['doctor_status'] = DOCTOR_NONE;
            $user_info['promo_bonus'] = 0;
            $user_info['referral_bonus'] = 0;
            $user_info['unit'] = '$';



            $info['user_id'] = $user->id;
            $info['request_id'] = $requests->id;
            $info['doctor_id'] = $doctor->id;
            $info['doctor_name'] = $doctor->first_name . " " . $doctor->last_name;
            $info['doctor_picture'] = $doctor->picture;
            $rating = DB::table('user_ratings')->where('doctor_id', $doctor->id)->avg('rating') ?: 0;
            $info['doctor_rating'] = round($rating, 2);
            $info['s_latitude'] = $requests->s_latitude;
            $info['s_longitude'] = $requests->s_longitude;
            $info['d_latitude'] = $doctor->latitude;
            $info['d_longitude'] = $doctor->longitude;
            $info['request_type'] = $requests->request_type;

            $info_data [] = $info;

            $user_data[] = $user_info;

            

            $inv['doctor_id'] = $request->id;
            $inv['total_time'] = 0;
            $inv['payment_mode'] = $requests->payment_mode;
            $inv['base_price'] = 0;
            $inv['time_price'] = 0;
            $inv['tax_price'] = 0;
            $inv['total'] = 0;
            $inv['distance'] = 0;

            $invoice_data[] = $inv;
        } 

         $response_array = array('success' => true,
                                'data' => $user_data,
                                'invoice' => $invoice_data,
                                'title' => 'new requests'
                            );     

        $response = response()->json($response_array, 200);
        return $response;                                          
        

    }

    public function cancelAppointment(Request $request)
    {

        $validator = Validator::make(
            $request->all(),
            array(
                'available_slot_id' => 'required'
            ));

        if ($validator->fails()) {
            $error_messages = implode(',', $validator->messages()->all());
            $response_array = array('success' => false, 'error' =>$error_messages , 'error_code' => 101, 'error_messages'=>$error_messages);

        } else {

            $availableSlot = AvailableSlot::find($request->available_slot_id);

            if($availableSlot)
            {
                $requests = Requests::find($availableSlot->request_id);
                $user = User::find($requests->user_id);
                $deviceToken = $user->device_token;
                $requests->is_booking_cancelled = 1;
                $requests->save();

                $availableSlot->slot_status = 1;
                $availableSlot->booked = 0;
                $availableSlot->request_id = 0;
                $availableSlot->save();

                $doctor = Doctor::find($request->id);

                $response_array = array(
                    'success' => true,
                    'message' => "Appoinment Cancelled"
                );

                $response_array_push = array(
                'success' => true,
                'title' =>'Your booking has been cancelled by'.$doctor->first_name.' '.$doctor->last_name,
                'doctor_id' => $request->id,
                'status' => 700
                 );

                \PushNotification::app('appNameAndroid')
                    ->to($deviceToken)
                    ->send($response_array_push);

        
            }else{

                $response_array = array(
                    'success' => false,
                    'message' => "somethingwent wrong!!"
                );

            }

           


        }

         $response = response()->json($response_array, 200);
        return $response;

    }

}
