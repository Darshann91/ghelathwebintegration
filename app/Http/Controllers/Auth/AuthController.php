<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Auth;


class AuthController extends Controller
{

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    protected $redirectTo = '/user/home';
    protected $guard = 'web';
    protected $loginView = 'home.signin';
    protected $registerView = '';


    public function __construct()
    {
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    }




    protected function validator(array $data, &$errors = [])
    {
        $validator = Validator::make($data, [
            'email'    => 'required|email|max:255',
            'password' => 'required|min:6',
        ]);

        if($validator->fails()) {
            $errors = $this->formatErrors($validator->errors()->getMessages());
            return false;
        } 

        return true;
    }



    protected function formatErrors($errors)
    {
        $temp = [];
        foreach($errors as $errorKey => $errorVal) {
            $temp[$errorKey] = $errorVal[0];
        }
        return $temp;
    }



    /* user login post route */
    public function login(Request $request)
    {
        if(!$this->validator($request->all(), $errors)) {
            return response()->json(
                $this->jsonResponseFormat(false, 'VALIDATION_ERROR', 'Validation error', $errors)
            );
        }


        $credentials = ['email' => $request->email, 'password' => $request->password];

        if (Auth::attempt($credentials, false)) {           
            return response()->json(
                $this->jsonResponseFormat(true, 'LOGGED_IN_SUCCESS', 'You logged in successfully', [
                    'redirect_url' => url('user/home')
                ])
            ); 
        } 
    
        return $this->jsonResponseFormat(false, 'LOGIN_FAILED', 'Email or password is invalid');

    }




    public function logout()
    {
        Auth::guard('web')->logout();
        return redirect('login');
    }



    protected function jsonResponseFormat($isSuccess, $type, $text, $data = [])
    {
        if($isSuccess) {

            return [
                'success' => true,
                'success_type' => $type,
                'success_text' => $text,
                'success_data' => $data 
            ];

        } else {

            return [
                'success' => false,
                'error_type' => $type,
                'error_text' => $text,
                'error_data' => $data 
            ];

        }
    }


}
