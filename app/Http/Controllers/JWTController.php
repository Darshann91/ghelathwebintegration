<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\JWT;
use Auth;

class JWTController extends Controller
{

    protected $authTypes = ['user', 'doctor', 'admin'];

    public function createWebJWTSocketToken(Request $request)
    {

        if(!$this->verifyAuthtype($request->token_for)) {
            return response()->json(
                $this->jsonFormatResponse(false, 'INVALID_TOKEN_FOR', '')
            );
        }

        $this->authUserByTokenFor($request->token_for);
        
        $payload = [];
        $payload['id']    = $this->authID();
        $payload['email'] = $this->authEmail();
        $payload['type']  = $request->token_for;

        $token = $this->generateToken($payload);

        return response()->json(
            $this->jsonFormatResponse(true, 'TOKEN_GENERATED', 'Token generated', ['token' => $token])
        );
    }



    protected function getPayload()
    {
        

        return $payload;
    }



    protected function generateToken($payload)
    {
        try {
            return JWT::encode($payload, config('jwt.secret_key'));
        } catch(\Exception $e) {
            return "";
        }
    }





    protected function authID()
    {
        return $this->getAuthProperty('id');
    }


    protected function authEmail()
    {
        return $this->getAuthProperty('email');
    }


    protected function verifyAuthtype($authType)
    {
        return in_array($authType, $this->authTypes);
    }


    protected function getAuthProperty($type)
    {
        return isset($this->auth->$type) ? $this->auth->$type : null;
    }



    protected function authUserByTokenFor($tokenFor)
    {
        switch ($tokenFor) {
            
            case 'user':
                $this->auth = $this->getAuthUserByTypeGuard('web');
                break;

            case 'doctor':
                $this->auth = $this->getAuthUserByTypeGuard('doctor-web');
                break;

            case 'admin':
                $this->auth = $this->getAuthUserByTypeGuard('admin');
                break;
            
            default:
                $this->auth = null;
                break;
        }
        
        return $this->auth;
    }



    protected function getAuthUserByTypeGuard($guard)
    {
        return Auth::user($guard);
    }


    protected function jsonFormatResponse($isSuccess, $type, $message, $data = [])
    {
        if($isSuccess) {

            return [
                'success' => true,
                'success_type' => $type,
                'success_text' => $message,
                'success_data' => empty($data) ? "" : $data,
            ];

        } else {
            return [
                'success' => false,
                'error_type' => $type,
                'error_text' => $message,
                'error_data' => empty($data) ? "" : $data
            ];
        }
    }

}