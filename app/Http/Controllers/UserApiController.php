<?php

namespace App\Http\Controllers;


use App\Clinic;
use App\DoctorMessage;
use App\FavoriteDoctor;
use App\ShippingAddress;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use Curl;
use Hash;
use Illuminate\Http\Request;
use League\Flysystem\Exception;
use PhpParser\Comment\Doc;
use Swap\Laravel\Facades\Swap;
use Validator;
use Log;
use Response;
use Twilio;
use Mail;
use App\Jobs\SendVerificationEmail;
use App\Jobs\SendForgotPassword;
use Apns;

use Twilio\Rest\Client;

use Services_Twilio;

use Tomcorbett\OpentokLaravel\Facades\OpentokApi;
use OpenTok\Role;
use OpenTokException;


use Braintree_Transaction;
use Braintree_Customer;
use Braintree_WebhookNotification;
use Braintree_Subscription;
use Braintree_CreditCard;
use Braintree_PaymentMethod;
use Braintree_ClientToken;


use App\User;
use App\Speciality;
use App\Doctor;
use App\DoctorConsult;
use App\DoctorFee;
use App\DoctorTime;
use DB;
use App\Consult;
use DateTime;
use App\Helpers\Helper;
use App\Requests;
use App\RequestsMeta;
use App\Article;
use App\ArticleCategory;
use App\UserRating;
use App\DoctorRating;
use App\DoctorAvailability;
use App\AvailableSlot;
use App\DoctorSlot;
use App\Payment;
use App\UserDebt;
use App\UserIssue;
use App\ArticleBookmark;
use App\Otp;
use App\Question;
use App\Setting;
use App\Attachment;
use App\CurrencyMultiplier;

define('USER', 0);

define('doctor', 1);

define('NONE', 0);

define('DEFAULT_FALSE', 0);
define('DEFAULT_TRUE', 1);

define('DEVICE_ANDROID', 'android');
define('DEVICE_IOS', 'ios');

// Payment Constants

define('COD', 'cod');
define('PAYPAL', 'paypal');
define('CARD', 'card');

// Request table status
define('REQUEST_NEW', 0);
define('REQUEST_WAITING', 1);
define('REQUEST_INPROGRESS', 2);
define('REQUEST_COMPLETE_PENDING', 3);
define('WAITING_FOR_DOCTOR_CONFRIMATION_COD', 8);
define('REQUEST_RATING', 4);
define('REQUEST_COMPLETED', 5);
define('REQUEST_CANCELLED', 6);
define('REQUEST_NO_DOCTOR_AVAILABLE', 7);


//Only when manual request
define('REQUEST_REJECTED_BY_DOCTOR', 9);

define('doctor_NOT_AVAILABLE', 0);
define('doctor_AVAILABLE', 1);

// Request table doctor_status

define('DOCTOR_NONE', 0);
define('DOCTOR_ACCEPTED', 1);
define('DOCTOR_STARTED', 2);
define('DOCTOR_ARRIVED', 3);
define('DOCTOR_SERVICE_STARTED', 4);
define('DOCTOR_SERVICE_COMPLETED', 5);
define('DOCTOR_RATED', 6);
define('DOCTOR_ACCEPTED_CHAT', 10);
define('DOCTOR_ACCEPTED_PHONE', 11);
define('DOCTOR_ACCEPTED_VIDEO', 12);
define('DOCTOR_ENDED_CHAT', 13);
define('DOCTOR_ENDED_PHONE', 14);
define('DOCTOR_ENDED_VIDEO', 15);

//doctor availability
define('DOCTOR_AVAILABILITY_FREE', 0);
define('DOCTOR_AVAILABILITY_SET', 1);
define('DOCTOR_AVAILABILITY_BOOKED', 2);


define('REQUEST_META_NONE', 0);
define('REQUEST_META_OFFERED', 1);
define('REQUEST_META_TIMEDOUT', 2);
define('REQUEST_META_DECLINED', 3);

define('WAITING_TO_RESPOND', 1);
define('WAITING_TO_RESPOND_NORMAL', 0);

define('RATINGS', '0,1,2,3,4,5');

// Consult/Service types

define('CHAT_CONSULT', 1);
define('PHONE_CONSULT', 2);
define('VIDEO_CONSULT', 3);
define('ONDEMAND_CONSULT', 4);
define('BOOKING', 5);


class UserApiController extends Controller
{


    public function __construct(Request $request)
    {
        $this->middleware('UserApiVal', array('except' => ['forgetPassword','iosPushTest','verifyEmail','uploadAttachment','register', 'login', 'forgot_password', 'generateOtp', 'testTwilio', 'cronNotification','currencyMultiplierCron','forgotPassword']));
    }

    //User Register API
    public function register_old(Request $request)
    {
         $response_array = array('success' => false, 'error_message' => 'We are sorry but registration is not currently supported at your region', 'error_code' => 105);

        $response = response()->json($response_array, 200);
        return $response;

    }

    public function register_new(Request $request)
    {
        
    }

    public function register(Request $request)
    {
        //Temporary Country Validation -- start

        if($request->has('current_country')){
            if(strtolower($request->current_country) != 'malaysia')
            {
                $response_array = array('success' => false, 'error_message' => 'We are sorry but registration is not currently supported at your region', 'error_code' => 105);

                $response = response()->json($response_array, 200);
                return $response;

            }
        }

        //Temporary Country Validation -- end

        $response_array = array();
        $operation = false;
        $new_user = DEFAULT_TRUE;

        // validate basic field

        $basicValidator = Validator::make(
            $request->all(),
            array(
                'device_type' => 'required|in:' . DEVICE_ANDROID . ',' . DEVICE_IOS,
                'device_token' => 'required',
                'login_by' => 'required|in:manual,facebook,google,twitter',
                'otp' => 'required'
            )
        );

        if ($basicValidator->fails()) {

            $error_messages = implode(',', $basicValidator->messages()->all());
            $response_array = array('success' => false, 'error' => Helper::get_error_message(101), 'error_code' => 101, 'error_message' => $error_messages);
            Log::info('Registration basic validation failed');

        } else {

            $login_by = $request->login_by;
            $allowed_social_login = array('facebook', 'google', 'twitter');


            if (in_array($login_by, $allowed_social_login)) {

                // validate social registration fields

                $socialValidator = Validator::make(
                    $request->all(),
                    array(
                        'social_unique_id' => 'required',
                        'first_name' => 'required|max:255',
                        'last_name' => 'max:255',
                        'email' => 'required|email|max:255|unique:users,email',
                        'picture' => 'mimes:jpeg,jpg,bmp,png',
                        'dob' => 'required',
                        'gender' => 'required',
                        'nationality' => 'required',
                        'country_code' => 'required',
                        'currency' => 'required',
                        'language' => 'required',
                    )
                );

                if ($socialValidator->fails()) {

                    $error_messages = implode(',', $socialValidator->messages()->all());
                    $response_array = array('success' => false, 'error' => Helper::get_error_message(101), 'error_code' => 101, 'error_message' => $error_messages);

                    Log::info('Registration social validation failed');

                } else {

                    $check_social_user = User::where('email', $request->email)->first();

                    if ($check_social_user) {
                        $new_user = DEFAULT_FALSE;
                    }

                    Log::info('Registration passed social validation');
                    $operation = true;
                }

            } else {

                // Validate manual registration fields

                $manualValidator = Validator::make(
                    $request->all(),
                    array(
                        'first_name' => 'required|max:255',
                        'last_name' => 'required|max:255',
                        'email' => 'required|email|max:255|unique:users,email',
                        'dob' => 'required',
                        'password' => 'required|min:6',
                        'picture' => 'mimes:jpeg,jpg,bmp,png',
                        'gender' => 'required',
                        'nationality' => 'required',
                        'country_code' => 'required',
                        'currency' => 'required',
                    )
                );

                // validate email existence

                $emailValidator = Validator::make(
                    $request->all(),
                    array(
                        'email' => 'unique:users,email',
                    )
                );

                if ($manualValidator->fails()) {

                    $error_messages = implode(',', $manualValidator->messages()->all());
                    $response_array = array('success' => false, 'error' => Helper::get_error_message(101), 'error_code' => 101, 'error_message' => $error_messages);
                    Log::info('Registration manual validation failed');

                } elseif ($emailValidator->fails()) {

                    $error_messages = implode(',', $emailValidator->messages()->all());
                    $response_array = array('success' => false, 'error' => Helper::get_error_message(101), 'error_code' => 101, 'error_message' => $error_messages);
                    Log::info('Registration manual email validation failed');

                } else {
                    Log::info('Registration passed manual validation');
                    $operation = true;
                }
            }

            $otp = Otp::where('phone', $request->phone)->where('country_code', $request->country_code)->first();

            if ($otp) {

                if ($request->otp == $otp->otp) {

                    if ($operation) {

                        if ($new_user) {
                            $user = new User;
                        } else {
                            $user = $check_social_user;
                        }

                        if ($request->has('first_name')) {
                            $user->first_name = $request->first_name;
                        }

                        if ($request->has('last_name')) {
                            $user->last_name = $request->last_name;
                        }

                        if ($request->has('email')) {
                            $user->email = $request->email;
                        }

                        if ($request->has('phone')) {
                            $user->phone = $request->phone;
                        }

                        if ($request->has('password'))
                            $user->password = Hash::make($request->password);

                        if ($request->has('dob')) {
                            $user->dob = $request->dob;
                        }

                        if ($request->has('gender')) {
                            $user->gender = $request->gender;
                        }

                        if ($request->has('country_code')) {
                            $user->country_code = $request->country_code;
                        }

                        if ($request->has('nationality')) {
                            $user->nationality = $request->nationality;
                        }

                        if ($request->has('ssn')) {
                            $user->ssn = $request->ssn;
                        }

                        if($request->has('currency')){
                            $user->currency = $request->currency;
                        }
                        if($request->has('language')){
                            $user->language = $request->language;
                        }
                        if($request->has('timezone')){
                            $user->timezone = $request->timezone;
                        }

                        $user->email_md5 = md5(rand(10,100000));
                        $user->session_token = Helper::generate_token();
                        $user->session_token_expiry = Helper::generate_token_expiry();

                        $check_device_exist = User::where('device_token', $request->device_token)->first();

                        if ($check_device_exist) {
                            $check_device_exist->device_token = "";
                            $check_device_exist->save();
                        }

                        $user->device_token = $request->has('device_token') ? $request->device_token : "";
                        $user->device_type = $request->has('device_type') ? $request->device_type : "";
                        $user->login_by = $request->has('login_by') ? $request->login_by : "";
                        $user->social_unique_id = $request->has('social_unique_id') ? $request->social_unique_id : '';
                        $user->access_token = $request->has('access_token') ? $request->access_token : '';

                        // Upload picture
                        $user->picture = Helper::upload_picture($request->file('picture'));



                        $user->save();
//
                        $client = new \GuzzleHttp\Client();

                        // if($request->login_by == 'manual'){
                        //     $email = $request->email;
                        // }else{
                        //     $email = $request->social_unique_id.'@socialuniqueid.com';
                        // }
                        $email = $request->email;

                        if($request->password)
                        {
                            $password = $request->password;
                        }else{
                            $password = $request->email;
                        }
                        

                        if($login_by == 'manual'){
                            $r = Curl::to('http://52.220.193.142:3000/api/v1/users/register')
                                    ->withData(array("name" =>$user->first_name." ".$user->last_name,
                                        "email" => $email,
                                        "password" =>$password))
                                    ->post();

                        }elseif ($login_by == 'facebook') {
                            $r = Curl::to('http://52.220.193.142:3000/api/v1/users/social_login')
                                    ->withData(array("access_token" =>$request->access_token,
                                        "gateway" => 'fb'
                                        ))
                                    ->post();
                        }elseif ($login_by == 'google') {
                            $r = Curl::to('http://52.220.193.142:3000/api/v1/users/social_login')
                                    ->withData(array("access_token" =>$request->access_token,
                                        "gateway" => 'google'
                                        ))
                                    ->post();
                        }



                       $ecomResponse = \GuzzleHttp\json_decode($r);


                       if($ecomResponse->status == "success"){

                        $user->ecom_id = $ecomResponse->response->_id;
                        $user->ecom_token = $ecomResponse->response->token?:0;


                       }

                       $user->save();

                       //Currency Update
        $userCurrency = $user->currency;

        if(!$userCurrency)
        {
          $userCurrency = 'USD';
        }

        $multiplierUSD = Swap::latest('USD/'.$userCurrency)->getValue();
        $multiplierMYR = Swap::latest('MYR/'.$userCurrency)->getValue();
        $multiplierCNY = Swap::latest('CNY/'.$userCurrency)->getValue();
        $multiplierSGD = Swap::latest('SGD/'.$userCurrency)->getValue();
        $multiplierIDR = Swap::latest('IDR/'.$userCurrency)->getValue();
        $multiplierVND = Swap::latest('VND/'.$userCurrency)->getValue();
        $multiplierMMK = Swap::latest('MMK/'.$userCurrency)->getValue();
        $multiplierIRR = Swap::latest('IRR/'.$userCurrency)->getValue();
        $multiplierJPY = Swap::latest('JPY/'.$userCurrency)->getValue();
        $multiplierTHB = Swap::latest('THB/'.$userCurrency)->getValue();

        $currencyMultiplier = CurrencyMultiplier::where('user_id',$user->id)->first();
        if(!$currencyMultiplier)
        {
            $currencyMultiplier = new CurrencyMultiplier;
        }

        $currencyMultiplier->user_id = $user->id;
        $currencyMultiplier->USD = $multiplierUSD;
        $currencyMultiplier->MYR = $multiplierMYR;
        $currencyMultiplier->CNY = $multiplierCNY;
        $currencyMultiplier->SGD = $multiplierSGD;
        $currencyMultiplier->IDR = $multiplierIDR;
        $currencyMultiplier->VND = $multiplierVND;
        $currencyMultiplier->MMK = $multiplierMMK;
        $currencyMultiplier->IRR = $multiplierIRR;
        $currencyMultiplier->JPY = $multiplierJPY;
        $currencyMultiplier->THB = $multiplierTHB;

        $currencyMultiplier->save();


                        // Send welcome email to the new user:
//                $result = Mail::send('emails.user_register',['user'=> $user],function ($message) use($user){
//                    $message->from('nithin@provenlogic.net');
//                    $message->to($user->email);
//                });

                        /* if($new_user) {
                             $subject = Helper::tr('user_welcome_title');
                             $email_data = $user;
                             $page = "emails.user.welcome";
                             $email = $user->email;
                             Helper::send_email($page,$subject,$email,$email_data);
                         }*/

                        $response_array = array(
                            'success' => true,
                            'id' => $user->id,
                            'first_name' => $user->first_name,
                            'last_name' => $user->last_name,
                            'phone' => $user->phone,
                            'email' => $user->email,
                            'picture' => $user->picture,
                            'login_by' => $user->login_by,
                            'gender' => $user->gender,
                            'dob' => $user->dob,
                            'country_code' => $user->country_code,
                            'nationality' => $user->nationality,
                            'social_unique_id' => $user->social_unique_id,
                            'device_token' => $user->device_token,
                            'device_type' => $user->device_type,
                            'session_token' => $user->session_token,
                            'b_street' => $user->b_street,
                            'b_city' => $user->b_city,
                            'b_state' => $user->b_state,
                            'b_country' => $user->b_country,
                            'b_postal' => $user->b_postal,
                            's_street' => $user->s_street,
                            's_city' => $user->s_city,
                            's_state' => $user->s_state,
                            's_country' => $user->s_country,
                            's_postal' => $user->s_postal,
                            'ssn' => $user->ssn,
                            'shipping_address' => $user->shipping_address,
                            'billing_address' =>$user->billing_address,
                            'sblock_no' => $user->sblock_no,
                            'slandmark' =>$user->slandmark,
                            'sphone_no' =>$user->sphone_no,
                            'bblock_no' => $user->bblock_no,
                            'blandmark' =>$user->blandmark,
                            'bphone_no' =>$user->bphone_no,
                           'ecom_token' => $user->ecom_token,
                            'currency' => $user->currency,
                            'language' => $user->language,
                            'timezone' => $user->timezone
                        );
                        $response_array = Helper::null_safe($response_array);
                        Log::info('Registration completed');

                        //Send Verification Email

                        $userid = $user->email_md5;
                        $url = asset_url().'/verifyemail/'.$userid;

                        $this->dispatch(new SendVerificationEmail($user,$url));


                    }

                } else {
                    $response_array = array('success' => false, 'error_message' => 'Otp does not match', 'error_code' => 105);

                }
            } else {
                $response_array = array('success' => false, 'error_message' => 'Otp does not exist', 'error_code' => 105);

            }

        }
        $response = response()->json($response_array, 200);
        return $response;




    }

    // User Login API
    public function login(Request $request)
    {
        $response_array = array();
        $operation = false;

        $basicValidator = Validator::make(
            $request->all(),
            array(
                'device_token' => 'required',
                'device_type' => 'required|in:' . DEVICE_ANDROID . ',' . DEVICE_IOS,
                'login_by' => 'required|in:manual,facebook,google,twitter',
            )
        );

        if ($basicValidator->fails()) {

            $error_messages = implode(',', $basicValidator->messages()->all());
            $response_array = array('success' => false, 'error_message' => Helper::get_error_message(101), 'error_code' => 101, 'error_messages' => $error_messages);

        } else {

            $login_by = $request->login_by;

            if ($login_by == 'manual') {

                /*validate manual login fields*/
                $manualValidator = Validator::make(
                    $request->all(),
                    array(
                        'email' => 'required|email',
                        'password' => 'required',
                    )
                );
                if ($manualValidator->fails()) {
                    $error_messages = implode(',', $manualValidator->messages()->all());
                    $response_array = array('success' => false, 'error_message' => Helper::get_error_message(101), 'error_code' => 101, 'error_messages' => $error_messages);
                } else {

                    $email = $request->email;
                    $password = $request->password;
                    
                    // Validate the user credentials
                    if ($user = User::where('email', '=', $email)->first()) {

                        if (Hash::check($password, $user->password)) {

                            /*manual login success*/
                            $operation = true;

                        } else {
                            $response_array = array('success' => false, 'error_message' => Helper::get_error_message(105), 'error_code' => 105);
                        }


                    } else {
                        $response_array = array('success' => false, 'error_message' => Helper::get_error_message(105), 'error_code' => 105);
                    }
                }


            } else {

                /*validate social login fields*/
                $socialValidator = Validator::make(
                    $request->all(),
                    array(
                        'social_unique_id' => 'required',
                    )
                );

                if ($socialValidator->fails()) {
                    $error_messages = implode(',', $socialValidator->messages()->all());
                    $response_array = array('success' => false, 'error_message' => Helper::get_error_message(101), 'error_code' => 101, 'error_messages' => $error_messages);
                } else {

                    $social_unique_id = $request->social_unique_id;
                    if ($user = User::where('social_unique_id', '=', $social_unique_id)->first()) {

                        $operation = true;

                    } else {
                        $response_array = array('success' => false, 'error_message' => Helper::get_error_message(125), 'error_code' => 125);
                    }
                }


            }

            if ($operation) {

                $device_token = $request->device_token;
                $device_type = $request->device_type;

                // Generate new tokens
                $user->session_token = Helper::generate_token();
                $user->session_token_expiry = Helper::generate_token_expiry();

                // Save device details
                $user->device_token = $device_token;
                $user->device_type = $device_type;
                $user->login_by = $login_by;



                $client = new \GuzzleHttp\Client();

                if($request->login_by == 'manual'){
                    $email = $request->email;
                }else{
                    $email = $request->social_unique_id.'@socialuniqueid.com';
                }


                if($request->password){
                    $password = $request->password;
                }else{
                    $password = "ecomghealth";
                }

                if($request->login_by == 'manual')
                {
                    $email = $request->email;
                    $password = $request->password;

                    $r = Curl::to('http://52.220.193.142:3000/api/v1/users/login')
                    ->withData(array(
                        "email" => $email,
                        "password" =>$password))
                    ->post();

                }elseif ($request->login_by == 'facebook') {

                    $r = Curl::to('http://52.220.193.142:3000/api/v1/users/social_login')
                                    ->withData(array("access_token" =>$request->access_token,
                                        "gateway" => 'fb'
                                        ))
                                    ->post();

                }elseif ($request->login_by == 'google') {

                    $r = Curl::to('http://52.220.193.142:3000/api/v1/users/social_login')
                                    ->withData(array("access_token" =>$request->access_token,
                                        "gateway" => 'google'
                                        ))
                                    ->post();

                }


                $ecomResponse = \GuzzleHttp\json_decode($r);

                if($ecomResponse->status == "fail")
                {
                    if($request->login_by == 'manual'){

                         $r = Curl::to('http://52.220.193.142:3000/api/v1/users/register')
                            ->withData(array("name" =>$user->first_name." ".$user->last_name,
                                "email" => $email,
                                "password" =>$password))
                            ->post();

                            $ecomResponse = \GuzzleHttp\json_decode($r);

                            $user->ecom_id = $ecomResponse->response->_id?: 0;
                            $user->ecom_token = $ecomResponse->response->token?:0;

                    }


                }elseif($request->login_by == 'manual' ){
                    $user->ecom_id = $ecomResponse->response->user->_id?: 0;
                    $user->ecom_token = $ecomResponse->response->user->token?:0;
                }else{
                     $user->ecom_id = $ecomResponse->response->_id?: 0;
                    $user->ecom_token = $ecomResponse->response->token?:0;

                }




                $user->save();




                $response_array = array(
                    'success' => true,
                    'id' => $user->id,
                    'first_name' => $user->first_name,
                    'last_name' => $user->last_name,
                    'phone' => $user->phone,
                    'email' => $user->email,
                    'picture' => $user->picture,
                    'login_by' => $user->login_by,
                    'gender' => $user->gender,
                    'nationality' => $user->nationality,
                    'country_code' => $user->country_code,
                    'social_unique_id' => $user->social_unique_id,
                    'device_token' => $user->device_token,
                    'device_type' => $user->device_type,
                    'session_token' => $user->session_token,
                    'b_street' => $user->b_street,
                    'b_city' => $user->b_city,
                    'b_state' => $user->b_state,
                    'b_country' => $user->b_country,
                    'b_postal' => $user->b_postal,
                    's_street' => $user->s_street,
                    's_city' => $user->s_city,
                    's_state' => $user->s_state,
                    's_country' => $user->s_country,
                    's_postal' => $user->s_postal,
                    'dob' => $user->dob,
                    'gender' => $user->gender,
                    'title' => $user->title,
                    'ssn' => $user->ssn,
                    'shipping_address' => $user->shipping_address,
                    'billing_address' =>$user->billing_address,
                    'sblock_no' => $user->sblock_no,
                    'slandmark' =>$user->slandmark,
                    'sphone_no' =>$user->sphone_no,
                    'bblock_no' => $user->bblock_no,
                    'blandmark' =>$user->blandmark,
                    'bphone_no' =>$user->bphone_no,
                    'ecom_token' =>$user->ecom_token,
                    'currency' => $user->currency,
                    'language' => $user->language,
                    'timezone' => $user->timezone
                );

                $response_array = Helper::null_safe($response_array);
            }
        }

        $response = response()->json($response_array, 200);
        return $response;
    }


    //profile update
    public function updateProfile(Request $request)
    {
        $id = $request->id;
        $user = User::find($id);

        if ($request->has('title'))
            $user->title = $request->title;
        if ($request->has('first_name'))
            $user->first_name = $request->first_name;
        if ($request->has('last_name'))
            $user->last_name = $request->last_name;
        if ($request->has('email'))
            $user->email = $request->email;
        if ($request->has('password')) {
            if ($request->password == $request->confirm_password) {
                $user->password = Hash::make($request->password);
            }
        }
        if ($request->file('picture'))
            $user->picture = Helper::upload_picture($request->file('picture'));
        if ($request->has('dob'))
            $user->dob = $request->dob;
        if ($request->has('otp')) {
            $otp = Otp::where('phone', $request->phone)->where('country_code', $request->country_code)->where('otp', $request->otp)->first();
            if ($otp) {
                $user->phone = $request->phone;
            } else {
                goto wrongOtp;
            }
        }

        if ($request->has('b_street'))
            $user->b_street = $request->b_street;
        else
            $user->b_street = "";
        if ($request->has('b_city'))
            $user->b_city = $request->b_city;
        else
            $user->b_city = "";
        if ($request->has('b_state'))
            $user->b_state = $request->b_state;
        else
            $user->b_state = "";
        if ($request->has('b_country'))
            $user->b_country = $request->b_country;
        else
            $user->b_country = "";
        if ($request->has('b_postal'))
            $user->b_postal = $request->b_postal;
        else
            $user->b_postal = "";
        if ($request->has('s_street'))
            $user->s_street = $request->s_street;
        else
            $user->s_street = "";
        if ($request->has('s_city'))
            $user->s_city = $request->s_city;
        else
            $user->s_city = "";
        if ($request->has('s_state'))
            $user->s_state = $request->s_state;
        else
            $user->s_state = "";
        if ($request->has('s_country'))
            $user->s_country = $request->s_country;
        else
            $user->s_country = "";
        if ($request->has('s_postal'))
            $user->s_postal = $request->s_postal;
        else
            $user->s_postal = "";
//        if ($request->same == 1) {
//            $user->s_street = $request->b_street;
//            $user->s_city = $request->b_city;
//            $user->s_state = $request->b_state;
//            $user->s_country = $request->b_country;
//            $user->s_postal = $request->b_postal;
//        }
        if ($request->has('gender'))
            $user->gender = $request->gender;
        if ($request->has('nationality'))
            $user->nationality = $request->nationality;
        if ($request->has('country_code'))
            $user->country_code = $request->country_code;
        if($request->has('language'))
            $user->language = $request->language;
        if($request->has('timezone'))
            $user->timezone = $request->timezone;

        if ($request->has('old_password')) {

            if (Hash::check($request->old_password, $user->password)) {

                if ($request->has('new_password')) {
                    $user->password = Hash::make($request->new_password);
                }

            } else {

                goto passwordWrong;

            }

        }

        if($request->has('currency'))
            $user->currency = $request->currency;

        if($request->has('sblock_no')){
            $user->sblock_no = $request->sblock_no;
        }
        if($request->has('slandmark')){
            $user->slandmark = $request->slandmark;
        }
        if($request->has('sphone_no')){
            $user->sphone_no = $request->sphone_no;
        }
        if($request->has('bblock_no')){
            $user->sblock_no = $request->sblock_no;
        }
        if($request->has('blandmark')){
            $user->slandmark = $request->slandmark;
        }
        if($request->has('bphone_no')){
            $user->sphone_no = $request->sphone_no;
        }

        if($request->has('shipping_address')){
            $user->shipping_address = $request->shipping_address;
        }

        if($request->has('shipping_latitude')){
            $user->shipping_latitude = $request->shipping_latitude;
        }

        if($request->has('shipping_longitude')){
            $user->shipping_longitude = $request->shipping_longitude;
        }

        if($request->has('billing_address')){
            $user->billing_address = $request->billing_address;
        }

        if($request->same == 1){
            $user->shipping_address = $request->billing_address;

        }

        $user->save();

        if($request->has('shipping_address')){
            $shipping = ShippingAddress::where('user_id',$request->id)->where('is_profile',1)->first();
            if(!$shipping){
                $shipping = new ShippingAddress;
            }

            $shipping->user_id = $request->id;
            $shipping->shipping_address = $request->shipping_address;
            $shipping->shipping_latitude = $request->shipping_latitude;
            $shipping->shipping_longitude = $request->shipping_longitude;
            $shipping->is_profile = 1;
            $shipping->is_default = 1;
            $shipping->sblock_no = $request->sblock_no;
            $shipping->slandmark = $request->slandmark;
            $shipping->sphone_no = $request->sphone_no;
            $shipping->save();
        }

        if($request->same == 1){

            $shipping = ShippingAddress::where('user_id',$request->id)->where('is_profile',1)->first();
            if(!$shipping){
                $shipping = new ShippingAddress;
            }

            $shipping->user_id = $request->id;
            $shipping->shipping_address = $request->billing_address;
            $shipping->shipping_latitude = $request->shipping_latitude;
            $shipping->shipping_longitude = $request->shipping_longitude;
            $shipping->is_profile = 1;
            $shipping->is_default = 1;
            $shipping->sblock_no = $request->sblock_no;
            $shipping->slandmark = $request->slandmark;
            $shipping->sphone_no = $request->sphone_no;
            $shipping->save();

        }

        //Currency Update
        $userCurrency = $user->currency;

        if(!$userCurrency)
        {
          $userCurrency = 'USD';
        }

        $multiplierUSD = Swap::latest('USD/'.$userCurrency)->getValue();
        $multiplierMYR = Swap::latest('MYR/'.$userCurrency)->getValue();
        $multiplierCNY = Swap::latest('CNY/'.$userCurrency)->getValue();
        $multiplierSGD = Swap::latest('SGD/'.$userCurrency)->getValue();
        $multiplierIDR = Swap::latest('IDR/'.$userCurrency)->getValue();
        $multiplierVND = Swap::latest('VND/'.$userCurrency)->getValue();
        $multiplierMMK = Swap::latest('MMK/'.$userCurrency)->getValue();
        $multiplierIRR = Swap::latest('IRR/'.$userCurrency)->getValue();
        $multiplierJPY = Swap::latest('JPY/'.$userCurrency)->getValue();
        $multiplierTHB = Swap::latest('THB/'.$userCurrency)->getValue();

        $currencyMultiplier = CurrencyMultiplier::where('user_id',$user->id)->first();
        if(!$currencyMultiplier)
        {
            $currencyMultiplier = new CurrencyMultiplier;
        }

        $currencyMultiplier->user_id = $user->id;
        $currencyMultiplier->USD = $multiplierUSD;
        $currencyMultiplier->MYR = $multiplierMYR;
        $currencyMultiplier->CNY = $multiplierCNY;
        $currencyMultiplier->SGD = $multiplierSGD;
        $currencyMultiplier->IDR = $multiplierIDR;
        $currencyMultiplier->VND = $multiplierVND;
        $currencyMultiplier->MMK = $multiplierMMK;
        $currencyMultiplier->IRR = $multiplierIRR;
        $currencyMultiplier->JPY = $multiplierJPY;
        $currencyMultiplier->THB = $multiplierTHB;

        $currencyMultiplier->save();





        $response_array = array('success' => true,
            'id' => $user->id,
            'title' => $user->title,
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'email' => $user->email,
            'dob' => $user->dob,
            'gender' => $user->gender,
            'nationality' => $user->nationality,
            'country_code' => $user->country_code,
            'picture' => $user->picture,
            'session_token' => $user->session_token,
            'device_token' => $user->device_token,
            'phone' => $user->phone,
            'b_street' => $user->b_street,
            'b_city' => $user->b_city,
            'b_state' => $user->b_state,
            'b_country' => $user->b_country,
            'b_postal' => $user->b_postal,
            's_street' => $user->s_street,
            's_city' => $user->s_city,
            's_state' => $user->s_state,
            's_country' => $user->s_country,
            's_postal' => $user->s_postal,
            'ssn' => $user->ssn,
            'shipping_address' => $user->shipping_address,
            'billing_address' =>$user->billing_address,
            'sblock_no' => $user->sblock_no,
            'slandmark' =>$user->slandmark,
            'sphone_no' =>$user->sphone_no,
            'bblock_no' => $user->bblock_no,
            'blandmark' =>$user->blandmark,
            'bphone_no' =>$user->bphone_no,
            'currency' =>$user->currency,
            'language' =>$user->language,
            'timezone' =>$user->timezone,
            'ecom_token' =>$user->ecom_token

        );
        $response_array = Helper::null_safe($response_array);

        $response = response()->json($response_array, 200);
        return $response;

        passwordWrong:
        $response_array = array('success' => false, 'error_message' => 'Please enter the correct password ');
        $response = response()->json($response_array, 200);
        return $response;


        wrongOtp:
        $response_array = array('success' => false, 'error_message' => 'Please enter the correct One Time Code');
        $response = response()->json($response_array, 200);
        return $response;


    }

    //List all the specialities
    public function getSpecialities(Request $request)
    {

        $specialities = Speciality::all();
        $speciality = array();

        $user = User::find($request->id);
        $language = str_replace(" ", "", $user->language);

        foreach ($specialities as $spec) {
            $special['id'] = $spec->id;

            if($language == "English(en)")
                $special['speciality'] = $spec->speciality;
            elseif($language == "Chinese(zh)")
                $special['speciality'] = $spec->speciality_chinese;
            elseif($language == "Thai(th)")
                $special['speciality'] = $spec->speciality_thai;
            elseif($language == "BahasaIndonesia(in)")
                $special['speciality'] = $spec->speciality_indonesian;
            elseif($language == "Vietnamese(vi)")
                $special['speciality'] = $spec->speciality_vietnamese;
            elseif($language == "Burmese(my)")
                $special['speciality'] = $spec->speciality_myanmar;
            elseif($language == "Farsi(fa)")
                $special['speciality'] = $spec->speciality_iran;
            elseif($language == "Japanese(ja)")
                $special['speciality'] = $spec->speciality_japan;
            else
                $special['speciality'] = $spec->speciality;

            $special['picture'] = $spec->picture;
            array_push($speciality, $special);
        }

        $response_array = array('success' => true, 'speciality' => $speciality);
        $response_array = Helper::null_safe($response_array);
        $response = response()->json($response_array, 200);
        return $response;


    }

    //Getting Doctors based on Clinic
    public function getDoctorOnlineClinic(Request $request)
    {

        $validator = Validator::make(
            $request->all(),
            array(
                'clinic_id' => 'required|integer',
                'speciality_id' => 'required'
            ));
        if ($validator->fails()) {
            $error_messages = implode(',', $validator->messages()->all());
            $response_array = array('success' => false, 'error_message' => Helper::get_error_message(156), 'error_code' => 101, 'error_messages' => $error_messages);
        } else {

            $doctors = Doctor::where('clinic_id', $request->clinic_id)->where('speciality_id',$request->speciality_id)->where('is_media_online', 1)->get();

            $doctor_list = array();

            foreach ($doctors as $doctor) {
                $clinic = Clinic::find($doctor->clinic_id);

                $doc['id'] = $doctor->id;
                $doc['d_name'] = $doctor->first_name . " " . $doctor->last_name;
                $doc['d_photo'] = $doctor->picture;
                $doc['email'] = $doctor->email;
                $doc['phone'] = $doctor->phone;
                $doc['nationality'] = $doctor->nationality;
                $doc['degree1'] = $doctor->degree1;
                $doc['degree2'] = $doctor->degree2;
                $doc['univ1'] = $doctor->univ1;
                $doc['univ2'] = $doctor->univ2;
                $doc['c_name'] = $clinic->clinic_name;
                $doc['c_pic1'] = $clinic->c_image1;
                $doc['c_pic2'] = $clinic->c_image2;
                $doc['c_pic3'] = $clinic->c_image3;
                $doc['d_latitude'] = $clinic->c_latitude;
                $doc['d_longitude'] = $clinic->c_longitude;
                $doc['c_address'] = $clinic->c_address;


                $doc['experience'] = $doctor->experience;
                $doc['helped'] = DB::table('user_ratings')->where('doctor_id', $doctor->id)->count();
                $rating = DB::table('user_ratings')->where('doctor_id', $doctor->id)->avg('rating') ?: 0;
                $doc['rating'] = round($rating, 1);

                $doctor_consult = DoctorConsult::where('doctor_id', $doctor->id)->first();
                $user = User::find($request->id);

                if ($doctor_consult) {
                    $doc['c_consult'] = $doctor_consult->chat_consult;
                    $doc['p_consult'] = $doctor_consult->phone_consult;
                    $doc['v_consult'] = $doctor_consult->video_consult;
                }

                $doctor_fee = DoctorFee::where('doctor_id', $doctor->id)->first();
                if ($doctor_fee) {
                    $doc['c_fee'] = round(Helper::convertMoney($user->id,$doctor->currency,$doctor_fee->chat_fee),2);
                    $doc['p_fee'] = round(Helper::convertMoney($user->id,$doctor->currency,$doctor_fee->phone_fee),2);
                    $doc['v_fee'] = round(Helper::convertMoney($user->id,$doctor->currency,$doctor_fee->video_fee),2);
                    $doc['free_minute'] = $doctor_fee->free_minute;
                } else {

                    $doc['c_fee'] = "0";
                    $doc['p_fee'] = "0";
                    $doc['v_fee'] = "0";
                    $doc['free_minute'] = 0;

                }

                array_push($doctor_list, $doc);

            }

            $response_array = array('success' => true,
                'doctor_list' => $doctor_list);
            $response_array = Helper::null_safe($response_array);


        }
        $response = response()->json($response_array, 200);
        return $response;
    }

    //Getting Doctors based on Clinic
    public function getDoctorBookingClinic(Request $request)
    {

        $validator = Validator::make(
            $request->all(),
            array(
                'clinic_id' => 'required|integer',
                'speciality_id' =>'required'
            ));
        if ($validator->fails()) {
            $error_messages = implode(',', $validator->messages()->all());
            $response_array = array('success' => false, 'error_message' => Helper::get_error_message(156), 'error_code' => 101, 'error_messages' => $error_messages);
        } else {

            $doctors = Doctor::where('clinic_id', $request->clinic_id)->where('speciality_id',$request->speciality_id)->get();

            $user = User::find($request->id);

            $doctor_list = array();

            foreach ($doctors as $doctor) {

                $clinic = Clinic::find($doctor->clinic_id);
                $doc['id'] = $doctor->id;
                $doc['d_name'] = $doctor->first_name . " " . $doctor->last_name;
                $doc['d_photo'] = $doctor->picture;
                $doc['email'] = $doctor->email;
                $doc['phone'] = $doctor->phone;
                $doc['nationality'] = $doctor->nationality;
                $doc['degree1'] = $doctor->degree1;
                $doc['degree2'] = $doctor->degree2;
                $doc['univ1'] = $doctor->univ1;
                $doc['univ2'] = $doctor->univ2;
                $doc['c_name'] = $clinic->clinic_name;
                $doc['c_pic1'] = $clinic->c_image1;
                $doc['c_pic2'] = $clinic->c_image2;
                $doc['c_pic3'] = $clinic->c_image3;
                $doc['d_latitude'] = $clinic->c_latitude;
                $doc['d_longitude'] = $clinic->c_longitude;
                $doc['c_address'] = $clinic->c_address;


                $doc['experience'] = $doctor->experience;
                $doc['helped'] = DB::table('user_ratings')->where('doctor_id', $doctor->id)->count();
                $rating = DB::table('user_ratings')->where('doctor_id', $doctor->id)->avg('rating') ?: 0;
                $doc['rating'] = round($rating, 1);
                $doc['appointment_fee'] = round(Helper::convertMoney($user->id,$doctor->currency,$doctor->booking_fee),2);


                array_push($doctor_list, $doc);

            }

            $response_array = array('success' => true,
                'doctor_list' => $doctor_list);
            $response_array = Helper::null_safe($response_array);


        }
        $response = response()->json($response_array, 200);
        return $response;
    }

    //Getting Doctors based on Clinic
    public function getDoctorOndemandClinic(Request $request)
    {

        $validator = Validator::make(
            $request->all(),
            array(
                'clinic_id' => 'required|integer',
                'speciality_id' =>'required'
            ));
        if ($validator->fails()) {
            $error_messages = implode(',', $validator->messages()->all());
            $response_array = array('success' => false, 'error_message' => Helper::get_error_message(156), 'error_code' => 101, 'error_messages' => $error_messages);
        } else {

            $doctors = Doctor::where('clinic_id', $request->clinic_id)
                ->where('speciality_id',$request->speciality_id)
                ->leftJoin('doctor_consults', 'doctor_consults.doctor_id', '=', 'doctors.id')
                ->where('doc_ondemand', '=', 1)
                ->get();

            $user = User::find($request->id);

            $doctor_list = array();

            foreach ($doctors as $doctor) {

                $clinic = Clinic::find($doctor->clinic_id);

                $doc['id'] = $doctor->id;
                $doc['d_name'] = $doctor->first_name . " " . $doctor->last_name;
                $doc['d_photo'] = $doctor->picture;
                $doc['email'] = $doctor->email;
                $doc['phone'] = $doctor->phone;
                $doc['nationality'] = $doctor->nationality;
                $doc['degree1'] = $doctor->degree1;
                $doc['degree2'] = $doctor->degree2;
                $doc['univ1'] = $doctor->univ1;
                $doc['univ2'] = $doctor->univ2;
                $doc['c_name'] = $clinic->clinic_name;
                $doc['c_pic1'] = $clinic->c_image1;
                $doc['c_pic2'] = $clinic->c_image2;
                $doc['c_pic3'] = $clinic->c_image3;
                $doc['d_latitude'] = $clinic->c_latitude;
                $doc['d_longitude'] = $clinic->c_longitude;
                $doc['c_address'] = $clinic->c_address;


                $doc['experience'] = $doctor->experience;
                $doc['helped'] = DB::table('user_ratings')->where('doctor_id', $doctor->id)->count();
                $rating = DB::table('user_ratings')->where('doctor_id', $doctor->id)->avg('rating') ?: 0;
                $doc['rating'] = round($rating, 1);
                $doctor_fee = DoctorFee::where('doctor_id', $doctor->id)->first();
                if ($doctor_fee)
                    $doc['ondemand_fee'] = round(Helper::convertMoney($user->id,$doctor->currency,$doctor_fee->ondemand_fee),2);
                else
                    $doc['ondemand_fee'] = "0";


                array_push($doctor_list, $doc);

            }

            $response_array = array('success' => true,
                'doctor_list' => $doctor_list);
            $response_array = Helper::null_safe($response_array);


        }
        $response = response()->json($response_array, 200);
        return $response;
    }

    //Get all doctors who are available for chat,video,phone consult
    public function getDoctorListOnline(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            array(
                'speciality_id' => 'required|integer',
            ));
        if ($validator->fails()) {
            $error_messages = implode(',', $validator->messages()->all());
            $response_array = array('success' => false, 'error_message' => Helper::get_error_message(156), 'error_code' => 101, 'error_messages' => $error_messages);
        } else {

            $doctor_array = array();

            if (!$request->has('skip'))
                $skip = 0;
            else
                $skip = $request->skip;

            if (!$request->has('take'))
                $take = 5000;
            else
                $take = $request->take;

            if($request->has('is_favorite') && $request->has('doctor_id')){

                if($request->is_favorite == 1){
                    $favoriteDoctor = FavoriteDoctor::where('user_id',$request->id)->where('doctor_id',$request->doctor_id)->first();

                    if(!$favoriteDoctor){

                        $favoriteDoctor = new FavoriteDoctor;
                        $favoriteDoctor->doctor_id = $request->doctor_id;
                        $favoriteDoctor->user_id = $request->id;
                        $favoriteDoctor->save();


                    }else{
                        //do nothing

                    }


                }else {
                    FavoriteDoctor::where('user_id', $request->id)->where('doctor_id', $request->doctor_id)->delete();

                }



            }///end of fav doc



            $doctors = DB::table('doctors')
                ->join('doctor_consults', 'doctors.id', '=', 'doctor_consults.doctor_id')
                ->select('doctors.*')
                ->where('doctors.speciality_id', $request->speciality_id)
                ->where('doctor_consults.is_online', '=', 1)
                ->where('doctors.is_approved', '=', 1)
                ->where('clinic_id', '!=', 0)
                ->skip($skip)
                ->take($take)
                ->get();
            $count = DB::table('doctors')
                ->join('doctor_consults', 'doctors.id', '=', 'doctor_consults.doctor_id')
                ->select('doctors.*')
                ->where('doctors.speciality_id', $request->speciality_id)
                ->where('doctor_consults.is_online', '=', 1)
                ->where('doctors.is_approved', '=', 1)
                ->where('clinic_id', '!=', 0)
                ->count();


            if ($doctors) {
                foreach ($doctors as $doctor) {

                    $favoriteDoctor = FavoriteDoctor::where('user_id',$request->id)
                                        ->where('doctor_id',$doctor->id)
                                        ->first();
                    if($favoriteDoctor){
                        $is_favorite = 1;
                    }else{
                        $is_favorite = 0;
                    }


                    $clinic = Clinic::find($doctor->clinic_id);
                    if ($clinic) {
                        $clinic_lat = $clinic->c_latitude;
                        $clinic_lng = $clinic->c_longitude;
                        $clinic_name = $clinic->clinic_name;
                        $clinic_address = $clinic->c_address;
                        $clinic_image1 = $clinic->c_image1;
                        $clinic_image2 = $clinic->c_image2;
                        $clinic_image3 = $clinic->c_image3;
                        $clinic_phone = $clinic->c_phone;
                        $clinic_fax = $clinic->c_fax;

                    }

                    $doc['id'] = $doctor->id;
                    $doc['d_name'] = $doctor->first_name . " " . $doctor->last_name;
                    $doc['d_photo'] = $doctor->picture;
                    $doc['c_name'] = $clinic_name;
                    //$doc['c_street'] = $doctor->c_street;
                    //$doc['c_city'] = $doctor->c_city;
                    //$doc['c_state'] = $doctor->c_state;
                    //$doc['c_postal'] = $doctor->c_postal;
                    //$doc['c_country'] = $doctor->c_country;
                    $doc['c_address'] = $clinic_address;
                    $doc['c_phone'] = $clinic_phone;
                    $doc['c_fax'] = $clinic_fax;
                    $doc['c_pic1'] = $clinic_image1;
                    $doc['c_pic2'] = $clinic_image2;
                    $doc['c_pic3'] = $clinic_image3;
                    $doc['d_latitude'] = $clinic_lat;
                    $doc['d_longitude'] = $clinic_lng;
                    $doc['experience'] = $doctor->experience;
                    $doc['nationality'] = $doctor->nationality;
                    $doc['degree1'] = $doctor->degree1;
                    $doc['degree2'] = $doctor->degree2;
                    $doc['univ1'] = $doctor->univ1;
                    $doc['univ2'] = $doctor->univ2;
                    $doc['helped'] = DB::table('user_ratings')->where('doctor_id', $doctor->id)->count();
                    $rating = round(DB::table('user_ratings')->where('doctor_id', $doctor->id)->avg('rating'), 1) ?: 0;
                    $doc['rating'] = round($rating, 1);
                    $doc['is_favorite'] = $is_favorite;

                    $doctor_consult = DoctorConsult::where('doctor_id', $doctor->id)->first();
                    $user = User::find($request->id);

                    if ($doctor_consult) {
                        $doc['c_consult'] = $doctor_consult->chat_consult;
                        $doc['p_consult'] = $doctor_consult->phone_consult;
                        $doc['v_consult'] = $doctor_consult->video_consult;
                    }

                    $doctor_fee = DoctorFee::where('doctor_id', $doctor->id)->first();
                    if ($doctor_fee) {
                        $doc['c_fee'] = round(Helper::convertMoney($user->id,$doctor->currency,$doctor_fee->chat_fee),2);
                        $doc['p_fee'] = round(Helper::convertMoney($user->id,$doctor->currency,$doctor_fee->phone_fee),2);
                        $doc['v_fee'] = round(Helper::convertMoney($user->id,$doctor->currency,$doctor_fee->video_fee),2);
                        $doc['free_minute'] = $doctor_fee->free_minute;
                    } else {

                        $doc['c_fee'] = "0";
                        $doc['p_fee'] = "0";
                        $doc['v_fee'] = "0";
                        $doc['free_minute'] =0;

                    }


                    array_push($doctor_array, $doc);

                }

                $response_array = array('success' => true,
                    'count' => $count,
                    'doctor_list' => $doctor_array);
                $response_array = Helper::null_safe($response_array);

            } else {
                $response_array = array('success' => false,
                    'error_message' => 'No doctors available');
            }


        }

        $response = response()->json($response_array, 200);
        return $response;
    }

    //Get all doctors who are available for appointment
    public function getDoctorListBooking(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            array(
                'speciality_id' => 'required|integer',
            ));
        if ($validator->fails()) {
            $error_messages = implode(',', $validator->messages()->all());
            $response_array = array('success' => false, 'error_message' => Helper::get_error_message(156), 'error_code' => 101, 'error_messages' => $error_messages);
        } else {

            $doctor_array = array();

            if (!$request->has('skip'))
                $skip = 0;
            else
                $skip = $request->skip;

            if (!$request->has('take'))
                $take = 50;
            else
                $take = $request->take;


            if($request->has('is_favorite') && $request->has('doctor_id')){

                if($request->is_favorite == 1){
                    $favoriteDoctor = FavoriteDoctor::where('user_id',$request->user_id)->where('doctor_id',$request->doctor_id)->first();

                    if(!$favoriteDoctor){

                        $favoriteDoctor = new FavoriteDoctor;
                        $favoriteDoctor->doctor_id = $request->doctor_id;
                        $favoriteDoctor->user_id = $request->id;
                        $favoriteDoctor->save();


                    }else{
                        //do nothing

                    }


                }else {
                    FavoriteDoctor::where('user_id', $request->id)->where('doctor_id', $request->doctor_id)->delete();

                }



            }///end of fav doc

            $user = User::find($request->id);

            $doctors = DB::table('doctors')
                ->select('doctors.*')
                ->where('doctors.is_booking_online', '=', 1)
                ->where('doctors.speciality_id', $request->speciality_id)
                ->where('doctors.is_approved', '=', 1)
                ->where('clinic_id', '!=', 0)
                ->skip($skip)
                ->take($take)
                ->get();
            $count = DB::table('doctors')
                ->select('doctors.*')
                ->where('doctors.is_booking_online', '=', 1)
                ->where('doctors.speciality_id', $request->speciality_id)
                ->where('doctors.is_approved', '=', 1)
                ->where('clinic_id', '!=', 0)
                ->count();
            if ($doctors) {

                foreach ($doctors as $doctor) {

                    $favoriteDoctor = FavoriteDoctor::where('user_id',$request->id)
                        ->where('doctor_id',$doctor->id)
                        ->first();
                    if($favoriteDoctor){
                        $is_favorite = 1;
                    }else{
                        $is_favorite = 0;
                    }

                    $clinic = Clinic::find($doctor->clinic_id);
                    if ($clinic) {
                        $clinic_lat = $clinic->c_latitude;
                        $clinic_lng = $clinic->c_longitude;
                        $clinic_name = $clinic->clinic_name;
                        $clinic_address = $clinic->c_address;
                        $clinic_image1 = $clinic->c_image1;
                        $clinic_image2 = $clinic->c_image2;
                        $clinic_image3 = $clinic->c_image3;
                        $clinic_phone = $clinic->c_phone;
                        $clinic_fax = $clinic->c_fax;

                    }

                    $doc['id'] = $doctor->id;
                    $doc['d_name'] = $doctor->first_name . " " . $doctor->last_name;
                    $doc['d_photo'] = $doctor->picture;
                    $doc['experience'] = $doctor->experience;
                    $doc['helped'] = DB::table('user_ratings')->where('doctor_id', $doctor->id)->count();
                    $rating = DB::table('user_ratings')->where('doctor_id', $doctor->id)->avg('rating') ?: 0;
                    $doc['rating'] = round($rating, 1);
                    $doc['is_favorite'] = $is_favorite;
                    $doc['d_latitude'] = $clinic_lat;
                    $doc['d_longitude'] = $clinic_lng;
                    $doc['c_name'] = $clinic_name;
//              $doc['c_street'] = $doctor->c_street;
//              $doc['c_city'] = $doctor->c_city;
//              $doc['c_state'] = $doctor->c_state;
//              $doc['c_postal'] = $doctor->c_postal;
//              $doc['c_country'] = $doctor->c_country;
                    $doc['c_address'] = $clinic_address;
                    $doc['c_phone'] = $clinic_phone;
                    $doc['c_fax'] = $clinic_fax;
                    $doc['c_pic1'] = $clinic_image1;
                    $doc['c_pic2'] = $clinic_image2;
                    $doc['c_pic3'] = $clinic_image3;
                    $doc['is_online'] = $doctor->is_booking_online;
                    $doc['appointment_fee'] = round(Helper::convertMoney($user->id,$doctor->currency,$doctor->booking_fee),2);
                    $doc['nationality'] = $doctor->nationality;
                    $doc['degree1'] = $doctor->degree1;
                    $doc['degree2'] = $doctor->degree2;
                    $doc['univ1'] = $doctor->univ1;
                    $doc['univ2'] = $doctor->univ2;

                    array_push($doctor_array, $doc);

                }

                $response_array = array('success' => true, 'count' => $count, 'doctor_list' => $doctor_array);
                $response_array = Helper::null_safe($response_array);
            } else {
                $response_array = array('success' => false, 'error_message' => 'No doctor available');

            }

        }

        $response = response()->json($response_array, 200);
        return $response;
    }

    // Get all doctors available for on demand booking
    public function getDoctorOnDemand(Request $request)
    {

        $validator = Validator::make(
            $request->all(),
            array(
                'latitude' => 'required',
                'longitude' => 'required',
                'speciality_id' => 'required|integer',
            ));
        if ($validator->fails()) {
            $error_messages = implode(',', $validator->messages()->all());
            $response_array = array('success' => false, 'error_message' => Helper::get_error_message(101), 'error_code' => 101, 'error_messages' => $error_messages);
        } else {
            //$currency = 'INR';
            //dd(Swap::latest($currency.'/MYR'));

            $distance = 300;
            $latitude = $request->latitude;
            $longitude = $request->longitude;
            $speciality_id = $request->speciality_id;

            $json = json_decode(file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?latlng=' . $request->latitude . ',' . $request->longitude . '&key=AIzaSyC4AEUI8uxPs42kVRzSUb4B718ZK0hKxew'), true);

            if (!$json['results']) {
                $address = '';
            } else {
                $address = $json['results'][0]['formatted_address'];
            }

            //$query = "SELECT doctors.id,doctors.first_name,doctors.last_name,doctors.phone,doctors.email,doctors.picture,doctors.dob,doctors.title,doctors.gender,doctors.med_number,doctors.latitude,doctors.longitude,doctors.clinic_id,doctors.c_name,doctors.c_street,doctors.c_city,doctors.c_state,doctors.c_postal,doctors.c_country,doctors.c_pic1,doctors.c_pic2,doctors.c_pic3,doctors.nationality,doctors.degree1,doctors.univ1,doctors.univ2,doctors.degree2,doctors.experience, 1.609344 * 3956 * acos( cos( radians('$latitude') ) * cos( radians(clinics.c_latitude) ) * cos( radians(clinics.c_longitude) - radians('$longitude') ) + sin( radians('$latitude') ) * sin( radians(clinics.c_latitude) ) ) as distance from doctors INNER JOIN clinics ON doctors.clinic_id = clinics.id INNER JOIN doctor_consults ON doctors.id = doctor_consults.doctor_id where speciality_id = $speciality_id and doctor_consults.doc_ondemand = 1 and doctors.is_approved = 1 and doctors.clinic_id != 0 and (1.609344 * 3956 * acos( cos( radians('$latitude') ) * cos( radians(clinics.c_latitude) ) * cos( radians(clinics.c_longitude) - radians('$longitude') ) + sin( radians('$latitude') ) * sin( radians(clinics.c_latitude) ) ) ) <= $distance order by distance";

            $query = "SELECT clinics.id,clinics.clinic_name,1.609344 * 3956 * acos( cos( radians('$latitude') ) * cos( radians(clinics.c_latitude) ) * cos( radians(clinics.c_longitude) - radians('$longitude') ) + sin( radians('$latitude') ) * sin( radians(clinics.c_latitude) ) ) as distance from clinics where (1.609344 * 3956 * acos( cos( radians('$latitude') ) * cos( radians(clinics.c_latitude) ) * cos( radians(clinics.c_longitude) - radians('$longitude') ) + sin( radians('$latitude') ) * sin( radians(clinics.c_latitude) ) ) ) <= $distance order by distance";


            //$query_old = "SELECT doctors.id,doctors.first_name,doctors.last_name,doctors.phone,doctors.clinic_id,doctors.email,doctors.picture,doctors.dob,doctors.title,doctors.gender,doctors.med_number,doctors.latitude,doctors.longitude,doctors.c_name,doctors.c_street,doctors.c_city,doctors.c_state,doctors.c_postal,doctors.c_country,doctors.c_pic1,doctors.c_pic2,doctors.c_pic3,doctors.nationality,doctors.degree1,doctors.univ1,doctors.univ2,doctors.degree2,doctors.experience, 1.609344 * 3956 * acos( cos( radians('$latitude') ) * cos( radians(latitude) ) * cos( radians(longitude) - radians('$longitude') ) + sin( radians('$latitude') ) * sin( radians(latitude) ) ) as distance from doctors INNER JOIN doctor_consults ON doctors.id = doctor_consults.doctor_id where speciality_id = $speciality_id and doctor_consults.doc_ondemand = 1 and doctors.is_approved = 1 and (1.609344 * 3956 * acos( cos( radians('$latitude') ) * cos( radians(latitude) ) * cos( radians(longitude) - radians('$longitude') ) + sin( radians('$latitude') ) * sin( radians(latitude) ) ) ) <= $distance order by distance";
            //$doctors = DB::select(DB::raw($query));
            $clinics = $doctors = DB::select(DB::raw($query));

            $doctor_list = array();
            if ($clinics) {
                foreach ($clinics as $clinic) {

                    $clinic = Clinic::find($clinic->id);
                    if ($clinic) {
                        $clinic_id = $clinic->id;
                        $clinic_lat = $clinic->c_latitude;
                        $clinic_lng = $clinic->c_longitude;
                        $clinic_name = $clinic->clinic_name;
                        $clinic_address = $clinic->c_address;
                        $clinic_image1 = $clinic->c_image1;
                        $clinic_image2 = $clinic->c_image2;
                        $clinic_image3 = $clinic->c_image3;
                        $clinic_phone = $clinic->c_phone;
                        $clinic_fax = $clinic->c_fax;

                        $doctors_count = Doctor::where('clinic_id', $clinic->id)
                            ->where('speciality_id',$request->speciality_id)
                            ->leftJoin('doctor_consults', 'doctor_consults.doctor_id', '=', 'doctors.id')
                            ->where('doc_ondemand', '=', 1)
                            ->count();

                    }


                    //$doctor_fee = DoctorFee::where('doctor_id',$doctor->id)->first();
//                      $doc['id'] = $doctor->id;
//                      $doc['d_name'] = $doctor->first_name." ".$doctor->last_name;
//                      $doc['d_photo'] = $doctor->picture;
//                      $doc['phone'] = $doctor->phone;
//                      $doc['email'] = $doctor->email;
                    $doc['d_latitude'] = $clinic_lat;
                    $doc['d_longitude'] = $clinic_lng;
//                        $doc['latitude'] = $doctor->latitude; //remove
//                        $doc['longitude'] = $doctor->longitude; //remove
//                      $doc['experience'] = $doctor->experience;
//                      $doc['helped'] = DB::table('user_ratings')->where('doctor_id', $doctor->id)->count();
//                      $rating = DB::table('user_ratings')->where('doctor_id', $doctor->id)->avg('rating') ?: 0;
//                        $doc['rating'] = round($rating,1);
                    $doc['c_id'] = $clinic_id;
                    $doc['c_name'] = $clinic_name;
//                      $doc['c_city'] = $doctor->c_city;
//                      $doc['c_street'] = $doctor->c_street;
//                      $doc['c_state'] = $doctor->c_state;
//                      $doc['c_country'] = $doctor->c_country;
                    $doc['c_address'] = $clinic_address;
                    $doc['c_phone'] = $clinic_phone;
                    $doc['c_fax'] = $clinic_fax;
//                      $doc['c_postal'] = $doctor->c_postal;
                    $doc['c_pic1'] = $clinic_image1;
                    $doc['c_pic2'] = $clinic_image2;
                    $doc['c_pic3'] = $clinic_image3;
                    $doc['c_count'] = $doctors_count;
//                        $doc['nationality'] = $doctor->nationality;
//                        $doc['degree1'] = $doctor->degree1;
//                        $doc['degree2'] = $doctor->degree2;
//                        $doc['univ1'] = $doctor->univ1;
//                        $doc['univ2'] = $doctor->univ2;

                    //$distance = Helper::getDistanceTime($clinic_lat, $clinic_lng, $request->latitude, $request->longitude);
                    //$distArray = explode("$$", $distance);

                    // $doc['eta'] = $distArray[0] . ' away';
                    // $doc['time'] = $distArray[1];
                    $distance = Helper::vincentyGreatCircleDistance($clinic_lat, $clinic_lng, $request->latitude, $request->longitude);
                    $distance = round($distance);
                    if($distance > 1000){
                        $distance = $distance/1000;
                        $doc['eta'] = $distance.' km away';
                    }else{
                        $doc['eta'] = $distance.' m away';
                    }
                    
                    $doc['time'] = 0;

//                      if($doctor_fee)
//                          $doc['ondemand_fee'] = $doctor_fee->ondemand_fee;
//                      else
//                          $doc['ondemand_fee'] = "0";
                    array_push($doctor_list, $doc);
                }

                $response_array = array('success' => true,
                    'source_address' => $address,
                    'doctors' => $doctor_list);

                $response_array = Helper::null_safe($response_array);
            } else {
                $response_array = array('success' => false,
                    'error_message' => 'No doctors available');
            }
        }

        $response = response()->json($response_array, 200);
        return $response;

    }

    // Get all doctors nearby available for chat,video,phone consult
    public function getNearbyOnline(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            array(
                'latitude' => 'required',
                'longitude' => 'required',
                'speciality_id' => 'required|integer',
            ));
        if ($validator->fails()) {
            $error_messages = implode(',', $validator->messages()->all());
            $response_array = array('success' => false, 'error_message' => Helper::get_error_message(101), 'error_code' => 101, 'error_messages' => $error_messages);
        } else {

            $json = json_decode(file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?latlng=' . $request->latitude . ',' . $request->longitude . '&key=AIzaSyC4AEUI8uxPs42kVRzSUb4B718ZK0hKxew'), true);

            if (!$json['results']) {
                $address = '';
            } else {
                $address = $json['results'][0]['formatted_address'];
            }

            $distance =300;
            $latitude = $request->latitude;
            $longitude = $request->longitude;
            $speciality_id = $request->speciality_id;


            //$query = "SELECT doctors.id,doctors.first_name,doctors.last_name,doctors.clinic_id,doctors.phone,doctors.email,doctors.picture,doctors.dob,doctors.title,doctors.gender,doctors.med_number,doctors.latitude,doctors.longitude,doctors.c_street,doctors.c_city,doctors.c_state,doctors.c_postal,doctors.c_country,doctor_consults.phone_consult, doctor_consults.chat_consult, doctor_consults.video_consult,doctors.c_name,doctors.c_pic1,doctors.c_pic2,doctors.c_pic3,doctors.nationality,doctors.univ1,doctors.univ2,doctors.degree1,doctors.degree2,doctors.experience ,1.609344 * 3956 * acos( cos( radians('$latitude') ) * cos( radians(latitude) ) * cos( radians(longitude) - radians('$longitude') ) + sin( radians('$latitude') ) * sin( radians(latitude) ) ) as distance from doctors INNER JOIN clinics ON doctors.clinic_id = clinics.id INNER JOIN doctor_consults ON doctors.id = doctor_consults.doctor_id where speciality_id = $speciality_id and doctor_consults.is_online = 1 and doctors.is_approved = 1 and doctors.clinic_id != 0 and (1.609344 * 3956 * acos( cos( radians('$latitude') ) * cos( radians(latitude) ) * cos( radians(longitude) - radians('$longitude') ) + sin( radians('$latitude') ) * sin( radians(latitude) ) ) ) <= $distance order by distance";
            $query = "SELECT clinics.id,clinics.clinic_name,1.609344 * 3956 * acos( cos( radians('$latitude') ) * cos( radians(clinics.c_latitude) ) * cos( radians(clinics.c_longitude) - radians('$longitude') ) + sin( radians('$latitude') ) * sin( radians(clinics.c_latitude) ) ) as distance from clinics where (1.609344 * 3956 * acos( cos( radians('$latitude') ) * cos( radians(clinics.c_latitude) ) * cos( radians(clinics.c_longitude) - radians('$longitude') ) + sin( radians('$latitude') ) * sin( radians(clinics.c_latitude) ) ) ) <= $distance order by distance";
            //$query_old = "SELECT doctors.id,doctors.first_name,doctors.last_name,doctors.phone,doctors.email,doctors.clinic_id,doctors.picture,doctors.dob,doctors.title,doctors.gender,doctors.med_number,doctors.latitude,doctors.longitude,doctors.c_street,doctors.c_city,doctors.c_state,doctors.c_postal,doctors.c_country,doctor_consults.phone_consult, doctor_consults.chat_consult, doctor_consults.video_consult,doctors.c_name,doctors.c_pic1,doctors.c_pic2,doctors.c_pic3,doctors.nationality,doctors.univ1,doctors.univ2,doctors.degree1,doctors.degree2,doctors.experience ,1.609344 * 3956 * acos( cos( radians('$latitude') ) * cos( radians(latitude) ) * cos( radians(longitude) - radians('$longitude') ) + sin( radians('$latitude') ) * sin( radians(latitude) ) ) as distance from doctors INNER JOIN doctor_consults ON doctors.id = doctor_consults.doctor_id where speciality_id = $speciality_id and doctor_consults.is_online = 1 and doctors.is_approved = 1 and (1.609344 * 3956 * acos( cos( radians('$latitude') ) * cos( radians(latitude) ) * cos( radians(longitude) - radians('$longitude') ) + sin( radians('$latitude') ) * sin( radians(latitude) ) ) ) <= $distance order by distance";
            $clinics = DB::select(DB::raw($query));

            Log:info("CLINIC NEARBY...",['query'=> $clinics]);

            $doctor_list = array();
            if ($clinics) {
                foreach ($clinics as $clinic) {

                    $clinic = Clinic::find($clinic->id);
                    if ($clinic) {
                        $clinic_lat = $clinic->c_latitude;
                        $clinic_lng = $clinic->c_longitude;
                        $clinic_name = $clinic->clinic_name;
                        $clinic_address = $clinic->c_address;
                        $clinic_image1 = $clinic->c_image1;
                        $clinic_image2 = $clinic->c_image2;
                        $clinic_image3 = $clinic->c_image3;
                        $clinic_phone = $clinic->c_phone;
                        $clinic_fax = $clinic->c_fax;
                        $clinic_id = $clinic->id;

                        $doctors_count = Doctor::where('clinic_id', $clinic->id)
                            ->where('speciality_id',$request->speciality_id)
                            ->where('is_media_online', 1)
                            ->count();

                    }

//                  $doctor_fee = DoctorFee::where('doctor_id',$doctor->id)->first();
//                  $doc['id'] = $doctor->id;
//                  $doc['d_name'] = $doctor->first_name." ".$doctor->last_name;
//                  $doc['d_photo'] = $doctor->picture;
//                  $doc['phone'] = $doctor->phone;
//                  $doc['email'] = $doctor->email;
                    $doc['d_latitude'] = $clinic_lat;
                    $doc['d_longitude'] = $clinic_lng;
//                    $doc['latitude'] = $doctor->latitude; // remove
//                    $doc['longitude'] = $doctor->longitude; //remove
//                  $doc['p_consult'] = $doctor->phone_consult;
//                  $doc['c_consult'] = $doctor->chat_consult;
//                  $doc['v_consult'] = $doctor->video_consult;
                    $doc['c_id'] = $clinic_id;
                    $doc['c_name'] = $clinic_name;
//                  $doc['c_city'] = $doctor->c_city;
//                  $doc['c_street'] = $doctor->c_street;
//                  $doc['c_state'] = $doctor->c_state;
//                  $doc['c_country'] = $doctor->c_country;
                    $doc['c_address'] = $clinic_address;
                    $doc['c_phone'] = $clinic_phone;
                    $doc['c_fax'] = $clinic_fax;
//                  $doc['c_postal'] = $doctor->c_postal;
                    $doc['c_pic1'] = $clinic_image1;
                    $doc['c_pic2'] = $clinic_image2;
                    $doc['c_pic3'] = $clinic_image3;
                    $doc['c_count'] = $doctors_count;
//                    $doc['nationality'] = $doctor->nationality;
//                    $doc['degree1'] = $doctor->degree1;
//                    $doc['degree2'] = $doctor->degree2;
//                    $doc['univ1'] = $doctor->univ1;
//                    $doc['univ2'] = $doctor->univ2;

                    //$distance = Helper::getDistanceTime($clinic_lat, $clinic_lng, $request->latitude, $request->longitude);

                    //$distArray = explode("$$", $distance);

                    //$doc['eta'] = $distArray[0] . ' away';
                    //$doc['time'] = $distArray[1];
                    $distance = Helper::vincentyGreatCircleDistance($clinic_lat, $clinic_lng, $request->latitude, $request->longitude);
                    $distance = round($distance);
                    if($distance > 1000){
                        $distance = $distance/1000;
                        $doc['eta'] = $distance.' km away';
                    }else{
                        $doc['eta'] = $distance.' m away';
                    }
                    
                    $doc['time'] = 0;

//                  $doc['experience'] = $doctor->experience;
//                  $doc['helped'] = DB::table('user_ratings')->where('doctor_id', $doctor->id)->count();
//                  $rating = DB::table('user_ratings')->where('doctor_id', $doctor->id)->avg('rating') ?: 0;
//                    $doc['rating'] = round($rating,1);
//                  if($doctor_fee)
//                  {
//                      $doc['c_fee'] = $doctor_fee->chat_fee;
//                      $doc['p_fee'] = $doctor_fee->phone_fee;
//                      $doc['v_fee'] = $doctor_fee->video_fee;
//                  }else{
//                      $doc['c_fee'] = "0";
//                      $doc['p_fee'] = "0";
//                      $doc['v_fee'] = "0";
//                  }
                    array_push($doctor_list, $doc);


                }

                $response_array = array('success' => true,
                    'source_address' => $address,
                    'doctors' => $doctor_list);
                $response_array = Helper::null_safe($response_array);
            } else {
                $response_array = array('success' => false,
                    'error_message' => 'No doctors available');

            }
        }

        $response = response()->json($response_array, 200);
        return $response;
    }


    public function getNearbyBooking(Request $request)
    {

        $validator = Validator::make(
            $request->all(),
            array(
                'latitude' => 'required',
                'longitude' => 'required',
                'speciality_id' => 'required|integer',
            ));
        if ($validator->fails()) {
            $error_messages = implode(',', $validator->messages()->all());
            $response_array = array('success' => false, 'error_message' => Helper::get_error_message(101), 'error_code' => 101, 'error_messages' => $error_messages);
        } else {

            $json = json_decode(file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?latlng=' . $request->latitude . ',' . $request->longitude . '&key=AIzaSyC4AEUI8uxPs42kVRzSUb4B718ZK0hKxew'), true);

            if (!$json['results']) {
                $address = '';
            } else {
                $address = $json['results'][0]['formatted_address'];
            }

            $distance = 300;
            $latitude = $request->latitude;
            $longitude = $request->longitude;
            $speciality_id = $request->speciality_id;

            //$query = "SELECT doctors.id,doctors.first_name,doctors.last_name,doctors.clinic_id,doctors.phone,doctors.email,doctors.picture,doctors.dob,doctors.title,doctors.gender,doctors.med_number,doctors.latitude,doctors.longitude,doctors.c_street,doctors.c_city,doctors.c_state,doctors.c_postal,doctors.c_country,doctors.experience,doctors.c_name,doctors.c_pic1,doctors.c_pic2,doctors.c_pic3,doctors.nationality,doctors.univ1,doctors.univ2,doctors.degree1,doctors.degree2,doctors.booking_fee,doctors.is_booking_online ,1.609344 * 3956 * acos( cos( radians('$latitude') ) * cos( radians(latitude) ) * cos( radians(longitude) - radians('$longitude') ) + sin( radians('$latitude') ) * sin( radians(latitude) ) ) as distance from doctors INNER JOIN clinics ON doctors.clinic_id = clinics.id where speciality_id = $speciality_id and doctors.is_booking_online = 1 and doctors.is_approved = 1 and doctors.clinic_id != 0 and (1.609344 * 3956 * acos( cos( radians('$latitude') ) * cos( radians(latitude) ) * cos( radians(longitude) - radians('$longitude') ) + sin( radians('$latitude') ) * sin( radians(latitude) ) ) ) <= $distance order by distance";
            $query = "SELECT clinics.id,clinics.clinic_name,1.609344 * 3956 * acos( cos( radians('$latitude') ) * cos( radians(clinics.c_latitude) ) * cos( radians(clinics.c_longitude) - radians('$longitude') ) + sin( radians('$latitude') ) * sin( radians(clinics.c_latitude) ) ) as distance from clinics where (1.609344 * 3956 * acos( cos( radians('$latitude') ) * cos( radians(clinics.c_latitude) ) * cos( radians(clinics.c_longitude) - radians('$longitude') ) + sin( radians('$latitude') ) * sin( radians(clinics.c_latitude) ) ) ) <= $distance order by distance";

            //$query_old = "SELECT doctors.id,doctors.first_name,doctors.last_name,doctors.phone,doctors.clinic_id,doctors.email,doctors.picture,doctors.dob,doctors.title,doctors.gender,doctors.med_number,doctors.latitude,doctors.longitude,doctors.c_street,doctors.c_city,doctors.c_state,doctors.c_postal,doctors.c_country,doctors.experience,doctors.c_name,doctors.c_pic1,doctors.c_pic2,doctors.c_pic3,doctors.nationality,doctors.univ1,doctors.univ2,doctors.degree1,doctors.degree2,doctors.booking_fee,doctors.is_booking_online ,1.609344 * 3956 * acos( cos( radians('$latitude') ) * cos( radians(latitude) ) * cos( radians(longitude) - radians('$longitude') ) + sin( radians('$latitude') ) * sin( radians(latitude) ) ) as distance from doctors where speciality_id = $speciality_id and doctors.is_booking_online = 1 and doctors.is_approved = 1 and (1.609344 * 3956 * acos( cos( radians('$latitude') ) * cos( radians(latitude) ) * cos( radians(longitude) - radians('$longitude') ) + sin( radians('$latitude') ) * sin( radians(latitude) ) ) ) <= $distance order by distance";
            $clinics = DB::select(DB::raw($query));

            $doctor_list = array();
            if ($clinics) {
                foreach ($clinics as $clinic) {

                    $clinic = Clinic::find($clinic->id);
                    if ($clinic) {
                        $clinic_lat = $clinic->c_latitude;
                        $clinic_lng = $clinic->c_longitude;
                        $clinic_name = $clinic->clinic_name;
                        $clinic_address = $clinic->c_address;
                        $clinic_image1 = $clinic->c_image1;
                        $clinic_image2 = $clinic->c_image2;
                        $clinic_image3 = $clinic->c_image3;
                        $clinic_phone = $clinic->c_phone;
                        $clinic_fax = $clinic->c_fax;
                        $clinic_id = $clinic->id;

                        $doctors_count = Doctor::where('clinic_id', $clinic->id)
                            ->where('speciality_id',$request->speciality_id)
                            ->count();

                    }

//                  $doc['id'] = $doctor->id;
//                  $doc['d_name'] = $doctor->first_name." ".$doctor->last_name;
//                  $doc['d_photo'] = $doctor->picture;
//                  $doc['phone'] = $doctor->phone;
//                  $doc['email'] = $doctor->email;
                    $doc['d_latitude'] = $clinic_lat;
                    $doc['d_longitude'] = $clinic_lng;
//                  $doc['is_online'] = $doctor->is_booking_online;
//                  $doc['appointment_fee'] = $doctor->booking_fee;
//                  $doc['experience'] = $doctor->experience;
//                  $doc['helped'] = DB::table('user_ratings')->where('doctor_id', $doctor->id)->count();
//                  $rating = DB::table('user_ratings')->where('doctor_id', $doctor->id)->avg('rating') ?: 0;
//                    $doc['rating'] = round($rating,1);
//                  $doc['latitude'] = $doctor->latitude; //remove
//                  $doc['longitude'] = $doctor->longitude; //remove
                    $doc['c_id'] = $clinic_id;
                    $doc['c_name'] = $clinic_name;
//                  $doc['c_city'] = $doctor->c_city;
//                  $doc['c_street'] = $doctor->c_street;
//                  $doc['c_state'] = $doctor->c_state;
//                  $doc['c_country'] = $doctor->c_country;
                    $doc['c_address'] = $clinic_address;
                    $doc['c_phone'] = $clinic_phone;
                    $doc['c_fax'] = $clinic_fax;
//                  $doc['c_postal'] = $doctor->c_postal;
                    $doc['c_pic1'] = $clinic_image1;
                    $doc['c_pic2'] = $clinic_image2;
                    $doc['c_pic3'] = $clinic_image3;
                    $doc['c_count'] = $doctors_count;
//                    $doc['nationality'] = $doctor->nationality;
//                    $doc['degree1'] = $doctor->degree1;
//                    $doc['degree2'] = $doctor->degree2;
//                    $doc['univ1'] = $doctor->univ1;
//                    $doc['univ2'] = $doctor->univ2;

                    //$distance = Helper::getDistanceTime($clinic_lat, $clinic_lng, $request->latitude, $request->longitude);
                    //$distArray = explode("$$", $distance);

                    //$doc['eta'] = $distArray[0] . ' away';
                    //$doc['time'] = $distArray[1];

                    $distance = Helper::vincentyGreatCircleDistance($clinic_lat, $clinic_lng, $request->latitude, $request->longitude);
                    $distance = round($distance);
                    if($distance > 1000){
                        $distance = $distance/1000;
                        $doc['eta'] = $distance.' km away';
                    }else{
                        $doc['eta'] = $distance.' m away';
                    }
                    
                    $doc['time'] = 0;

                    array_push($doctor_list, $doc);
                }
                $response_array = array('success' => true,
                    'source_address' => $address,
                    'doctors' => $doctor_list);
                $response_array = Helper::null_safe($response_array);
            } else {
                $response_array = array('success' => false,
                    'error_message' => 'No doctors available');

            }


        }

        $response = response()->json($response_array, 200);
        return $response;

    }


    public function createRequest(Request $request)
    {

        $basicValidator = Validator::make(
            $request->all(),
            array(
                'request_type' => 'required',
                'doctor_id' => 'required|exists:doctors,id',
                'payment_mode' => 'required'
            ));

        if ($basicValidator->fails()) {
            $error_messages = implode(',', $basicValidator->messages()->all());
            $response_array = array('success' => false, 'error_message' => Helper::get_error_message(101), 'error_code' => 101, 'error_messages' => $error_messages);
        } else {

            // Check already request exists

            $check_status = array(REQUEST_NO_DOCTOR_AVAILABLE, REQUEST_CANCELLED, REQUEST_COMPLETED, 20);

            $check_requests = Requests::where('user_id', $request->id)->whereNotIn('status', $check_status)->where('later', 0)->get();

            foreach($check_requests as $check_request)
            {
                if($meta = RequestsMeta::where('request_id',$check_request->id)->first()) {
                    $meta->delete();
                }
                $check_request->delete();
            }


            if (true) {

                $debt = UserDebt::where('user_id', $request->id)->first();
                $user = User::find($request->id);
                $doctor = Doctor::find($request->doctor_id);

                if (!$debt) {

                    if ($request->request_type == ONDEMAND_CONSULT) {

                        $onlineValidator = Validator::make(
                            $request->all(),
                            array(
                                's_latitude' => 'required',
                                's_longitude' => 'required',
                            ));

                        if ($onlineValidator->fails()) {
                            $error_messages = implode(',', $onlineValidator->messages()->all());
                            $response_array = array('success' => false, 'error_message' => Helper::get_error_message(101), 'error_code' => 101, 'error_messages' => $error_messages);
                        } else {

                            $doctorConsult = DoctorConsult::where('doctor_id', $request->doctor_id)->first();
                            $doctor = Doctor::find($request->doctor_id);


                                $online_consult = new Requests;
                                $online_consult->doctor_id = $request->doctor_id;
                                $online_consult->user_id = $request->id;
                                $online_consult->s_latitude = $request->s_latitude;
                                $online_consult->s_longitude = $request->s_longitude;
                                $online_consult->current_doctor = $request->doctor_id;
                                $online_consult->request_type = $request->request_type;
                                $online_consult->request_start_time = date("Y-m-d H:i:s");
                                $online_consult->status = REQUEST_WAITING;

                                $online_consult->payment_mode = $request->payment_mode;
                                $online_consult->save();

                                $request_meta = new RequestsMeta;
                                $request_meta->request_id = $online_consult->id;
                                $request_meta->service_id = $request->request_type;
                                $request_meta->status = 1;
                                $request_meta->is_cancelled = 0;
                                $request_meta->doctor_id = $request->doctor_id;
                                $request_meta->save();

                                $user = User::find($request->id);


                                //Send Push notification to Doctor
                                $doctor = Doctor::find($request->doctor_id);
                                $deviceToken = $doctor->device_token;
                                $info_data = array();
                                $user_data = array();

                                $user_info['user_id'] = $request->id;
                                $user_info['user_name'] = $user->first_name . " " . $user->last_name;
                                $user_info['user_mobile'] = $user->phone;
                                $user_info['user_email'] = $user->email;
                                $user_info['user_dob'] = $user->dob;
                                $user_info['user_gender'] = $user->gender;
                                $user_info['user_picture'] = $user->picture;
                                $user_info['user_title'] = $user->title;
                                $user_info['user_street'] = $user->b_street;
                                $user_info['user_city'] = $user->b_city;
                                $user_info['user_state'] = $user->b_state;
                                $user_info['user_country'] = $user->b_country;
                                $user_info['user_postal'] = $user->b_postal;
                                $user_info['user_country'] = $user->b_country;
                                $user_info['request_id'] = $online_consult->id;
                                $user_info['s_latitude'] = $online_consult->s_latitude;
                                $user_info['s_longitude'] = $online_consult->s_longitude;
                                $user_info['request_type'] = ONDEMAND_CONSULT;
                                $user_info['doctor_status'] = DOCTOR_NONE;
                                $user_info['promo_bonus'] = 0;
                                $user_info['referral_bonus'] = 0;
                                $user_info['unit'] = '$';


                                $info['user_id'] = $request->id;
                                $info['request_id'] = $online_consult->id;
                                $info['doctor_id'] = $doctor->id;
                                $info['doctor_name'] = $doctor->first_name . " " . $doctor->last_name;
                                $info['doctor_picture'] = $doctor->picture;
                                $rating = DB::table('user_ratings')->where('doctor_id', $doctor->id)->avg('rating') ?: 0;
                                $info['doctor_rating'] = round($rating, 2);
                                $info['s_latitude'] = $online_consult->s_latitude;
                                $info['s_longitude'] = $online_consult->s_longitude;
                                $info['d_latitude'] = $doctor->latitude;
                                $info['d_longitude'] = $doctor->longitude;
                                $info['request_type'] = ONDEMAND_CONSULT;

                                $info_data [] = $info;

                                $user_data[] = $user_info;

                                $invoice_data = array();

                                $inv['doctor_id'] = $request->id;
                                $inv['total_time'] = 0;
                                $inv['payment_mode'] = $online_consult->payment_mode;
                                $inv['base_price'] = 0;
                                $inv['time_price'] = 0;
                                $inv['tax_price'] = 0;
                                $inv['total'] = 0;
                                $inv['distance'] = 0;

                                $invoice_data[] = $inv;

                                $response_array_push = array('success' => true,
                                    'data' => $user_data,
                                    'invoice' => $invoice_data,
                                    'title' => 'new requests'
                                );

                                $response_array = array('success' => true,
                                    'title' => 'new request',
                                    'data' => $info_data,
                                );


                                $response_array = Helper::null_safe($response_array);
                                $response_array_push = Helper::null_safe($response_array_push);

                                
                                \PushNotification::app('appNameAndroid')
                                    ->to($deviceToken)
                                    ->send($response_array_push);
                                
                                
                                


                        }

                    } elseif ($request->request_type == BOOKING) {
                        //Booking

                        $booking = new Requests;
                        $booking->doctor_id = $request->doctor_id;
                        //$booking->confirmed_doctor = $request->doctor_id;
                        $booking->user_id = $request->id;


                    } else {
                        //Online chat,phone,video

                        $doctor = Doctor::find($request->doctor_id);
                        $doctorConsult = DoctorConsult::where('doctor_id', $request->doctor_id)->first();

                        if ($doctor->is_media_online == 0) {
                            goto noDoctor;
                        }

                        if ($doctorConsult) {
                            if ($request->request_type == 1 && $doctorConsult->chat_consult == 0) {
                                goto noDoctor;
                            }

                            if ($request->request_type == 2 && $doctorConsult->phone_consult == 0) {
                                goto noDoctor;
                            }

                            if ($request->request_type == 3 && $doctorConsult->video_consult == 0) {
                                goto noDoctor;
                            }
                        }


                        $payment = Payment::where('user_id', $request->id)
                            ->where('is_default', 1)
                            ->first();
                        if (!$payment) {
                            //no card

                            $response_array = array('success' => false,
                                'error_message' => 'No cards added'
                            );


                        } else {

                            $online_consult = new Requests;
                            $online_consult->doctor_id = $request->doctor_id;
                            $online_consult->user_id = $request->id;
                            $online_consult->current_doctor = $request->doctor_id;
                            $online_consult->request_type = $request->request_type;
                            $online_consult->payment_mode = 'card';
                            $online_consult->save();


                            $request_meta = new RequestsMeta;
                            $request_meta->request_id = $online_consult->id;
                            $request_meta->service_id = $request->request_type;
                            $request_meta->status = 1;
                            $request_meta->is_cancelled = 0;
                            $request_meta->doctor_id = $request->doctor_id;
                            $request_meta->save();


                            $user = User::find($request->id);


                            //Send Push notification to Doctor
                            $doctor = Doctor::find($request->doctor_id);
                            $deviceToken = $doctor->device_token;
                            $info_data = array();
                            $user_data = array();

                            $user_info['user_id'] = $request->id;
                            $user_info['user_name'] = $user->first_name . " " . $user->last_name;
                            $user_info['user_mobile'] = $user->phone;
                            $user_info['user_email'] = $user->email;
                            $user_info['user_dob'] = $user->dob;
                            $user_info['user_gender'] = $user->gender;
                            $user_info['user_picture'] = $user->picture;
                            $user_info['user_title'] = $user->title;
                            $user_info['user_street'] = $user->b_street;
                            $user_info['user_city'] = $user->b_city;
                            $user_info['user_state'] = $user->b_state;
                            $user_info['user_country'] = $user->b_country;
                            $user_info['user_postal'] = $user->b_postal;
                            $user_info['user_country'] = $user->b_country;
                            $user_info['request_id'] = $online_consult->id;
                            $user_info['s_latitude'] = $online_consult->s_latitude;
                            $user_info['s_longitude'] = $online_consult->s_longitude;
                            $user_info['request_type'] = ONDEMAND_CONSULT;
                            $user_info['doctor_status'] = DOCTOR_NONE;
                            $user_info['promo_bonus'] = 0;
                            $user_info['referral_bonus'] = 0;
                            $user_info['unit'] = '$';


                            $info['user_id'] = $request->id;
                            $info['request_id'] = $online_consult->id;
                            $info['doctor_id'] = $doctor->id;
                            $info['doctor_name'] = $doctor->first_name . " " . $doctor->last_name;
                            $info['doctor_picture'] = $doctor->picture;
                            $rating = DB::table('user_ratings')->where('doctor_id', $doctor->id)->avg('rating') ?: 0;
                            $info['doctor_rating'] = round($rating, 2);
                            $info['s_latitude'] = $online_consult->s_latitude;
                            $info['s_longitude'] = $online_consult->s_longitude;
                            $info['d_latitude'] = $doctor->latitude;
                            $info['d_longitude'] = $doctor->longitude;
                            $info['request_type'] = ONDEMAND_CONSULT;

                            $info_data [] = $info;

                            $user_data[] = $user_info;

                            $invoice_data = array();

                            $inv['doctor_id'] = $request->id;
                            $inv['total_time'] = 0;
                            $inv['payment_mode'] = $online_consult->payment_mode;
                            $inv['base_price'] = 0;
                            $inv['time_price'] = 0;
                            $inv['tax_price'] = 0;
                            $inv['total'] = 0;
                            $inv['distance'] = 0;

                            $invoice_data[] = $inv;

                            $response_array_push = array('success' => true,
                                'data' => $user_data,
                                'invoice' => $invoice_data,
                                'title' => 'new requests'
                            );


                            $response_array = array('success' => true,
                                'request_id' => $online_consult->id,
                            );
                            $response_array = Helper::null_safe($response_array);

                            if($doctor->device_type == 'ios'){

                                \PushNotification::app('appNameIOSDoctor')
                                        ->to($deviceToken)
                                        ->send('You have a new request');

                            }else{
                                $push = \PushNotification::app('appNameAndroid')
                                        ->to($deviceToken)
                                        ->send($response_array_push);

                                
                                
                                        

                            }




                        }


                    }

                } else { // debt is there clear debt



                        $response_array = array(
                            'success' => false,
                            'error_message' => 'You have previous payment pending',
                            'error_code' => 568,
                            'amount' => round(Helper::convertMoney($user->id,$doctor->currency,$debt->amount),2)
                        );



                }

            } else { //already request


                $response_array = array('success' => false, 'error_message' => Helper::get_error_message(127), 'error_code' => 127);

            }

        }

        $response = response()->json($response_array, 200);
        return $response;

        noDoctor:

        $response_array = array(
            'success' => false,
            'error_message' => 'Sorry, doctor not available',
            'error_code' => 567
        );

        $response = response()->json($response_array, 200);
        return $response;
    }


    public function createRequestBooking(Request $request)
    {

        $basicValidator = Validator::make(
            $request->all(),
            array(
                'start_time' => 'required',
                'end_time' => 'required',
                'payment_mode' => 'required',
                'request_type' => 'required',
                'doctor_id' => 'required',
                'date' => 'required'
            ));

        if ($basicValidator->fails()) {
            $error_messages = implode(',', $basicValidator->messages()->all());
            $response_array = array('success' => false, 'error_message' => 'article id required', 'error_code' => 101, 'error_messages' => $error_messages);
        } else {

            //check the slots are empty
            $availability = DoctorAvailability::where('doctor_id', $request->doctor_id)->where('available_date', $request->date)->where('start_time', $request->start_time)->where('end_time', $request->end_time)->where('status', 1)->first();
            if ($availability) {

                //create entry in request

                $online_consult = new Requests;
                $online_consult->doctor_id = $request->doctor_id;
                $online_consult->user_id = $request->id;
                $online_consult->current_doctor = $request->doctor_id;
                $online_consult->request_type = $request->request_type;

                $response_array = array('success' => false, 'error_message' => 'Slot unavailable', 'error_code' => 130);


            } else {

                $response_array = array('success' => false, 'error_message' => 'Slot unavailable', 'error_code' => 130);

            }


        }

        return response()->json($response_array, 200);


    }


    public function get_availabilities(Request $request)
    {

        $basicValidator = Validator::make(
            $request->all(),
            array(
                'doctor_id' => 'required'
            ));

        if ($basicValidator->fails()) {
            $error_messages = implode(',', $basicValidator->messages()->all());
            $response_array = array('success' => false, 'error_message' => 'doctor id required', 'error_code' => 101, 'error_messages' => $error_messages);
        } else {

            $avails = DoctorAvailability::where('doctor_id', $request->doctor_id)->where('available_date', $request->date)->where('status', 1)->first();
            $availabilities = DoctorAvailability::where('doctor_id', $request->doctor_id)
                ->where('available_date', $request->date)
                ->where('status', 1)
                ->select('doctor_id', 'id as availability_id', 'available_date', 'start_time', 'end_time', 'status')
                ->get();

            if ($avails) {


                $data = array();

                foreach ($availabilities as $availability) {
                    $doctor_data = array();
                    $doctor_data['id'] = $availability->availability_id;
                    // $doctor_data['title'] = "Available";
                    $doctor_data['date'] = date('Y-m-d', strtotime($availability->available_date));
                    $doctor_data['start_time'] = date('H:i', strtotime($availability->start_time));
                    $doctor_data['end_time'] = date('H:i', strtotime($availability->end_time));
                    //$doctor_data['start'] = date('Y-m-d',strtotime($availability->available_date)).'T'.date('H:i:s',strtotime($availability->start_time)).'Z';
                    //$doctor_data['end'] = date('Y-m-d',strtotime($availability->available_date)).'T'.date('H:i:s',strtotime($availability->end_time)).'Z';
                    $doctor_data['allDay'] = false;
                    if ($availability->status == DOCTOR_AVAILABILITY_BOOKED) {
                        $doctor_data['className'] = "booked";
                    } else {
                        $doctor_data['className'] = "no-booked";
                    }

                    if ($availability->status == DEFAULT_FALSE) {
                        $doctor_data['editable'] = false;
                    }

                    array_push($data, $doctor_data);
                }


                $response_array = Helper::null_safe(array('success' => true, 'data' => $data));
            } else {
                $response_array = array('success' => false, 'error_message' => 'no availability', 'error_code' => 130);
            }

        }

        return response()->json($response_array, 200);

    }


    public function pushTest(Request $request)
    {
        $id = $request->id;
        $user = User::find($id);
        $deviceToken = $user->device_token;

        dd(\PushNotification::app('appNameAndroid')
            ->to('APA91bGijWKzM4CMK2aoR4UsNYjC3JLpZLVvOO-YPRwIHj8hRAIvvBwUCKadqvFxfidbM1uWGTNpewIiXJrYLL55ODTOPE4asJXjxU2vosD7WxlZon1YAQubApYHQThkw4SHXHiNFFQ5')
            ->send('Hello World, i`m a push message'));

       // $response_array = array('success' => true);

        //$response_array = Helper::null_safe($response_array);

        $response = Response::json($response_array, 200);
        return $response;
    }


    public function finishOnlineConsult(Request $request)
    {

    }


    public function getSlots(Request $request)
    {


    }


    public function rate_doctor(Request $request)
    {

        $user = User::find($request->id);

        $validator = Validator::make(
            $request->all(),
            array(
                'request_id' => 'required|integer|exists:requests,id,user_id,' . $user->id ,
                'rating' => 'required|integer|in:' . RATINGS,
                'comments' => 'max:255',
            ),
            array(
                'exists' => 'The :attribute doesn\'t belong to user:' . $user->id,
                'unique' => 'The :attribute already rated.'
            )
        );

        if ($validator->fails()) {
            $error_messages = implode(',', $validator->messages()->all());
            $response_array = array('success' => false, 'error_message' => $error_messages, 'error_code' => 101, 'error_messages' => $error_messages);

        } else {

            $request_id = $request->request_id;
            $comment = $request->comment;

            $req = Requests::where('id', $request_id)
                ->where('is_doctor_rated', 0)
                ->first();
            // ->orWhere('status',WAITING_FOR_DOCTOR_CONFRIMATION_COD)
            // ->orWhere('status',REQUEST_COMPLETED)


            if ($req && intval($req->status) != REQUEST_CANCELLED) {
                //Save Rating
                $rev_user = new UserRating();
                $rev_user->doctor_id = $req->confirmed_doctor;
                $rev_user->user_id = $req->user_id;
                $rev_user->request_id = $request->request_id;
                $rev_user->rating = $request->rating;
                $rev_user->comment = $comment ? $comment : '';
                $rev_user->save();

                $req->is_doctor_rated = 1;
                $req->status = REQUEST_COMPLETED;
                $req->save();


                // Send Push Notification to Provider
                // $title = Helper::tr('provider_rated_by_user_title');
                // $doctor = Doctor::find($req->confirmed_doctor);
                // $deviceToken = $doctor->device_token;


                $response_array = array('success' => true, 'message' => 'doctor rated');

            } else {
                $response_array = array('success' => false, 'error_message' => Helper::get_error_message(150), 'error_code' => 150);
            }
        }

        $response = response()->json($response_array, 200);
        return $response;
    }


    public function getArticleCategory(Request $request)
    {

        $articleCategories = ArticleCategory::all();

        $categoryArray = array();

        foreach ($articleCategories as $articleCategory) {
            $category['id'] = $articleCategory->id;
            $category['category'] = $articleCategory->article_category;
            $category['picture'] = $articleCategory->picture;

            array_push($categoryArray, $category);
        }
        $response_array = array('success' => true, 'article_category' => $categoryArray);
        $response = response()->json($response_array, 200);
        return $response;

    }


    public function getArticles(Request $request)
    {


        $article_array = array();

        if (!$request->has('skip'))
            $skip = 0;
        else
            $skip = $request->skip;

        if (!$request->has('take'))
            $take = 60;
        else
            $take = $request->take;

        if ($request->article_category_id) {
            $articles = DB::table('articles')
                ->where('article_category_id', $request->article_category_id)
                ->where('is_approved',1)
                ->skip($skip)
                ->take($take)
                ->get();

            $count = DB::table('articles')
                ->where('article_category_id', $request->article_category_id)
                ->where('is_approved',1)
                ->skip($skip)
                ->take($take)
                ->count();


        } else {

            $articles = DB::table('articles')
                ->where('is_approved',1)
                ->skip($skip)
                ->take($take)
                ->get();

            $count = DB::table('articles')
                ->where('is_approved',1)
                ->skip($skip)
                ->take($take)
                ->count();

        }


        $article_array = array();

        foreach ($articles as $article) {
            $doctor = Doctor::find($article->doctor_id);
            $articleCategory = ArticleCategory::find($article->article_category_id);

            $art['article_id'] = $article->id;
            $art['doctor_name'] = $doctor->first_name . " " . $doctor->last_name;
            $art['category'] = $articleCategory->article_category;
            $art['picture'] = $article->picture;
            $art['heading'] = $article->heading;

            $articleBookmark = ArticleBookmark::where('user_id', $request->id)
                ->where('article_id', $article->id)
                ->first();
            if ($articleBookmark) {
                $art['is_bookmarked'] = '1';
            } else {
                $art['is_bookmarked'] = '0';
            }


            array_push($article_array, $art);

        }

        $response_array = array('success' => true,
            'count' => $count,
            'articles' => $article_array);
        $response_array = Helper::null_safe($response_array);


        $response = response()->json($response_array, 200);
        return $response;
    }


    public function getOneArticle(Request $request)
    {

        $basicValidator = Validator::make(
            $request->all(),
            array(
                'article_id' => 'required|exists:articles,id',
            ));

        if ($basicValidator->fails()) {
            $error_messages = implode(',', $basicValidator->messages()->all());
            $response_array = array('success' => false, 'error_message' => 'article id required', 'error_code' => 101, 'error_messages' => $error_messages);
        } else {

            $article = Article::find($request->article_id);

            $doctor = Doctor::find($article->doctor_id);
            $articleCategory = ArticleCategory::find($article->article_category_id);

            $articleBookmark = ArticleBookmark::where('user_id', $request->id)
                ->where('article_id', $request->article_id)
                ->first();
            if ($articleBookmark) {
                $is_bookmarked = '1';
            } else {
                $is_bookmarked = '0';
            }

            $response_array = array('success' => true,
                'article_id' => $article->id,
                'doctor_name' => $doctor->first_name . " " . $doctor->last_name,
                'category' => $articleCategory->article_category,
                'picture' => $article->picture,
                'heading' => $article->heading,
                'content' => $article->content,
                'is_bookmarked' => $is_bookmarked,
            );
            $response_array = Helper::null_safe($response_array);


        }

        $response = response()->json($response_array, 200);
        return $response;

    }


    public function request_status_check(Request $request)
    {

        $validator = Validator::make(
            $request->all(),
            array(
                'request_id' => 'required|numeric|exists:requests,id',
            ));

        if ($validator->fails()) {
            $error_messages = implode(',', $validator->messages()->all());
            $response_array = array('success' => false, 'error' => $error_messages, 'error_messages' => Helper::get_error_message(101), 'error_code' => 101);
        } else {
            $requests = Requests::find($request->request_id);
            $doctor = Doctor::find($requests->confirmed_doctor);
            $user = User::find($request->id);

            $clinic = Clinic::find($doctor->clinic_id);

            $clinic_lat = $clinic->c_latitude;
            $clinic_lng = $clinic->c_longitude;
            $clinic_address = $clinic->c_address;

            $json1 = json_decode(file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?latlng=' . $requests->s_latitude . ',' . $requests->s_longitude . '&key=AIzaSyC4AEUI8uxPs42kVRzSUb4B718ZK0hKxew'), true);
            if (!$json1['results']) {
                $source_address = '';
            } else {
                $source_address = $json1['results'][0]['formatted_address'];
            }

            $json2 = json_decode(file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?latlng=' . $clinic_lat . ',' . $clinic_lng . '&key=AIzaSyC4AEUI8uxPs42kVRzSUb4B718ZK0hKxew'), true);
            if (!$json2['results']) {
                $c_address = '';
            } else {
                $c_address = $json2['results'][0]['formatted_address'];
            }

            if ($requests->s_latitude != 0 && $requests->s_longitude != 0) {
                $distance = Helper::getDistanceTime($doctor->latitude, $doctor->longitude, $requests->s_latitude, $requests->s_longitude);
                $distArray = explode("$$", $distance);
                $eta = $distArray[0] . ' away';
                $times = $distArray[1];
            } else {
                $eta = 0;
                $times = 0;
            }

            //Get Distance time
//            $distance =Helper::getDistanceTime($doctor->latitude,$doctor->longitude,$requests->s_latitude,$requests->s_longitude);
//            $distArray = explode("$$",$distance);


            $doctorData = array(
                'request_id' => $requests->id,
                'user_id' => $requests->user_id,
                'doctor_id' => $requests->doctor_id,
                'status' => $requests->status,
                'doctor_status' => $requests->doctor_status,
                's_longitude' => $requests->s_longitude,
                's_latitude' => $requests->s_latitude,
                'doctor_name' => $doctor->first_name . " " . $doctor->last_name,
                'doctor_picture' => $doctor->picture,
                'doctor_mobile' => $doctor->phone,
                'd_latitude' => $doctor->latitude,
                'd_longitude' => $doctor->longitude,
                'c_address' => $c_address,
                'source_address' => $source_address,
                'eta' => $eta,
                'time' => $times,
                'doctor_rating' => round(DoctorRating::where('doctor_id', $doctor->id)->avg('rating'), 2) ?: 0,
            );

            $invoice_data = array();

            $doctorFee = DoctorFee::where('doctor_id', $doctor->id)->first();
            if ($doctorFee) {
                $ondemand_fee = $doctorFee->ondemand_fee;
            } else {
                $ondemand_fee = 0;
            }

            $setting = Setting::find(1);
            if ($setting) {
                $fee_km = $setting->fee_km;
            } else {
                $fee_km = 0;
            }

            $distance_price = $fee_km * $requests->distance;
            $total = $requests->amount;
            $distance = $requests->real_distance;

            $inv['doctor_id'] = $request->id;
            $inv['doctor_name'] = $doctor->first_name . " " . $doctor->last_name;
            $inv['doctor_picture'] = $doctor->picture;
            $inv['total_time'] = $requests->duration;
            $inv['payment_mode'] = $requests->payment_mode;
            $inv['base_price'] = round(Helper::convertMoney($user->id,$doctor->currency,$ondemand_fee),2);
            $inv['per_km'] = $fee_km;
            $inv['distance_cost'] = $distance_price;
            $inv['time_price'] = 0;
            $inv['tax_price'] = 0;
            $inv['total'] = round(Helper::convertMoney($user->id,$doctor->currency,$total),2);
            $inv['distance'] = $distance;
            $inv['medicine_fee'] = 0;
            $inv['referral_bonus'] = 0;
            $inv['promo_bonus'] = 0;

            $invoice_data[] = $inv;

            //video call ended
            if (($requests->request_type == 3 && $requests->doctor_status == 5 && $requests->is_doctor_rated == 0) || ($requests->request_type == 3 && $requests->doctor_status == 6 && $requests->is_doctor_rated == 0)) {

                $doctorFee = DoctorFee::where('doctor_id', $doctor->id)->first();
                if ($doctorFee) {
                    $base_fee = $doctorFee->video_fee;
                } else {
                    $base_fee = 0;
                }

                $invoice_data = array();

                $inv['doctor_id'] = $request->id;
                $inv['doctor_name'] = $doctor->first_name . " " . $doctor->last_name;
                $inv['doctor_picture'] = $doctor->picture;
                $inv['total_time'] = $requests->duration;
                $inv['payment_mode'] = $requests->payment_mode;
                $inv['base_price'] = round(Helper::convertMoney($user->id,$doctor->currency,$base_fee),2);
                $inv['time_price'] = 0;
                $inv['tax_price'] = 0;
                $inv['total'] = round(Helper::convertMoney($user->id,$doctor->currency,$requests->amount),2);
                $inv['distance'] = 0;
                $inv['medicine_fee'] = 0;
                $inv['referral_bonus'] = 0;
                $inv['promo_bonus'] = 0;

                $invoice_data[] = $inv;


                $response_array = Helper::null_safe(array(
                    'success' => true,
                    'request_id' => $requests->id,
                    'title' => 'check status',
                    'data' => $doctorData,
                    'status' => DOCTOR_ENDED_VIDEO,
                    'invoice' => $invoice_data
                ));

            } elseif (($requests->request_type == 2 && $requests->doctor_status == 5 && $requests->is_doctor_rated == 0) || ($requests->request_type == 2 && $requests->doctor_status == 6 && $requests->is_doctor_rated == 0)) {
                # Audio call ended...

                $doctorFee = DoctorFee::where('doctor_id', $doctor->id)->first();
                if ($doctorFee) {
                    $base_fee = $doctorFee->phone_fee;
                } else {
                    $base_fee = 0;
                }

                $invoice_data = array();

                $inv['doctor_id'] = $request->id;
                $inv['doctor_name'] = $doctor->first_name . " " . $doctor->last_name;
                $inv['doctor_picture'] = $doctor->picture;
                $inv['total_time'] = $requests->duration;
                $inv['payment_mode'] = $requests->payment_mode;
                $inv['base_price'] = round(Helper::convertMoney($user->id,$doctor->currency,$base_fee),2);
                $inv['time_price'] = 0;
                $inv['tax_price'] = 0;
                $inv['total'] = round(Helper::convertMoney($user->id,$doctor->currency,$requests->amount),2);
                $inv['distance'] = 0;
                $inv['medicine_fee'] = 0;
                $inv['referral_bonus'] = 0;
                $inv['promo_bonus'] = 0;

                $invoice_data[] = $inv;

                $response_array = Helper::null_safe(array(
                    'success' => true,
                    'request_id' => $requests->id,
                    'title' => 'check status',
                    'data' => $doctorData,
                    'status' => DOCTOR_ENDED_PHONE,
                    'invoice' => $invoice_data
                ));
            } elseif (($requests->request_type == 1 && $requests->doctor_status == 5 && $requests->is_doctor_rated == 0) || ($requests->request_type == 1 && $requests->doctor_status == 6 && $requests->is_doctor_rated == 0)) {
                # chat ended

                $doctorFee = DoctorFee::where('doctor_id', $doctor->id)->first();
                if ($doctorFee) {
                    $base_fee = $doctorFee->chat_fee;
                } else {
                    $base_fee = 0;
                }

                $invoice_data = array();

                $inv['doctor_id'] = $request->id;
                $inv['doctor_name'] = $doctor->first_name . " " . $doctor->last_name;
                $inv['doctor_picture'] = $doctor->picture;
                $inv['total_time'] = $requests->duration;
                $inv['payment_mode'] = $requests->payment_mode;
                $inv['base_price'] = round(Helper::convertMoney($user->id,$doctor->currency,$base_fee),2);
                $inv['time_price'] = 0;
                $inv['tax_price'] = 0;
                $inv['total'] = round(Helper::convertMoney($user->id,$doctor->currency,$requests->amount),2);
                $inv['distance'] = 0;
                $inv['medicine_fee'] = 0;
                $inv['referral_bonus'] = 0;
                $inv['promo_bonus'] = 0;

                $invoice_data[] = $inv;

                $response_array = Helper::null_safe(array(
                    'success' => true,
                    'request_id' => $requests->id,
                    'title' => 'check status',
                    'data' => $doctorData,
                    'status' => DOCTOR_ENDED_CHAT,
                    'invoice' => $invoice_data
                ));

            } elseif ($requests->request_type == 1 && $requests->doctor_status == 1 && $requests->is_doctor_rated == 0) {
                # chat running

                $response_array = Helper::null_safe(array(
                    'success' => true,
                    'request_id' => $requests->id,
                    'title' => 'check status',
                    'data' => $doctorData,
                    'status' => DOCTOR_ACCEPTED_CHAT,
                    'invoice' => $invoice_data
                ));

            } elseif ($requests->request_type == 2 && $requests->doctor_status == 1 && $requests->is_doctor_rated == 0) {
                # audio running

                $response_array = Helper::null_safe(array(
                    'success' => true,
                    'request_id' => $requests->id,
                    'title' => 'check status',
                    'data' => $doctorData,
                    'status' => DOCTOR_ACCEPTED_PHONE,
                    'invoice' => $invoice_data
                ));

            } elseif ($requests->request_type == 3 && $requests->doctor_status == 1 && $requests->is_doctor_rated == 0) {
                # video running

                $response_array = Helper::null_safe(array(
                    'success' => true,
                    'request_id' => $requests->id,
                    'title' => 'check status',
                    'data' => $doctorData,
                    'status' => DOCTOR_ACCEPTED_VIDEO,
                    'invoice' => $invoice_data
                ));

            } elseif ($requests->request_type == 5 && $requests->is_doctor_rated == 0) {
                # booking ended

                $invoice_data = array();

                $inv['doctor_id'] = $request->id;
                $inv['doctor_name'] = $doctor->first_name . " " . $doctor->last_name;
                $inv['doctor_picture'] = $doctor->picture;
                $inv['total_time'] = $requests->duration;
                $inv['payment_mode'] = $requests->payment_mode;
                $inv['base_price'] = round(Helper::convertMoney($user->id,$doctor->currency,$doctor->booking_fee),2);
                $inv['time_price'] = 0;
                $inv['tax_price'] = 0;
                $inv['total'] = round(Helper::convertMoney($user->id,$doctor->currency,$doctor->booking_fee),2);
                $inv['distance'] = 0;
                $inv['medicine_fee'] = 0;
                $inv['referral_bonus'] = 0;
                $inv['promo_bonus'] = 0;


                $invoice_data[] = $inv;

                $response_array = Helper::null_safe(array(
                    'success' => true,
                    'request_id' => $requests->id,
                    'title' => 'check status',
                    'data' => $doctorData,
                    'status' => 30,
                    'invoice' => $invoice_data
                ));

            } else {

                $response_array = Helper::null_safe(array(
                    'success' => true,
                    'request_id' => $requests->id,
                    'title' => 'check status',
                    'data' => $doctorData,
                    'status' => $requests->doctor_status,
                    'invoice' => $invoice_data
                ));

            }


        }

        $response = response()->json($response_array, 200);
        return $response;


    }

    public function checkRequestAvailable(Request $request)
    {
        $requests = Requests::where('user_id', $request->id)
            ->whereIn('status',[0,1,2,3,4,5,8])
            ->where('is_doctor_rated', 0)
            ->where('doctor_status', '>=', DOCTOR_ACCEPTED)
            ->where('confirmed_doctor', '!=', 0)
            ->orderBy('created_at', 'desc')
            ->first();

        //->where('doctor_status','>=',DOCTOR_ACCEPTED)
        //->orWhere('status','=',WAITING_FOR_DOCTOR_CONFRIMATION_COD)
        if ($requests) {
            $response_array = Helper::null_safe(array(
                'success' => true,
                'request_id' => $requests->id,
            ));


        } else {

            $response_array = array(
                'success' => true,
                'request_id' => ' '
            );

        }

        $response = response()->json($response_array, 200);
        return $response;

    }


    public function getAvailableSlots(Request $request)
    {

        $validator = Validator::make(
            $request->all(),
            array(
                'date' => 'required',
                'doctor_id' => 'required'
            ));

        if ($validator->fails()) {
            $error_messages = implode(',', $validator->messages()->all());
            $response_array = array('success' => false, 'error_message' => $error_messages, 'error_code' => 101, 'error_messages' => $error_messages);

        } else {
            $availableSlots = AvailableSlot::where('doctor_id', $request->doctor_id)
                ->whereDate('date','=', $request->date)
                ->where('slot_status',1)
                ->where('booked', false)
                ->get();


            $doctorSlotArray = array();

            foreach ($availableSlots as $availableSlot) {

                $doctorSlot = DoctorSlot::find($availableSlot->slot_id);

                if ($doctorSlot) {

                    $slotData['available_slot_id'] = $availableSlot->id;
                    $slotData['id'] = $doctorSlot->id;
                    $slotData['type'] = $doctorSlot->type;
                    $slotData['start_time'] = $doctorSlot->start_time;
                    $slotData['end_time'] = $doctorSlot->end_time;
                    $slotData['booked'] = $availableSlot->booked;


                }
                array_push($doctorSlotArray, $slotData);
            }

            $doctor = Doctor::find($request->doctor_id);
            $user = User::find($request->id);
            $doctorArray = array();

            $info['doctor_id'] = $doctor->id;
            $info['doctor_name'] = $doctor->first_name . " " . $doctor->last_name;
            $rating = DB::table('user_ratings')->where('doctor_id', $doctor->id)->avg('rating') ?: 0;
            $info['doctor_rating'] = round($rating, 2);
            $info['c_name'] = $doctor->c_name;
            $info['c_city'] = $doctor->c_city;
            $info['c_street'] = $doctor->c_street;
            $info['c_state'] = $doctor->c_state;
            $info['c_country'] = $doctor->c_country;
            $info['c_postal'] = $doctor->c_postal;
            $info['booking_fee'] = round(Helper::convertMoney($user->id,$doctor->currency,$doctor->booking_fee),2);

            array_push($doctorArray, $info);


            if (count($doctorSlotArray) == 0) {
                $response_array = array(
                    'success' => true,
                    'error_message' => 'No slots availble',
                );
            } else {

                $response_array = array(
                    'success' => true,
                    'slots' => $doctorSlotArray,
                    'doctor_data' => $doctorArray
                );

            }

            $response_array = Helper::null_safe($response_array);

        }

        $response = response()->json($response_array, 200);
        return $response;

    }


    public function bookSlots(Request $request)
    {

        $validator = Validator::make(
            $request->all(),
            array(
                'available_slot_ids' => 'required',
                'payment_mode' => 'required',
                's_latitude' => 'required',
                's_longitude' => 'required'
            ));

        if ($validator->fails()) {
            $error_messages = implode(',', $validator->messages()->all());
            $response_array = array('success' => false, 'error_message' => $error_messages, 'error_code' => 101, 'error_messages' => $error_messages);

        } else {
            $debt = UserDebt::where('user_id', $request->id)->first();
            $avail = AvailableSlot::find($request->available_slot_ids);
            $doctor = Doctor::find($avail->doctor_id);
            $user = User::find($request->id);

            if (!$debt) {

                $available_slot_ids = explode(",", $request->available_slot_ids);

                foreach ($available_slot_ids as $available_slot_id) {

                    $availableSlot = AvailableSlot::where('id', $available_slot_id)
                        ->where('booked', 0)
                        ->first();

                    if ($availableSlot) {
                        $availableSlot->slot_status = 0;
                        $availableSlot->booked = true;
                        $availableSlot->save();


                        $slot = DoctorSlot::find($availableSlot->slot_id);
                        if ($slot->type == 'm')
                            $start_time = $slot->start_time . 'am';
                        else
                            $start_time = $slot->start_time . 'pm';
                        $date = $availableSlot->date;
                        $date_temp = strtotime($date);
                        $date_formatted = date('Y-m-d',$date_temp);


                        $s_time = strtotime($start_time);
                        $start = date('H:i', $s_time);

                        $start_datetime = $date . " " . $start;
                        $booking_start_time = $date_formatted." ".$start;
                        
                        $requests = new Requests;
                        $requests->doctor_id = $availableSlot->doctor_id;
                        $requests->user_id = $request->id;
                        $requests->current_doctor = $availableSlot->doctor_id;
                        $requests->confirmed_doctor = $availableSlot->doctor_id;
                        $requests->request_type = 5;
                        $requests->payment_mode = $request->payment_mode;
                        $requests->later = 1;
                        $requests->status = 20;
                        $requests->doctor_status = 20;
                        $requests->start_time = $start_datetime;
                        $requests->request_start_time = $booking_start_time;
                        $requests->s_latitude = $request->s_latitude;
                        $requests->s_longitude = $request->s_longitude;
                        $requests->save();

                        $availableSlot->request_id = $requests->id;
                        $availableSlot->save();


                    } else { //already booked

                        goto dontAllow;

                    }

                }


                $response_array = array(
                    'success' => true,
                    'request_id' => $availableSlot->request_id
                );
                $response_array = Helper::null_safe($response_array);

                $requestData = array();
                $info['request_type'] = 5;
                $info['request_id'] = $availableSlot->request_id;
                $info['date'] = $date_formatted;
                array_push($requestData, $info);

                $push_array = array('success' => true,
                    'title' => 'You have new booking,please check My Schedule',
                    'data' => $requestData
                );

                $doctor = Doctor::find($availableSlot->doctor_id);
                $deviceToken = $doctor->device_token;
                if($doctor->device_type == 'ios')
                {
                    $push =\PushNotification::app('appNameIOSDoctor')
                        ->to($deviceToken)
                        ->send('You have a new appointment');

                }else{
                    \PushNotification::app('appNameAndroid')
                    ->to($deviceToken)
                    ->send($push_array);

                }



            } else {
                //debt is there charge user
                // debt is there clear debt



                $response_array = array(
                    'success' => false,
                    'error_message' => 'You have previous payment pending',
                    'amount' => round(Helper::convertMoney($user->id,$doctor->currency,$debt->amount),2)
                );





            }

        }

        $response = response()->json($response_array, 200);
        return $response;

        dontAllow:

        $response_array = array(
            'success' => false,
            'error_message' => 'Slot was already booked,please try again'
        );

        $response = response()->json($response_array, 200);
        return $response;


    }

    public function mySchedule(Request $request)
    {

        $validator = Validator::make(
            $request->all(),
            array(
                'date' => 'required',
            ));

        if ($validator->fails()) {
            $error_messages = implode(',', $validator->messages()->all());
            $response_array = array('success' => false, 'error_message' => $error_messages, 'error_code' => 101, 'error_messages' => $error_messages);

        } else {

            $date = new DateTime(date('Y-m-d H:i:s'));
            $date->setTimezone(new \DateTimeZone(env('TIMEZONE')));

            $availableSlots = DB::table('available_slots')
                ->join('requests', 'available_slots.request_id', '=', 'requests.id')
                ->select('requests.confirmed_doctor as doctor_id', 'available_slots.slot_id as slot_id', 'available_slots.date as date')
                ->whereDate('available_slots.date', '=', $request->date)
                ->where('requests.user_id', '=', $request->id)
                ->where('available_slots.booked', '=', 1)
                //->where('available_slots.date', '>=', $date->format('Y-m-d'))
                ->get();

            $slotArray = array();

            foreach ($availableSlots as $availableSlot) {


                $doctor = Doctor::find($availableSlot->doctor_id);
                $doctorSlot = DoctorSlot::find($availableSlot->slot_id);
                if ($doctorSlot->type == 'm') {
                    $ampm = 'am';

                } else {
                    $ampm = 'pm';
                }
                $info['doctor_name'] = $doctor->first_name . " " . $doctor->last_name;
                $info['c_name'] = $doctor->c_name;
                $info['c_street'] = $doctor->c_street;
                $info['c_city'] = $doctor->c_city;
                $info['c_state'] = $doctor->c_state;
                $info['c_country'] = $doctor->c_country;
                $info['c_postal'] = $doctor->c_postal;
                $info['start_time'] = $doctorSlot->start_time;
                $info['ampm'] = $ampm;
                $info['end_time'] = $doctorSlot->end_time;
                $info['date'] = date('Y-m-d',strtotime($availableSlot->date));

                array_push($slotArray, $info);


            }

            $response_array = array(
                'success' => true,
                'data' => $slotArray,
            );


        }

        $response = response()->json($response_array, 200);
        return $response;


    }


    public function getScheduleDates(Request $request)
    {

        $date = new DateTime(date('Y-m-d H:i:s'));
        $date->setTimezone(new \DateTimeZone(env('TIMEZONE')));


        $availableSlots = DB::table('available_slots')
            ->join('requests', 'available_slots.request_id', '=', 'requests.id')
            ->select('requests.confirmed_doctor as doctor_id', 'available_slots.slot_id as slot_id', 'available_slots.date as date')
            ->where('requests.user_id', '=', $request->id)
            ->where('requests.status', '=', 20)
            ->where('available_slots.booked', '=', 1)
            ->where('available_slots.date', '>=', $date->format('Y-m-d'))
            ->get();


        $slotArray = array();

        foreach ($availableSlots as $availableSlot) {
            $slot['slot_id'] = $availableSlot->slot_id;
            $slot['date'] = date('Y-m-d',strtotime($availableSlot->date));
            array_push($slotArray, $slot);
        }

        $response_array = array(
            'success' => true,
            'data' => $slotArray,
        );

        $response = response()->json($response_array, 200);
        return $response;

    }


    public function userAddCard(Request $request)
    {
        $payment_method_nonce = $request->payment_method_nonce;
        $user = User::find($request->id);
        $payment = Payment::where('user_id', $request->id)->first();


        try {

            if (!$payment) {

                $result = Braintree_Customer::create(array(
                    'firstName' => $user->first_name,
                    'lastName' => $user->last_name,
                    'paymentMethodNonce' => $payment_method_nonce
                ));
                //dd($result->customer->creditCards[0]->cardType);

                if ($result->success) {

                    Log::info('New User creation success');
                    if ($result->customer->creditCards) {

                        $payment = new Payment;
                        $payment->user_id = $request->id;
                        $payment->customer_id = $result->customer->id;
                        $payment->payment_method_nonce = $request->payment_method_nonce;
                        $payment->last_four = (string)$result->customer->creditCards[0]->last4;
                        $payment->is_default = 1;
                        $payment->card_type = $result->customer->creditCards[0]->cardType;
                        $payment->save();

                        $response_array = array('success' => true, 'message' => 'Thank you for adding your first card.');


                    } elseif ($result->customer->paypalAccounts) {
                        //adding paypal
                        $payment = new Payment;
                        $payment->user_id = $request->id;
                        $payment->customer_id = $result->customer->id;
                        $payment->payment_method_nonce = $request->payment_method_nonce;
                        $payment->paypal_email = $result->customer->paypalAccounts[0]->email;
                        $payment->card_type = 'na';
                        $payment->save();
                        $response_array = array('success' => true, 'message' => 'Thank you for adding your paypal account.');
                    }

                } else {// $result->success failed

                    $response_array = array('success' => true, 'message' => 'Braintree Adding Card Error: ' . $result->message);

                }


            } else { //if payment exist in payment table
                $payments = Payment::where('user_id', $request->id)->get();
                if($payments){
                    foreach ($payments as $data) {
                    $data->is_default = 0;
                    $data->save();
                }

                }
                

                $customer_id = $payment->customer_id;
                $result = Braintree_PaymentMethod::create(array(
                    'customerId' => $customer_id,
                    'paymentMethodNonce' => $payment_method_nonce
                ));

                if ($result->success) {

                    if (preg_match('/Braintree_CreditCard/', $result->paymentMethod)) {
                        //credit card

                        $payment = new Payment;
                        $payment->user_id = $request->id;
                        $payment->customer_id = $customer_id;
                        $payment->payment_method_nonce = $request->payment_method_nonce;
                        $payment->last_four = (string)$result->paymentMethod->last4;
                        $payment->card_type = $result->paymentMethod->cardType;
                        $payment->is_default = 1;
                        $payment->save();


                        $response_array = array('success' => true, 'message' => 'Thank you for adding your card.');


                    } elseif (preg_match('/Braintree_PayPalAccount/', $result->paymentMethod)
                    ) {
                        //paypal
                        $payment = new Payment;
                        $payment->user_id = $request->id;
                        $payment->customer_id = $customer_id;
                        $payment->payment_method_nonce = $request->payment_method_nonce;
                        $payment->paypal_email = $result->customer->paypalAccounts[0]->email;
                        $payment->card_type = 'na';
                        $payment->save();
                        $response_array = array('success' => true, 'message' => 'Thank you for adding your paypal account.');
                    }
                } else {
                    // failed

                    $response_array = array('success' => true, 'message' => 'Braintree Adding Card Error: ' . $result->message);
                }
            }

        } catch (Braintree_Exception_Authorization $e) {
            Log::error('Error = ' . $e->getMessage());
            $response_array = array('success' => true, 'message' => 'Something went wrong. Please try again later or contact us.');
        }

        $response = response()->json($response_array, 200);
        return $response;

    }


    public function testPayment(Request $request)
    {
        $payment = Payment::where('user_id', $request->id)->first();


        $trans = Helper::createTransaction($payment->customer_id, 30, 10);
        dd($trans);
    }


    public function getCards(Request $request)
    {
        $payment = Payment::where('user_id', $request->id)->first();
        $cardArray = array();
        if ($payment) {

            $payment_data = Payment::where('user_id', $request->id)
                ->where('is_deleted', 0)
                ->get();

            foreach ($payment_data as $pay) {
                $card['id'] = $pay->id;
                $card['customer_id'] = $pay->customer_id;
                if ($pay->last_four) {
                    $card['last_four'] = $pay->last_four;
                    $card['type'] = 'card';
                    $card['card_type'] = $pay->card_type;
                    $card['email'] = '';

                } else {
                    $card['last_four'] = '0';
                    $card['type'] = 'paypal';
                    $card['email'] = $pay->paypal_email;
                    $card['card_type'] = $pay->card_type;
                }

                $card['is_default'] = $pay->is_default;
                array_push($cardArray, $card);
            }


            $response_array = array(
                'success' => true,
                'cards' => $cardArray
            );
        } else {
            //no payments
            $response_array = array(
                'success' => false,
                'error_message' => 'No Card Found'
            );
        }

        $response = response()->json($response_array, 200);
        return $response;

    }

    public function selectCard(Request $request)
    {

        $validator = Validator::make(
            $request->all(),
            array(
                'last_four' => 'required',
            ));

        if ($validator->fails()) {
            $error_messages = implode(',', $validator->messages()->all());
            $response_array = array('success' => false, 'error_message' => $error_messages, 'error_code' => 101, 'error_messages' => $error_messages);

        } else {

            $payments = Payment::where('user_id', $request->id)->get();
            foreach ($payments as $data) {
                $data->is_default = 0;
                $data->save();
            }

            $payment = Payment::where('last_four', $request->last_four)->where('user_id', $request->id)->where('is_deleted',0)->first();
            $payment->is_default = 1;
            $payment->save();

            $response_array = array(
                'success' => true
            );

        }
        $response = response()->json($response_array, 200);
        return $response;

    }

    public function deleteCard(Request $request)
    {

        $validator = Validator::make(
            $request->all(),
            array(
                'card_id' => 'required',
            ));

        if ($validator->fails()) {
            $error_messages = implode(',', $validator->messages()->all());
            $response_array = array('success' => false, 'error_message' => $error_messages, 'error_code' => 101, 'error_messages' => $error_messages);

        } else {

            $payment = Payment::find($request->card_id);
            if ($payment) {
                $payment->is_deleted = 1;
                $payment->is_default = 0;
                $payment->save();
                $response_array = array(
                    'success' => true,
                    'message' => 'Card deleted succesfully'
                );
            } else {
                $response_array = array(
                    'success' => false,
                    'error_message' => 'wrong card id'
                );

            }

        }
        $response = response()->json($response_array, 200);
        return $response;
    }


    public function getBraintreeToken(Request $request)
    {
        $clientToken = Braintree_ClientToken::generate();
        $response_array = array(
            'success' => true,
            'client_token' => $clientToken
        );
        $response = response()->json($response_array, 200);
        return $response;

    }


    public function consultHistoryChat(Request $request)
    {

        if (!$request->has('skip'))
            $skip = 0;
        else
            $skip = $request->skip;

        if (!$request->has('take'))
            $take = 1000;
        else
            $take = $request->take;

        $requests = Requests::where('request_type', 1)
            ->where('status', 5)
            ->where('user_id', $request->id)
            ->skip($skip)
            ->take($take)
            ->get();

        $requestArray = array();

        foreach ($requests as $key) {
            $doctor = Doctor::find($key->confirmed_doctor);
            $doctorFee = DoctorFee::where('doctor_id', $doctor->id)->first();
            $user = User::find($request->id);

            $fee = $key->amount;


            $info['id'] = $key->id;
            $info['type'] = 'chat';
            $info['doctor_name'] = $doctor->first_name . " " . $doctor->last_name;
            $info['doctor_picture'] = $doctor->picture;
            $info['phone'] = $doctor->phone;
            $time = Helper::formatHour($key->created_at);
            $date = Helper::formatDate($key->created_at);
            $info['date'] = $date;
            $info['time'] = $time;
            $info['duration'] = $key->duration;
            $info['fee'] = round(Helper::convertMoney($user->id,$doctor->currency,$fee),2);

            array_push($requestArray, $info);
        }

        $response_array = array(
            'success' => true,
            'data' => $requestArray,
        );

        $response = response()->json($response_array, 200);
        return $response;

    }


    public function consultHistoryPhone(Request $request)
    {

        if (!$request->has('skip'))
            $skip = 0;
        else
            $skip = $request->skip;

        if (!$request->has('take'))
            $take = 1000;
        else
            $take = $request->take;

        $requests = Requests::where('request_type', 2)
            ->where('status', 5)
            ->where('user_id', $request->id)
            ->skip($skip)
            ->take($take)
            ->get();

        $requestArray = array();

        foreach ($requests as $key) {
            $doctor = Doctor::find($key->confirmed_doctor);
            $doctorFee = DoctorFee::where('doctor_id', $doctor->id)->first();
            $user = User::find($request->id);

            $fee = $key->amount;


            $info['id'] = $key->id;
            $info['type'] = 'phone';
            $info['doctor_name'] = $doctor->first_name . " " . $doctor->last_name;
            $info['doctor_picture'] = $doctor->picture;
            $info['phone'] = $doctor->phone;
            $time = Helper::formatHour($key->created_at);
            $date = Helper::formatDate($key->created_at);
            $info['date'] = $date;
            $info['time'] = $time;
            $info['duration'] = $key->duration;
            $info['fee'] = round(Helper::convertMoney($user->id,$doctor->currency,$fee),2);

            array_push($requestArray, $info);
        }

        $response_array = array(
            'success' => true,
            'data' => $requestArray,
        );

        $response = response()->json($response_array, 200);
        return $response;

    }

    public function consultHistoryVideo(Request $request)
    {

        if (!$request->has('skip'))
            $skip = 0;
        else
            $skip = $request->skip;

        if (!$request->has('take'))
            $take = 1000;
        else
            $take = $request->take;

        $requests = Requests::where('request_type', 3)
            ->where('status', 5)
            ->where('user_id', $request->id)
            ->skip($skip)
            ->take($take)
            ->get();

        $requestArray = array();

        $user = User::find($request->id);

        foreach ($requests as $key) {
            $doctor = Doctor::find($key->confirmed_doctor);
            $doctorFee = DoctorFee::where('doctor_id', $doctor->id)->first();


            $fee = $key->amount;


            $info['id'] = $key->id;
            $info['type'] = 'video';
            $info['doctor_name'] = $doctor->first_name . " " . $doctor->last_name;
            $info['doctor_picture'] = $doctor->picture;
            $info['phone'] = $doctor->phone;
            $time = Helper::formatHour($key->created_at);
            $date = Helper::formatDate($key->created_at);
            $info['date'] = $date;
            $info['time'] = $time;
            $info['duration'] = $key->duration;
            $info['fee'] = round(Helper::convertMoney($user->id,$doctor->currency,$fee),2);

            array_push($requestArray, $info);
        }

        $response_array = array(
            'success' => true,
            'data' => $requestArray,
        );

        $response = response()->json($response_array, 200);
        return $response;

    }


    public function reportIssue(Request $request)
    {

        $validator = Validator::make(
            $request->all(),
            array(
                'content' => 'required'
            ));

        if ($validator->fails()) {
            $error_messages = implode(',', $validator->messages()->all());
            $response_array = array('success' => false, 'error' => $error_messages, 'error_code' => 101, 'error_messages' => $error_messages);

        } else {



            $userIssue = new UserIssue;
            $userIssue->user_id = $request->id;
            $userIssue->content = $request->content;
            if ($request->file('picture'))
                $userIssue->picture = Helper::upload_picture($request->file('picture'));
            $userIssue->save();

            $response_array = array(
                'success' => true,
                'message' => 'Your issue has been send successfully',
            );


        }

        $response = response()->json($response_array, 200);
        return $response;

    }


    public function addBookmark(Request $request)
    {

        $validator = Validator::make(
            $request->all(),
            array(
                'article_id' => 'required',
                'is_bookmarked' => 'required'
            ));

        if ($validator->fails()) {
            $error_messages = implode(',', $validator->messages()->all());
            $response_array = array('success' => false, 'error' => $error_messages, 'error_code' => 101, 'error_messages' => $error_messages);

        } else {

            if ($request->is_bookmarked == 1) {

                $articleBookmark = ArticleBookmark::where('user_id', $request->id)
                    ->where('article_id', $request->article_id)
                    ->first();
                if (!$articleBookmark) {
                    $articleBookmark = new ArticleBookmark;
                    $articleBookmark->user_id = $request->id;
                    $articleBookmark->article_id = $request->article_id;
                    $articleBookmark->save();

                    $response_array = array(
                        'success' => true,
                        'message' => 'Article bookmarked successfully',
                    );


                } else {

                    $response_array = array(
                        'success' => true,
                        'message' => 'Article bookmarked successfully',
                    );

                }


            } else {

                $articleBookmark = ArticleBookmark::where('user_id', $request->id)
                    ->where('article_id', $request->article_id)
                    ->delete();
                $response_array = array(
                    'success' => true,
                    'message' => 'Article bookmarked removed',
                );

            }


        }

        $response = response()->json($response_array, 200);
        return $response;

    }


    public function viewBookmark(Request $request)
    {
        $articleBookmarks = ArticleBookmark::where('user_id', $request->id)->get();

        $bookmarkArray = array();

        foreach ($articleBookmarks as $articleBookmark) {
            $article = Article::find($articleBookmark->article_id);
            $doctor = Doctor::find($article->doctor_id);
            $articleCategory = ArticleCategory::find($article->article_category_id);

            $art['article_id'] = $article->id;
            $art['doctor_name'] = $doctor->first_name . " " . $doctor->last_name;
            $art['category'] = $articleCategory->article_category;
            $art['picture'] = $article->picture;
            $art['heading'] = $article->heading;
            $art['is_bookmarked'] = 1;

            array_push($bookmarkArray, $art);

        }

        $response_array = array('success' => true,
            'articles' => $bookmarkArray);
        $response = response()->json($response_array, 200);
        return $response;
    }

    public function generateOtp(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            array(
                'phone' => 'required',
                'country_code' => 'required'
            ));

        if ($validator->fails()) {
            $error_messages = implode(',', $validator->messages()->all());
            $response_array = array('success' => false, 'error_message' => $error_messages, 'error_code' => 101, 'error_messages' => $error_messages);

        } else {
            $c_code = $request->country_code;
            $country_code = str_replace(")", "(", $c_code);

            $country = explode('(', $country_code);

            $accountSid = env('TWILIO_SID');
            $authToken = env('TWILIO_TOKEN');
            $twilioNumber = env('TWILIO_FROM');


            $otp_number = rand(1000, 9999);

            $otp = Otp::where('country_code', $request->country_code)
                ->where('phone', $request->phone)
                ->first();

            if (!$otp) {
                $otp = new Otp;
            }

            $otp->country_code = $request->country_code;
            $otp->phone = $request->phone;
            $otp->otp = $otp_number;
            $otp->save();

            $phone = $country[1] . $request->phone;
            $message = 'Your One Time Code is ' . $otp_number;

            $client = new Services_Twilio($accountSid, $authToken);

            try {
                $m = $client->account->messages->sendMessage(
                    $twilioNumber, // the text will be sent from your Twilio number
                    $phone, // the phone number the text will be sent to
                    $message // the body of the text message
                );


            } catch (Services_Twilio_RestException $e) {
                return $e;

            }

            $response_array = array('success' => true);


        }
        $response = response()->json($response_array, 200);
        return $response;


    }

    public function testTwilio(Request $request)
    {
        //dd(date('Y-m-d h:i:s e'));
        $date = new DateTime(date('Y-m-d h:i:s'));

        $date->setTimezone(new \DateTimeZone(env('TIMEZONE')));

        dd(Helper::time_diff('2016-10-19 07:51:24', $date->format('Y-m-d h:i:s')));

        //dd($date->format('Y-m-d h:i:s'));
        $phone = '+919611568727';
        $message = 'OTP TEST';
        $accountSid = env('TWILIO_SID');
        $authToken = env('TWILIO_TOKEN');
        $twilioNumber = env('TWILIO_FROM');

        $client = new Services_Twilio($accountSid, $authToken);

        try {
            $m = $client->account->messages->sendMessage(
                $twilioNumber, // the text will be sent from your Twilio number
                $phone, // the phone number the text will be sent to
                $message // the body of the text message
            );


        } catch (Services_Twilio_RestException $e) {
            return $e;

        }

        return $m;

    }


    public function cronNotification(Request $request)
    {

        $date = new DateTime(date('Y-m-d H:i:s'));
        $date->setTimezone(new \DateTimeZone(env('TIMEZONE')));


        $requests = Requests::where('request_type', 5)->where('start_time', '>=', $date->format('Y-m-d'))->where('is_booking_cancelled', 0)
                        ->where('is_notified', 0)
                        ->where('request_start_time', '!=', NULL)
                        ->where('request_start_time','>', 0)
                        ->get();

        foreach ($requests as $key) {


            $dateDiff = Helper::time_diff_minutes($key->request_start_time, $date->format('Y-m-d H:i:s'));
            
            
            if ($dateDiff <= 30) {
                

                $key->is_notified = 1;
                $key->save();


                $user = User::find($key->user_id);
                $doctor = Doctor::find($key->confirmed_doctor);
                $doctor_name = $doctor->first_name . " " . $doctor->last_name;

                $deviceToken = $user->device_token;
                $deviceTokenDoctor = $doctor->device_token;

                $reqArray = array();

                $info['request_id'] = $key->id;
                $info['request_type'] = $key->request_type;
                $info['date'] = $date->format('Y-m-d');

                array_push($reqArray, $info);

                $response_array_push = array('success' => true,
                    'data' => [],
                    'invoice' => [],
                    'status' => 98,
                    'title' => 'You have an Appointment with ' . $doctor_name . 'in 30 minutes'
                );

                $response_array_push_doctor = array('success' => true,
                    'data' => $reqArray,
                    'invoice' => [],
                    'title' => 'You have an Appointment in 30 minutes'
                );
                if($user->device_type == 'ios'){
                    $push =\PushNotification::app('appNameIOSDoctor')
                        ->to($deviceToken)
                        ->send('You have a new appointment with '.$doctor_name. 'now');
                    

                }else{
                    \PushNotification::app('appNameAndroid')
                    ->to($deviceToken)
                    ->send($response_array_push);

                }
                if($doctor->device_type == 'ios'){
                    $push =\PushNotification::app('appNameIOSDoctor')
                        ->to($deviceTokenDoctor)
                        ->send('You have a new appointment');

                }else{
                    \PushNotification::app('appNameAndroid')
                    ->to($deviceTokenDoctor)
                    ->send($response_array_push_doctor);

                }

                $response_array = array('success' => true,);


                $response = response()->json($response_array, 200);
                return $response;

            }


        }


    }


    public function cancelRequest(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            array(
                'request_id' => 'required',
            ));

        if ($validator->fails()) {
            $error_messages = implode(',', $validator->messages()->all());
            $response_array = array('success' => false, 'error_message' => $error_messages, 'error_code' => 101, 'error_messages' => $error_messages);

        } else {

            $requests = Requests::find($request->request_id);
            $doctor = Doctor::find($requests->current_doctor);
            $deviceToken = $doctor->device_token;

            if ($requests) {
                RequestsMeta::where('request_id', $request->request_id)->delete();
                $requests->delete();

                $dataArray = array();
                $info['request_type'] = 'C';
                array_push($dataArray, $info);

                $response_array = array('success' => true, 'message' => 'Your request has been cancelled');

                

                $response_array_push = array('success' => true,
                    'data' => $dataArray,
                    'invoice' => [],
                    'title' => 'Request cancelled by User',
                    'request_type' => 'C'
                );

                 if($doctor->device_type == 'ios'){
                    $push =\PushNotification::app('appNameIOSDoctor')
                        ->to($deviceToken)
                        ->send('Your request has been cancelled');

                }else{
                    \PushNotification::app('appNameAndroid')
                    ->to($deviceToken)
                    ->send($response_array_push);

                }


                
            }
        }

        $response = response()->json($response_array, 200);
        return $response;
    }


    public function askQuestion(Request $request)
    {

        $validator = Validator::make(
            $request->all(),
            array(
                'doctor_id' => 'required',
                'subject' => 'required',
                'content' => 'required'
            ));

        if ($validator->fails()) {
            $error_messages = implode(',', $validator->messages()->all());
            $response_array = array('success' => false, 'error_message' => $error_messages, 'error_code' => 101, 'error_messages' => $error_messages);

        } else {

            $question = new Question;
            $question->user_id = $request->id;
            $question->doctor_id = $request->doctor_id;
            $question->subject = $request->subject;
            $question->content = $request->content;
            $question->save();

            $dataArray = array();
            $info['request_type'] = 'A';
            array_push($dataArray, $info);

            $user = User::find($request->id);
            $doctor = Doctor::find($request->doctor_id);

            $deviceToken = $doctor->device_token;

            $response_array = array('success' => true, 'message' => 'Question submitted successfully');

            $response_array_push = array('success' => true,
                'data' => $dataArray,
                'invoice' => [],
                'title' => 'You have a Question from ' . $user->first_name . " " . $user->last_name
            );

            \PushNotification::app('appNameAndroid')
                ->to($deviceToken)
                ->send($response_array_push);

        }
        $response = response()->json($response_array, 200);
        return $response;

    }


    public function getDoctorbySpeciality(Request $request)
    {

        $validator = Validator::make(
            $request->all(),
            array(
                'speciality_id' => 'required',
            ));

        if ($validator->fails()) {
            $error_messages = implode(',', $validator->messages()->all());
            $response_array = array('success' => false, 'error_message' => $error_messages, 'error_code' => 101, 'error_messages' => $error_messages);

        } else {


            $doctor_array = array();

            if (!$request->has('skip'))
                $skip = 0;
            else
                $skip = $request->skip;

            if (!$request->has('take'))
                $take = 50;
            else
                $take = $request->take;


            $doctors = Doctor::where('speciality_id', $request->speciality_id)
                ->skip($skip)
                ->take($take)
                ->get();

            $count = Doctor::where('speciality_id', $request->speciality_id)->count();


            if ($doctors) {

                foreach ($doctors as $doctor) {
                    $doc['id'] = $doctor->id;
                    $doc['d_name'] = $doctor->first_name . " " . $doctor->last_name;
                    $doc['d_photo'] = $doctor->picture;
                    $doc['experience'] = $doctor->experience;
                    $doc['helped'] = DB::table('user_ratings')->where('doctor_id', $doctor->id)->count();
                    $rating = DB::table('user_ratings')->where('doctor_id', $doctor->id)->avg('rating') ?: 0;
                    $doc['rating'] = round($rating, 2);
                    $doc['d_latitude'] = $doctor->latitude;
                    $doc['d_longitude'] = $doctor->longitude;
                    $doc['c_name'] = $doctor->c_name;
                    $doc['c_street'] = $doctor->c_street;
                    $doc['c_city'] = $doctor->c_city;
                    $doc['c_state'] = $doctor->c_state;
                    $doc['c_postal'] = $doctor->c_postal;
                    $doc['c_country'] = $doctor->c_country;
                    $doc['c_pic1'] = $doctor->c_pic1;
                    $doc['c_pic2'] = $doctor->c_pic2;
                    $doc['c_pic3'] = $doctor->c_pic3;
                    $doc['is_online'] = $doctor->is_booking_online;
                    $doc['appointment_fee'] = $doctor->booking_fee;
                    $doc['nationality'] = $doctor->nationality;
                    $doc['degree1'] = $doctor->degree1;
                    $doc['degree2'] = $doctor->degree2;
                    $doc['univ1'] = $doctor->univ1;
                    $doc['univ2'] = $doctor->univ2;

                    array_push($doctor_array, $doc);

                }

                $response_array = array('success' => true, 'count' => $count, 'doctor_list' => $doctor_array);
                $response_array = Helper::null_safe($response_array);
            } else {
                $response_array = array('success' => false, 'error_message' => 'No doctor available');

            }


        }
        $response = response()->json($response_array, 200);
        return $response;

    }


    public function viewAnswers(Request $request)
    {

        $questions = Question::where('user_id', $request->id)->where('is_answered', 1)->get();

        $questionArray = array();

        foreach ($questions as $question) {
            $doctor = Doctor::find($question->doctor_id);

            $date = new DateTime($question->created_at->format('Y-m-d H:i'));
            $date->setTimezone(new \DateTimeZone(env('TIMEZONE')));

            $info['id'] = $question->id;
            $info['subject'] = $question->subject;
            $info['content'] = $question->content;
            $info['answer'] = $question->answer;
            $info['time'] = $date->format('h:ia');
            $info['date'] = $date->format('Y-m-d');
            $info['doctor_name'] = $doctor->first_name . " " . $doctor->last_name;
            $info['doctor_picture'] = $doctor->picture;

            array_push($questionArray, $info);
        }

        $response_array = array('success' => true, 'questionArray' => $questionArray);
        $response = response()->json($response_array, 200);
        return $response;


    }

    public function generateOpentok(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            array(
                'request_id' => 'required',
            ));

        if ($validator->fails()) {
            $error_messages = implode(',', $validator->messages()->all());
            $response_array = array('success' => false, 'error_message' => $error_messages, 'error_code' => 101, 'error_messages' => $error_messages);

        } else {

            $requests = Requests::find($request->request_id);

            if (!$requests->session && !$requests->token) {

                // new session
                $session = OpentokApi::createSession();
                $sessionId = $session->getSessionId();

                // check if it's been created or not (could have failed)
                if (empty($sessionId)) {
                    throw new \Exception("An open tok session could not be created");
                }

                // then create a token (session created in previous step)
                try {
                    // note we're create a publisher token here, for subscriber tokens we would specify.. yep 'subscriber' instead
                    $token = OpentokApi::generateToken($sessionId);
                } catch (OpenTokException $e) {
                    // do something here for failure
                }

                $requests->session = $sessionId;
                $requests->token = $token;

                $requests->save();


            } else {

                $sessionId = $requests->session;
                $token = $requests->token;

            }


            $response_array = array('success' => true, 'session_id' => $sessionId, 'token' => $token);
        }


        $response = response()->json($response_array, 200);
        return $response;


    }

    public function checkToken(Request $request)
    {
        $user = User::find($request->id);
        $response_array = array('success' => true, 'message' => 'Token Valid','email_verification' => $user->email_verification);
        $response = response()->json($response_array, 200);
        return $response;
    }

    public function testEmail(Request $request)
    {
        $data = array();
        $data[0] = 'data';
        $data[1] = 'data2';
        $result = Mail::send('emails.test', $data, function ($message) {
            $message->from('nithin@provenlogic.net');
            $message->to('amalpaul@provenlogic.net');
        });

        dd($result);

        $response_array = array('success' => true, 'message' => 'Mail Sent');
        $response = response()->json($response_array, 200);
        return $response;
    }


    public function changePassword(Request $request)
    {

        $validator = Validator::make(
            $request->all(),
            array(
                'old_password' => 'required',
                'new_password' => 'required'
            ));

        if ($validator->fails()) {
            $error_messages = implode(',', $validator->messages()->all());
            $response_array = array('success' => false, 'error_message' => $error_messages, 'error_code' => 101, 'error_messages' => $error_messages);

        } else {

            $user = User::find($request->id);

            if (Hash::check($request->old_password, $user->password)) {

                $user->password = Hash::make($request->new_password);

                $user->save();

                $headers ="Authorization :y2h2eb8pdetk0boozc8zolxr7aitcjsh";

//                $r = Curl::to('http://52.220.193.142:3000/api/v1/users/account/update')
//                    ->withHeader($headers)
//                    ->withData(array(
//                        "password" =>$request->new_password))
//                    ->post();
//                dd($r);

                //NOrmal Curl

                $post = array("_id"=>$user->ecom_id,"old_password"=>$request->old_password,"password"=> $request->new_password,"confirm_password"=>$request->new_password);

                $headers = array('Authorization:'.$user->ecom_token);

                $ch = curl_init('http://52.220.193.142:3000/api/v1/users/account/change-password');
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));

                // execute!
                $response = curl_exec($ch);

                // close the connection, release resources used
                curl_close($ch);

                // do anything you want with your response
                //dd($response);


                //End


                $response_array = array('success' => true, 'message' => 'Password changed successfully');


            } else {

                $response_array = array('success' => false, 'error_message' => 'Current password is wrong');

            }

        }

        $response = response()->json($response_array, 200);
        return $response;

    }

    public function getChatHistory(Request $request)
    {
        $completedRequests = Requests::where('status',5)->where('request_type',1)->get();

        $requestArray = array();

        foreach ($completedRequests as $completedRequest)
        {
            $doctor = Doctor::find($completedRequest->confirmed_doctor);

            $info['request_id'] = $completedRequest->id;
            $info['doctor_id'] = $completedRequest->confirmed_doctor;
            $info['doctor_name'] = $doctor->first_name." ".$doctor->last_name;
            $info['doctor_image'] = $doctor->picture;


            array_push($requestArray,$info);
        }

        $response_array = array('success' => true, 'requestArray' => $requestArray);
        $response = response()->json($response_array, 200);
        return $response;

    }

    public function uploadAttachment(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            array(
                'request_id' => 'required',
                'image' => 'required'
            ));

        if ($validator->fails()) {
            $error_messages = implode(',', $validator->messages()->all());
            $response_array = array('success' => false, 'error_message' => $error_messages, 'error_code' => 101, 'error_messages' => $error_messages);

        } else {

            $image = Helper::upload_picture($request->file('image'));
            $attachment = new Attachment;
            $attachment->request_id = $request->request_id;
            $attachment->image = $image;

            $response_array = array('success' => true, 'request_id' => $attachment->request_id,'image'=>$attachment->image);

        }
        $response = response()->json($response_array, 200);
        return $response;

    }

    public function clearDebt(Request $request)
    {
        $debt = UserDebt::where('user_id', $request->id)->first();

        $payment = Payment::where('user_id', $request->id)
            ->where('is_default', 1)
            ->first();

        $transaction = Helper::createTransaction($payment->customer_id, $debt->request_id, $debt->amount);
        if ($transaction != 0) {
            UserDebt::where('user_id', $request->id)->delete();
            $response_array = array(
                'success' => true,
                'message' => 'Previous debt cleared'
            );

        }else{
            $response_array = array(
                'success' => false,
                'error_message' => 'Transaction failed, Please try again'
            );

        }
        $response = response()->json($response_array, 200);
        return $response;

    }


    public function getDoctorMessages(Request $request)
    {
        $doctorMessages = DoctorMessage::where('user_id',$request->id)->get();

        $messageArray = array();

        foreach ($doctorMessages as $doctorMessage) {
            $doctor = Doctor::find($doctorMessage->doctor_id);
            $info['id'] = $doctorMessage->id;
            $info['doctor_id'] = $doctorMessage->doctor_id;
            $info['doctor_name'] = $doctor->first_name.' '.$doctor->last_name;
            $info['doctor_picture'] = $doctor->picture;

            $info['sent'] = $doctorMessage->sent;
            $info['reply'] = $doctorMessage->reply;

            array_push($messageArray,$info);
        }

        $response_array = array(
            'success' => true,
            'messageArray' => $messageArray
        );

        $response = response()->json($response_array, 200);
        return $response;

    }

    public function replyDoctorMessage(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            array(
                'message_id' => 'required',
                'reply' => 'required'
            ));

        if ($validator->fails()) {
            $error_messages = implode(',', $validator->messages()->all());
            $response_array = array('success' => false, 'error_message' => $error_messages, 'error_code' => 101, 'error_messages' => $error_messages);

        } else {
            $doctorMessage = DoctorMessage::find($request->message_id);
            $user = User::find($request->id);
            if($doctorMessage){
                $doctorMessage->reply = $request->reply;
                $doctorMessage->save();

                $dataArray = array();
                $info['request_type'] = 166;
                array_push($dataArray, $info);

                $doctorMessages = DoctorMessage::where('user_id',$request->id)->get();

                $messageArray = array();

                foreach ($doctorMessages as $doctorMessage) {
                    $doctor = Doctor::find($doctorMessage->doctor_id);
                    $info['id'] = $doctorMessage->id;
                    $info['doctor_id'] = $doctorMessage->doctor_id;
                    $info['doctor_name'] = $doctor->first_name.' '.$doctor->last_name;
                    $info['doctor_picture'] = $doctor->picture;

                    $info['sent'] = $doctorMessage->sent;
                    $info['reply'] = $doctorMessage->reply;

                    array_push($messageArray,$info);
                }

                $response_array = array(
                    'success' => true,
                    'messageArray' => $messageArray
                );

                $response_array_push = array(
                    'success' => true,
                    'data'=>$dataArray,
                    'title' =>$user->first_name.' replied to your message',
                    'user_id' => $doctorMessage->user_id
                );


                $doctor = Doctor::find($doctorMessage->doctor_id);
                $deviceToken = $doctor->device_token;

                \PushNotification::app('appNameAndroid')
                    ->to($deviceToken)
                    ->send($response_array_push);

            }



        }

        $response = response()->json($response_array, 200);
        return $response;
    }


    public function verifyEmail(Request $request)
    {

        $hashid=$request->segment(2);
        $user = User::where('email_md5',$hashid)->first();
        if($user){
            $user->email_verification = true;
            $user->save();
            return view('emails.verified')->with('user',$user);
        }

    }

    public function getNotified(Request $request)
    {
        // $check_status = array(REQUEST_CANCELLED,REQUEST_NO_DOCTOR_AVAILABLE,);

        $requests = Requests::where('user_id',$request->id)
                            ->where('status',REQUEST_INPROGRESS)
                            ->where('doctor_status',DOCTOR_ACCEPTED)
                            ->where('confirmed_doctor','!=',0)
                            ->first();


        $info_data = array();
        $status = 0;

        if($requests)
        {

            $doctor = Doctor::find($requests->confirmed_doctor);

            $clinic = Clinic::find($doctor->clinic_id);

            $clinic_lat = $clinic->c_latitude;
            $clinic_lng = $clinic->c_longitude;
            $clinic_address = $clinic->c_address;

            //Get Distance time
                    if($requests->s_latitude!=0 && $requests->s_longitude!=0){
                        $distance =Helper::getDistanceTime($doctor->latitude,$doctor->longitude,$requests->s_latitude,$requests->s_longitude);
                        $distArray = explode("$$",$distance);
                        $eta = $distArray[0].' away';
                        $times = $distArray[1];
                    }else{
                        $eta =0;
                        $times =0;
                    }




            $data['request_id'] = $requests->id;
            $data['user_id'] = $requests->user_id;
            $data['doctor_id'] = $request->id;
            $data['status'] = $requests->status;
            $data['doctor_status'] = $requests->doctor_status;
            $data['s_longitude'] = $requests->s_longitude;
            $data['s_latitude'] = $requests->s_latitude;
            $data['doctor_name'] = $doctor->first_name." ".$doctor->last_name;
            $data['doctor_picture'] = $doctor->picture;
            $data['doctor_mobile'] = $doctor->phone;
            $data['d_latitude'] = $doctor->latitude;
            $data['d_longitude'] = $doctor->longitude;
            $data['c_address'] =$clinic_address;
            $data['source_address'] = $requests->s_address;
            $data['eta'] = $eta;
            $data['time'] = $times;
            $data['doctor_rating'] = round(DoctorRating::where('doctor_id', $request->id)->avg('rating'),1) ?: 0;

            $info_data [] = $data;

            if($requests->request_type == 1){
                $status = DOCTOR_ACCEPTED_CHAT;
            }elseif ($requests->request_type == 2) {
                $status = DOCTOR_ACCEPTED_PHONE;
            }elseif ($requests->request_type == 3) {
                $status = DOCTOR_ACCEPTED_VIDEO;
            }else{
                $status = DOCTOR_ACCEPTED;
            }


        }



        $response_array = array('success' => true,
                                'data' => $info_data,
                                'status' => $status,

                            );

        $response = response()->json($response_array, 200);
        return $response;
    }

    public function forgotPassword(Request $request)
    {

        $length = rand(8,8);
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';

        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }

        $email = $request->email;
        $newPassword = $randomString;
        
        if(User::where('email',$email)->first() || Doctor::where('email',$email)->first())
        {
            $this->dispatch(new SendForgotPassword($email,$newPassword));

            if($user = User::where('email',$email)->first())
            {
                $user->password = Hash::make($randomString);
                $user->save();

            }elseif($doctor = Doctor::where('email',$email)->first()){
                $doctor->password = Hash::make($randomString);
                $user->save();
            }

            return 'ok';
        }



    }

    public function forgetPassword(Request $request)
    {
        $length = rand(8,8);
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';

        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }

        $email = $request->email;
        $newPassword = $randomString;
        
        if(User::where('email',$email)->first() || Doctor::where('email',$email)->first())
        {
            $this->dispatch(new SendForgotPassword($email,$newPassword));

            if($user = User::where('email',$email)->first())
            {
                $post = array("email"=>$request->email, "password"=>$newPassword);

                $headers = array('Authorization:'.$user->ecom_token);

                $ch = curl_init('http://52.220.193.142:3000/api/v1/users/forgot');
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));
                
                // execute!
                $response = curl_exec($ch);

                // close the connection, release resources used
                curl_close($ch);

                

                $user->password = Hash::make($randomString);
                $user->save();

            }elseif($doctor = Doctor::where('email',$email)->first()){
                $doctor->password = Hash::make($randomString);
                $doctor->save();
            }

             $response_array = array(
                    'success' => true,
                    'message' => "Please check your inbox"
                );
        }else{

            $response_array = array(
                    'success' => false,
                    'error_message' => "Email Id not found"
                );

        }

        $response = response()->json($response_array, 200);
        return $response;

    }

    



    public function getRequest(Request $request)
    {

        $validator = Validator::make(
            $request->all(),
            array(
                'request_id' => 'required|numeric|exists:requests,id',
            ));

        if ($validator->fails()) {
            $error_messages = implode(',', $validator->messages()->all());
            $response_array = array('success' => false, 'error' => $error_messages, 'error_messages' => Helper::get_error_message(101), 'error_code' => 101);
        } else {
            $requests = Requests::find($request->request_id);
            $doctor = Doctor::find($requests->current_doctor);
            $user = User::find($request->id);

            //$clinic = Clinic::find($doctor->clinic_id);

            // $clinic_lat = $clinic->c_latitude;
            // $clinic_lng = $clinic->c_longitude;
            // $clinic_address = $clinic->c_address;

            // $json1 = json_decode(file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?latlng=' . $requests->s_latitude . ',' . $requests->s_longitude . '&key=AIzaSyC4AEUI8uxPs42kVRzSUb4B718ZK0hKxew'), true);
            // if (!$json1['results']) {
            //     $source_address = '';
            // } else {
            //     $source_address = $json1['results'][0]['formatted_address'];
            // }

            // $json2 = json_decode(file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?latlng=' . $clinic_lat . ',' . $clinic_lng . '&key=AIzaSyC4AEUI8uxPs42kVRzSUb4B718ZK0hKxew'), true);
            // if (!$json2['results']) {
            //     $c_address = '';
            // } else {
            //     $c_address = $json2['results'][0]['formatted_address'];
            // }

            // if ($requests->s_latitude != 0 && $requests->s_longitude != 0) {
            //     $distance = Helper::getDistanceTime($doctor->latitude, $doctor->longitude, $requests->s_latitude, $requests->s_longitude);
            //     $distArray = explode("$$", $distance);
            //     $eta = $distArray[0] . ' away';
            //     $times = $distArray[1];
            // } else {
            //     $eta = 0;
            //     $times = 0;
            // }

            //Get Distance time
//            $distance =Helper::getDistanceTime($doctor->latitude,$doctor->longitude,$requests->s_latitude,$requests->s_longitude);
//            $distArray = explode("$$",$distance);


            $doctorData = array(
                'request_id' => $requests->id,
                'user_id' => $requests->user_id,
                'doctor_id' => $request->id,
                'status' => $requests->status,
                'doctor_status' => $requests->doctor_status,
                's_longitude' => $requests->s_longitude,
                's_latitude' => $requests->s_latitude,
                'doctor_name' => $doctor->first_name . " " . $doctor->last_name,
                'doctor_picture' => $doctor->picture,
                'doctor_mobile' => $doctor->phone,
                'd_latitude' => $doctor->latitude,
                'd_longitude' => $doctor->longitude,
                // 'c_address' => $clinic_address,
                'source_address' => "",
                'eta' => "",
                'time' => "",
                'doctor_rating' => round(DoctorRating::where('doctor_id', $doctor->id)->avg('rating'), 2) ?: 0,
            );

            $invoice_data = array();

            $doctorFee = DoctorFee::where('doctor_id', $doctor->id)->first();
            if ($doctorFee) {
                $ondemand_fee = $doctorFee->ondemand_fee;
            } else {
                $ondemand_fee = 0;
            }

            $setting = Setting::find(1);
            if ($setting) {
                $fee_km = $setting->fee_km;
            } else {
                $fee_km = 0;
            }

            $distance_price = $fee_km * $requests->distance;
            $total = $requests->amount;
            $distance = $requests->real_distance;

            $inv['doctor_id'] = $request->id;
            $inv['doctor_name'] = $doctor->first_name . " " . $doctor->last_name;
            $inv['doctor_picture'] = $doctor->picture;
            $inv['total_time'] = $requests->duration;
            $inv['payment_mode'] = $requests->payment_mode;
            $inv['base_price'] = round(Helper::convertMoney($user->id,$doctor->currency,$ondemand_fee),2);
            $inv['per_km'] = $fee_km;
            $inv['distance_cost'] = $distance_price;
            $inv['time_price'] = 0;
            $inv['tax_price'] = 0;
            $inv['total'] = round(Helper::convertMoney($user->id,$doctor->currency,$total),2);
            $inv['distance'] = $distance;
            $inv['medicine_fee'] = 0;
            $inv['referral_bonus'] = 0;
            $inv['promo_bonus'] = 0;

            $invoice_data[] = $inv;

            //video call ended
            if (($requests->request_type == 3 && $requests->doctor_status == 5 && $requests->is_doctor_rated == 0) || ($requests->request_type == 3 && $requests->doctor_status == 6 && $requests->is_doctor_rated == 0)) {

                $doctorFee = DoctorFee::where('doctor_id', $doctor->id)->first();
                if ($doctorFee) {
                    $base_fee = $doctorFee->video_fee;
                } else {
                    $base_fee = 0;
                }

                $invoice_data = array();

                $inv['doctor_id'] = $request->id;
                $inv['doctor_name'] = $doctor->first_name . " " . $doctor->last_name;
                $inv['doctor_picture'] = $doctor->picture;
                $inv['total_time'] = $requests->duration;
                $inv['payment_mode'] = $requests->payment_mode;
                $inv['base_price'] = round(Helper::convertMoney($user->id,$doctor->currency,$base_fee),2);
                $inv['time_price'] = 0;
                $inv['tax_price'] = 0;
                $inv['total'] = round(Helper::convertMoney($user->id,$doctor->currency,$requests->amount),2);
                $inv['distance'] = 0;
                $inv['medicine_fee'] = 0;
                $inv['referral_bonus'] = 0;
                $inv['promo_bonus'] = 0;

                $invoice_data[] = $inv;


                $response_array = Helper::null_safe(array(
                    'success' => true,
                    'request_id' => $requests->id,
                    'title' => 'check status',
                    'data' => $doctorData,
                    'status' => DOCTOR_ENDED_VIDEO,
                    'invoice' => $invoice_data
                ));

            } elseif (($requests->request_type == 2 && $requests->doctor_status == 5 && $requests->is_doctor_rated == 0) || ($requests->request_type == 2 && $requests->doctor_status == 6 && $requests->is_doctor_rated == 0)) {
                # Audio call ended...

                $doctorFee = DoctorFee::where('doctor_id', $doctor->id)->first();
                if ($doctorFee) {
                    $base_fee = $doctorFee->phone_fee;
                } else {
                    $base_fee = 0;
                }

                $invoice_data = array();

                $inv['doctor_id'] = $request->id;
                $inv['doctor_name'] = $doctor->first_name . " " . $doctor->last_name;
                $inv['doctor_picture'] = $doctor->picture;
                $inv['total_time'] = $requests->duration;
                $inv['payment_mode'] = $requests->payment_mode;
                $inv['base_price'] = round(Helper::convertMoney($user->id,$doctor->currency,$base_fee),2);
                $inv['time_price'] = 0;
                $inv['tax_price'] = 0;
                $inv['total'] = round(Helper::convertMoney($user->id,$doctor->currency,$requests->amount),2);
                $inv['distance'] = 0;
                $inv['medicine_fee'] = 0;
                $inv['referral_bonus'] = 0;
                $inv['promo_bonus'] = 0;

                $invoice_data[] = $inv;

                $response_array = Helper::null_safe(array(
                    'success' => true,
                    'request_id' => $requests->id,
                    'title' => 'check status',
                    'data' => $doctorData,
                    'status' => DOCTOR_ENDED_PHONE,
                    'invoice' => $invoice_data
                ));
            } elseif (($requests->request_type == 1 && $requests->doctor_status == 5 && $requests->is_doctor_rated == 0) || ($requests->request_type == 1 && $requests->doctor_status == 6 && $requests->is_doctor_rated == 0)) {
                # chat ended

                $doctorFee = DoctorFee::where('doctor_id', $doctor->id)->first();
                if ($doctorFee) {
                    $base_fee = $doctorFee->chat_fee;
                } else {
                    $base_fee = 0;
                }

                $invoice_data = array();

                $inv['doctor_id'] = $request->id;
                $inv['doctor_name'] = $doctor->first_name . " " . $doctor->last_name;
                $inv['doctor_picture'] = $doctor->picture;
                $inv['total_time'] = $requests->duration;
                $inv['payment_mode'] = $requests->payment_mode;
                $inv['base_price'] = round(Helper::convertMoney($user->id,$doctor->currency,$base_fee),2);
                $inv['time_price'] = 0;
                $inv['tax_price'] = 0;
                $inv['total'] = round(Helper::convertMoney($user->id,$doctor->currency,$requests->amount),2);
                $inv['distance'] = 0;
                $inv['medicine_fee'] = 0;
                $inv['referral_bonus'] = 0;
                $inv['promo_bonus'] = 0;

                $invoice_data[] = $inv;

                $response_array = Helper::null_safe(array(
                    'success' => true,
                    'request_id' => $requests->id,
                    'title' => 'check status',
                    'data' => $doctorData,
                    'status' => DOCTOR_ENDED_CHAT,
                    'invoice' => $invoice_data
                ));

            } elseif ($requests->request_type == 1 && $requests->doctor_status == 1 && $requests->is_doctor_rated == 0) {
                # chat running

                $response_array = Helper::null_safe(array(
                    'success' => true,
                    'request_id' => $requests->id,
                    'title' => 'check status',
                    'data' => $doctorData,
                    'status' => DOCTOR_ACCEPTED_CHAT,
                    'invoice' => $invoice_data
                ));

            } elseif ($requests->request_type == 2 && $requests->doctor_status == 1 && $requests->is_doctor_rated == 0) {
                # audio running

                $response_array = Helper::null_safe(array(
                    'success' => true,
                    'request_id' => $requests->id,
                    'title' => 'check status',
                    'data' => $doctorData,
                    'status' => DOCTOR_ACCEPTED_PHONE,
                    'invoice' => $invoice_data
                ));

            } elseif ($requests->request_type == 3 && $requests->doctor_status == 1 && $requests->is_doctor_rated == 0) {
                # video running

                $response_array = Helper::null_safe(array(
                    'success' => true,
                    'request_id' => $requests->id,
                    'title' => 'check status',
                    'data' => $doctorData,
                    'status' => DOCTOR_ACCEPTED_VIDEO,
                    'invoice' => $invoice_data
                ));

            } elseif ($requests->request_type == 5 && $requests->is_doctor_rated == 0) {
                # booking ended
                $response_array = Helper::null_safe(array(
                    'success' => true,
                    'request_id' => $requests->id,
                    'title' => 'check status',
                    'data' => $doctorData,
                    'status' => 30,
                    'invoice' => $invoice_data
                ));

            } elseif($requests->status == REQUEST_REJECTED_BY_DOCTOR){

                $response_array = Helper::null_safe(array(
                    'success' => true,
                    'request_id' => $requests->id,
                    'title' => 'check status',
                    'data' => $doctorData,
                    'status' => REQUEST_REJECTED_BY_DOCTOR,
                    'invoice' => $invoice_data
                ));

            } elseif ($requests->request_type == 4 && $requests->doctor_status == 1) {
                $response_array = Helper::null_safe(array(
                    'success' => true,
                    'request_id' => $requests->id,
                    'title' => 'check status',
                    'data' => $doctorData,
                    'status' => 1,
                    'invoice' => $invoice_data
                ));
            } elseif(($requests->request_type == 4 && $requests->doctor_status != 1)&&($requests->request_type == 4 && $requests->doctor_status != 0)){
                $response_array = Helper::null_safe(array(
                    'success' => true,
                    'request_id' => $requests->id,
                    'title' => 'check status',
                    'data' => $doctorData,
                    'status' =>$requests->doctor_status ,
                    'invoice' => $invoice_data
                ));
            }
            else {

                $response_array = Helper::null_safe(array(
                    'success' => true,
                    'request_id' => $requests->id,
                    'title' => 'check status',
                    'data' => $doctorData,
                    'status' => "",
                    'invoice' => $invoice_data
                ));

            }


        }

        $response = response()->json($response_array, 200);
        return $response;

    }

    public static function currencyMultiplierCron()
    {
            Log::info('cron2');
            $users = User::all();

            foreach($users as $user)
            {
                $userCurrency = $user->currency;

                if(!$userCurrency)
                    $userCurrency = 'USD';



                $multiplierUSD = Swap::latest('USD/'.$userCurrency)->getValue();
                $multiplierMYR = Swap::latest('MYR/'.$userCurrency)->getValue();
                $multiplierCNY = Swap::latest('CNY/'.$userCurrency)->getValue();
                $multiplierSGD = Swap::latest('SGD/'.$userCurrency)->getValue();
                $multiplierIDR = Swap::latest('IDR/'.$userCurrency)->getValue();
                $multiplierVND = Swap::latest('VND/'.$userCurrency)->getValue();
                $multiplierMMK = Swap::latest('MMK/'.$userCurrency)->getValue();
                $multiplierIRR = Swap::latest('IRR/'.$userCurrency)->getValue();
                $multiplierJPY = Swap::latest('JPY/'.$userCurrency)->getValue();
                $multiplierTHB = Swap::latest('THB/'.$userCurrency)->getValue();


                $currencyMultiplier = CurrencyMultiplier::where('user_id',$user->id)->first();

                if(!$currencyMultiplier)
                    $currencyMultiplier = new CurrencyMultiplier;


                $currencyMultiplier->user_id = $user->id;
                $currencyMultiplier->USD = $multiplierUSD;
                $currencyMultiplier->MYR = $multiplierMYR;
                $currencyMultiplier->CNY = $multiplierCNY;
                $currencyMultiplier->SGD = $multiplierSGD;
                $currencyMultiplier->IDR = $multiplierIDR;
                $currencyMultiplier->VND = $multiplierVND;
                $currencyMultiplier->MMK = $multiplierMMK;
                $currencyMultiplier->IRR = $multiplierIRR;
                $currencyMultiplier->JPY = $multiplierJPY;
                $currencyMultiplier->THB = $multiplierTHB;

                $currencyMultiplier->save();

            }






        }

        public static function iosPushTest(Request $request)
        {

            $deviceToken = '057219fab31423065415c2e7e831cb549e051bf72272f4fc4267e390dc810d77';
            $msg = 'hi';

            $push =\PushNotification::app('appNameIOSDoctor')
                ->to($deviceToken)
                ->send('Hello World, i`m a push message');

                dd($push);

        }







}
