<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Repositories\SocialLoginRepository;
use Illuminate\Http\Request;
use Socialite;
use Auth;

class GoogleController extends Controller
{

	public function __construct(SocialLoginRepository $socialRepo)
	{
		$this->socialRepo = $socialRepo;
	}


	public function providerRedirect(Request $request)
	{
		session(['social_login_user_type' => $request->user_type]);
		return Socialite::driver('google')->scopes($this->scopes())->redirect();
	}


	protected function scopes()
	{ 
		return [
			'https://www.googleapis.com/auth/plus.login', 
			'https://www.google.com/m8/feeds', 
			'email', 
			'https://www.googleapis.com/auth/plus.me'
		];
	}


	public function providerCallback(Request $request)
	{ 
		try {

			$user = Socialite::driver('google')->user();			

			$this->socialRepo->setSocialID($user->getId())->setUserEmail($user->getEmail());
			
			$user_type = session()->get('social_login_user_type');
			$storedUser = $this->socialRepo->getStoredUser($user_type);

			if($storedUser) {

				if($user_type == 'user')
					Auth::login($storedUser);
				else if($user_type == 'doctor')
					Auth::guard('doctor-web')->login($storedUser);

				session()->forget('social_login_user_type');
				return $this->socialRepo->successResponse(['login_status' => true], true);
			}



			$this->socialRepo->setUsername($user->getName())
							->setUserAvatarURL($user->getAvatar());



			if(isset($user->user['gender'])) {
				$this->socialRepo->setGender($user->user['gender']);
			}

			session()->forget('social_login_user_type');
			\Log::info($this->socialRepo->successResponse(['social_provider' => 'google']));
			return $this->socialRepo->successResponse(['social_provider' => 'google']);

		} catch(\Exception $e){
			session()->forget('social_login_user_type');
			\Log::info($this->socialRepo->errorResponse($e->getMessage()));
			return $this->socialRepo->errorResponse($e->getMessage());
		}

	}

}