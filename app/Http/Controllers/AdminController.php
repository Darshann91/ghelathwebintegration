<?php

namespace App\Http\Controllers;

use App\Clinic;
use App\Document;
use App\ProductsCategory;
use App\Setting;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Admin;
use App\User;
use App\Doctor;
use App\Speciality;
use App\ArticleCategory;
use App\DoctorFee;
use Hash;
use App\Helpers\Helper;
use App\Requests;
use DB;
use App\DoctorSlot;

use Illuminate\Http\Request;

class AdminController extends Controller
{

     public function __construct()
    {
        $this->middleware('admin');
    }

    // public function verify()
    // {

    //  $username = Input::get('username');
    //  $password = bcrypt(Input::get('password'));
        
    //  if(!Admin::count()){
    //      $admin = new Admin;
    //      $admin->username = $username;
    //      $admin->password = $password;
    //      $admin->save();
    //      return redirect('admin/login');
    //  }else{
    //      if(Auth::attempt(['username'=>$username, 'password'=>Input::get('password')])){
    //          return redirect()->route('dashboard');
    //      }else{
    //          return redirect()->route('login');
    //      }
    //  }
    // }


    // public function login()
    // {
    //  if(Admin::count())
    //  {
    //      return view('admin.login')->with('title','Login')->with('button','Login');
    //  }else{
    //      return view('admin.login')->with('title','Create Admin')->with('button','Create');
    //  }
        
    // }

    public function index()
    {
     return Redirect::to('/admin/login');
    }

    public function dashboard()
    {
        //$consults = Requests::where('status',5);
        $chat= Requests::where('status',5)->where('request_type',1)->count();
        $phone= Requests::where('status',5)->where('request_type',2)->count();
        $video= Requests::where('status',5)->where('request_type',3)->count();
        $ondemand= Requests::where('status',5)->where('request_type',4)->count();
        $booking= Requests::where('status',5)->where('request_type',5)->count();

        //dd($ondemand);
        return view('admin.dashboard')->with('chat',$chat)->with('phone',$phone)->with('video',$video)->with('ondemand',$ondemand)->with('booking',$booking);
    }

    public function listCustomers()
    {
        $users = User::paginate(50);
        return view('admin.list_customers')->with('users',$users);
    }

    public function addCustomers()
    {
        return view('admin.add_new_customers');
    }

    public function listSpecialities()
    {
        $specialities = Speciality::all();
        return view('admin.list_specialities')->with('specialities',$specialities);
    }

    public function listDoctors()
    { 
        $doctors = Doctor::simplePaginate(10);
        $specialities = Speciality::all();
        return view ('admin.list_doctors')->with('doctors',$doctors)
                                         ->with('specialities',$specialities);
    }

    public function listConsults()
    {
        $consults = DB::table('requests')
                    ->leftJoin('doctors','requests.confirmed_doctor','=','doctors.id')
                    ->leftJoin('users','requests.user_id','=','users.id')
                    ->select('users.first_name as user_first_name','users.last_name as user_last_name','doctors.first_name as doctor_first_name','doctors.last_name as doctor_last_name','requests.id as request_id','requests.start_time as start_time','requests.end_time as end_time','requests.payment_mode','requests.status','requests.request_type','requests.amount')
                    ->simplePaginate(20);

        return view ('admin.list_consults')->with('consults',$consults);
    }

    public function addSpeciality()
    {
        return view('admin.add_new_speciality');
    }

    public function doctorFee(Request $request)
    {
        $doctor_id = $request->id;
        $doctor = Doctor::find($doctor_id);
        $doctor_fee = DoctorFee::where('doctor_id',$doctor_id)->first();
        return view('admin.doctor_fees')->with('doctor',$doctor)->with('doctor_fee',$doctor_fee);
    }

    public function specialitySubmit(Request $request)
    {   
                        
        $speciality=new Speciality;
        $speciality->speciality=$request->s_name;

        $speciality->picture = Helper::upload_picture($request->file('s_picture'));
        $speciality->save();

        return redirect('/admin/listspecialities');

    }

    public function listArticleCategories()
    {
        $articleCategories = ArticleCategory::all();
        return view('admin.list_article_categories')
                ->with('articleCategories',$articleCategories);
    }

    public function addArticleCategory()
    {
        return view('admin.add_article_category');
    }

    public function articleSubmit(Request $request)
    {   
        $category_name = Input::get('category_name');
        $articleCategory = new ArticleCategory;
        $articleCategory->article_category=$category_name;
        $articleCategory->picture = Helper::upload_picture($request->file('c_picture'));
        $articleCategory->save();

        return redirect('/admin/listarticlecategories');
    }

    public function getSettings()
    {
        $setting = Setting::find(1);
        if($setting){
            $percentage = $setting->percentage;
        }else{
            $percentage = 0;
        }
        return view('admin.settings')->with('percentage',$percentage);
    }

    public function approveDoctor(Request $request)
    {
        $doctor = Doctor::find($request->segment(3));

        if($doctor)
        {
            if($doctor->is_approved == 0)
                $doctor->is_approved = 1;
            else
                $doctor->is_approved = 0;
            
            $doctor->save();
            return redirect('/admin/listdoctors');
        }
    }

    public function listProductCategory(Request $request)
    {
        $prodCategories = ProductsCategory::all();

        return view('admin.list_product_category')->with('prodCategories',$prodCategories);
    }

    public function addProductCategory(Request $request)
    {
        return view('admin.add_product_category');
    }


    public function productCategorySubmit(Request $request)
    {
        $prodCategory = new ProductsCategory;
        $prodCategory->category_name = $request->s_name;
        $prodCategory->picture = Helper::upload_picture($request->file('s_picture'));
        $prodCategory->save();

        return redirect('/admin/listproductcategories');
    }

    public function saveGeneral(Request $request)
    {
        $setting = Setting::find(1);
        if(!$setting){
            $setting = new Setting;
        }

        $setting->percentage = $request->percentage;
        $setting->save();

        return redirect('/admin/settings');
    }

    public function listClinics(Request $request)
    {
        $clinics = Clinic::simplePaginate(20);

        return view('admin.list_clinics')->with('clinics',$clinics);
    }

    public function addClinic(Request $request)
    {
        return view('admin.add_clinic');
    }

    public function clinicSubmit(Request $request)
    {
        $clinic = new Clinic;
        $clinic->clinic_name = $request->clinic_name;
        $clinic->c_image1 = Helper::upload_picture($request->file('c_image1'));
        $clinic->c_image2 = Helper::upload_picture($request->file('c_image2'));
        $clinic->c_image3 = Helper::upload_picture($request->file('c_image3'));
        $clinic->c_address = $request->address;
        $clinic->c_phone = $request->c_phone;
        $clinic->c_fax = $request->c_fax;
        $clinic->c_latitude = $request->c_lat;
        $clinic->c_longitude = $request->c_lng;

        $clinic->save();

        return redirect('/admin/listclinics');
    }

    public function associateClinic(Request $request)
    {
        $doctor = Doctor::find($request->segment(3));
        $clinics = Clinic::all();
        return view('admin.associate_clinic')->with('doctor',$doctor)->with('clinics',$clinics);
    }

    public function associateClinicSubmit(Request $request)
    {
        $doctor = Doctor::find($request->doctor_id);
        if($doctor)
        {
            $doctor->clinic_id = $request->clinic;
            $doctor->save();
        }

        return redirect('/admin/listdoctors');
    }

    public function viewDocuments(Request $request)
    {
        $documents = Document::where('doctor_id',$request->segment(3))->get();
        return view('admin.view_documents')->with('documents',$documents);
    }

    public function addEducation(Request $request)
    {
        $doctor = Doctor::find($request->segment(3));

        return view('admin.add_education')->with('doctor',$doctor);
    }

    public function addEducationSubmit(Request $request)
    {
        $doctor = Doctor::find($request->doctor_id);
        $doctors = Doctor::simplePaginate(10);

        $doctor->univ1 = $request->univ1;
        $doctor->degree1 = $request->degree1;
        $doctor->year1 = $request->year1;
        $doctor->univ2 = $request->univ2;
        $doctor->degree2 = $request->degree2;
        $doctor->year2 = $request->year2;

        $doctor->save();

        flash('Education Updated Successfully');
        return view('admin.list_doctors')->with('doctors',$doctors);

    }

    public function viewDoctorDetails(Request $request)
    {
        $doctor = Doctor::find($request->segment(3));
        $speciality = Speciality::all();
        $spec = Speciality::find($doctor->speciality_id);
        $current_speciality = $spec->speciality;
        $current_speciality_id = $spec->id;

        return view('admin.view_doctor_details')->with('doctor',$doctor)->with('speciality',$speciality)->with('current_speciality',$current_speciality)->with('current_speciality_id',$current_speciality_id);
    }

    public function viewDoctorDetailsSubmit(Request $request)
    {
        $doctors = Doctor::simplePaginate(10);
        $doctor = Doctor::find($request->doctor_id);
        if($doctor){
            if($request->has('first_name'))
                $doctor->first_name = $request->first_name;
            if($request->has('last_name'))
                $doctor->last_name = $request->last_name;
            if($request->has('phone'))
                $doctor->phone = $request->phone;
            if($request->has('email'))
                $doctor->email = $request->email;
            if($request->has('dob'))
                $doctor->dob = $request->dob;
            if($request->has('gender'))
                $doctor->gender = $request->gender;
            if($request->has('mednumber'))
                $doctor->med_number = $request->med_number;
            if($request->has('speciality'))
                $doctor->speciality_id = $request->speciality;
            if($request->has('experience'))
                $doctor->experience = $request->experience;
            if($request->has('degree1'))
                $doctor->degree1 = $request->degree1;
            if($request->has('univ1'))
                $doctor->univ1 = $request->univ1;
            if($request->has('year1'))
                $doctor->year1 = $request->year1;
            if($request->has('degree2'))
                $doctor->degree2 = $request->degree2;
            if($request->has('univ2'))
                $doctor->univ2 = $request->univ2;
            if($request->has('year2'))
                $doctor->year2 = $request->year2;
            if($request->has('nationality'))
                $doctor->nationality = $request->nationality;
            if($request->has('ssn'))
                $doctor->ssn = $request->ssn;

            $doctor->save();

            flash('Details Edited Successfully');

            return view('admin.list_doctors')->with('doctors',$doctors);
        }
    }

    public function associateSpeciality(Request $request)
    {

        $doctor = Doctor::find($request->segment(3));
        $speciality = Speciality::all();
        $spec = Speciality::find($doctor->speciality_id);

        if($doctor->speciality_id){
            $current_speciality = $spec->speciality;
            $current_speciality_id = $spec->id;
        }else{
            $current_speciality = 'no speciality';
            $current_speciality_id = 0;
        }




        return view('admin.associate_speciality')->with('doctor',$doctor)->with('speciality',$speciality)->with('current_speciality',$current_speciality)->with('current_speciality_id',$current_speciality_id);


    }

    public function associateSpecialitySubmit(Request $request)
    {
        $doctor = Doctor::find($request->doctor_id);
        $doctors = Doctor::simplePaginate(10);

        $doctor->speciality_id = $request->speciality;
        $doctor->save();

        flash('Specaility Associated Successfully');
        return redirect('/admin/listdoctors');

    }

    public function editClinic(Request $request)
    {
        $clinic = Clinic::find($request->segment(3));

        return view('admin.edit_clinic')->with('clinic',$clinic);

    }

    public function clinicEditSubmit(Request $request)
    {
        $clinic = Clinic::find($request->clinic_id);

        if($request->has('clinic_name'))
            $clinic->clinic_name = $request->clinic_name;
        if($request->file('c_image1'))
            $clinic->c_image1 = Helper::upload_picture($request->file('c_image1'));
        if($request->file('c_image2'))
            $clinic->c_image2 = Helper::upload_picture($request->file('c_image2'));
        if($request->file('c_image3'))
            $clinic->c_image3 = Helper::upload_picture($request->file('c_image3'));
        if($request->has('c_address')){
            $clinic->c_address = $request->address;
            $clinic->c_latitude = $request->c_lat;
            $clinic->c_longitude = $request->c_lng;
        }

        if($request->has('c_phone'))
            $clinic->c_phone = $request->c_phone;
        if($request->has('c_fax'))
            $clinic->c_fax = $request->c_fax;


        $clinic->save();

        return redirect('/admin/listclinics');
    }

    public function clinicDelete(Request $request)
    {

        Clinic::find($request->segment(3))->delete();
        return redirect('/admin/listclinics');
    }

    public function addTimeSlots(Request $request)
    {

        $speciality_id = $request->segment(3);
        $doctorSlots = DoctorSlot::where('speciality_id',$speciality_id)->get();


        return view('admin.add_time_slots')->with('doctorSlots',$doctorSlots)->with('speciality_id',$speciality_id);
    }

    public function editTimeSlot(Request $request)
    {

        $doctorSlot = DoctorSlot::find($request->segment(3));

        $speciality_id = $request->segment(4);


        if($doctorSlot){
            $start_time = $doctorSlot->start_time;
            $end_time  = $doctorSlot->end_time;
            $type = $doctorSlot->type;
            $slotId = $doctorSlot->id;
        }else{
            $start_time = "";
            $end_time  = "";
            $type = "";
            $slotId = 0;
        }

        return view('admin.edit_time_slot')->with('start_time',$start_time)->with('end_time',$end_time)->with('type',$type)->with('slotId',$slotId)->with('speciality_id',$speciality_id);

    }

    public function timeSlotSubmit(Request $request)
    {
        $doctorSlot = DoctorSlot::find($request->slotId);

        if(!$doctorSlot){
            $doctorSlot = new DoctorSlot;
        }

        $doctorSlot->start_time = $request->start_time;
        $doctorSlot->end_time = $request->end_time;
        $doctorSlot->type = $request->type;
        $doctorSlot->speciality_id = $request->specialityId;

        $doctorSlot->save();

        flash('Time Slot Updated Successfully');

        return redirect('admin/addtimeslots/'.$request->specialityId);

    }


    public function editSpecialities(Request $request)
    {
        $speciality = Speciality::find($request->segment(3));

        return view('admin.edit_speciality')->with('speciality',$speciality);

    }

    public function specialityEditSubmit(Request $request)
    {
        $speciality = Speciality::find($request->speciality_id);

        if($request->has('s_name'))
            $speciality->speciality=$request->s_name;
        if($request->has('s_name_chinese'))
            $speciality->speciality_chinese = $request->s_name_chinese;
        if($request->has('s_name_thai'))
            $speciality->speciality_thai = $request->s_name_thai;
        if($request->has('s_name_indonesian'))
            $speciality->speciality_indonesian = $request->s_name_indonesian;
         if($request->has('s_name_myanmar'))
            $speciality->speciality_myanmar = $request->s_name_myanmar;
         if($request->has('s_name_iran'))
            $speciality->speciality_iran = $request->s_name_iran;
         if($request->has('s_name_vietnamese'))
            $speciality->speciality_vietnamese = $request->s_name_vietnamese;
         if($request->has('s_name_japan'))
            $speciality->speciality_japan = $request->s_name_japan;
        if($request->file('s_picture'))
            $speciality->picture = Helper::upload_picture($request->file('s_picture'));
        $speciality->save();

        return redirect('/admin/listspecialities');

    }


     public function doctorPayment(Request $request)
    {   
        $requests = Requests::find($request->segment(3));

        if(!$requests)
        {
            return Redirect::back();
        }
        $doctor = Doctor::find($requests->confirmed_doctor);
        if(!$doctor)
        {
            return Redirect::back();
        }
        $setting = Setting::find(1);
        $percentage = $setting->percentage;
        $amountToBePaid = $requests->amount-(($requests->amount*$percentage)/100);


        return view('admin.doctor_payment')->with('requests',$requests)->with('doctor',$doctor)->with('amountToBePaid',$amountToBePaid);
    }

    public function changePaymentStatus(Request $request)
    {
        $requests = Requests::find($request->segment((3)));

        if($requests->is_doctor_paid == 0)
            $requests->is_doctor_paid = 1;
        else
            $requests->is_doctor_paid = 0;

        $requests->save();

            return redirect::back();

    }



}