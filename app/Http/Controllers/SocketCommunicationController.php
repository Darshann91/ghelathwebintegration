<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Requests;


class SocketCommunicationController extends Controller
{

	public function __construct(Requests $requests)
	{
		$this->secret_key = config('SocketIOServer.server_internal_communication_key');
		$this->requests = $requests;
	}


	protected function remoteProcedureCall(Request $request)
	{
		$this->clientRequestBag = $request;

		if(is_null($request->secret_key) 
			|| $request->secret_key == ""
			|| is_null($this->secret_key) 
			|| $this->secret_key == ""
			|| $request->secret_key !== $this->secret_key
		) {

			return response()->json($this->unauthorizedResponse());
		} 


		if(!method_exists($this, $request->method)) {
			return response()->json($this->unauthorizedResponse());
		}


		$parameters = is_array($request->params) ? $request->params : [$request->params];

		if(is_array($parameters)) {
			return call_user_func_array([$this, $request->method], $parameters);
		} else {
			return call_user_func([$this, $request->method], $parameters);
		}
		

	}


	protected function unauthorizedResponse()
	{
		return [
			'success' => false,
			'error_code' => 403,
			'error_message' => 'Unauthorized access'
		];
	}


	/* After this segment all the methods will act as normal controller methods */




	/**
	* This method used to get details of request table record by id 
	*/
	protected function getRequestDetailByID($requestID)
	{
		$request = $this->requests->find($requestID);
		if(!$request) {
			return response()->json($this->unauthorizedResponse());
		}

		return response()->json($request->toArray());

	}



	/**
	* This method used to save chat message details
	*/
	protected function saveChatMessage()
	{
		$this->doctorApi = app('App\Http\Controllers\DoctorApiController');
		return $this->doctorApi->saveChatMessage($this->clientRequestBag);
	}


}
