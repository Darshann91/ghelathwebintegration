<?php

namespace App\Http\Controllers;


use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;

use App\Shop;
use App\Doctor;
use Response;
use Illuminate\Http\Request;
use Validator;
use DB;
use App\Helpers\Helper;
use App\Product;

define('FULL_TIME', 1);
define('PART_TIME_CHANGE_FULL_TIME', 2);
define('PART_TIME', 3);
define('OTHERS', 4);

//PAYMENTS
define('PAYPAL', 1);
define('PAYMENT_MAIL', 2);
define('OTHERS', 3);


class ShopDoctorApiController extends Controller
{


    public function __construct(Request $request)
    {
        $this->middleware('DoctorApiVal' , array('except' => ['register' , 'login' , 'forgot_password','getSpeciality','generateOtp']));
    }


    //Create new Shop
    public function addShop(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            array(
                'shop_name' => 'required',
                'shop_language' =>'required',
                'shop_currency' =>'required',
                'shop_country' =>'required',
                'describe' => 'required',
                'payments' => 'required'
            ));

        if ($validator->fails()) {
            $error_messages = implode(',', $validator->messages()->all());
            $response_array = array('success' => false, 'error_message' =>$error_messages , 'error_code' => 101, 'error_messages'=>$error_messages);

        } else {

            $shop = new Shop;
            $shop->doctor_id = $request->id;
            $shop->shop_name = $request->shop_name;
            $shop->shop_language = $request->shop_language;
            $shop->shop_currency = $request->shop_currency;
            $shop->shop_country = $request->shop_country;
            $shop->describe = $request->describe;
            $shop->payments = $request->payments;

            $shop->save();

            $response_array = array('success' => true ,
                'shop_id' => $shop->id,
                'shop_name'=> $shop->shop_name,
                'shop_language'=>$shop->shop_language,
                'shop_currency'=>$shop->shop_currency,
                'shop_country'=>$shop->shop_country,
                'describe'=> $shop->describe,
                'payments'=>$shop->payments);

        }

        $response = response()->json($response_array, 200);
        return $response;
    }

    public function getShop(Request $request)
    {
        $shop = Shop::where('doctor_id',$request->id)->first();
        if($shop){

            $response_array = array('success' => true ,
                'shop_id' => $shop->id,
                'shop_name'=> $shop->shop_name,
                'shop_language'=>$shop->shop_language,
                'shop_currency'=>$shop->shop_currency,
                'shop_country'=>$shop->shop_country,
                'describe'=> $shop->describe,
                'payments'=>$shop->payments);

        }else{
            $response_array = array('success' => false,'error_message'=>'No shops found');
        }

        $response = response()->json($response_array, 200);
        return $response;

    }

    public function editShop(Request $request)
    {

        $validator = Validator::make(
            $request->all(),
            array(
                'shop_id' => 'required',
            ));

        if ($validator->fails()) {
            $error_messages = implode(',', $validator->messages()->all());
            $response_array = array('success' => false, 'error_message' =>$error_messages , 'error_code' => 101, 'error_messages'=>$error_messages);

        } else {
            $shop = Shop::find($request->shop_id);
            if($request->has('shop_name'))
                $shop->shop_name = $request->shop_name;

            if($request->has('shop_lanuage'))
                $shop->shop_language = $request->shop_language;

            if($request->has('shop_currency'))
                $shop->shop_currency = $request->shop_currency;

            if($request->has('shop_country'))
                $shop->shop_country = $request->shop_country;

            if($request->has('describe'))
                $shop->describe = $request->describe;

            if($request->has('payments'))
                $shop->payments = $request->payments;

            $shop->save();

            $response_array = array('success' => true ,
                'shop_id' => $shop->id,
                'shop_name'=> $shop->shop_name,
                'shop_language'=>$shop->shop_language,
                'shop_currency'=>$shop->shop_currency,
                'shop_country'=>$shop->shop_country,
                'describe'=> $shop->describe,
                'payments'=>$shop->payments);



        }
        $response = response()->json($response_array, 200);
        return $response;

    }



    public function getProductCategories(Request $request)
    {

        if(!$request->has('skip'))
            $skip = 0;
        else
            $skip = $request->skip;

        if(!$request->has('take'))
            $take = 100;
        else
            $take = $request->take;


        $prodCategories = DB::table('products_categories')->skip($skip)->take($take)->get();

        $categoryArray = array();

        foreach ($prodCategories as $prodCategory)
        {
            $data['id'] = $prodCategory->id;
            $data['category_name'] = $prodCategory->category_name;
            $data['picture'] = $prodCategory->picture;

            array_push($categoryArray,$data);
        }

        $response_array = array(
            'success' => true,
            'data' => $categoryArray,
        );

        $response = response()->json($response_array, 200);
        return $response;
    }



    public function addProduct(Request $request)
    {

        $validator = Validator::make(
            $request->all(),
            array(
                'shop_id' => 'required',
                'product_category' =>'required',
                'product_name' =>'required',
                'product_description' =>'required',
                'currency' => 'required',
                'price' => 'required',
                'price2' => 'required',
                'status' =>'required',
                'pic1' =>'required',
                'pic2' =>'required',
                'pic3' =>'required',
                'quantity' =>'required'
            ));

        if ($validator->fails()) {
            $error_messages = implode(',', $validator->messages()->all());
            $response_array = array('success' => false, 'error_message' =>$error_messages , 'error_code' => 101, 'error_messages'=>$error_messages);

        } else {

            $product = new Product;
            $product->doctor_id = $request->id;
            $product->shop_id = $request->shop_id;
            $product->product_category = $request->product_category;
            $product->product_name = $request->product_name;
            $product->product_description = $request->product_description;
            $product->currency = $request->currency;
            $product->price = $request->currency;
            $product->price2 = $request->price2;
            $product->status = $request->status;
            $product->pic1 = Helper::upload_picture($request->file('pic1'));
            $product->pic2 = Helper::upload_picture($request->file('pic2'));
            $product->pic3 = Helper::upload_picture($request->file('pic3'));
            $product->quantity = $request->quantity;
            if($request->has('tag'))
                $product->tag = $request->tag;

            $product->save();

            $response_array = array('success' => true ,
                'shop_id' => $product->shop_id,
                'product_category'=> $product->product_category,
                'product_name' => $product->product_name,
                'product_description'=>$product->product_description,
                'currency'=>$product->currency,
                'price'=>$product->price,
                'price2'=> $product->price2,
                'status'=>$product->status,
                'pic1'=>$product->pic1,
                'pic2'=>$product->pic2,
                'pic3'=> $product->pic3,
                'quantity'=>$product->quantity,
                'tag' => $product->tag);

            $response_array = Helper::null_safe($response_array);


        }

        $response = response()->json($response_array, 200);
        return $response;

    }

    public function getMyProducts(Request $request)
    {
        $products = Product::where('doctor_id',$request->id)->get();

        $productArray = array();

        foreach ($products as $product)
        {
            $info['product_id'] = $product->id;
            $info['product_name'] = $product->product_name;
            $info['pic1'] = $product->pic1;

            array_push($productArray,$info);

        }

        $response_array = array(
            'success' => true,
            'data' => $productArray,
        );

        $response = response()->json($response_array, 200);
        return $response;
    }

    public function getSingleProduct(Request $request)
    {

        $validator = Validator::make(
            $request->all(),
            array(
                'product_id' => 'required',

            ));

        if ($validator->fails()) {
            $error_messages = implode(',', $validator->messages()->all());
            $response_array = array('success' => false, 'error_message' =>$error_messages , 'error_code' => 101, 'error_messages'=>$error_messages);

        } else {

            $product = Product::find($request->product_id);

            if($product){

                $response_array = array('success' => true ,
                    'shop_id' => $product->shop_id,
                    'product_category'=> $product->product_category,
                    'product_name' => $product->product_name,
                    'product_description'=>$product->product_description,
                    'currency'=>$product->currency,
                    'price'=>$product->price,
                    'price2'=> $product->price2,
                    'status'=>$product->status,
                    'pic1'=>$product->pic1,
                    'pic2'=>$product->pic2,
                    'pic3'=> $product->pic3,
                    'quantity'=>$product->quantity,
                    'tag' => $product->tag);

                $response_array = Helper::null_safe($response_array);

            }


        }

        $response = response()->json($response_array, 200);
        return $response;

    }





}