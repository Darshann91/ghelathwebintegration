<?php

namespace App\Http\Controllers;

use App\Repositories\OTPRepository as OtpRepo;
use App\Repositories\UserRepository as UserRepo;
use App\Repositories\DoctorRepository as DocRepo;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Log;
use App\Article;
use App\Doctor;
use App\Clinic;
use Curl;

class UserWebController extends Controller
{


    public function __construct(OtpRepo $otpRepo, UserRepo $userRepo, DocRepo $docRepo)
    {
        $this->authUser = Auth::guard('web')->user();
        $this->otpRepo  = $otpRepo;
        $this->userRepo = $userRepo;
        $this->docRepo  = $docRepo;
        $this->userApi  = app('App\Http\Controllers\UserApiController');
    }


    public function showHome()
    {   
        $specialities = $this->userRepo->getAllSpecialities(true);
        $articles = $this->userRepo->getAllArticles(true);
        
        $countSpecialities = count($specialities);
        $auth_user = $this->authUser;

        $r = Curl::to('http://52.220.193.142:3000/api/v1/users/category/get-categories')
                                    ->get();

        $ecomResponse = \GuzzleHttp\json_decode($r);
        $healthCategories = $ecomResponse->response->categories;

    	return view('home.patient.home', compact('specialities', 'auth_user', 'countSpecialities','articles','healthCategories'));
    }



    /**
    *   show user registation form view
    */
    public function showRegister()
    {
        $countryCodes = DocRepo::getCountryCodesSortByName();
        return view('home.patient.register', compact('countryCodes'));
    }


    public function register(Request $request)
    {

        //chekc login type is social or not
        //if social then take social unique id
        //if registerd  alredy error
        //if not registered then register
    	
    	if(!UserRepo::validateUserRegiserData($request->all(), $errors)) {
    		return response()->json(UserRepo::jsonFormatResponse(
    				false, 'VALIDATION_ERROR', 'Validation error', $errors
    		));
    	}
        

        if(!OtpRepo::verifyOTP($request->country_code, $request->phone, $request->otp)) {
        	return response()->json(UserRepo::jsonFormatResponse(
    				false, 'INVALID_OTP', 'Otp does not match. Resend again.'
    		));	
        }

        $user = UserRepo::createUser($request->all());

      	if(!$user){
      		return response()->json(UserRepo::jsonFormatResponse(false, 'UNKNOWN_ERROR', 'Unknown error'));
      	} 

      	UserRepo::sendUserVerificationMail($user);
      	return response()->json(UserRepo::jsonFormatResponse(
      		true, 'USER_REGISTERED', 'User registered successfully.', ['user' => UserRepo::clockUserData($user)->toArray()]
      	));
            
    }






    public function showSettings()
    {
        $auth_user = $this->authUser;
        return view('home.patient.settings', compact('auth_user'));
    }




    /**
    *   returns the user profile detials update view
    */
    public function profileTemplate()
    {
        $countryCodes = $this->docRepo->getCountryCodesSortByName();
        return view('home.patient.settings_profile_template', compact('countryCodes'));
    }


    public function profileDetails()
    {
        return response()->json($this->userRepo->jsonFormatResponse(
            true, "USER_PROFILE", 'User profile retrived', $this->userRepo->clockUserData($this->authUser)
        ));
    }


    public function upateProfile(Request $request)
    {
        if(!$this->userRepo->validateUpdateProfileData(
                $this->authUser->id, $request->all(), $errors
        )) {

            return response()->json(
                $this->userRepo->jsonFormatResponse(false, 'VALIDATION_ERROR', 'Validation error', $errors)
            );
        }


        if(!OtpRepo::verifyOTP($request->country_code, $request->phone, $request->otp)) {
            return response()->json(DocRepo::jsonFormatResponse(
                    false, 'INVALID_OTP', 'Otp does not match. Resend again.'
            )); 
        }


        $user = $this->userRepo->updateProfile($this->authUser, $request->all());

        if(!$user){   
            return response()->json($this->userRepo->jsonFormatResponse(false, 'UNKNOWN_ERROR', 'Unknown error'));
        } else {

            return response()->json($this->userRepo->jsonFormatResponse(
                true, "USER_PROFILE_UPDATED", 'User profile updated', $this->userRepo->clockUserData($user)
            ));
        }

    }







    public function schedulesTemplate()
    {
        return view('home.patient.settings_schedules_template');
    }


    public function getMySchedules(Request $request)
    {
        $request->request->add([ 
            'id' => $this->authUser->id,
            'date' => $this->docRepo->formatMyScheduleDate($request->date)
        ]);

        return $this->userApi->mySchedule($request);
    }



    public function getScheduledDates()
    {
        $dates = $this->userRepo->getScheduledDates($this->authUser->id);
        return response()->json(
            $this->userRepo->jsonFormatResponse(
                true, 'USER_SCHEDULED_DATES', 'Scheduled dates', $dates
            )
        );
    }



    public function getSpecialities()
    {
        return response()->json(
            $this->userRepo->jsonFormatResponse(
                true, 'SPECIALITIES', 'Specialities', $this->userRepo->getAllSpecialities(true)
            )
        );
    }




    public function showCategories(Request $request)
    {
        $auth_user = $this->authUser;
        return view('home.patient.categories', compact("auth_user"));
    }




    public function onlineConsultTemple()
    {
        return view('home.patient.online_consult_template');
    }


    public function getLastOngoingRequest(Request $request)
    {
        $request = $this->userRepo->getLastOnProgressRequest($this->authUser->id);
        return response()->json([
            'success' => $request ? true : false,
            'request' => $request
        ]);
    }   


    public function getOnlineDoctorsList(Request $request)
    {
        $request->request->add([ 
            'id' => $this->authUser->id            
        ]);
        return $this->userApi->getDoctorListOnline($request);
    }





    public function bookAppointmentTemplate()
    {
        return view('home.patient.book_appointment_template');
    }



    public function getBookingDoctorsList(Request $request)
    {   
        $request->request->add([ 
            'id' => $this->authUser->id            
        ]);
        return $this->userApi->getDoctorListBooking($request);
    }






    public function getAvailableSlots(Request $request)
    {
        $request->request->add([ 
            'id' => $this->authUser->id,
            'date' => $this->docRepo->formatMyScheduleDate($request->date)          
        ]);
        return $this->userApi->getAvailableSlots($request);
    }



    public function userCardsTemplate(Request $request)
    {
        return view('home.patient.card_settings_template');
    }



    public function getUserSavedCards(Request $request)
    {
        $request->request->add([ 'id' => $this->authUser->id]);
        return $this->userApi->getCards($request);
    }


    public function makeUserCardDefault(Request $request)
    {
        $request->request->add([ 'id' => $this->authUser->id]);
        return $this->userApi->selectCard($request);
    }



    public function getBraintreeClientToken(Request $request)
    {
        return $this->userApi->getBraintreeToken($request);
    }



    public function saveCard(Request $request)
    {
        $request->request->add([ 'id' => $this->authUser->id]);
        return $this->userApi->userAddCard($request);
    }


    public function bookSlot(Request $request)
    {
        $request->request->add([ 'id' => $this->authUser->id]);
        return $this->userApi->bookSlots($request);
    }








    /* creating request for video, audio, chat */
    public function createRequest(Request $request)
    {
        $request->request->add([ 
            'id' => $this->authUser->id
        ]);

        return $this->userApi->createRequest($request);
    }



    public function cancelRequest(Request $request)
    {
        return $this->userApi->cancelRequest($request);
    }



    public function getRequest(Request $request)
    {
        $request->request->add([ 
            'id' => $this->authUser->id
        ]);
        return $this->userApi->getRequest($request);
    }



    public function createOpentokSession(Request $request)
    {
        return $this->userApi->generateOpentok($request);
    }


    public function clearDebt(Request $request)
    {
        $request->request->add(['id' => $this->authUser->id]);
        return $this->userApi->clearDebt($request);
    }


    public function rateDoctor(Request $request)
    {
        $request->request->add(['id' => $this->authUser->id]);
        return $this->userApi->rate_doctor($request);
    }



    public function uploadImage(Request $request)
    {
        return $this->userApi->uploadAttachment($request);
    }





    public function changePasswordTemplate()
    {
        return view('home.patient.settings_change_password_template');
    }



    public function changePassword(Request $request)
    {
        $request->request->add(['id' => $this->authUser->id]);
        return $this->userApi->changePassword($request);
    }


    public function chatConsultTemplate()
    {
        return view('home.patient.settings_chat_consult_history_template');
    }

    public function getChatConsultHistories(Request $request)
    {
        $request->request->add(['id' => $this->authUser->id]);
        return $this->userApi->consultHistoryChat($request);
    }


    public function getChatMessages(Request $request)
    {
        $messages = $this->userRepo->getChatMessages($this->authUser->id, $request->request_id);
        
        return response()->json([
            'success' => true,
            'chatArray' => $messages
        ]);
    }



    public function phoneConsultHistoryTemplate()
    {
        return view('home.patient.settings_phone_consult_history_template');
    }

    public function getPhoneConsultHistories(Request $request)
    {
        $request->request->add(['id' => $this->authUser->id]);
        return $this->userApi->consultHistoryPhone($request);
    }



    public function videoConsultHistoryTemplate()
    {
        return view('home.patient.settings_video_consult_history_template');
    }

    public function getVideoConsultHistories(Request $request)
    {
        $request->request->add(['id' => $this->authUser->id]);
        return $this->userApi->consultHistoryVideo($request);
    }




    public function askQuestionTemplate()
    {
        return view('home.patient.settings_ask_question_template');
    }

    public function askQuestion(Request $request)
    {
        $request->request->add(['id' => $this->authUser->id]);
        return $this->userApi->askQuestion($request);
    }



    public function searchDoctors(Request $request)
    {
        $doctors = $this->userRepo->searchDoctors(
            $request->speciality_id, 
            $request->doctor_name, 
            $request->nDocotr
        );

        return response()->json([
            "success" => true, 
            'doctors' => $doctors->toArray()
        ]);
    }

    public function showArticle(Request $request)
    {
        $article_id = $request->segment(4);
        $article = Article::find($article_id);
        $doctor = Doctor::find($article->doctor_id);
        $clinic = Clinic::find($doctor->clinic_id);

        return view('home.patient.article')->with('article',$article)->with('doctor',$doctor)->with('clinic',$clinic);

    }

    public function showAllArticles(Request $request)
    {
        $articles = Article::all();
        return view('home.patient.articles')->with('articles',$articles);
    }



    public function makeFavouriteDoctor(Request $request)
    {
        $response = $this->userRepo->makeFavouriteDoctor(
            $this->authUser->id, 
            $request->doctor_id, 
            $request->is_fav
        );


        switch ($response) {
            
            case 'ALREADY_EXISTS':

                return response()->json([
                    'success' => false,
                    'error_type' => 'ALREADY_FAV',
                    'error_text' => 'Doctor is already favourited'
                ]);
                break;

            case 'FAV_SUCCESS':

                return response()->json([
                    'success' => true,
                    'success_type' => 'FAVOURITED',
                    'success_text' => 'Doctor added to your favourited doctors list successfully'
                ]);
                break;

            case 'UNFAV_SUCCESS':

                return response()->json([
                    'success' => true,
                    'success_type' => 'FAVOURITED',
                    'success_text' => 'Doctor removed from your favourited doctors list'
                ]);
                break;
            
        }

        
    }




    public function doctorMessagesTemplate()
    {
        return view('home.patient.doctor_messages_template');
    }

    public function getMessages(Request $request)
    {
        $request->request->add(['id' => $this->authUser->id]);
        return $this->userApi->getDoctorMessages($request);
    }


    public function sendMessageReply(Request $request)
    {
        $request->request->add(['id' => $this->authUser->id]);
        return $this->userApi->replyDoctorMessage($request);
    }

}