<?php

namespace App\Http\Controllers;


use App\Product;
use Illuminate\Http\Request;
use App\ProductsCategory;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use DB;


class ShopUserApiController extends Controller
{

    public function __construct(Request $request)
    {
        $this->middleware('UserApiVal' , array('except' => ['register' , 'login' , 'forgot_password','generateOtp','testTwilio','cronNotification']));
    }


    public function getProductCategories(Request $request)
    {

        if(!$request->has('skip'))
            $skip = 0;
        else
            $skip = $request->skip;

        if(!$request->has('take'))
            $take = 100;
        else
            $take = $request->take;


        $prodCategories = DB::table('products_categories')->skip($skip)->take($take)->get();

        $categoryArray = array();

        foreach ($prodCategories as $prodCategory)
        {
            $data['id'] = $prodCategory->id;
            $data['category_name'] = $prodCategory->category_name;
            $data['picture'] = $prodCategory->picture;

            array_push($categoryArray,$data);
        }

        $response_array = array(
            'success' => true,
            'data' => $categoryArray,
        );

        $response = response()->json($response_array, 200);
        return $response;
    }

    public function getProducts(Request $request)
    {
        if($request->has('category_id')){
            $products = Product::where('product_category',$request->category_id)->get();
        }else{
            $products = Product::all();
        }

        productsArray = array();

        foreach ($products as $product)
        {
            $info['product_id'] = $product->id;
            $info['product_name'] = $product->product_name;
            $info['product_image'] = $product->pic1;
            $info['product_']
        }

    }




}