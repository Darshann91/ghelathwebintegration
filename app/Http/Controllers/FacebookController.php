<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Repositories\SocialLoginRepository;
use Illuminate\Http\Request;
use Socialite;
use Auth;

class FacebookController extends Controller
{

	public function __construct(SocialLoginRepository $socialRepo)
	{
		$this->socialRepo = $socialRepo;
	}


	public function providerRedirect(Request $request)
	{
		session(['social_login_user_type' => $request->user_type]);
		return Socialite::driver('facebook')
						->fields($this->fields())
						->scopes($this->scopes())
						->redirect();
	}


	public function fields()
	{
		return ['name','email', 'gender', 'birthday', 'hometown'];
	}


	protected function scopes()
	{
		return [
			'email', 'user_birthday',
			'public_profile', 'user_hometown'
		];
	}

	public function providerCallback(Request $request)
	{
		try {

			$user = Socialite::driver('facebook')->fields($this->fields())->user();			

			$this->socialRepo->setSocialID($user->getId())->setUserEmail($user->getEmail());
			
			$user_type = session()->get('social_login_user_type');
			$storedUser = $this->socialRepo->getStoredUser($user_type);

			if($storedUser) {

				if($user_type == 'user')
					Auth::login($storedUser);
				else if($user_type == 'doctor')
					Auth::guard('doctor-web')->login($storedUser);

				session()->forget('social_login_user_type');
				return $this->socialRepo->successResponse(['login_status' => true], true);
			}



			$this->socialRepo->setUsername($user->getName())
							->setUserAvatarURL($user->getAvatar());



			if(isset($user->user['gender'])) {
				$this->socialRepo->setGender($user->user['gender']);
			}

			if(isset($user->user['birthday'])) {
				$dob = \DateTime::createFromFormat('m/d/Y', $user->user['birthday'])
								->format('d-m-Y');
				$this->socialRepo->setDob($dob);
			}
			session()->forget('social_login_user_type');
			return $this->socialRepo->successResponse(['social_provider' => 'facebook']);

		} catch(\Exception $e){
			session()->forget('social_login_user_type');
			return $this->socialRepo->errorResponse($e->getMessage());
		}

	}

}