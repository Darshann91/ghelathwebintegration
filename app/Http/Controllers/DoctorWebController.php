<?php

namespace App\Http\Controllers;

use App\Repositories\OTPRepository as OtpRepo;
use App\Repositories\DoctorRepository as DocRepo;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Auth;
use Log;
use Hash;

class DoctorWebController extends Controller
{

    public function __construct(DocRepo $docRepo)
    {
        $this->authDoc = Auth::guard('doctor-web')->user();
        $this->docRepo = $docRepo;
        $this->doctorApi = app('App\Http\Controllers\DoctorApiController');
    }



    public function showSettings()
    {
        $auth_doctor = $this->authDoc;
        return view('home.doctor.settings', compact('auth_doctor'));
    }









    /**
    *   returns the user profile detials update view
    */
    public function profileTemplate()
    {
       
        $countryCodes = $this->docRepo->getCountryCodesSortByName();
        return view('home.doctor.settings_profile_template', compact('countryCodes'));
    }


    public function profileDetails()
    {
        $doctor_details = $this->docRepo->getProfileDetails($this->authDoc);
        return $this->docRepo->jsonFormatResponse(
            true, 'DOCTOR_PROFILE_DETAILS', 'Doctor profile details retrived', $doctor_details
        );
    }


    public function upateProfileDetails(Request $request)
    {
        if(!$this->docRepo->updateDoctorProfileDataValidate($this->authDoc->id, $request->all(), $errors)) {
            return response()->json(
                $this->docRepo->jsonFormatResponse(false, 'VALIDATION_ERROR', 'Validation error', $errors)
            );
        }
        

        $changePassword = false;
        if(!is_null($request->old_password) && !is_null($request->new_password)) {
           
            if(strlen($request->old_password) < 6 || strlen($request->new_password) < 6) {

                return response()->json(
                    $this->docRepo->jsonFormatResponse(false, 'PASSWORDS_LENGHT_LESS_SIX', 'Password lenght must be 6+ characters')
                );

            } else if(!Hash::check($request->old_password, $this->authDoc->password)) {

                return response()->json(
                    $this->docRepo->jsonFormatResponse(false, 'INVALID_OLD_PASS', 'Old password does not match')
                );

            } else {

                $changePassword = true;
            }
        }



        if(!OtpRepo::verifyOTP($request->country_code, $request->phone, $request->otp)) {
            return response()->json(DocRepo::jsonFormatResponse(
                    false, 'INVALID_OTP', 'Otp does not match. Resend again.'
            )); 
        }

        $doctor = $this->docRepo->updateDoctorProfile($this->authDoc, $request->all(), $changePassword);

        if(!$doctor){   
            return response()->json(DocRepo::jsonFormatResponse(false, 'UNKNOWN_ERROR', 'Unknown error'));
        } 

        $doctor_details = $this->docRepo->getProfileDetails($this->authDoc);
        return response()->json(
            DocRepo::jsonFormatResponse(true, 'DOCTOR_PROFILE_UPDATED', 'Doctor profile updated successfully.', $doctor_details)
        );


    }











    public function schedulesTemplate()
    {
        return view('home.doctor.settings_schedules_template');
    }

    public function getMySchedules(Request $request)
    {
        $request->request->add([ 
            'id' => $this->authDoc->id,
            'date' => $this->docRepo->formatMyScheduleDate($request->date)
        ]);
        return $this->doctorApi->mySchedule($request);
    }


    public function getScheduleDates() {
        $dates = $this->docRepo->getScheduledDates($this->authDoc->id);
        return response()->json(
            $this->docRepo->jsonFormatResponse(
                true, 'DOCTOR_SCHEDULED_DATES', 'Scheduled dates', $dates
            )
        );
    }







    /**
    *   show doctor registation form view
    */
    public function showRegister()
    {
        $countryCodes = DocRepo::getCountryCodesSortByName();
        return view('home.doctor.register', compact('countryCodes'));
    }




    public function register(Request $request)
    {   
    	if(!DocRepo::validateDoctorRegiserData($request->all(), $errors)) {
    		return response()->json(DocRepo::jsonFormatResponse(
    				false, 'VALIDATION_ERROR', 'Validation error', $errors
    		));
    	}
        

        if(!OtpRepo::verifyOTP($request->country_code, $request->phone, $request->otp)) {
        	return response()->json(DocRepo::jsonFormatResponse(
    				false, 'INVALID_OTP', 'Otp does not match. Resend again.'
    		));	
        }

        $doctor = DocRepo::createDoctor($request->all());

      	if(!$doctor){
      		return response()->json(DocRepo::jsonFormatResponse(false, 'UNKNOWN_ERROR', 'Unknown error'));
      	} 

      	
      	return response()->json(DocRepo::jsonFormatResponse(
      		true, 'DOCTOR_REGISTERED', 'Doctor registered successfully.', ['doctor' => DocRepo::clockDoctorData($doctor)->toArray()]
      	));
    }







    public function showHome()
    {
        return view('home.doctor.home');
    }


    public function getAppointmentsTemplate()
    {
        $consultStatus = $this->docRepo->getDoctorConsultStatus($this->authDoc->id);
        return view('home.doctor.appointments_template', compact('consultStatus'));
    }



    public function setConsultStatus(Request $request)
    {
        $request->request->add(['id' => $this->authDoc->id]);
        return $this->doctorApi->setConsults($request);
    }


    public function getSlots(Request $request)
    {
        $request->request->add(['id' => $this->authDoc->id]);
        return $this->doctorApi->getSlots($request);
    }


    public function getSavedSlotsByDate(Request $request)
    {
        $request->request->add([ 
            'id' => $this->authDoc->id,
            'date' => $this->docRepo->formatMyScheduleDate($request->date)
        ]);
        return $this->doctorApi->getDoctorSlots($request);
    }



    public function saveSlots(Request $request)
    {
        $request->request->add([ 
            'id' => $this->authDoc->id
        ]);
        return $this->doctorApi->sendSlots($request);
    }





    public function getIncomingRequest(Request $request)
    {
        $request->request->add(['id' => $this->authDoc->id]);
        return $this->doctorApi->get_incoming_request($request);
    }


    public function createOpentokSession(Request $request)
    {
        return $this->doctorApi->generateOpentok($request);
    }



    public function serviceAccept(Request $request)
    {
        $request->request->add([ 
            'id' => $this->authDoc->id
        ]);

        return $this->doctorApi->service_accept($request);
    }


    public function serviceReject(Request $request)
    {
        $request->request->add([ 
            'id' => $this->authDoc->id
        ]);
        return $this->doctorApi->service_reject($request);
    }



    public function serviceOnlineCompleted(Request $request)
    {
        $request->request->add([ 
            'id' => $this->authDoc->id
        ]);
        return $this->doctorApi->serviceCompletedOnline($request);
    }


    public function rateUser(Request $request)
    {
        $request->request->add(['id' => $this->authDoc->id]);
        return $this->doctorApi->rate_user($request);
    }


    public function getPatientsFavourite(Request $request)
    {
        $request->request->add(['id' => $this->authDoc->id]);
        return $this->doctorApi->getPatientsFav($request);
    }


    public function getMessages(Request $request)
    {
        $request->request->add(['id' => $this->authDoc->id]);
        return $this->doctorApi->getMessageList($request);
    }


    public function sendMessageToUser(Request $request)
    {
        $request->request->add(['id' => $this->authDoc->id]);
        return $this->doctorApi->sendMessageToUser($request);
    }

    public function getMesssagesTemplate()
    {
        return view('home.doctor.settings_messages_template');
    }




    /**
    *   end user appointment booking and make payment
    *   required request_id, doctor id, 
    */
    public function endAppointment(Request $request)
    {
        $request->request->add([
            'id' => $this->authDoc->id
        ]);
        return $this->doctorApi->endBooking($request);
    }   



    /**
    *   cancel user appointment booking
    *   required available_slot_id, doctor id, 
    */
    public function cancelAppointment(Request $request)
    {
        $request->request->add([
            'id' => $this->authDoc->id
        ]);
        return $this->doctorApi->cancelAppointment($request);
    }



}
