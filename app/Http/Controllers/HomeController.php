<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Repositories\OTPRepository as OtpRepo;
use App\Repositories\UserRepository as UserRepo;
use App\Repositories\DoctorRepository as DocRepo;
use Curl;

class HomeController extends Controller
{
	public function __construct(OtpRepo $otpRepo, UserRepo $userRepo, DocRepo $docRepo)
    {
        
        $this->otpRepo  = $otpRepo;
        $this->userRepo = $userRepo;
        $this->docRepo  = $docRepo;
        $this->userApi  = app('App\Http\Controllers\UserApiController');
    }


    public function index()
    {
    	$specialities = $this->userRepo->getAllSpecialities(true);
    	$articles = $this->userRepo->getAllArticles(true);
    	$countSpecialities = count($specialities);

        $r = Curl::to('http://52.220.193.142:3000/api/v1/users/category/get-categories')
                                    ->get();

        $ecomResponse = \GuzzleHttp\json_decode($r);
        $healthCategories = $ecomResponse->response->categories;


        return view('home.home', compact('articles','specialities','countSpecialities','healthCategories'));
    }
}
