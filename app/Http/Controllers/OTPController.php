<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Repositories\OTPRepository as OtpRepo;
use Illuminate\Http\Request;
use Log;

class OTPController extends Controller
{

	public function sendOtp(Request $request)
	{
		if($request->country_code == "" || !OtpRepo::countryCodePatMatched($request->country_code) || $request->phone == "") {
			return response()->json(
				$this->jsonFormatResponse(false, 'VALIDATION_ERROR', 'Country code and phone is required')
			);
		}

		if(OtpRepo::sendOTP($request->country_code, $request->phone, 'Your one time code is : {{otp_code}}', $error)) {
			return response()->json(
				$this->jsonFormatResponse(true, 'OTP_SENT', 'Otp sent successfully')
			);
		} else {
			Log::info('otp send failed ' . $error);
			return response()->json(
				$this->jsonFormatResponse(false, 'UNKNOWN_ERROR', 'Otp send failed')
			);
		}

	}



	protected function jsonFormatResponse($isSuccess, $type, $message)
	{
		if($isSuccess) {
			return [
				'status' => true,
				'success_type' => $type,
				'success_text' => $message
			];
		} else {
			return [
				'status' => false,
				'error_type' => $type,
				'error_text' => $message
			];
		}
	}


}
