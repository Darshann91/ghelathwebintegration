<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
//crons
Route::get('/cron/recurring_slot', 'DoctorApiController@recurring_slot_cron');
Route::get('/verifyemail/{id}','UserApiController@verifyEmail');

Route::get('/cron/currencyMultiplierCron','UserApiController@currencyMultiplierCron');
//

Route::get('/verifyemail/{id}','UserApiController@verifyEmail');
Route::post('/forgotpassword','UserApiController@forgotPassword');
Route::post('/forgetpassword', 'UserApiController@forgetPassword');

Route::get('/iospush','UserApiController@iosPushTest');

Route::get('/userprivacy', function(){
	return Response::view('terms.user_privacy');
});

Route::get('/usersubterms', function(){
	return Response::view('terms.user_subscriber_terms');
});

Route::get('/userterms', function(){
	return Response::view('terms.user_terms');
});

Route::get('/doctorprivacy', function(){
	return Response::view('terms.doctor_privacy');
});

Route::get('/doctorsubterms', function(){
	return Response::view('terms.doctor_subscriber_terms');
});

Route::get('/doctorterms', function(){
	return Response::view('terms.doctor_terms');
});

Route::group(['prefix' => 'admin'], function(){

	Route::get('login', 'Auth\AdminAuthController@showLoginForm')->name('admin.login');

    Route::post('login', 'Auth\AdminAuthController@login')->name('admin.login.post');

    Route::get('logout', 'Auth\AdminAuthController@logout')->name('admin.logout');

	Route::get('/', 'AdminController@dashboard')->name('dashboard');

	Route::get('dashboard', 'AdminController@dashboard')->name('dashboard');

	Route::get('listcustomers', 'AdminController@listCustomers')->name('listCustomers');

	Route::get('listspecialities', 'AdminController@listSpecialities')->name('listSpecialities');

	Route::get('listdoctors', 'AdminController@listDoctors')->name('listDoctors');

	Route::get('listproductcategory', 'AdminController@listProductCategory')->name('listProductCategory');

	Route::get('listarticlecategories', 'AdminController@listArticleCategories')->name('listArticleCategories');

	Route::get('listclinics', 'AdminController@listClinics')->name('listClinics');

	Route::get('listconsults', 'AdminController@listConsults')->name('listConsults');
	
	Route::get('settings', 'AdminController@getSettings')->name('getSettings');

	Route::get('addcustomers', 'AdminController@addCustomers')->name('addCustomers');

	Route::get('addspeciality', 'AdminController@addSpeciality')->name('addSpeciality');

	Route::get('addarticlecategory', 'AdminController@addArticleCategory')->name('addArticleCategory');

	Route::post('specialtySubmit', 'AdminController@specialtySubmit')->name('specialtySubmit');
	
	Route::post('articlesubmit', 'AdminController@articleSubmit')->name('articleSubmit');

	Route::get('doctorfees/{id}', 'AdminController@doctorFee')->name('doctorFee');

	//////////////////////////////////

	Route::get('listconsults', 'AdminController@listConsults')->name('listConsults');

	Route::get('/approvedoctor/{id}', 'AdminController@approveDoctor')->name('approveDoctor');

	Route::get('addproductcategory', 'AdminController@addProductCategory')->name('addProductCategory');

	Route::post('productCategorySubmit', 'AdminController@productCategorySubmit')->name('productCategorySubmit');

	Route::post('savegeneral', 'AdminController@saveGeneral')->name('saveGeneral');

	Route::post('specialitySubmit', 'AdminController@specialitySubmit')->name('specialitySubmit');
	
	Route::get('addclinic', 'AdminController@addClinic')->name('addClinic');

	Route::post('clinicsubmit', 'AdminController@clinicSubmit')->name('clinicSubmit');

////////////////////////////////////////

	Route::get('associateclinic/{id}', 'AdminController@associateClinic')->name('associateClinic');

	Route::post('associateclinicsubmit', 'AdminController@associateClinicSubmit')->name('associateClinicSubmit');

	Route::get('viewdocuments/{id}', 'AdminController@viewDocuments')->name('viewDocuments');

	Route::get('addeducation/{id}', 'AdminController@addEducation')->name('addEducation');

	Route::post('addeducationsubmit', 'AdminController@addEducationSubmit')->name('addEducationSubmit');
	
	Route::get('viewdoctordetails/{id}', 'AdminController@viewDoctorDetails')->name('viewDoctorDetails');

	Route::post('viewDoctorDetailsSubmit', 'AdminController@viewDoctorDetailsSubmit')->name('viewDoctorDetailsSubmit');

	///////////////////////////////////

	Route::post('associatespecialitysubmit', 'AdminController@associateSpecialitySubmit')->name('associateSpecialitySubmit');

	Route::get('associatespeciality/{id}', 'AdminController@associateSpeciality')->name('associateSpeciality');

	Route::get('editclinic/{id}', 'AdminController@editClinic')->name('editClinic');

	Route::post('cliniceditsubmit', 'AdminController@cliniceditsubmit')->name('cliniceditsubmit');

	Route::get('clinicDelete/{id}', 'AdminController@clinicDelete')->name('clinicDelete');
	
	Route::get('viewdoctordetails/{id}', 'AdminController@viewDoctorDetails')->name('viewDoctorDetails');

	Route::post('viewDoctorDetailsSubmit', 'AdminController@viewDoctorDetailsSubmit')->name('viewDoctorDetailsSubmit');

	Route::get('addtimeslots/{id}', 'AdminController@addTimeSlots')->name('addTimeSlots');

	Route::get('edittimeslot/{slotid}/{specid}', 'AdminController@editTimeSlot')->name('editTimeSlot');

	Route::post('timeslotsubmit', 'AdminController@timeSlotSubmit')->name('timeSlotSubmit');

	Route::get('editSpecialities/{id}', 'AdminController@editSpecialities')->name('editSpecialities');

	Route::post('specialityEditSubmit', 'AdminController@specialityEditSubmit')->name('specialityEditSubmit');

	Route::get('doctorpayment/{id}', 'AdminController@doctorPayment')->name('doctorPayment');

	Route::get('changePaymentStatus/{id}', 'AdminController@changePaymentStatus')->name('changePaymentStatus');


	});








Route::group(['prefix' => 'doctorapi'], function(){

//Docotr API Controller routed
//POST Methods
	Route::post('/register','DoctorApiController@register');
	Route::post('/login','DoctorApiController@login');
	Route::post('/setconsults','DoctorApiController@setConsults');
	Route::post('/setconsulttime','DoctorApiController@setConsultTime');
	Route::post('/updatelocation','DoctorApiController@updateDoctorLocation');
	Route::post('/deletearticle','DoctorApiController@deleteArticle');

	Route::post('/addarticle','DoctorApiController@addArticle');
	Route::post('/editarticle','DoctorApiController@editArticle');
	Route::post('/pushtest','DoctorApiController@pushTest');
	Route::post('/updateprofile','DoctorApiController@updateProfile');

	// Request process in provider
	Route::post('/serviceAccept', 'DoctorApiController@service_accept');

	Route::post('/serviceReject', 'DoctorApiController@service_reject');

	Route::post('/doctorStarted', 'DoctorApiController@doctorstarted');

	Route::post('/arrived', 'DoctorApiController@arrived');

	Route::post('/serviceStarted', 'DoctorApiController@servicestarted');

	Route::post('/serviceCompleted', 'DoctorApiController@servicecompleted');

	Route::post('/serviceCompletedOnline', 'DoctorApiController@serviceCompletedOnline');

	Route::post('/codPaidConfirmation', 'DoctorApiController@cod_paid_confirmation');

	Route::post('/rateUser', 'DoctorApiController@rate_user');

	Route::post('/cancelrequest', 'DoctorApiController@cancelrequest');

	Route::post('/history', 'DoctorApiController@history');

	Route::post('/incomingRequest', 'DoctorApiController@get_incoming_request');

	Route::post('/requestStatusCheck', 'DoctorApiController@request_status_check');

	Route::post('/sendslots', 'DoctorApiController@sendSlots');

	Route::post('/endbooking', 'DoctorApiController@endBooking');

	Route::post('/reportissue', 'DoctorApiController@reportIssue');

	Route::post('/generateotp', 'DoctorApiController@generateOtp');

	Route::post('/answerquestion', 'DoctorApiController@answerQuestion');

    Route::post('/uploaddocuments', 'DoctorApiController@uploadDocuments');

    Route::post('/changepassword', 'DoctorApiController@changePassword');

    Route::post('/logout', 'DoctorApiController@logout');

    Route::post('/checktoken', 'DoctorApiController@checkToken');

    Route::post('/sendmessagetouser', 'DoctorApiController@sendMessageToUser');

    Route::post('/cancelappointment','DoctorApiController@cancelAppointment');






    //SHOP APIS POST

    Route::post('/addshop', 'ShopDoctorApiController@addShop');
    Route::post('/addproduct', 'ShopDoctorApiController@addProduct');
    Route::post('/editshop', 'ShopDoctorApiController@editShop');
    Route::post('/editproduct', 'ShopDoctorApiController@editProduct');
    Route::post('/deleteproduct', 'ShopDoctorApiController@deleteProduct');






    //SHOP APIS GET

    Route::get('/getproductcategories', 'ShopDoctorApiController@getProductCategories');
    Route::get('/getmyproducts', 'ShopDoctorApiController@getMyProducts');
    Route::get('/getsingleproduct', 'ShopDoctorApiController@getSingleProduct');
    Route::get('/getshop', 'ShopDoctorApiController@getShop');
    Route::get('/getproductquestions', 'ShopDoctorApiController@getProductQuestions');







	Route::get('/getdoctorslots', 'DoctorApiController@getDoctorSlots');

	Route::get('/myschedule', 'DoctorApiController@mySchedule');

	Route::get('/getscheduledates', 'DoctorApiController@getScheduleDates');

	Route::get('/consulthistory', 'DoctorApiController@consultHistory');

	Route::get('/getquestion', 'DoctorApiController@getQuestion');

    Route::get('/viewanswers', 'DoctorApiController@viewAnswers');

    Route::get('/generateopentok', 'DoctorApiController@generateOpentok');

    Route::get('/getchathistory', 'DoctorApiController@getChatHistory');

    Route::get('/getpatientsfav', 'DoctorApiController@getPatientsFav');

    Route::get('/getmessagelist', 'DoctorApiController@getMessageList');

    Route::get('/chatconsulthistory', 'DoctorApiController@chatConsultHistory');

    Route::get('/phoneconsulthistory', 'DoctorApiController@phoneConsultHistory');

    Route::get('/videoconsulthistory', 'DoctorApiController@videoConsultHistory');

    Route::get('/getrequests', 'DoctorApiController@getRequests');

    












	// Availability

	Route::post('/scheduleAvailabilityTime' , 'DoctorApiController@schedule_availability_time');

	Route::get('/getAvailabilities' , 'DoctorApiController@get_availabilities');




	//GET Method
	Route::get('/getspeciality','DoctorApiController@getSpeciality');
	Route::get('/getconsults','DoctorApiController@getConsults');
	Route::get('/getconsulttime','DoctorApiController@getConsultTime');
	Route::get('/getmyarticles','DoctorApiController@getMyArticles');
	Route::get('/getarticlecategory','DoctorApiController@getArticleCategory');
	Route::get('/getslots','DoctorApiController@getSlots');





});

Route::post('/savechatmessage' , 'DoctorApiController@saveChatMessage');
Route::get('/getchatmessage' , 'DoctorApiController@getChatMessage');


Route::group(['prefix' => 'userapi'], function(){

	//User API Controller routes
	//POST Methods
	Route::post('/register','UserApiController@register');
	Route::post('/login','UserApiController@login');
	Route::post('/updateprofile','UserApiController@updateProfile');
	Route::post('/updatelocation','UserApiController@updateUserLocation');
	Route::post('/createrequest','UserApiController@createRequest');
	Route::post('/finishOnlineConsult','UserApiController@finishOnlineConsult');
	Route::post('/testpush','UserApiController@PushTest');
	Route::post('/rateDoctor', 'UserApiController@rate_doctor');
	Route::post('/requestStatusCheck', 'UserApiController@request_status_check');
	Route::post('/checkrequest', 'UserApiController@checkRequestAvailable');
	Route::post('/createRequestBooking', 'UserApiController@createRequestBooking');
	Route::post('/bookslots', 'UserApiController@bookSlots');
	Route::post('/addcard', 'UserApiController@userAddCard');
	Route::post('/testpayment', 'UserApiController@testPayment');
	Route::post('/selectcard', 'UserApiController@selectCard');
	Route::post('/deletecard', 'UserApiController@deleteCard');
	Route::post('/getbraintreetoken' , 'UserApiController@getBraintreeToken');
	Route::post('/reportissue', 'UserApiController@reportIssue');
	Route::post('/addbookmark', 'UserApiController@addBookmark');
	Route::post('/cancelrequest', 'UserApiController@cancelRequest');
	Route::post('/testtwilio', 'UserApiController@testTwilio');
	Route::post('/generateotp', 'UserApiController@generateOtp');
	Route::post('/askquestion', 'UserApiController@askQuestion');
    Route::post('/checktoken','UserApiController@checkToken');
    Route::post('/testemail','UserApiController@testEmail');
    Route::post('/changepassword','UserApiController@changePassword');
    Route::post('/cleardebt','UserApiController@clearDebt');
    Route::post('/replydoctormessage','UserApiController@replyDoctorMessage');
    Route::post('/bookslotsnew', 'UserApiController@bookSlotsNew');











	//GET Methods
	Route::get('/getspeciality','UserApiController@getSpecialities');
	Route::get('/getdoctorlistonline','UserApiController@getDoctorListOnline');
	Route::get('/getdoctorlistbooking','UserApiController@getDoctorListBooking');
	Route::get('/getdoctorondemand','UserApiController@getDoctorOnDemand');
	Route::get('/getnearbyonline','UserApiController@getNearbyOnline');
	Route::get('/getnearbybooking','UserApiController@getNearbyBooking');
	Route::get('/getslots','UserApiController@getSlots');
	Route::get('/test','UserApiController@test');
	Route::get('/getarticles','UserApiController@getArticles');
	Route::get('/getondearticle','UserApiController@getOneArticle');
	Route::get('/getAvailabilities' , 'UserApiController@get_availabilities');
	Route::get('/getavailableslots' , 'UserApiController@getAvailableSlots');
	Route::get('/myschedule' , 'UserApiController@mySchedule');
	Route::get('/getscheduledates' , 'UserApiController@getScheduleDates');
	Route::get('/getcards' , 'UserApiController@getCards');
	Route::get('/consulthistorychat', 'UserApiController@consultHistoryChat');
	Route::get('/consulthistoryphone', 'UserApiController@consultHistoryPhone');
	Route::get('/consulthistoryvideo', 'UserApiController@consultHistoryVideo');
	Route::get('/getarticlecategory', 'UserApiController@getArticleCategory');
	Route::get('/viewbookmark', 'UserApiController@viewBookmark');
	Route::get('/cronnotification', 'UserApiController@cronNotification');
	Route::get('/getdoctorbyspeciality', 'UserApiController@getDoctorbySpeciality');
	Route::get('/viewanswers', 'UserApiController@viewAnswers');
    Route::get('/generateopentok', 'UserApiController@generateOpentok');
    Route::get('/getdoctorondemandclinic', 'UserApiController@getDoctorOndemandClinic');
    Route::get('/getdoctoronlineclinic', 'UserApiController@getDoctorOnlineClinic');
    Route::get('/getdoctorbookingclinic', 'UserApiController@getDoctorBookingClinic');
    Route::get('/getchathistory', 'UserApiController@getChatHistory');
    Route::get('/getdoctormessages', 'UserApiController@getDoctorMessages');
    Route::get('/getnotified', 'UserApiController@getNotified');
    Route::get('/getrequests', 'UserApiController@getRequest');





    //E-commerce API s
    Route::get('/getproductcategories', 'ShopUserApiController@getProductCategories');
    Route::get('/getproducts', 'ShopUserApiController@getProducts');
    Route::get('/getsingleproduct', 'ShopUserApiController@getSingleProduct');
    Route::get('/getcart', 'ShopUserApiController@getCart');
    Route::post('/addcart', 'ShopUserApiController@addCart');
    Route::post('/addwishlist', 'ShopUserApiController@addWishlist');
    Route::get('/viewwishlist', 'ShopUserApiController@viewWishlist');
    Route::post('/removewishlist', 'ShopUserApiController@removeWishlist');
    Route::post('/removecart', 'ShopUserApiController@removeCart');
    Route::get('/orderdetails', 'ShopUserApiController@orderDetails');
    Route::get('/getaddress', 'ShopUserApiController@getAddress');
    Route::post('/addaddress', 'ShopUserApiController@addAddress');
    Route::post('/selectaddress', 'ShopUserApiController@selectAddress');
    Route::get('/getquestion', 'ShopUserApiController@getQuestion');
    Route::get('/orderdetails', 'ShopUserApiController@orderDetails');




});
Route::post('/uploadattachment', 'UserApiController@uploadAttachment');
