<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    protected $table = 'users';


    public function getRememberToken()
    {
        return null; // not supported
    }

    public function setRememberToken($value)
    {
        // not supported
    }


    //Relationship with other models
    public function requests()
    {
    	return $this->hasMany('App\Requests');
    }

    public function user_issues()
    {
    	return $this->hasMany('App\UserIssue');
    }

    public function payments()
    {
    	return $this->hasMany('App\Payment');
    }

    public function user_ratings()
    {
    	return $this->hasMany('App\UserRating');
    }

    public function doctor_ratings()
    {
    	return $this->hasMany('App\DoctorRating');
    }

    public function articleBookmarks()
    {
        return $this->hasMany('App\ArticleBookmark');
    }
}
