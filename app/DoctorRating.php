<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DoctorRating extends Model
{
    //


    //Relationship to other models
    public function doctors()
    {
    	return $this->belongsTo('App\Doctor');
    }


    public function users()
    {
    	return $this->belongsTo('App\User');
    }
    

    public function requests()
    {
    	return $this->belongsTo('App\Requests');
    }
}
