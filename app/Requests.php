<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Requests extends Model
{
    //

    //Relationship to other models

	 public function user_ratings()
    {
    	return $this->hasOne('App\UserRating');
    }


    public function doctor_ratings()
    {
    	return $this->hasOne('App\DoctorRating');
    }
    
    public function doctors()
    {
    	return $this->belongsTo('App\Doctor');
    }


    public function users()
    {
    	return $this->belongsTo('App\User');
    }


   
}
