<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $table = 'articles';


    //Relationship to other models
    public function doctors()
    {
    	return $this->belongsTo('App\Doctor');
    }

    public function articleBookmarks()
    {
    	return $this->hasMany('App\ArticleBookmark');
    }
}
