<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DoctorIssue extends Model
{
    //


    //Relationship to other models
    public function doctors()
    {
    	return $this->belongsTo('App\Doctor');
    }
}
