<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DoctorFee extends Model
{
    protected $table = 'doctor_fees';


    //Relationship to other models
    public function doctors()
    {
    	return $this->belongsTo('App\Doctor');
    }
}
