<?php

namespace App\Jobs;

use App\DoctorFee;
use App\Jobs\Job;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\User;
use App\Requests;
use App\Doctor;

class SendUserInvoiceEmail extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $user;
    protected $doctor;
    protected $requests;
    protected $doctorFee;
    protected $distanceCost;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user, Doctor $doctor, Requests $requests,DoctorFee $doctorFee,$distanceCost)
    {
        $this->user = $user;
        $this->doctor = $doctor;
        $this->requests = $requests;
        $this->doctorFee = $doctorFee;
        $this->distanceCost = $distanceCost;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Mail $mail)
    {
        Mail::send('emails.invoice', ['user'=>$this->user,'doctor'=>$this->doctor,'requests'=>$this->requests,'doctorFee'=>$this->doctorFee,'distanceCost'=>$this->distanceCost], function ($message) {
            $message->from('message@ghealth.com','gHealth');
            $message->to($this->user->email)->subject('Invoice');
            $message->to($this->doctor->email)->subject('Invoice');

        });
    }
}
