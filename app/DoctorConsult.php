<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DoctorConsult extends Model
{
    protected $table = 'doctor_consults';

    //Relationship to other models
    public function doctors()
    {
    	return $this->belongsTo('App\Doctor');
    }
}
