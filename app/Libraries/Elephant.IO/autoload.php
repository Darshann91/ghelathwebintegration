<?php

$vendorDir = dirname(dirname(__FILE__));
$loader = new \Composer\Autoload\ClassLoader();
$loader->setPsr4('ElephantIO\\', $vendorDir . '/Elephant.IO/src');
$loader->register(true);
