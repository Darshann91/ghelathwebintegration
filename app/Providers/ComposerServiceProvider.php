<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
   
    public function boot()
    { 
        view()->composer('*', 'GlobalComposer');
    }

  
    public function register()
    {
        $this->app->singleton('GlobalComposer', function($app){
            return $this->app->make('App\Http\ViewComposers\GlobalComposer');
        });
    }
}