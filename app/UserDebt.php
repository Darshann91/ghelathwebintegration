<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserDebt extends Model
{
    //

    //Relationship to other models
    public function users()
    {
    	return $this->belongsTo('App\User');
    }
}
