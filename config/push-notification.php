<?php

return array(

    'appNameIOSPatient'     => array(
        'environment' =>'production',
        'certificate' =>public_path().'/ios/patient.pem',
        'passPhrase'  =>'',
        'service'     =>'apns'
    ),

    'appNameIOSDoctor'     => array(
        'environment' =>'production',
        'certificate' =>public_path().'/ios/doctor.pem',
        'passPhrase'  =>'',
        'service'     =>'apns'
    ),

    'appNameAndroid' => array(
        'environment' =>'production',
        'apiKey'      =>'AIzaSyA8UeiRsalspSdBYP4jZUL2nuoPfCAOF00',
        'service'     =>'gcm'
    )

);