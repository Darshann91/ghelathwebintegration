<?php

return [
	
	"socket_server_port"                => '3000',
	"socket_server_address"             => 'http://localhost',
	"server_internal_communication_key" => env('SERVER_INTERNAL_COMMUNICATION_KEY', '123456')

];