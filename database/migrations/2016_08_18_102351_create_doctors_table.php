<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDoctorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doctors', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('phone');
            $table->string('email');
            $table->string('password');
            $table->string('dob');
            $table->string('picture');
            $table->string('title');
            $table->string('gender');
            $table->string('currency');
            $table->string('c_street');
            $table->string('c_city');
            $table->string('c_state');
            $table->string('c_postal');
            $table->string('c_country');
            $table->string('med_number');
            $table->string('device_token');
            $table->string('device_type');
            $table->string('session_token');
            $table->string('session_token_expiry');
            $table->enum('login_by', array('manual', 'facebook', 'google','twitter'));
            $table->string('social_unique_id');
            $table->float('latitude');
            $table->float('longitude');
            $table->string('timezone');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('doctors');
    }
}
