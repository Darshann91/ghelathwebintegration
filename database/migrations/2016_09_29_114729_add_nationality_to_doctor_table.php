<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNationalityToDoctorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('doctors', function (Blueprint $table) {
            $table->string('nationality');
            $table->string('country_code');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('doctors', function (Blueprint $table) {
            $table->dropColumn('nationality');
            $table->dropColumn('country_code');
            
        });
    }
}
