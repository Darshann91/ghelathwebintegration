<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDeliveryFreeToProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->boolean('delivery_free')->default(0);
            $table->float('product_latitude');
            $table->float('product_longitude');
            $table->string('product_address');
            $table->float('delivery_cost')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('delivery_free')->default(0);
            $table->dropColumn('product_latitude');
            $table->dropColumn('product_longitude');
            $table->dropColumn('product_address');
            $table->dropColumn('delivery_cost');
        });
    }
}
