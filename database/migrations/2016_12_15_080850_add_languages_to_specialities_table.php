<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLanguagesToSpecialitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('specialities', function(Blueprint $table)
        {
            $table->string('speciality_chinese');
            $table->string('speciality_thai');
            $table->string('speciality_indonesian');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('specialities', function(Blueprint $table)
        {
            $table->dropColumn('speciality_chinese');
            $table->dropColumn('speciality_thai');
            $table->dropColumn('speciality_indonesian');

        });
    }
}
