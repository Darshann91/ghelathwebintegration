<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddConsultPriceToDoctorFee extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('doctor_fees', function(Blueprint $table)
        {
            $table->float('minutes_consult_fee');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('doctor_fees', function(Blueprint $table)
        {
            $table->dropColumn('minutes_consult_fee');

        });
    }
}
