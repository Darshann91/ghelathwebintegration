<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddShippingAddressToUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('shipping_address');
            $table->string('billing_address');
            $table->double('shipping_latitude',15,8);
            $table->double('shipping_longitude',15,8);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('shipping_address');
            $table->dropColumn('billing_address');
            $table->dropColumn('shipping_latitude');
            $table->dropColumn('shipping_longitude');
        });
    }
}
