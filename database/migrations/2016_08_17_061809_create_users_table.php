<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('phone');
            $table->string('email');
            $table->string('password');
            $table->string('dob');
            $table->string('gender');
            $table->string('picture');
            $table->string('title');
            $table->string('b_street');
            $table->string('b_city');
            $table->string('b_state');
            $table->string('b_postal');
            $table->string('b_country');
            $table->string('s_street');
            $table->string('s_city');
            $table->string('s_state');
            $table->string('s_postal');
            $table->string('s_country');
            $table->string('device_token');
            $table->string('device_type');
            $table->string('session_token');
            $table->string('session_token_expiry');
            $table->enum('login_by', array('manual', 'facebook', 'google','twitter'));
            $table->string('social_unique_id');
            $table->float('latitude');
            $table->float('longitude');
            $table->string('timezone');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
