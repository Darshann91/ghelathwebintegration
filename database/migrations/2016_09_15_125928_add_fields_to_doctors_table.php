<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToDoctorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('doctors', function (Blueprint $table)
        {
            $table->string('c_name');
            $table->string('experience');
            $table->string('univ1');
            $table->string('degree1');
            $table->string('year1');
            $table->string('univ2');
            $table->string('degree2');
            $table->string('year2');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('doctors', function (Blueprint $table)
        {
            $table->dropColumn('c_name');
            $table->dropColumn('experience');
            $table->dropColumn('univ1');
            $table->dropColumn('degree1');
            $table->dropColumn('year1');
            $table->dropColumn('univ2');
            $table->dropColumn('degree2');
            $table->dropColumn('year2');
        });
    }
}
