<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('products', function (Blueprint $table) {
          $table->increments('id');
          $table->unsignedInteger('doctor_id');
          $table->unsignedInteger('shop_id');
          $table->unsignedInteger('product_category');

          $table->string('product_name');
          $table->text('product_description');
          $table->string('currency');
          $table->float('price');
          $table->float('price2');
          $table->string('status');
          $table->string('pic1');
          $table->string('pic2');
          $table->string('pic3');
          $table->string('tag');
          $table->integer('quantity');

          $table->foreign('doctor_id')->references('id')->on('doctors')->onDelete('cascade');

          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('products');
    }
}
