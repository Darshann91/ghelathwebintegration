<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddClinicPicsToDoctorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('doctors', function (Blueprint $table) {
            $table->string('c_pic1');
            $table->string('c_pic2');
            $table->string('c_pic3');
            $table->string('speciality_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('doctors', function (Blueprint $table) {
            $table->dropColumn('c_pic1');
            $table->dropColumn('c_pic2');
            $table->dropColumn('c_pic3');
            $table->dropColumn('speciality_id');
            
        });
    }
}
