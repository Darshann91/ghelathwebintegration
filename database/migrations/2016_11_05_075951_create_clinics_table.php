<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClinicsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clinics', function (Blueprint $table) {
            $table->increments('id');
            $table->string('clinic_name');
            $table->string('c_image1');
            $table->string('c_image2');
            $table->string('c_image3');
            $table->string('c_address');
            $table->string('c_phone');
            $table->string('c_fax');
            $table->double('c_latitude',15,8);
            $table->double('c_longitude',15,8);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('clinics');
    }
}
