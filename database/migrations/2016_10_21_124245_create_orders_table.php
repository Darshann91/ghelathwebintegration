<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('orders', function (Blueprint $table) {
          $table->increments('id');
          $table->unsignedInteger('user_id');
          $table->unsignedInteger('product_id');
          $table->unsignedInteger('doctor_id');
          $table->dateTime('order_date');
          $table->integer('quantity');
          $table->integer('payment_type');
          $table->float('total_price');
          $table->integer('order_status');
          $table->timestamps();

          $table->foreign('doctor_id')->references('id')->on('doctors')->onDelete('cascade');
          $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('orders');
    }
}
