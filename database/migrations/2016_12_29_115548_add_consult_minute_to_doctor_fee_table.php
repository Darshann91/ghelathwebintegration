<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddConsultMinuteToDoctorFeeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('doctor_fees', function(Blueprint $table)
        {
            $table->integer('consult_minute')->default(1);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('doctor_fees', function(Blueprint $table)
        {
            $table->dropColumn('consult_minute');

        });
    }
}
