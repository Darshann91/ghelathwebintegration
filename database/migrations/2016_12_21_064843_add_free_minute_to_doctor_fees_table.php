<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFreeMinuteToDoctorFeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('doctor_fees', function(Blueprint $table)
        {
            $table->integer('free_minute')->default(0);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('doctor_fees', function(Blueprint $table)
        {
            $table->dropColumn('free_minute');

        });
    }
}
