<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('shops', function (Blueprint $table) {
          $table->increments('id');
          $table->unsignedInteger('doctor_id');
          $table->string('shop_name');
          $table->string('shop_language');
          $table->string('shop_currency');
          $table->string('shop_country');
          $table->integer('describe');
          $table->integer('payments');
          $table->timestamps();

          $table->foreign('doctor_id')->references('id')->on('doctors')->onDelete('cascade');

      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('shops');
    }
}
