<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsAvailableToDoctors extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('doctors', function(Blueprint $table)
        {
            $table->boolean('is_available')->default(0);
            $table->boolean('is_approved')->default(0);
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('doctors', function(Blueprint $table)
        {
            $table->dropColumn('is_available');
            $table->dropColumn('is_approved');
           
        });
    }
}
