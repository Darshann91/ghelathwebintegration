<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecurringSlotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('recurring_slots', function (Blueprint $table) {
          $table->increments('id');
          $table->unsignedInteger('doctor_id');
          $table->string('exp_date');
          $table->string('cron_date');
          $table->integer('days');
          $table->integer('status');
          $table->string('slots_id');
          $table->timestamps();

          $table->foreign('doctor_id')->references('id')->on('doctors')->onDelete('cascade');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('recurring_slots');
    }
}
