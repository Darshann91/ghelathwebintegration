<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddShippingFieldsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function(Blueprint $table)
        {
            $table->string('sblock_no');
            $table->string('slandmark');
            $table->string('sphone_no');
            $table->string('bblock_no');
            $table->string('blandmark');
            $table->string('bphone_no');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function(Blueprint $table)
        {
            $table->dropColumn('sblock_no');
            $table->dropColumn('slandmark');
            $table->dropColumn('sphone_no');
            $table->dropColumn('bblock_no');
            $table->dropColumn('blandmark');
            $table->dropColumn('bphone_no');
        });
    }
}
