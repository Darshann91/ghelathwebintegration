
var homeApp = angular.module('homeApp', ['ngRoute'], function($interpolateProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
});


homeApp.filter('chat_timestamp_filter', function() {
    
    return function(timeStamp) {
        var date = new Date(timeStamp);
    	return date.toLocaleString('en-US', { 
    		hour: 'numeric',
    		minute:'numeric', 
    		hour12: true, 
    		year:'numeric', 
    		month:'numeric',
    		day:'numeric' 
    	});
    };
});


homeApp.factory('socket', function ($rootScope) {
  
  var socket = io.connect(socket_host+":"+socket_port);

  return {
    on: function (eventName, callback) {
      socket.on(eventName, function () {  
        var args = arguments;
        $rootScope.$apply(function () {
          callback.apply(socket, args);
        });
      });
    },
    emit: function (eventName, data, callback) {
      socket.emit(eventName, data, function () {
        var args = arguments;
        $rootScope.$apply(function () {
          if (callback) {
            callback.apply(socket, args);
          }
        });
      })
    },
    removeAllListeners: function (eventName, callback) {
		socket.removeAllListeners(eventName, function() {
			var args = arguments;
			$rootScope.$apply(function () {
				if (callback) {
					callback.apply(socket, args);
				}
			});
		}); 
	}
  };
});


homeApp.directive('rating', function ($parse) {
    return {
        restrict: 'A',
        scope: {
		    rating : "=model"
		},
        link: function ($scope, element, attrs) {

        	$(element).rateYo({
    			rating: 0,
    			fullStar: true
  			});

  			$(element).rateYo().on("rateyo.change", function (e, data) {
  				$scope.$apply(function(){
  					$scope.rating.value = data.rating;
  				});
            });
        }
    };
});



homeApp.directive('jqdatepicker', function ($parse) {
    return {
        restrict: 'A',
        require: 'ngModel',
        scope: {
		    selectFinish:"&"
		},
         link: function (scope, element, attrs, ngModel) {
         	var homeScope = angular.element($("#homeDiv")).scope();
            element.datepicker({
                dateFormat: 'yy-mm-dd',
                onSelect: function (date) {
    				
    				ngModel.$setViewValue(date);
    				ngModel.$render();
    				scope.$apply(function(){
    					scope.selectFinish();
    				});
                },
                beforeShowDay: function( date ) {
                	
                	if(attrs.dateHighlight !== 'true') return [true, '', ''];
              
                	if(homeScope.selectedSlotDateExists(getDateString(date))) {
                		return [true, "event", 'Tooltip text'];
                	} else {
                		return [true, '', ''];
                	}
			    }
            });
        }
    };
});



function getDateString(date)
{
	var month = date.getMonth();
	month = month < 9 ? "0" + (month+1) : month+1;

	var day = date.getDate();
	day = day < 10 ? "0" + day : day;
	return date.getFullYear() + "-" + month + "-" + day;
}


homeApp.config(function($routeProvider) {

    $routeProvider
    .when("/", {
		templateUrl : appointments_template_url	
    })
    .when("/appointments", {
		templateUrl : appointments_template_url
    });

});



homeApp.controller('homeCtrl', function($rootScope, $scope, $http, $interval, socket, $timeout) {

	$scope.currency = currency;	
	$scope.self_picture = doctor_picture;

	$rootScope.selectedAppointmentTab = 'up_coming';

	$scope.appointmentTabClicked = function(tab)
	{
		$rootScope.selectedAppointmentTab = (tab != undefined) ? tab : $rootScope.selectedAppointmentTab;
		console.log($rootScope.selectedAppointmentTab);
		$('.nav-tabs a[data-target="#'+$rootScope.selectedAppointmentTab+'"]').tab('show');


		if($rootScope.selectedAppointmentTab == 'up_coming') {
			$scope.getSchedulesByDate(today);
			$scope.getSchedulesByDate(tomorrow, false);
		} else if($rootScope.selectedAppointmentTab == 'my_hour') {
			$scope.getSlots($scope.appointment.slotDate);
		}

	}


	$scope.mormingSlotsFilter = function(slot)
	{
		return (slot.type == 'm');
	}

	$scope.afternoonSlotsFilter = function(slot)
	{
		return (slot.type == 'a');
	}

	$scope.eveningSlotsFilter = function(slot)
	{
		return (slot.type == 'e');
	}	





	$scope.isSlotSaved = function(slotID, returnIndex = false)
	{	
		if(returnIndex) {
			return $scope.savedSlotIDs.indexOf(slotID)
		} else {
			return $scope.savedSlotIDs.indexOf(slotID) < 0 ? false : true;
		}
		
	}



	$scope.slots = [];
	$scope.selectedSlotDates = [];
	$scope.savedSlotIDs = [];


	$scope.appointment = {
		slotDate : today,
		recurringSlotDate : today
	};

	$scope.saveSlots = function()
	{
		$("#recurring-slots-modal").modal('hide');

		showLoaderMessage(true, 'Saving slots. wait for a while ...');

		if(!$scope.selectedSlotDates.length) {
			toastr.error('No Slot dates are selected. Selecte mininum one date');
			showLoaderMessage(false);
			return;
		}
		var dates = [];
		$scope.selectedSlotDates.forEach(function(item){
			dates.push(item+" 00:00:00");
		});
	

		if(dates.length == 1) {
			postData = {
				slots : $scope.savedSlotIDs.join(','),
				date  : dates.join(',')
			}
		} else {
			postData = {
				slots : $scope.savedSlotIDs.join(','),
				date  : dates.join(','),
				recurring_slots_status : 1
			}
		}


		
		$http({
			url    : save_slots,
			method : "POST",
			data   : postData
    	}).then(function(response) {
 			
 			showLoaderMessage(false);

 			if(!response.data.success) {
 				toastr.error('Failed to save slots. Try again.');
 			} else {
 				toastr.success('Slots saved successfully.');
 			}
 			

    	},function(response) {
    		showLoaderMessage(false);
    		toastr.error('Failed to save slots. Try again.');
    	});
	}


	$scope.selectedSlotDateExists = function(date)
	{
		return $scope.selectedSlotDates.indexOf(date) < 0 ? false : true;
	}


	$scope.slotsSelectionDone = function()
	{
		$("#recurringDatepicker").datepicker("refresh");
		$("#recurring-slots-modal").modal('show');
	}


	$scope.recurringSlotDateSelected = function()
	{
		var index = $scope.selectedSlotDates.indexOf($scope.appointment.recurringSlotDate);
		if(index < 0) {
			$scope.selectedSlotDates.push($scope.appointment.recurringSlotDate);
			//$scope.highlightDate($scope.appointment.recurringSlotDate);
		} else {
			$scope.selectedSlotDates.splice(index, 1);
			//$scope.highlightDate($scope.appointment.recurringSlotDate, false);
		}
		console.log($scope.selectedSlotDates, $scope.savedSlotIDs);
	}




	$scope.appointmentDateChanged = function()
	{
		$scope.getSlots($scope.appointment.slotDate);
	}


	$scope.isAllMorningSlotsSelected = false;
	$scope.allMorningSlotsSelcted = function()
	{

		var filtered = $scope.slots.filter(function(slot){
			return slot.type == "m";
		});

		$scope.isAllMorningSlotsSelected = true;
		for(var i = 0; i < filtered.length; i++) {
				
			if(!$scope.isSlotSaved(filtered[i].id)) {
				$scope.isAllMorningSlotsSelected = false;
				break;
			}
		}
	
	}


	$scope.isAllAfternoonSlotsSelected = false;
	$scope.allAfternoonSlotsSelcted = function()
	{

		var filtered = $scope.slots.filter(function(slot){
			return slot.type == "a";
		});

		$scope.isAllAfternoonSlotsSelected = true;
		for(var i = 0; i < filtered.length; i++) {
				
			if(!$scope.isSlotSaved(filtered[i].id)) {
				$scope.isAllAfternoonSlotsSelected = false;
				break;
			}
		}
	
	}


	$scope.isAllEveningSlotsSelected = false;
	$scope.allEveningSlotsSelcted = function()
	{

		var filtered = $scope.slots.filter(function(slot){
			return slot.type == "e";
		});

		$scope.isAllEveningSlotsSelected = true;
		for(var i = 0; i < filtered.length; i++) {
				
			if(!$scope.isSlotSaved(filtered[i].id)) {
				$scope.isAllEveningSlotsSelected = false;
				break;
			}
		}
		console.log($scope.isAllEveningSlotsSelected);
	}


	$scope.selctAllSlots = function(type, slots)
	{
		slots.forEach(function(item){
			if(!$scope.isSlotSaved(item.id)) {
				$scope.savedSlotIDs.push(item.id);
			}
		});

		if(type == 'm') {
			$scope.allMorningSlotsSelcted();
		} else if(type == 'a'){
			$scope.allAfternoonSlotsSelcted();
		} else if(type == 'e'){
			$scope.allEveningSlotsSelcted();
		}
	}


	$scope.unSelctAllSlots = function(type, slots)
	{
		slots.forEach(function(item){

			index = $scope.isSlotSaved(item.id, true);
			if(index > -1) {
				$scope.savedSlotIDs.splice(index, 1);
			}
		});
		if(type == 'm') {
			$scope.allMorningSlotsSelcted();
		} else if(type == 'a'){
			$scope.allAfternoonSlotsSelcted();
		} else if(type == 'e'){
			$scope.allEveningSlotsSelcted();
		}
	}




	$scope.slotTimeSelected = function(type, id)
	{
		var index = $scope.isSlotSaved(id, true);

		if(index < 0) {
			$scope.savedSlotIDs.push(id);
		} else {
			$scope.savedSlotIDs.splice(index, 1);
		}
		
		if(type == 'm') {
			$scope.allMorningSlotsSelcted();
		} else if(type == 'a'){
			$scope.allAfternoonSlotsSelcted();
		} else if(type == 'e'){
			$scope.allEveningSlotsSelcted();
		}		
	}


	$scope.getSavedSlots = function(date)
	{
		$http({
			url    : get_saved_slots_by_date+"?date="+date,
			method : "GET"
    	}).then(function(response) {
 				
 			if(response.data.success) {
 				$scope.savedSlotIDs = [];
 				response.data.slots.forEach(function(item){
 					$scope.savedSlotIDs.push(item.id);
 				});

 				$scope.slots = $scope.slots_temp;
 				$scope.allMorningSlotsSelcted();
				$scope.allAfternoonSlotsSelcted();
				$scope.allEveningSlotsSelcted();
 				console.log($scope.slots, $scope.savedSlotIDs);
 			}

    	},function(response) {});
	}
	
	
	$scope.getSlots = function(date)
	{
		$scope.slots_temp = [];
		$scope.selectedSlotDates = [];

		$http({
			url    : get_slots_url,
			method : "GET"
    	}).then(function(response) {
 				
 			if(response.data.success) {
 				$scope.slots_temp = response.data.slots;
 				$scope.selectedSlotDates.push(date);
 				$scope.getSavedSlots(date);
 			}

    	},function(response) {});
	}





	$scope.consult = {};
	$scope.setConsultStatus = function()
	{
		var data = {
			doc_ondemand  : $scope.consult.doc_ondemand_status,
			chat_consult  : $scope.consult.chat_consult_status,
			phone_consult : $scope.consult.phone_consult_status,
			video_consult : $scope.consult.video_consult_status
		};

		/*showLoaderMessage(true, 'Saving your consult status ...');*/
		$http({
			url    : set_consult_status_url,
			method : "POST",
			data   : data
    	}).then(function(response) {
 				
 			if(response.data.success) {

 				$scope.consult.doc_ondemand_status = response.data.doc_ondemand;
 				$scope.consult.chat_consult_status = response.data.chat_consult;
 				$scope.consult.phone_consult_status = response.data.phone_consult;
 				$scope.consult.video_consult_status = response.data.video_consult;

 			} else {
 				toastr.error('Faied to set your consult status');
 			}

    	},function(response) { 
            toastr.error('Faied to set your consult status');
    	});


	}



	$scope.todaysSchedules = [];
	$scope.tomorrowSchedules = [];
	$scope.scheduleFetchCount = 0;
	$scope.getSchedulesByDate = function(date, today = true)
	{
		if(today) {
			showLoaderMessage(true, 'Fetching upcoming bookings ...');
		}

		$http({
        	url: get_schedules_by_date_url+"?date="+date,
        	method: "GET"
    	}).then(function(response) {
    		
    		if(response.data.success){
    			if(today) {
    				$scope.todaysSchedules = response.data.data;
    			} else {
    				$scope.tomorrowSchedules = response.data.data;
    			}
    		}

    		if(!today) {
				showLoaderMessage(false);
			} 

    	}, function(response) { 
            showLoaderMessage(false);
    	});
	}

	$scope.getSchedulesByDate(today);
	$scope.getSchedulesByDate(tomorrow, false);






	$scope.rejectService = function()
	{
		
		$http({
			url    : reject_service,
			method : "POST",
			data   : {
				request_id : $scope.currentRequestData.request_id
			}
    	}).then(function(response) {
			console.log(response.data);
			if(response.data.succcess) {
				$("user-card").css('display', 'none');
				$scope.stopAudio();
			}
    	},function(response) {});
	}



	$scope.messages = [];
	$scope.messageText = "";
	$scope.sendMessageText = function()
	{
		if($scope.messageText == "") {
			return;
		}

		socket.emit('online_chat_message', {
			message : $scope.messageText, 
			message_type : 'TEXT'
		});
		$scope.messageText = "";
	}


	$scope.isTyping = false;
	$scope.emitTyping = function(){

        if (!$scope.isTyping) {
            $scope.isTyping = true;
            socket.emit('online_chat_start_typing', {});
        }
        
        startTypingTime = (new Date()).getTime();

        $timeout(function(){

            var currTime = (new Date()).getTime();
            var timeDiff    = currTime - startTypingTime;
            
            if (timeDiff >= 500 && $scope.isTyping) {
                socket.emit('online_chat_stop_typing', {});
                $scope.isTyping = false;
            }

        }, 1000);
    
    }



    socket.on('online_chat_consult', function(data){});


					
	socket.on('user_joined_chat', function(data){


		console.log('user joined');


		$scope.hour = 0;
	    $scope.min = 0;
	    $scope.sec = 0;


		$scope.timer = $interval(function(){
				$scope.timerSec++;

				d = Number($scope.timerSec);
		    $scope.hour = Math.floor(d / 3600);
		    $scope.min = Math.floor(d % 3600 / 60);
		    $scope.sec = Math.floor(d % 3600 % 60);

		    socket.emit('timer_tick', {timer : $scope.timerSec});
		    console.log($scope.timerSec);

			}, 1000);

			$("#chat-modal").modal('show');

			$scope.messages = [];
		
		
	});


	socket.on('online_chat_start_typing', function(data){
		$scope.chat_notif_popup_text = "Patient is typing ...";
		$scope.chat_notif_popup_fa_icon = 'fa fa-keyboard-o';
		$(".chat-typing").slideDown();
	});

	socket.on('online_chat_stop_typing', function(data){
		$(".chat-typing").slideUp();
	});


	socket.on("online_chat_message", function(data){
		$scope.messages.push(data);
		$timeout(function () {
	        $("#online_message_container").scrollTop($("#online_message_container")[0].scrollHeight);
	    }, 100);
		
	});




	$scope.acceptService = function()
	{
		$("user-card").css('display', 'none');
		$http({
			url    : accept_service,
			method : "POST",
			data   : {
				request_id : $scope.currentRequestData.request_id
			}
    	}).then(function(response) {
			
			if(response.data.success) {
				
				$scope.stopAudio();
				$scope.poll.stopPoll();

				if($scope.currentRequestData.request_type == 1) {


					socket.emit("online_chat_consult", {
						request_id : $scope.currentRequestData.request_id,
						type : 'doctor'
					});


				} else {
					setTimeout(function(){
						$scope.generateOpentokSession();
    			}, 4000);
					
				}
			}
			
			
    	},function(response) {});
	}



	
	$scope.openUploadFileWindow = function()
	{
		$timeout(function(){
			angular.element(document.querySelector('#chat_upload_file')).click();
		},10);
		
	}


	$scope.validateFileTypes = function(type)
	{
		if(type !== 'image/png'
			&& type !== 'image/jpg'
			&& type !== 'image/jpeg'
			&& type !== 'image/JPG'
			&& type !== 'image/JPEG'
			&& type !== 'image/bmp'
			&& type !== 'image/gif') {

			return false;
		} 

		return true;
	}


	$scope.uploadAttachment = function(files)
	{

		if(!files.length) {
			return;
		}

		if(!$scope.validateFileTypes(files[0].type)) {
			toastr.error('Image types must be PNG, JPEG, BMP, GIF');
			return;
		}


		$scope.chat_notif_popup_text = "Uploading file "+files[0].name+" ...";
		$scope.chat_notif_popup_fa_icon = 'fa fa-upload';
		$(".chat-typing").slideDown();

		var formData = new FormData();
		formData.append('image', files[0]);
		formData.append('request_id', $scope.currentRequestData.request_id);

		$http({
			url    : upload_image_url,
			method : "POST",
			data   : formData,
			headers: {'Content-Type': undefined},
            transformRequest: angular.identity,
    	}).then(function(response) {
    		
    		if(response.data.success) {

    			socket.emit('online_chat_message', {
					message : response.data.image, 
					message_type : 'IMAGE'
				});

    		} else {

    			toastr.error('failed to upload');
    		}
    		$(".chat-typing").slideUp();
    	}, function(response) {
    		$(".chat-typing").slideUp();
    	});

	

	}






	$scope.opentokSession = "";
	$scope.opentokToken = "";
	$scope.opentokKey = openTokKey;
	$scope.generateOpentokSession = function()
	{

		$http({
        	url: opentok_generate_session+"?request_id="+$scope.currentRequestData.request_id,
        	method: "GET"
    	}).then(function(response) {
    		console.log(response.data);

    		if(response.data.success) {
    			$scope.opentokSession =  response.data.session_id;
    			$scope.opentokToken =  response.data.token;
    			$scope.openTokConnection();
    			
    			
    		} else {
    			//$scope.poll.stopPoll();
    			$scope.consult.phone_consult_status = 1;
    			$scope.setConsultStatus();
			}

    	}, function(response) {});
	}


	$scope.timerSec = 0;
	$scope.hour = 0;
	$scope.min = 0;
	$scope.sec = 0;
	$scope.timer = null;
	$scope.session = null;
	$scope.openTokConnection = function()
	{
		$scope.session = OT.initSession($scope.opentokKey, $scope.opentokSession);


		$scope.session.on('streamCreated', function(event) {
				console.log('stream created');
				$scope.session.subscribe(event.stream, 'opentok-subscriber', {
				    insertMode: 'append',
				    width: '100%',
				    height: '100%',
				    subscribeToVideo:$scope.isVideo
		  		});


		});



		var pubOption = {
	  		insertMode: 'append',
	  		width: '100%',
	  		height: '100%'
		};

		if(!$scope.isVideo) {
			pubOption.videoSource = null;
		}


		$scope.session.connect($scope.opentokToken, function(error) {

			// If the connection is successful, initialize a publisher and publish to the session
			if (!error) {

				$("#video-audio-modal").modal('show');

				if(typeof publisher !== 'undefined') {
					publisher.destroy();
				}

				publisher = OT.initPublisher('opentok-publisher', pubOption);

		    	
	    		$scope.timer = $interval(function(){
					$scope.timerSec++;

					d = Number($scope.timerSec);
				    $scope.hour = Math.floor(d / 3600);
				    $scope.min = Math.floor(d % 3600 / 60);
				    $scope.sec = Math.floor(d % 3600 % 60);

					$scope.session.signal({data:$scope.timerSec, type:'timer'}, function(error){});
					console.log($scope.min, $scope.sec);
				}, 1000);

	    		return $scope.session.publish(publisher);


	  		} else {
	  			
			    if (error.name === "OT_NOT_CONNECTED") {
			      console.log("You are not connected to the internet. Check your network connection.");
			    }
	    		console.log('There was an error connecting to the session:', error.code, error.message);
	  		}
		});



	}




	$scope.completeConsult = function()
	{
		$("#video-audio-modal").modal('hide');
		$("#chat-modal").modal('hide');
		showLoaderMessage(true, 'Wait... Completing consulatation');
		$interval.cancel($scope.timer);


		/*socket.removeAllListeners('online_chat_consult');
		socket.removeAllListeners('user_joined_chat');
		socket.removeAllListeners('online_chat_message');
		socket.removeAllListeners('timer_tick');
		socket.removeAllListeners('online_chat_start_typing');
		socket.removeAllListeners('online_chat_stop_typing');*/


		if($scope.isFullscreen) {
 			$scope.toggleFullScreen();
 		}

		var hms = $scope.secondsToHms($scope.timerSec);

		$scope.timerSec = 0;
		$scope.timer = null;
		if($scope.session) {
			$scope.session.disconnect();
		}
		console.log(hms);
		$http({
        	url: serviceOnlineCompleted,
        	method: "POST",
        	data : {
        		time : hms,
        		request_id : $scope.currentRequestData.request_id
        	}
    	}).then(function(response) {
    		$scope.invoice = response.data.invoice[0];
    		$("#invoice").modal('show');
    		$scope.poll.startPoll();
    		showLoaderMessage(false);
    		console.log(response.data);
    		
    	}, function(response) {showLoaderMessage(false);});
	}

	$scope.secondsToHms = function(d){
	    d = Number(d);
	    var h = Math.floor(d / 3600);
	    var m = Math.floor(d % 3600 / 60);
	    var s = Math.floor(d % 3600 % 60);
	    return h+":"+m+":"+s;
	}


	$scope.closeInvoice = function()
    {
    	$("#invoice").modal('hide');
    	$("#rating-modal").modal('show');
    }

    $scope.rating = {};
    $scope.rating.value = 0;
    $scope.rating.comment = "";

    $scope.giveRating = function()
	{

		if($scope.rating.comment === "") {
			toastr.warning('Type your comment first');
			return;
		}


		showLoaderMessage(true, "Rating doctor ..");
		$http({
        	url: rate_user,
        	method: "POST",
        	data : {
        		request_id : $scope.currentRequestData.request_id,
        		rating : $scope.rating.value,
        		comment : $scope.rating.comment
        	}
    	}).then(function(response) {
    		
    		if(response.data.success) {
    			toastr.success('Doctor rated successfully.');
    			$("#rating-modal").modal('hide');
    			$scope.rating.comment = "";
    			$scope.rating.value = 0;
    		} else {
    			toastr.error('Doctor rating failed. Try again');
    		}

    		showLoaderMessage(false);

    	}, function(response) {
    		showLoaderMessage(false);
    		toastr.error('Doctor rating failed.');
    	});
	}



	$scope.audio = null;
	$scope.playAudio = function() 
	{
		$scope.audio = new Audio(incoming_call_sound);
		if ($scope.audio && $scope.audio > 0 && !$scope.audio) {
	         $scope.audio.play();
	     } 
    };

    $scope.stopAudio = function()
    {
		if($scope.audio != null) {
			$scope.audio.pause();
			$scope.audio = null;
		}
    }





	$scope.requestTypeTitle = "";
	$scope.currentRequestData = null;
	$scope.isVideo = false;

	$scope.poll = new Polling(
         {
			url           : incoming_request_polling,
			requestMethod : 'GET',
			requestData   : {}
         }, 
         function(response){

            console.log(response);

            if(response.success) {

            	var requestLength = response.data.length;
            	if(requestLength) {
            		
            		if(response.data[0].request_type == 2 && $scope.consult.phone_consult_status == 1) {
            			this.scope.requestTypeTitle = "Phone Call Request";
            			$("user-card").css('display', 'flex');
	            		this.scope.playAudio();
	            		this.scope.currentRequestData = response.data[0];
	            		this.scope.isVideo = false;

            		} else if(response.data[0].request_type == 3) {
            			this.scope.requestTypeTitle = "Video Call Request";
            			$("user-card").css('display', 'flex');
	            		this.scope.playAudio();
	            		this.scope.currentRequestData = response.data[0];
	            		this.scope.isVideo = true;

            		} else if(response.data[0].request_type == 1) {
            			this.scope.requestTypeTitle = "Chat Consult Request";
            			$("user-card").css('display', 'flex');
	            		this.scope.playAudio();
	            		this.scope.currentRequestData = response.data[0];
            		}

            	} else {
            		this.scope.stopAudio();
            		$("user-card").css('display', 'none');
            	}

            	/*if(requestLength > 1) {
            		toastr.warning((requestLength-1) + " more waiting requests");
            	}*/




            	if(response.is_approved == 0) {
                 	$("#accountNotApprovedModal").modal('show');
            	}
            }
    
    		this.scope.$apply();
         },
         function(adf,textStatus,aasf){
         	this.scope.stopAudio();
            $("user-card").css('display', 'none');
         },
         $scope, 
         2000
    );
            
            
    $scope.poll.startPoll();




    $scope.isFullscreen = false;
    $scope.toggleFullScreen = function()
    {
    	console.log('asfdas');
    	if(
			document.fullscreenEnabled || 
			document.webkitFullscreenEnabled || 
			document.mozFullScreenEnabled ||
			document.msFullscreenEnabled
		) {
			console.log('in');
			var i = document.querySelector("#video-audio-modal .modal-content");
			console.log($scope.isFullscreen);
			if(!$scope.isFullscreen) {

				if (i.requestFullscreen) {
					i.requestFullscreen();
				} else if (i.webkitRequestFullscreen) {
					i.webkitRequestFullscreen();
				} else if (i.mozRequestFullScreen) {
					i.mozRequestFullScreen();
				} else if (i.msRequestFullscreen) {
					i.msRequestFullscreen();
				}

				$scope.isFullscreen = true;

			} else {

				if (document.exitFullscreen) {
					document.exitFullscreen();
				} else if (document.webkitCancelFullScreen) {
					document.webkitCancelFullScreen();
				} else if (document.mozCancelFullScreen) {
					document.mozCancelFullScreen();
				} else if (document.msExitFullscreen) {
					document.msExitFullscreen();
				}

				$scope.isFullscreen = false;
			}
    		
			

		}


    }

});