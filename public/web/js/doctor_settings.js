function initDatepicker() {

    $("#dob").datepicker({
        dateFormat: 'dd-mm-yy',
        maxDate: '0'
    });

    var scheduleScope = angular.element($("#schedule")).scope();

    $("#schedule-choose-date").datepicker({
        dateFormat: 'yy-mm-dd',
        beforeShowDay: function(date) {

            if (scheduleScope.dateExists(getDateString(date))) {
                return [true, 'schedule-date-hightlight', 'Booked date'];
            } else {
                return [true, '', ''];
            }
        }

    });

}

function getDateString(date) {
    var month = date.getMonth();
    month = month < 9 ? "0" + (month + 1) : month + 1;

    var day = date.getDate();
    day = day < 10 ? "0" + day : day;
    return date.getFullYear() + "-" + month + "-" + day;
}

var useSettings = angular.module('userSettings', ['ngRoute'], function($interpolateProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
});

useSettings.config(function($routeProvider) {

    $routeProvider
        .when("/", {
            templateUrl: profile_template_url,
            controller: "profileController"
        })
        .when("/profile", {
            templateUrl: profile_template_url,
            controller: "profileController"
        })
        .when("/schedules", {
            templateUrl: schedules_template_url,
            controller: "scheduleController"
        })
        .when("/messages", {
            templateUrl: messages_template_url,
            controller: "messagesController"
        });

});

useSettings.controller('messagesController', function($rootScope, $scope, $http) {

    $scope.cloaseSendMessageModal = function() {
        $scope.getMessagesList();
        $("#send-message-modal").modal('hide');
    }

    $scope.message = '';
    $scope.sendMessage = function() {
        showLoaderMessage(true, 'Sending message ...');

        if (!$scope.selectedPatient.user_id) {
            showLoaderMessage(false);
            toastr.info('Select a user first');
            return;
        }

        if ($scope.message == '') {
            showLoaderMessage(false);
            toastr.info('Enter some message first');
            return;
        }

        $http({
            url: send_message,
            method: "POST",
            data: {
                user_id: $scope.selectedPatient.user_id,
                message: $scope.message
            }
        }).then(function(response) {

            if (response.data.success) {
                toastr.success(response.data.message);
            }

            showLoaderMessage(false);
        }, function(response) {
            toastr.error('Failed to send message. Try again later.');
            showLoaderMessage(false);
        });
    }

    $scope.selectedPatient = {};
    $scope.selectPatient = function(user) {
        $scope.selectedPatient = user;
    }

    $scope.openSendMessageModal = function() {
        $scope.getFavPatients();
        $("#send-message-modal").modal('show');
    }

    $scope.favPatients = [];
    $scope.getFavPatients = function() {

        $http({
            url: fav_patients,
            method: "GET"
        }).then(function(response) {

            if (response.data.success) {
                $scope.favPatients = response.data.userArray;
                console.log($scope.favPatients);
            }

        }, function(response) {});
    }

    $scope.auth_name = auth_doctor_name;
    $scope.auth_picture = auth_doctor_picture;
    $scope.messages = [];

    $scope.getMessagesList = function() {
        showLoaderMessage(true, "Fetching messages ...");

        $http({
            url: messages_list,
            method: "GET"
        }).then(function(response) {

            if (response.data.success) {

                $scope.messages = response.data.messageArray;
                console.log($scope.messages);
            }

            showLoaderMessage(false);

        }, function(response) {
            showLoaderMessage(false);
        });
    }

    $scope.getMessagesList();

});




useSettings.controller('scheduleController', function($rootScope, $scope, $http) {

    $scope.schedules = [];
    $scope.no_schedules_found_text = "";
    $scope.scheduledDates = [];

    $scope.getSchedulesByDate = function() {

        showLoaderMessage(true, "Fetching schedules ...");

        $http({
            url: get_schedules_url + "?date=" + $scope.selectedDate,
            method: "GET"
        }).then(function(response) {
            if (response.data.success) {

                $scope.schedules = response.data.data;

                showLoaderMessage(false);

                if (!response.data.data.lenght) {
                    $scope.no_schedules_found_text = 'No schedules found on selected date : ' + $scope.selectedDate;
                }

                console.log('getSchedulesByDate :');
                console.log($scope.schedules);
            }

        }, function(response) {
            showLoaderMessage(false);
        });
    }

    $scope.getScheduledDates = function() {

        $http({
            url: get_schedule_dates,
            method: "GET"
        }).then(function(response) {

            if (response.data.status) {
                $scope.scheduledDates = response.data.success_data;
            }

            console.log('getScheduledDates : ');
            console.log(response.data);

        }, function(response) {});
    }

    $scope.dateExists = function(date) {
        return $scope.scheduledDates.indexOf(date) < 0 ? false : true;
    }

    $scope.showNoSchedule = function() {
        return ($scope.schedules.length == 0);
    }

    $scope.getScheduledDates();
    $scope.getSchedulesByDate();
    initDatepicker();



    $scope.removeScheduleObject = function(schedule)
    {
        var index = $scope.schedules.indexOf(schedule);
        if(index > -1) {
            $scope.schedules.splice(index, 1);
        }
    }


    /**
    *   cancel user booking
    */
    $scope.cancelBooking = function(schedule)
    {
        showLoaderMessage(true, "Canceling booking ...");
           
        $http({
            url: cancel_booking,
            method: "POST",
            data : {
                available_slot_id : schedule.available_slot_id
            }
        }).then(function(response) {

            if(response.data.success) {
                toastr.success(response.data.message);
                $scope.removeScheduleObject(schedule);
            }

           console.log(response.data);

           showLoaderMessage(false);

        }, function(response) {
            toastr.error('Internal server error try after sometiime');
        });
    }



    /**
    *   end user booking
    */
    $scope.endBooking = function(schedule)
    {


        showLoaderMessage(true, "Accepting booking ...");
           
        $http({
            url: end_booking,
            method: "POST",
            data : {
                request_id : schedule.request_id
            }
        }).then(function(response) {

            if(response.data.success) {
                toastr.success(response.data.title);
                $scope.removeScheduleObject(schedule);

                $('#invoice').modal('show');

                $scope.endBookingResponse = response.data;

            }

           console.log(response.data);

           showLoaderMessage(false);

        }, function(response) {
            toastr.error('Internal server error try after sometiime');
        });

     

        

    }



    $scope.closeInvoice = function()
    {
        $('#invoice').modal('hide');
    }


    $scope.doc_currency = currency;






});







useSettings.controller('profileController', function($rootScope, $scope, $http) {

    $scope.profileDetails = {};
    $scope.doctor = {};

    $scope.getProfileDetails = function() {

        showLoaderMessage(true, 'Fetching profile ...');

        $http({
            url: profile_details_url,
            method: "GET",
        }).then(function(response) {
            if (response.data.status) {
                $scope.profileDetails = response.data.success_data;
                $scope.initDoctorDetails();
                showLoaderMessage(false);
                console.log($scope.profileDetails);
            }

        }, function(response) { // optional
            showLoaderMessage(false);
        });

    }

    $scope.getProfileDetails();

    $scope.initDoctorDetails = function() {
        $scope.doctor.first_name = $scope.profileDetails.doctor.first_name;
        $scope.doctor.last_name = $scope.profileDetails.doctor.last_name;
        $scope.doctor.gender = $scope.profileDetails.doctor.gender;
        $scope.doctor.email = $scope.profileDetails.doctor.email;
        $scope.doctor.language = $scope.profileDetails.doctor.language;
        $scope.doctor.currency = $scope.profileDetails.doctor.currency;
        $scope.doctor.dob = $scope.profileDetails.doctor.dob;
        $scope.doctor.nationality = $scope.profileDetails.doctor.nationality;
        $scope.doctor.country_code = $scope.profileDetails.doctor.country_code;
        $scope.doctor.phone = $scope.profileDetails.doctor.phone;
        $scope.doctor.ssn = $scope.profileDetails.doctor.ssn;
        $scope.doctor.med_number = $scope.profileDetails.doctor.med_number;
        $scope.doctor.booking_fee = $scope.profileDetails.doctor_fees.booking_fee;
        $scope.doctor.chat_fee = $scope.profileDetails.doctor_fees.chat_fee;
        $scope.doctor.ondemand_fee = $scope.profileDetails.doctor_fees.on_demand_fee;
        $scope.doctor.phone_fee = $scope.profileDetails.doctor_fees.phone_fee;
        $scope.doctor.video_fee = $scope.profileDetails.doctor_fees.video_fee;
        $scope.doctor.free_minute = $scope.profileDetails.doctor_fees.free_minutes;
        $scope.doctor.consult_minute = $scope.profileDetails.doctor_fees.consult_rate.minutes;
        $scope.doctor.minutes_consult_fee = $scope.profileDetails.doctor_fees.consult_rate.rate_amount;
    }

    initDatepicker();

    $scope.updateDoctor = function() {

        showLoaderMessage(true, 'Updating profile ...');

        $scope.doctor._token = csrf_token;

        var formData = $scope.formData($scope.doctor);

        if ($("#picture")[0].files[0] != undefined) {
            formData.append('picture', $("#picture")[0].files[0]);
        }

        $http({
            url: profile_details_update_url,
            method: "POST",
            data: formData,
            headers: {
                'Content-Type': undefined
            },
            transformRequest: angular.identity
        }).then(function(response) {

            console.log(response.data);

            if (response.data.status) {

                $scope.profileDetails = response.data.success_data;
                $scope.initDoctorDetails();
                toastr.success('Profile updated successfully.');

            } else if (response.data.error_type == "VALIDATION_ERROR") {
                toastr.error(response.data.error_data[Object.keys(response.data.error_data)[0]]);
            } else if (response.data.error_type == "PASSWORDS_LENGHT_LESS_SIX") {
                toastr.error(response.data.error_text);
            } else if (response.data.error_type == "INVALID_OLD_PASS") {
                toastr.error(response.data.error_text);
            } else if (response.data.error_type == "INVALID_OTP") {
                toastr.error(response.data.error_text);
            }

            showLoaderMessage(false);

        }, function(response) { // optional

            showLoaderMessage(false);
        });
    }

    $scope.formData = function(data) {
        var fd = new FormData();
        angular.forEach(data, function(value, key) {
            fd.append(key, value);
        });
        return fd;
    }

    $scope.sendOTP = function() {

        showLoaderMessage(true, 'Sending otp ...');

        $("#send_otp_btn").attr("disabled", true);
        var phone = $scope.doctor.phone;
        var countryCode = $scope.doctor.country_code;

        $http({
            url: otp_send_url,
            method: "POST",
            data: {
                phone: phone,
                country_code: countryCode,
                _token: csrf_token
            },
        }).then(function(response) {

            if (!response.data.status && response.data.error_type == "VALIDATION_ERROR") {

                toastr.error(response.data.error_text);

            } else if (!response.data.status && response.data.error_type == "UNKNOWN_ERROR") {

                toastr.error('Some error. Try after sometime.')

            } else if (response.data.status && response.data.success_type == "OTP_SENT") {

                toastr.success(response.data.success_text);
            }

            $("#send_otp_btn").attr("disabled", false);
            showLoaderMessage(false, 'Sending otp ...');

        }, function(response) {
            $("#send_otp_btn").attr("disabled", false);
            showLoaderMessage(false, 'Sending otp ...');
        });

    }

});