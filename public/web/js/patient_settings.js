function initDatepicker() {

    $("#dob").datepicker({
        dateFormat: 'dd-mm-yy',
        maxDate: '0'
    });

    var scheduleScope = angular.element($("#schedules")).scope();

    $("#schedule-choose-date").datepicker({
        dateFormat: 'yy-mm-dd',
        beforeShowDay: function(date) {

            if (scheduleScope.dateExists(getDateString(date))) {
                return [true, 'schedule-date-hightlight', 'Booked date'];
            } else {
                return [true, '', ''];
            }
        }

    });

};

function getDateString(date) {
    var month = date.getMonth();
    month = month < 9 ? "0" + (month + 1) : month + 1;

    var day = date.getDate();
    day = day < 10 ? "0" + day : day;
    return date.getFullYear() + "-" + month + "-" + day;
}

var useSettings = angular.module('userSettings', ['ngRoute'], function($interpolateProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
});

useSettings.filter('chat_timestamp_filter', function() {

    return function(timeStamp) {
        var date = new Date(timeStamp);
        return date.toLocaleString('en-US', {
            hour: 'numeric',
            minute: 'numeric',
            hour12: true,
            year: 'numeric',
            month: 'numeric',
            day: 'numeric'
        });
    };
});

useSettings.config(function($routeProvider) {

    $routeProvider
        .when("/", {
            templateUrl: profile_template_url,
            controller: "profileController"
        })
        .when("/profile", {
            templateUrl: profile_template_url,
            controller: "profileController"
        })
        .when("/schedules", {
            templateUrl: schedules_template_url,
            controller: "scheduleController"
        })
        .when("/cards", {
            templateUrl: user_cards_template_url,
            controller: "cardsController"
        })
        .when("/change-password", {
            templateUrl: change_password_template_url,
            controller: "passwordController"
        })
        .when("/chat-consult-history", {
            templateUrl: chat_consult_history_template,
            controller: "chatConsultHistoryController"
        })
        .when("/phone-consult-history", {
            templateUrl: phone_consult_history_template,
            controller: "phoneConsultHistoryController"
        })
        .when("/video-consult-history", {
            templateUrl: video_consult_history_template,
            controller: "videoConsultHistoryController"
        })
        .when("/ask-question", {
            templateUrl: ask_question_template,
            controller: "askQuestionController"
        })
        .when("/doctor-messages", {
            templateUrl: doctor_messages_template,
            controller: "doctorMessagesController"
        });

});

function removeAllActiveMenus() {
    $('.active-menu').each(function(item) {
        $(this).removeClass('active-menu');
    });
}

function activeMenu(refname) {
    removeAllActiveMenus();
    $("a[href='#" + refname + "']").find('li').addClass('active-menu');
}

useSettings.controller('doctorMessagesController', function($rootScope, $scope, $http) {

    activeMenu("doctor-messages");

    $scope.auth_user_picture = user_picture;
    $scope.auth_user_name = user_name;

    $scope.messages = [];
    $scope.getMessages = function() {
        showLoaderMessage(true, 'Fetching messages ...');
        $http({
            url: doctor_messages,
            method: "GET"
        }).then(function(response) {

            if (response.data.success) {
                $scope.messages = response.data.messageArray;
            }
            console.log(response.data.messageArray);
            showLoaderMessage(false);

        }, function(response) {
            showLoaderMessage(false);
        });

    }

    $scope.getMessages();

    $scope.sendMessageReply = function(message) {
        if (message.reply_text == '') {
            showLoaderMessage(false);
            toastr.info('Message reply body is required');
            return;
        }

        showLoaderMessage(true, 'Sending message reply ...');
        $http({
            url: send_message_reply,
            method: "POST",
            data: {
                message_id: message.id,
                reply: message.reply_text
            }
        }).then(function(response) {

            if (response.data.success) {
                message.reply = message.reply_text;
                toastr.success('Message reply send successfully.');
            }
            console.log(response.data);
            showLoaderMessage(false);

        }, function(response) {
            showLoaderMessage(false);
        });
    }

});

useSettings.controller('askQuestionController', function($rootScope, $scope, $http) {

    activeMenu("ask-question");

    $scope.specialities = [];
    $scope.selectedSpeciality = null;

    $scope.getSpecialities = function() {

        $http({
            url: get_specialities,
            method: "GET"
        }).then(function(response) {

            console.log(response.data);
            if (response.data.status) {
                $scope.specialities = response.data.success_data;
                $scope.selectedSpeciality = $scope.specialities[0];
            }

        }, function(response) {});

    }

    $scope.getSpecialities();

    $scope.selectSpeciality = function(index) {

        $scope.selectedSpeciality = $scope.specialities[index];
        console.log($scope.selectedSpeciality);
    }

    $scope.searchedDoctors = [];
    $scope.search_doctor = "";
    $scope.selectedDoctor = null;

    $scope.searchDoctors = function() {
        $scope.docotr_name = "";
        $scope.docotr_name = $scope.search_doctor;
        $scope.search_doctor = "";
        $scope.selectedDoctor = null;

        $http({
            url: search_doctors_url + "?speciality_id=" + $scope.selectedSpeciality.id + "&doctor_name=" + $scope.docotr_name + "&nDocotr=10",
            method: "GET"
        }).then(function(response) {

            console.log(response.data);
            if (response.data.success) {
                $scope.searchedDoctors = response.data.doctors;
                if ($scope.searchedDoctors.length) {
                    $("#search_doctor").find('[data-toggle=dropdown]').dropdown('toggle');
                }
            }

        }, function(response) {});
    }

    $scope.selectDoctor = function(index) {
        $scope.selectedDoctor = $scope.searchedDoctors[index];
        $scope.search_doctor = $scope.selectedDoctor.first_name + " " + $scope.selectedDoctor.first_name;
    }

    $scope.subject = "";
    $scope.content = "";
    $scope.submitQuestion = function() {

        showLoaderMessage(true, "Submitting your question...");

        if (!$scope.selectedDoctor || !$scope.selectedDoctor.id) {
            toastr.error("Choose a doctor");
            showLoaderMessage(false);
            return;
        }

        if ($scope.subject == "") {
            toastr.error("Type your subject");
            showLoaderMessage(false);
            return;
        }

        if ($scope.content == "") {
            toastr.error("Type your question");
            showLoaderMessage(false);
            return;
        }

        $http({
            url: ask_question,
            method: "POST",
            data: {
                doctor_id: $scope.selectedDoctor.id,
                subject: $scope.subject,
                content: $scope.content
            }
        }).then(function(response) {

            console.log(response.data);
            if (response.data.success) {
                toastr.success(response.data.message);
                $scope.subject = "";
                $scope.content = "";
            } else {
                toastr.error('Failed to submit question. Try after sometime.');
            }
            showLoaderMessage(false);

        }, function(response) {
            showLoaderMessage(false);
        });

    }

});

useSettings.controller('videoConsultHistoryController', function($rootScope, $scope, $http) {

    $("#collapseTwo").collapse('show');
    activeMenu("video-consult-history");

    $scope.currency = user_currency;
    $scope.user_picture = user_picture;

    $scope.videoConsultHistories = [];
    $scope.take = 5;
    $scope.skip = 0;

    $scope.loadMore = true;

    $scope.getVideoConsultHistories = function() {
        $scope.isFetchingVideoHistories = true;
        showLoaderMessage(true, "Fetching video consult histories ...");
        $http({
            url: video_consult_histories + "?take=" + $scope.take + "&skip=" + $scope.skip,
            method: "GET"
        }).then(function(response) {

            if (response.data.success) {
                $scope.videoConsultHistories = $scope.videoConsultHistories.concat(response.data.data);

                console.log($scope.videoConsultHistories);

                if (response.data.data.length) {
                    $scope.loadMore = true;
                    $scope.skip += $scope.take;
                } else {
                    $scope.loadMore = false;
                }

            }

            $scope.isFetchingVideoHistories = false;
            showLoaderMessage(false);

        }, function(response) {
            $scope.isFetchingVideoHistories = false;
            showLoaderMessage(false);
        });
    }

    $scope.getVideoConsultHistories();

});

useSettings.controller('phoneConsultHistoryController', function($rootScope, $scope, $http) {

    $("#collapseTwo").collapse('show');
    activeMenu("phone-consult-history");

    $scope.currency = user_currency;
    $scope.user_picture = user_picture;

    $scope.phoneConsultHistories = [];
    $scope.take = 5;
    $scope.skip = 0;

    $scope.loadMore = true;

    $scope.getPhoneConsultHistories = function() {
        $scope.isFetchingPhoneHistories = true;
        showLoaderMessage(true, "Fetching phone consult histories ...");
        $http({
            url: phone_consult_histories + "?take=" + $scope.take + "&skip=" + $scope.skip,
            method: "GET"
        }).then(function(response) {

            if (response.data.success) {
                $scope.phoneConsultHistories = $scope.phoneConsultHistories.concat(response.data.data);

                console.log($scope.phoneConsultHistories);

                if (response.data.data.length) {
                    $scope.loadMore = true;
                    $scope.skip += $scope.take;
                } else {
                    $scope.loadMore = false;
                }

            }

            $scope.isFetchingPhoneHistories = false;
            showLoaderMessage(false);

        }, function(response) {
            $scope.isFetchingPhoneHistories = false;
            showLoaderMessage(false);
        });
    }

    $scope.getPhoneConsultHistories();

});

useSettings.controller('chatConsultHistoryController', function($rootScope, $scope, $http) {

    $("#collapseTwo").collapse('show');
    activeMenu("chat-consult-history");

    $scope.currency = user_currency;
    $scope.user_picture = user_picture;

    $scope.chatConsultHistories = [];
    $scope.take = 5;
    $scope.skip = 0;

    $scope.loadMore = true;

    $scope.getChatConsultHistories = function() {
        $scope.isFetchingChatHistories = true;
        showLoaderMessage(true, "Fetching chat consult histories ...");
        $http({
            url: chat_consult_histories + "?take=" + $scope.take + "&skip=" + $scope.skip,
            method: "GET"
        }).then(function(response) {

            if (response.data.success) {
                $scope.chatConsultHistories = $scope.chatConsultHistories.concat(response.data.data);

                console.log($scope.chatConsultHistories);

                if (response.data.data.length) {
                    $scope.loadMore = true;
                    $scope.skip += $scope.take;
                } else {
                    $scope.loadMore = false;
                }

            }

            $scope.isFetchingChatHistories = false;
            showLoaderMessage(false);

        }, function(response) {
            $scope.isFetchingChatHistories = false;
            showLoaderMessage(false);
        });
    }

    $scope.getChatConsultHistories();

    $scope.chatMessages = [];

    $scope.chatMessageExists = function(requestId) {
        return ($scope.chatMessages[requestId]) ? true : false;
    }

    $scope.getChatMessages = function(requestId) {

        if ($scope.chatMessageExists(requestId)) {
            return;
        }

        showLoaderMessage(true, "Chat messages are being fetched ...");

        $http({
            url: chat_consult_messages + "?request_id=" + requestId,
            method: "GET"
        }).then(function(response) {

            console.log(response.data);

            if (response.data.success) {
                $scope.chatMessages[requestId] = response.data.chatArray;
            }

            showLoaderMessage(false);

        }, function(response) {
            showLoaderMessage(false);
        });

    }

});

useSettings.controller('passwordController', function($rootScope, $scope, $http) {
    //

    activeMenu("change-password");

    $scope.password = {
        old_password: "",
        new_password: "",
        confirm_password: ""
    };

    $scope.changePassword = function() {

        showLoaderMessage(true, 'Changing password ...');

        if ($scope.password.old_password == "") {
            toastr.error('Enter current password');
            showLoaderMessage(false);
            return;
        }

        if ($scope.password.new_password == "" || $scope.password.confirm_password == "") {
            toastr.error('Both new password and confirm password required');
            showLoaderMessage(false);
            return;
        }

        if ($scope.password.new_password !== $scope.password.confirm_password) {
            toastr.error('New password and confirm password does not match');
            showLoaderMessage(false);
            return;
        }

        $http({
            url: change_password,
            method: "POST",
            data: $scope.password
        }).then(function(response) {
            console.log(response.data);

            if (response.data.success) {
                toastr.success(response.data.message);
            } else {
                toastr.error(response.data.error_message);
            }

            showLoaderMessage(false);

        }, function(response) {
            showLoaderMessage(false);
        });

    }
});

useSettings.controller('cardsController', function($rootScope, $scope, $http) {

    activeMenu("cards");

    $scope.addCardModal = function() {
        $("#add-card-modal").modal('show');
        $scope.getBraintreeClientToken();
    }

    $scope.braintreeClientToken = "";
    $scope.getBraintreeClientToken = function() {
        showLoaderMessage(true, 'Generating braintree token ...');
        $http({
            url: get_braintree_client_token,
            method: "POST"
        }).then(function(response) {
            console.log(response.data);
            if (response.data.success) {
                $scope.braintreeClientToken = response.data.client_token;
                $scope.nonce = "";
                $scope.initBraintree();
            }
            showLoaderMessage(false);

        }, function(response) {
            showLoaderMessage(false);
        });
    }

    $scope.nonce = "";
    $scope.initBraintree = function() {
        $("#dropin-container").html("");
        braintree.setup(
            $scope.braintreeClientToken,
            'dropin', {
                container: 'dropin-container',
                onPaymentMethodReceived: function(object) {

                    $scope.nonce = object.nonce;
                    $scope.saveCard();

                },
                onError: function(error) {
                    if (error.type == "VALIDATION") {
                        toastr.error(error.message);
                    }
                }

            });
    }

    $scope.makeDefaultCard = function(index) {
        var card = $scope.savedCards[index];
        if (card.is_default) {
            return;
        }

        showLoaderMessage(true, 'Making Card as default card ...');
        $http({
            method: "POST",
            url: make_default_card,
            data: {
                last_four: card.last_four
            }
        }).then(function success(response) {

            if (response.data.success) {
                $scope.getSavedCards();
            }

            showLoaderMessage(false);
        }, function error() {
            showLoaderMessage(false);
        });

    }

    $scope.saveCard = function() {

        showLoaderMessage(true, 'Saving card ...');
        $http({
            url: save_card_url,
            method: "POST",
            data: {
                payment_method_nonce: $scope.nonce
            }
        }).then(function(response) {

            if (response.data.success) {
                toastr.success(response.data.message);
            } else {
                toastr.error(response.data.message);
            }

            $("#add-card-modal").modal('hide');

            $scope.getSavedCards();

            showLoaderMessage(false);

        }, function(response) {
            showLoaderMessage(false);
        });
    }

    $scope.savedCards = [];

    $scope.getSavedCards = function() {
        showLoaderMessage(true, 'Fetching saved cards ...');
        $http({
            url: get_cards_url,
            method: "GET"
        }).then(function(response) {

            if (response.data.success) {
                $scope.savedCards = response.data.cards;
                $scope.getCardTypeImage($scope.savedCards[0].card_type);
            }
            showLoaderMessage(false);

        }, function(response) {
            showLoaderMessage(false);
        });
    }

    $scope.getSavedCards();

    $scope.cardImage32BaseUrl = card_image_32_base_url;
    $scope.getCardTypeImage = function(type) {
        return $scope.cardImage32BaseUrl + '/' + type.toLowerCase() + '_32.png';
    }

});



useSettings.directive('emitLastRepeaterElement', function() {
    return function(scope) {
        if (scope.$last){
        scope.$emit('LastRepeaterElement');
        }
    };
});


useSettings.controller('scheduleController', function($rootScope, $scope, $http) {

    activeMenu("schedules");

    $scope.schedules = [];
    $scope.no_schedules_found_text = "";
    $scope.scheduledDates = [];

    $scope.getSchedulesByDate = function() {

        showLoaderMessage(true, "Fetching schedules ...");

        $http({
            url: get_schedules_url + "?date=" + $scope.selectedDate,
            method: "GET"
        }).then(function(response) {
            if (response.data.success) {

                $scope.schedules = response.data.data;

                showLoaderMessage(false);

                if (!response.data.data.lenght) {
                    $scope.no_schedules_found_text = 'No schedules found on selected date : ' + $scope.selectedDate;
                }

                console.log($scope.schedules);
            }

        }, function(response) {
            showLoaderMessage(false);
        });
    }

    $scope.getScheduledDates = function() {

        $http({
            url: get_schedule_dates,
            method: "GET"
        }).then(function(response) {

            if (response.data.status) {
                $scope.scheduledDates = response.data.success_data;
            }

        }, function(response) {});
    }

    $scope.dateExists = function(date) {
        return $scope.scheduledDates.indexOf(date) < 0 ? false : true;
    }

    $scope.showNoSchedule = function() {
        return ($scope.schedules.length == 0);
    }

    $scope.getScheduledDates();
    $scope.getSchedulesByDate();
    initDatepicker();

    $scope.$on('LastRepeaterElement', function(){
        
        setTimeout(function(){

            elems = document.getElementsByClassName('canvas_clock');
            for(i = 0; i < elems.length; i++) {
               
                $scope.drawAnalogClock(elems[i].getAttribute('data-hour-min'), elems[i] );
            }


        }, 1000);

            
    });



    $scope.drawAnalogClock = function(hourMin, canvas)
    {
        //var canvas = document.getElementById(canvasid);
        
        var ctx = canvas.getContext("2d");
        var radius = canvas.height / 2;
        ctx.translate(radius, radius);
        radius = radius * 0.90
        hour = hourMin.split(":")[0];
        minute = hourMin.split(":")[1];

        $scope.drawClock(ctx, radius, hour,minute,0);
    }


    $scope.drawClock = function (ctx,radius, hour, minute, second) {
        $scope.drawFace(ctx, radius);
        $scope.drawNumbers(ctx, radius);
        $scope.drawTime(ctx, radius, hour, minute, second);
    }

    $scope.drawFace = function (ctx, radius) {
      var grad;
      ctx.beginPath();
      ctx.arc(0, 0, radius, 0, 2*Math.PI);
      ctx.fillStyle = 'white';
      ctx.fill();
      grad = ctx.createRadialGradient(0,0,radius*0.95, 0,0,radius*1.05);
      grad.addColorStop(0, '#33B5E5');
      grad.addColorStop(0.5, 'white');
      grad.addColorStop(1, '#33B5E5');
      ctx.strokeStyle = grad;
      ctx.lineWidth = radius*0.1;
      ctx.stroke();
      ctx.beginPath();
      ctx.arc(0, 0, radius*0.1, 0, 2*Math.PI);
      ctx.fillStyle = '#333';
      ctx.fill();
    }

    $scope.drawNumbers =  function (ctx, radius) {
      var ang;
      var num;
      ctx.font = radius*0.15 + "px arial";
      ctx.textBaseline="middle";
      ctx.textAlign="center";
      for(num = 1; num < 13; num++){
        ang = num * Math.PI / 6;
        ctx.rotate(ang);
        ctx.translate(0, -radius*0.85);
        ctx.rotate(-ang);
        ctx.fillText(num.toString(), 0, 0);
        ctx.rotate(ang);
        ctx.translate(0, radius*0.85);
        ctx.rotate(-ang);
      }
    }

    $scope.drawTime = function (ctx, radius, hour, minute, second){
       
        hour=(hour*Math.PI/6)+
        (minute*Math.PI/(6*60))+
        (second*Math.PI/(360*60));
        $scope.drawHand(ctx, hour, radius*0.5, radius*0.07);
        //minute
        minute=(minute*Math.PI/30)+(second*Math.PI/(30*60));
        $scope.drawHand(ctx, minute, radius*0.8, radius*0.07);
        // second
        second=(second*Math.PI/30);
        $scope.drawHand(ctx, second, radius*0.9, radius*0.02);
    }

    $scope.drawHand = function (ctx, pos, length, width) {
        ctx.beginPath();
        ctx.lineWidth = width;
        ctx.lineCap = "round";
        ctx.moveTo(0,0);
        ctx.rotate(pos);
        ctx.lineTo(0, -length);
        ctx.stroke();
        ctx.rotate(-pos);
    }




});

useSettings.controller('profileController', function($rootScope, $scope, $http) {

    activeMenu("profile");

    $scope.profileDetails = {};
    $scope.patient = {};

    $scope.getProfileDetails = function() {

        showLoaderMessage(true, 'Fetching profile ...');

        $http({
            url: profile_details_url,
            method: "GET",
        }).then(function(response) {
            if (response.data.status) {
                $scope.profileDetails = response.data.success_data;
                $scope.initDoctorDetails();
                showLoaderMessage(false);
                console.log($scope.profileDetails);
            }

        }, function(response) { // optional
            showLoaderMessage(false);
        });

    }

    $scope.getProfileDetails();

    $scope.initDoctorDetails = function() {
        $scope.patient.first_name = $scope.profileDetails.first_name;
        $scope.patient.last_name = $scope.profileDetails.last_name;
        $scope.patient.gender = $scope.profileDetails.gender;
        $scope.patient.email = $scope.profileDetails.email;
        $scope.patient.language = $scope.profileDetails.language;
        $scope.patient.currency = $scope.profileDetails.currency;
        $scope.patient.dob = $scope.profileDetails.dob;
        $scope.patient.nationality = $scope.profileDetails.nationality;
        $scope.patient.country_code = $scope.profileDetails.country_code;
        $scope.patient.phone = $scope.profileDetails.phone;
        $scope.patient.ssn = $scope.profileDetails.ssn;
    }

    initDatepicker();

    $scope.updateUser = function() {

        showLoaderMessage(true, 'Updating profile ...');

        $scope.patient._token = csrf_token;

        var formData = $scope.formData($scope.patient);

        if ($("#picture")[0].files[0] != undefined) {
            formData.append('picture', $("#picture")[0].files[0]);
        }

        $http({
            url: profile_details_update_url,
            method: "POST",
            data: formData,
            headers: {
                'Content-Type': undefined
            },
            transformRequest: angular.identity
        }).then(function(response) {

            console.log(response.data);

            if (response.data.status) {

                $scope.profileDetails = response.data.success_data;
                $scope.initDoctorDetails();
                toastr.success('Profile updated successfully.');

            } else if (response.data.error_type == "VALIDATION_ERROR") {
                toastr.error(response.data.error_data[Object.keys(response.data.error_data)[0]]);
            } else if (response.data.error_type == "INVALID_OLD_PASS") {
                toastr.error(response.data.error_text);
            } else if (response.data.error_type == "INVALID_OTP") {
                toastr.error(response.data.error_text);
            }

            showLoaderMessage(false);

        }, function(response) { // optional

            showLoaderMessage(false);
        });
    }

    $scope.formData = function(data) {
        var fd = new FormData();
        angular.forEach(data, function(value, key) {
            fd.append(key, value);
        });
        return fd;
    }

    $scope.sendOTP = function() {

        showLoaderMessage(true, 'Sending otp ...');

        $("#send_otp_btn").attr("disabled", true);
        var phone = $scope.patient.phone;
        var countryCode = $scope.patient.country_code;

        $http({
            url: otp_send_url,
            method: "POST",
            data: {
                phone: phone,
                country_code: countryCode,
                _token: csrf_token
            },
        }).then(function(response) {

            if (!response.data.status && response.data.error_type == "VALIDATION_ERROR") {

                toastr.error(response.data.error_text);

            } else if (!response.data.status && response.data.error_type == "UNKNOWN_ERROR") {

                toastr.error('Some error. Try after sometime.')

            } else if (response.data.status && response.data.success_type == "OTP_SENT") {

                toastr.success(response.data.success_text);
            }

            $("#send_otp_btn").attr("disabled", false);
            showLoaderMessage(false, 'Sending otp ...');

        }, function(response) {
            $("#send_otp_btn").attrpatient("disabled", false);
            showLoaderMessage(false, 'Sending otp ...');
        });

    }

});