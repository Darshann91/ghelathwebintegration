function initBookingCalendar()
{
	$("#booking_calendar").datepicker({
	    dateFormat: 'yy-mm-dd',
	    minDate: 0,
	    onSelect: function(dateText, inst) {
      		$("#selected_booking_date").val(dateText);
      		$("#selected_booking_date").trigger("change");
    	}
	});
}
var categoriesApp = angular.module('categoriesApp', ['ngRoute'], function($interpolateProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
});


categoriesApp.filter('chat_timestamp_filter', function() {
    
    return function(timeStamp) {
        var date = new Date(timeStamp);
    	return date.toLocaleString('en-US', { 
    		hour: 'numeric',
    		minute:'numeric', 
    		hour12: true, 
    		year:'numeric', 
    		month:'numeric',
    		day:'numeric' 
    	});
    };
});



categoriesApp.factory('socket', function ($rootScope) {
  
  var socket = io.connect(socket_host+":"+socket_port);

  return {
    on: function (eventName, callback) {
      socket.on(eventName, function () {  
        var args = arguments;
        $rootScope.$apply(function () {
          callback.apply(socket, args);
        });
      });
    },
    emit: function (eventName, data, callback) {
      socket.emit(eventName, data, function () {
        var args = arguments;
        $rootScope.$apply(function () {
          if (callback) {
            callback.apply(socket, args);
          }
        });
      })
    }
  };
});




categoriesApp.directive('rating', function ($parse) {
    return {
        restrict: 'A',
        scope: {
		    rating : "=model"
		},
        link: function ($scope, element, attrs) {

        	$(element).rateYo({
    			rating: 0,
    			fullStar: true
  			});

  			$(element).rateYo().on("rateyo.change", function (e, data) {
  				$scope.$apply(function(){
  					$scope.rating.value = data.rating;
  				});
            });
        }
    };
});




categoriesApp.config(function($routeProvider) {

    $routeProvider
    .when("/", {
        templateUrl : online_consult_template_url,
        controller : "onlineConsultCtrl"
    })
    .when("/online-consult", {
        templateUrl : online_consult_template_url,
        controller : "onlineConsultCtrl"
    })
    .when("/book_appointment", {
        templateUrl : book_appointment_template_url,
        controller : "bookAppointmentCtrl"
    })

});


categoriesApp.controller('mainCtrl', function($rootScope, $scope, $http) {

	$scope.specialities = [];

	$scope.getOnlineConsultDoctorsList = function()
	{
		$scope.$broadcast('speciality_selected_event', $scope.selectedSpeciality);
	}


    $scope.getDoctorListBooking = function()
    {
        $scope.$broadcast('doctor_booking_speciality_selected_event', $scope.selectedSpeciality);
    }




	$scope.fetchSpecialities = function()
	{
		$http({

			method: "GET",
			url   : get_specialities_url

	    }).then(function mySucces(response) {
	        
	        if(response.data.status) {

	        	$scope.specialities = response.data.success_data;
	        	$scope.selectedSpeciality = initial_spcialitiy_id;
	        	console.log("specialities", $scope.specialities);
	        }

	    }, function myError(response) {});
	}

	$scope.fetchSpecialities();

});



function getCurrentDateString()
{
	return getDateString(new Date());
}


function getDateString(date) 
{ 
	var month = date.getMonth();
	month = month < 9 ? "0" + (month + 1) : month + 1; 
	var day = date.getDate(); 
	day = day < 10 ? "0" + day : day; 
	return date.getFullYear() + "-" + month + "-" + day; 
}



/**
*	used to get timestring less than current time or not
* 	format timeString : hh:mm aa (eg 11:00 PM)
*/
function isTimeLess(timeString)
{
	var now = new Date();
	var dt = (now.getMonth()+1) + "/" + now.getDate() + "/" + now.getFullYear() + " " + timeString;
	var givenDate = new Date(dt);

	return (givenDate < now) ? true : false;
}





categoriesApp.controller('bookAppointmentCtrl', function($rootScope, $scope, $http) {

	initGoogleAutoComplete();

	//show the book appoint block
	$('#collapse1').collapse("hide");
	$('#collapse2').collapse("show");


	$scope.speciality_id = initial_spcialitiy_id;    


	$scope.google_map_api_key = google_map_api_key;

	//log user detail boock
	$scope.user_currency = user_currency;
    $scope.user_fname = user_fname;
    $scope.user_lname = user_lname;
    $scope.user_photo = user_photo;


    /* pagination variables */

	$scope.initPaginationVariables = function()
	{
		$scope.booking_doctors_count    = 0;
		$scope.booking_doctors_take     = 5;
		$scope.booking_doctors_skip     = 0;
		$scope.booking_doctors_cur_page = 0;
		$scope.booking_doctors_list_cur_page_increment = false;
	}
	$scope.initPaginationVariables();


	$scope.selectedDoctor = null;
	$scope.bookingDoctorsList = [];


	$scope.nextBookingDoctorsList = function()
	{  
		if($scope.booking_doctors_skip < $scope.booking_doctors_count) {
			$scope.booking_doctors_skip += $scope.booking_doctors_take;
			$scope.booking_doctors_list_cur_page_increment = true;
			$scope.fetchBookingDoctorsList();
		}
	}

	$scope.previousBookingDoctorsList = function()
	{
		if($scope.booking_doctors_skip > 0) {
			$scope.booking_doctors_skip -= $scope.booking_doctors_take;
			$scope.booking_doctors_list_cur_page_increment = false;
			$scope.fetchBookingDoctorsList();
		}
	}

	$scope.hidePrevious = false;
	$scope.hideNext = false;
	$scope.totalDoctorFetchedCount = 0;
	$scope.fetchBookingDoctorsList = function()
	{
		showLoaderMessage(true, 'Fetching Booking Doctors ...');
		$http({
			method : "GET",
			url : get_booking_doctors_list_url 
					+ "?speciality_id=" 
					+ $scope.speciality_id
					+ "&skip="
					+ $scope.booking_doctors_skip
					+ "&take="
					+ $scope.booking_doctors_take
		}).then(function success(response){

			if(response.data.success) {
				$scope.booking_doctors_count = response.data.count;
				$scope.bookingDoctorsList = response.data.doctor_list;	

				$scope.totalDoctorFetchedCount += $scope.bookingDoctorsList.length;

				if($scope.booking_doctors_list_cur_page_increment) {
					$scope.booking_doctors_cur_page++;
				} else {
					$scope.booking_doctors_cur_page--;
				}	

                $scope.has_doctors = true;



                if((response.data.doctor_list.length < $scope.booking_doctors_take) || ($scope.totalDoctorFetchedCount == $scope.booking_doctors_count)) {
					$scope.hideNext = true;
				} else {
					$scope.hideNext = false;
				}


				if($scope.booking_doctors_skip > 0) {
					$scope.hidePrevious = false;
				} else {
					$scope.hidePrevious = true;
				}





			} else {
                $scope.has_doctors = false;
                $scope.hideNext = true;
            }
        
            $scope.hideAllBlocks();
            $("#book_appointment_block").fadeIn();

			showLoaderMessage(false);
			console.log(response.data);
		}, function error(){
			showLoaderMessage(false);
		});
	}

	$scope.fetchBookingDoctorsList();



	$scope.$on("doctor_booking_speciality_selected_event", function(event, specialityID){

        $scope.speciality_id = specialityID;
        $scope.initPaginationVariables();
        $scope.fetchBookingDoctorsList();
    });



    $scope.getMinAmount = function(c_fee, p_fee, v_fee)
    {
        return Math.min(c_fee, p_fee, v_fee);
    }


    $scope.hideAllBlocks = function()
    {
        $(".book_appointment_blocks").hide();
    }


   	$scope.openBookAppointment = function(doctor)
   	{
   		$scope.selectedDoctor = doctor;
   		$scope.hideAllBlocks();
   		initBookingCalendar();
   		$("#schedule_appointment_block").fadeIn();
   	}



   	$scope.initBookingSlots = function()
   	{
   		$scope.availableSlots = [];
		$scope.mormingSlots   = [];
		$scope.eveningSlots   = [];
		$scope.afternoonSlots = [];
		$scope.selectedSlot   = null;
   	}

   	$scope.getAvailableSlots = function (dateString, doctorID)
   	{

   		$scope.isCurrentDateSelected = getCurrentDateString() == dateString;

   		$scope.initBookingSlots();
   		showLoaderMessage(true, 'Fetching booking slots on '+dateString+'...');
		$http({
			method : "GET",
			url : get_available_slots + "?date=" + dateString+ "&doctor_id=" + doctorID
		}).then(function success(response){

			if(response.data.error_message) {

			} else if(response.data.slots) {

				$scope.availableSlots = response.data.slots;
				
				$scope.availableSlots.forEach(function(slot){
					
				
					if(slot.type == 'm') {


						if($scope.isCurrentDateSelected) {

							if(!isTimeLess(slot.start_time+' AM')) {
								$scope.mormingSlots.push(slot);
							}

						} else {
							$scope.mormingSlots.push(slot);
						}

						
					} else if(slot.type == 'a') {


						if($scope.isCurrentDateSelected) {

							if(!isTimeLess(slot.start_time+' PM')) {
								$scope.afternoonSlots.push(slot);
							}

						} else {
							$scope.afternoonSlots.push(slot);
						}


						
					} else if(slot.type == 'e') {	

						if($scope.isCurrentDateSelected) {

							if(!isTimeLess(slot.start_time+' PM')) {
								$scope.eveningSlots.push(slot);
							}

						} else {
							$scope.eveningSlots.push(slot);
						}

					}

				});


			}

			showLoaderMessage(false);
			console.log(response.data);

		}, function error(){
			showLoaderMessage(false);
		});

   	}


   	$scope.bookingDateSelected = function()
   	{
   		$scope.getAvailableSlots($scope.selected_booking_date, $scope.selectedDoctor.id);
   	}



   	$scope.selectedSlot = null;
   	$scope.selectSlot = function(slot)
   	{
   		$scope.selectedSlot = slot; 
   	}


   	$scope.bookNow = function()
   	{
   		if(!$scope.selectedSlot) {
   			toastr.error("Select booking time slot first");
   			return;
   		}

   		$(".payment_method_options").removeClass('color-payment-method-background');
   		$scope.choose_payment_modal_footer = false;
   		$scope.paymentStep = 'choose_payment_type';
   		$("#book_slots_payment_modal").modal("show");

   	}


   	$scope.paymentStep = 'choose_payment_type';
   	$scope.userCards = [];
   	$scope.fetchUserCards = function()
   	{
   		$scope.payment_mode = 'card';

		$http({
			method : "GET",
			url : get_saved_cards
		}).then(function success(response){			

			if(response.data.success) {
				$scope.paymentStep = 'list_cards';	
				$scope.userCards = response.data.cards;
				$scope.choose_payment_modal_footer = true;
			} else {
				$scope.paymentStep = "no_cards";
				$scope.userCards = [];
			}
			
			console.log(response.data);

		}, function error(){
		});

   	}

   	$scope.payment_mode = 'cash';
   	$scope.selectCod = function($event)
   	{
   		$(".payment_method_options").removeClass('color-payment-method-background');
   		$($event.target).addClass('color-payment-method-background');
   		$scope.choose_payment_modal_footer=true;	
   		$scope.payment_mode = 'cash';
   	}
   	


   	$scope.getCardIconURLByType = function(type)
   	{
   		if(type == 'Visa') {
   			return cards_32_base_url+"/visa_32.png";
   		} else if(type == 'paypal') {
   			return cards_32_base_url+"/paypal_32.png";
   		}
   	}



   	$scope.makdeDefaultCard = function(card)
   	{
   		if(card.is_default) {
   			return;
   		}

   		showLoaderMessage(true, 'Making Card as default card ...');
   		$http({
			method : "POST",
			url : make_default_card,
			data : {
				_token : csrf_token,
				last_four : card.last_four
			}
		}).then(function success(response){			

			if(response.data.success) {
				$scope.fetchUserCards();
			}

			showLoaderMessage(false);
		}, function error(){
			showLoaderMessage(false);
		});
   		
   	}

   	$scope.bookingLatitude = "";
   	$scope.bookingLongitude = ""
   	$scope.bookSlot = function()
   	{
   		if(!$scope.selectedSlot) {
   			toastr.error("Select booking time slot first");
   			return;
   		}

   	
	   		/*if($scope.bookingLatitude == "" || $scope.bookingLongitude == "") {
	   			toastr.error('Enter and Choose your location from dropdown');
	   			return;
	   		}*/

   		showLoaderMessage(true, 'Wait while booking slots ...');
   		$http({
			method : "POST",
			url : book_slot,
			data : {
				_token            : csrf_token,
				available_slot_ids: $scope.selectedSlot.available_slot_id,
				payment_mode      : $scope.payment_mode,
				s_latitude        : 12.9081,
				s_longitude		  : 77.6476
			}
		}).then(function success(response){			

			console.log(response.data);


			if(response.data.success) {
				var booking_date = $("#selected_booking_date").val();
				var time = $scope.selectedSlot.start_time+($scope.selectedSlot.type=='m' ? 'am' : 'pm');
				toastr.success('Your booking completed successfully on date : '+booking_date+" Time : "+time);
			} else {

				if(response.data.amount) {
					var msg = response.data.error_message + " (Pending Amount : "+response.data.amount+" "+$scope.user_currency;
				} else {
					var msg = response.data.error_message;
				}
				
				toastr.error(msg);
			}


			showLoaderMessage(false);
			
		}, function error(){
			showLoaderMessage(false);
		});

   	}


   	$scope.makeFavouriteDoctor = function(doctor)
	{

		$http({
			method : "POST",
			url : make_fav_doctor,
			data : {
				doctor_id : doctor.id,
				is_fav : doctor.is_favorite ? 0 : 1
			}
		}).then(
		function success(response){
			if(response.data.success){
				toastr.success(response.data.success_text);
				doctor.is_favorite = doctor.is_favorite ? 0 : 1;
			}
		}, 
		function(error){});

	}


});















categoriesApp.controller('onlineConsultCtrl', function($rootScope, $scope, $http, socket, $timeout) {




	$('#collapse2').collapse("hide");

    $scope.user_currency = user_currency;
    $scope.user_fname = user_fname;
    $scope.user_lname = user_lname;
    $scope.user_photo = user_photo;

    $scope.selectedDoctor = null;


    $scope.rating = {};
    $scope.rating.value = 0;
    $scope.rating.comment = "";


	$scope.onlineDoctorsList = [];
	$scope.speciality_id = initial_spcialitiy_id;

	/* pagination variables */	
	$scope.initPaginationVariables = function()
	{
		$scope.online_doctors_count    = 0;
		$scope.online_doctors_take     = 5;
		$scope.online_doctors_skip     = 0;
		$scope.online_doctors_cur_page = 0;
		$scope.online_doctors_list_cur_page_increment = false;
	}
	$scope.initPaginationVariables();


	$scope.nextOnlineDoctorsList = function()
	{  
		if($scope.online_doctors_skip < $scope.online_doctors_count) {
			$scope.online_doctors_skip += $scope.online_doctors_take;
			$scope.online_doctors_list_cur_page_increment = true;
			$scope.fetchOnlineConsultDoctorsList();
		}
	}

	$scope.previousOnlineDoctorsList = function()
	{
		if($scope.online_doctors_skip > 0) {
			$scope.online_doctors_skip -= $scope.online_doctors_take;
			$scope.online_doctors_list_cur_page_increment = false;
			$scope.fetchOnlineConsultDoctorsList();
		}
	}


	$scope.hidePatination = false;
	$scope.hidePrevious = false;
	$scope.hideNext = false;
	$scope.totalDoctorFetchedCount = 0;
	
	$scope.fetchOnlineConsultDoctorsList = function()
	{
		showLoaderMessage(true, 'Fetching Online Consult Doctors ...');
		$http({
			method : "GET",
			url : get_online_consult_doctors_list 
					+ "?speciality_id=" 
					+ $scope.speciality_id
					+ "&skip="
					+ $scope.online_doctors_skip
					+ "&take="
					+ $scope.online_doctors_take
		}).then(function success(response){
			
			

			if(response.data.success) {

				$scope.online_doctors_count = response.data.count;
				$scope.onlineDoctorsList = response.data.doctor_list;
				$scope.totalDoctorFetchedCount += response.data.doctor_list.length;


				if($scope.online_doctors_list_cur_page_increment) {
					$scope.online_doctors_cur_page++;
				} else {
					$scope.online_doctors_cur_page--;
				}	


				$scope.has_doctors = true;

				if((response.data.doctor_list.length < $scope.online_doctors_take) || ($scope.totalDoctorFetchedCount == $scope.online_doctors_count)) {
					$scope.hideNext = true;
				} else {
					$scope.hideNext = false;
				}


				if($scope.online_doctors_skip > 0) {
					$scope.hidePrevious = false;
				} else {
					$scope.hidePrevious = true;
				}


                
			} else {
                $scope.has_doctors = false;
                $scope.hideNext = true;
            }
        
            $scope.hideAllBlocks();
            $("#online-consult").fadeIn();

			showLoaderMessage(false);
			console.log(response.data);
		}, function error(){
			showLoaderMessage(false);
		});
	}

	$scope.fetchOnlineConsultDoctorsList();


	$scope.lastOngoingRequest = function()
	{
		$http({
			method : "GET",
			url : last_ongoing_request
		}).then(
		function success(response){
			if(response.data.success){
				$scope.consult_type_code = response.data.request.request_type;
				$scope.currentRequestID = response.data.request.id;
				$scope.startGetRequestPoll($scope.currentRequestID);
			}
		}, 
		function(error){});
	}


	$scope.lastOngoingRequest();





	$scope.makeFavouriteDoctor = function(doctor)
	{

		$http({
			method : "POST",
			url : make_fav_doctor,
			data : {
				doctor_id : doctor.id,
				is_fav : doctor.is_favorite ? 0 : 1
			}
		}).then(
		function success(response){
			if(response.data.success){
				toastr.success(response.data.success_text);
				doctor.is_favorite = doctor.is_favorite ? 0 : 1;
			}
		}, 
		function(error){});

	}







	$scope.$on('speciality_selected_event', function (event, selectedSpeciality) {
        $scope.speciality_id = selectedSpeciality;
        $scope.initPaginationVariables();
		$scope.fetchOnlineConsultDoctorsList();
	});





    $scope.getMinAmount = function(c_fee, p_fee, v_fee)
    {
        return Math.min(c_fee, p_fee, v_fee);
    }



    $scope.selectDoctor = function(doctor)
    {
        $scope.selectedDoctor = doctor;
        $scope.selectedDoctorMapLink = "//maps.googleapis.com/maps/api/staticmap?zoom=17&size=400x400&maptype=roadmap&markers=color:red|"+doctor.d_latitude+",%20"+doctor.d_longitude+"&key="+google_map_api_key;
        
        $scope.hideAllBlocks();
        $("#online_doctor_profile_block").fadeIn();

        console.log(doctor);
    }


    $scope.hideAllBlocks = function()
    {
        $(".online-doctor-bolcks").hide();
    }



    $scope.consult_type = 'chat';
    $scope.consult_type_code = 0;
    $scope.consultNow = function()
    {
        if($scope.consult_type == "chat") {

            /*$scope.hideAllBlocks();
            $("#chat_consult_block").fadeIn();*/

            $scope.consult_type_code = 1;
            $scope.requestDoctor();

        } else if($scope.consult_type == "video") {
        	$scope.consult_type_code = 3;
        	$scope.requestDoctor();
        } else if($scope.consult_type == "phone") {
        	$scope.consult_type_code = 2;
        	$scope.requestDoctor();
        } 

    }



    $scope.requestDoctor = function()
    {

    	//showLoaderMessage(true, 'Checking your saved cards...');
    	$http({
			method : "GET",
			url : get_saved_cards
		}).then(function success(response){			

			if(response.data.success && response.data.cards.length) {
				
				$scope.createRequestDoctor();

			} else {
				toastr.error('You have no cards. Add your cards in settings.')
			}
			
			//showLoaderMessage(false);

		}, function error(){
			//showLoaderMessage(false);
		});

	}


	$scope.currentRequestID = null;
	$scope.createRequestDoctor = function(){

    	$("#request_doctor_modal").modal('show');

    	$http({
			method       : "POST",
			url          : create_request_url,
			data         : {
				_token       : csrf_token,
				doctor_id    : $scope.selectedDoctor.id,
				s_latitude   : "",
				s_longitude  : "",
				request_type : $scope.consult_type_code,
				payment_mode : "card"
			}
		}).then(function success(response){		

			$scope.currentRequestID = null;
			if(response.data.success) {
				$scope.currentRequestID = response.data.request_id;
				$scope.startGetRequestPoll($scope.currentRequestID);
			} else {

				$("#request_doctor_modal").modal('hide');
				
				$scope.stopGetRequestPoll();
				$scope.currentRequestID = null;

				//previous payment
				if(response.data.error_code == 568) {

					toastr.error(response.data.error_message);
					$scope.clearDebt();

				} else if(response.data.error_code == 567) {
					//no doctor available
					toastr.error(response.data.error_message);
				}
				
			}

		}, function error(){});
    	
    }



    $scope.clearDebt = function() 
    {
    	showLoaderMessage(true, 'Clearing your previous pending debt..');
    	$http({
			method : "POST",
			url : clearDebt
		}).then(function success(response){			
			showLoaderMessage(false);
		}, function error(){
			showLoaderMessage(false);
		});
    }


    $scope.getRequestPoll = null;
    $scope.startGetRequestPoll = function(request_id)
    {
    	$scope.getRequestPoll = new Polling(
			{
				url           : get_request+"?request_id="+request_id,
				requestMethod : 'GET',
				requestData   : {}
			}, 
         	function(response){

         		console.log(response);
         		if(response.error_code == 101) {
         			toastr.warning("Docotr might rejected your request");
         			$("#request_doctor_modal").modal('hide');
					this.scope.stopGetRequestPoll();
					this.scope.currentRequestID = null;

         		} else if(response.status == 11) {

         			//for audio accept

         			if(this.scope.onGoing) return;

         			$("#request_doctor_modal").modal('hide');
         			//toastr.success('Doctor accepted the call');
         			this.scope.isVideo = false;
         			this.scope.generateOpentokSession();

         		} else if(response.status == 14) {
         			//audio call stopped

         			$("#video-audio-modal").modal('hide');
         			this.scope.onGoing = false;
         			this.scope.getRequestPoll.stopPoll();
         			$scope.invoice = response.invoice[0];
         			console.log($scope.invoice);
         			$("#invoice").modal('show');

         			if(this.scope.session)
         				this.scope.session.disconnect();

         		} else if(response.status == 12) {

         			//for video accept

         			if(this.scope.onGoing) return;

         			$("#request_doctor_modal").modal('hide');
         			//toastr.success('Doctor accepted the video call');
         			this.scope.isVideo = true;
         			$scope.timerSec = 0;
         			$scope.hour = 0;
					$scope.min = 0;
					$scope.sec = 0;
         			this.scope.generateOpentokSession();


         			
         		} else if(response.status == 15) {
         			//video call stopped

         			$("#video-audio-modal").modal('hide');
         			this.scope.onGoing = false;
         			this.scope.getRequestPoll.stopPoll();
         			$scope.invoice = response.invoice[0];
         			console.log($scope.invoice);
         			$("#invoice").modal('show');

         			if(this.scope.session)
         				this.scope.session.disconnect();

         		} else if(response.status == 10) {

         			//for chat accept

         			if(this.scope.onGoing) return;

         			$("#request_doctor_modal").modal('hide');
         			//toastr.success('Doctor accepted chat consult request');

         			$scope.connectChatConsultSocket();

         			
         		} else if(response.status == 13) {
         			//chat stopped
         			this.scope.onGoing = false;
         			this.scope.getRequestPoll.stopPoll();
         			$scope.invoice = response.invoice[0];
         			$("#invoice").modal('show');
         			
         		}

         		if(this.scope.isFullscreen) {
         			this.scope.toggleFullScreen();
         		}

         		this.scope.$apply();
            
         	},
	        function(adf,textStatus,aasf){
	             
	        },
         	$scope, 
         	3000
    	);
    	$scope.getRequestPoll.startPoll();
    }

    $scope.stopGetRequestPoll = function()
    {
    	if($scope.getRequestPoll) {
    		$scope.getRequestPoll.stopPoll();
    	}
    }




    $scope.closeInvoice = function()
    {
    	$("#invoice").modal('hide');
    	$("#rating-modal").modal('show');
    }




    $scope.isVideo = false;
    $scope.onGoing = false;
    $scope.opentokSession = "";
	$scope.opentokToken = "";
	$scope.opentokKey = openTokKey;
	$scope.generateOpentokSession = function()
	{
		$scope.$apply();
		$http({
        	url: generate_opentok+"?request_id="+$scope.currentRequestID,
        	method: "GET"
    	}).then(function(response) {
    		console.log(response.data);

    		if(response.data.success) {
    			$scope.opentokSession =  response.data.session_id;
    			$scope.opentokToken =  response.data.token;
    			$scope.openTokConnection($scope.isVideo);
    		}
    	}, function(response) {});
	}


	$scope.session = null;
	$scope.timerSec = 0;
	$scope.hour = 0;
	$scope.min = 0;
	$scope.sec = 0;
	$scope.openTokConnection = function(video)
	{
		$scope.session = OT.initSession($scope.opentokKey, $scope.opentokSession);


		$scope.session.on('streamCreated', function(event) {
				console.log('stream created');
				try {
					$("#opentok-subscriber").html('');
				} catch(e) {}
				
				return $scope.session.subscribe(event.stream, 'opentok-subscriber', {
				    insertMode: 'append',
				    width: '100%',
				    height: '100%',
				    subscribeToVideo:video
			  	});

		});


		var pubOption = {
	  		insertMode: 'append',
	  		width: '100%',
	  		height: '100%'
		};

		if(!video) {
			pubOption.videoSource = null;
		}

		

		$scope.session.connect($scope.opentokToken, function(error) {

			// If the connection is successful, initialize a publisher and publish to the session
			if (!error) {

				$("#video-audio-modal").modal('show');

				if(typeof publisher !== 'undefined') {
					publisher.destroy();
				}

				publisher = OT.initPublisher('opentok-publisher', pubOption);

	    		$scope.onGoing = true;

	    		return $scope.session.publish(publisher);


	  		} else {
	  			if (error.name === "OT_NOT_CONNECTED") {
			      console.log("You are not connected to the internet. Check your network connection.");
			    }
	    		console.log('There was an error connecting to the session:', error.code, error.message);
	  		}
		});


		$scope.session.on("signal:timer", function(event) {
			$scope.timerSec = event.data;

			d = Number($scope.timerSec);
		    $scope.hour = Math.floor(d / 3600);
		    $scope.min = Math.floor(d % 3600 / 60);
		    $scope.sec = Math.floor(d % 3600 % 60);

				$scope.$apply();
		});



	}

	$scope.messages = [];
	$scope.messageText = "";
	$scope.sendMessageText = function()
	{
		if($scope.messageText == "") {
			return;
		}

		socket.emit('online_chat_message', {
			message : $scope.messageText, 
			message_type : 'TEXT'
		}, function(){
			$scope.messageText = "";
		});
	}


	$scope.connectChatConsultSocket = function()
	{	
		socket.emit("online_chat_consult", {
			request_id : $scope.currentRequestID,
			type : 'user'
		});

	}


	socket.on('online_chat_consult', function(data){
		$scope.timerSec = 0;
		$scope.hour = 0;
		$scope.min = 0;
		$scope.sec = 0;
		$scope.onGoing = true;
		$scope.messages = [];
		$scope.hideAllBlocks();
        $("#chat_consult_block").fadeIn();
		console.log('online_chat_consulted');
	});

	socket.on('timer_tick', function(data){
		d = Number(data);
	    $scope.hour = Math.floor(d / 3600);
	    $scope.min = Math.floor(d % 3600 / 60);
	    $scope.sec = Math.floor(d % 3600 % 60);
	});


	socket.on('online_chat_start_typing', function(data){
		$scope.chat_notif_popup_text = "Doctor is typing ...";
		$scope.chat_notif_popup_fa_icon = 'fa fa-keyboard-o';
		$(".chat-typing").slideDown();
	});

	socket.on('online_chat_stop_typing', function(data){
		$(".chat-typing").slideUp();
	});


	socket.on("online_chat_message", function(data){
		console.log(data);
		$scope.messages.push(data);
		$timeout(function () {
	        $("#online_message_container").scrollTop($("#online_message_container")[0].scrollHeight);
	    }, 100);
	});



	$scope.openUploadFileWindow = function()
	{
		$timeout(function(){
			angular.element(document.querySelector('#chat_upload_file')).click();
		},10);
		
	}


	$scope.validateFileTypes = function(type)
	{
		if(type !== 'image/png'
			&& type !== 'image/jpg'
			&& type !== 'image/jpeg'
			&& type !== 'image/JPG'
			&& type !== 'image/JPEG'
			&& type !== 'image/bmp'
			&& type !== 'image/gif') {

			return false;
		} 

		return true;
	}


	$scope.uploadAttachment = function(files)
	{

		if(!files.length) {
			return;
		}

		if(!$scope.validateFileTypes(files[0].type)) {
			toastr.error('Image types must be PNG, JPEG, BMP, GIF');
			return;
		}


		$scope.chat_notif_popup_text = "Uploading file "+files[0].name+" ...";
		$scope.chat_notif_popup_fa_icon = 'fa fa-upload';
		$(".chat-typing").slideDown();

		var formData = new FormData();
		formData.append('image', files[0]);
		formData.append('request_id', $scope.currentRequestID);

		$http({
			url    : upload_image_url,
			method : "POST",
			data   : formData,
			headers: {'Content-Type': undefined},
            transformRequest: angular.identity,
    	}).then(function(response) {
    		
    		if(response.data.success) {

    			socket.emit('online_chat_message', {
					message : response.data.image, 
					message_type : 'IMAGE'
				});

    		} else {

    			toastr.error('failed to upload');
    		}
    		$(".chat-typing").slideUp();
    	}, function(response) {
    		$(".chat-typing").slideUp();
    	});

	

	}






	$scope.messageText = "";
	$scope.sendMessageText = function()
	{
		if($scope.messageText == "") {
			return;
		}
		
		socket.emit('online_chat_message', {
			message : $scope.messageText, 
			message_type : 'TEXT'
		});
		$scope.messageText = "";
	}



	$scope.isTyping = false;
	$scope.emitTyping = function(){

        if (!$scope.isTyping) {
            $scope.isTyping = true;
            socket.emit('online_chat_start_typing', {});
        }
        
        startTypingTime = (new Date()).getTime();

        $timeout(function(){

            var currTime = (new Date()).getTime();
            var timeDiff    = currTime - startTypingTime;
            
            if (timeDiff >= 500 && $scope.isTyping) {
                socket.emit('online_chat_stop_typing', {});
                $scope.isTyping = false;
            }

        }, 1000);
    
    }




	$scope.giveRating = function()
	{

		if($scope.rating.comment === "") {
			toastr.warning('Type your comment first');
			return;
		}


		showLoaderMessage(true, "Rating doctor ..");
		$http({
        	url: rate_doctor,
        	method: "POST",
        	data : {
        		request_id : $scope.currentRequestID,
        		rating : $scope.rating.value,
        		comment : $scope.rating.comment
        	}
    	}).then(function(response) {
    		
    		if(response.data.success) {
    			$scope.rating.comment = "";
    			$scope.rating.value = 0;
    			toastr.success('Doctor rated successfully.');
    			$("#rating-modal").modal('hide');
    			$scope.hideAllBlocks();
 				$("#online-consult").fadeIn();
    		} else {
    			toastr.error('Doctor rating failed. Try again');
    		}

    		showLoaderMessage(false);

    	}, function(response) {
    		showLoaderMessage(false);
    		toastr.error('Doctor rating failed.');
    	});
	}




    $scope.cancelRequest = function()
    {
    	$http({
			method       : "POST",
			url          : cancel_request_url,
			data         : {
				_token       : csrf_token,
				request_id    : $scope.currentRequestID
			}
		}).then(function success(response){			
			
			if(response.data.success) {
				$("#request_doctor_modal").modal('hide');
				$scope.stopGetRequestPoll();
				$scope.currentRequestID = null;
			}

		}, function error(){});
    }





    /* socket codes */
    socket.on('connected', function(data){
		console.log('you are connected '+data);
	});








    $scope.isFullscreen = false;
    $scope.toggleFullScreen = function()
    {
    	console.log('asfdas');
    	if(
			document.fullscreenEnabled || 
			document.webkitFullscreenEnabled || 
			document.mozFullScreenEnabled ||
			document.msFullscreenEnabled
		) {
			console.log('in');
			var i = document.querySelector("#video-audio-modal .modal-content");
			console.log($scope.isFullscreen);
			if(!$scope.isFullscreen) {

				if (i.requestFullscreen) {
					i.requestFullscreen();
				} else if (i.webkitRequestFullscreen) {
					i.webkitRequestFullscreen();
				} else if (i.mozRequestFullScreen) {
					i.mozRequestFullScreen();
				} else if (i.msRequestFullscreen) {
					i.msRequestFullscreen();
				}

				$scope.isFullscreen = true;

			} else {

				if (document.exitFullscreen) {
					document.exitFullscreen();
				} else if (document.webkitCancelFullScreen) {
					document.webkitCancelFullScreen();
				} else if (document.mozCancelFullScreen) {
					document.mozCancelFullScreen();
				} else if (document.msExitFullscreen) {
					document.msExitFullscreen();
				}

				$scope.isFullscreen = false;
			}
    		
			

		}


    }


});